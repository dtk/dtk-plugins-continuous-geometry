// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore>

class dtkClippedNurbsSurface;
class dtkNurbsSurface;
class dtkAbstractNurbsCurve2DData;
class dtkNurbsCurve2D;
class dtkAbstractNurbsSurfaceData;
class dtkNurbsSurface;
class dtkAbstractTrimLoopData;
class dtkTrimLoop;
class dtkTopoTrim;
class dtkTrim;

class dtkClippedNurbsSurfaceTestCase : public QObject
{
    Q_OBJECT

public:
             dtkClippedNurbsSurfaceTestCase(void);
    virtual ~dtkClippedNurbsSurfaceTestCase(void);

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);

private :
    dtkClippedNurbsSurface* m_clipped_nurbs_surface;

    dtkAbstractNurbsSurfaceData *m_nurbs_surface_data;
    dtkNurbsSurface *m_nurbs_surface;

    dtkAbstractTrimLoopData* m_trim_loop_data;
    dtkTrimLoop *m_trim_loop;
    dtkTopoTrim *m_topo_trim;
    dtkAbstractNurbsCurve2DData *m_nurbs_curve_2d_data;
    dtkNurbsCurve2D *m_nurbs_curve_2d;
    dtkTrim *m_trim;

    dtkAbstractTrimLoopData* m_trim_loop_data_outer;
    dtkTrimLoop *m_trim_loop_outer;
    dtkTopoTrim *m_topo_trim_0;
    dtkTopoTrim *m_topo_trim_1;
    dtkTopoTrim *m_topo_trim_2;
    dtkTopoTrim *m_topo_trim_3;
    dtkAbstractNurbsCurve2DData *m_nurbs_curve_2d_data_0;
    dtkAbstractNurbsCurve2DData *m_nurbs_curve_2d_data_1;
    dtkAbstractNurbsCurve2DData *m_nurbs_curve_2d_data_2;
    dtkAbstractNurbsCurve2DData *m_nurbs_curve_2d_data_3;
    dtkNurbsCurve2D *m_nurbs_curve_2d_0;
    dtkNurbsCurve2D *m_nurbs_curve_2d_1;
    dtkNurbsCurve2D *m_nurbs_curve_2d_2;
    dtkNurbsCurve2D *m_nurbs_curve_2d_3;
    dtkTrim *m_trim_0;
    dtkTrim *m_trim_1;
    dtkTrim *m_trim_2;
    dtkTrim *m_trim_3;
};


//
// dtkClippedNurbsSurfaceTest.h ends here
