// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore>

class dtkTopoTrim;
class dtkTrim;
class dtkNurbsCurve2D;
class dtkNurbsSurface;
class dtkAbstractNurbsCurve2DData;
class dtkAbstractNurbsSurfaceData;

class dtkTrimTestCase : public QObject
{
    Q_OBJECT

public:
             dtkTrimTestCase(void);
    virtual ~dtkTrimTestCase(void);

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testToPolyline(void);

private slots:


private slots:
    void cleanupTestCase(void);
    void cleanup(void);

private :
    dtkTopoTrim* m_topo_trim;
    dtkTrim* m_trim;
    dtkAbstractNurbsCurve2DData* m_nurbs_curve_data;
    dtkAbstractNurbsSurfaceData* m_nurbs_surface_data;
    dtkNurbsCurve2D* m_nurbs_curve;
    dtkNurbsSurface* m_nurbs_surface;
};


//
// dtkTrimTest.h ends here
