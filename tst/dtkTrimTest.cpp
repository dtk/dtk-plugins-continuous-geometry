// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkTrimTest.h"

#include <dtkTrim>

#include <fstream>

#include <QtCore>
#include <QTest>

#include <dtkTest>
#include <dtkLog>

#include <dtkContinuousGeometryUtils>
#include <dtkAbstractNurbsCurve2DData>
#include <dtkAbstractNurbsSurfaceData>
#include <dtkNurbsCurve2D>
#include <dtkNurbsSurface>

#include <dtkTopoTrim>
#include <dtkContinuousGeometry>

dtkTrimTestCase::dtkTrimTestCase(void) : m_topo_trim(nullptr), m_trim(nullptr), m_nurbs_curve_data(nullptr), m_nurbs_surface_data(nullptr), m_nurbs_curve(nullptr), m_nurbs_surface(nullptr)
{
    dtkLogger::instance().attachConsole();

    dtkContinuousGeometrySettings settings;
    settings.beginGroup("continuous-geometry");
    dtkContinuousGeometry::initialize(settings.value("plugins").toString());
    settings.endGroup();
}

dtkTrimTestCase::~dtkTrimTestCase(void)
{
}

void dtkTrimTestCase::initTestCase(void)
{
   std::size_t dim = 2;
   std::size_t nb_cp = 3;
   std::size_t order = 3;

    /* Forces openNURBS plugins to be compiled and working
       TODO : change to default implementation
    pp*/
    m_nurbs_curve_data = dtkContinuousGeometry::abstractNurbsCurve2DData::pluginFactory().create("dtkNurbsCurve2DDataOn");
    m_nurbs_surface_data = dtkContinuousGeometry::abstractNurbsSurfaceData::pluginFactory().create("dtkNurbsSurfaceDataOn");

    if (m_nurbs_curve_data == nullptr) {
        dtkFatal() << "The dtkAbstractNurbsCurve2DData could not be loaded by the factory under the openNURBS implementation";
    }
    if (m_nurbs_surface_data == nullptr) {
        dtkFatal() << "The dtkAbstractNurbsSurfaceData could not be loaded by the factory under the openNURBS implementation";
    }

    m_nurbs_curve = new dtkNurbsCurve2D(m_nurbs_curve_data);
    m_nurbs_surface = new dtkNurbsSurface(m_nurbs_surface_data);

    /* memory leak */
    double* knots = new double[nb_cp + order - 2];
    /* 0. */ knots[0] = 0.; knots[1] = 0.; knots[2] = 2.; knots[3] = 2.; /* 2. */

    double* cps_1 = new double[(dim + 1) * nb_cp];
    cps_1[0] = 0.;   cps_1[1] = 0.; cps_1[2] = 1.;
    cps_1[3] = 0.;   cps_1[4] = 3.46; cps_1[5] = 1.;
    cps_1[6] = 2.;   cps_1[7] = 3.46; cps_1[8] = 1.;

    m_nurbs_curve->create(nb_cp, order, knots, cps_1);
    m_topo_trim = new dtkTopoTrim();
    m_trim = new dtkTrim(*m_nurbs_surface, *m_nurbs_curve, m_topo_trim);
}

void dtkTrimTestCase::init(void)
{

}

void dtkTrimTestCase::testToPolyline(void)
{
    std::list< dtkContinuousGeometryPrimitives::Point_2 > polyline;
    m_trim->toPolyline(polyline, 0.1);

    QVERIFY(polyline.size() == 21);
    QVERIFY((*polyline.begin()) == dtkContinuousGeometryPrimitives::Point_2(0., 0.));
    QVERIFY((*std::prev(polyline.end())) == dtkContinuousGeometryPrimitives::Point_2(2., 3.46));
}

void dtkTrimTestCase::cleanup(void)
{
}

void dtkTrimTestCase::cleanupTestCase(void)
{
    delete m_topo_trim;
    delete m_trim;
    delete m_nurbs_curve;
    delete m_nurbs_surface;

}

DTKTEST_MAIN_NOGUI(dtkTrimTest, dtkTrimTestCase)

//
// dtkTrimTest.cpp ends here
