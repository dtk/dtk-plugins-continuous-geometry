// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkClippedTrimTest.h"

#include <QtCore>
#include <QTest>

#include <dtkLog>
#include <dtkTest>

#include <dtkContinuousGeometryUtils>

#include <dtkClippedTrim>

#include <dtkContinuousGeometry>
#include <dtkAbstractNurbsSurfaceData>
#include <dtkAbstractNurbsCurve2DData>
#include <dtkNurbsSurface>
#include <dtkNurbsCurve2D>
#include <dtkTrim>
#include <dtkTopoTrim>

dtkClippedTrimTestCase::dtkClippedTrimTestCase(void) : m_clipped_trim(nullptr), m_trim(nullptr), m_nurbs_curve_2d_data(nullptr), m_nurbs_curve_2d(nullptr), m_nurbs_surface_data(nullptr), m_nurbs_surface(nullptr), m_topo_trim(nullptr)
{
    dtkLogger::instance().attachConsole();

    dtkContinuousGeometrySettings settings;
    settings.beginGroup("continuous-geometry");
    dtkContinuousGeometry::initialize(settings.value("plugins").toString());
    settings.endGroup();
}

dtkClippedTrimTestCase::~dtkClippedTrimTestCase(void)
{
}

void dtkClippedTrimTestCase::initTestCase(void)
{
    /* Forces openNURBS plugins to be compiled and working
       TODO : change to default implementation
    */
    m_nurbs_curve_2d_data = dtkContinuousGeometry::abstractNurbsCurve2DData::pluginFactory().create("dtkNurbsCurve2DDataOn");
    m_nurbs_surface_data = dtkContinuousGeometry::abstractNurbsSurfaceData::pluginFactory().create("dtkNurbsSurfaceDataOn");

    if (m_nurbs_curve_2d_data == nullptr) {
        dtkFatal() << "The dtkAbstractNurbsCurve2DData could not be loaded by the factory under the openNURBS implementation";
    }
    if (m_nurbs_surface_data == nullptr) {
        dtkFatal() << "The dtkAbstractNurbsSurfaceData could not be loaded by the factory under the openNURBS implementation";
    }

    m_nurbs_curve_2d = new dtkNurbsCurve2D(m_nurbs_curve_2d_data);
    std::size_t c_dim = 2;
    std::size_t nb_cp = 3;
    std::size_t order = 2;

    /* memory leak */
    double* knots = new double[nb_cp + order - 2];
    /* -1. */ knots[0] = -1.; knots[1] = 0.; knots[2] = 1.;/* 1. */

    double* c_cps = new double[(c_dim + 1) * nb_cp];
    c_cps[0] = 1.5;   c_cps[1] = 0.5; c_cps[2] = 1.;
    c_cps[3] = 1.5;   c_cps[4] = 1.5; c_cps[5] = 1.;
    c_cps[3] = 1.5;   c_cps[4] = 2.5; c_cps[5] = 1.;

    m_nurbs_curve_2d->create(nb_cp, order, knots, c_cps);

    // ///////////////////////////////////////////////////////////////////

    m_nurbs_surface = new dtkNurbsSurface(m_nurbs_surface_data);
    //bi-degree 1,2
    std::size_t s_dim = 3;
    std::size_t nb_cp_u = 4;
    std::size_t nb_cp_v = 4;
    std::size_t order_u = 2;
    std::size_t order_v = 2;
    /* memory leak */
    double* knots_u = new double[nb_cp_u + order_u - 2];
    /* 0. */ knots_u[0] = 0.; knots_u[1] = 1.; knots_u[2] = 2.; knots_u[3] = 3.; /* 3. */
    double* knots_v = new double[nb_cp_v + order_v - 2];
    /* 0. */ knots_v[0] = 0.; knots_v[1] = 1.; knots_v[2] = 2.; knots_v[3] = 3.; /* 3. */

    double* cps = new double[(s_dim + 1) * nb_cp_u * nb_cp_v];
    cps[0]  = 0.;  cps[1]  = 0.; cps[2]  = -1.; cps[3]  = 1.;
    cps[4]  = 1.;  cps[5]  = 0.; cps[6]  = 1.; cps[7]   = 1.;
    cps[8]  = 2.;  cps[9]  = 0.; cps[10] = 0.; cps[11]  = 1.;
    cps[12] = 3.;  cps[13] = 0.; cps[14] = 1.; cps[15]  = 1.;
    cps[16] = 0.;  cps[17] = 1.; cps[18] = 1.; cps[19]  = 1.;
    cps[20] = 1.;  cps[21] = 1.; cps[22] = 0.; cps[23]  = 1.;
    cps[24] = 2.;  cps[25] = 1.; cps[26] = 1.; cps[27]  = 1.;
    cps[28] = 3.;  cps[29] = 1.; cps[30] = -1.; cps[31] = 1.;
    cps[32] = 0.;  cps[33] = 2.; cps[34] = -1.; cps[35] = 1.;
    cps[36] = 1.;  cps[37] = 2.; cps[38] = 1.; cps[39]  = 1.;
    cps[40] = 2.;  cps[41] = 2.; cps[42] = -1.; cps[43] = 1.;
    cps[44] = 3.;  cps[45] = 2.; cps[46] = 0.; cps[47]  = 1.;
    cps[48] = 0.;  cps[49] = 3.; cps[50] = 0.; cps[51]  = 1.;
    cps[52] = 1.;  cps[53] = 3.; cps[54] = -1.; cps[55] = 1.;
    cps[56] = 2.;  cps[57] = 3.; cps[58] = 1.; cps[59]  = 1.;
    cps[60] = 3.;  cps[61] = 3.; cps[62] = 1.; cps[63]  = 1.;
    m_nurbs_surface_data->create(s_dim, nb_cp_u, nb_cp_v, order_u, order_v, knots_u, knots_v, cps);

    m_topo_trim = new dtkTopoTrim();
    m_trim = new dtkTrim(*m_nurbs_surface, *m_nurbs_curve_2d, m_topo_trim);
    m_clipped_trim = new dtkClippedTrim(*m_trim);
    QVERIFY(m_clipped_trim->m_rational_bezier_curves_2d.size() == 2);
}

void dtkClippedTrimTestCase::init(void)
{

}

void dtkClippedTrimTestCase::testSplit(void)
{
    std::vector< dtkContinuousGeometryPrimitives::Line_2 > clipping_lines;
    clipping_lines.push_back(dtkContinuousGeometryPrimitives::Line_2(dtkContinuousGeometryPrimitives::Point_2(0., 1.), dtkContinuousGeometryPrimitives::Vector_2(1., 0.)));
    clipping_lines.push_back(dtkContinuousGeometryPrimitives::Line_2(dtkContinuousGeometryPrimitives::Point_2(0., 2.), dtkContinuousGeometryPrimitives::Vector_2(1., 0.)));

    m_clipped_trim->split(clipping_lines);

    QVERIFY(m_clipped_trim->m_rational_bezier_curves_2d.size() == 4);
}

void dtkClippedTrimTestCase::cleanup(void)
{
}

void dtkClippedTrimTestCase::cleanupTestCase(void)
{
    delete m_clipped_trim;

    delete m_trim;
    delete m_nurbs_curve_2d;
    delete m_nurbs_surface;
    delete m_topo_trim;
}

DTKTEST_MAIN_NOGUI(dtkClippedTrimTest, dtkClippedTrimTestCase)

//
// dtkClippedTrimTest.cpp ends here
