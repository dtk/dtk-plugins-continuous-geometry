// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore>

class dtkClippedTrim;
class dtkTrim;
class dtkAbstractNurbsCurve2DData;
class dtkNurbsCurve2D;
class dtkAbstractNurbsSurfaceData;
class dtkNurbsSurface;
class dtkTopoTrim;

class dtkClippedTrimTestCase : public QObject
{
    Q_OBJECT

public:
             dtkClippedTrimTestCase(void);
    virtual ~dtkClippedTrimTestCase(void);

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testSplit(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);

private :
    dtkClippedTrim* m_clipped_trim;
    dtkTrim *m_trim;
    dtkAbstractNurbsCurve2DData *m_nurbs_curve_2d_data;
    dtkNurbsCurve2D *m_nurbs_curve_2d;
    dtkAbstractNurbsSurfaceData *m_nurbs_surface_data;
    dtkNurbsSurface *m_nurbs_surface;
    dtkTopoTrim *m_topo_trim;
};


//
// dtkClippedTrimTest.h ends here
