// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkClippedNurbsSurfaceTest.h"

#include <QtCore>
#include <QTest>

#include <dtkLog>
#include <dtkTest>

#include <dtkContinuousGeometryUtils>

#include <dtkClippedNurbsSurface>

#include <dtkContinuousGeometry>
#include <dtkAbstractNurbsSurfaceData>
#include <dtkNurbsSurface>
#include <dtkRationalBezierCurve>
#include <dtkRationalBezierSurface>

#include <dtkAbstractNurbsCurve2DData>
#include <dtkNurbsCurve2D>

#include <dtkTrim>
#include <dtkTopoTrim>

#include <dtkAbstractTrimLoopData>
#include <dtkTrimLoop>

#include <dtkClippedTrim>
#include <dtkClippedTrimLoop>

dtkClippedNurbsSurfaceTestCase::dtkClippedNurbsSurfaceTestCase(void) : m_clipped_nurbs_surface(nullptr), m_nurbs_surface_data(nullptr), m_nurbs_surface(nullptr), m_trim_loop_data(nullptr), m_trim_loop(nullptr), m_topo_trim(nullptr), m_nurbs_curve_2d_data(nullptr), m_nurbs_curve_2d(nullptr), m_trim(nullptr), m_trim_loop_data_outer(nullptr), m_trim_loop_outer(nullptr), m_topo_trim_0(nullptr), m_topo_trim_1(nullptr), m_topo_trim_2(nullptr), m_topo_trim_3(nullptr), m_nurbs_curve_2d_data_0(nullptr), m_nurbs_curve_2d_data_1(nullptr), m_nurbs_curve_2d_data_2(nullptr), m_nurbs_curve_2d_data_3(nullptr), m_nurbs_curve_2d_0(nullptr), m_nurbs_curve_2d_1(nullptr), m_nurbs_curve_2d_2(nullptr), m_nurbs_curve_2d_3(nullptr), m_trim_0(nullptr), m_trim_1(nullptr), m_trim_2(nullptr), m_trim_3(nullptr)
{
    dtkLogger::instance().attachConsole();

    dtkContinuousGeometrySettings settings;
    settings.beginGroup("continuous-geometry");
    dtkContinuousGeometry::initialize(settings.value("plugins").toString());
    settings.endGroup();
}

dtkClippedNurbsSurfaceTestCase::~dtkClippedNurbsSurfaceTestCase(void)
{
}

void dtkClippedNurbsSurfaceTestCase::initTestCase(void)
{
    /* Forces openNURBS plugins to be compiled and working
       TODO : change to default implementation
    */
    m_nurbs_curve_2d_data = dtkContinuousGeometry::abstractNurbsCurve2DData::pluginFactory().create("dtkNurbsCurve2DDataOn");
    m_nurbs_curve_2d_data_0 = dtkContinuousGeometry::abstractNurbsCurve2DData::pluginFactory().create("dtkNurbsCurve2DDataOn");
    m_nurbs_curve_2d_data_1 = dtkContinuousGeometry::abstractNurbsCurve2DData::pluginFactory().create("dtkNurbsCurve2DDataOn");
    m_nurbs_curve_2d_data_2 = dtkContinuousGeometry::abstractNurbsCurve2DData::pluginFactory().create("dtkNurbsCurve2DDataOn");
    m_nurbs_curve_2d_data_3 = dtkContinuousGeometry::abstractNurbsCurve2DData::pluginFactory().create("dtkNurbsCurve2DDataOn");

    m_nurbs_surface_data = dtkContinuousGeometry::abstractNurbsSurfaceData::pluginFactory().create("dtkNurbsSurfaceDataOn");

    if (m_nurbs_curve_2d_data == nullptr || m_nurbs_curve_2d_data_0 == nullptr || m_nurbs_curve_2d_data_1 == nullptr || m_nurbs_curve_2d_data_2 == nullptr || m_nurbs_curve_2d_data_3 == nullptr) {
        dtkFatal() << "The dtkAbstractNurbsCurve2DData could not be loaded by the factory under the openNURBS implementation";
    }
    if (m_nurbs_surface_data == nullptr) {
        dtkFatal() << "The dtkAbstractNurbsSurfaceData could not be loaded by the factory under the openNURBS implementation";
    }

    m_nurbs_curve_2d = new dtkNurbsCurve2D(m_nurbs_curve_2d_data);
    std::size_t c_dim = 2;
    std::size_t nb_cp = 3;
    std::size_t order = 2;

    /* memory leak */
    double* knots = new double[nb_cp + order - 2];
    /* -1. */ knots[0] = -1.; knots[1] = 0.; knots[2] = 1.;/* 1. */

    double* c_cps = new double[(c_dim + 1) * nb_cp];
    c_cps[0] = 1.5;   c_cps[1] = 0.5; c_cps[2] = 1.;
    c_cps[3] = 1.5;   c_cps[4] = 1.5; c_cps[5] = 1.;
    c_cps[6] = 1.5;   c_cps[7] = 2.5; c_cps[8] = 1.;

    m_nurbs_curve_2d->create(nb_cp, order, knots, c_cps);

    std::size_t c_dim_i = 2;
    std::size_t nb_cp_i = 2;
    std::size_t order_i = 2;

    m_nurbs_curve_2d_0 = new dtkNurbsCurve2D(m_nurbs_curve_2d_data_0);
    m_nurbs_curve_2d_1 = new dtkNurbsCurve2D(m_nurbs_curve_2d_data_1);
    m_nurbs_curve_2d_2 = new dtkNurbsCurve2D(m_nurbs_curve_2d_data_2);
    m_nurbs_curve_2d_3 = new dtkNurbsCurve2D(m_nurbs_curve_2d_data_3);

    /* memory leak */
    double* knots_0 = new double[nb_cp_i + order_i - 2];
    /* 0. */ knots_0[0] = 0.; knots_0[1] = 1.; /* 1. */

    double* c_cps_0 = new double[(c_dim_i + 1) * nb_cp_i];
    c_cps_0[0] = 0.;   c_cps_0[1] = 0.; c_cps_0[2] = 1.;
    c_cps_0[3] = 3.;   c_cps_0[4] = 0.; c_cps_0[5] = 1.;

    m_nurbs_curve_2d_0->create(nb_cp_i, order_i, knots_0, c_cps_0);

    /* memory leak */
    double* knots_1 = new double[nb_cp_i + order_i - 2];
    /* 0. */ knots_1[0] = 0.; knots_1[1] = 1.;/* 1. */

    double* c_cps_1 = new double[(c_dim_i + 1) * nb_cp_i];
    c_cps_1[0] = 3.;   c_cps_1[1] = 0.; c_cps_1[2] = 1.;
    c_cps_1[3] = 3.;   c_cps_1[4] = 3.; c_cps_1[5] = 1.;

    m_nurbs_curve_2d_1->create(nb_cp_i, order_i, knots_1, c_cps_1);

    /* memory leak */
    double* knots_2 = new double[nb_cp_i + order_i - 2];
    /* 0. */ knots_2[0] = 0.; knots_2[1] = 1.;/* 1. */

    double* c_cps_2 = new double[(c_dim_i + 1) * nb_cp_i];
    c_cps_2[0] = 3.;   c_cps_2[1] = 3.; c_cps_2[2] = 1.;
    c_cps_2[3] = 0.;   c_cps_2[4] = 3.; c_cps_2[5] = 1.;

    m_nurbs_curve_2d_2->create(nb_cp_i, order_i, knots_2, c_cps_2);

    /* memory leak */
    double* knots_3 = new double[nb_cp_i + order_i - 2];
    /* 0. */ knots_3[0] = 0.; knots_3[1] = 1.;/* 1. */

    double* c_cps_3 = new double[(c_dim_i + 1) * nb_cp_i];
    c_cps_3[0] = 0.;   c_cps_3[1] = 3.; c_cps_3[2] = 1.;
    c_cps_3[3] = 0.;   c_cps_3[4] = 0.; c_cps_3[5] = 1.;

    m_nurbs_curve_2d_3->create(nb_cp_i, order_i, knots_3, c_cps_3);

    // ///////////////////////////////////////////////////////////////////

    m_nurbs_surface = new dtkNurbsSurface(m_nurbs_surface_data);
    //bi-degree 1,2
    std::size_t s_dim = 3;
    std::size_t nb_cp_u = 4;
    std::size_t nb_cp_v = 4;
    std::size_t order_u = 2;
    std::size_t order_v = 2;
    /* memory leak */
    double* knots_u = new double[nb_cp_u + order_u - 2];
    /* 0. */ knots_u[0] = 0.; knots_u[1] = 1.; knots_u[2] = 2.; knots_u[3] = 3.; /* 3. */
    double* knots_v = new double[nb_cp_v + order_v - 2];
    /* 0. */ knots_v[0] = 0.; knots_v[1] = 1.; knots_v[2] = 2.; knots_v[3] = 3.; /* 3. */

    double* cps = new double[(s_dim + 1) * nb_cp_u * nb_cp_v];
    cps[0]  = 0.;  cps[1]  = 0.; cps[2]  = -1.; cps[3]  = 1.;
    cps[4]  = 1.;  cps[5]  = 0.; cps[6]  = 1.; cps[7]   = 1.;
    cps[8]  = 2.;  cps[9]  = 0.; cps[10] = 0.; cps[11]  = 1.;
    cps[12] = 3.;  cps[13] = 0.; cps[14] = 1.; cps[15]  = 1.;
    cps[16] = 0.;  cps[17] = 1.; cps[18] = 1.; cps[19]  = 1.;
    cps[20] = 1.;  cps[21] = 1.; cps[22] = 0.; cps[23]  = 1.;
    cps[24] = 2.;  cps[25] = 1.; cps[26] = 1.; cps[27]  = 1.;
    cps[28] = 3.;  cps[29] = 1.; cps[30] = -1.; cps[31] = 1.;
    cps[32] = 0.;  cps[33] = 2.; cps[34] = -1.; cps[35] = 1.;
    cps[36] = 1.;  cps[37] = 2.; cps[38] = 1.; cps[39]  = 1.;
    cps[40] = 2.;  cps[41] = 2.; cps[42] = -1.; cps[43] = 1.;
    cps[44] = 3.;  cps[45] = 2.; cps[46] = 0.; cps[47]  = 1.;
    cps[48] = 0.;  cps[49] = 3.; cps[50] = 0.; cps[51]  = 1.;
    cps[52] = 1.;  cps[53] = 3.; cps[54] = -1.; cps[55] = 1.;
    cps[56] = 2.;  cps[57] = 3.; cps[58] = 1.; cps[59]  = 1.;
    cps[60] = 3.;  cps[61] = 3.; cps[62] = 1.; cps[63]  = 1.;
    m_nurbs_surface->create(s_dim, nb_cp_u, nb_cp_v, order_u, order_v, knots_u, knots_v, cps);

    m_trim_loop_data = dtkContinuousGeometry::abstractTrimLoopData::pluginFactory().create("dtkTrimLoopDataCgal");
    m_trim_loop_data_outer = dtkContinuousGeometry::abstractTrimLoopData::pluginFactory().create("dtkTrimLoopDataCgal");

    if (m_trim_loop_data == nullptr || m_trim_loop_data_outer == nullptr) {
        dtkFatal() << "In : " << Q_FUNC_INFO << " The dtkAbstractTrimLoopData using cgal implementation could not be created. Check that the cgal plugin is compiled.";
    }
    m_trim_loop = new dtkTrimLoop(m_trim_loop_data);
    m_trim_loop_outer = new dtkTrimLoop(m_trim_loop_data_outer);

    m_topo_trim = new dtkTopoTrim();
    m_topo_trim_0 = new dtkTopoTrim();
    m_topo_trim_1 = new dtkTopoTrim();
    m_topo_trim_2 = new dtkTopoTrim();
    m_topo_trim_3 = new dtkTopoTrim();

    m_trim = new dtkTrim(*m_nurbs_surface, *m_nurbs_curve_2d, m_topo_trim);
    m_trim_0 = new dtkTrim(*m_nurbs_surface, *m_nurbs_curve_2d_0, m_topo_trim_0);
    m_trim_1 = new dtkTrim(*m_nurbs_surface, *m_nurbs_curve_2d_1, m_topo_trim_1);
    m_trim_2 = new dtkTrim(*m_nurbs_surface, *m_nurbs_curve_2d_2, m_topo_trim_2);
    m_trim_3 = new dtkTrim(*m_nurbs_surface, *m_nurbs_curve_2d_3, m_topo_trim_3);
    m_trim_loop->trims().push_back(m_trim);
    m_trim_loop_outer->trims().push_back(m_trim_0);
    m_trim_loop_outer->trims().push_back(m_trim_1);
    m_trim_loop_outer->trims().push_back(m_trim_2);
    m_trim_loop_outer->trims().push_back(m_trim_3);
    m_nurbs_surface->trimLoops().push_back(m_trim_loop);
    m_nurbs_surface->trimLoops().push_back(m_trim_loop_outer);

    m_clipped_nurbs_surface = new dtkClippedNurbsSurface(*m_nurbs_surface);

    QVERIFY(m_clipped_nurbs_surface->m_clipped_trim_loops.size() == 2);
    QVERIFY(m_clipped_nurbs_surface->m_clipped_trim_loops.front()->m_clipped_trims.size() == 1);
    QVERIFY(m_clipped_nurbs_surface->m_clipped_trim_loops.front()->m_clipped_trims[0]->m_rational_bezier_curves.size() == 4);
    QVERIFY(m_clipped_nurbs_surface->m_clipped_trim_loops.back()->m_clipped_trims[0]->m_rational_bezier_curves.size() == 3);

    std::ofstream clip_file("clip.xyz");
    dtkContinuousGeometryPrimitives::Point_3 p(0., 0., 0.);

    for (auto clip_trim_loop : m_clipped_nurbs_surface->m_clipped_trim_loops) {
        for (auto clip_trim : clip_trim_loop->m_clipped_trims) {
            for (auto clip : clip_trim->m_rational_bezier_curves) {
                for (double i = 0.; i <= 1.0; i+=0.1) {
                    clip->evaluatePoint(i, p.data());
                    clip_file << p[0] << " " << p[1] << " " << p[2] << std::endl;
                }
            }
        }
    }

    std::ofstream surface_file("nurbs_surface.xyz");
    for(double i = 0.; i <= 3.; i += 0.06) {
        for(double j = 0.; j <= 3.; j += 0.06) {
            m_nurbs_surface->evaluatePoint(i, j, p.data());
            surface_file << p[0] << " " << p[1] << " " << p[2] << std::endl;
        }
    }
}

void dtkClippedNurbsSurfaceTestCase::init(void)
{

}

void dtkClippedNurbsSurfaceTestCase::cleanup(void)
{
}

void dtkClippedNurbsSurfaceTestCase::cleanupTestCase(void)
{
    delete m_clipped_nurbs_surface;

    delete m_trim;
    delete m_nurbs_curve_2d;
    delete m_nurbs_surface;
    delete m_topo_trim;
}

DTKTEST_MAIN_NOGUI(dtkClippedNurbsSurfaceTest, dtkClippedNurbsSurfaceTestCase)

//
// dtkClippedNurbsSurfaceTest.cpp ends here
