#include "Standard_DomainError.hxx"
#include "Standard_TypeDef.hxx"

#include <BRep_CurveOnSurface.hxx>
#include <BRep_TEdge.hxx>
#include <BRep_TFace.hxx>
#include <GCPnts_UniformDeflection.hxx>
#include <Geom2dAdaptor_Curve.hxx>
#include <Geom_Surface.hxx>
#include <STEPControl_Reader.hxx>
#include <TopoDS.hxx>
#include <TopoDS_Iterator.hxx>
#include <TopoDS_Shape.hxx>

#include <BRepBuilderAPI_NurbsConvert.hxx>

#include <cassert>

void dump_2d_pcurves_of_3rd_face(std::string name, const TopoDS_Shape& solid)
{
  TopoDS_Iterator iterator_main_shape{solid};
  auto shell = iterator_main_shape.Value();
  TopoDS_Iterator iterator_shell{shell};
  for(int i = 0; i < 2; ++i)
    iterator_shell.Next();
  auto face = iterator_shell.Value();
  auto brep_tface = Handle(BRep_TFace)::DownCast(face.TShape());
  assert(!brep_tface.IsNull());
  auto surface = brep_tface->Surface();

  TopoDS_Iterator wire_iterator(face);
  auto wire = wire_iterator.Value();
  // BRepTools::Dump(wire, std::cerr);
  std::ofstream out(name + ".polylines.txt");
  out.precision(17);
  double maximal_distance_between_endpoints = 0.;
  auto register_distance = [&](const gp_Pnt2d& first_point, const gp_Pnt2d& last_point_of_previous_curve) {
    auto square = [](double x) { return x * x; };
    auto dist = std::sqrt(square(first_point.X() - last_point_of_previous_curve.X()) +
                          square(first_point.Y() - last_point_of_previous_curve.Y()));
    // std::cerr << "dist between endpoints: " << dist << '\n';
    maximal_distance_between_endpoints = std::max(maximal_distance_between_endpoints, dist);
  };
  bool first_curve = true;
  gp_Pnt2d last_point_of_previous_curve{};
  gp_Pnt2d first_point_of_first_curve{};
  for(TopoDS_Iterator edge_iterator(wire); edge_iterator.More(); edge_iterator.Next()) {
    auto edge = edge_iterator.Value();
    const bool is_edge_reversed = (TopAbs_REVERSED == edge.Orientation());
    assert(TopAbs_EDGE == edge.ShapeType());
    auto brep_tedge = Handle(BRep_TEdge)::DownCast(edge.TShape());
    auto curves = brep_tedge->Curves();
    for(auto curve_rep : curves) {
      if(!curve_rep->IsCurveOnSurface())
        continue;
      auto gcurve = Handle(BRep_CurveOnSurface)::DownCast(curve_rep);
      auto pcurve = curve_rep->PCurve();
      if(gcurve->Surface() != surface)
        continue;
      auto first = gcurve->First();
      auto last = gcurve->Last();
      if(is_edge_reversed) {
        if(curve_rep->IsCurveOnClosedSurface()) {
          pcurve = curve_rep->PCurve2();
          std::swap(first, last);
        } else {
          pcurve = pcurve->Reversed();
        }
      }
      auto first_point = pcurve->Value(first);
      auto last_point = pcurve->Value(last);
      if(first_curve) {
        first_point_of_first_curve = first_point;
        first_curve = false;
      } else {
        register_distance(first_point, last_point_of_previous_curve);
      }
      last_point_of_previous_curve = last_point;
      Geom2dAdaptor_Curve curve_adaptor{pcurve};
      GCPnts_UniformDeflection deflection_algorithm{curve_adaptor, 0.1, first, last};
      assert(deflection_algorithm.IsDone());
      const auto nb_points = deflection_algorithm.NbPoints();
      out << nb_points;
      for(Standard_Integer i = 1; i <= nb_points; ++i) {
        auto t = deflection_algorithm.Parameter(i);
        auto p_2d = pcurve->Value(t);
        out << " " << p_2d.X() << " " << p_2d.Y() << " " << 0;
      }
      out << '\n';
    }
  }
  register_distance(first_point_of_first_curve, last_point_of_previous_curve);
  std::cout << "## " << name << ":\n";
  std::cout << "Maximal distance between endpoints: " << maximal_distance_between_endpoints << '\n';
  assert(maximal_distance_between_endpoints < 1e-5);
}

int main(int, char**)
{
  std::cout.precision(17);
  std::cerr.precision(17);

  STEPControl_Reader reader;
  const auto error = reader.ReadFile("abc_0_31-mod.step");
  if(error != IFSelect_RetDone) {
    std::cerr << "File abc_0_31-mod.step is NOT loaded\n"
              << "\n Error code: " << static_cast<int>(error) << std::endl;
    return EXIT_FAILURE;
  }
  Standard_Integer NbTrans = reader.TransferRoots();
  assert(NbTrans == 1);
  assert(reader.NbShapes() == 1);

  TopoDS_Shape main_shape = reader.OneShape();
  dump_2d_pcurves_of_3rd_face("abc_0_31-mod.step", main_shape);

  BRepBuilderAPI_NurbsConvert nurbsconvert{main_shape};
  TopoDS_Shape nurbs = nurbsconvert.Shape();
  dump_2d_pcurves_of_3rd_face("abc_0_31-mod.step-nurbs", nurbs);
}
