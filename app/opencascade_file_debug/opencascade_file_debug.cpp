#include <BRepBuilderAPI_NurbsConvert.hxx>
#include "Standard_DomainError.hxx"
#include "Standard_TypeDef.hxx"
#include <cmath>
#include <cstddef>
#include <sstream>
#include <string>
#include <QFileInfo>

#include <STEPControl_Reader.hxx>
#include <IGESControl_Reader.hxx>
#include <TopoDS.hxx>
#include <TopoDS_Shape.hxx>
#include <TopoDS_Wire.hxx>
#include <TopoDS_Iterator.hxx>
#include <BRepTools.hxx>
#include <BRep_Tool.hxx>
// #include <BRepBuilderAPI_NurbsConvert.hxx>
#include <BRep_TFace.hxx>
#include <BRep_TEdge.hxx>
#include <BRep_TVertex.hxx>
#include <BRep_GCurve.hxx>
#include <Geom_BSplineSurface.hxx>
#include <Geom_BSplineCurve.hxx>
#include <Geom2d_BSplineCurve.hxx>
#include <Geom2d_Line.hxx>
#include <GeomTools_SurfaceSet.hxx>
#include <GeomTools_CurveSet.hxx>
#include <GeomTools_Curve2dSet.hxx>
#include <Geom_RectangularTrimmedSurface.hxx>
#include <Geom_CylindricalSurface.hxx>
#include <ShapeBuild_ReShape.hxx>



#include <functional>

auto main(int, char** argv) -> int {
    std::cout.precision(17);
    std::cerr.precision(17);

    STEPControl_Reader stepreader;
    IGESControl_Reader igesreader;
    XSControl_Reader* reader_ptr = nullptr;

    QString filename = argv[1];
    QFileInfo fileinfo{filename};
    auto extension = fileinfo.suffix().toLower();
    if(extension == "igs" || extension == "iges") {
        reader_ptr = &igesreader;
    } else if(extension == "stp" || extension == "step") {
        reader_ptr = &stepreader;
    } else {
        std::cerr << "Unknown file extension \"" << qPrintable(extension) << "\"\n";
        return EXIT_FAILURE;
    }
    auto& reader = *reader_ptr;

    switch(auto error = reader.ReadFile(filename.toStdString().c_str())) {
    case IFSelect_RetDone:
        std::clog << "File " << qPrintable(filename) << " is loaded" << std::endl;
        break;
    default:
        std::clog << "File " << qPrintable(filename) << " is NOT loaded\n"
                  << "\n  Error code: " << static_cast<int>(error) << std::endl;;
        return EXIT_FAILURE;
    }
    Standard_Integer NbRoots = reader.NbRootsForTransfer();

    // gets the number of transferable roots
    std::cout << "Number of roots in file: " << NbRoots << '\n';

    Standard_Integer NbTrans = reader.TransferRoots();
    // translates all transferable roots, and returns the number of    //successful translations
    std::cout << "Roots transferred: " << NbTrans << '\n';
    std::cout << "Number of resulting shapes is: " << reader.NbShapes() << '\n';


    TopoDS_Shape main_shape = reader.OneShape();
    GeomTools_SurfaceSet all_surfaces;
    GeomTools_CurveSet all_3dcurves;
    GeomTools_Curve2dSet all_2dcurves;
    TopTools_IndexedMapOfShape all_shapes;

    ShapeBuild_ReShape reshape{};

    auto add_all_shapes = [&]() {
        std::function<void (const TopoDS_Shape&)> add_shapes_rec = [&](const TopoDS_Shape& shape) {
            all_shapes.Add(shape);
            // std::cerr << "Shape #" << all_shapes.FindIndex(shape) << " " << (void*)&*shape.TShape() << '\n';

            switch(shape.ShapeType()) {
            case TopAbs_FACE: {
                auto tface = Handle(BRep_TFace)::DownCast(shape.TShape());
                assert(!tface->Surface().IsNull());
                all_surfaces.Add(tface->Surface());
                break;
            }
            case TopAbs_EDGE: {
                auto tedge = Handle(BRep_TEdge)::DownCast(shape.TShape());
                for (auto curve : tedge->Curves()) {
                  if (curve->IsCurve3D()) {
                      all_3dcurves.Add(curve->Curve3D());
                  } else if (curve->IsCurveOnClosedSurface()) {
                      all_2dcurves.Add(curve->PCurve());
                      all_2dcurves.Add(curve->PCurve2());
                  } else if (curve->IsCurveOnSurface()) {
                      all_2dcurves.Add(curve->PCurve());
                  } else if (curve->IsRegularity()) {
                    // all_surfaces.Add(curve->Surface());
                    // all_surfaces.Add(curve->Surface2());
                  }
                }
                break;
            }
            case TopAbs_VERTEX: {
                auto tvertex = Handle(BRep_TVertex)::DownCast(shape.TShape());
                for(auto point: tvertex->Points()) {
                    if (point->IsPointOnCurve()) {
                        all_3dcurves.Add(point->Curve());
                    }
                    else if (point->IsPointOnCurveOnSurface()) {
                        all_2dcurves.Add(point->PCurve());
                        // all_surfaces.Add(point->Surface());
                    }
                    else if (point->IsPointOnSurface()) {
                        // all_surfaces.Add(point->Surface());
                    }
                }
                break;
            }
            case TopAbs_COMPOUND:
            case TopAbs_COMPSOLID:
            case TopAbs_SOLID:
            case TopAbs_SHELL:
            case TopAbs_WIRE:
            case TopAbs_SHAPE:
                // no geometry to add
                break;
            } // end switch
            for (TopoDS_Iterator it(shape); it.More(); it.Next()) {
                add_shapes_rec(it.Value());
            }
        };
        add_shapes_rec(main_shape);
    };

    add_all_shapes();

    auto shape_type = [](const TopoDS_Shape &shape) {
      using namespace std::string_literals;
      auto type = shape.ShapeType();
      switch (type) {
      case TopAbs_COMPOUND:
        return "COMPOUND"s;
      case TopAbs_COMPSOLID:
        return "COMPSOLID"s;
      case TopAbs_SOLID:
        return "SOLID"s;
      case TopAbs_SHELL:
        return "SHELL"s;
      case TopAbs_FACE:
        return "FACE"s;
      case TopAbs_WIRE:
        return "WIRE"s;
      case TopAbs_EDGE:
        return "EDGE"s;
      case TopAbs_VERTEX:
        return "VERTEX"s;
      case TopAbs_SHAPE:
        return "SHAPE"s;
      }
      std::stringstream s;
      s << "UNKNOWN TYPE (" << static_cast<int>(type) << ")";
      return s.str();
    };
    auto display_geom_type = [&](int indentation, const TopoDS_Shape &shape) {
      auto type = shape.ShapeType();
      std::string result{};
      switch (type) {
      case TopAbs_FACE: {
        auto face = Handle(BRep_TFace)::DownCast(shape.TShape());
        auto surface = face->Surface();
        result = "(surf#" + std::to_string(all_surfaces.Index(surface));
        result = result + ") surface type: ";
        result = result + surface->DynamicType()->Name();
        std::stringstream s;
        s.precision(17);
        auto rect_trimmed_surface = Handle(Geom_RectangularTrimmedSurface)::DownCast(surface);
        if (rect_trimmed_surface) {
          s << "  ("
            << rect_trimmed_surface->BasisSurface()->DynamicType()->Name()
            << ")";
        }
        Standard_Real U1, U2, V1, V2;
        surface->Bounds(U1, U2, V1, V2);
        s << " [ " << U1 << ", " << U2 << " ] ×";
        s << " [ " << V1 << ", " << V2 << " ]";
        result = result + s.str();
        if (rect_trimmed_surface) {
          if(STANDARD_TYPE(Geom_CylindricalSurface) == rect_trimmed_surface->BasisSurface()->DynamicType()) {
            if(U1 < 0 && U1 >= -Epsilon(Abs(U2-U1))) {
              U1 = 0.;
              rect_trimmed_surface->SetTrim(U1, U2, Standard_True);
            }
            if((U2-U1) > 2*M_PI) {
              U2 = U1 + 2*M_PI;
              while((U2-U1) > 2*M_PI) {
                U2 = std::nextafter(U2, U1);
              }
              rect_trimmed_surface->SetTrim(U1, U2, Standard_True);
            }
            else if((U2-U1) < 0.) {
              U1 = U2 - 2*M_PI;
              while((U2-U1) < 0.) {
                U1 = std::nextafter(U1, U2);
              }
              rect_trimmed_surface->SetTrim(U1, U2, Standard_True);
            }
          }
        };
      } break;
      case TopAbs_EDGE: {
        auto display_pcurve_curve = [&](const Handle(Geom2d_Curve) & pcurve) {
          std::string result = "curve #" +
                               std::to_string(all_2dcurves.Index(pcurve)) + " " +
                               pcurve->DynamicType()->Name();
          auto geom2d_line = Handle(Geom2d_Line)::DownCast(pcurve);
          if (!geom2d_line.IsNull()) {
            std::stringstream output;
            output.precision(17);
            output << "=( p:(" << geom2d_line->Location().X() << ", "
                   << geom2d_line->Location().Y() << "), dir:("
                   << geom2d_line->Direction().X() << ", "
                   << geom2d_line->Direction().Y() << ") )";
            result = result + output.str();
          } else {
            auto nurbs_curve_2d = Handle(Geom2d_BSplineCurve)::DownCast(pcurve);
            if(!nurbs_curve_2d.IsNull()) {
              auto display_2d_curve_with_poles = [&](auto curve_2d,
                                                     std::string indent = "") {
                auto nb_poles = curve_2d->NbPoles();
                std::cerr << indent << "2d Bezier curve #"
                          << all_2dcurves.Index(curve_2d) << ", degree "
                          << curve_2d->Degree()
                          << ", periodic=" << curve_2d->IsPeriodic() << "\n";
                std::cerr << indent << nb_poles << " poles [";
                for (auto i = 1; i <= nb_poles; ++i /* i += (nb_poles - 1) */) {
                  auto p = curve_2d->Pole(i);
                  std::cerr << " (" << p.X() << ", " << p.Y()
                            << ",  w=" << curve_2d->Weight(i) << ")";
                }
                std::cerr << " ]\n";
              };
              auto display_2d_nurb = [&](auto nurbs_curve_2d,
                                         std::string indent = "") {
                display_2d_curve_with_poles(nurbs_curve_2d, indent);
                auto nb_knots = nurbs_curve_2d->NbKnots();
                std::cerr << indent << nb_knots << " knots [";
                for (auto i = 1; i <= nb_knots; ++i) {
                  std::cerr << " (" << nurbs_curve_2d->Knot(i) << ", "
                            << nurbs_curve_2d->Multiplicity(i) << ")";
                }
                std::cerr << " ]\n";
              };
              display_2d_nurb(nurbs_curve_2d, "    ");
            }
          }
          return result;
        };
        auto display_surface = [&](const Handle(Geom_Surface)& surf) {
          std::string result = "surf#" + std::to_string(all_surfaces.Index(surf));
          return result;
        };
        auto edge = Handle(BRep_TEdge)::DownCast(shape.TShape());
        for(auto c: edge->Curves()) {
          result = result + '\n' + std::string(indentation+2, ' ');
          if (c->IsCurve3D()) {
            result = result + "- 3d curve #" + std::to_string(all_3dcurves.Index(c->Curve3D()));
          } else {
            result = result + "- curve";
          }
          result = result + " " + c->DynamicType()->Name();
          auto gcurve = Handle(BRep_GCurve)::DownCast(c);
          if(!gcurve.IsNull()) {
              std::stringstream s;
              s.precision(17);
              s << " range("  << gcurve->First() << ", " << gcurve->Last() << ") ";
              result = result + s.str();
          }
          if (c->IsCurve3D()) {
            auto curve3d = c->Curve3D();
            result = result + "(" + (curve3d.IsNull() ? "NULL" : curve3d->DynamicType()->Name()) + ")";
          } else if (c->IsCurveOnClosedSurface()) {
            result = result + "(" + display_surface(c->Surface())+ ": ";
            auto pcurve = c->PCurve();
            result = result + display_pcurve_curve(pcurve) + " + ";
            pcurve = c->PCurve2();
            result = result + display_pcurve_curve(pcurve) + ")";
          } else if (c->IsCurveOnSurface()) {
            auto pcurve = c->PCurve();
            result = result + "(" + display_surface(c->Surface()) + ": " + display_pcurve_curve(pcurve) + ")";
          } else if (c->IsRegularity()) {
            result = result + "(" + display_surface(c->Surface()) + "/" + display_surface(c->Surface2()) + ")";
          }
        }
      } break;
      case TopAbs_VERTEX: {
        auto vertex = Handle(BRep_TVertex)::DownCast(shape.TShape());
        auto pt_xyz = vertex->Pnt().XYZ();
        std::stringstream output;
        output.precision(17);
        output << "(" << pt_xyz.X() << ", " << pt_xyz.Y() << ", " << pt_xyz.Z()<< ")";
        result = output.str();
      } break;
      case TopAbs_COMPOUND:
      case TopAbs_COMPSOLID:
      case TopAbs_SOLID:
      case TopAbs_SHELL:
      case TopAbs_WIRE:
      case TopAbs_SHAPE:
        break;
      }
      return result;
    };
    std::function<void(const TopoDS_Shape &, int)> visit =
        [&](const TopoDS_Shape &shape, int indent = 0) {
            std::cerr << indent << std::string(indent*2-1, ' ') << shape_type(shape);
            if (shape.Orientation() != TopAbs_FORWARD)
                std::cerr << "[-]";
            std::cerr << " #" << all_shapes.FindIndex(shape)
                      << " (" << (void*) &(*shape.TShape()) <<")";
            auto geom_type = display_geom_type(indent*2, shape);
            if(!geom_type.empty()) {
                std::cerr << "  " << geom_type;
                try {
                  BRepBuilderAPI_NurbsConvert nurbsconvert{shape};
                  TopoDS_Shape converted = nurbsconvert.Shape();
                } catch (Standard_DomainError &) {
                  std::cerr << "  ERROR!";
                }
            }
            std::cerr << '\n';
            for (TopoDS_Iterator iterator(shape); iterator.More();
                 iterator.Next()) {
                visit(iterator.Value(), indent + 1);
            }
        };
    BRepTools::Dump(main_shape, std::cerr);
    std::cerr << "Start the description of the content of the file...\n";
    visit(main_shape, 1);
    BRepBuilderAPI_NurbsConvert nurbsconvert{main_shape};
    (void)nurbsconvert.Shape();
}

// Local Variables:
// c-basic-offset: 4
// End:
