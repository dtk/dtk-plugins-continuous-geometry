// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <dtkRationalBezierCurve2DElevator>

#include <defaultRationalBezierCurve2DElevatorExport.h>

#include <dtkContinuousGeometry>

class dtkRationalBezierCurve2D;
class dtkRationalBezierSurface;
class dtkRationalBezierCurve;
class defaultRationalBezierCurve2DElevatorPrivate;

class DEFAULTRATIONALBEZIERCURVE2DELEVATOR_EXPORT defaultRationalBezierCurve2DElevator : public dtkRationalBezierCurve2DElevator
{
 public:
    defaultRationalBezierCurve2DElevator(void);
    virtual ~defaultRationalBezierCurve2DElevator(void);

 public:
    void setInputRationalBezierCurve2D(dtkRationalBezierCurve2D *) final;
    void setInputRationalBezierSurface(dtkRationalBezierSurface *) final;

 public:
    void run() final;

    dtkRationalBezierCurve *rationalBezierCurve(void) final;

 protected:
    defaultRationalBezierCurve2DElevatorPrivate *d;
};


// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

inline dtkRationalBezierCurve2DElevator *defaultRationalBezierCurve2DElevatorCreator(void)
{
    return new defaultRationalBezierCurve2DElevator();
}

//
// defaultRationalBezierCurve2DElevator.h ends here
