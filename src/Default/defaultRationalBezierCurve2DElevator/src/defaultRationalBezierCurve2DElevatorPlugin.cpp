// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "defaultRationalBezierCurve2DElevator.h"
#include "defaultRationalBezierCurve2DElevatorPlugin.h"

#include "dtkContinuousGeometry.h"

#include <dtkCore>

// ///////////////////////////////////////////////////////////////////
// defaultRationalBezierCurve2DElevatorPlugin
// ///////////////////////////////////////////////////////////////////

void defaultRationalBezierCurve2DElevatorPlugin::initialize(void)
{
    dtkContinuousGeometry::rationalBezierCurve2DElevator::pluginFactory().record("defaultRationalBezierCurve2DElevator", defaultRationalBezierCurve2DElevatorCreator);
}

void defaultRationalBezierCurve2DElevatorPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(defaultRationalBezierCurve2DElevator)

//
// defaultRationalBezierCurve2DElevatorPlugin.cpp ends here
