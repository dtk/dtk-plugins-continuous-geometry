// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkRationalBezierCurve2DElevator.h>

#include <defaultRationalBezierCurve2DElevatorExport.h>

#include <dtkCore>

class DEFAULTRATIONALBEZIERCURVE2DELEVATOR_EXPORT defaultRationalBezierCurve2DElevatorPlugin : public dtkRationalBezierCurve2DElevatorPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkRationalBezierCurve2DElevatorPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.defaultRationalBezierCurve2DElevatorPlugin" FILE "defaultRationalBezierCurve2DElevatorPlugin.json")

public:
     defaultRationalBezierCurve2DElevatorPlugin(void) {}
    ~defaultRationalBezierCurve2DElevatorPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

//
// defaultRationalBezierCurve2DElevatorPlugin.h ends here
