// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "defaultRationalBezierCurve2DElevator.h"

#include <dtkContinuousGeometry.h>

#include <dtkContinuousGeometryUtils>

#include <dtkRationalBezierCurve2D>
#include <dtkRationalBezierCurve>
#include <dtkRationalBezierSurface>

#include <boost/container/small_vector.hpp>
#include <cstdint>

// /////////////////////////////////////////////////////////////////
// defaultRationalBezierCurve2DElevatorPrivate
// /////////////////////////////////////////////////////////////////

class defaultRationalBezierCurve2DElevatorPrivate
{
    template< typename K > using dtkMatrixMap = dtkContinuousGeometryTools::dtkMatrixMap< K >;
public:
    void run(void);
    static void filterPartitions(std::size_t N, std::size_t l, std::size_t m,  std::list< std::vector< std::uint_least16_t > >& r_partitions);

public:
    dtkRationalBezierCurve2D *rational_bezier_curve_2d;
    dtkRationalBezierSurface *rational_bezier_surface;
    dtkRationalBezierCurve *rational_bezier_curve_3d;
};

/**/
void defaultRationalBezierCurve2DElevatorPrivate::filterPartitions(std::size_t N, std::size_t l, std::size_t m,  std::list< std::vector< std::uint_least16_t> >& r_partitions) {
    // ///////////////////////////////////////////////////////////////////
    // Filters all the partitions with number of elements higher than l + m
    // because there is no way to have sum(elements) = R in l + m positions
    // ///////////////////////////////////////////////////////////////////
    for(auto partition = r_partitions.begin(); partition != r_partitions.end();) {
        if(partition->size() > l + m) {
            partition = r_partitions.erase(partition);
        } else {
            ++partition;
        }
    }
    // ///////////////////////////////////////////////////////////////////
    // Filters all the partitions with element higher than N
    // ///////////////////////////////////////////////////////////////////
    bool erased = false;
    for(auto partition = r_partitions.begin(); partition != r_partitions.end();) {
        erased = false;
        for(std::size_t i = 0; i < partition->size(); ++i) {
            if((*partition)[i] > N) {
                partition = r_partitions.erase(partition);
                erased = true;
                break;
            }
        }
        if(!erased) {
            ++partition;
        }
    }
}

void defaultRationalBezierCurve2DElevatorPrivate::run(void)
{
    // ///////////////////////////////////////////////////////////////////
    // Checks that all inputs are set
    // ///////////////////////////////////////////////////////////////////
    if (rational_bezier_curve_2d == nullptr || rational_bezier_surface == nullptr) {
        dtkFatal() << "Not all inputs of defaultRationalBezierCurve2DElevator were set";
    }

    std::size_t l = rational_bezier_surface->uDegree();
    std::size_t m = rational_bezier_surface->vDegree();
    std::size_t N = rational_bezier_curve_2d->degree();
    dtkMatrixMap< double > binomial_coefficients;
    dtkContinuousGeometryTools::computeBinomialCoefficients(binomial_coefficients, N * (l + m), N * (l + m));

    // ///////////////////////////////////////////////////////////////////
    // Creates the storage for the new 3D curve
    // ///////////////////////////////////////////////////////////////////
    dtkAbstractRationalBezierCurveData *rational_bezier_curve_data = dtkContinuousGeometry::abstractRationalBezierCurveData::pluginFactory().create("dtkRationalBezierCurveDataOn");
    if(rational_bezier_curve_data == nullptr) {
        dtkFatal() << "To use " << Q_FUNC_INFO << " , dtkAbstractRationalBezierCurveData must be compiled under the openNURBS plugins";
    }
    std::vector<double> cps((3 + 1) * (N * (l + m) + 1));

    for (std::size_t R = 0; R <= N * (l + m); ++R) {
        // ///////////////////////////////////////////////////////////////////
        // R partitionning (computes the decomposition of R)
        // ///////////////////////////////////////////////////////////////////
        auto r_partitions = [R, N, l, m]() {
            std::list< std::vector< std::uint_least16_t > > r_partitions;
            dtkContinuousGeometryTools::integerPartitioning(R, r_partitions);
            filterPartitions(N, l, m, r_partitions);
            // ///////////////////////////////////////////////////////////////////
            // Fills the partitions with 0 up to l + m elements
            // It doesnt change that sum(elements) = R
            // ///////////////////////////////////////////////////////////////////
            for(auto partition = r_partitions.begin(); partition != r_partitions.end(); ++partition) {
                for(std::size_t i = partition->size(); i < l + m; ++i) {
                    (*partition).push_back(0);
                }
                std::sort(partition->begin(), partition->end());
            }
            return r_partitions;
        }();
        double omega_r = 0.;
        dtkContinuousGeometryPrimitives::Point_3 b_r(0., 0., 0.);
        double w_00 = 0.;
        dtkContinuousGeometryPrimitives::Point_3 f_00(0., 0., 0.);
        dtkContinuousGeometryPrimitives::Point_2 b_0(0., 0.);
        for(auto partition = r_partitions.begin(); partition != r_partitions.end(); ++partition) {
            do {
                boost::container::small_vector< double, 16 > uis(partition->begin(), partition->begin() + l);
                boost::container::small_vector< double, 16 > vis(partition->begin() + l, partition->end());
                // ///////////////////////////////////////////////////////////////////
                // Computes the us and vs
                // ///////////////////////////////////////////////////////////////////
                boost::container::small_vector< double, 16 > us(uis.size());
                boost::container::small_vector< double, 16 > vs(vis.size());
                for(std::size_t i = 0; i < uis.size(); ++i) {
                    rational_bezier_curve_2d->controlPoint(uis[i], b_0.data());
                    us[i] = b_0[0];
                }
                for(std::size_t i = 0; i < vis.size(); ++i) {
                    rational_bezier_curve_2d->controlPoint(vis[i], b_0.data());
                    vs[i] = b_0[1];
                }
                rational_bezier_surface->evaluatePointBlossom(us.data(), vs.data(), f_00.data(), &w_00);
                double br_u = 1.;
                double br_v = 1.;
                double w = 0;
                // ///////////////////////////////////////////////////////////////////
                // Computes Product(Qu = 1->l)[Beta_(I^u_Qu)]
                // ///////////////////////////////////////////////////////////////////
                for(std::size_t i = 0; i < uis.size(); ++i) {
                    rational_bezier_curve_2d->weight(uis[i], &w);
                    br_u *=  w * binomial_coefficients[{rational_bezier_curve_2d->degree(), uis[i]}];
                }
                // ///////////////////////////////////////////////////////////////////
                // Computes Product(Qv = 1->m)[Beta_(I^v_Qv)]
                // ///////////////////////////////////////////////////////////////////
                for(std::size_t i = 0; i < vis.size(); ++i) {
                    rational_bezier_curve_2d->weight(vis[i], &w);
                    br_v *=  w * binomial_coefficients[{rational_bezier_curve_2d->degree(), vis[i]}];
                }
                double br = br_u * br_v / binomial_coefficients[{N * (l + m), R}];
                omega_r += w_00 * br;
                b_r = b_r + f_00 * w_00 * br;
            } while (std::next_permutation(partition->begin(), partition->end()));
        }
        cps[R * 4] = b_r[0] / omega_r; //x
        cps[R * 4 + 1] = b_r[1] / omega_r; //y
        cps[R * 4 + 2] = b_r[2] / omega_r; //z
        cps[R * 4 + 3] = omega_r; //w
    }
    rational_bezier_curve_3d = new dtkRationalBezierCurve(rational_bezier_curve_data);
    rational_bezier_curve_data->create( N * (l + m) + 1, cps.data());
}

// ///////////////////////////////////////////////////////////////////
// defaultRationalBezierCurve2DElevator implementation
// ///////////////////////////////////////////////////////////////////

defaultRationalBezierCurve2DElevator::defaultRationalBezierCurve2DElevator(void) : d(new defaultRationalBezierCurve2DElevatorPrivate)
{
    d->rational_bezier_curve_2d = nullptr;
    d->rational_bezier_surface = nullptr;
    d->rational_bezier_curve_3d = nullptr;
}

defaultRationalBezierCurve2DElevator::~defaultRationalBezierCurve2DElevator(void)
{
    delete d;
    d = nullptr;
}

void defaultRationalBezierCurve2DElevator::setInputRationalBezierCurve2D(dtkRationalBezierCurve2D *rational_bezier_curve_2d)
{
    d->rational_bezier_curve_2d = rational_bezier_curve_2d;
    d->rational_bezier_curve_3d = nullptr;
}

void defaultRationalBezierCurve2DElevator::setInputRationalBezierSurface(dtkRationalBezierSurface *rational_bezier_surface)
{
    d->rational_bezier_surface = rational_bezier_surface;
    d->rational_bezier_curve_3d = nullptr;
}

void defaultRationalBezierCurve2DElevator::run(void)
{
    d->run();
}

dtkRationalBezierCurve *defaultRationalBezierCurve2DElevator::rationalBezierCurve(void)
{
    if (d->rational_bezier_curve_3d == nullptr) {
        dtkInfo() << "The run method has probably not been called, or there was an issue computing the embedded bezier curve";
    }
    return d->rational_bezier_curve_3d;
}


//
// defaultRationalBezierCurve2DElevator.cpp ends here

// Local Variables:
// c-basic-offset: 4
// End:
