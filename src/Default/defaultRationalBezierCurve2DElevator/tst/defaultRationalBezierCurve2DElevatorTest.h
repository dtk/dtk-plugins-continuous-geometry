// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <QTest>

class defaultRationalBezierCurve2DElevatorTestCasePrivate;

class defaultRationalBezierCurve2DElevatorTestCase : public QObject
{
    Q_OBJECT

public:
    defaultRationalBezierCurve2DElevatorTestCase(void);
    ~defaultRationalBezierCurve2DElevatorTestCase(void);

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testRun(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);

private:
    defaultRationalBezierCurve2DElevatorTestCasePrivate* d;
};

//
// defaultRationalBezierCurve2DElevatorTest.h ends here
