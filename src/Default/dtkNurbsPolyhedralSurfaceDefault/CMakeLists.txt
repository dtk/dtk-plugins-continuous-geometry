## Version: $Id$
##
######################################################################
##
### Commentary:
##
######################################################################
##
### Change Log:
##
######################################################################
##
### Code:

project(dtkNurbsPolyhedralSurfaceDefault)
if(NOT TARGET dtkDiscreteGeometryCore)
  find_package(dtkDiscreteGeometry 0.0.1 QUIET)
endif()
if (dtkDiscreteGeometry_FOUND)
  include_directories(${dtkDiscreteGeometry_INCLUDE_DIRS})

  include_directories(src)

  ## #################################################################
  ## Inputs
  ## #################################################################

  add_subdirectory(src)
#  add_subdirectory(tst)
endif (dtkDiscreteGeometry_FOUND)
######################################################################
### CMakeLists.txt ends here
