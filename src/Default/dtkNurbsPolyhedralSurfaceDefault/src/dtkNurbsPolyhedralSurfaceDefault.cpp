// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkNurbsPolyhedralSurfaceDefault.h"

#include <dtkContinuousGeometryUtils>
#include <dtkNurbsSurface>
#include <dtkNurbsCurve2D>
#include <dtkTrim>
#include <dtkTrimLoop>
#include <dtkMesh>
#include <dtkMeshDataDefault>
#include <fstream>
#include <dtkDiscreteGeometryCore>

/*!
enpass  \class dtkNurbsPolyhedralSurfaceDefault
  \inmodule dtkPluginsContinuousGeometry
  \brief dtkNurbsPolyhedralSurfaceDefault is a default implementation of the concept dtkNurbsPolyhedarlSurface.

  The following line of code shows how to instanciate the class in pratice :
  \code
  dtkNurbsPolyhedralSurface *polyhedral_surface = dtkContinuousGeometry::nurbsPolyhedralSurface::pluginFactory().create("dtkNurbsPolyhedralSurfaceDefault");
  \endcode
*/

/*! \fn dtkNurbsPolyhedralSurfaceDefault::dtkNurbsPolyhedralSurfaceDefault(void)
  Instanciates.

  See \l initialize(dtkNurbsSurface *nurbs_surface) for the initialization.
*/
dtkNurbsPolyhedralSurfaceDefault::dtkNurbsPolyhedralSurfaceDefault() : m_nb_vertices(0), m_untrimmed_elements(nullptr), m_u_sampling(0), m_v_sampling(0), m_us(nullptr), m_vs(nullptr) {}

/*! \fn dtkNurbsPolyhedralSurfaceDefault::~dtkNurbsPolyhedralSurfaceDefault(void)
  Destructor.
*/
dtkNurbsPolyhedralSurfaceDefault::~dtkNurbsPolyhedralSurfaceDefault(void)
{
    delete[] m_untrimmed_elements;
    delete[] m_us;
    delete[] m_vs;
}

/*! \fn  dtkNurbsPolyhedralSurfaceDefault::initialize(dtkNurbsSurface *p_nurbs_surface, double approximation)
  Initializes the dtkNurbsPolyhedralSurfaceDefault with a NURBS surface (\a p_nurbs_surface).

  \a nurbs_surface : the NURBS surface to polyhedralize

  \a approximation : the tolerated approximation from the NURBS surface to its polyhedralization
*/
void dtkNurbsPolyhedralSurfaceDefault::initialize(dtkNurbsSurface *nurbs_surface, double approximation, int)
{
    dtkDebug() << "Initialization...";
    dtkNurbsPolyhedralSurface::initialize(nurbs_surface, approximation);
    dtkDebug() << "Initialized";

    // ///////////////////////////////////////////////////////////////////
    // Sampling corresponds to the number of triangles edges along a NURBS edge
    // ///////////////////////////////////////////////////////////////////
    m_u_sampling = 100;
    m_v_sampling = 100;

    m_nb_vertices = m_u_sampling * m_v_sampling;

    // ///////////////////////////////////////////////////////////////////
    // Builds up the array of 2d vertices positions by sampling the parameter space of the surface
    // ///////////////////////////////////////////////////////////////////
    m_us = new double[m_nb_vertices];
    m_vs = new double[m_nb_vertices];
    std::vector<double> u_knots(m_nurbs_surface->uNbCps() + m_nurbs_surface->uDegree() - 1);
    m_nurbs_surface->uKnots(u_knots.data());
    std::vector<double> v_knots(m_nurbs_surface->vNbCps() + m_nurbs_surface->vDegree() - 1);
    m_nurbs_surface->vKnots(v_knots.data());

    double u_span = (u_knots[m_nurbs_surface->uNbCps() + m_nurbs_surface->uDegree() - 2] - u_knots[0]) / (double(m_u_sampling) - 1);
    double v_span = (v_knots[m_nurbs_surface->vNbCps() + m_nurbs_surface->vDegree() - 2] - v_knots[0]) / (double(m_v_sampling) - 1);
    for (std::size_t i = 0.; i < m_u_sampling; ++i) {
        for (std::size_t j = 0.; j < m_v_sampling; ++j) {
            m_us[j + i * m_v_sampling] = u_knots[0] + double(i) * u_span;
            m_vs[j + i * m_v_sampling] = v_knots[0] + double(j) * v_span;
        }
    }

    std::size_t nb_untrimmed_elements = (m_u_sampling - 1) * (m_v_sampling - 1) * 2;
    m_untrimmed_elements = new std::size_t[nb_untrimmed_elements * 3];
    std::size_t cntr = 0;
    for (std::size_t i = 0; i < (m_u_sampling - 1); ++i) {
        for (std::size_t j = 0; j < (m_v_sampling - 1); ++j, cntr+=6) {
            m_untrimmed_elements[cntr + 0] = i * m_v_sampling + j;
            m_untrimmed_elements[cntr + 1] = i * m_v_sampling + j + 1;
            m_untrimmed_elements[cntr + 2] = i * m_v_sampling + j + m_v_sampling + 1 ;

            m_untrimmed_elements[cntr + 3] = i * m_v_sampling + j;
            m_untrimmed_elements[cntr + 4] = i * m_v_sampling + j + m_v_sampling + 1;
            m_untrimmed_elements[cntr + 5] = i * m_v_sampling + j + m_v_sampling ;
        }
    }

    // ///////////////////////////////////////////////////////////////////
    // Filter elements
    // ///////////////////////////////////////////////////////////////////
    dtkContinuousGeometryPrimitives::Point_2 p(0., 0.);
    for(std::size_t i = 0; i < (m_u_sampling - 1) * (m_u_sampling - 1) * 2 * 3; i += 3) {
        p[0] = (m_us[m_untrimmed_elements[i]] + m_us[m_untrimmed_elements[i + 1]] + m_us[m_untrimmed_elements[i + 2]]) / 3.;
        p[1] = (m_vs[m_untrimmed_elements[i]] + m_vs[m_untrimmed_elements[i + 1]] + m_vs[m_untrimmed_elements[i + 2]]) / 3.;
        if(!m_nurbs_surface->isPointCulled(p)) {
            std::cerr << "Point is not culled" << std::endl;
            m_trimmed_elements.push_back(m_untrimmed_elements[i    ]);
            m_trimmed_elements.push_back(m_untrimmed_elements[i + 1]);
            m_trimmed_elements.push_back(m_untrimmed_elements[i + 2]);
        }
    }

    // dtkMeshData *mesh_data = dtkDiscreteGeometryCore::meshData::pluginFactory().create("dtkMeshDataDefault");
    // this->m_polyhedral_surface = new dtkMesh(mesh_data);
    // dtkDebug() << "Converting completed";
}

/*! \fn dtkNurbsPolyhedralSurfaceDefault::isPointCulled(const dtkContinuousGeometryPrimitives::Point_2& point) const
  Returns true if \a point is culled by the trim loops of the surface, else returns false.
*/
bool dtkNurbsPolyhedralSurfaceDefault::isPointCulled(const dtkContinuousGeometryPrimitives::Point_2& point) const
{
    return m_nurbs_surface->isPointCulled(point);
}

/*! \fn dtkNurbsPolyhedralSurfaceDefault::evaluatePoint(double p_u, double p_v, double *r_point) const
  Writes in \a r_point the evaluation at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_point : array of size 3 to store the result of the evaluation
*/
void dtkNurbsPolyhedralSurfaceDefault::evaluatePoint(double p_u, double p_v, double* r_point) const
{
    return m_nurbs_surface->evaluatePoint(p_u, p_v, r_point);
}

/*! \fn dtkNurbsPolyhedralSurfaceDefault::evaluateNormal(double p_u, double p_v, double *r_normal) const
  Writes in \a r_normal the normal evaluation at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_normal : array of size 3 to store the result of the normal evaluation
*/
void dtkNurbsPolyhedralSurfaceDefault::evaluateNormal(double p_u, double p_v, double* r_normal) const
{
    return m_nurbs_surface->evaluateNormal(p_u, p_v, r_normal);
}

/*! \fn dtkNurbsPolyhedralSurfaceDefault::evaluate1stDer(double p_u, double p_v, double *r_point, double *r_u_deriv, double *r_v_deriv) const
  Writes in \a r_point the evaluation, in \a r_u_deriv the first derivative in "u" direction, in \a r_v_deriv the first derivative in the "v" direction at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_point : array of size 3 to store the result of the evaluation

  \a r_u_deriv : array of size 3 to store the result of the first derivative evaluation in the "u" direction

  \a r_v_deriv : array of size 3 to store the result of the first derivative evaluation in the "v" direction
*/
void dtkNurbsPolyhedralSurfaceDefault::evaluate1stDer(double p_u, double p_v, double* r_point, double* r_u_deriv, double* r_v_deriv) const
{
    return m_nurbs_surface->evaluate1stDer(p_u, p_v, r_point, r_u_deriv, r_v_deriv);
}

/*! \fn dtkNurbsPolyhedralSurfaceDefault::evaluate1stDer(double p_u, double p_v, double *r_u_deriv, double *r_v_deriv) const
  Writes in \a r_u_deriv the first derivative in "u" direction and in \a r_v_deriv the first derivative in the "v" direction at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_u_deriv : array of size 3 to store the result of the first derivative evaluation in the "u" direction

  \a r_v_deriv : array of size 3 to store the result of the first derivative evaluation in the "v" direction
*/
void dtkNurbsPolyhedralSurfaceDefault::evaluate1stDer(double p_u, double p_v, double* r_u_deriv, double* r_v_deriv) const
{
    return m_nurbs_surface->evaluate1stDer(p_u, p_v, r_u_deriv, r_v_deriv);
}

/*! \fn dtkNurbsPolyhedralSurfaceDefault::pointsAndTriangles(std::vector< dtkContinuousGeometryPrimitives::Point_3 >& r_points, std::vector< std::size_t >& r_triangles) const
  Returns a triangulation approximating the NURBS surface as points and triangles.

  \a r_points : the geometrical points of the triangulation

  \a r_triangles : the triangles of the triangulation
*/
void dtkNurbsPolyhedralSurfaceDefault::pointsAndTriangles(std::vector< dtkContinuousGeometryPrimitives::Point_3 >& r_points, std::vector< std::size_t >& r_triangles) const
{
    // ///////////////////////////////////////////////////////////////
    std::ofstream trimmed_mesh_file("trimmed_mesh_file.off");
    trimmed_mesh_file << "OFF" << std::endl;
    trimmed_mesh_file << m_u_sampling * m_v_sampling << " " << m_trimmed_elements.size() / 3 << " 0" << std::endl;
    dtkContinuousGeometryPrimitives::Point_3 p(0., 0., 0.);
    for(std::size_t i = 0; i < m_nb_vertices; ++i) {
        m_nurbs_surface->evaluatePoint(m_us[i], m_vs[i], p.data());
        trimmed_mesh_file << p << std::endl;
    }
    for(std::size_t i = 0; i < m_trimmed_elements.size(); i += 3) {
        trimmed_mesh_file << "3 " << m_trimmed_elements[i] << " " << m_trimmed_elements[i + 1] << " " << m_trimmed_elements[i + 2]  << std::endl;
    }
    r_points.clear();
    r_triangles.clear();
    for (std::size_t i = 0; i < m_nb_vertices; ++i) {
        m_nurbs_surface->evaluatePoint(m_us[i], m_vs[i], p.data());
        r_points.push_back(p); //order is thus the same in r_points as in m_us and m_vs
    }
    for (std::size_t i = 0; i < m_trimmed_elements.size(); ++i) {
        r_triangles.push_back(m_trimmed_elements[i]);
    }
}

void dtkNurbsPolyhedralSurfaceDefault::pointsTrianglesAndNormals(std::vector< dtkContinuousGeometryPrimitives::Point_3 >&, std::vector< std::size_t >&, std::vector< dtkContinuousGeometryPrimitives::Vector_3 >&) const
{
    dtkWarn() << Q_FUNC_INFO;
}
/*! \fn dtkNurbsPolyhedralSurfaceDefault::aabb(double* r_aabb) const
  Writes in \a r_aabb the [xmin, ymin, zmin, xmax, ymax, zmax] coordinates.

  \a r_aabb : array of size 6 to store the limits coordinates
*/
void dtkNurbsPolyhedralSurfaceDefault::aabb(double *r_aabb) const
{
    //TODO change for the mesh bounding box
    return m_nurbs_surface->aabb(r_aabb);
}

/*! \fn dtkNurbsPolyhedralSurfaceDefault::extendedAabb(double *r_aabb, double factor) const
  Writes in \a r_aabb the [xmin, ymin, zmin, xmax, ymax, zmax] coordinates of an extened bounding box of factor \a factor. The extension factor extends the diagonal.

  \a r_aabb : array of size 6 to store the extended limits coordinates

  \a factor : factor used to lengthen the diagonal of the axis aligned bounding box
*/
void dtkNurbsPolyhedralSurfaceDefault::extendedAabb(double *r_aabb, double factor) const
{
    return m_nurbs_surface->extendedAabb(r_aabb, factor);
}

/*! \fn dtkNurbsPolyhedralSurfaceDefault::clone(void) const
  Clone
*/
dtkNurbsPolyhedralSurfaceDefault* dtkNurbsPolyhedralSurfaceDefault::clone(void) const
{
    return new dtkNurbsPolyhedralSurfaceDefault(*this);
}

dtkNurbsPolyhedralSurfaceDefault::Trim_polygon_type dtkNurbsPolyhedralSurfaceDefault::trim_polygon_2d() const
{
	return dtkNurbsPolyhedralSurfaceDefault::Trim_polygon_type();
}

//
// dtkNurbsPolyhedralSurfaceDefault.cpp ends here
