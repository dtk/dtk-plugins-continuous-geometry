// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#include "dtkNurbsPolyhedralSurfaceDefault.h"
#include "dtkNurbsPolyhedralSurfaceDefaultPlugin.h"

#include <dtkCore>
#include <dtkContinuousGeometry>

// ///////////////////////////////////////////////////////////////////
// dtkNurbsPolyhedralSurfaceDefaultPlugin
// ///////////////////////////////////////////////////////////////////

void dtkNurbsPolyhedralSurfaceDefaultPlugin::initialize(void)
{
    dtkContinuousGeometry::nurbsPolyhedralSurface::pluginFactory().record("dtkNurbsPolyhedralSurfaceDefault", dtkNurbsPolyhedralSurfaceDefaultCreator);
}

void dtkNurbsPolyhedralSurfaceDefaultPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkNurbsPolyhedralSurfaceDefault)

//
// dtkNurbsPolyhedralSurfaceDefaultPlugin.cpp ends here
