// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <dtkNurbsPolyhedralSurfaceDefaultExport.h>

#include <dtkContinuousGeometry>

#include <dtkContinuousGeometryUtils>

#include <dtkNurbsPolyhedralSurface>

#include <map>

class DTKNURBSPOLYHEDRALSURFACEDEFAULT_EXPORT dtkNurbsPolyhedralSurfaceDefault final : public dtkNurbsPolyhedralSurface
{
private:
  typedef typename std::list< std::pair< dtkContinuousGeometryEnums::TrimLoopType, std::vector< dtkContinuousGeometryPrimitives::Point_2 > > > Trim_polygon_type;

 public:
    dtkNurbsPolyhedralSurfaceDefault(void);
    virtual ~dtkNurbsPolyhedralSurfaceDefault(void) final;

    void initialize(dtkNurbsSurface *nurbs_surface, double approximation, int surface_number) override;

 public:
    bool isPointCulled(const dtkContinuousGeometryPrimitives::Point_2& point) const override;

 public:
    void evaluatePoint(double p_u, double p_v, double* r_point) const override;
    void evaluateNormal(double p_u, double p_v, double* r_normal) const override;
    void evaluate1stDer(double p_u, double p_v, double* r_point, double* r_u_deriv, double* r_v_deriv) const override;
    void evaluate1stDer(double p_u, double p_v, double* r_u_deriv, double* r_v_deriv) const override;

 public:
    void aabb(double *r_aabb) const override;
    void extendedAabb(double *r_aabb, double factor) const override;

 public:
    void pointsAndTriangles(std::vector< dtkContinuousGeometryPrimitives::Point_3 >& r_points, std::vector< std::size_t >& r_triangles) const override;
    void pointsTrianglesAndNormals(std::vector< dtkContinuousGeometryPrimitives::Point_3 >& r_points, std::vector< std::size_t >& r_triangles, std::vector< dtkContinuousGeometryPrimitives::Vector_3 >& r_normals) const override;
 public:
    dtkNurbsPolyhedralSurfaceDefault* clone(void) const override;

 public:
    Trim_polygon_type trim_polygon_2d() const override;

 private:
    std::size_t m_nb_vertices;
    std::vector< std::size_t > m_trimmed_elements;
    std::size_t *m_untrimmed_elements;
    std::size_t m_u_sampling;
    std::size_t m_v_sampling;
    double *m_us;
    double *m_vs;
};


// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

inline dtkNurbsPolyhedralSurface *dtkNurbsPolyhedralSurfaceDefaultCreator()
{
    return new dtkNurbsPolyhedralSurfaceDefault();
}

//
// dtkNurbsPolyhedralSurfaceDefault.h ends here
