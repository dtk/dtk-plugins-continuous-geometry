// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <QtCore>

#include <dtkContinuousGeometry>

#include <dtkNurbsPolyhedralSurface.h>

class dtkNurbsPolyhedralSurfaceDefaultPlugin : public dtkNurbsPolyhedralSurfacePlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkNurbsPolyhedralSurfacePlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkNurbsPolyhedralSurfaceDefaultPlugin" FILE "dtkNurbsPolyhedralSurfaceDefaultPlugin.json")

public:
     dtkNurbsPolyhedralSurfaceDefaultPlugin(void) {}
    ~dtkNurbsPolyhedralSurfaceDefaultPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

//
// dtkNurbsPolyhedralSurfaceDefaultPlugin.h ends here
