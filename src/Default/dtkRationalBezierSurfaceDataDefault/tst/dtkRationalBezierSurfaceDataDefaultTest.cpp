// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkRationalBezierSurfaceDataDefaultTest.h"

#include "dtkRationalBezierSurfaceDataDefault.h"

// ///////////////////////////////////////////////////////////////////
// D-Pointer implementation
// ///////////////////////////////////////////////////////////////////

class dtkRationalBezierSurfaceDataDefaultTestCasePrivate{
public:
    dtkRationalBezierSurfaceDataDefault* rational_bezier_surface_data;
};
// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkRationalBezierSurfaceDataDefaultTestCase::dtkRationalBezierSurfaceDataDefaultTestCase(void):d(new dtkRationalBezierSurfaceDataDefaultTestCasePrivate())
{

}

dtkRationalBezierSurfaceDataDefaultTestCase::~dtkRationalBezierSurfaceDataDefaultTestCase(void)
{
}

void dtkRationalBezierSurfaceDataDefaultTestCase::initTestCase(void)
{
    d->rational_bezier_surface_data = new dtkRationalBezierSurfaceDataDefault();
    //bi-degree 1,2
    std::size_t dim = 3;
    std::size_t nb_cp_u = 2;
    std::size_t nb_cp_v = 3;
    /* memory leak */
    double* cps = new double[(dim + 1) * nb_cp_u * nb_cp_v];
    cps[0] = 0.;  cps[1] = 0.;  cps[2] = 1.;  cps[3] = 2.;
    cps[4] = 0.;  cps[5] = 1.;  cps[6] = 1.;  cps[7] = 2.;
    cps[8] = 0.;  cps[9] = 2.;  cps[10] = 1.; cps[11] = 2.;
    cps[12] = 1.; cps[13] = 0.; cps[14] = 1.; cps[15] = 2.;
    cps[16] = 1.; cps[17] = 1.; cps[18] = 1.; cps[19] = 2.;
    cps[20] = 1.; cps[21] = 2.; cps[22] = 1.; cps[23] = 2.;
    d->rational_bezier_surface_data->create(dim, nb_cp_u, nb_cp_v, cps);
}

void dtkRationalBezierSurfaceDataDefaultTestCase::init(void)
{

}

void dtkRationalBezierSurfaceDataDefaultTestCase::testUDegree(void)
{
    QVERIFY(d->rational_bezier_surface_data->uDegree() == 1);
}

void dtkRationalBezierSurfaceDataDefaultTestCase::testVDegree(void)
{
    QVERIFY(d->rational_bezier_surface_data->vDegree() == 2);
}

//To ameliorate with dataSurfacePoint and several pints to test
void dtkRationalBezierSurfaceDataDefaultTestCase::testSurfacePoint(void)
{
    double* point = new double[3];
    d->rational_bezier_surface_data->evaluatePoint(0.25, 0.65, point);

    QVERIFY(point[0] > 0.25 - 10e-5 && point[0] < 0.25 + 10e-5);
    QVERIFY(point[1] > 1.3 - 10e-5 && point[1] < 1.3 + 10e-5);
    QVERIFY(point[2] > 1. - 10e-5 && point[2] < 1. + 10e-5);
    delete[] point;
}

//To ameliorate with dataSurfacePoint and several pints to test
void dtkRationalBezierSurfaceDataDefaultTestCase::testSurfaceNormal(void)
{
    double* normal = new double[3];
    d->rational_bezier_surface_data->evaluateNormal(0.5, 0.5, normal);
    // std::cerr << "normal result on 1_2: " << result_1_2[0] << " " << result_1_2[1] << " " << result_1_2[2] << std::endl;
    delete[] normal;

}

void dtkRationalBezierSurfaceDataDefaultTestCase::cleanup(void)
{

}

void dtkRationalBezierSurfaceDataDefaultTestCase::cleanupTestCase(void)
{
    delete d->rational_bezier_surface_data;
    d->rational_bezier_surface_data = nullptr;
}

DTKNURBSSURFACEDATADEFAULTTEST_MAIN_NOGUI(dtkRationalBezierSurfaceDataDefaultTest, dtkRationalBezierSurfaceDataDefaultTestCase)

//
// dtkRationalBezierSurfaceDataDefaultTest.cpp ends here
