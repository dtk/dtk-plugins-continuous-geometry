// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include "dtkRationalBezierSurfaceDataDefaultTests.h"

class dtkRationalBezierSurfaceDataDefaultTestCasePrivate;

class dtkRationalBezierSurfaceDataDefaultTestCase : public QObject
{
    Q_OBJECT

public:
    dtkRationalBezierSurfaceDataDefaultTestCase(void);
    ~dtkRationalBezierSurfaceDataDefaultTestCase(void);

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testUDegree(void);
    void testVDegree(void);
    void testSurfacePoint(void);
    void testSurfaceNormal(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);

private:
    dtkRationalBezierSurfaceDataDefaultTestCasePrivate* d;
};

//
// dtkRationalBezierSurfaceDataDefaultTest.h ends here
