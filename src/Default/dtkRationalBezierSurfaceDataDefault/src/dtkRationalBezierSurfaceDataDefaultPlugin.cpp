// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#include "dtkRationalBezierSurfaceDataDefault.h"
#include "dtkRationalBezierSurfaceDataDefaultPlugin.h"

#include <dtkCore>
#include <dtkContinuousGeometry>

// ///////////////////////////////////////////////////////////////////
// dtkRationalBezierSurfaceDataDefaultPlugin
// ///////////////////////////////////////////////////////////////////

void dtkRationalBezierSurfaceDataDefaultPlugin::initialize(void)
{
    dtkContinuousGeometry::abstractRationalBezierSurfaceData::pluginFactory().record("dtkRationalBezierSurfaceDataDefault", dtkRationalBezierSurfaceDataDefaultCreator);
}

void dtkRationalBezierSurfaceDataDefaultPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkRationalBezierSurfaceDataDefault)

//
// dtkRationalBezierSurfaceDataDefaultPlugin.cpp ends here
