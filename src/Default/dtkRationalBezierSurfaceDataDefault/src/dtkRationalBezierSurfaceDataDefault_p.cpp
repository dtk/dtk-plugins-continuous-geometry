// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkRationalBezierSurfaceDataDefault_p.h"

/*!
    \class dtkRationalBezierSurfaceDataDefaultPrivate
    \inmodule dtkPluginsContinuousGeometry
    \brief dtkRationalBezierSurfaceDataDefaultPrivate is the "d-pointer" of dtkRationalBezierSurfaceDataDefault.
 */

/*!
  Constructor
*/
dtkRationalBezierSurfaceDataDefaultPrivate::dtkRationalBezierSurfaceDataDefaultPrivate()
{
}

/*!
  Destructor
*/
dtkRationalBezierSurfaceDataDefaultPrivate::~dtkRationalBezierSurfaceDataDefaultPrivate()
{
}

/*!
  Returns the degree of the surface in the u direction
 */
std::size_t dtkRationalBezierSurfaceDataDefaultPrivate::uDegree(void) const
{
    return (m_u_knots.size() - m_u_control_points_max_id - 2);// m_u_control_points_max_id + 1 = u_control_points_count
}

/*!
  Returns the degree of the surface in the v direction
 */
std::size_t dtkRationalBezierSurfaceDataDefaultPrivate::vDegree(void) const
{
    return (m_v_knots.size() - m_v_control_points_max_id - 2); // m_v_control_points_max_id + 1 = v_control_points_count
}

/*!
    Returns the weight at the \a i, \a j location
*/
double dtkRationalBezierSurfaceDataDefaultPrivate::weight(std::size_t i, std::size_t j) const
{
    return m_weights[{i, j}];
}

/*!
    Returns the control point at the \a i, \a j location
*/
const dtkRationalBezierSurfaceDataDefaultPrivate::Point_3& dtkRationalBezierSurfaceDataDefaultPrivate::controlPoint(std::size_t i, std::size_t j) const
{
    return m_control_points.at({i, j});
}

/*!
  Returns the knot vector in the u direction
 */
const std::vector< double >& dtkRationalBezierSurfaceDataDefaultPrivate::uKnots(void) const
{
    return m_u_knots;
}

/*!
  Returns the knot vector in the v direction
 */
const std::vector< double >& dtkRationalBezierSurfaceDataDefaultPrivate::vKnots(void) const
{
    return m_v_knots;
}

/*!
    Evaluates a \a r_point on the surface given a \a p_point in the parameters' domain
 */
void dtkRationalBezierSurfaceDataDefaultPrivate::surfacePoint(Point_3& r_point, const Point_2& p_point) const
{
    std::size_t uspan = findUSpan(p_point[0]);
    std::vector< double > u_basis_functions;
    uBasisFuns(u_basis_functions, uspan, p_point[0]);
    std::size_t vspan = findVSpan(p_point[1]);
    std::vector < double > v_basis_functions;
    vBasisFuns(v_basis_functions, vspan, p_point[1]);

    std::array < double, 4 > temp = {0., 0., 0., 0.};
    std::array < double, 4 > Sw = {0., 0., 0., 0.};
    for (std::size_t l = 0; l <= vDegree(); ++l) {
        temp = {0., 0., 0., 0.};
        for (std::size_t k = 0; k <= uDegree(); ++k) {
            std::size_t i = uspan - uDegree() + k;
            std::size_t j = vspan - vDegree() + l;
            temp[0] += u_basis_functions[k] * m_control_points.at({i, j})[0] * m_weights[{i, j}];
            temp[1] += u_basis_functions[k] * m_control_points.at({i, j})[1] * m_weights[{i, j}];
            temp[2] += u_basis_functions[k] * m_control_points.at({i, j})[2] * m_weights[{i, j}];
            temp[3] += u_basis_functions[k] * m_weights[{i, j}];
        }

        Sw[0] += v_basis_functions[l] * temp[0];
        Sw[1] += v_basis_functions[l] * temp[1];
        Sw[2] += v_basis_functions[l] * temp[2];
        Sw[3] += v_basis_functions[l] * temp[3];
    }
    r_point[0] = Sw[0]/Sw[3];
    r_point[1] = Sw[1]/Sw[3];
    r_point[2] = Sw[2]/Sw[3];
}

/*!
    Returns a point on the surface given a \a p_point in the parameters' domain
 */
dtkContinuousGeometryPrimitives::Point_3 dtkRationalBezierSurfaceDataDefaultPrivate::surfacePoint(const Point_2& p_point) const
{
    Point_3 r_point(0., 0., 0.);
    surfacePoint(r_point, p_point);
    return r_point;
}

/*!
    Evaluates the first derivative (\a r_vector) in the u direction on the surface given a \a p_point in the parameters' domain
 */
void dtkRationalBezierSurfaceDataDefaultPrivate::surfaceUDeriv(Vector_3& r_vector, const Point_2& p_point) const
{
    surfaceDeriv(r_vector, p_point, 1, 0);
}

/*!
    Returns the first derivative in the u direction on the surface given a \a p_point in the parameters' domain
 */
dtkContinuousGeometryPrimitives::Vector_3 dtkRationalBezierSurfaceDataDefaultPrivate::surfaceUDeriv(const Point_2& p_point) const
{
    Vector_3 r_vector(0., 0., 0.);
    surfaceUDeriv(r_vector, p_point);
    return r_vector;
}

/*!
    Evaluates the first derivative (\a r_vector) in the v direction on the surface given a \a p_point in the parameters' domain
 */
void dtkRationalBezierSurfaceDataDefaultPrivate::surfaceVDeriv(Vector_3& r_vector, const Point_2& p_point) const
{
    surfaceDeriv(r_vector, p_point, 0, 1);
}

/*!
    Returns the first derivative in the u direction on the surface given a \a p_point in the parameters' domain
 */
dtkContinuousGeometryPrimitives::Vector_3 dtkRationalBezierSurfaceDataDefaultPrivate::surfaceVDeriv(const Point_2& p_point) const
{
    Vector_3 r_vector(0., 0., 0.);
    surfaceVDeriv(r_vector, p_point);
    return r_vector;
}

/*!
    Evaluates the normal (\a r_vector) of the surface given a \a p_point in the parameters' domain
 */
void dtkRationalBezierSurfaceDataDefaultPrivate::surfaceNormal(Vector_3& r_vector, const Point_2& p_point) const
{
   // /////////////////////////////////////////////////////////////////
    // Computing the normal at a given point in the space parameters (u, v)
    // requires calculating dS/du and dS/dv at (u, v) and taking their cross-product.
    // /////////////////////////////////////////////////////////////////


    // /////////////////////////////////////////////////////////////////
    // Computes the value of (S^w)^(k, l)(u, v) (the homogeneous expression of S(u, v)).
    // k and l beeing the derivatives with respect to u and v directions.
    // d = 1, will store into aders/wders the first derivative with respect to u, and to v.
    // /////////////////////////////////////////////////////////////////
    dtkMatrixMap< Array_3 > aders;
    dtkMatrixMap< double > wders;
    surfaceDerivsAlg1(aders, wders, p_point[0], p_point[1], 1);

    // /////////////////////////////////////////////////////////////////
    // Projects S^w(u, v) and derivatives computed in the previous function onto R3 to obtain S(u, v)
    // And stores S(u, v) and dS(u, v)/du / dS(u, v)/dv
    // /////////////////////////////////////////////////////////////////
    dtkMatrixMap< Array_3 > pders;
    ratSurfaceDerivs(pders, aders, wders, 1);

    r_vector = dtkContinuousGeometryTools::crossProduct(pders.at({1, 0}), pders.at({0, 1}));
}

/*!
    Returns the normal of the surface given a \a p_point in the parameters' domain
 */
dtkContinuousGeometryPrimitives::Vector_3 dtkRationalBezierSurfaceDataDefaultPrivate::surfaceNormal(const Point_2& p_point) const
{
    Vector_3 r_vector(0., 0., 0.);
    surfaceNormal(r_vector, p_point);
    return r_vector;
}

/*!
    Evaluates the \a n_u th derivative in the u direction/ \a n_v th derivative in the v direction (\a r_vector) on the surface given a \a p_point in the parameters' domain
 */
void dtkRationalBezierSurfaceDataDefaultPrivate::surfaceDeriv(Vector_3& r_vector, const Point_2& p_point, std::size_t n_u, std::size_t n_v) const
{
    dtkMatrixMap< Array_3 > aders;
    dtkMatrixMap< double > wders;
    surfaceDerivsAlg1(aders, wders, p_point[0], p_point[1], n_u + n_v);
    dtkMatrixMap< Array_3 > pders;
    ratSurfaceDerivs(pders, aders, wders, n_u + n_v);

    r_vector =  pders.at({n_u, n_v});
}

/*!
    Returns the \a n_u th derivative in the u direction/ \a n_v th derivative in the v direction on the surface given a \a p_point in the parameters' domain
 */
dtkContinuousGeometryPrimitives::Vector_3 dtkRationalBezierSurfaceDataDefaultPrivate::surfaceDeriv(const Point_2& p_point, std::size_t n_u, std::size_t n_v) const
{
    Vector_3 r_vector(0., 0., 0.);
    surfaceDeriv(r_vector, p_point, n_u, n_v);
    return r_vector;
}

/*!
    Computes all the derivatives up to k + l = \a n_d of the surface (k being the derivative degree in the u direction and l being the derivative degree in the v direction) in \a r_derivatives given a \a p_point in the parameters' domain
 */
void dtkRationalBezierSurfaceDataDefaultPrivate::surfaceDeriv(dtkMatrixMap< Array_3 >& r_derivatives, const Point_2& p_point, std::size_t n_d) const
{
    dtkMatrixMap< Array_3 > aders;
    dtkMatrixMap< double > wders;
    surfaceDerivsAlg1(aders, wders, p_point[0], p_point[1], n_d);
    ratSurfaceDerivs(r_derivatives, aders, wders, n_d);
}

/*!
    Returns all the derivatives up to k + l = \a n_d of the surface (k being the derivative degree in the u direction and l being the derivative degree in the v direction) given a \a p_point in the parameters' domain
 */
dtkContinuousGeometryTools::dtkMatrixMap< dtkContinuousGeometryPrimitives::Array_3 >  dtkRationalBezierSurfaceDataDefaultPrivate::surfaceDeriv(const Point_2& p_point, std::size_t n_d) const
{
    dtkMatrixMap< Array_3 > r_derivatives;
    surfaceDeriv(r_derivatives, p_point, n_d);
    return r_derivatives;
}

/*! \fn dtkRationalBezierSurfaceDataDefaultPrivate::aabb(double* r_aabb) const
  Writes in \a r_aabb the [xmin, ymin, zmin, xmax, ymax, zmax] coordinates.

  \a r_aabb : array of size 6 to store the limits coordinates
*/
void dtkRationalBezierSurfaceDataDefaultPrivate::aabb(double* r_aabb) const
{
    double xmin = std::numeric_limits< double >::max();
    double ymin = std::numeric_limits< double >::max();
    double zmin = std::numeric_limits< double >::max();

    double xmax = std::numeric_limits< double >::min();
    double ymax = std::numeric_limits< double >::min();
    double zmax = std::numeric_limits< double >::min();

    Point_3 p(0., 0., 0.);
    for (std::size_t i = 0; i <= m_u_control_points_max_id; ++i) {
        for (std::size_t j = 0; j <= m_v_control_points_max_id; ++j) {
            p = m_control_points.at({i, j});

            if (p[0] < xmin) {
                xmin = p[0];
            }

            if (p[0] > xmax) {
                xmax = p[0];
            }

// ///////////////////////////////////////////////////////////////////

            if (p[1] < ymin) {
                ymin = p[1];
            }

            if (p[1] > ymax) {
                ymax = p[1];
            }

// ///////////////////////////////////////////////////////////////////x

            if (p[2] < zmin) {
                zmin = p[2];
            }

            if (p[2] > zmax) {
                zmax = p[2];
            }
        }
    }
    r_aabb[0] = xmin;
    r_aabb[1] = ymin;
    r_aabb[2] = zmin;
    r_aabb[3] = xmax;
    r_aabb[4] = ymax;
    r_aabb[5] = zmax;
}
// void dtkRationalBezierSurfaceDataDefaultPrivate::decomposeSurface(std::vector< dtkRationalBezierSurface* >& bezier_patches) const
// {
//     std::size_t u_degree = uDegree();
//     std::size_t v_degree = vDegree();
//     std::vector< double > alphas((std::max(u_degree, v_degree) + 1), 0.);

//     //u knots for bezier patch of degree u_degree in u direction
//     std::vector < double > u_knots(2 * (u_degree + 1), 0.);
//     for (std::size_t i = u_knots.size() / 2; i < u_knots.size(); ++i) {
//         u_knots[i] = 1;
//     }

//     // ///////////////////////////////////////////////////////////
//     ///For debugging purpose only
//     // std::cerr << "/////////////////////////////////" << std::endl;
//     // std::cerr << "u knots :" << std::endl;
//     // for (std::size_t i = 0; i < u_knots.size(); ++i) {
//     //         std::cerr << u_knots[i] << std::endl;
//     // }
//     // std::cerr << "/////////////////////////////////" << std::endl;
//     // ///////////////////////////////////////////////////////////////

//     //v knots for bezier patch of degree vDegree in v direction
//     std::vector < double > v_knots(2 * (v_degree +1));
//     for (std::size_t i = 0; i < v_knots.size() / 2; ++i) {
//         v_knots[i] = 1;
//     }

//     // ///////////////////////////////////////////////////////////
//     ///For debugging purpose only
//     // std::cerr << "/////////////////////////////////" << std::endl;
//     // std::cerr << "v knots :" << std::endl;
//     // for (std::size_t i = 0; i < v_knots.size(); ++i) {
//     //         std::cerr << v_knots[i] << std::endl;
//     // }
//     // std::cerr << "/////////////////////////////////" << std::endl;
//     // ///////////////////////////////////////////////////////////////

//     //The algorithms first creates strips of Bezier pathces/BSpline, then the BSpline is discretized in Bezier patches to obtain Bezier patches
//     //Set the knots for all the bezier patches alogn the u direction
//     dtkRationalBezierSurface d = dtkRationalBezierSurface();
//     d.addControlPoint(0, 0, {0,0,0});
//     std::vector < dtkRationalBezierSurface > Su(m_u_control_points_max_id + 1 - u_degree);
//     for (std::size_t i = 0; i < Su.size(); ++i) {
//         Su[i].setUKnots(u_knots);
//         Su[i].setVKnots(m_v_knots);
//     }

//     std::size_t m = m_u_control_points_max_id + 1 + u_degree;
//     std::size_t a = u_degree;
//     std::size_t b = u_degree + 1;
//     std::size_t nb = 0;

//     for (std::size_t i = 0; i <= u_degree; ++i) {
//         for (std::size_t col = 0; col <= m_v_control_points_max_id; ++col){
//             Su[nb].addControlPoint(i, col, m_control_points[{i, col}]);
//         }
//     }
//     std::size_t mult = 0; //knot multiplicity
//     std::size_t numer = 0;
//     std::size_t g = 0;
//     std::size_t r = 0;
//     std::size_t save = 0;
//     std::size_t s =0;
//     double alpha = 0.;

//     while ( b < m) {
//         g = b;
//         //Counts the multiplicity of the knot
//         while ( b < m && u_knots[b + 1] <= u_knots[b]){
//             ++b;
//         }
//         mult = b - g + 1;
//         if ( mult < u_degree) {
//             numer = u_knots[b] - u_knots[a];// the enumerator of the alphas
//             for (std::size_t j = u_degree; j > mult; --j) {
//                 alphas[j - mult - 1] = numer / (m_u_knots[a + j] - m_u_knots[a]);
//             }
//             r = u_degree - mult; // insert knot r times
//             for (std::size_t j = 1; j <= r; ++j) {
//                 save = r - j;
//                 s = mult + j;
//                 for (std::size_t k =  u_degree; k >= s; --k) {
//                     alpha = alphas[k - s];
//                     for (std::size_t col = 0; col < m_v_control_points_max_id + 1; ++col) {
//                         std::array < double, 3 > point;
//                         std::array < double , 3 > control_point_k = Su[nb].controlPoint(k, col);
//                         std::array < double , 3 > control_point_k_1 = Su[nb].controlPoint(k-1, col);
//                         point[0] = alpha * control_point_k[0] + (1.0 - alpha) * control_point_k_1[0];
//                         point[1] = alpha * control_point_k[1] + (1.0 - alpha) * control_point_k_1[1];
//                         point[2] = alpha * control_point_k[2] + (1.0 - alpha) * control_point_k_1[2];

//                         Su[nb].addControlPoint(k, col, point);
//                     }
//                 }
//                 if (b < m){
//                     for ( std::size_t col = 0; col < m_v_control_points_max_id + 1; ++col) {
//                         Su[nb + 1].addControlPoint(save, col, Su[nb].controlPoint(u_degree, col));
//                     }
//                 }
//             }
//         }
//         ++nb;
//         if (b < m) {
//             for (std::size_t i = u_degree - mult; i <= u_degree; ++i) {
//                 for (std::size_t col = 0; col < m_v_control_points_max_id + 1; ++col) {
//                     Su[nb].addControlPoint(i, col, m_control_points[{b - u_degree + i, col}]);
//                 }
//             }
//             a = b;
//             ++b;
//         }
//     }

//     Su.resize(nb);

//     std::vector< dtkRationalBezierSurface* >& S = bezier_patches;
//     S.resize(Su.size() * (m_v_control_points_max_id + 1 - v_degree));
//     for (std::size_t i = 0; i < S.size(); ++i) {
//         S[i] = new dtkRationalBezierSurface();
//         S[i]->setUKnots(u_knots);
//         S[i]->setVKnots(v_knots);
//     }

//     nb = 0;

//     for (std::size_t np = 0; np < Su.size(); ++ np) {
//         for (std::size_t i = 0; i <= u_degree; ++i){
//             for (std::size_t j = 0; j <= v_degree; ++j){
//                 S[nb]->addControlPoint(i, j, Su[np].controlPoint(i, j));
//             }
//         }
//         m = m_v_control_points_max_id + 1 + v_degree;
//         a = v_degree;
//         b = v_degree +1;

//         while (b < m) {
//             g = b;
//             while (b < m && v_knots[b + 1] <= v_knots[b]) {
//                 ++b;
//             }
//             mult = b - g + 1;
//             if (mult < v_degree) {
//                 numer = v_knots[b] - v_knots[a];
//                 for (std::size_t j = v_degree; j > mult; --j) {
//                     alphas[j - mult -1] = numer / (m_v_knots[a +j] - m_v_knots[a]);
//                 }
//                 r = v_degree - mult;
//                 for (std::size_t j = 1; j <= r; ++j) {
//                     save = r -j;
//                     s = mult + j;
//                     for (std::size_t k = v_degree; k >= s; --k) {
//                         alpha = alphas[k - s];
//                         for (std::size_t row = 0; row <= u_degree; ++row) {
//                             std::array < double, 3 > point;
//                             std::array < double , 3 > control_point_k = S[nb]->controlPoint(row, k);
//                             std::array < double , 3 > control_point_k_1 = S[nb]->controlPoint(row, k - 1);
//                             point[0] = alpha * control_point_k[0] + (1.0 - alpha) * control_point_k_1[0];
//                             point[1] = alpha * control_point_k[1] + (1.0 - alpha) * control_point_k_1[1];
//                             point[2] = alpha * control_point_k[2] + (1.0 - alpha) * control_point_k_1[2];

//                             S[nb]->addControlPoint(row, k, point);
//                         }
//                     }
//                 }
//                 if (b < m) {
//                     for (std::size_t row = 0; row <= u_degree; ++row) {
//                         S[nb + 1]->addControlPoint(row, save, S[nb]->controlPoint(row, v_degree));
//                     }
//                 }
//             }
//             ++nb;
//             if (b < m){
//                 for (std::size_t i = v_degree - mult; i <= v_degree; ++i) {
//                     for (std::size_t row = 0; row <= u_degree; ++row) {
//                         S[nb]->addControlPoint(row, i, Su[np].controlPoint(row, b - v_degree + i));
//                     }
//                 }
//                 a = b;
//                 ++b;
//             }
//         }
//     }
// }

/*!
    Returns the knot span index

    \a a is a value inside the knots overall span (a_0 -> a_(m+p+1)), described by \a knots
    if \a a is between a_k and a_(k+1) the function returns k
    \a knots is the considered vector of knots in either u or v direction, \a degree the degree in the considered u or v direction
*/
std::size_t dtkRationalBezierSurfaceDataDefaultPrivate::findSpan(const std::vector< double >& knots, std::size_t degree, double a) const
{
    std::size_t m = knots.size() - 1;
    std::size_t n = m - degree - 1;
    //Handles the case a outside of bounds : any valid span index could be given because all function basis will return 0.
    if (a <= knots[0]) {
        return degree;
    }
    if (a >= knots[n+1]) {
        return n;
    }

    std::size_t low = degree;
    std::size_t high = n + 1;
    std::size_t mid = (low + high) / 2;

    while(a < knots[mid] || a >= knots[mid + 1]) {
        if (a < knots[mid]) {
            high = mid;
        } else {
            low = mid;
        }
        mid = (low + high) / 2;
    }
    return mid;
}

/*!
    Returns the knot span index of \a u in the u direction

    \a u is a value inside the knots overall span (u_0 -> u_(u_knots.size() + /l uDegree + 1))
    if \a u is between u_k and u_(k+1) the function returns k
*/
std::size_t dtkRationalBezierSurfaceDataDefaultPrivate::findUSpan(double u) const
{
  return findSpan(m_u_knots, uDegree(), u);
}

/*!
    Returns the knot span index of \a v in the v direction

    \a v is a value inside the knots overall span (v_0 -> v_(v_knots.size() + /l vDegree + 1))
    if \a v is between v_k and v_(k+1) the function returns k
*/
std::size_t dtkRationalBezierSurfaceDataDefaultPrivate::findVSpan(double v) const
{
    return findSpan(m_v_knots, vDegree(), v);
}

/*!
    Computes the nonvanishing \a basis_functions of the knot span \a i, at \a a, up to the degree \a degree.
    It requires \a knots which is the knots vector in the appropriate direction u or v.
*/
void dtkRationalBezierSurfaceDataDefaultPrivate::basisFuns( std::vector< double >& basis_functions, std::size_t i, double a, std::size_t degree, const std::vector< double >& knots) const
{
    basis_functions.clear();
    std::vector< double > left(degree + 1, 0);
    std::vector< double > right(degree +1, 0);

    double saved = 0.;
    double temp = 0.;

    basis_functions.resize(degree + 1);
    basis_functions[0] = 1.;
    for( std::size_t j = 1; j <= degree; ++j) {
        left[j] = a - knots[i + 1 - j];
        right[j] = knots[i + j] - a;
        saved = 0.;
        for (std::size_t r = 0; r < j; ++r) {
            temp = basis_functions[r] / (right[r + 1] + left[j - r]);
            basis_functions[r] = saved + right[r + 1] * temp;
            saved = left[j - r] * temp;
        }
        basis_functions[j] = saved;
    }

}

/*!
    Returns in \a u_basis_functions the nonvanishing basis functions of the knot span \a i, computed at \a u.
*/
void dtkRationalBezierSurfaceDataDefaultPrivate::uBasisFuns(std::vector< double >& u_basis_functions, std::size_t i, double u) const
{
    basisFuns(u_basis_functions, i, u, uDegree(), m_u_knots);
}

/*!
    Returns in \a v_basis_functions the nonvanishing basis functions of the knot span \a i, computed at \a v.
*/
void dtkRationalBezierSurfaceDataDefaultPrivate::vBasisFuns(std::vector< double >& v_basis_functions, std::size_t i, double v) const
{
    basisFuns(v_basis_functions, i, v, vDegree(), m_v_knots);
}

/*!
  Compute nonzero basis functions and their derivatives.

  \a n is the degree up to which the derivatives will be computed, it must be lower than \a degree, which is the degree of the curve. \a knots is the vector containing the knot values, \a a is the value at which the derivatives basis functions will be computed. \a i is the span index associated with the value \a a. \a der_basis_functions is a matrix containing for each derivative degree the non-zero derivative functions.
*/
void  dtkRationalBezierSurfaceDataDefaultPrivate::derBasisFuns(dtkMatrixMap < double >& der_basis_functions, std::size_t i, std::size_t n, double a, std::size_t degree, const std::vector< double >& knots) const
{
    der_basis_functions.clear();
    dtkMatrixMap< double > ndu; // of size degree + 1 x degree + 1 to store the basis functions and knots differences

    for (std::size_t j = 0; j <= degree; ++j) {
        for (std::size_t r = 0; r <= degree; ++r) {
            ndu[{j, r}] = 0.;
        }
    }

    std::vector< double > left(degree + 1, 0.);
    std::vector< double > right(degree + 1, 0.);

    double saved = 0.;
    double temp = 0.;

    ndu[{0, 0}] = 1.0;
    for( std::size_t j = 1; j <= degree; ++j) {
        left[j] = a - knots[i + 1 - j];
        right[j] = knots[i + j] - a;
        saved = 0.;
        for (std::size_t r = 0; r < j; ++r) {
            //Lower triangle
            ndu[{j, r}] = right[r + 1] + left[j - r];
            temp = ndu[{r, j - 1}] / ndu[{j, r}];
            // std::cerr << "ndu(j, r) : " <<  right[r + 1] + left[j - r] << std::endl;
            //Upper triangle
            ndu[{r, j}] = saved + right[r + 1] * temp;
            // std::cerr << "ndu(r, j) : " << saved + right[r + 1] * temp << std::endl;
            saved = left[j - r] * temp;
        }
        //Diagonal
        ndu[{j, j}] = saved;
	// std::cerr << "ndu(j, j) : " << saved << std::endl;

    }

    // ///////////////////////////////////////////////////////////
    ///For debuggind purpose only
    // std::cerr << "/////////////////////////////////" << std::endl;
    // std::cerr << "ndu :" << std::endl;
    // for (std::size_t j = 0; j <= degree; ++j) {
    //     for (std::size_t r = 0; r <= degree; ++r) {
    //         std:: cerr << ndu[{j, r}] << " ";
    //     }
    //     std::cerr << std::endl;
    // }
    // std::cerr << "/////////////////////////////////" << std::endl;
    // ///////////////////////////////////////////////////////////////

    //Load the basis function
    for( std::size_t j = 0; j <= degree; ++j) {
        der_basis_functions[{0, j}] = ndu[{j, degree}];
    }


    //Computes the derivativves
    //to store the 2 most recently computed rows a(k, j) and a(k - 1, j)
    std::vector < double > a_0(degree + 1, 0.);
    std::vector < double > a_1(degree + 1, 0.);
    for (std::size_t r = 0; r <= degree; ++r) {
        int s1 = 0;
        int s2 = 1;
        a_0[0] = 1.;
        //Loop to conmputes kth derivative
        for (std::size_t k = 1; k <= n; ++k) {
            double d = 0.;
            int rk = (int)r - (int)k;
	    // std::cerr << "rk : " << rk << std::endl;
            int pk = (int)degree - (int)k;
	    // std::cerr << "pk : " << pk << std::endl;
            if (r >= k) {
                a_1[0] = a_0[0] / ndu[{pk + 1, rk}];
                // std::cerr <<  "a_0[, 0}] : " <<  a_0[, 0}] << std::endl;
                // std::cerr << " a_1[ 0}] : " <<  a_1[ 0}] << std::endl;
                d = a_1[0] * ndu[{rk, pk}];
                // std::cerr << "r >= k  d : " << d << std::endl;

            }

            std::size_t j1;
            std::size_t j2;

            if (rk >= -1) {
                j1 = 1;
                // std::cerr << "rk >= -1 j1 : " << j1 << std::endl;
            } else {
                j1 = -rk;
                 // std::cerr << "rk < -1 j1 : " << j1 << std::endl;
                    }
            if ((int)r - 1 <= pk) {
                j2 = k - 1;
                 // std::cerr << " r - 1 <= pk j2 : " << j2 << std::endl;
            } else {
                j2 = degree - r;
                // std::cerr << " r - 1 > pk j2 : " << j2 << std::endl;
            }

            for (std::size_t j = j1; j <= j2; ++j) {
                a_1[j] = (a_0[j] - a_0[j - 1]) / ndu[{pk + 1, rk + j}];
                // std::cerr << "a_0[, j}] : " << a_0[, j}] << std::endl;
                // std::cerr << " a_0[, j - 1}] : " << a_0[, j - 1}] << std::endl;
                // std::cerr << "ndu[{pk + 1, rk + j}] : " <<  ndu[{pk + 1, rk + j}] << std::endl;
                // std::cerr << "a_1[ j}] : " <<  a_1[ j}] << std::endl;
                d += a_1[j] * ndu[{rk + j, pk}];
                // std::cerr << "j1->j2 d : " << d << std::endl;
            }
            if ((int)r <= pk) {
                a_1[k] = - a_0[k - 1] / ndu[{pk + 1, r}];
                //  std::cerr << "a_1[ k}] : " <<  a_1[ k}] << std::endl;
                // std::cerr << "r : " << r << " p : " << degree << " k : " << k << std::endl;
                // std::cerr << "ndu[{pk + 1, rk + j}] : " <<  ndu[{pk + 1, rk + j}] << std::endl;

                d += a_1[k] * ndu[{r, pk}];
                // std::cerr << " r <= pk d : " << d << std::endl;
            }
            der_basis_functions[{k, r}] = d;
            // std::cerr << "der_basis_functions[{k, r}] : " << der_basis_functions[{k, r}] << std::endl;
            std::size_t b = s1;
            s1 = s2;
            s2 = b;
        }
    }

    std::size_t r = degree;
    for (std::size_t k = 1; k <= n; k++) {
        for (std::size_t j = 0; j <= degree; j++) {
            der_basis_functions[{k, j}] *= r;
            // std::cerr << "r : " << r << std::endl;
        }
        r *= degree - k;
    }

    // ///////////////////////////////////////////////////////////
    ///For debugging purpose only
    // std::cerr << "/////////////////////////////////" << std::endl;
    // std::cerr << "der_basis_functions :" << std::endl;
    // for (std::size_t k = 0; k <= n; ++k) {
    //     for (std::size_t j = 0; j <= degree; ++j) {
    //         std:: cerr << der_basis_functions[{k, j}] << " ";
    //     }
    //     std::cerr << std::endl;
    // }
    // std::cerr << "/////////////////////////////////" << std::endl;
    // ///////////////////////////////////////////////////////////////
}

/*!
    Returns in \a der_u_basis_functions the nonzero basis functions of span index \a i and their derivatives up to the degree \a n, at \a u.
*/
void dtkRationalBezierSurfaceDataDefaultPrivate::uDersBasisFuns(dtkMatrixMap< double >& der_u_basis_functions, std::size_t i, std::size_t n, double u) const
{
  derBasisFuns(der_u_basis_functions, i, n, u, uDegree(), m_u_knots);
}

/*!
    Returns in \a der_v_basis_functions the nonzero basis functions of span index \a i and their derivatives up to the degree \a n, at \a v.
*/
void dtkRationalBezierSurfaceDataDefaultPrivate::vDersBasisFuns(dtkMatrixMap< double >& der_v_basis_functions, std::size_t i, std::size_t n, double v) const
{
  derBasisFuns(der_v_basis_functions, i, n, v, vDegree(), m_v_knots);
}

/*!
    Computes the surface derivatives at \a u, \a v of the homogeneous expression of S(u,v) a.k. S^w(u,v).
    Up to l + k = \a degree where l and k are respectively the derivatives degree with regard to u and v directions.
    The results (the A matrix) are store in \a aders and \a wders, \a aders for the points coordinates and weights, \a wders for the weights only.
*/
//This version of the algorithm is modified from the NURBS Book : is takes into accounts the weights, and instead of skl it returns its splitted version in aders and wders which matches better with ratSurfaceDerivs ->not sure its wize ...
void dtkRationalBezierSurfaceDataDefaultPrivate::surfaceDerivsAlg1(dtkMatrixMap< Array_3 >& aders, dtkMatrixMap< double >& wders ,double u, double v, std::size_t degree) const
{
    std::size_t du = std::min(degree, uDegree());
    for (std::size_t k = uDegree() + 1; k <= degree; ++k) {
        for (std::size_t l = 0; l <= degree - k; ++l) {
            aders.emplace(std::make_pair(k, l), Array_3(0., 0., 0.));
            wders.emplace(std::make_pair(k, l), 0.);
        }
    }

    std::size_t dv = std::min(degree, vDegree());
    for (std::size_t l = vDegree() + 1; l <= degree; ++l) {
        for (std::size_t k = 0; k <= degree - l; ++k) {
            aders.emplace(std::make_pair(k, l), Array_3(0., 0., 0.));
            wders.emplace(std::make_pair(k, l), 0.);
        }
    }

    std::size_t uspan = findUSpan(u);
    // std::cerr << "u span : " << uspan << std::endl;
    // std::cerr << "du : " << du << std::endl;
    dtkMatrixMap< double > nu;
    uDersBasisFuns(nu, uspan,  du, u);
    // ///////////////////////////////////////////////////////////
    ///For debugging purpose only
    // std::cerr << "/////////////////////////////////" << std::endl;
    // std::cerr << "der_u_basis_functions :" << std::endl;
    // for (std::size_t k = 0; k <= du; ++k) {
    //     for (std::size_t j = 0; j <= uDegree(); ++j) {
    //         std:: cerr << nu[{k, j}] << " ";
    //     }
    //     std::cerr << std::endl;
    // }
    // std::cerr << "/////////////////////////////////" << std::endl;
    // ///////////////////////////////////////////////////////////////

    std::size_t vspan = findVSpan(v);
    // std::cerr << "v span : " << vspan << std::endl;
    // std::cerr << "dv : " << dv << std::endl;
    dtkMatrixMap< double > nv;
    vDersBasisFuns(nv, vspan, dv, v);
    // ///////////////////////////////////////////////////////////
    ///For debugging purpose only
    // std::cerr << "/////////////////////////////////" << std::endl;
    // std::cerr << "der_v_basis_functions :" << std::endl;    // for (std::size_t k = 0; k <= dv; ++k) {
    //     for (std::size_t j = 0; j <= vDegree(); ++j) {
    //         std:: cerr << nv[{k, j}] << " ";
    //     }
    //     std::cerr << std::endl;
    // }
    // std::cerr << "/////////////////////////////////" << std::endl;
    // ///////////////////////////////////////////////////////////////

    //one vector for each component (x, y, z, w)
    std::vector< double > temp_0(vDegree() + 1);
    std::vector< double > temp_1(vDegree() + 1);
    std::vector< double > temp_2(vDegree() + 1);
    std::vector< double > temp_3(vDegree() + 1);

    for (std::size_t k = 0; k <= du; ++k) {
        for (std::size_t s = 0; s <= vDegree(); ++s) {
            temp_0[s] = 0.;
            temp_1[s] = 0.;
            temp_2[s] = 0.;
            temp_3[s] = 0.;
            for (std::size_t r = 0; r <= uDegree(); ++r) {
                std::size_t i = (int)uspan - (int)uDegree() + (int)r;
                std::size_t j = (int)vspan - (int)vDegree() + (int)s;
                temp_0[s] += nu[{k, r}] * m_control_points.at({i, j})[0] * m_weights[{i, j}];
                temp_1[s] += nu[{k, r}] * m_control_points.at({i, j})[1] * m_weights[{i, j}];
                temp_2[s] += nu[{k, r}] * m_control_points.at({i, j})[2] * m_weights[{i, j}];
                temp_3[s] += nu[{k, r}] * m_weights[{i, j}];
            }
        }
        // ///////////////////////////////////////////////////////////
        ///For debugging purpose only
        // ///////////////////////////////////////////////////////////
        // std::cerr << "/////////////////////////////////" << std::endl;
        // std::cerr << "Nu * Pw :" << std::endl;
        // for (std::size_t s = 0; s <= vDegree(); ++s) {
        //     std:: cerr << "x : " << temp_0[s] << " y : " << temp_1[s] << " z : " << temp_2[s] << " w : " << temp_3[s] << std::endl;
        // }
        // ///////////////////////////////////////////////////////////////


        std::size_t dd = std::min((int)degree - (int)k, (int)dv);
        Array_3 a_point(0., 0., 0.);
        double w_coef;
        for (std::size_t l = 0; l <= dd; ++l) {
            a_point[0] = 0.;
            a_point[1] = 0.;
            a_point[2] = 0;
            w_coef = 0.;

            for (std::size_t s = 0; s <= vDegree(); s++) {
                a_point[0] += nv[{l, s}] * temp_0[s];
                a_point[1] += nv[{l, s}] * temp_1[s];
                a_point[2] += nv[{l, s}] * temp_2[s];
                w_coef +=  nv[{l, s}] * temp_3[s];
            }
            aders.emplace(std::make_pair(k, l), a_point);
            wders.emplace(std::make_pair(k, l), w_coef);
        }
    }

    // ///////////////////////////////////////////////////////////
    ///For debugging purpose only
    // ///////////////////////////////////////////////////////////
    // std::cerr << "/////////////////////////////////" << std::endl;
    // std::cerr << "aders :" << std::endl;
    // for (std::size_t k = 0; k <= du; ++k) {
    //     std::size_t dd = std::min((int)degree - (int)k, (int)dv);
    //     for (std::size_t l = 0; l <= dd; ++l) {
    //         std:: cerr << "x : " << aders[{k, l}][0] << " y : " << aders[{k, l}][1] << " z : " << aders[{k, l}][2] << " " << std::endl;
    //     }
    //     std::cerr << std::endl;
    // }
    // std::cerr << "wders :" << std::endl;
    // for (std::size_t k = 0; k <= du; ++k) {
    //     std::size_t dd = std::min((int)degree - (int)k, (int)dv);
    //     for (std::size_t l = 0; l <= dd; ++l) {
    //         std:: cerr << " w : " << wders[{k, l}] << std::endl;
    //     }
    //     std::cerr << std::endl;
    // }
    // std::cerr << "/////////////////////////////////" << std::endl;
    // ///////////////////////////////////////////////////////////////
}

/*!
    Returns the S(u, v) derivatives in \a pders from S^w(u, v) (non rational expression of S) given as \a aders and \a wders.
    the k + l = \a degree is the degree up to which the derivatives in u and v directions must be computed. It has to match the degree set to compute \a aders and \a wders in \l surfaceDerivsAlg1.
 */
void dtkRationalBezierSurfaceDataDefaultPrivate::ratSurfaceDerivs(dtkMatrixMap< Array_3 >& pders, dtkMatrixMap< Array_3 >& aders, dtkMatrixMap< double >& wders, std::size_t degree) const
{
    dtkContinuousGeometryTools::computeBinomialCoefficients(m_binomial_coefficients, degree + 1, degree + 1);
    // ///////////////////////////////////////////////////////////
    ///For debugging purpose only
    // std::cerr << "/////////////////////////////////" << std::endl;
    // std::cerr << "binomial coefficients :" << std::endl;
    // for (std::size_t k = 0; k <= degree; ++k) {
    //     for (std::size_t j = 0; j <= degree; ++j) {
    //         std:: cerr << Bin(k, j) << " ";
    //     }
    //     std::cerr << std::endl;
    // }
    // std::cerr << "/////////////////////////////////" << std::endl;
    // ///////////////////////////////////////////////////////////////

    for (std::size_t k = 0; k <= degree; ++k) {
        for (std::size_t l = 0; l <= degree; ++l) {
            pders.emplace(std::make_pair(k, l), Array_3(0., 0., 0.));
        }
    }

    Array_3 v(0., 0., 0.);
    Array_3 v2(0., 0., 0.);
    for (std::size_t k = 0; k <= degree; ++k){
        for (std::size_t l = 0; l <= degree - k; ++l){
            v[0] = aders.at({k, l})[0];
            v[1] = aders.at({k, l})[1];
            v[2] = aders.at({k, l})[2];

            for (std::size_t j = 1; j <= l; ++j){
                v[0] -= Bin(l, j) * wders.at({0, j}) * pders.at({k, l-j})[0];
                v[1] -= Bin(l, j) * wders.at({0, j}) * pders.at({k, l-j})[1];
                v[2] -= Bin(l, j) * wders.at({0, j}) * pders.at({k, l-j})[2];

            }
            for (std::size_t i = 1; i <= k; ++i){
                v[0] -= Bin(k, i) * wders.at({i, 0}) * pders.at({k - i, l})[0];
                v[1] -= Bin(k, i) * wders.at({i, 0}) * pders.at({k - i, l})[1];
                v[2] -= Bin(k, i) * wders.at({i, 0}) * pders.at({k - i, l})[2];

                v2[0] = 0.;
                v2[1] = 0.;
                v2[2] = 0.;

                for (std::size_t j = 1; j <= l; ++j){
                    v2[0] += Bin(l, j) * wders.at({i, j}) * pders.at({k - i, l - j})[0];
                    v2[1] += Bin(l, j) * wders.at({i, j}) * pders.at({k - i, l - j})[1];
                    v2[2] += Bin(l, j) * wders.at({i, j}) * pders.at({k - i, l - j})[2];
                }
                v[0] -= Bin(k, i) * v2[0];
                v[1] -= Bin(k, i) * v2[1];
                v[2] -= Bin(k, i) * v2[2];
            }
            pders.at({k, l})[0] = v[0] / wders.at({0, 0});
            pders.at({k, l})[1] = v[1] / wders.at({0, 0});
            pders.at({k, l})[2] = v[2] / wders.at({0, 0});
        }
    }
}

/*!
    Returns the pre-computed binomial coefficient B(\a n, \a k) : \a k among \a n

    The function \l dtkContinuousGeometryTools::computeBinomialCoefficients(binomial_coefficients, n_max, k_max) must be called first
*/
double dtkRationalBezierSurfaceDataDefaultPrivate::Bin(std::size_t n, std::size_t k) const
{
    return m_binomial_coefficients[{n, k}];
}

//
// dtkRationalBezierSurfaceDataDefaultPrivate_p.cpp ends here
