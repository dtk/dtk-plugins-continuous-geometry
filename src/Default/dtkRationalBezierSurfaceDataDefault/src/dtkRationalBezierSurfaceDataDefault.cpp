// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkRationalBezierSurfaceDataDefault.h"

#include "dtkRationalBezierSurfaceDataDefault_p.h"

/*!
  \class dtkRationalBezierSurfaceDataDefault
  \inmodule dtkPluginsContinuousGeometry
  \brief dtkRationalBezierSurfaceDataDefault is the default implementation of the concept dtkAbstractRationalBezierSurfaceData.

  The following lines of code show how to instanciate and use the class in pratice:
  \code
  dtkAbstractRationalBezierSurfaceData *nurbs_surface_data = dtkContinuousGeometry::abstractRationalBezierSurfaceData::pluginFactory().create("dtkRationalBezierSurfaceDataDefault");
  dtkRationalBezierSurface *rational_bezier_surface = new dtkRationalBezierSurface(rational_bezier_surface_data);
  rational_bezier_surface->create(...);
  \endcode
*/

/*! \fn dtkRationalBezierSurfaceDataDefault::dtkRationalBezierSurfaceDataDefault(void)
  Instanciates the structure.

  To add the content, see \l create(std::size_t dim, std::size_t order_u, std::size_t order_v, double* cps) const.
*/
dtkRationalBezierSurfaceDataDefault::dtkRationalBezierSurfaceDataDefault(void) : d(new dtkRationalBezierSurfaceDataDefaultPrivate) {}

/*! \fn dtkRationalBezierSurfaceDataDefault::~dtkRationalBezierSurfaceDataDefault(void)
  Destroys the instance.
*/
dtkRationalBezierSurfaceDataDefault::~dtkRationalBezierSurfaceDataDefault(void)
{
    delete d;
}

/*! \fn void dtkRationalBezierSurfaceDataDefault::create(std::size_t dim, std::size_t nb_cp_u, std::size_t nb_cp_v, double *cps) const
  Creates a NURBS surface by providing the required content.

  \a dim : dimension of the space in which lies the surface

  \a nb_cp_u : number of control points along the "u" direction

  \a nb_cp_v : number of control points along the "v" direction

  \a cps : array containing the weighted control points, secified as : [x_00, y_00, z_00, w_00, x_01, y_01, ...]

  \a cps are copied.

  The control points are given as such :

  v = 1      cps[1]      | cps[3]

        ---------------- | --------------

  v= 0       cps[0]      | cps[2]

              u=0           u=1
*/
void dtkRationalBezierSurfaceDataDefault::create(std::size_t dim, std::size_t order_u, std::size_t order_v, double* cps) const
{
    // /////////////////////////////////////////////////////////////////
    // Initializes the map of control points and weights
    // /////////////////////////////////////////////////////////////////
    for (std::size_t i = 0; i < order_u; ++i) {
        for (std::size_t j = 0; j < order_v; ++j) {
            std::size_t pos = (dim + 1) * (i * order_v + j);
            Point_3 control_point_3(0., 0., 0.);
            control_point_3[0] = cps[pos];
            control_point_3[1] = cps[pos + 1];
            control_point_3[2] = cps[pos + 2];

            d->m_control_points.insert({{i, j}, control_point_3});
            d->m_weights.insert({{i, j}, cps[pos + 3]});
        }
    }
    d->m_u_control_points_max_id = order_u - 1;
    d->m_v_control_points_max_id = order_v - 1;
    d->m_u_weights_count = order_u;
    d->m_v_weights_count = order_v;

    // /////////////////////////////////////////////////////////////////
    // Initializes the vectors of knots
    // /////////////////////////////////////////////////////////////////
    for (std::size_t i = 0; i < order_u; ++i) {
        d->m_u_knots.push_back(0);
    }
    for (std::size_t i = 0; i < order_u; ++i) {
        d->m_u_knots.push_back(1);
    }

    for (std::size_t i = 0; i < order_v; ++i) {
        d->m_v_knots.push_back(0);
    }
    for (std::size_t i = 0; i < order_v; ++i) {
        d->m_v_knots.push_back(1);
    }
}

/*! \fn void dtkRationalBezierSurfaceDataDefault::create(std::string path) const
  Creates a NURBS surface by providing a path to a file storing the required information to create a rational Bezier surface.

  \a path : a valid path to the file containing the information regarding a given rational Bezier surface
*/
void dtkRationalBezierSurfaceDataDefault::create(std::string) const
{
    dtkFatal() << Q_FUNC_INFO << " not yet implemented.";
}

/*! \fn dtkRationalBezierSurfaceDataDefault::uDegree(void) const
  Returns the degree of the surface in the "u" direction.
*/
std::size_t dtkRationalBezierSurfaceDataDefault::uDegree(void) const
{
    return d->uDegree();
}

/*! \fn dtkRationalBezierSurfaceDataDefault::vDegree(void) const
  Returns the degree of the surface in the "v" direction.
*/
std::size_t dtkRationalBezierSurfaceDataDefault::vDegree(void) const
{
    return d->vDegree();
}

/*! \fn dtkRationalBezierSurfaceDataDefault::controlPoint(std::size_t i, std::size_t j, double *r_cp) const
  Writes in \a r_cp the \a i th, \a j th weighted control point coordinates[xij, yij, zij].

  \a i : index of the weighted control point along the "u" direction

  \a j : index of the weighted control point along the "v" direction

  \a r_cp : array of size 3 to store the weighted control point coordinates[xij, yij, zij]
 */
void dtkRationalBezierSurfaceDataDefault::controlPoint(std::size_t i, std::size_t j, double* r_cp) const
{
    Point_3 t_cp(0., 0., 0.);
    t_cp = d->controlPoint(i, j);
    r_cp[0] = t_cp[0];
    r_cp[1] = t_cp[1];
    r_cp[2] = t_cp[2];
}

/*! \fn dtkRationalBezierSurfaceDataDefault::weight(std::size_t i, std::size_t j, double *r_w) const
  Writes in \a r_w the \a i th, \a j th weighted control point weight wij.

  \a i : index of the weighted control point along the "u" direction

  \a j : index of the weighted control point along the "v" direction

  \a r_w : array of size 1 to store the weighted control point weight wij
*/
void dtkRationalBezierSurfaceDataDefault::weight(std::size_t i, std::size_t j, double* r_w) const
{
    *r_w = d->weight(i, j);
}

/*! \fn dtkRationalBezierSurfaceDataDefault::isDegenerate(void) const
  Returns true if the rational Bezier surface is degenerate : i.e. a line of control points coalesce, else returns false.
*/
bool dtkRationalBezierSurfaceDataDefault::isDegenerate(void) const
{
    dtkWarn() << "Does nothing";

    return false;
}

/*! \fn dtkRationalBezierSurfaceDataDefault::degenerateLocations(std::list< dtkContinuousGeometryPrimitives::Point_3 >& r_degenerate_locations) const
  Returns true if the rational Bezier surface is degenerate : i.e. a line of control points coalesce and adds in \a r_degenerate_locations the locations at which there are degeneracies, else returns false.
*/
bool dtkRationalBezierSurfaceDataDefault::degenerateLocations(std::list< dtkContinuousGeometryPrimitives::Point_3 >&) const
{
    dtkWarn() << "Does nothing";

    return false;
}

/*! \fn dtkRationalBezierSurfaceDataDefault::evaluatePoint(double p_u, double p_v, double *r_point) const
  Writes in \a r_point the evaluation at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_point : array of size 3 to store the result of the evaluation
*/
void dtkRationalBezierSurfaceDataDefault::evaluatePoint(double p_u, double p_v, double* r_point) const
{
    Point_2 p_point(p_u, p_v);
    Point_3 r_point_3(0., 0., 0.);
    d->surfacePoint(r_point_3, p_point);
    r_point[0] = r_point_3[0];
    r_point[1] = r_point_3[1];
    r_point[2] = r_point_3[2];
}

/*! \fn dtkRationalBezierSurfaceDataDefault::evaluateNormal(double p_u, double p_v, double *r_normal) const
  Writes in \a r_normal the normal evaluation at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_normal : array of size 3 to store the result of the normal evaluation
*/
void dtkRationalBezierSurfaceDataDefault::evaluateNormal(double p_u, double p_v, double* r_normal) const
{
    Point_2 p_point(p_u, p_v);

    Vector_3 r_vector_3(0., 0., 0.);
    d->surfaceNormal(r_vector_3, p_point);
    r_normal[0] = r_vector_3[0];
    r_normal[1] = r_vector_3[1];
    r_normal[2] = r_vector_3[2];
}

/*! \fn dtkRationalBezierSurfaceDataDefault::evaluate1stDer(double p_u, double p_v, double *r_point, double *r_u_deriv, double *r_v_deriv) const
  Writes in \a r_point the evaluation, in \a r_u_deriv the first derivative in "u" direction, in \a r_v_deriv the first derivative in the "v" direction at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_point : array of size 3 to store the result of the evaluation

  \a r_u_deriv : array of size 3 to store the result of the first derivative evaluation in the "u" direction

  \a r_v_deriv : array of size 3 to store the result of the first derivative evaluation in the "v" direction
*/
void dtkRationalBezierSurfaceDataDefault::evaluate1stDer(double p_u, double p_v, double* r_point, double* r_u_deriv, double* r_v_deriv) const
{
    dtkContinuousGeometryTools::dtkMatrixMap< Array_3 > first_partial_derivatives;

    Point_2 p_point(p_u, p_v);

    d->surfaceDeriv(first_partial_derivatives, p_point, 1);
    r_point[0] = first_partial_derivatives.at({0, 0})[0];
    r_point[1] = first_partial_derivatives.at({0, 0})[1];
    r_point[2] = first_partial_derivatives.at({0, 0})[2];

    r_u_deriv[0] = first_partial_derivatives.at({1, 0})[0];
    r_u_deriv[1] = first_partial_derivatives.at({1, 0})[1];
    r_u_deriv[2] = first_partial_derivatives.at({1, 0})[2];

    r_v_deriv[0] = first_partial_derivatives.at({0, 1})[0];
    r_v_deriv[1] = first_partial_derivatives.at({0, 1})[1];
    r_v_deriv[2] = first_partial_derivatives.at({0, 1})[2];
}

/*! \fn dtkRationalBezierSurfaceDataDefault::evaluate1stDer(double p_u, double p_v, double *r_u_deriv, double *r_v_deriv) const
  Writes in \a r_u_deriv the first derivative in "u" direction and in \a r_v_deriv the first derivative in the "v" direction at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_u_deriv : array of size 3 to store the result of the first derivative evaluation in the "u" direction

  \a r_v_deriv : array of size 3 to store the result of the first derivative evaluation in the "v" direction
*/
void dtkRationalBezierSurfaceDataDefault::evaluate1stDer(double p_u, double p_v, double* r_u_deriv, double* r_v_deriv) const
{
    dtkContinuousGeometryTools::dtkMatrixMap< Array_3 > first_partial_derivatives;

    Point_2 p_point(p_u, p_v);

    d->surfaceDeriv(first_partial_derivatives, p_point, 1);

    r_u_deriv[0] = first_partial_derivatives.at({1, 0})[0];
    r_u_deriv[1] = first_partial_derivatives.at({1, 0})[1];
    r_u_deriv[2] = first_partial_derivatives.at({1, 0})[2];

    r_v_deriv[0] = first_partial_derivatives.at({0, 1})[0];
    r_v_deriv[1] = first_partial_derivatives.at({0, 1})[1];
    r_v_deriv[2] = first_partial_derivatives.at({0, 1})[2];
}

/*! \fn dtkRationalBezierSurfaceDataDefault::evaluatePointBlossom(const double* p_uis, const double* p_vis, double *r_point, double *r_weight) const
  Stores in \a r_point and \a r_weight the result of the blossom evaluation at \a p_uis, \a p_vis values.

  \a p_uis : "u" values for blossom evaluation

  \a p_vis : "v" values for blossom evaluation

  \a r_point : array of size 3 to slore the point result of the blossom evaluation

  \a r_weight : array of size 1 to slore the weight result of the blossom evaluation
*/
void dtkRationalBezierSurfaceDataDefault::evaluatePointBlossom(const double*, const double*, double*, double*) const
{
    dtkFatal() << "Does nothing, not implemented";
}

/*! \fn dtkRationalBezierSurfaceDataDefault::split(dtkRationalBezierSurface *r_split_surface, double *p_splitting_parameters) const
  Creates \a r_split_surface as the part of rational Bezier surface lying inside the bounding box of parameters \a p_splitting_parameters.

  \a r_split_surface : pointer that will point to the newly created rationalBezierSurface which is the part of the initial surface restricted to the bouding box of parameters given in \a p_splitting_parameters

  \a p_splitting_parameters : array of size 4, storing the parameters defining the restriction in the parameter space of the rational Bezier surface, given as [u_0, v_0, u_1, v_1]
 */
void dtkRationalBezierSurfaceDataDefault::split(dtkRationalBezierSurface *, double *) const
{
    dtkFatal() << "Does nothing, not implemented";
}

/*! \fn dtkRationalBezierSurfaceDataDefault::aabb(double* r_aabb) const
  Writes in \a r_aabb the [xmin, ymin, zmin, xmax, ymax, zmax] coordinates.

  \a r_aabb : array of size 6 to store the limits coordinates
 */
void dtkRationalBezierSurfaceDataDefault::aabb(double* r_aabb) const
{
    d->aabb(r_aabb);
}

/*! \fn dtkRationalBezierSurfaceDataDefault::extendedAabb(double *r_aabb, double factor) const
  Writes in \a r_aabb the [xmin, ymin, zmin, xmax, ymax, zmax] coordinates of an extened bounding box of factor \a factor. The extension factor extends the diagonal.

  \a r_aabb : array of size 6 to store the extended limits coordinates

  \a factor : factor used to lengthen the diagonal of the axis aligned bounding box
*/
void dtkRationalBezierSurfaceDataDefault::extendedAabb(double*, double) const {
    dtkFatal() << "Does nothing, not implemented";
}

/*! \fn dtkRationalBezierSurfaceDataDefault::clone(void) const
   Clone.
*/
dtkRationalBezierSurfaceDataDefault* dtkRationalBezierSurfaceDataDefault::clone(void) const
{
    return new dtkRationalBezierSurfaceDataDefault(*this);
}

void dtkRationalBezierSurfaceDataDefault::print(std::ostream&) const
{
    dtkFatal() << "Not yet implemented";
}

//
// dtkRationalBezierSurfaceDataDefault.cpp ends here
