// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <QtCore>

#include <dtkContinuousGeometry.h>

#include <dtkAbstractRationalBezierSurfaceData.h>

class dtkRationalBezierSurfaceDataDefaultPlugin : public dtkAbstractRationalBezierSurfaceDataPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkAbstractRationalBezierSurfaceDataPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkRationalBezierSurfaceDataDefaultPlugin" FILE "dtkRationalBezierSurfaceDataDefaultPlugin.json")

public:
     dtkRationalBezierSurfaceDataDefaultPlugin(void) {}
    ~dtkRationalBezierSurfaceDataDefaultPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

//
// dtkRationalBezierSurfaceDataDefaultPlugin.h ends here
