// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkRationalBezierCurveLossLessDegreeReductor.h>

#include <defaultRationalBezierCurveLossLessDegreeReductorExport.h>

#include <dtkCore>

class DEFAULTRATIONALBEZIERCURVELOSSLESSDEGREEREDUCTOR_EXPORT defaultRationalBezierCurveLossLessDegreeReductorPlugin : public dtkRationalBezierCurveLossLessDegreeReductorPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkRationalBezierCurveLossLessDegreeReductorPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.defaultRationalBezierCurveLossLessDegreeReductorPlugin" FILE "defaultRationalBezierCurveLossLessDegreeReductorPlugin.json")

public:
     defaultRationalBezierCurveLossLessDegreeReductorPlugin(void) {}
    ~defaultRationalBezierCurveLossLessDegreeReductorPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

//
// defaultRationalBezierCurveLossLessDegreeReductorPlugin.h ends here
