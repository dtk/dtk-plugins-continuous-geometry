// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "defaultRationalBezierCurveLossLessDegreeReductor.h"
#include "defaultRationalBezierCurveLossLessDegreeReductorPlugin.h"

#include "dtkContinuousGeometry.h"

#include <dtkCore>

// ///////////////////////////////////////////////////////////////////
// defaultRationalBezierCurveLossLessDegreeReductorPlugin
// ///////////////////////////////////////////////////////////////////

void defaultRationalBezierCurveLossLessDegreeReductorPlugin::initialize(void)
{
    dtkContinuousGeometry::rationalBezierCurveLossLessDegreeReductor::pluginFactory().record("defaultRationalBezierCurveLossLessDegreeReductor", defaultRationalBezierCurveLossLessDegreeReductorCreator);
}

void defaultRationalBezierCurveLossLessDegreeReductorPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(defaultRationalBezierCurveLossLessDegreeReductor)

//
// defaultRationalBezierCurveLossLessDegreeReductorPlugin.cpp ends here
