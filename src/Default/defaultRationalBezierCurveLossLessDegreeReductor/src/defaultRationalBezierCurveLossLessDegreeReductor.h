// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkRationalBezierCurveLossLessDegreeReductor>

#include <defaultRationalBezierCurveLossLessDegreeReductorExport.h>

class defaultRationalBezierCurveLossLessDegreeReductorPrivate;

class dtkRationalBezierCurve;

class DEFAULTRATIONALBEZIERCURVELOSSLESSDEGREEREDUCTOR_EXPORT defaultRationalBezierCurveLossLessDegreeReductor : public dtkRationalBezierCurveLossLessDegreeReductor
{
public:
     defaultRationalBezierCurveLossLessDegreeReductor(void);
    ~defaultRationalBezierCurveLossLessDegreeReductor(void);

public:
    void setRationalBezierCurve(dtkRationalBezierCurve *) final;

    void run(void) final;

    dtkRationalBezierCurve *rationalBezierCurve(void) final;

protected:
    defaultRationalBezierCurveLossLessDegreeReductorPrivate *d;
};

// ///////////////////////////////////////////////////////////////////

inline dtkRationalBezierCurveLossLessDegreeReductor* defaultRationalBezierCurveLossLessDegreeReductorCreator(void)
{
    return new defaultRationalBezierCurveLossLessDegreeReductor();
}

//
// defaultRationalBezierCurveLossLessDegreeReductor.h ends here
