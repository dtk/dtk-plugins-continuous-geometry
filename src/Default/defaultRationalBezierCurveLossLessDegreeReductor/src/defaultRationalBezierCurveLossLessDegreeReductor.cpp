// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "defaultRationalBezierCurveLossLessDegreeReductor.h"

#include <dtkContinuousGeometry.h>

#include <dtkContinuousGeometryUtils>

#include <dtkAbstractRationalBezierCurveData>
#include <dtkRationalBezierCurve>

// /////////////////////////////////////////////////////////////////
// defaultRationalBezierCurveLossLessDegreeReductorPrivate
// /////////////////////////////////////////////////////////////////

class defaultRationalBezierCurveLossLessDegreeReductorPrivate
{
public:
    dtkRationalBezierCurve *in_bezier_curve;
    dtkRationalBezierCurve *out_bezier_curve;
};

// ///////////////////////////////////////////////////////////////////
// defaultRationalBezierCurveLossLessDegreeReductor implementation
// ///////////////////////////////////////////////////////////////////

defaultRationalBezierCurveLossLessDegreeReductor::defaultRationalBezierCurveLossLessDegreeReductor(void) : dtkRationalBezierCurveLossLessDegreeReductor(), d(new defaultRationalBezierCurveLossLessDegreeReductorPrivate)
{
    d->in_bezier_curve = nullptr;
    d->out_bezier_curve = nullptr;
}

defaultRationalBezierCurveLossLessDegreeReductor::~defaultRationalBezierCurveLossLessDegreeReductor(void)
{
    delete d;
    d = nullptr;
}

void defaultRationalBezierCurveLossLessDegreeReductor::setRationalBezierCurve(dtkRationalBezierCurve *bezier_curve)
{
    d->in_bezier_curve = bezier_curve;
    d->out_bezier_curve = nullptr;
}

void defaultRationalBezierCurveLossLessDegreeReductor::run(void)
{
    // ///////////////////////////////////////////////////////////////////
    // The algorithm is based on an idea from Akos Balazs presented in Efficient trimmed NURBS tessellation
    // This idea is itself based on the classical elevation algorithm
    // n = degree of C(u)
    // The curve to reduce is : C(u) = Sum(Qi * Bi(u)) : i = 0 -> n
    // And will be reduced to : R(u) = Sum(Pi * Bi(u)) : i = 0 -> (n - 1)
    // The extremities are to match so : Q0 = P0, Qn = P(n - 1)
    // The elevation rules gives : Qi = i / n * P(i - 1) + (1 - i / n) * Pi
    // To inverse the (n * n - 1) system (and recover the Pis), since the matrix is not square :
    // We compute once from the left the Pi and once from the right
    // From the left : Pi = n / (n - i) * Qi - i / (n - i) * P(i - 1) : i = 1 -> n - 1
    // From the right : P(i - 1) = n / i * Qi - (n - i) / i * Pi : i = n - 1 -> 0
    // Then we compare the distances (to tol = 1e-12), if they match the curve could indeed be reduced
    // left : from 0
    // right : from n - 1
    // Except we deal with rational Bezier curve, so we embed the 3D weighted point in 4D
    // ///////////////////////////////////////////////////////////////////
    double tol = 1e-10;
    std::size_t n = d->in_bezier_curve->degree();
    if(n <= 1) { return; }
    std::vector< dtkContinuousGeometryPrimitives::Point_4 > left_points;
    left_points.reserve(n);
    std::vector< dtkContinuousGeometryPrimitives::Point_4 > right_points;
    right_points.reserve(n);
    dtkContinuousGeometryPrimitives::Point_4 p(0., 0., 0., 0.);
    dtkContinuousGeometryPrimitives::Point_3 p_3d(0., 0., 0.);
    double w = 0.;
    d->in_bezier_curve->controlPoint(0, p_3d.data());
    d->in_bezier_curve->weight(0, &w);
    p[0] = p_3d[0] / w; p[1] = p_3d[1] / w; p[2] = p_3d[2] / w; p[3] = w;
    left_points[0] = p;
    dtkContinuousGeometryPrimitives::Point_3 qi_3d(0., 0., 0.);
    dtkContinuousGeometryPrimitives::Point_4 qi(0., 0., 0., 0.);
    for (std::size_t i = 1; i < n; ++i) {
        d->in_bezier_curve->controlPoint(i, qi_3d.data());
        d->in_bezier_curve->weight(0, &w);
        qi[0] = qi_3d[0] / w; qi[1] = qi_3d[1] / w; qi[2] = qi_3d[2] / w; qi[3] = w;
        left_points[i] = qi * (double(n) / (double(n) - double(i)))  - left_points[i - 1] * (double(i) / (double(n) - double(i)));
    }

    d->in_bezier_curve->controlPoint(n, p_3d.data());
    p[0] = p_3d[0] / w; p[1] = p_3d[1] / w; p[2] = p_3d[2] / w; p[3] = w;
    right_points[n - 1] = p;
    for (std::size_t i = n - 1; i > 0; --i) {
        d->in_bezier_curve->controlPoint(i, qi_3d.data());
        d->in_bezier_curve->weight(0, &w);
        qi[0] = qi_3d[0] / w; qi[1] = qi_3d[1] / w; qi[2] = qi_3d[2] / w; qi[3] = w;
        right_points[i - 1] = qi * (double(n) / double(i)) - right_points[i] * ((double(n) - double(i)) / double(i));
    }

    // ///////////////////////////////////////////////////////////////////
    // Invert right_points so they are rightly ordered
    // ///////////////////////////////////////////////////////////////////
    std::reverse(right_points.begin(), right_points.end());

    // ///////////////////////////////////////////////////////////////////
    // Verifies that both right_points and left_points match
    // ///////////////////////////////////////////////////////////////////
    bool lossless_reducable = true;
    dtkContinuousGeometryPrimitives::Point_3 r(0., 0., 0.);
    dtkContinuousGeometryPrimitives::Point_3 l(0., 0., 0.);
    for(std::size_t i = 0; i < n; ++i) {
        r[0] = right_points[i][0] * right_points[i][3];
        r[1] = right_points[i][1] * right_points[i][3];
        r[2] = right_points[i][2] * right_points[i][3];
        l[0] = left_points[i][0] * left_points[i][3];
        l[1] = left_points[i][1] * left_points[i][3];
        l[2] = left_points[i][2] * left_points[i][3];
        if (dtkContinuousGeometryTools::squaredDistance(r, l) > tol)  {
            lossless_reducable = false;
            break;
        }
    }
    if (lossless_reducable == true) {
        // ///////////////////////////////////////////////////////////////////
        // Creates the reduced curve based on the computed control points
        // ///////////////////////////////////////////////////////////////////
        dtkAbstractRationalBezierCurveData* out_curve_data = dtkContinuousGeometry::abstractRationalBezierCurveData::pluginFactory().create("dtkRationalBezierCurveDataOn");
        if (out_curve_data == nullptr) {
            dtkFatal() << "The plugin defaultRationalBezierCurveLossLessDegreeReductor relies at the moment on openNURBS implementation of dtkAbtractRationalBezierCurveData";
        }
        d->out_bezier_curve = new dtkRationalBezierCurve(out_curve_data);
        double *cps = new double[(3 + 1) * n];
        for(std::size_t i = 0; i < n; ++i) {
            cps[4 * i] = right_points[i][0] * right_points[i][3];
            cps[4 * i + 1] = right_points[i][1] * right_points[i][3];
            cps[4 * i + 2] = right_points[i][2] * right_points[i][3];
            cps[4 * i + 3] = right_points[i][3];
        }
        out_curve_data->create(n, cps);
    }
}

dtkRationalBezierCurve* defaultRationalBezierCurveLossLessDegreeReductor::rationalBezierCurve(void)
{
    return d->out_bezier_curve;
}

//
// defaultRationalBezierCurveLossLessDegreeReductor.cpp ends here
