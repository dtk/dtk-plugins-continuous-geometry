// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <QTest>

class defaultRationalBezierCurveLossLessDegreeReductorTestCasePrivate;

class defaultRationalBezierCurveLossLessDegreeReductorTestCase : public QObject
{
    Q_OBJECT

public:
    defaultRationalBezierCurveLossLessDegreeReductorTestCase(void);
    ~defaultRationalBezierCurveLossLessDegreeReductorTestCase(void);

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testRun(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);

private:
    defaultRationalBezierCurveLossLessDegreeReductorTestCasePrivate* d;
};

//
// defaultRationalBezierCurveLossLessDegreeReductorTest.h ends here
