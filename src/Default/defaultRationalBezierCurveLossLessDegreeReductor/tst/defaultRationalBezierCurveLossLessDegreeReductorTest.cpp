// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "defaultRationalBezierCurveLossLessDegreeReductorTest.h"

#include <defaultRationalBezierCurveLossLessDegreeReductor.h>

#include <dtkContinuousGeometry.h>
#include <dtkContinuousGeometrySettings.h>

#include <dtkAbstractRationalBezierCurveData>
#include <dtkRationalBezierCurve>

#include <dtkTest>

// ///////////////////////////////////////////////////////////////////
// D-Pointer implementation
// ///////////////////////////////////////////////////////////////////

class defaultRationalBezierCurveLossLessDegreeReductorTestCasePrivate{
public:
    dtkRationalBezierCurve *rational_bezier_curve;
    dtkAbstractRationalBezierCurveData *rational_bezier_curve_data;

    double tolerance = 1e-15;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

defaultRationalBezierCurveLossLessDegreeReductorTestCase::defaultRationalBezierCurveLossLessDegreeReductorTestCase(void):d(new defaultRationalBezierCurveLossLessDegreeReductorTestCasePrivate())
{
    dtkLogger::instance().attachConsole();
    dtkLogger::instance().setLevel(dtk::LogLevel::Trace);
    dtkContinuousGeometrySettings settings;
    settings.beginGroup("continuous-geometry");
    dtkContinuousGeometry::initialize(settings.value("plugins").toString());
    settings.endGroup();
}

defaultRationalBezierCurveLossLessDegreeReductorTestCase::~defaultRationalBezierCurveLossLessDegreeReductorTestCase(void)
{

}

void defaultRationalBezierCurveLossLessDegreeReductorTestCase::initTestCase(void)
{
    d->rational_bezier_curve_data = dtkContinuousGeometry::abstractRationalBezierCurveData::pluginFactory().create("dtkRationalBezierCurveDataOn");
    if (d->rational_bezier_curve_data == nullptr) {
        dtkFatal() << "The dtkAbstractRationalBezierCurveData could not be loaded by the factory under the openNURBS implementation";
    }
    d->rational_bezier_curve = new dtkRationalBezierCurve(d->rational_bezier_curve_data);

    //degree 3
    std::size_t order = 4;
    double* cps = new double[(3 + 1) * order];
    cps[0] = 0.;  cps[1] = 0.;  cps[2] = 0.; cps[3] = 1.;
    cps[4] = 1.;  cps[5] = 0.;  cps[6] = 0.; cps[7] = 1.;
    cps[8] = 2.;  cps[9] = 0.;  cps[10] = 0.; cps[11] = 1.;
    cps[12] = 3.; cps[13] = 0.; cps[14] = 0.; cps[15] = 1.;

    d->rational_bezier_curve->create(order, cps);
}

void defaultRationalBezierCurveLossLessDegreeReductorTestCase::init(void)
{

}


void defaultRationalBezierCurveLossLessDegreeReductorTestCase::testRun(void)
{
    defaultRationalBezierCurveLossLessDegreeReductor reductor = defaultRationalBezierCurveLossLessDegreeReductor();
    reductor.setRationalBezierCurve(d->rational_bezier_curve);

    reductor.run();

    dtkRationalBezierCurve *out_curve = reductor.rationalBezierCurve();

    QVERIFY(out_curve != nullptr);
    QVERIFY(out_curve->degree() == 2);

    reductor.setRationalBezierCurve(out_curve);

    reductor.run();

    dtkRationalBezierCurve *out_curve_1 = reductor.rationalBezierCurve();

    QVERIFY(out_curve_1 != nullptr);
    QVERIFY(out_curve_1->degree() == 1);

    reductor.setRationalBezierCurve(out_curve_1);

    reductor.run();

    dtkRationalBezierCurve *out_curve_2 = reductor.rationalBezierCurve();
    QVERIFY(out_curve_2 == nullptr);
}


void defaultRationalBezierCurveLossLessDegreeReductorTestCase::cleanup(void)
{

}

void defaultRationalBezierCurveLossLessDegreeReductorTestCase::cleanupTestCase(void)
{
    delete d->rational_bezier_curve;
}

DTKTEST_MAIN_NOGUI(defaultRationalBezierCurveLossLessDegreeReductorTest, defaultRationalBezierCurveLossLessDegreeReductorTestCase)

//
// defaultRationalBezierCurveLossLessDegreeReductorTest.cpp ends here
