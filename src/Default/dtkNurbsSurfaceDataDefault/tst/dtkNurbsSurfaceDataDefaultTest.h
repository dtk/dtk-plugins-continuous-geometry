// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include "dtkNurbsSurfaceDataDefaultTests.h"

class dtkNurbsSurfaceDataDefaultTestCasePrivate;

class dtkNurbsSurfaceDataDefaultTestCase : public QObject
{
    Q_OBJECT

public:
    dtkNurbsSurfaceDataDefaultTestCase(void);
    ~dtkNurbsSurfaceDataDefaultTestCase(void);

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testUNbCps(void);
    void testVNbCps(void);
    void testUDegree(void);
    void testVDegree(void);
    void testControlPoint(void);
    void testWeight(void);
    void testUKnots(void);
    void testVKnots(void);
    void testSurfacePoint(void);
    void testSurfaceNormal(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);

private:
    dtkNurbsSurfaceDataDefaultTestCasePrivate* d;
};

//
// dtkNurbsSurfaceDataDefaultTest.h ends here
