// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtTest>

#define DTKNURBSSURFACEDATADEFAULTTEST_MAIN(TestMain, TestObject)	        \
    int TestMain(int argc, char *argv[])			\
    {                                               \
        QApplication app(argc, argv);               \
        TestObject tc;                              \
        return QTest::qExec(&tc, argc, argv);		\
    }

#define DTKNURBSSURFACEDATADEFAULTTEST_MAIN_NOGUI(TestMain, TestObject)	\
    int TestMain(int argc, char *argv[])			\
    {                                               \
        QCoreApplication app(argc, argv);           \
        TestObject tc;                              \
        return QTest::qExec(&tc, argc, argv);       \
    }

//
// dtkNurbsSurfaceDataDefaultTests.h ends here
