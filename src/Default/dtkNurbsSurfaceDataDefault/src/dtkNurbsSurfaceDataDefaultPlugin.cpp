// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#include "dtkNurbsSurfaceDataDefault.h"
#include "dtkNurbsSurfaceDataDefaultPlugin.h"

#include <dtkCore>
#include <dtkContinuousGeometry>

// ///////////////////////////////////////////////////////////////////
// dtkNurbsSurfaceDataDefaultPlugin
// ///////////////////////////////////////////////////////////////////

void dtkNurbsSurfaceDataDefaultPlugin::initialize(void)
{
    dtkContinuousGeometry::abstractNurbsSurfaceData::pluginFactory().record("dtkNurbsSurfaceDataDefault", dtkNurbsSurfaceDataDefaultCreator);
}

void dtkNurbsSurfaceDataDefaultPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkNurbsSurfaceDataDefault)

//
// dtkNurbsSurfaceDataDefaultPlugin.cpp ends here
