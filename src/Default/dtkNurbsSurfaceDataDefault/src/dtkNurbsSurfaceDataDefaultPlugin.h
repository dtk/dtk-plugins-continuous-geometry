// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <QtCore>

#include <dtkContinuousGeometry.h>

#include <dtkAbstractNurbsSurfaceData.h>

class dtkNurbsSurfaceDataDefaultPlugin : public dtkAbstractNurbsSurfaceDataPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkAbstractNurbsSurfaceDataPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkNurbsSurfaceDataDefaultPlugin" FILE "dtkNurbsSurfaceDataDefaultPlugin.json")

public:
     dtkNurbsSurfaceDataDefaultPlugin(void) {}
    ~dtkNurbsSurfaceDataDefaultPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

//
// dtkNurbsSurfaceDataDefaultPlugin.h ends here
