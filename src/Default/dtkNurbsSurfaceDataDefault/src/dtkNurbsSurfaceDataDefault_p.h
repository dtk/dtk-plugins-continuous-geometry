// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <vector>

// ///////////////////////////////////////////////////////////////////
// dtkNurbsSurfaceDataDefaultPrivate
// ///////////////////////////////////////////////////////////////////
#include <dtkContinuousGeometryUtils>

class dtkRationalBezierSurface;
class dtkTrimLoop;
namespace dtkContinuousGeometryPrimitives {
    class Point_2;
    class Array_3;
    class Point_3;
    class Vector_3;
}

class dtkNurbsSurfaceDataDefaultPrivate
{
 private:
    typedef dtkContinuousGeometryPrimitives::Point_2 Point_2;

    typedef dtkContinuousGeometryPrimitives::Array_3 Array_3;
    typedef dtkContinuousGeometryPrimitives::Point_3 Point_3;
    typedef dtkContinuousGeometryPrimitives::Vector_3 Vector_3;

    template< typename K > using dtkMatrixMap = dtkContinuousGeometryTools::dtkMatrixMap< K >;

public:
    dtkNurbsSurfaceDataDefaultPrivate();
    ~dtkNurbsSurfaceDataDefaultPrivate();

 public:
    std::size_t uDegree(void) const;
    std::size_t vDegree(void) const;

    std::size_t uNbCps(void) const;
    std::size_t vNbCps(void) const;

    std::size_t dim(void) const;

    double weight(std::size_t i, std::size_t j) const;
    const Point_3& controlPoint(std::size_t i, std::size_t j) const;

    const std::vector< double >& uKnots(void) const;
    const std::vector< double >& vKnots(void) const;

    void surfacePoint(Point_3& r_point, const Point_2& p_point) const;
    Point_3 surfacePoint(const Point_2& p_point) const;

    void surfaceUDeriv(Vector_3& r_vector, const Point_2& p_point) const;
    Vector_3 surfaceUDeriv(const Point_2& p_point) const;

    void surfaceVDeriv(Vector_3& r_vector, const Point_2& p_point) const;
    Vector_3 surfaceVDeriv(const Point_2& p_point) const;

    void surfaceNormal(Vector_3& r_vector, const Point_2& p_point) const;
    Vector_3 surfaceNormal(const Point_2& p_point) const;

    void surfaceDeriv(Vector_3& r_point, const Point_2& p_point, std::size_t n_u, std::size_t n_v) const;
    Vector_3 surfaceDeriv(const Point_2& p_point, std::size_t n_u, std::size_t n_v) const;

    void surfaceDeriv(dtkMatrixMap< Array_3 >& r_derivatives, const Point_2& p_point, std::size_t n_d) const;
    dtkMatrixMap< Array_3 >  surfaceDeriv(const Point_2& p_point, std::size_t n_d) const;

    void decomposeSurface(std::vector< dtkRationalBezierSurface* >& bezier_patches) const;

public:
    std::size_t findSpan(const std::vector< double >& knots, std::size_t degree, double a) const;
    std::size_t findUSpan(double u) const;
    std::size_t findVSpan(double v) const;

    void basisFuns(std::vector< double >& basis_functions, std::size_t i, double a, std::size_t degree, const std::vector< double >& knots) const;
    void uBasisFuns(std::vector< double >& u_basis_functions, std::size_t i, double u) const;
    void vBasisFuns(std::vector< double >& v_basis_functions, std::size_t i, double v) const;

    void derBasisFuns(dtkMatrixMap < double >& der_basis_functions, std::size_t i, std::size_t n, double a, std::size_t degree, const std::vector< double >& knots) const;
    void uDersBasisFuns(dtkMatrixMap< double >& der_u_basis_functions, std::size_t i, std::size_t n, double u) const;
    void vDersBasisFuns(dtkMatrixMap< double >& der_v_basis_functions, std::size_t i, std::size_t n, double v) const;

    void surfaceDerivsAlg1(dtkMatrixMap< Array_3 >& aders, dtkMatrixMap< double >& wders, double u, double v, std::size_t degree) const;

    void ratSurfaceDerivs(dtkMatrixMap< Array_3 >& pders, dtkMatrixMap< Array_3 >& aders, dtkMatrixMap< double >& wders, std::size_t degree) const;

    double Bin(std::size_t n, std::size_t k) const;

public:
    mutable dtkMatrixMap< Point_3 > m_control_points;
    std::size_t m_u_control_points_max_id;
    std::size_t m_v_control_points_max_id;

    mutable dtkMatrixMap< double > m_weights;
    std::size_t m_u_weights_count;
    std::size_t m_v_weights_count;

    std::vector< double > m_u_knots;
    std::vector< double > m_v_knots;

    mutable dtkMatrixMap< double > m_binomial_coefficients;
    mutable bool m_computed_binomial_coefficients;
};

//
// dtkNurbsSurfaceDataDefault_p.h ends here
