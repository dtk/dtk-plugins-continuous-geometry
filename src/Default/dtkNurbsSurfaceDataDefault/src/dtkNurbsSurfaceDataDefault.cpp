// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkNurbsSurfaceDataDefault.h"

#include "dtkNurbsSurfaceDataDefault_p.h"

#include <dtkContinuousGeometryUtils>
#include <dtkRationalBezierSurface>
#include <dtkTrimLoop>

/*!
  \class dtkNurbsSurfaceDataDefault
  \inmodule dtkPluginsContinuousGeometry
  \brief dtkNurbsSurfaceDataDefault is the default implementation of the concept dtkAbstractNurbsSurfaceData.

  The following lines of code show how to instanciate and use the class in pratice:
  \code
  dtkAbstractNurbsSurfaceData *nurbs_surface_data = dtkContinuousGeometry::abstractNurbsSurfaceData::pluginFactory().create("dtkNurbsSurfaceDataDefault");
  dtkNurbsSurface *nurbs_surface = new dtkNurbsSurface(nurbs_surface_data);
  nurbs_surface->create(...);
  \endcode
*/

/*! \fn dtkNurbsSurfaceDataDefault::dtkNurbsSurfaceDataDefault(void)
  Instanciates the structure.

  To add the content, see \l create(std::size_t dim, std::size_t nb_cp_u, std::size_t nb_cp_v, std::size_t order_u, std::size_t order_v, const double *knots_u, const double *knots_v, const double *cps) const.
*/
dtkNurbsSurfaceDataDefault::dtkNurbsSurfaceDataDefault(void) : d(new dtkNurbsSurfaceDataDefaultPrivate)
{
}

/*! \fn dtkNurbsSurfaceDataDefault::~dtkNurbsSurfaceDataDefault(void)
  Destroys the instance.
*/
dtkNurbsSurfaceDataDefault::~dtkNurbsSurfaceDataDefault(void)
{
    delete d;
}

/*! \fn void dtkNurbsSurfaceDataDefault::create(std::size_t dim, std::size_t nb_cp_u, std::size_t nb_cp_v, std::size_t order_u, std::size_t order_v, const double *knots_u, const double *knots_v, const double *cps) const
  Creates a NURBS surface by providing the required content.

  \a dim : dimension of the space in which lies the surface

  \a nb_cp_u : number of control points along the "u" direction

  \a nb_cp_v : number of control points along the "v" direction

  \a order_u : order(degree + 1) in the "u" direction

  \a order_v : order(degree + 1) in the "v" direction

  \a knots_u : array containing the knots in the "u" direction

  \a knots_v : array containing the knots in the "v" direction

  \a cps : array containing the weighted control points, secified as : [x_00, y_00, z_00, w_00, x_01, y_01, ...]

  \a knots_u, \a knots_v, and \a cps are copied.
*/
void dtkNurbsSurfaceDataDefault::create(std::size_t dim, std::size_t nb_cp_u, std::size_t nb_cp_v, std::size_t order_u, std::size_t order_v, const double *knots_u, const double *knots_v, const double *cps) const
{
    // /////////////////////////////////////////////////////////////////
    // Initializes the map of control points and weights
    // /////////////////////////////////////////////////////////////////
    for (std::size_t i = 0; i < nb_cp_u; ++i) {
        for (std::size_t j = 0; j < nb_cp_v; ++j) {
            std::size_t pos = (dim + 1) * (i * nb_cp_v + j);
            Point_3 control_point_3(0., 0., 0.);
            control_point_3[0] = cps[pos];
            control_point_3[1] = cps[pos + 1];
            control_point_3[2] = cps[pos + 2];

            d->m_control_points.insert({{i, j}, control_point_3});
            d->m_weights.insert({{i, j}, cps[pos + 3]});
        }
    }
    d->m_u_control_points_max_id = nb_cp_u - 1;
    d->m_v_control_points_max_id = nb_cp_v - 1;
    d->m_u_weights_count = nb_cp_u;
    d->m_v_weights_count = nb_cp_v;

    // /////////////////////////////////////////////////////////////////
    // Initializes the vectors of knots (with the "superflous" knots)
    // /////////////////////////////////////////////////////////////////
    std::size_t nb_of_knots_u = nb_cp_u + order_u;
    std::size_t nb_of_knots_v = nb_cp_v + order_v;

    d->m_u_knots.push_back(knots_u[0]);
    for (std::size_t i = 0; i < nb_of_knots_u - 2; ++i) {
        d->m_u_knots.push_back(knots_u[i]);
    }
    d->m_u_knots.push_back(knots_u[nb_of_knots_u - 3]);

    d->m_v_knots.push_back(knots_v[0]);
    for (std::size_t i = 0; i < nb_of_knots_v - 2; ++i) {
        d->m_v_knots.push_back(knots_v[i]);
    }
    d->m_v_knots.push_back(knots_v[nb_of_knots_v - 3]);
}

/*! \fn void dtkNurbsSurfaceDataDefault::create(std::string path) const
  Creates a NURBS surface by providing a path to a file storing the required information to create a NURBS surface.

  \a path : a valid path to the file containing the information regarding a given NURBS surface
*/
void dtkNurbsSurfaceDataDefault::create(std::string) const
{
    dtkFatal() << "Does nothing, not implemented";
}

/*! \fn dtkNurbsSurfaceDataDefault::uDegree(void) const
  Returns the degree of in the "u" direction.
*/
std::size_t dtkNurbsSurfaceDataDefault::uDegree(void) const
{
    return d->uDegree();
}

/*! \fn dtkNurbsSurfaceDataDefault::vDegree(void) const
  Returns the degree of in the "v" direction.
*/
std::size_t dtkNurbsSurfaceDataDefault::vDegree(void) const
{
    return d->vDegree();
}

/*! \fn dtkNurbsSurfaceDataDefault::uNbCps(void) const
  Returns the number of weighted control points in the "u" direction.
*/
std::size_t dtkNurbsSurfaceDataDefault::uNbCps(void) const
{
    return d->uNbCps();
}

/*! \fn dtkNurbsSurfaceDataDefault::vNbCps(void) const
  Returns the number of weighted control points in the "v" direction.
*/
std::size_t dtkNurbsSurfaceDataDefault::vNbCps(void) const
{
    return d->vNbCps();
}

std::size_t dtkNurbsSurfaceDataDefault::uNbKnots(void) const
{
    return (d->uNbCps() + d->uDegree() - 1);
}

std::size_t dtkNurbsSurfaceDataDefault::vNbKnots(void) const
{
    return (d->vNbCps() + d->vDegree() - 1);
}

/*! \fn dtkNurbsSurfaceDataDefault::dim(void) const
  Returns the dimension of the space in which the NURBS surface lies.
*/
std::size_t dtkNurbsSurfaceDataDefault::dim(void) const
{
    return d->dim();
}

/*! \fn dtkNurbsSurfaceDataDefault::controlPoint(std::size_t i, std::size_t j, double *r_cp) const
  Writes in \a r_cp the \a i th, \a j th weighted control point coordinates[xij, yij, zij].

  \a i : index of the weighted control point along the "u" direction

  \a j : index of the weighted control point along the "v" direction

  \a r_cp : array of size 3 to store the weighted control point coordinates[xij, yij, zij]
*/
void dtkNurbsSurfaceDataDefault::controlPoint(std::size_t i, std::size_t j, double* r_cp) const
{
    Point_3 t_cp(0., 0., 0.);
    t_cp = d->controlPoint(i, j);
    r_cp[0] = t_cp[0];
    r_cp[1] = t_cp[1];
    r_cp[2] = t_cp[2];
}

/*! \fn dtkNurbsSurfaceDataDefault::weight(std::size_t i, std::size_t j, double *r_w) const
  Writes in \a r_w the \a i th, \a j th weighted control point weight wij.

  \a i : index of the weighted control point along the "u" direction

  \a j : index of the weighted control point along the "v" direction

  \a r_w : array of size 1 to store the weighted control point weight wij
*/
void dtkNurbsSurfaceDataDefault::weight(std::size_t i, std::size_t j, double* r_w) const
{
    *r_w = d->weight(i, j);
}

/*! \fn dtkNurbsSurfaceDataDefault::uKnots(double *r_u_knots) const
  Writes in \a r_u_knots the knots of the curve along the "u" direction.

  \a r_u_knots : array of size (nb_cp_u + order_u - 2) to store the knots
*/
void dtkNurbsSurfaceDataDefault::uKnots(double* r_u_knots) const
{
    std::size_t nb_of_knots_u = d->uNbCps() + d->uDegree() - 1;
    for (std::size_t i = 0; i < nb_of_knots_u; ++i) {
        r_u_knots[i] = d->m_u_knots[i + 1];
    }
}

/*! \fn dtkNurbsSurfaceDataDefault::vKnots(double *r_v_knots) const
  Writes in \a r_v_knots the knots of the curve along the "v" direction.

  \a r_v_knots : array of size (nb_cp_v + order_v - 2) to store the knots
*/
void dtkNurbsSurfaceDataDefault::vKnots(double* r_v_knots) const
{
    std::size_t nb_of_knots_v = d->vNbCps() + d->vDegree() - 1;
    for (std::size_t i = 0; i < nb_of_knots_v; ++i) {
        r_v_knots[i] = d->m_v_knots[i + 1];
    }
}

/*! \fn dtkNurbsSurfaceDataDefault::uKnots(void) const
  Returns a pointer to the array storing the knots of the surface along the "u" direction. The array is of size : (nb_cp_u + order_u - 2).
*/
const double *dtkNurbsSurfaceDataDefault::uKnots(void) const
{
    return &d->m_u_knots.data()[1];
}

/*! \fn dtkNurbsSurfaceDataDefault::vKnots(void) const
  Returns a pointer to the array storing the knots of the surface along the "v" direction. The array is of size : (nb_cp_v + order_v - 2).
*/
const double *dtkNurbsSurfaceDataDefault::vKnots(void) const
{
    return &d->m_v_knots.data()[1];
}

double dtkNurbsSurfaceDataDefault::uPeriod(void) const
{
    // do nothing
    return 0.;
}

double dtkNurbsSurfaceDataDefault::vPeriod(void) const
{
    // do nothing
    return 0.;
}

/*! \fn dtkNurbsSurfaceDataDefault::isPointCulled(const dtkContinuousGeometryPrimitives::Point_2& point) const
  Returns true if \a point is culled by the trim loops of the surface, else returns false.
*/
bool dtkNurbsSurfaceDataDefault::isPointCulled(const dtkContinuousGeometryPrimitives::Point_2&) const
{
    dtkWarn() << "Does nothing atm";
    return false;
}

/*! \fn dtkNurbsSurfaceDataDefault::evaluatePoint(double p_u, double p_v, double *r_point) const
  Writes in \a r_point the evaluation at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_point : array of size 3 to store the result of the evaluation
*/
void dtkNurbsSurfaceDataDefault::evaluatePoint(double p_u, double p_v, double* r_point) const
{
    Point_2 p_point(p_u, p_v);
    Point_3 r_point_3(0., 0., 0.);
    d->surfacePoint(r_point_3, p_point);
    r_point[0] = r_point_3[0];
    r_point[1] = r_point_3[1];
    r_point[2] = r_point_3[2];
}

/*! \fn dtkNurbsSurfaceDataDefault::evaluateNormal(double p_u, double p_v, double *r_normal) const
  Writes in \a r_normal the normal evaluation at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_normal : array of size 3 to store the result of the normal evaluation
*/
void dtkNurbsSurfaceDataDefault::evaluateNormal(double p_u, double p_v, double* r_normal) const
{
    Point_2 p_point(p_u, p_v);
    Vector_3 r_vector_3(0., 0., 0.);
    d->surfaceNormal(r_vector_3, p_point);
    r_normal[0] = r_vector_3[0];
    r_normal[1] = r_vector_3[1];
    r_normal[2] = r_vector_3[2];
}

/*! \fn dtkNurbsSurfaceDataDefault::evaluate1stDer(double p_u, double p_v, double *r_point, double *r_u_deriv, double *r_v_deriv) const
  Writes in \a r_point the evaluation, in \a r_u_deriv the first derivative in "u" direction, in \a r_v_deriv the first derivative in the "v" direction at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_point : array of size 3 to store the result of the evaluation

  \a r_u_deriv : array of size 3 to store the result of the first derivative evaluation in the "u" direction

  \a r_v_deriv : array of size 3 to store the result of the first derivative evaluation in the "v" direction
*/
void dtkNurbsSurfaceDataDefault::evaluate1stDer(double p_u, double p_v, double* r_point, double* r_u_deriv, double* r_v_deriv) const
{
    dtkContinuousGeometryTools::dtkMatrixMap< Array_3 > first_partial_derivatives;
    Point_2 p_point(p_u, p_v);
    d->surfaceDeriv(first_partial_derivatives, p_point, 1);
    r_point[0] = first_partial_derivatives.at({0, 0})[0];
    r_point[1] = first_partial_derivatives.at({0, 0})[1];
    r_point[2] = first_partial_derivatives.at({0, 0})[2];

    r_u_deriv[0] = first_partial_derivatives.at({1, 0})[0];
    r_u_deriv[1] = first_partial_derivatives.at({1, 0})[1];
    r_u_deriv[2] = first_partial_derivatives.at({1, 0})[2];

    r_v_deriv[0] = first_partial_derivatives.at({0, 1})[0];
    r_v_deriv[1] = first_partial_derivatives.at({0, 1})[1];
    r_v_deriv[2] = first_partial_derivatives.at({0, 1})[2];
}

/*! \fn dtkNurbsSurfaceDataDefault::evaluate1stDer(double p_u, double p_v, double *r_u_deriv, double *r_v_deriv) const
  Writes in \a r_u_deriv the first derivative in "u" direction and in \a r_v_deriv the first derivative in the "v" direction at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_u_deriv : array of size 3 to store the result of the first derivative evaluation in the "u" direction

  \a r_v_deriv : array of size 3 to store the result of the first derivative evaluation in the "v" direction
*/
void dtkNurbsSurfaceDataDefault::evaluate1stDer(double p_u, double p_v, double* r_u_deriv, double* r_v_deriv) const
{
    dtkContinuousGeometryTools::dtkMatrixMap< Array_3 > first_partial_derivatives;
    Point_2 p_point(p_u, p_v);
    d->surfaceDeriv(first_partial_derivatives, p_point, 1);

    r_u_deriv[0] = first_partial_derivatives.at({1, 0})[0];
    r_u_deriv[1] = first_partial_derivatives.at({1, 0})[1];
    r_u_deriv[2] = first_partial_derivatives.at({1, 0})[2];

    r_v_deriv[0] = first_partial_derivatives.at({0, 1})[0];
    r_v_deriv[1] = first_partial_derivatives.at({0, 1})[1];
    r_v_deriv[2] = first_partial_derivatives.at({0, 1})[2];
}

/*! \fn dtkNurbsSurfaceDataDefault::decomposeToRationalBezierSurfaces(std::vector< std::pair< dtkRationalBezierSurface *, double * > >& r_rational_bezier_surfaces) const
  Decomposes the NURBS surface into dtkRationalBezierSurface \a r_rational_bezier_surfaces.

  \a r_rational_bezier_surfaces : vector in which the dtkRationalBezierSurface are stored, along with their limits in the NURBS surface parameter space

  Not sure what happens when the not extreme nodes are of multiplicity >1.
*/
void dtkNurbsSurfaceDataDefault::decomposeToRationalBezierSurfaces(std::vector< std::pair < dtkRationalBezierSurface*, double* > >&) const
{
    dtkFatal() << "Does nothing, not implemented";
}

/*! \fn dtkNurbsSurfaceDataDefault::aabb(double* r_aabb) const
  Writes in \a r_aabb the [xmin, ymin, zmin, xmax, ymax, zmax] coordinates.

  \a r_aabb : array of size 6 to store the limits coordinates
*/
void dtkNurbsSurfaceDataDefault::aabb(double*) const
{
    dtkFatal() << "Does nothing, not implemented";
}

/*! \fn dtkNurbsSurfaceDataDefault::extendedAabb(double *r_aabb, double factor) const
  Writes in \a r_aabb the [xmin, ymin, zmin, xmax, ymax, zmax] coordinates of an extened bounding box of factor \a factor. The extension factor extends the diagonal.

  \a r_aabb : array of size 6 to store the extended limits coordinates

  \a factor : factor used to lengthen the diagonal of the axis aligned bounding box
*/
void dtkNurbsSurfaceDataDefault::extendedAabb(double*, double) const
{
    dtkFatal() << "Does nothing, not implemented";
}

/*! \fn dtkNurbsSurfaceDataDefault::clone(void) const
  Clone
*/
dtkNurbsSurfaceDataDefault* dtkNurbsSurfaceDataDefault::clone(void) const
{
    return new dtkNurbsSurfaceDataDefault(*this);
}

void dtkNurbsSurfaceDataDefault::print(std::ostream&) const
{
    dtkFatal() << "Does nothing, not implemented";
}

//
// dtkNurbsSurfaceDataDefault.cpp ends here
