// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#include "dtkTrimLoopDataNp.h"
#include "dtkTrimLoopDataNpPlugin.h"

#include <dtkCore>
#include <dtkContinuousGeometry>

// ///////////////////////////////////////////////////////////////////
// dtkTrimLoopDataNpPlugin
// ///////////////////////////////////////////////////////////////////

void dtkTrimLoopDataNpPlugin::initialize(void)
{
    dtkContinuousGeometry::abstractTrimLoopData::pluginFactory().record("dtkTrimLoopDataNp", dtkTrimLoopDataNpCreator);
}

void dtkTrimLoopDataNpPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkTrimLoopDataNp)

//
// dtkTrimLoopDataNpPlugin.cpp ends here
