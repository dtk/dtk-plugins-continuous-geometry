// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkTrimLoopDataNp.h"

#include <dtkContinuousGeometryUtils>
#include <dtkNurbsCurve2D>
#include <dtkTrim>

#include <CGAL/Profile_counter.h>

#include <array>

/*!
  \class dtkTrimLoopDataNp
  \inmodule dtkPluginsContinuousGeometry
  \brief dtkTrimLoopDataNp is a dtkNurbsProbing implementation of the concept dtkAbstracTrimLoopData.

  The following lines of code show how to instanciate and use the class in pratice:
  \code
  dtkAbstracTrimLoopData *trim_loop_data = dtkContinuousGeometry::abstractTrimLoopData::pluginFactory().create("dtkTrimLoopDataNp");
  dtkTrimLoop *trim_loop = new dtkTrimLoop(trim_loop);
  trim_loop->trims().push_back(...);
  trim_loop->setType(...);
  \endcode
*/

/*! \fn dtkTrimLoopDataNp::dtkTrimLoopDataNp(void)
   Instanciates an empty class.
*/
dtkTrimLoopDataNp::dtkTrimLoopDataNp(void) : m_initialized(false) {}

/*! \fn dtkTrimLoopDataNp::~dtkTrimLoopDataNp(void)
  Destroys the instance.
*/
dtkTrimLoopDataNp::~dtkTrimLoopDataNp(void)
{
  for(auto trim : m_trims)
  {
    delete trim;
  }
}

/*! \fn dtkTrimLoopDataNp::type(void) const
  Returns the type of the trim loop, it can either be dtkContinuousGeometryEnums::TrimLoopType::outer, dtkContinuousGeometryEnums::TrimLoopType::inner or dtkContinuousGeometryEnums::TrimLoopType::unknown.
 */
void dtkTrimLoopDataNp::setType(dtkContinuousGeometryEnums::TrimLoopType p_type) const
{
    m_type = p_type;
}

/*! \fn dtkTrimLoopDataNp::setType(dtkContinuousGeometryEnums::TrimLoopType p_type) const
  Sets the type of the trim loop.

  \a p_type : type of the trim loop, it can be dtkContinuousGeometryEnums::TrimLoopType::outer, dtkContinuousGeometryEnums::TrimLoopType::inner or dtkContinuousGeometryEnums::TrimLoopType::unknown
*/
dtkContinuousGeometryEnums::TrimLoopType dtkTrimLoopDataNp::type(void) const
{
    return m_type;
}

/*! \fn dtkTrimLoopDataNp::isPointCulled(dtkContinuousGeometryPrimitives::Point_2 p_point) const
  Returns true if \a p_point is culled by the trim loop.

  \a p_point : point to check if culled or not
*/
bool dtkTrimLoopDataNp::isPointCulled(dtkContinuousGeometryPrimitives::Point_2 p_point) const
{
    if (m_initialized == false) {
        std::lock_guard<std::mutex> guard{mutex};
        if(m_initialized == false) {
            for (auto trim = m_trims.begin(); trim != m_trims.end(); ++trim) {
                //m_mrep_intersects.emplace_back((*trim)->curve2D());
                m_intersects.emplace_back((*trim)->curve2D());
            }
            m_initialized = true;
        }
    }
    constexpr std::size_t nb_rays = 3; /// @TODO: yet another arbitrary constant

    std::array<double,4> bbox;

    this->aabb(&bbox[0]);

    // extend bounding box a bit to avoid a segment intersected at boundaries exactly
    std::array<double,6> bbox_3 = {
      bbox[0] - (bbox[2] - bbox[0])*0.1,    bbox[1] - (bbox[3] - bbox[1])*0.1,   0.,
      bbox[2] + (bbox[2] - bbox[0])*0.1,  bbox[3] + (bbox[3] - bbox[1])*0.1, 0.};

    // ///////////////////////////////////////////////////////////////////
    // Checks at first if the point is included in the bounding box :
    // if not, returnsn outside of domain
    // if yes, checks that a vertical ray shot from the point intersects an even number of times the domain :
    //     if yes, returns outside of domain
    //     if not, returns inside of domain
    // ///////////////////////////////////////////////////////////////////
    //TODO modify for aabb check
    // if (false) {
    //         return true;
    // } else {
        std::size_t in = 0;
        std::size_t out = 0;

        for (std::size_t i = 0; i < nb_rays; ++i) {
            double vx = ((std::rand() / (double)RAND_MAX) - 0.5) * 2;
            double vy = ((std::rand() / (double)RAND_MAX) - 0.5) * 2;
            dtkContinuousGeometryPrimitives::Vector_2 v(vx, vy);
            dtkContinuousGeometryPrimitives::Ray_2 ray(p_point, v);

            std::list< dtkContinuousGeometryPrimitives::IntersectionObject_2> intersections;
            /*
            for (auto it = m_mrep_intersects.begin(); it != m_mrep_intersects.end(); ++it) {
                // ///////////////////////////////////////////////////////////////////
                // Checks if the ray "ray" intersects the trim boundingbox, if yes
                // if not returns false and returns Subdomain()
                // ///////////////////////////////////////////////////////////////////

                //     (*it)->aabb(&mrep_box[0]);
                //     if (dtkNurbsProbingGeometryTools::isIntersected(&mrep_box[0], r_s)) {
                it->intersect(intersections, ray);
                //     }
            }
            */

            // clip the ray to a segment by the bbox
            dtkContinuousGeometryPrimitives::Ray_3 ray_3( dtkContinuousGeometryPrimitives::Point_3(p_point[0], p_point[1], 0.),
                                                          dtkContinuousGeometryPrimitives::Vector_3(v[0], v[1], 0.) );

            dtkContinuousGeometryPrimitives::Segment_3 r_s_3( dtkContinuousGeometryPrimitives::Point_3(0., 0., 0.),
                                                              dtkContinuousGeometryPrimitives::Point_3(0., 0., 0.) );

            bool is_clipped = dtkNurbsProbingGeometryTools::clip(r_s_3, &bbox_3[0], ray_3);

            if ( !is_clipped ) {
              ++out;
            }
            else {
              dtkContinuousGeometryPrimitives::Point_2 p1( r_s_3[0][0], r_s_3[0][1] );

              dtkContinuousGeometryPrimitives::Segment_2 r_s( dtkContinuousGeometryPrimitives::Point_2(r_s_3[0][0], r_s_3[0][1]),
                                                              dtkContinuousGeometryPrimitives::Point_2(r_s_3[1][0], r_s_3[1][1]) );

              for (auto it = m_intersects.begin(); it != m_intersects.end(); ++it) {
                it->intersect(intersections, r_s);
              }

              if ((intersections.size() % 2) != 0) {
                ++in;
              } else {
                ++out;
              }
            }

        }

      CGAL_HISTOGRAM_PROFILER("Trim loop check (0 - outside or 3 - inside): ", in);

        if (in > out) {
            if (m_type == dtkContinuousGeometryEnums::TrimLoopType::inner) {
                return false; //not to be culled
            } else {
                return true;
            }
        } else {
            if (m_type == dtkContinuousGeometryEnums::TrimLoopType::outer) {
                return false; //not to be culled
            } else {
                return true;
            }
        }
    // }
}

/*! \fn dtkTrimLoopDataNp::toPolyline(std::list< dtkContinuousGeometryPrimitives::Point_2 >& r_polyline, double p_discretisation_length) const
  Stores in \a r_polyline a polyline representation of the trim loop with regard to the length of discretisation (\a p_discretisation_length), returns true if successfull, else returns false.

  \a r_polyline : list in which the dtkContinuousGeometryPrimitives::Point_2 will be added as polyline representation of the curve
  \a p_discretisation_length : length in parameter space between two samples
 */
bool dtkTrimLoopDataNp::toPolyline(std::list< dtkContinuousGeometryPrimitives::Point_2 >& r_polyline, double p_discretisation_length) const
{
      if (m_trims.empty()) {
        dtkWarn() << "The trim loop is empty, it cannot be converted to polyline";
        return false;
    }
    // ///////////////////////////////////////////////////////////////////
    // Stores the first trim polyline in r_polyline
    // ///////////////////////////////////////////////////////////////////
    (*m_trims.begin())->toPolyline(r_polyline, p_discretisation_length);
    if (r_polyline.empty()) {
        dtkWarn() << "One of the trims is empty, it cannot be converted to polyline";
        return false;
    }
    if (m_trims.size() > 1) {
        std::list< dtkContinuousGeometryPrimitives::Point_2 > temp;
        for (auto it = std::next(m_trims.begin()); it !=m_trims.end(); ++it) {
            (*it)->toPolyline(temp, p_discretisation_length);
            if (!temp.empty()) {
                // ///////////////////////////////////////////////////////////////////
                // Clustering
                // ///////////////////////////////////////////////////////////////////
                double distance = dtkContinuousGeometryTools::squaredDistance(*temp.begin(), *std::prev(r_polyline.end()));
                if(distance > 1e-10) {
                    dtkWarn() << "End point : " << (*std::prev(r_polyline.end()))[0] << " " << (*std::prev(r_polyline.end()))[1];
                    dtkWarn() << "Start point : " << (*temp.begin())[0] << " " << (*temp.begin())[1];
                    dtkWarn() << "Squared distance : " << distance;
                    dtkWarn() << "The start point and end point of two consecutives trims do not match.";
                    return false;
                }
                temp.erase(temp.begin());
                r_polyline.splice(r_polyline.end(), temp);
            } else {
                dtkWarn() << "The trim loop is empty, it cannot be converted to polyline";
                return false;
            }
        }
    }
    double distance = dtkContinuousGeometryTools::squaredDistance(*r_polyline.begin(), *std::prev(r_polyline.end()));
    if ( distance > 1e-10) {
        dtkWarn() << "End point : " << (*r_polyline.begin())[0] << " " << (*r_polyline.begin())[1];
        dtkWarn() << "Start point : " <<  (*std::prev(r_polyline.end()))[0] << " " <<  (*std::prev(r_polyline.end()))[1];
        dtkWarn() << "Squared distance : " << distance;
        dtkWarn() << "The start point and end point of the first and last trims do not match, it doesnt form a loop";
        return false;
    }
    return true;
}

/*! \fn dtkTrimLoopDataNp::toPolylineApprox(std::list< dtkContinuousGeometryPrimitives::Point_2 >& r_polyline, double p_approximation) const
  Stores in \a r_polyline a polyline representation of the trim loop with regard to an approximation distance to the curve(\a p_approximation), returns true if successfull, else returns false.

  \a r_polyline : list in which the dtkContinuousGeometryPrimitives::Point_2 will be added as polyline representation of the curve
  \a p_approximation : maximal tolerated distance from the polygonalization to the trim loop
 */
bool dtkTrimLoopDataNp::toPolylineApprox(std::list< dtkContinuousGeometryPrimitives::Point_2 >& r_polyline, double p_approximation) const
{
      if (m_trims.empty()) {
        dtkWarn() << "The trim loop is empty, it cannot be converted to polyline";
        return false;
    }
    // ///////////////////////////////////////////////////////////////////
    // Stores the first trim polyline in r_polyline
    // ///////////////////////////////////////////////////////////////////
    (*m_trims.begin())->toPolylineApprox(r_polyline, p_approximation);
    if (r_polyline.empty()) {
        dtkWarn() << "One of the trims is empty, it cannot be converted to polyline";
        return false;
    }
    if (m_trims.size() > 1) {
        std::list< dtkContinuousGeometryPrimitives::Point_2 > temp;
        for (auto it = std::next(m_trims.begin()); it !=m_trims.end(); ++it) {
            (*it)->toPolylineApprox(temp, p_approximation);
            if (!temp.empty()) {
                // ///////////////////////////////////////////////////////////////////
                // Clustering
                // ///////////////////////////////////////////////////////////////////
                double distance = dtkContinuousGeometryTools::squaredDistance(*temp.begin(), *std::prev(r_polyline.end()));
                if(distance > 1e-10) {
                    dtkWarn() << "End point : " << (*std::prev(r_polyline.end()))[0] << " " << (*std::prev(r_polyline.end()))[1];
                    dtkWarn() << "Start point : " << (*temp.begin())[0] << " " << (*temp.begin())[1];
                    dtkWarn() << "Squared distance : " << distance;
                    dtkWarn() << "The start point and end point of two consecutives trims do not match.";
                    return false;
                }
                temp.erase(temp.begin());
                r_polyline.splice(r_polyline.end(), temp);
            } else {
                dtkWarn() << "The trim loop is empty, it cannot be converted to polyline";
                return false;
            }
        }
    }
    double distance = dtkContinuousGeometryTools::squaredDistance(*r_polyline.begin(), *std::prev(r_polyline.end()));
    if ( distance > 1e-10) {
        dtkWarn() << "End point : " << (*r_polyline.begin())[0] << " " << (*r_polyline.begin())[1];
        dtkWarn() << "Start point : " <<  (*std::prev(r_polyline.end()))[0] << " " <<  (*std::prev(r_polyline.end()))[1];
        dtkWarn() << "Squared distance : " << distance;
        dtkWarn() << "The start point and end point of the first and last trims do not match, it doesnt form a loop";
        return false;
    }
    return true;
}

/*! \fn dtkTrimLoopDataNp::isValid(void) const
  Returns true if the all the trims of the trim loop are connected, else returns false.
 */
bool dtkTrimLoopDataNp::isValid(void) const
{
    // ///////////////////////////////////////////////////////////////////
    // Checks that all the trims of the trim loop are connected
    // ///////////////////////////////////////////////////////////////////
    dtkContinuousGeometryPrimitives::Point_2 p_start(0., 0.);
    dtkContinuousGeometryPrimitives::Point_2 p_end(0., 0.);
    std::size_t nb_of_knots = 0;
    if (m_trims.size() == 0) {
        dtkWarn() << "The trim loop doesn't contain any trim";
        return false;
    } else if (m_trims.size() == 1) {
        nb_of_knots = (*m_trims.begin())->curve2D().nbCps() + (*m_trims.begin())->curve2D().degree() -1;
        double* knots = new double[nb_of_knots];
        (*m_trims.begin())->curve2D().knots(&knots[0]);
        (*m_trims.begin())->curve2D().evaluatePoint(knots[0], p_start.data());
        (*m_trims.begin())->curve2D().evaluatePoint(knots[nb_of_knots - 1], p_end.data());
        delete[] knots;
        if (!(p_start == p_end)) {
            dtkWarn() << "There is a single trim in the loop, and the trim is not closed";
            return false;
        } else {
            return true;
        }
    } else {
        // ///////////////////////////////////////////////////////////////////
        // Recovers the first trim
        // ///////////////////////////////////////////////////////////////////
        nb_of_knots = (*m_trims.begin())->curve2D().nbCps() + (*m_trims.begin())->curve2D().degree() - 1;
        dtkContinuousGeometryPrimitives::Point_2 p_init(0., 0.);
        double* knots_1 = new double[nb_of_knots];
        (*m_trims.begin())->curve2D().knots(&knots_1[0]);
        (*m_trims.begin())->curve2D().evaluatePoint(knots_1[0], p_init.data());
        (*m_trims.begin())->curve2D().evaluatePoint(knots_1[nb_of_knots - 1], p_end.data());
        delete[] knots_1;
        for (auto trim = std::next(m_trims.begin()); trim != m_trims.end(); ++trim) {
            nb_of_knots = (*trim)->curve2D().nbCps() + (*trim)->curve2D().degree() - 1;
            double* knots = new double[nb_of_knots];
            (*trim)->curve2D().knots(&knots[0]);
            (*trim)->curve2D().evaluatePoint(knots[0], p_start.data());
            dtkWarn() << "End point : " << p_end[0] << " " << p_end[1];
            dtkWarn() << "Start point : " << p_start[0] << " " << p_start[1];
            if (!(p_start == p_end)) {
                delete[] knots;
                dtkWarn() << "End point : " << p_end[0] << " " << p_end[1];
                dtkWarn() << "Start point : " << p_start[0] << " " << p_start[1];
                dtkWarn() << "The trims do not form a loop.";
                return false;
            }
            (*trim)->curve2D().evaluatePoint(knots[nb_of_knots - 1], p_end.data());
            delete[] knots;
        }
        // ///////////////////////////////////////////////////////////////////
        // Checks the last point of the last curve with the first point of the first curve
        // ///////////////////////////////////////////////////////////////////
        if (!(p_end == p_init)) {
            dtkWarn() << "End point : " << p_end[0] << " " << p_end[1];
            dtkWarn() << "Start point : " << p_init[0] << " " << p_init[1];
            dtkWarn() << "The trims do not form a loop.";
            return false;
        }
        return true;
    }
}

/*! \fn dtkTrimLoopDataNp::aabb(double* r_aabb) const
  Writes in \a r_aabb the [xmin, ymin, zmin, xmax, ymax, zmax] coordinates.

  \a r_aabb : array of size 6 to store the limits coordinates
*/
void dtkTrimLoopDataNp::aabb(double* r_aabb) const
{
  std::array<double,4> aabb;
    r_aabb[0] = std::numeric_limits<double>::max();
    r_aabb[1] = std::numeric_limits<double>::max();
    r_aabb[2] = std::numeric_limits<double>::min();
    r_aabb[3] = std::numeric_limits<double>::min();

    for (auto trim = m_trims.begin(); trim != m_trims.end(); ++trim) {
        (*trim)->curve2D().aabb(&aabb[0]);
        if(r_aabb[0] > aabb[0]) {
            r_aabb[0] = aabb[0];
        }
        if(r_aabb[1] > aabb[1]) {
            r_aabb[1] = aabb[1];
        }
        if(r_aabb[2] < aabb[2]) {
            r_aabb[2] = aabb[2];
        }
        if(r_aabb[3] < aabb[3]) {
            r_aabb[3] = aabb[3];
        }
    }
}

/*! \fn dtkTrimLoopDataNp::clone(void) const
   Clone
*/
dtkTrimLoopDataNp* dtkTrimLoopDataNp::clone(void) const
{
    return new dtkTrimLoopDataNp(*this);
}

//
// dtkTrimLoopDataNp.cpp ends here
