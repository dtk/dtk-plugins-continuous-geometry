// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <dtkTrimLoopDataNpExport.h>

#include <dtkContinuousGeometry>

#include <dtkContinuousGeometryUtils>

#include <dtkAbstractTrimLoopData>

#include <dtkNurbsCurve2DIntersect.h>

#include <atomic>
#include <mutex>

class DTKTRIMLOOPDATANP_EXPORT dtkTrimLoopDataNp final : public dtkAbstractTrimLoopData
{
 public:
    dtkTrimLoopDataNp(void);
    dtkTrimLoopDataNp(const dtkTrimLoopDataNp& other) : dtkAbstractTrimLoopData(other) {}
    ~dtkTrimLoopDataNp(void) final ;

 public:
    void setType(dtkContinuousGeometryEnums::TrimLoopType p_type) const override;

    dtkContinuousGeometryEnums::TrimLoopType type(void) const override;

    bool isPointCulled(dtkContinuousGeometryPrimitives::Point_2 p_point) const override;

    bool toPolyline(std::list< dtkContinuousGeometryPrimitives::Point_2 >& r_polyline, double p_discretisation_length) const override;
    bool toPolylineApprox(std::list< dtkContinuousGeometryPrimitives::Point_2 >& r_polyline, double p_approximation) const override;

    bool isValid(void) const override;

    void aabb(double *r_aabb) const override;

 public:
    dtkTrimLoopDataNp* clone(void) const override;

private :
    mutable dtkContinuousGeometryEnums::TrimLoopType m_type;
    //mutable std::list< dtkNurbsCurve2DMRepIntersect > m_mrep_intersects;
    mutable std::atomic<bool> m_initialized = { false };
    mutable std::mutex mutex;

    mutable std::list< dtkNurbsCurve2DIntersect > m_intersects;
};


// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

inline dtkAbstractTrimLoopData *dtkTrimLoopDataNpCreator(void)
{
    return new dtkTrimLoopDataNp();
}

//
// dtkTrimLoopDataNp.h ends here
