// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <QtCore>

#include <dtkContinuousGeometry>

#include <dtkAbstractTrimLoopData>

class dtkTrimLoopDataNpPlugin : public dtkAbstractTrimLoopDataPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkAbstractTrimLoopDataPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkTrimLoopDataNpPlugin" FILE "dtkTrimLoopDataNpPlugin.json")

public:
     dtkTrimLoopDataNpPlugin(void) {}
    ~dtkTrimLoopDataNpPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

//
// dtkTrimLoopDataNpPlugin.h ends here
