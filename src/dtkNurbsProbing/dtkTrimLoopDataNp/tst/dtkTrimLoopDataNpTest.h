// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <QTest>

class dtkTrimLoopDataNpTestCasePrivate;

class dtkTrimLoopDataNpTestCase : public QObject
{
    Q_OBJECT

public:
    dtkTrimLoopDataNpTestCase(void);
    ~dtkTrimLoopDataNpTestCase(void);

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testType(void);
    void testToPolyline(void);
    void testIsPointCulled(void);
    void testIsValid(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);

private:
    dtkTrimLoopDataNpTestCasePrivate* d;
};

//
// dtkTrimLoopDataNpTest.h ends here
