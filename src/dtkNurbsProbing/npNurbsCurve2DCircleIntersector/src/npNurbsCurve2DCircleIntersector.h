// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <dtkNurbsCurve2DCircleIntersector>

#include <npNurbsCurve2DCircleIntersectorExport.h>

#include <dtkContinuousGeometry>

class dtkNurbsCurve2D;

struct npNurbsCurve2DCircleIntersectorPrivate;

class NPNURBSCURVE2DCIRCLEINTERSECTOR_EXPORT npNurbsCurve2DCircleIntersector : public dtkNurbsCurve2DCircleIntersector
{
 public:
    npNurbsCurve2DCircleIntersector(void);
    ~npNurbsCurve2DCircleIntersector(void);

 public:
    void setNurbsCurve2D(dtkNurbsCurve2D *) final;
    void setCircle(const dtkContinuousGeometryPrimitives::Circle_2& circle) final;

 public:
    bool run(void) final;

 public:
    std::vector< double > intersectionParameters(void) final;

 protected:
    npNurbsCurve2DCircleIntersectorPrivate *d;
};


// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

inline dtkNurbsCurve2DCircleIntersector *npNurbsCurve2DCircleIntersectorCreator(void)
{
    return new npNurbsCurve2DCircleIntersector();
}

//
// npNurbsCurve2DCircleIntersector.h ends here
