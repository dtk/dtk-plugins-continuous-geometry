// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "npNurbsCurve2DCircleIntersector.h"

#include <dtkContinuousGeometry.h>

#include <dtkContinuousGeometryUtils>

#include <dtkNurbsCurve2DIntersect>

// /////////////////////////////////////////////////////////////////
// npNurbsCurve2DCircleIntersectorPrivate
// /////////////////////////////////////////////////////////////////

struct npNurbsCurve2DCircleIntersectorPrivate
{
    dtkNurbsCurve2D *nurbs_curve_2d = nullptr;
    dtkContinuousGeometryPrimitives::Circle_2 circle = { {0., 0.}, 0.};

    std::vector< double > intersection_parameters;
};

/*!
  \class npNurbsCurve2DCircleIntersector
  \inmodule dtkPluginsContinuousGeometry
  \brief npNurbsCurve2DCircleIntersector is a dtkNurbsProbing implementation of the concept dtkNurbsCurve2DCircleIntersector.

  The following lines of code show how to instanciate and use the class in pratice :
  \code
  dtkNurbsCurve2DCircleIntersector *intersector = dtkContinuousGeometry::nurbsCurve2DCircleIntersector::pluginFactory().create("npNurbsCurve2DCircleIntersector");
  intersector->setNurbsCurve(...);
  intersector->setCircle(...);
  intersector->run();
  std::vector< double > intersection_paramters = intersector->intersectionParameters();
  \endcode
*/
npNurbsCurve2DCircleIntersector::npNurbsCurve2DCircleIntersector(void) : d(new npNurbsCurve2DCircleIntersectorPrivate) {}

npNurbsCurve2DCircleIntersector::~npNurbsCurve2DCircleIntersector(void)
{
    delete d;
    d = nullptr;
}

/*! \fn npNurbsCurve2DCircleIntersector::setNurbsCurve2D(dtkNurbsCurve2D *curve)
  Provides the 2D NURBS curve to the intersector (\a curve).

  \a curve : the 2D NURBS curve to intersect
*/
void npNurbsCurve2DCircleIntersector::setNurbsCurve2D(dtkNurbsCurve2D *nurbs_curve_2d)
{
    d->nurbs_curve_2d = nurbs_curve_2d;
}

/*! \fn npNurbsCurve2DCircleIntersector::setCircle(dtkContinuousGeometryPrimitives::Circle_2 *circle)
  Provides the \a circle to intersect the 2D NURBS curve with to the intersector.

  \a circle : the circle to intersect the 2D NURBS curve with
*/
void npNurbsCurve2DCircleIntersector::setCircle(const dtkContinuousGeometryPrimitives::Circle_2& circle)
{
    d->circle = circle;
}

/*! \fn npNurbsCurve2DCircleIntersector::run(void)
  Intersects the 2D NURBS curve  (provided with \l setNurbsCurve2D(dtkNurbsCurve2D *curve)) with the circle(provided with \l setCircle(Circle_2 *circle)), the result can be retrieved with \l intersection(void).
*/
bool npNurbsCurve2DCircleIntersector::run(void)
{
    // ///////////////////////////////////////////////////////////////////
    // Checks that all inputs are set
    // ///////////////////////////////////////////////////////////////////
    if (d->nurbs_curve_2d == nullptr) {
        dtkFatal() << "Not all inputs of npNurbsCurve2DCircleIntersector were set";
        return false;
    }
    dtkNurbsCurve2DIntersect intersector(*d->nurbs_curve_2d);
    std::set< double > s_intersection_parameters;
    if(!intersector.intersect(s_intersection_parameters, d->circle)) {
        dtkFatal() << Q_FUNC_INFO << " Intersection failed.";
        return false;
    }
    d->intersection_parameters = std::vector< double >(s_intersection_parameters.begin(), s_intersection_parameters.end());
    return true;
}

/*! \fn npNurbsCurve2DCircleIntersector::intersectionsParameters(void);
  Returns the intersection parameters in the dtkNurbsCurve2D space of parameters.
*/
std::vector< double > npNurbsCurve2DCircleIntersector::intersectionParameters(void)
{
    return d->intersection_parameters;
}

//
// npNurbsCurve2DCircleIntersector.cpp ends here
