// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "npNurbsCurve2DCircleIntersector.h"
#include "npNurbsCurve2DCircleIntersectorPlugin.h"

#include <dtkCore>
#include <dtkContinuousGeometry>

// ///////////////////////////////////////////////////////////////////
// npNurbsCurve2DCircleIntersectorPlugin
// ///////////////////////////////////////////////////////////////////

void npNurbsCurve2DCircleIntersectorPlugin::initialize(void)
{
    dtkContinuousGeometry::nurbsCurve2DCircleIntersector::pluginFactory().record("npNurbsCurve2DCircleIntersector", npNurbsCurve2DCircleIntersectorCreator);
}

void npNurbsCurve2DCircleIntersectorPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(npNurbsCurve2DCircleIntersector)

//
// npNurbsCurve2DCircleIntersectorPlugin.cpp ends here
