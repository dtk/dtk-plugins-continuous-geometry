// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore>

#include <dtkContinuousGeometry.h>

#include <dtkNurbsCurve2DCircleIntersector.h>

class npNurbsCurve2DCircleIntersectorPlugin : public dtkNurbsCurve2DCircleIntersectorPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkNurbsCurve2DCircleIntersectorPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.npNurbsCurve2DCircleIntersectorPlugin" FILE "npNurbsCurve2DCircleIntersectorPlugin.json")

public:
     npNurbsCurve2DCircleIntersectorPlugin(void) {}
    ~npNurbsCurve2DCircleIntersectorPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

//
// npNurbsCurve2DCircleIntersectorPlugin.h ends here
