// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <QTest>

class npRationalBezierCurveSphereIntersectorTestCasePrivate;

class npRationalBezierCurveSphereIntersectorTestCase : public QObject
{
    Q_OBJECT

public:
    npRationalBezierCurveSphereIntersectorTestCase(void);
    ~npRationalBezierCurveSphereIntersectorTestCase(void);

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testRun(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);

private:
    npRationalBezierCurveSphereIntersectorTestCasePrivate* d;
};

//
// npRationalBezierCurveSphereIntersectorTest.h ends here
