// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <dtkRationalBezierCurveSphereIntersector>

#include <npRationalBezierCurveSphereIntersectorExport.h>

#include <dtkContinuousGeometry>

class dtkRationalBezierCurve;

struct npRationalBezierCurveSphereIntersectorPrivate;

class NPRATIONALBEZIERCURVESPHEREINTERSECTOR_EXPORT npRationalBezierCurveSphereIntersector : public dtkRationalBezierCurveSphereIntersector
{
 public:
    npRationalBezierCurveSphereIntersector(void);
    ~npRationalBezierCurveSphereIntersector(void);

 public:
    void setRationalBezierCurve(const dtkRationalBezierCurve *) final;
    void setSphere(const dtkContinuousGeometryPrimitives::Sphere_3& sphere) final;

 public:
    bool run(void) final;
    void reset() final;

 public:
    std::vector< double > intersectionParameters(void) final;

 protected:
    npRationalBezierCurveSphereIntersectorPrivate *d;
};


// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

inline dtkRationalBezierCurveSphereIntersector *npRationalBezierCurveSphereIntersectorCreator(void)
{
    return new npRationalBezierCurveSphereIntersector();
}

//
// npRationalBezierCurveSphereIntersector.h ends here
