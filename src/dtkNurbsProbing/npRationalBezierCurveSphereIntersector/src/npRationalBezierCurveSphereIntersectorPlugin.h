// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore>

#include <dtkContinuousGeometry.h>

#include <dtkRationalBezierCurveSphereIntersector.h>

class npRationalBezierCurveSphereIntersectorPlugin : public dtkRationalBezierCurveSphereIntersectorPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkRationalBezierCurveSphereIntersectorPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.npRationalBezierCurveSphereIntersectorPlugin" FILE "npRationalBezierCurveSphereIntersectorPlugin.json")

public:
     npRationalBezierCurveSphereIntersectorPlugin(void) {}
    ~npRationalBezierCurveSphereIntersectorPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

//
// npRationalBezierCurveSphereIntersectorPlugin.h ends here
