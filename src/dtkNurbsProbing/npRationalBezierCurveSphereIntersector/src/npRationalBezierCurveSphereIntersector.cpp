// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "npRationalBezierCurveSphereIntersector.h"

#include <dtkContinuousGeometry.h>

#include <dtkContinuousGeometryUtils>
#include <dtkRationalBezierCurve>

#include <dtkRationalBezierCurveIntersect>



// /////////////////////////////////////////////////////////////////
// npRationalBezierCurveSphereIntersectorPrivate
// /////////////////////////////////////////////////////////////////

struct npRationalBezierCurveSphereIntersectorPrivate
{
    const dtkRationalBezierCurve *rational_bezier_curve = nullptr;
    dtkContinuousGeometryPrimitives::Sphere_3 sphere = { {0., 0., 0.}, 0.};

    std::vector< double > intersection_parameters;
};

/*!
  \class npRationalBezierCurveSphereIntersector
  \inmodule dtkPluginsContinuousGeometry
  \brief npRationalBezierCurveSphereIntersector is a dtkNurbsProbing implementation of the concept dtkRationalBezierCurveSphereIntersector.

  The following lines of code show how to instanciate and use the class in pratice :
  \code
  dtkRationalBezierCurveSphereIntersector *intersector = dtkContinuousGeometry::rationalBezierCurveElevator::pluginFactory().create("npRationalBezierCurveSphereIntersector");
  intersector->setRationalBezierCurve(...);
  intersector->setSphere(...);
  intersector->run();
  std::vector< double > intersection_parameters = intersector->intersectionParameters();
  \endcode
*/
npRationalBezierCurveSphereIntersector::npRationalBezierCurveSphereIntersector(void) : d(new npRationalBezierCurveSphereIntersectorPrivate) {}

npRationalBezierCurveSphereIntersector::~npRationalBezierCurveSphereIntersector(void)
{
    delete d;
    d = nullptr;
}

/*! \fn npRationalBezierCurveSphereIntersector::setRationalBezierCurve(const dtkRationalBezierCurve *curve)
  Provides the rational Bezier curve to the intersector (\a curve).

  \a curve : the rational Bezier curve to intersect
*/
void npRationalBezierCurveSphereIntersector::setRationalBezierCurve(const dtkRationalBezierCurve *rational_bezier_curve)
{
    d->rational_bezier_curve = rational_bezier_curve;
}

/*! \fn npRationalBezierCurveSphereIntersector::setSphere(dtkContinuousGeometryPrimitives::Sphere_3 *sphere)
  Provides the \a sphere to intersect the rational Bezier curve with to the intersector.

  \a sphere : the sphere to intersect the rational Bezier curve with
*/
void npRationalBezierCurveSphereIntersector::setSphere(const dtkContinuousGeometryPrimitives::Sphere_3& sphere)
{
    d->sphere = sphere;
}

void npRationalBezierCurveSphereIntersector::reset() {
    d->intersection_parameters.clear();
}

/*! \fn npRationalBezierCurveSphereIntersector::run(void)
  Intersects the rational Bezier curve (provided with \l setRationalBezierCurve(const dtkRationalBezierCurve *curve)) with the sphere(provided with \l setSphere(Sphere_3 *sphere)), the result can be retrieved with \l intersectionParameters(void).
*/
bool npRationalBezierCurveSphereIntersector::run(void)
{
    // ///////////////////////////////////////////////////////////////////
    // Checks that all inputs are set
    // ///////////////////////////////////////////////////////////////////
    if (d->rational_bezier_curve == nullptr) {
        dtkFatal() << "Not all inputs of npRationalBezierCurveSphereIntersector were set";
        return false;
    }
    dtkRationalBezierCurveIntersect intersector(*d->rational_bezier_curve);
    std::set< double > s_intersection_parameters;
    if(!intersector.intersect(s_intersection_parameters, d->sphere)) {
        dtkFatal() << Q_FUNC_INFO << " Intersection failed.";
        return false;
    }

    dtkContinuousGeometryPrimitives::Point_3 p(0., 0., 0.);
    for(auto parameter : s_intersection_parameters) {
        d->rational_bezier_curve->evaluatePoint(parameter, p.data());
        double distance = std::sqrt(dtkContinuousGeometryTools::squaredDistance(p, d->sphere.center()));
        double epsilon = 1e-1 * d->sphere.radius();
        if(distance < d->sphere.radius() + epsilon && distance > d->sphere.radius() - epsilon) {
            d->intersection_parameters.push_back(parameter);
        } else {
            dtkError() << "Filtered intersection : " << parameter << " because distance - radius = " << distance - d->sphere.radius();
        }
    }

    return true;
}

/*! \fn npRationalBezierCurveSphereIntersector::intersectionsParameters(void);
  Returns the intersection parameters in the dtkRationalBezierCurve space of parameters.
*/
std::vector< double > npRationalBezierCurveSphereIntersector::intersectionParameters(void)
{
    return d->intersection_parameters;
}

//
// npRationalBezierCurveSphereIntersector.cpp ends here
