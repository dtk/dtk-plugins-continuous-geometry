// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "npRationalBezierCurveSphereIntersector.h"
#include "npRationalBezierCurveSphereIntersectorPlugin.h"

#include <dtkCore>
#include <dtkContinuousGeometry>

// ///////////////////////////////////////////////////////////////////
// npRationalBezierCurveSphereIntersectorPlugin
// ///////////////////////////////////////////////////////////////////

void npRationalBezierCurveSphereIntersectorPlugin::initialize(void)
{
    dtkContinuousGeometry::rationalBezierCurveSphereIntersector::pluginFactory().record("npRationalBezierCurveSphereIntersector", npRationalBezierCurveSphereIntersectorCreator);
}

void npRationalBezierCurveSphereIntersectorPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(npRationalBezierCurveSphereIntersector)

//
// npRationalBezierCurveSphereIntersectorPlugin.cpp ends here
