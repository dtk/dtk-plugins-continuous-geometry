// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkNurbsSurfaceDataOCCT.h"

#include <dtkTrimLoop>

#include <dtkRationalBezierSurfaceDataOCCT.h>

#include <cassert>

#include <Geom_BSplineSurface.hxx>
#include <GeomConvert_BSplineSurfaceToBezierSurface.hxx>
#include <Bnd_Box.hxx>
#include <BndLib_AddSurface.hxx>
#include <GeomAdaptor_Surface.hxx>

#include "multiplicities.h"

/*!
  \class dtkNurbsSurfaceDataOCCT
  \inmodule dtkPluginsContinuousGeometry
  \brief dtkNurbsSurfaceDataOCCT is an openNURBS implementation of the concept dtkAbstractNurbsSurfaceData.

  The following lines of code show how to instanciate and use the class in pratice:
  \code
  dtkAbstractNurbsSurfaceData *nurbs_surface_data = dtkContinuousGeometry::abstractNurbsSurfaceData::pluginFactory().create("dtkNurbsSurfaceDataOCCT");
  dtkNurbsSurface *nurbs_surface = new dtkNurbsSurface(nurbs_surface_data);
  nurbs_surface->create(...);
  \endcode
*/

/*! \fn dtkNurbsSurfaceDataOCCT::dtkNurbsSurfaceDataOCCT(void)
  Instanciates the structure.

  To add the content, see \l create(std::size_t dim, std::size_t nb_cp_u, std::size_t nb_cp_v, std::size_t order_u, std::size_t order_v, const double *knots_u, const double *knots_v, const double *cps) const.
*/
dtkNurbsSurfaceDataOCCT::dtkNurbsSurfaceDataOCCT(void) : d(nullptr)
{

}

/*! \fn dtkNurbsSurfaceDataOCCT::~dtkNurbsSurfaceDataOCCT(void)
  Destroys the instance.
*/
dtkNurbsSurfaceDataOCCT::~dtkNurbsSurfaceDataOCCT(void)
{
    for (auto it = m_trim_loops.begin(); it != m_trim_loops.end(); ++it) {
        delete (*it);
    }
    m_trim_loops.clear();
}

/*! \fn dtkNurbsSurfaceDataOCCT::create(const ON_NurbsSurface& nurbs_surface)
  Not available in the dtkAbstractNurbsSurfaceData.

  Creates the NURBS surface by providing a NURBS surface from the openNURBS API.

  \a nurbs_surface : the NURBS surface to copy.
*/
void dtkNurbsSurfaceDataOCCT::create(const Handle(Geom_BSplineSurface)& nurbs_surface)
{
    d = Handle(Geom_BSplineSurface)::DownCast(nurbs_surface->Copy());
    update_m_knots();
}

/*! \fn void dtkNurbsSurfaceDataOCCT::create(std::size_t dim, std::size_t nb_cp_u, std::size_t nb_cp_v, std::size_t order_u, std::size_t order_v, const double *knots_u, const double *knots_v, const double *cps) const
  Creates a NURBS surface by providing the required content.

  \a dim : dimension of the space in which lies the surface

  \a nb_cp_u : number of control points along the "u" direction

  \a nb_cp_v : number of control points along the "v" direction

  \a order_u : order(degree + 1) in the "u" direction

  \a order_v : order(degree + 1) in the "v" direction

  \a knots_u : array containing the knots in the "u" direction

  \a knots_v : array containing the knots in the "v" direction

  \a cps : array containing the weighted control points, secified as : [x_00, y_00, z_00, w_00, x_01, y_01, ...]

  \a knots_u, \a knots_v, and \a cps are copied.
*/
void dtkNurbsSurfaceDataOCCT::create(std::size_t dim, std::size_t nb_cp_u, std::size_t nb_cp_v, std::size_t order_u, std::size_t order_v, const double *knots_u, const double *knots_v, const double *cps) const
{
    (void)dim;
    assert(dim == 3);
    TColgp_Array2OfPnt Poles(1, nb_cp_u, 1, nb_cp_v);
    TColStd_Array2OfReal Weights(1, nb_cp_u, 1, nb_cp_v);

    for(std::size_t i = 0; i < nb_cp_u; ++i) {
        for(std::size_t j = 0; j < nb_cp_v; ++j) {
            Poles.SetValue(i+1, j+1, gp_Pnt{ cps[0], cps[1], cps[2] });
            Weights.SetValue(i+1, j+1, cps[3]);
            cps += 4;
        }
    }

    auto knots_and_multiplicities_u = multiplicities(knots_u, order_u+nb_cp_u-2);
    const auto& knots_u_ = knots_and_multiplicities_u.first;
    const auto& mults_u = knots_and_multiplicities_u.second;
    assert(knots_u_.size() == mults_u.size());
    TColStd_Array1OfReal UKnots(1, knots_u_.size());
    std::copy(knots_u_.begin(), knots_u_.end(), &UKnots.ChangeValue(1));
    TColStd_Array1OfInteger UMults(1, mults_u.size());
    std::copy(mults_u.begin(), mults_u.end(), &UMults.ChangeValue(1));

    auto knots_and_multiplicities_v = multiplicities(knots_v, order_v+nb_cp_v-2);
    const auto& knots_v_ = knots_and_multiplicities_v.first;
    const auto& mults_v = knots_and_multiplicities_v.second;
    assert(knots_v_.size() == mults_v.size());
    TColStd_Array1OfReal VKnots(1, knots_v_.size());
    std::copy(knots_v_.begin(), knots_v_.end(), &VKnots.ChangeValue(1));
    TColStd_Array1OfInteger VMults(1, mults_v.size());
    std::copy(mults_v.begin(), mults_v.end(), &VMults.ChangeValue(1));

    /*
      Difference with the OpenNURBS API: the sum of multiplicities
      must be order+nb_cp and not order+nb_cp-2.
      See https://developer.rhino3d.com/guides/opennurbs/superfluous-knots/
    */
    ++UMults.ChangeFirst();
    ++UMults.ChangeLast();
    ++VMults.ChangeFirst();
    ++VMults.ChangeLast();

    Standard_Integer UDegree = order_u - 1;
    Standard_Integer VDegree = order_v - 1;

    d = new Geom_BSplineSurface(Poles, Weights, UKnots, VKnots, UMults, VMults, UDegree, VDegree);

    auto nb_knots_u = order_u + nb_cp_u - 2;
    m_knots_u.resize(nb_knots_u);
    std::copy(knots_u, knots_u + nb_knots_u, &m_knots_u[0]);
    auto nb_knots_v = order_v + nb_cp_v - 2;
    m_knots_v.resize(nb_knots_v);
    std::copy(knots_v, knots_v + nb_knots_v, &m_knots_v[0]);
}

/*! \fn void dtkNurbsSurfaceDataOCCT::create(std::string path) const
  Creates a NURBS surface by providing a path to a file storing the required information to create a NURBS surface.

  \a path : a valid path to the file containing the information regarding a given NURBS surface
*/
void dtkNurbsSurfaceDataOCCT::create(std::string path) const
{
    QFile file(QString::fromStdString(path));
    QIODevice *in = &file;

    QIODevice::OpenMode mode = QIODevice::ReadOnly;
    mode |= QIODevice::Text;

    // to avoid troubles with floats separators ('.' and not ',')
    QLocale::setDefault(QLocale::c());
#if defined (Q_OS_UNIX) && !defined(Q_OS_MAC)
    setlocale(LC_NUMERIC, "C");
#endif

    if (!in->open(mode)) {
        dtkFatal() << Q_FUNC_INFO << "The file could not be found, please verify the path";
        return;
    }

    QString line = in->readLine().trimmed();
    //Run tests on the file to check its validity TODO more
    if ( line != "#dtkNurbsSurfaceDataOCCT"){
        dtkFatal() << Q_FUNC_INFO << "file not in a dtkNurbsSurfaceDataOCCT format.";
        return;
    }
    std::size_t dim = 3;
    QRegExp re = QRegExp("\\s+");

    line = in->readLine().trimmed();
    QStringList vals = line.split(re);
    std::size_t order_u = vals[1].toInt() + 1;
    line = in->readLine().trimmed();
    vals = line.split(re);
    std::size_t order_v = vals[1].toInt() + 1;
    line = in->readLine().trimmed();
    vals = line.split(re);
    std::size_t nb_cp_u = vals[1].toInt();
    line = in->readLine().trimmed();
    vals = line.split(re);
    std::size_t nb_cp_v = vals[1].toInt();
    line = in->readLine().trimmed();
    vals = line.split(re);
    std::vector<double> knots_u(vals.size());
    std::size_t i = 0;
    for(auto string : vals) {
        knots_u[i] = string.toDouble();
        ++i;
    }
    line = in->readLine().trimmed();
    vals = line.split(re);
    std::vector<double> knots_v(vals.size());
    i = 0;
    for(auto string : vals) {
        knots_v[i] = string.toDouble();
        ++i;
    }

    std::vector<double> cps_vec(4 * nb_cp_u * nb_cp_v);
    double *cps = cps_vec.data();
    for(std::size_t i = 0; i < nb_cp_u; ++i) {
        for(std::size_t j = 0; j < nb_cp_v;) {
            line = in->readLine().trimmed();
            if (line.startsWith("#")) {
                continue;
            }
            std::istringstream lin(line.toStdString());
            double x, y, z, w;
            lin >> x >> y >> z >> w;       //now read the whitespace-separated floats
            cps[0] = x;
            cps[1] = y;
            cps[2] = z;
            cps[3] = w;
            cps += 4;
            ++j;
        }
    }

    return this->create(dim, nb_cp_u, nb_cp_v, order_u, order_v, knots_u.data(),
                        knots_v.data(), cps_vec.data());
}

/*! \fn dtkNurbsSurfaceDataOCCT::uDegree(void) const
  Returns the degree of in the "u" direction.
*/
std::size_t dtkNurbsSurfaceDataOCCT::uDegree(void) const
{
    return d->UDegree();
}

/*! \fn dtkNurbsSurfaceDataOCCT::vDegree(void) const
  Returns the degree of in the "v" direction.
*/
std::size_t dtkNurbsSurfaceDataOCCT::vDegree(void) const
{
    return d->VDegree();
}

/*! \fn dtkNurbsSurfaceDataOCCT::uNbCps(void) const
  Returns the number of weighted control points in the "u" direction.
*/
std::size_t dtkNurbsSurfaceDataOCCT::uNbCps(void) const
{
    return d->NbUPoles();
}

/*! \fn dtkNurbsSurfaceDataOCCT::vNbCps(void) const
  Returns the number of weighted control points in the "v" direction.
*/
std::size_t dtkNurbsSurfaceDataOCCT::vNbCps(void) const
{
    return d->NbVPoles();
}

std::size_t dtkNurbsSurfaceDataOCCT::uNbKnots(void) const
{
    if (!d->IsUPeriodic()) {
        return (d->NbUPoles() + d->UDegree() - 1);
    }
    else {
        return (d->NbUPoles() + d->UMultiplicity(1) - 2);
    }
}

std::size_t dtkNurbsSurfaceDataOCCT::vNbKnots(void) const
{
    if (!d->IsVPeriodic()) {
        return (d->NbVPoles() + d->VDegree() - 1);
    }
    else {
        return (d->NbVPoles() + d->VMultiplicity(1) - 2);
    }
}

/*! \fn dtkNurbsSurfaceDataOCCT::dim(void) const
  Returns the dimension of the space in which the NURBS surface lies.
*/
std::size_t dtkNurbsSurfaceDataOCCT::dim(void) const
{
    return 3;
}

/*! \fn dtkNurbsSurfaceDataOCCT::controlPoint(std::size_t i, std::size_t j, double *r_cp) const
  Writes in \a r_cp the \a i th, \a j th weighted control point coordinates[xij, yij, zij].

  \a i : index of the weighted control point along the "u" direction

  \a j : index of the weighted control point along the "v" direction

  \a r_cp : array of size 3 to store the weighted control point coordinates[xij, yij, zij]
*/
void dtkNurbsSurfaceDataOCCT::controlPoint(std::size_t i, std::size_t j, double* r_cp) const
{
    const auto& pole = d->Pole(i+1, j+1);
    const auto& xyz = pole.XYZ();
    std::copy(xyz.GetData(), xyz.GetData() + 3, r_cp);
}

/*! \fn dtkNurbsSurfaceDataOCCT::weight(std::size_t i, std::size_t j, double *r_w) const
  Writes in \a r_w the \a i th, \a j th weighted control point weight wij.

  \a i : index of the weighted control point along the "u" direction

  \a j : index of the weighted control point along the "v" direction

  \a r_w : array of size 1 to store the weighted control point weight wij
*/
void dtkNurbsSurfaceDataOCCT::weight(std::size_t i, std::size_t j, double* r_w) const
{
    *r_w = d->Weight(i+1, j+1);
}

void dtkNurbsSurfaceDataOCCT::update_m_knots() const
{
    if (!d->IsUPeriodic()) {
      m_knots_u.resize(d->NbUPoles() + d->UDegree() - 1);
    }
    else {
      m_knots_u.resize(d->NbUPoles() + d->UMultiplicity(1) - 2);
    }
    auto* r_knots = m_knots_u.data();
    for(int i = 1, nb = d->NbUKnots(); i <= nb; ++i) {
        const auto knot = d->UKnot(i);
        auto mult = d->UMultiplicity(i);
        if(i == 1 || i == nb) mult -= 1;
        for(int j = 0; j < mult; ++j) {
            *r_knots++ = knot;
        }
    }
    if (!d->IsVPeriodic()) {
      m_knots_v.resize(d->NbVPoles() + d->VDegree() - 1);
    }
    else {
      m_knots_v.resize(d->NbVPoles() + d->VMultiplicity(1) - 2);
    }
    r_knots = m_knots_v.data();
    for(int i = 1, nb = d->NbVKnots(); i <= nb; ++i) {
        const auto knot = d->VKnot(i);
        auto mult = d->VMultiplicity(i);
        if(i == 1 || i == nb) mult -= 1;
        for(int j = 0; j < mult; ++j) {
            *r_knots++ = knot;
        }
    }
}

/*! \fn dtkNurbsSurfaceDataOCCT::uKnots(double *r_u_knots) const
  Writes in \a r_u_knots the knots of the curve along the "u" direction.

  \a r_u_knots : array of size (nb_cp_u + order_u - 2) to store the knots
*/
void dtkNurbsSurfaceDataOCCT::uKnots(double* r_u_knots) const
{
    std::copy(m_knots_u.begin(), m_knots_u.end(), r_u_knots);
}

/*! \fn dtkNurbsSurfaceDataOCCT::vKnots(double *r_v_knots) const
  Writes in \a r_v_knots the knots of the curve along the "v" direction.

  \a r_v_knots : array of size (nb_cp_v + order_v - 2) to store the knots
*/
void dtkNurbsSurfaceDataOCCT::vKnots(double* r_v_knots) const
{
    std::copy(m_knots_v.begin(), m_knots_v.end(), r_v_knots);
}

/*! \fn dtkNurbsSurfaceDataOCCT::uKnots(void) const
  Returns a pointer to the array storing the knots of the surface along the "u" direction. The array is of size : (nb_cp_u + order_u - 2).
*/
const double *dtkNurbsSurfaceDataOCCT::uKnots(void) const
{
    return m_knots_u.data();
}

/*! \fn dtkNurbsSurfaceDataOCCT::uvBounds(double* uv_bounds) const
  Writes in \a uv_bounds the parametric bounds of the surface: the
  minimal and maximal parameters in the "u" direction, and then in the
  "v" direction.

  \a uv_bounds : array of size 4
*/
void dtkNurbsSurfaceDataOCCT::uvBounds(double* uv_bounds) const
{
    d->Bounds(uv_bounds[0], uv_bounds[1], uv_bounds[2], uv_bounds[3]);
}

/*! \fn dtkNurbsSurfaceDataOCCT::vKnots(void) const
  Returns a pointer to the array storing the knots of the surface along the "v" direction. The array is of size : (nb_cp_v + order_v - 2).
*/
const double *dtkNurbsSurfaceDataOCCT::vKnots(void) const
{
    return m_knots_v.data();
}

double dtkNurbsSurfaceDataOCCT::uPeriod(void) const
{
    return d->UPeriod();
}

double dtkNurbsSurfaceDataOCCT::vPeriod(void) const
{
    return d->VPeriod();
}

/*! \fn dtkNurbsSurfaceDataOCCT::isPointCulled(const dtkContinuousGeometryPrimitives::Point_2& point) const
  Returns true if \a point is culled by the trim loops of the surface, else returns false.
*/
bool dtkNurbsSurfaceDataOCCT::isPointCulled(const dtkContinuousGeometryPrimitives::Point_2& point) const
{
    //This works because the outter trim loop is necessarily of type inner (CCW)
    if (m_trim_loops.size() == 0) {
      return false;
        //dtkFatal() << "Trying to test if a point is culled when no trim loops are recorded.";
    } else if (m_trim_loops.size() == 1) {
        if((*m_trim_loops.begin())->isPointCulled(point)) {
            return true;
        } else {
            return false;
        }
    } else {
        std::size_t culled = 0;
        // std::size_t not_culled = 0;
/*
        for (auto trim_loop = m_trim_loops.begin(); trim_loop != m_trim_loops.end(); ++trim_loop) {
            if((*trim_loop)->isPointCulled(point) && (*trim_loop)->type() == dtkContinuousGeometryEnums::outer) {
                ++culled;
            } else if (!(*trim_loop)->isPointCulled(point) && (*trim_loop)->type() == dtkContinuousGeometryEnums::inner) {
                ++not_culled;
            }
        }
        if (culled == 0 && not_culled == 0) {
            return true;
        }
        if (culled == not_culled) {
            return true;
        } else {
            return false;
        }
*/

        std::size_t not_culled_inner = 0;
        std::size_t not_culled_outer = 0;

        for (auto trim_loop = m_trim_loops.begin(); trim_loop != m_trim_loops.end(); ++trim_loop) {

          if ( (*trim_loop)->type() ==  dtkContinuousGeometryEnums::inner ) {
            if ( (*trim_loop)->isPointCulled(point) == true ) ++culled;
            else ++not_culled_inner;
          }
          else if ( (*trim_loop)->type() ==  dtkContinuousGeometryEnums::outer ) {
            if ( (*trim_loop)->isPointCulled(point) == true ) ++culled;
            else ++not_culled_outer;
          }

        }

        if (culled > 0) {
          return true;
        }
        else {
          return false;
        }

    }

    return false; //Should never happen
}

auto copy_xyz = [](const auto& gp_obj, double* output) {
    const auto& xyz = gp_obj.XYZ();
    std::copy(xyz.GetData(), xyz.GetData() + 3, output);
 };

/*! \fn dtkNurbsSurfaceDataOCCT::evaluatePoint(double p_u, double p_v, double *r_point) const
  Writes in \a r_point the evaluation at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_point : array of size 3 to store the result of the evaluation
*/
void dtkNurbsSurfaceDataOCCT::evaluatePoint(double p_u, double p_v, double* r_point) const
{
    auto p = d->Value(p_u, p_v);
    copy_xyz(p, r_point);
}

/*! \fn dtkNurbsSurfaceDataOCCT::evaluateNormal(double p_u, double p_v, double *r_normal) const
  Writes in \a r_normal the normal evaluation at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_normal : array of size 3 to store the result of the normal evaluation
*/
void dtkNurbsSurfaceDataOCCT::evaluateNormal(double p_u, double p_v, double* r_normal) const
{
    gp_Pnt p;
    gp_Vec der1_u;
    gp_Vec der1_v;
    d->D1(p_u, p_v, p, der1_u, der1_v);
    gp_Vec n = der1_u ^ der1_v;
    try {
      n.Normalize();
    } catch(Standard_ConstructionError&) { // null-vector, or almost null, let's return it as it is...
    }
    copy_xyz(n, r_normal);
}

/*! \fn dtkNurbsSurfaceDataOCCT::evaluate1stDer(double p_u, double p_v, double *r_point, double *r_u_deriv, double *r_v_deriv) const
  Writes in \a r_point the evaluation, in \a r_u_deriv the first derivative in "u" direction, in \a r_v_deriv the first derivative in the "v" direction at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_point : array of size 3 to store the result of the evaluation

  \a r_u_deriv : array of size 3 to store the result of the first derivative evaluation in the "u" direction

  \a r_v_deriv : array of size 3 to store the result of the first derivative evaluation in the "v" direction
*/
void dtkNurbsSurfaceDataOCCT::evaluate1stDer(double p_u, double p_v, double* r_point, double* r_u_deriv, double* r_v_deriv) const
{
    gp_Pnt p;
    gp_Vec der1_u;
    gp_Vec der1_v;
    d->D1(p_u, p_v, p, der1_u, der1_v);
    copy_xyz(p, r_point);
    copy_xyz(der1_u, r_u_deriv);
    copy_xyz(der1_v, r_v_deriv);
}

/*! \fn dtkNurbsSurfaceDataOCCT::evaluate1stDer(double p_u, double p_v, double *r_u_deriv, double *r_v_deriv) const
  Writes in \a r_u_deriv the first derivative in "u" direction and in \a r_v_deriv the first derivative in the "v" direction at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_u_deriv : array of size 3 to store the result of the first derivative evaluation in the "u" direction

  \a r_v_deriv : array of size 3 to store the result of the first derivative evaluation in the "v" direction
*/
void dtkNurbsSurfaceDataOCCT::evaluate1stDer(double p_u, double p_v, double* r_u_deriv, double* r_v_deriv) const
{
    gp_Pnt p;
    gp_Vec der1_u;
    gp_Vec der1_v;
    d->D1(p_u, p_v, p, der1_u, der1_v);
    copy_xyz(der1_u, r_u_deriv);
    copy_xyz(der1_v, r_v_deriv);
}

/*! \fn dtkNurbsSurfaceDataOCCT::decomposeToRationalBezierSurfaces(std::vector< std::pair< dtkRationalBezierSurface *, double * > >& r_rational_bezier_surfaces) const
  Decomposes the NURBS surface into dtkRationalBezierSurface \a r_rational_bezier_surfaces.

  \a r_rational_bezier_surfaces : vector in which the dtkRationalBezierSurface are stored, along with their limits in the NURBS surface parameter space

  Not sure what happens when the not extreme nodes are of multiplicity >1.
*/
void dtkNurbsSurfaceDataOCCT::decomposeToRationalBezierSurfaces(std::vector< std::pair< dtkRationalBezierSurface *, double * > >& r_rational_bezier_surfaces) const
{
    GeomConvert_BSplineSurfaceToBezierSurface convert{d};

    auto nb_u = convert.NbUPatches();
    auto nb_v = convert.NbVPatches();

    TColGeom_Array2OfBezierSurface bezier_surfaces(1, nb_u, 1, nb_v);
    TColStd_Array1OfReal uknots(1, nb_u+1);
    TColStd_Array1OfReal vknots(1, nb_v+2);
    convert.Patches(bezier_surfaces);
    convert.UKnots(uknots);
    convert.VKnots(vknots);

    for (auto i = 1; i <= nb_u; ++i) {
        for (auto j = 1; j <= nb_v; ++j) {
            double* knots = new double[4]; /// @TODO: memory leak
            knots[0] = uknots[i];   // u0
            knots[1] = vknots[j];   // v0
            knots[2] = uknots[i+1]; // u1
            knots[3] = vknots[j+1]; // v1

            if (knots[0] < knots[2] && knots[1] < knots[3]) {
                dtkRationalBezierSurfaceDataOCCT* occt_bezier = new dtkRationalBezierSurfaceDataOCCT();
                auto bezier_surface_handle = bezier_surfaces.Value(i, j);
                occt_bezier->create(bezier_surface_handle);
                r_rational_bezier_surfaces.emplace_back(new dtkRationalBezierSurface{occt_bezier}, knots);
            }
        }
    }
}

/*! \fn dtkNurbsSurfaceDataOCCT::aabb(double* r_aabb) const
  Writes in \a r_aabb the [xmin, ymin, zmin, xmax, ymax, zmax] coordinates.

  \a r_aabb : array of size 6 to store the limits coordinates
*/
void dtkNurbsSurfaceDataOCCT::aabb(double* r_aabb) const
{
    Bnd_Box bb;
    BndLib_AddSurface::Add(GeomAdaptor_Surface(this->d), 0., bb);
    bb.Get(r_aabb[0], r_aabb[1], r_aabb[2], r_aabb[3], r_aabb[4], r_aabb[5]);
}

/*! \fn dtkNurbsSurfaceDataOCCT::extendedAabb(double *r_aabb, double factor) const
  Writes in \a r_aabb the [xmin, ymin, zmin, xmax, ymax, zmax] coordinates of an extened bounding box of factor \a factor. The extension factor extends the diagonal.

  \a r_aabb : array of size 6 to store the extended limits coordinates

  \a factor : factor used to lengthen the diagonal of the axis aligned bounding box
*/
void dtkNurbsSurfaceDataOCCT::extendedAabb(double* r_aabb, double factor) const
{
    aabb(r_aabb);
    factor -= 1.;
    factor /= 2.;
    auto dx = factor * (r_aabb[3] - r_aabb[0]);
    auto dy = factor * (r_aabb[4] - r_aabb[1]);
    auto dz = factor * (r_aabb[5] - r_aabb[2]);
    r_aabb[0] -= dx;
    r_aabb[1] -= dy;
    r_aabb[2] -= dz;
    r_aabb[3] += dx;
    r_aabb[4] += dy;
    r_aabb[5] += dz;
}

/*! \fn dtkNurbsSurfaceDataOCCT::clone(void) const
  Clone
*/
dtkNurbsSurfaceDataOCCT* dtkNurbsSurfaceDataOCCT::clone(void) const
{
    return new dtkNurbsSurfaceDataOCCT(*this);
}

#include <iomanip>
void dtkNurbsSurfaceDataOCCT::print(std::ostream& stream) const
{
    stream.precision(17);
    stream << "#dtkNurbsSurfaceDataOCCT" << std::endl;
    stream << "u_degree " << d->UDegree() << std::endl;
    stream << "v_degree " << d->VDegree() << std::endl;
    stream << "u_nb_cps " << d->NbUPoles() << std::endl;
    stream << "v_nb_cps " << d->NbVPoles() << std::endl;
    for(auto uknot : m_knots_u) {
        stream << uknot << " ";
    }
    stream << std::endl;
    for(auto vknot : m_knots_v) {
        stream << vknot << " ";
    }
    stream << std::endl;
    for(auto i = 1, nb_u = d->NbUPoles(); i <= nb_u ; ++i) {
        for(auto j = 1, nb_v = d->NbVPoles(); j <= nb_v; ++j) {
            std::array<double, 3> point;
            controlPoint(i, j, point.data());
            stream << point[0] << " " << point[1] << " " << point[2] << " " << d->Weight(i, j) << '\n';
        }
    }
    stream << "#dtkNurbsSurfaceDataOCCT" << std::endl;
}

//
// dtkNurbsSurfaceDataOCCT.cpp ends here

// Local Variables:
// c-basic-offset: 4
// End:
