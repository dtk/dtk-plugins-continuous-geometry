// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <QtCore>

#include <dtkContinuousGeometry>

#include <dtkAbstractNurbsSurfaceData>

class dtkNurbsSurfaceDataOCCTPlugin : public dtkAbstractNurbsSurfaceDataPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkAbstractNurbsSurfaceDataPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkNurbsSurfaceDataOCCTPlugin" FILE "dtkNurbsSurfaceDataOCCTPlugin.json")

public:
     dtkNurbsSurfaceDataOCCTPlugin(void) {}
    ~dtkNurbsSurfaceDataOCCTPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

//
// dtkNurbsSurfaceDataOCCTPlugin.h ends here
