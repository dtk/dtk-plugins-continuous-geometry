// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#include "dtkNurbsSurfaceDataOCCT.h"
#include "dtkNurbsSurfaceDataOCCTPlugin.h"

#include <dtkCore>
#include <dtkContinuousGeometry>

// ///////////////////////////////////////////////////////////////////
// dtkNurbsSurfaceDataOCCTPlugin
// ///////////////////////////////////////////////////////////////////

void dtkNurbsSurfaceDataOCCTPlugin::initialize(void)
{
    dtkContinuousGeometry::abstractNurbsSurfaceData::pluginFactory().record("dtkNurbsSurfaceDataOCCT", dtkNurbsSurfaceDataOCCTCreator);
}

void dtkNurbsSurfaceDataOCCTPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkNurbsSurfaceDataOCCT)

//
// dtkNurbsSurfaceDataOCCTPlugin.cpp ends here
