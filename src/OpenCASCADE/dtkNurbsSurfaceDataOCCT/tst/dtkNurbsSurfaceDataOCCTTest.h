// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <QTest>

class dtkNurbsSurfaceDataOCCTTestCasePrivate;

class dtkNurbsSurfaceDataOCCTTestCase : public QObject
{
    Q_OBJECT

public:
    dtkNurbsSurfaceDataOCCTTestCase(void);
    ~dtkNurbsSurfaceDataOCCTTestCase(void);

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testUNbCps(void);
    void testVNbCps(void);
    void testUDegree(void);
    void testVDegree(void);
    void testControlPoint(void);
    void testWeight(void);
    void testUKnots(void);
    void testVKnots(void);
    void testSurfacePoint(void);
    void testSurfaceNormal(void);
    void testDecomposeToRationalBezierSurfaces(void);
    void testAabb(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);

private:
    dtkNurbsSurfaceDataOCCTTestCasePrivate* d;
};

//
// dtkNurbsSurfaceDataOCCTTest.h ends here
