// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <QTest>

class dtkNurbsCurveDataOCCTTestCasePrivate;

class dtkNurbsCurveDataOCCTTestCase : public QObject
{
    Q_OBJECT

public:
    dtkNurbsCurveDataOCCTTestCase(void);
    ~dtkNurbsCurveDataOCCTTestCase(void);

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testControlPoint(void);
    void testWeight(void);
    void testDegree(void);
    void testKnots(void);
    void testCurvePoint(void);
    void testCurveNormal(void);
    void testPrintOutCurve(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);

private:
    dtkNurbsCurveDataOCCTTestCasePrivate* d;
};

//
// dtkNurbsCurveDataOCCTTest.h ends here
