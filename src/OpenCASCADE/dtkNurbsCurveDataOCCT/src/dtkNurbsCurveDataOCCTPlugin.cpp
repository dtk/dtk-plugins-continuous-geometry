// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#include "dtkNurbsCurveDataOCCT.h"
#include "dtkNurbsCurveDataOCCTPlugin.h"

#include <dtkCore>
#include <dtkContinuousGeometry>

// ///////////////////////////////////////////////////////////////////
// dtkNurbsCurveDataOCCTPlugin
// ///////////////////////////////////////////////////////////////////

void dtkNurbsCurveDataOCCTPlugin::initialize(void)
{
    dtkContinuousGeometry::abstractNurbsCurveData::pluginFactory().record("dtkNurbsCurveDataOCCT", dtkNurbsCurveDataOCCTCreator);
}

void dtkNurbsCurveDataOCCTPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkNurbsCurveDataOCCT)

//
// dtkNurbsCurveDataOCCTPlugin.cpp ends here
