// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkContinuousGeometry.h>

#include <dtkNurbsCurveDataOCCTExport.h>

#include <dtkAbstractNurbsCurveData>

#include <Standard_Handle.hxx>
class Geom_BSplineCurve;

#include <atomic>
#include <vector>
#include <mutex>

class DTKNURBSCURVEDATAOCCT_EXPORT dtkNurbsCurveDataOCCT final : public dtkAbstractNurbsCurveData
{
public:
    dtkNurbsCurveDataOCCT(void);
    ~dtkNurbsCurveDataOCCT(void) final ;

public:
    void create(std::size_t dim, std::size_t nb_cp, std::size_t order, double* knots, double* cps) override;
    void create(const std::vector< dtkRationalBezierCurve* >& rational_bezier_curves) override;
    void create(const Handle(Geom_BSplineCurve)& nurbs_curve);

public:
    std::size_t degree(void) const override;

    std::size_t nbCps(void) const override;

    std::size_t dim(void) const override;

    void controlPoint(std::size_t i, double* r_cp) const override;
    void weight(std::size_t i, double* r_w) const override;

    void knots(double* r_u_knots) const override;

    const double *knots(void) const override;

public:
    void evaluatePoint(double p_u, double* r_point) const override;
    void evaluateNormal(double p_u, double* r_normal) const override;
    void evaluateNormal(double p_u, double* r_point, double* r_normal) const override;
    void evaluateCurvature(double p_u, double* r_curvature) const override;

 public:
    void decomposeToRationalBezierCurves(std::vector< std::pair< dtkRationalBezierCurve*, double* >>& r_rational_bezier_curves ) const override;
    void decomposeToRationalBezierCurves(std::vector< dtkRationalBezierCurve * >& r_rational_bezier_curves) const override;

public:
    dtkNurbsCurveDataOCCT* clone(void) const override;

private :
    Handle(Geom_BSplineCurve) d;
    mutable std::vector< std::pair< dtkRationalBezierCurve*, double* > > m_rational_bezier_curves;
    mutable std::mutex m_mutex;
    mutable std::atomic<bool> m_rational_bezier_curves_initialized = false;

    void initialize_m_rational_bezier_curves() const;

    std::vector<double> m_knots;

    void update_m_knots();
};


// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

inline dtkAbstractNurbsCurveData *dtkNurbsCurveDataOCCTCreator(void)
{
    return new dtkNurbsCurveDataOCCT();
}

//
// dtkNurbsCurveDataOCCT.h ends here
