// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkNurbsCurveDataOCCT.h"

#include <dtkAbstractRationalBezierCurveData>
#include <dtkRationalBezierCurve>

#include <dtkRationalBezierCurveDataOCCT.h>

#include <cassert>

#include <Geom_BSplineCurve.hxx>
#include <GeomConvert_BSplineCurveToBezierCurve.hxx>

#include "multiplicities.h"

/*!
  \class dtkNurbsCurveDataOCCT
  \inmodule dtkPluginsContinuousGeometry
  \brief dtkNurbsCurveDataOCCT is an openNURBS implementation of the concept dtkAbstractNurbsCurveData.

  The following lines of code show how to instantiate and use the class in pratice:
  \code
  dtkAbstractNurbsCurveData *nurbs_curve_data = dtkContinuousGeometry::abstractNurbsCurveData::pluginFactory().create("dtkNurbsCurveDataOCCT");
  dtkNurbsCurve *nurbs_curve = new dtkNurbsCurve2D(nurbs_curve_data);
  nurbs_curve->create(...);
  \endcode
*/

/*! \fn dtkNurbsCurveDataOCCT::dtkNurbsCurveDataOCCT(void)
  Instantiates the structure.

  To add the content, see \l create(std::size_t dim, std::size_t nb_cp, std::size_t order, double *knots, double *cps).
*/
dtkNurbsCurveDataOCCT::dtkNurbsCurveDataOCCT(void) : d(nullptr)
{

}

/*! \fn dtkNurbsCurveDataOCCT::~dtkNurbsCurveDataOCCT(void)
  Destroys the instance.
*/
dtkNurbsCurveDataOCCT::~dtkNurbsCurveDataOCCT(void)
{
    // ///////////////////////////////////////////////////////////////////
    // Destroys if necessary the bezier curves
    // ///////////////////////////////////////////////////////////////////
    for(auto it = m_rational_bezier_curves.begin(); it != m_rational_bezier_curves.end(); ++it) {
        delete it->first;
        delete it->second;
    }
}

/*! \fn dtkNurbsCurveDataOCCT::create(const ON_NurbsCurve& nurbs_curve)
  Not available in the dtkAbstractNurbsCurveData.

  Creates the NURBS curve by providing a NURBS curve from the openNURBS API.

  \a nurbs_curve : the NURBS curve to copy.
*/
void dtkNurbsCurveDataOCCT::create(const Handle(Geom_BSplineCurve)& nurbs_curve)
{
    d = Handle(Geom_BSplineCurve)::DownCast(nurbs_curve->Copy());
    update_m_knots();
}

/*! \fn dtkNurbsCurveDataOCCT::create(std::size_t dim, std::size_t nb_cp, std::size_t order, double *knots, double *cps)
  Creates a dtkNurbsCurveDataOCCT by providing the required content for a nD NURBS curve.

  \a dim : dimension of the space in which lies the curve

  \a nb_cp : number of control points

  \a order : order

  \a knots : array containing the knots

  \a cps : array containing the weighted control points, specified as : [x0, y0, z0, w0, ...]
*/
void dtkNurbsCurveDataOCCT::create(std::size_t, std::size_t nb_cp, std::size_t order, double *knots, double *cps)
{
    TColgp_Array1OfPnt poles(1, nb_cp);
    TColStd_Array1OfReal weights(1, nb_cp);

    for(std::size_t i = 0; i < nb_cp; ++i) {
        /// @TODO: use a loop more efficient with pointers instead of ChangeValue
        poles.ChangeValue(i+1) = gp_Pnt{cps[0], cps[1], cps[2]};
        weights.ChangeValue(i+1) = cps[3];
        cps += 4;
    }

    auto knots_and_multiplicities = multiplicities(knots, order+nb_cp-2);
    const auto& knots_ = knots_and_multiplicities.first;
    const auto& mults_ = knots_and_multiplicities.second;
    assert(knots_.size() == mults_.size());
    TColStd_Array1OfReal occt_knots(1, knots_.size());
    std::copy(knots_.begin(), knots_.end(), &occt_knots.ChangeValue(1));
    TColStd_Array1OfInteger occt_mults(1, mults_.size());
    std::copy(mults_.begin(), mults_.end(), &occt_mults.ChangeValue(1));

    /*
      Difference with the OpenNURBS API: the sum of multiplicities
      must be order+nb_cp and not order+nb_cp-2.
      See https://developer.rhino3d.com/guides/opennurbs/superfluous-knots/
    */
    ++occt_mults.ChangeFirst();
    ++occt_mults.ChangeLast();

    Standard_Integer degree_ = order - 1;
    assert(std::accumulate(mults_.begin(), mults_.end(), 0) ==
           static_cast<int>(order + nb_cp - 2));

    d = new Geom_BSplineCurve{poles, weights, occt_knots, occt_mults, degree_, false};
    auto nb_knots = order + nb_cp - 2;
    m_knots.resize(nb_knots);
    std::copy(knots, knots + nb_knots, &m_knots[0]);
}

/*! \fn dtkNurbsCurveDataOCCT::create(const std::vector< dtkRationalBezierCurve* >& rational_bezier_curves)
  Creates the NURBS curve by concatenating connected rational Bezier curves.

  \a rational_bezier_curves : the rational Bezier curves to concatenante

  The \a rational_bezier_curves must be given in order.

  The curves are copied.
*/
void dtkNurbsCurveDataOCCT::create(const std::vector< dtkRationalBezierCurve* >& rational_bezier_curves)
{
    Q_ASSERT(rational_bezier_curves.size() > 0);
    std::vector<dtkRationalBezierCurve*> rational_bezier_curves_new;
    rational_bezier_curves_new.reserve(rational_bezier_curves.size());
    // ///////////////////////////////////////////////////////////////////
    // Makes sure that all the rational_bezier_curves are connected (C0)
    // ///////////////////////////////////////////////////////////////////
    dtkContinuousGeometryPrimitives::Point_3 p_0(0., 0., 0.);
    dtkContinuousGeometryPrimitives::Point_3 p_n(0., 0., 0.);
    std::size_t degree = (*rational_bezier_curves.begin())->degree();
    for (auto bezier_curve : rational_bezier_curves) {
        if (bezier_curve->degree() > degree) degree = bezier_curve->degree();
    }

    for(auto bezier_curve = rational_bezier_curves.begin(); bezier_curve != std::prev(rational_bezier_curves.end()); ++bezier_curve) {
        if ((*bezier_curve)->degree() != degree) {
            dtkWarn() << "The degrees of the curves vary: " << (*bezier_curve)->degree() << ", " << degree;
            dtkWarn() << "Degree elevation";
            std::vector<dtkContinuousGeometryPrimitives::Point_3> cps_new;
            cps_new.reserve((*bezier_curve)->degree() + 1);
            for (std::size_t i = 0; i < (*bezier_curve)->degree() + 1; ++i) {
                dtkContinuousGeometryPrimitives::Point_3 cp(0., 0., 0.);
                (*bezier_curve)->controlPoint(i, cp.data());
                cps_new.push_back(cp);
            }

            dtkContinuousGeometryPrimitives::Point_3 cp_0 = cps_new[0];
            dtkContinuousGeometryPrimitives::Point_3 cp_n = cps_new.back();

            std::vector<dtkContinuousGeometryPrimitives::Point_3> cps_tmp;
            for (std::size_t i = 1; i < degree - (*bezier_curve)->degree() + 1; ++i) {
                int degree_new = (*bezier_curve)->degree() + i;
                cps_tmp.reserve(degree_new + 1);
                // first control point
                cps_tmp.push_back(cp_0);
                // control points in between
                for (int j = 1; j < degree_new; ++j) {
                    dtkContinuousGeometryPrimitives::Point_3 cp_1 = cps_new[j - 1];
                    dtkContinuousGeometryPrimitives::Point_3 cp_2 = cps_new[j];

                    dtkContinuousGeometryPrimitives::Point_3 cp_j(0., 0., 0.);
                    cp_j = cp_1*(double(j)/double(degree_new)) + cp_2*(1. - double(j)/double(degree_new));
                    cps_tmp.push_back(cp_j);
                }
                // last control point
                cps_tmp.push_back(cp_n);

                cps_new.clear();
                cps_new = cps_tmp;
                cps_tmp.clear();
            }

            // \todo the weights of new control points
            double* cps = new double[4*(degree + 1)];
            for (std::size_t i = 0; i < degree + 1; ++i) {
                cps[4*i] = cps_new[i][0];
                cps[4*i + 1] = cps_new[i][1];
                cps[4*i + 2] = cps_new[i][2];
                cps[4*i + 3] = 1.; // the weight is 1 by default, which is actually not reasonable.
            }

            dtkAbstractRationalBezierCurveData *rational_bezier_curve_data = dtkContinuousGeometry::abstractRationalBezierCurveData::pluginFactory().create("dtkRationalBezierCurveDataOCCT");
            rational_bezier_curve_data->create(degree + 1, cps);
            dtkRationalBezierCurve* rational_bezier_curve_3d = new dtkRationalBezierCurve(rational_bezier_curve_data);

            rational_bezier_curves_new.push_back(rational_bezier_curve_3d);

            //dtkFatal() << "The dtkNurbsCurveDataOCCT is not valid";
        }
        else {
            rational_bezier_curves_new.push_back(*bezier_curve);
        }
        (*bezier_curve)->controlPoint((*bezier_curve)->degree(), p_n.data());
        (*std::next(bezier_curve))->controlPoint(0, p_0.data());
        if(dtkContinuousGeometryTools::squaredDistance(p_0, p_n) > 1.e-9) {
            dtkWarn() << "The end/begin control points of two following bezier curves do not match";
            dtkWarn() << qSetRealNumberPrecision(17) << p_0 << " and " << p_n;
            dtkFatal() << "The dtkNurbsCurveDataOCCT is not valid";
        }
    }
    // ///////////////////////////////////////////////////////////////////
    // Copies the rational bezier curves
    // ///////////////////////////////////////////////////////////////////
    m_rational_bezier_curves.reserve(rational_bezier_curves.size());
    std::size_t i = 0;
    for(auto bezier_curve = rational_bezier_curves.begin(); bezier_curve != rational_bezier_curves.end(); ++bezier_curve) {
        double* knots = new double[2];
        knots[0] = i;
        knots[1] = i + 1;
        m_rational_bezier_curves.push_back(std::make_pair(new dtkRationalBezierCurve(*(*bezier_curve)), knots));
        ++i;
    }

    // /////////////////////////////////////////////////////////////////
    // Initializes the Open Nurbs NURBS curve
    // /////////////////////////////////////////////////////////////////
    std::size_t nb_cp = m_rational_bezier_curves.size() * degree + 1;
    dtkFatal() << "Not yet implemented";
#if 0
    // d = ON_NurbsCurve::New(3, true, degree + 1, nb_cp);

    // ///////////////////////////////////////////////////////////////////
    // In its call to New, openNURBS reserves enough space at d->m_knot, d->m_cv to store the values of knots and control points
    // Each degree control points, the curve is interpolated
    // ///////////////////////////////////////////////////////////////////
    for(std::size_t j = 0; j < degree; ++j) {
        d->m_knot[j] = 0;
    }
    for(std::size_t i = 0; i < m_rational_bezier_curves.size(); ++i) {
        for(std::size_t j = 0; j < degree; ++j) {
            d->m_knot[(i + 1) * degree + j] = i + 1;
        }
    }

    dtkContinuousGeometryPrimitives::Point_3 p(0., 0., 0.);
    double w = 0.;
    m_rational_bezier_curves[0].first->controlPoint(0, p.data());
    m_rational_bezier_curves[0].first->weight(0, &w);
    d->m_cv[0] = p[0] * w;
    d->m_cv[1] = p[1] * w;
    d->m_cv[2] = p[2] * w;
    d->m_cv[3] = w;

    for(std::size_t i = 0; i < m_rational_bezier_curves.size(); ++i) {
        for(std::size_t j = 1; j <= degree; ++j) {
            m_rational_bezier_curves[i].first->controlPoint(j, p.data());
            m_rational_bezier_curves[i].first->weight(j, &w);
            d->m_cv[4 * (i * degree + j)] = p[0] * w;
            d->m_cv[4 * (i * degree + j) + 1] = p[1] * w;
            d->m_cv[4 * (i * degree + j) + 2] = p[2] * w;
            d->m_cv[4 * (i * degree + j) + 3] = w;
        }
    }

    if(!d->IsValid()) {
        dtkFatal() << "The dtkNurbsCurveDataOCCT is not valid";
    }
#endif
}

/*! \fn dtkNurbsCurveDataOCCT::degree(void) const
  Returns the degree of the curve.
*/
std::size_t dtkNurbsCurveDataOCCT::degree(void) const
{
    return d->Degree();
}

/*! \fn dtkNurbsCurveDataOCCT::nbCps(void) const
  Returns the number of weighted control points controlling the curve.
*/
std::size_t dtkNurbsCurveDataOCCT::nbCps(void) const
{
    return d->NbPoles();;
}

/*! \fn dtkNurbsCurveDataOCCT::dim(void) const
  Returns the dimension of the space in which the curve lies.
*/
std::size_t dtkNurbsCurveDataOCCT::dim(void) const
{
    return 3;
}

/*! \fn dtkNurbsCurveDataOCCT::controlPoint(std::size_t i, double *r_cp) const
  Writes in \a r_cp the \a i th weighted control point coordinates[xi, yi, zi].

  \a i : index of the weighted control point

  \a r_cp : array of size 3 to store the weighted control point coordinates[xi, yi, zi]
*/
void dtkNurbsCurveDataOCCT::controlPoint(std::size_t i, double* r_cp) const
{
    const auto& pole = d->Pole(i+1);
    const auto& xyz = pole.XYZ();
    std::copy(xyz.GetData(), xyz.GetData() + 3, r_cp);
}

/*! \fn dtkNurbsCurveDataOCCT::weight(std::size_t i, double *r_w) const
  Writes in \a r_w the \a i th weighted control point weight wi.

  \a i : index of the weighted control point

  \a r_w : array of size 1 to store the weighted control point weight wi
*/
void dtkNurbsCurveDataOCCT::weight(std::size_t i, double* r_w) const
{
    *r_w = d->Weight(i+1);
}

void dtkNurbsCurveDataOCCT::update_m_knots()
{
    m_knots.resize(d->NbPoles() + d->Degree() - 1);
    auto* r_knots = m_knots.data();
    for(int i = 1, nb = d->NbKnots(); i <= nb; ++i) {
        const auto knot = d->Knot(i);
        auto mult = d->Multiplicity(i);
        if(i == 1 || i == nb) mult -= 1;
        for(int j = 0; j < mult; ++j) {
            *r_knots++ = knot;
        }
    }
    assert((m_knots.data() + (d->NbPoles() + d->Degree() - 1)) == r_knots);
}

/*! \fn dtkNurbsCurveDataOCCT::knots(double *r_knots) const
  Writes in \a r_knots the knots of the curve.

  \a r_knots : array of size (nb_cp + order - 2) to store the knots
*/
void dtkNurbsCurveDataOCCT::knots(double* r_knots) const
{
    std::copy(m_knots.begin(), m_knots.end(), r_knots);
}

/*! \fn dtkNurbsCurveDataOCCT::knots(void) const
  Returns a pointer to the array storing the knots of the curve. The array is of size : (nb_cp + order - 2).
*/
const double *dtkNurbsCurveDataOCCT::knots(void) const
{
    return m_knots.data();
}

/*! \fn dtkNurbsCurveDataOCCT::evaluatePoint(double p_u, double *r_point) const
  Writes in \a r_point the evaluation at parameter \a p_u.

  \a p_u : parameter

  \a r_point : array of size 3 to store the result of the evaluation
*/
void dtkNurbsCurveDataOCCT::evaluatePoint(double p_u, double* r_point) const
{
    auto xyz = d->Value(p_u).XYZ();
    std::copy(xyz.GetData(), xyz.GetData() + 3, r_point);
}

/*! \fn dtkNurbsCurveDataOCCT::evaluateNormal(double p_u, double *r_normal) const
  Writes in \a r_normal the normal evaluation at parameter \a p_u.

  \a p_u : parameter

  \a r_normal : array of size 3 to store the result of the normal evaluation
*/
void dtkNurbsCurveDataOCCT::evaluateNormal(double p_u, double* r_normal) const
{
    gp_Pnt p;
    gp_Vec der1;
    d->D1(p_u, p, der1);
    const auto& xyz = der1.XYZ();
    std::copy(xyz.GetData(), xyz.GetData() + 3, r_normal);
}

/*! \fn dtkNurbsCurveDataOCCT::evaluateNormal(double p_u, double *r_point, double *r_normal) const
  Writes in \a r_normal the normal evaluation at parameter \a p_u.

  \a p_u : parameter

  \a r_point : array of size 3 to store the result of the evaluation

  \a r_normal : array of size 3 to store the result of the normal evaluation
*/
void dtkNurbsCurveDataOCCT::evaluateNormal(double p_u, double* r_point, double* r_normal) const
{
    gp_Pnt p;
    gp_Vec der1;
    d->D1(p_u, p, der1);
    const auto& p_xyz = p.XYZ();
    std::copy(p_xyz.GetData(), p_xyz.GetData() + 3, r_point);
    const auto& xyz = der1.XYZ();
    std::copy(xyz.GetData(), xyz.GetData() + 3, r_normal);
}

void dtkNurbsCurveDataOCCT::evaluateCurvature(double p_u, double* r_curvature) const
{
    gp_Pnt p;
    gp_Vec der1;
    gp_Vec der2;
    d->D2(p_u, p, der1, der2);
    const auto& xyz = der2.XYZ();
    std::copy(xyz.GetData(), xyz.GetData() + 3, r_curvature);
}

void dtkNurbsCurveDataOCCT::initialize_m_rational_bezier_curves() const {
    std::lock_guard guard{m_mutex};
    if(m_rational_bezier_curves_initialized) return;
    GeomConvert_BSplineCurveToBezierCurve convert{d};
    TColStd_Array1OfReal knots(1, convert.NbArcs() + 1);
    convert.Knots(knots);
    for(auto i = 1, nb = convert.NbArcs(); i <= nb; ++i) {
        dtkRationalBezierCurveDataOCCT* occt_bezier = new dtkRationalBezierCurveDataOCCT();
        occt_bezier->create(convert.Arc(i));
        double* pair_of_knots = new double[2];
        pair_of_knots[0] = knots[i];
        pair_of_knots[1] = knots[i+1];
        this->m_rational_bezier_curves.emplace_back(new dtkRationalBezierCurve{occt_bezier}, pair_of_knots);
    }
    m_rational_bezier_curves_initialized = true;
}

/*! \fn dtkNurbsCurveDataOCCT::decomposeToRationalBezierCurves(std::vector< std::pair< dtkRationalBezierCurve*, double* >>& r_rational_bezier_curves) const
  Decomposes the NURBS curve into dtkRationalBezierCurve \a r_rational_bezier_curves.

  \a r_rational_bezier_curves : vector in which the dtkRationalBezierCurve are stored, along with their limits in the NURBS curve parameter space
*/
void dtkNurbsCurveDataOCCT::decomposeToRationalBezierCurves(std::vector< std::pair< dtkRationalBezierCurve*, double* >>& r_rational_bezier_curves) const
{
    if(!m_rational_bezier_curves_initialized) {
        initialize_m_rational_bezier_curves();
    }
    r_rational_bezier_curves = m_rational_bezier_curves;
}

/*!  \fn dtkNurbsCurveDataOCCT::decomposeToRationalBezierCurves(std::vector< dtkRationalBezierCurve * >& r_rational_bezier_curves) const
  Decomposes the NURBS curve into dtkRationalBezierCurve \a r_rational_bezier_curves.

  \a r_rational_bezier_curves : vector in which the dtkRationalBezierCurve are stored

  To recover the limits of the rational Bezier curves in the NURBS parameter space, check : \l decomposeToRationalBezierCurves(std::vector< std::pair< dtkRationalBezierCurve*, double* > >& r_rational_bezier_curves).
*/
void dtkNurbsCurveDataOCCT::decomposeToRationalBezierCurves(std::vector< dtkRationalBezierCurve * >& r_rational_bezier_curves) const
{
    if(!m_rational_bezier_curves_initialized) {
        initialize_m_rational_bezier_curves();
    }
    for(auto bezier_curve = m_rational_bezier_curves.begin(); bezier_curve != m_rational_bezier_curves.end(); ++bezier_curve) {
        r_rational_bezier_curves.push_back(bezier_curve->first);
    }
}

/*!  \fn dtkNurbsCurveDataOCCT::clone(void) const
  Clone.
*/
dtkNurbsCurveDataOCCT* dtkNurbsCurveDataOCCT::clone(void) const
{
    dtkFatal() << "not implemented";
    return nullptr; // new dtkNurbsCurveDataOCCT(*this);
}

//
// dtkNurbsCurveDataOCCT.cpp ends here

// Local Variables:
// c-basic-offset: 4
// End:
