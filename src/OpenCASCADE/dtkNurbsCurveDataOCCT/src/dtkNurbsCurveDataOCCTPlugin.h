// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <QtCore>

#include <dtkContinuousGeometry>

#include <dtkAbstractNurbsCurveData>

class dtkNurbsCurveDataOCCTPlugin : public dtkAbstractNurbsCurveDataPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkAbstractNurbsCurveDataPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkNurbsCurveDataOCCTPlugin" FILE "dtkNurbsCurveDataOCCTPlugin.json")

public:
     dtkNurbsCurveDataOCCTPlugin(void) {}
    ~dtkNurbsCurveDataOCCTPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

//
// dtkNurbsCurveDataOCCTPlugin.h ends here
