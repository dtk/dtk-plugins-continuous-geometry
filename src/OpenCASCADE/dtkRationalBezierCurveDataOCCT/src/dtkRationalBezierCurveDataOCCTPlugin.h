// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <QtCore>

#include <dtkContinuousGeometry>

#include <dtkAbstractRationalBezierCurveData>

class dtkRationalBezierCurveDataOCCTPlugin : public dtkAbstractRationalBezierCurveDataPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkAbstractRationalBezierCurveDataPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkRationalBezierCurveDataOCCTPlugin" FILE "dtkRationalBezierCurveDataOCCTPlugin.json")

public:
     dtkRationalBezierCurveDataOCCTPlugin(void) {}
    ~dtkRationalBezierCurveDataOCCTPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

//
// dtkRationalBezierCurveDataOCCTPlugin.h ends here
