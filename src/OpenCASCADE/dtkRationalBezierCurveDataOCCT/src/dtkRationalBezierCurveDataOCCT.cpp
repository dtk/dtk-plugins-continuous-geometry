// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkRationalBezierCurveDataOCCT.h"

#include <dtkRationalBezierCurve>

#include <cassert>

#include <Geom_BezierCurve.hxx>
#include <Geom_BSplineCurve.hxx>
#include <Bnd_Box.hxx>
#include <BndLib_Add3dCurve.hxx>
#include <GeomAdaptor_Curve.hxx>
#include <GeomConvert.hxx>
#include <GeomConvert_CompCurveToBSplineCurve.hxx>
#include <GeomConvert_BSplineCurveToBezierCurve.hxx>
/*!
  \class dtkRationalBezierCurveDataOCCT
  \inmodule dtkPluginsContinuousGeometry
  \brief dtkRationalBezierCurveDataOCCT is an openNURBS implementation of the concept dtkAbstractRationalBezierCurveData.

  The following lines of code show how to instanciate and use the class in pratice:
  \code
  dtkAbstractRationalBezierCurveData *rational_bezier_curve_data = dtkContinuousGeometry::abstractRationalBezierCurveData::pluginFactory().create("dtkRationalBezierCurveDataOCCT");
  dtkRationalBezierCurve *rational_bezier_curve = new dtkRationalBezierCurve(rational_bezier_curve_data);
  rational_bezier_curve->create(...);
  \endcode
*/

/*! \fn dtkRationalBezierCurveDataOCCT::dtkRationalBezierCurveDataOCCT(void)
  Instanciates the structure.

  To add the content, see \l create(std::size_t order, double* cps) const.
*/
dtkRationalBezierCurveDataOCCT::dtkRationalBezierCurveDataOCCT(void) : d(nullptr) {}

/*! \fn dtkRationalBezierCurveDataOCCT::dtkRationalBezierCurveDataOCCT(const dtkRationalBezierCurveDataOCCT& curve_data)
  Copy constructor.

  Not available in the dtkAbstractRationalBezierCurveData.

  Creates the rational Bezier curve by copying the provided rational Bezier curve.

  \a curve_data : the rational Bezier curve to copy.
*/
dtkRationalBezierCurveDataOCCT::dtkRationalBezierCurveDataOCCT(const dtkRationalBezierCurveDataOCCT& curve_data)
{
    d = Handle(Geom_BezierCurve)::DownCast(curve_data.d->Copy());
}

/*! \fn dtkRationalBezierCurveDataOCCT::~dtkRationalBezierCurveDataOCCT(void)
  Destroys the instance.
*/
dtkRationalBezierCurveDataOCCT::~dtkRationalBezierCurveDataOCCT(void)
{
}

/*!
  Not available in the dtkAbstractRationalBezierCurveData.

  Creates the rational Bezier curve by providing a rational Bezier curve from the OpenCASCADE API.

  \a bezier_curve : the rational Bezier curve to copy.
*/
void dtkRationalBezierCurveDataOCCT::create(const Handle(Geom_BezierCurve)& bezier_curve)
{
    d = Handle(Geom_BezierCurve)::DownCast(bezier_curve->Copy());
}

/*! \fn dtkRationalBezierCurveDataOCCT::create(std::size_t order, double *cps) const
  Creates the rational Bezier curve by providing the required content.

 \a order : order

 \a cps : array containing the weighted control points, specified as :
 [x0, y0, z0, w0, ...]
*/
void dtkRationalBezierCurveDataOCCT::create(std::size_t order, double* cps) const
{
    TColgp_Array1OfPnt poles(1, order);
    TColStd_Array1OfReal weights(1, order);

    for(std::size_t i = 0; i < order; ++i) {
        /// @TODO: use a loop more efficient with pointers instead of ChangeValue
        poles.ChangeValue(i+1) = gp_Pnt{cps[0], cps[1], cps[2]};
        weights.ChangeValue(i+1) = cps[3];
        cps += 4;
    }
    d = new Geom_BezierCurve{poles, weights};
}

/*! \fn void dtkRationalBezierCurveDataOCCT::create(std::string path) const
  Creates a rational Bezier curve by providing a path to a file storing the required information to create a rational Bezier curve.

  \a path : a valid path to the file containing the information regarding a given rational Bezier curve
*/
void dtkRationalBezierCurveDataOCCT::create(std::string path) const
{
    QFile file(QString::fromStdString(path));
    QIODevice *in = &file;

    QIODevice::OpenMode mode = QIODevice::ReadOnly;
    mode |= QIODevice::Text;

    // to avoid troubles with floats separators ('.' and not ',')
    QLocale::setDefault(QLocale::c());
#if defined (Q_OS_UNIX) && !defined(Q_OS_MAC)
    setlocale(LC_NUMERIC, "C");
#endif

    if (!in->open(mode)) {
        dtkFatal() << Q_FUNC_INFO << "The file could not be found, please verify the path";
        return;
    }

    QString line = in->readLine().trimmed();
    //Run tests on the file to check its validity TODO more
    if ( line != "#dtkRationalBezierCurveData"){
        dtkFatal() << Q_FUNC_INFO << "file not in a dtkRationalBezierCurveData format.";
        return;
    }
    // std::size_t dim = 3;
    QRegExp re = QRegExp("\\s+");
    line = in->readLine().trimmed();
    QStringList vals = line.split(re);
    std::size_t order = vals[1].toInt() + 1;

    std::vector<double> cps(order*4);
    for(std::size_t i = 0; i < order; ++i) {
        line = in->readLine().trimmed();
        if (line.startsWith("#")) {
            continue;
        }
        std::istringstream lin(line.toStdString());
        double x, y, z, w;
        lin >> x >> y >> z >> w;       //now read the whitespace-separated floats
        cps[4 * i]     = x;
        cps[4 * i + 1] = y;
        cps[4 * i + 2] = z;
        cps[4 * i + 3] = w;
    }
    return create(order, cps.data());
}

/*! \fn  dtkRationalBezierCurveDataOCCT::degree(void) const
  Returns the degree of the curve.
*/
std::size_t dtkRationalBezierCurveDataOCCT::degree(void) const
{
    return d->Degree();
}

/*! \fn  dtkRationalBezierCurveDataOCCT::controlPoint(std::size_t i, double *r_cp) const
  Writes in \a r_cp the \a i th weighted control point coordinates[xi, yi].

  \a i : index of the weighted control point

  \a r_cp : array of size 3 to store the weighted control point coordinates[xi, yi, zi]

  The method can be called as follow:
  \code
  dtkContinuousGeometryPrimitives::Point_3 point3(0, 0, 0);
  rational_bezier_curve_data->controlPoint(3, point3.data());
  \endcode
*/
void dtkRationalBezierCurveDataOCCT::controlPoint(std::size_t i, double* r_cp) const
{
    const auto& pole = d->Pole(i+1);
    const auto& xyz = pole.XYZ();
    std::copy(xyz.GetData(), xyz.GetData() + 3, r_cp);
}

/*! \fn  dtkRationalBezierCurveDataOCCT::weight(std::size_t i, double *r_w) const
  Writes in \a r_w the \a i th weighted control point weight wi.

  \a i : index of the weighted control point

  \a r_w : array of size 1 to store the weighted control point weight wi
*/
void dtkRationalBezierCurveDataOCCT::weight(std::size_t i, double* r_w) const
{
    *r_w = d->Weight(i+1);
}

/*! \fn  dtkRationalBezierCurveDataOCCT::setWeightedControlPoint(std::size_t i, double *p_cp)
  Modifies the \a i th weighted control point by \a p_cp : [xi, yi, zi, wi].

  \a i : index of the weighted control point

  \a p_cp : array of size 4 containing the weighted control point coordinates and weight  : [xi, yi, zi, wi]
*/
void dtkRationalBezierCurveDataOCCT::setWeightedControlPoint(std::size_t i, double *p_cp)
{

    d->SetPole(i+1, { p_cp[0], p_cp[1], p_cp[2] }, p_cp[3]);
}

/*! \fn dtkRationalBezierCurveDataOCCT::evaluatePoint(double p_u, double *r_point) const
  Writes in \a r_point the evaluation at parameter \a p_u.

  \a p_u : parameter

  \a r_point : array of size 3 to store the result of the evaluation
*/
void dtkRationalBezierCurveDataOCCT::evaluatePoint(double p_u, double* r_point) const
{
    auto xyz = d->Value(p_u).XYZ();
    std::copy(xyz.GetData(), xyz.GetData() + 3, r_point);
}

/*! \fn dtkRationalBezierCurveDataOCCT::evaluateNormal(double p_u, double *r_normal) const
  Writes in \a r_normal the normal evaluation at parameter \a p_u.

  \a p_u : parameter

  \a r_normal : array of size 3 to store the result of the normal evaluation
*/
void dtkRationalBezierCurveDataOCCT::evaluateNormal(double p_u, double* r_normal) const
{
    return evaluateDerivative(p_u, r_normal);
}

void dtkRationalBezierCurveDataOCCT::evaluateDerivative(double p_u, double* r_derivative) const
{
    gp_Pnt p;
    gp_Vec der1;
    d->D1(p_u, p, der1);
    const auto& xyz = der1.XYZ();
    std::copy(xyz.GetData(), xyz.GetData() + 3, r_derivative);
}

void dtkRationalBezierCurveDataOCCT::evaluateCurvature(double p_u, double* r_curvature) const
{
    gp_Pnt p;
    gp_Vec der1;
    gp_Vec der2;
    d->D2(p_u, p, der1, der2);
    const auto& xyz = der2.XYZ();
    std::copy(xyz.GetData(), xyz.GetData() + 3, r_curvature);
}

/*! \fn dtkRationalBezierCurveDataOCCT::split(dtkRationalBezierCurve *r_split_curve_a, dtkRationalBezierCurve *r_split_curve_b, double splitting_parameter) const
  Splits at \a splitting_parameter the rational Bezier curve and stores in \a r_split_curve_a and \a r_split_curve_b the split parts (from 0 to \a splitting_parameter and from \a splitting_parameter to 1 respectively).

  \a r_split_curve_a : a pointer to a valid dtkRationalBezierCurve with data not initialized, in which the first part of the curve (from 0 to to \a splitting_parameter) will be stored

  \a r_split_curve_b : a pointer to a valid dtkRationalBezierCurve with data not initialized, in which the second part of the curve (from \a splitting_parameter to 1) will be stored

  \a splitting_parameter : the parameter at which the curve will be split
*/
void dtkRationalBezierCurveDataOCCT::split(dtkRationalBezierCurve *r_split_curve_a, dtkRationalBezierCurve *r_split_curve_b, double splitting_parameter) const
{
    GeomConvert_CompCurveToBSplineCurve convert{d};
    auto bsplinecurve = convert.BSplineCurve();
    auto bsplinecurve_a = GeomConvert::SplitBSplineCurve(bsplinecurve, bsplinecurve->FirstParameter(), splitting_parameter, 0.);
    auto bsplinecurve_b = GeomConvert::SplitBSplineCurve(bsplinecurve, splitting_parameter, bsplinecurve->LastParameter(), 0.);
    GeomConvert_BSplineCurveToBezierCurve convert_a_to_bezier{bsplinecurve_a};
    GeomConvert_BSplineCurveToBezierCurve convert_b_to_bezier{bsplinecurve_b};
    assert(convert_a_to_bezier.NbArcs() == 1);
    assert(convert_b_to_bezier.NbArcs() == 1);
    auto curve_a_data = new dtkRationalBezierCurveDataOCCT;
    curve_a_data->create(convert_a_to_bezier.Arc(1));
    auto curve_b_data = new dtkRationalBezierCurveDataOCCT;
    curve_b_data->create(convert_b_to_bezier.Arc(1));
    r_split_curve_a->setData(curve_a_data);
    r_split_curve_b->setData(curve_b_data);
}

/*! \fn dtkRationalBezierCurveDataOCCT::split(std::list< dtkRationalBezierCurve * >& r_split_curves, const std::vector< double >& p_splitting_parameters) const
  Splits the rational Bezier curve at each value of \a p_splitting_parameters and stores in \a r_split_curves the split parts.

  \a r_split_curves : a list to which the split rational Bezier curves will be added

  \a p_splitting_parameters : the parameters at which the curve will be split
*/
void dtkRationalBezierCurveDataOCCT::split(std::list< dtkRationalBezierCurve * >& r_split_curves, const std::vector< double >& p_splitting_parameters) const
{
    // ///////////////////////////////////////////////////////////////////
    // Makes sure that the list r_split_curves is empty or warn that some pointed data may be lost
    // ///////////////////////////////////////////////////////////////////
    for (auto it = r_split_curves.begin(); it != r_split_curves.end(); ++it) {
        if ((*it) != nullptr) {
            dtkWarn() << "r_split_curves contains some non nullptr, the pointed data might be lost";
        }
    }
    r_split_curves.clear();
    // ///////////////////////////////////////////////////////////////////
    // Make sure that all splitting parameters are between 0. and 1.
    // ///////////////////////////////////////////////////////////////////
    //TODO
    std::vector< double > splitting_parameters = p_splitting_parameters;
    std::sort(splitting_parameters.begin(), splitting_parameters.end());
    // ///////////////////////////////////////////////////////////////////
    // Copies the current dtkRationalBezierCurve
    // ///////////////////////////////////////////////////////////////////
    dtkRationalBezierCurve *curve = new dtkRationalBezierCurve(this->clone());
    r_split_curves.push_back(curve);
    auto split_curve = r_split_curves.begin();
    double param = 0.;
    auto p = splitting_parameters.begin();
    while (!splitting_parameters.empty()) {
        param = *p;
        if (param <= 0 || param >=1) {
            splitting_parameters.erase(p);
        } else {
            dtkRationalBezierCurve *split_curve_a = new dtkRationalBezierCurve();
            dtkRationalBezierCurve *split_curve_b = new dtkRationalBezierCurve();
            (*split_curve)->split(split_curve_a, split_curve_b, *p);
            delete (*split_curve);
            split_curve = r_split_curves.erase(split_curve);
            r_split_curves.insert(split_curve, split_curve_b);
            --split_curve;
            r_split_curves.insert(split_curve, split_curve_a);
            splitting_parameters.erase(p);
            // ///////////////////////////////////////////////////////////////////
            // Recompute the remaining parameters for splitting the second of the two curves
            // ///////////////////////////////////////////////////////////////////
            for (auto rp = splitting_parameters.begin(); rp != splitting_parameters.end(); ++rp) {
                (*rp) = ((*rp) - param) / (1. - param);
            }
        }
    }
}

/*! \fn dtkRationalBezierCurveDataOCCT::aabb(double* r_aabb) const
  Writes in \a r_aabb the [xmin, ymin, zmin, xmax, ymax, zmax] coordinates.

  \a r_aabb : array of size 6 to store the limits coordinates
*/
void dtkRationalBezierCurveDataOCCT::aabb(double* r_aabb) const
{
    Bnd_Box bb;
    BndLib_Add3dCurve::Add(GeomAdaptor_Curve(this->d), 0., bb);
    bb.Get(r_aabb[0], r_aabb[1], r_aabb[2], r_aabb[3], r_aabb[4], r_aabb[5]);
}

/*! \fn dtkRationalBezierCurveDataOCCT::extendedAabb(double *r_aabb, double factor) const
  Writes in \a r_aabb the [xmin, ymin, xmax, ymax] coordinates of an extened bounding box of factor \a factor. The extension factor extends the diagonal.

  \a r_aabb : array of size 6 to store the extended limits coordinates

  \a factor : factor used to lengthen the diagonal of the axis aligned bounding box
*/
void dtkRationalBezierCurveDataOCCT::extendedAabb(double* r_aabb, double factor) const
{
    aabb(r_aabb);
    factor -= 1.;
    factor /= 2.;
    auto dx = factor * (r_aabb[3] - r_aabb[0]);
    auto dy = factor * (r_aabb[4] - r_aabb[1]);
    auto dz = factor * (r_aabb[5] - r_aabb[2]);
    r_aabb[0] -= dx;
    r_aabb[1] -= dy;
    r_aabb[2] -= dz;
    r_aabb[3] += dx;
    r_aabb[4] += dy;
    r_aabb[5] += dz;
}

/*! \fn dtkRationalBezierCurveDataOCCT::onRationalBezierCurve(void)
  Not available in the dtkAbstractRationalBezierCurveData.

  Returns the underlying OpenCascade rational Bezier curve.
 */
Geom_BezierCurve& dtkRationalBezierCurveDataOCCT::occtRationalBezierCurve(void)
{
    return *d;
}

/*! \fn dtkRationalBezierCurveDataOCCT::clone(void) const
   Clone
*/
dtkRationalBezierCurveDataOCCT* dtkRationalBezierCurveDataOCCT::clone(void) const
{
    return new dtkRationalBezierCurveDataOCCT(*this);
}

#include <iomanip>
void dtkRationalBezierCurveDataOCCT::print(std::ostream& stream) const
{
    stream << "#dtkRationalBezierCurveData" << '\n';
    stream << "degree " << d->Degree() << '\n';
    for(auto i = 0; i < d->Degree() + 1; ++i) {
        auto point = d->Pole(i+1).XYZ().GetData();
        stream << point[0] << " " << point[1] << " " << point[2] << " " << d->Weight(i) << std::setprecision(std::numeric_limits< double >::digits10 + 1) << '\n';
    }
    stream << "#dtkRationalBezierCurveData" << std::endl;
}

//
// dtkRationalBezierCurveDataOCCT.cpp ends here

// Local Variables:
// c-basic-offset: 4
// End:
