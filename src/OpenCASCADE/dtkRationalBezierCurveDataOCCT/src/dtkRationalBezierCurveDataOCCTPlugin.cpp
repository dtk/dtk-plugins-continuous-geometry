// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#include "dtkRationalBezierCurveDataOCCT.h"
#include "dtkRationalBezierCurveDataOCCTPlugin.h"

#include <dtkCore>
#include <dtkContinuousGeometry>

// ///////////////////////////////////////////////////////////////////
// dtkRationalBezierCurveDataOCCTPlugin
// ///////////////////////////////////////////////////////////////////

void dtkRationalBezierCurveDataOCCTPlugin::initialize(void)
{
    dtkContinuousGeometry::abstractRationalBezierCurveData::pluginFactory().record("dtkRationalBezierCurveDataOCCT", dtkRationalBezierCurveDataOCCTCreator);
}

void dtkRationalBezierCurveDataOCCTPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkRationalBezierCurveDataOCCT)

//
// dtkRationalBezierCurveDataOCCTPlugin.cpp ends here
