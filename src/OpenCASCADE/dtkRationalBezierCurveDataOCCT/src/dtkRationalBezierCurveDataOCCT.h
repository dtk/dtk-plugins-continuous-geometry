// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <dtkContinuousGeometry.h>

#include <dtkContinuousGeometryUtils>

#include <dtkRationalBezierCurveDataOCCTExport.h>

#include <dtkAbstractRationalBezierCurveData>

#include <Standard_Handle.hxx>

class Geom_BezierCurve;

class DTKRATIONALBEZIERCURVEDATAOCCT_EXPORT dtkRationalBezierCurveDataOCCT final : public dtkAbstractRationalBezierCurveData
{
private:
    typedef dtkContinuousGeometryPrimitives::Point_3 Point_3;
    typedef dtkContinuousGeometryPrimitives::Vector_3 Vector_3;

public:
    dtkRationalBezierCurveDataOCCT(void);
    dtkRationalBezierCurveDataOCCT(const dtkRationalBezierCurveDataOCCT& curve_data);
    ~dtkRationalBezierCurveDataOCCT(void) final ;

public:
    void create(std::size_t order, double *cps) const override;
    void create(std::string path) const override;
    void create(const Handle(Geom_BezierCurve)& bezier_curve);

public:
    std::size_t degree(void) const override;
    void controlPoint(std::size_t i, double *r_cp) const override;
    void weight(std::size_t i, double *r_w) const override;

public:
    void setWeightedControlPoint(std::size_t i, double *p_cp) override;

public:
    void evaluatePoint(double p_u, double *r_point) const override;
    void evaluateNormal(double p_u, double *r_normal) const override;
    void evaluateDerivative(double p_u, double* r_derivative) const override;
    void evaluateCurvature(double p_u, double* r_curvature) const override;

public:
    void split(dtkRationalBezierCurve *r_split_curve_a, dtkRationalBezierCurve *r_split_curve_b, double splitting_parameter) const override;
    void split(std::list< dtkRationalBezierCurve * >& r_split_curves, const std::vector< double >& p_splitting_parameters) const override;

public:
    void aabb(double* r_aabb) const override;
    void extendedAabb(double* r_aabb, double factor) const override;

public:
    Geom_BezierCurve& occtRationalBezierCurve(void);

public:
    dtkRationalBezierCurveDataOCCT *clone(void) const override;

public:
    void print(std::ostream& stream) const override;

private:
    mutable Handle(Geom_BezierCurve) d;
};


// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

inline dtkAbstractRationalBezierCurveData *dtkRationalBezierCurveDataOCCTCreator(void)
{
    return new dtkRationalBezierCurveDataOCCT();
}

//
// dtkRationalBezierCurveDataOCCT.h ends here
