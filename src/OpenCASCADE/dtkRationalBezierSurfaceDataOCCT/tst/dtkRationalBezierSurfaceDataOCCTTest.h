// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <QTest>

class dtkRationalBezierSurfaceDataOCCTTestCasePrivate;

class dtkRationalBezierSurfaceDataOCCTTestCase : public QObject
{
    Q_OBJECT

public:
    dtkRationalBezierSurfaceDataOCCTTestCase(void);
    ~dtkRationalBezierSurfaceDataOCCTTestCase(void);

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testControlPoint(void);
    void testWeight(void);
    void testUDegree(void);
    void testVDegree(void);
    void testIsDegenerate(void);
    void testdegenerateLocations(void);
    void testSurfacePoint(void);
    void testSurfaceNormal(void);
    void testPrintOutSurface(void);
    void evaluatePointBlossom(void);
    void testSplit(void);
    void testAabb(void);
    void testExtendedAabb(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);

private:
    dtkRationalBezierSurfaceDataOCCTTestCasePrivate* d;
};

//
// dtkRationalBezierSurfaceDataOCCTTest.h ends here
