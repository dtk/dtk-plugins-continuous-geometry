// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkRationalBezierSurfaceDataOCCTTest.h"

#include "dtkRationalBezierSurfaceDataOCCT.h"

#include <dtkTest>

#include <dtkRationalBezierSurface>

// ///////////////////////////////////////////////////////////////////
// D-Pointer implementation
// ///////////////////////////////////////////////////////////////////

class dtkRationalBezierSurfaceDataOCCTTestCasePrivate{
public:
    dtkRationalBezierSurfaceDataOCCTTestCasePrivate() : bezier_surface_data_1_2(nullptr), bezier_surface_data_1_1(nullptr), bezier_surface_data_2_2(nullptr), tolerance(1e-5) {};

public:
    dtkRationalBezierSurfaceDataOCCT *bezier_surface_data_1_2;
    dtkRationalBezierSurfaceDataOCCT *bezier_surface_data_1_1;

    dtkRationalBezierSurfaceDataOCCT *bezier_surface_data_2_2;

    double tolerance;
};
// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkRationalBezierSurfaceDataOCCTTestCase::dtkRationalBezierSurfaceDataOCCTTestCase(void):d(new dtkRationalBezierSurfaceDataOCCTTestCasePrivate()) {}

dtkRationalBezierSurfaceDataOCCTTestCase::~dtkRationalBezierSurfaceDataOCCTTestCase(void) {}

void dtkRationalBezierSurfaceDataOCCTTestCase::initTestCase(void)
{
    dtkLogger::instance().attachConsole();
    dtkLogger::instance().setLevel(dtk::LogLevel::Trace);

    std::size_t dim = 3;

    d->bezier_surface_data_1_1 = new dtkRationalBezierSurfaceDataOCCT();
    //bi-degree 1,1
    std::size_t order_u_1_1 = 2;
    std::size_t order_v_1_1 = 2;
    std::vector< double > cps_1_1((dim + 1) * order_u_1_1 * order_v_1_1);
    cps_1_1[0] = 0.;  cps_1_1[1] = 0.;  cps_1_1[2] = 0.;  cps_1_1[3] = 1.;
    cps_1_1[4] = 0.;  cps_1_1[5] = 1.;  cps_1_1[6] = 0.;  cps_1_1[7] = 0.6;
    cps_1_1[8] = 1.;  cps_1_1[9] = 0.;  cps_1_1[10] = 0.; cps_1_1[11] = 2.;
    cps_1_1[12] = 1.; cps_1_1[13] = 1.; cps_1_1[14] = 0.; cps_1_1[15] = 1.;
    d->bezier_surface_data_1_1->create(dim, order_u_1_1, order_v_1_1, cps_1_1.data());

    d->bezier_surface_data_1_2 = new dtkRationalBezierSurfaceDataOCCT();
    //bi-degree 1,2
    std::size_t order_u_1_2 = 2;
    std::size_t order_v_1_2 = 3;
    std::vector< double > cps_1_2((dim + 1) * order_u_1_2 * order_v_1_2);
    //x_00,           y_00,             z_00,             w_00
    cps_1_2[0] = 1.;  cps_1_2[1] = 0.;  cps_1_2[2] = 0.;  cps_1_2[3] = 1.;
    //x_01,           y_01,             z_01,             w_01
    cps_1_2[4] = 1.;  cps_1_2[5] = 0.;  cps_1_2[6] = 1.;  cps_1_2[7] = 0.6;
    //x_02,           y_02,             z_02,             w_02
    cps_1_2[8] = 0.;  cps_1_2[9] = 0.;  cps_1_2[10] = 2.; cps_1_2[11] = 2.;
    //x_10,           y_10,             z_10,             w_10
    cps_1_2[12] = 1.; cps_1_2[13] = 1.; cps_1_2[14] = 0.; cps_1_2[15] = 1.;
    cps_1_2[16] = 1.; cps_1_2[17] = 1.; cps_1_2[18] = 1.; cps_1_2[19] = 1.;
    cps_1_2[20] = 0.; cps_1_2[21] = 2.; cps_1_2[22] = 0.; cps_1_2[23] = 2.;
    d->bezier_surface_data_1_2->create(dim, order_u_1_2, order_v_1_2, cps_1_2.data());

    d->bezier_surface_data_2_2 = new dtkRationalBezierSurfaceDataOCCT();
    d->bezier_surface_data_2_2->create(QFINDTESTDATA("data/rational_bezier_surface_data_occt_2_2").toStdString());
}

void dtkRationalBezierSurfaceDataOCCTTestCase::init(void)
{

}

void dtkRationalBezierSurfaceDataOCCTTestCase::testControlPoint(void)
{
    dtkContinuousGeometryPrimitives::Point_3 point(0., 0., 0.);
    d->bezier_surface_data_1_2->controlPoint(0, 1, point.data());
    QVERIFY(point[0] == 1.);
    QVERIFY(point[1] == 0.);
    QVERIFY(point[2] == 1.);

    d->bezier_surface_data_1_2->controlPoint(1, 1, point.data());
    QVERIFY(point[0] == 1.);
    QVERIFY(point[1] == 1.);
    QVERIFY(point[2] == 1.);

    d->bezier_surface_data_2_2->controlPoint(1, 1, point.data());
    QVERIFY(point[0] == 5.);
    QVERIFY(point[1] == 15.);
    QVERIFY(point[2] == 5.);
}

void dtkRationalBezierSurfaceDataOCCTTestCase::testWeight(void)
{
    double w = 0.;
    d->bezier_surface_data_1_2->weight(0, 1, &w);
    QVERIFY(w == 0.6);

    d->bezier_surface_data_2_2->weight(0, 1, &w);
    QVERIFY(w == 1.);
}

void dtkRationalBezierSurfaceDataOCCTTestCase::testUDegree(void)
{
    QVERIFY(d->bezier_surface_data_1_2->uDegree() == 1);
    QVERIFY(d->bezier_surface_data_2_2->uDegree() == 2);
}

void dtkRationalBezierSurfaceDataOCCTTestCase::testVDegree(void)
{
    QVERIFY(d->bezier_surface_data_1_2->vDegree() == 2);
    QVERIFY(d->bezier_surface_data_2_2->vDegree() == 2);
}

void dtkRationalBezierSurfaceDataOCCTTestCase::testIsDegenerate(void) {
    dtkRationalBezierSurfaceDataOCCT degeneratePatch_0;
    // //bi-degree 1,1
    std::size_t dim_0 = 3;
    std::size_t order_u_0 = 2;
    std::size_t order_v_0 = 2;
    double* cps_0 = new double[(dim_0 + 1) * order_u_0 * order_v_0];
    cps_0[0] = 1.;  cps_0[1] = 0.;  cps_0[2] = 0.;  cps_0[3] = 1.;
    cps_0[4] = 1.;  cps_0[5] = 1.;  cps_0[6] = 0.;  cps_0[7] = 1.;
    cps_0[8] = 0.;  cps_0[9] = 0.;  cps_0[10] = 2.; cps_0[11] = 1.;
    cps_0[12] = 1.; cps_0[13] = 1.; cps_0[14] = 0.; cps_0[15] = 1.;
    degeneratePatch_0.create(dim_0, order_u_0, order_v_0, cps_0);
    delete[] cps_0;

    dtkRationalBezierSurfaceDataOCCT degeneratePatch_1;
    // //bi-degree 1,1
    std::size_t dim_1 = 3;
    std::size_t order_u_1 = 2;
    std::size_t order_v_1 = 2;
    double* cps_1 = new double[(dim_1 + 1) * order_u_1 * order_v_1];
    cps_1[0] = 1.;  cps_1[1] = 0.;  cps_1[2] = 0.;  cps_1[3] = 1.;
    cps_1[4] = 1.;  cps_1[5] = 0.;  cps_1[6] = 0.;  cps_1[7] = 1.;
    cps_1[8] = 0.;  cps_1[9] = 0.;  cps_1[10] = 2.; cps_1[11] = 1.;
    cps_1[12] = 1.; cps_1[13] = 1.; cps_1[14] = 0.; cps_1[15] = 1.;
    degeneratePatch_1.create(dim_1, order_u_1, order_v_1, cps_1);
    delete[] cps_1;

    QVERIFY(degeneratePatch_0.isDegenerate());
    QVERIFY(degeneratePatch_1.isDegenerate());
    QVERIFY(!d->bezier_surface_data_1_2->isDegenerate());
}

void dtkRationalBezierSurfaceDataOCCTTestCase::testdegenerateLocations(void) {
    dtkRationalBezierSurfaceDataOCCT degeneratePatch_0;
    // //bi-degree 1,1
    std::size_t dim_0 = 3;
    std::size_t order_u_0 = 2;
    std::size_t order_v_0 = 2;
    double* cps_0 = new double[(dim_0 + 1) * order_u_0 * order_v_0];
    cps_0[0] = 1.;  cps_0[1] = 0.;  cps_0[2] = 0.;  cps_0[3] = 1.;
    cps_0[4] = 1.;  cps_0[5] = 1.;  cps_0[6] = 0.;  cps_0[7] = 1.;
    cps_0[8] = 0.;  cps_0[9] = 0.;  cps_0[10] = 2.; cps_0[11] = 1.;
    cps_0[12] = 1.; cps_0[13] = 1.; cps_0[14] = 0.; cps_0[15] = 1.;
    degeneratePatch_0.create(dim_0, order_u_0, order_v_0, cps_0);
    delete[] cps_0;

    dtkRationalBezierSurfaceDataOCCT degeneratePatch_1;
    // //bi-degree 1,2
    std::size_t dim_1 = 3;
    std::size_t order_u_1 = 2;
    std::size_t order_v_1 = 3;
    double* cps_1 = new double[(dim_1 + 1) * order_u_1 * order_v_1];
    cps_1[0] = 1.;  cps_1[1] = 0.;  cps_1[2] = 0.;  cps_1[3] = 1.;
    cps_1[4] = 1.;  cps_1[5] = 0.;  cps_1[6] = 1.;  cps_1[7] = 1.;
    cps_1[8] = 1.;  cps_1[9] = 0.;  cps_1[10] = 0.; cps_1[11] = 1.;
    cps_1[12] = 1.; cps_1[13] = 1.; cps_1[14] = 0.; cps_1[15] = 1.;
    cps_1[16] = 1.; cps_1[17] = 0.; cps_1[18] = 1.; cps_1[19] = 1.;
    cps_1[20] = 0.; cps_1[21] = 2.; cps_1[22] = 0.; cps_1[23] = 2.;
    degeneratePatch_1.create(dim_1, order_u_1, order_v_1, cps_1);
    delete[] cps_1;

    std::list<dtkContinuousGeometryPrimitives::Point_3 > degenerate_locations_0;
    degeneratePatch_0.degenerateLocations(degenerate_locations_0);
    std::list<dtkContinuousGeometryPrimitives::Point_3 > degenerate_locations_1;
    degeneratePatch_1.degenerateLocations(degenerate_locations_1);
    QVERIFY(degenerate_locations_0.size() == 1);
    QVERIFY(degenerate_locations_1.size() == 1);
}
void dtkRationalBezierSurfaceDataOCCTTestCase::testSurfacePoint(void)
{
    dtkContinuousGeometryPrimitives::Point_3 point(0., 0., 0.);
    d->bezier_surface_data_1_2->evaluatePoint(0.25, 0.65, point.data());

    QVERIFY(point[0] > 0.342924 - 10e-5 && point[0] < 0.342924 + 10e-5);
    QVERIFY(point[1] > 0.440805 - 10e-5 && point[1] < 0.440805 + 10e-5);
    QVERIFY(point[2] > 1.23328 - 10e-5 && point[2] < 1.23328 + 10e-5);
}

//To ameliorate with dataSurfacePoint and several pints to test
void dtkRationalBezierSurfaceDataOCCTTestCase::testSurfaceNormal(void)
{
    double *normal = new double[3];
    d->bezier_surface_data_1_2->evaluateNormal(0.5, 0.5, normal);
    delete[] normal;
}


void dtkRationalBezierSurfaceDataOCCTTestCase::testSplit(void)
{
    dtkRationalBezierSurfaceDataOCCT *bezier_surface_data_1_1 = new dtkRationalBezierSurfaceDataOCCT();
    //bi-degree 1,1
    std::size_t order_u_1_1 = 2;
    std::size_t order_v_1_1 = 2;
    double* cps_1_1 = new double[(3 + 1) * order_u_1_1 * order_v_1_1];
    cps_1_1[0] = 0.;  cps_1_1[1] = 0.;  cps_1_1[2] = 0.;  cps_1_1[3] = 1.;
    cps_1_1[4] = 0.;  cps_1_1[5] = 1.;  cps_1_1[6] = 0.;  cps_1_1[7] = 1.;
    cps_1_1[8] = 1.;  cps_1_1[9] = 0.;  cps_1_1[10] = 0.; cps_1_1[11] = 1.;
    cps_1_1[12] = 1.; cps_1_1[13] = 1.; cps_1_1[14] = 0.; cps_1_1[15] = 1.;
    bezier_surface_data_1_1->create(3, order_u_1_1, order_v_1_1, cps_1_1);
    delete[] cps_1_1;

    dtkRationalBezierSurface *r_bezier_surface = new dtkRationalBezierSurface();
    dtkContinuousGeometryPrimitives::AABB_2 aabb(0.25, 0.25, 0.75, 0.75);
    bezier_surface_data_1_1->split(r_bezier_surface, aabb.data());
    QVERIFY(r_bezier_surface->data() != nullptr);
    std::ofstream initial_surface("initial_surface.xyz");
    std::ofstream split_surface("split_surface.xyz");
    dtkContinuousGeometryPrimitives::Point_3 p(0., 0., 0.);
    for(double i = 0.; i < 1.; i += 0.01) {
        for(double j = 0.; j < 1.; j += 0.01) {
            bezier_surface_data_1_1->evaluatePoint(i, j, p.data());
            initial_surface << p << std::endl;
        }
    }

    for(double i = 0.; i < 1.; i += 0.01) {
        for(double j = 0.; j < 1.; j += 0.01) {
            r_bezier_surface->evaluatePoint(i, j, p.data());
            split_surface << p << std::endl;
        }
    }
    delete r_bezier_surface;
}

void  dtkRationalBezierSurfaceDataOCCTTestCase::testAabb(void)
{
    double *box = new double[6];
    d->bezier_surface_data_1_2->aabb(box);
    QVERIFY(box[0] == 0.);
    QVERIFY(box[1] == 0.);
    QVERIFY(box[2] == 0.);
    QVERIFY(box[3] == 1.);
    QVERIFY(box[4] == 2.);
    QVERIFY(box[5] == 2.);
    delete[] box;
}

void  dtkRationalBezierSurfaceDataOCCTTestCase::testExtendedAabb(void)
{
    double *box = new double[6];
    double factor = 1.1;
    d->bezier_surface_data_1_2->extendedAabb(box, factor);
    QVERIFY(box[0] > -0.05 - d->tolerance && box[0] < -0.05 + d->tolerance);
    QVERIFY(box[1] > -0.1 - d->tolerance && box[1] < -0.1 + d->tolerance);
    QVERIFY(box[2] > -0.1 - d->tolerance && box[2] < -0.1 + d->tolerance);
    QVERIFY(box[3] > 1.05 - d->tolerance && box[3] < 1.05 + d->tolerance);
    QVERIFY(box[4] > 2.1 - d->tolerance && box[4] < 2.1 + d->tolerance);
    QVERIFY(box[5] > 2.1 - d->tolerance && box[5] < 2.1 + d->tolerance);
    delete[] box;
}

void dtkRationalBezierSurfaceDataOCCTTestCase::testPrintOutSurface(void)
{
    double point_cp[3];
    std::ofstream bezier_surface_data_on_cp_file("bezier_surface_data_on_cp.xyz");
    for (std::size_t i = 0; i <= d->bezier_surface_data_1_2->uDegree(); ++i) {
        for (std::size_t j = 0; j <= d->bezier_surface_data_1_2->vDegree(); ++j) {
            d->bezier_surface_data_1_2->controlPoint(i, j, &point_cp[0]);
            bezier_surface_data_on_cp_file << point_cp[0] << " " << point_cp[1] << " " << point_cp[2] << std::endl;
        }
    }
    bezier_surface_data_on_cp_file.close();

    std::ofstream bezier_surface_data_on_file("bezier_surface_data_on.xyz");
    double point[3];
    for (double i = 0.; i <= 1.; i += 0.01) {
        for (double j = 0.; j <= 1.; j += 0.01) {
            d->bezier_surface_data_1_2->evaluatePoint(i, j, &point[0]);
            bezier_surface_data_on_file << point[0] << " " << point[1] << " " << point[2] << std::endl;
        }
    }
    bezier_surface_data_on_file.close();
}

void dtkRationalBezierSurfaceDataOCCTTestCase::evaluatePointBlossom(void)
{
    dtkContinuousGeometryPrimitives::Point_3 point_result(0., 0., 0.);
    dtkContinuousGeometryPrimitives::Point_3 point_result_test(0., 0., 0.);

    double w_1_1 = 0.;
    std::vector< double > uis_1_1;
    uis_1_1.resize(d->bezier_surface_data_1_1->uDegree());
    uis_1_1[0] = 0.1;
    std::vector< double > vis_1_1;
    vis_1_1.resize(d->bezier_surface_data_1_1->vDegree());
    vis_1_1[0] = 0.4;

    d->bezier_surface_data_1_1->evaluatePoint(0.1, 0.4, point_result_test.data());
    d->bezier_surface_data_1_1->evaluatePointBlossom(uis_1_1.data(), vis_1_1.data(), point_result.data(), &w_1_1);
    QVERIFY(point_result[0] > point_result_test[0] - d->tolerance && point_result[0] < point_result_test[0] + d->tolerance);
    QVERIFY(point_result[1] > point_result_test[1] - d->tolerance && point_result[1] < point_result_test[1] + d->tolerance);
    QVERIFY(point_result[2] > point_result_test[2] - d->tolerance && point_result[2] < point_result_test[2] + d->tolerance);

    // ///////////////////////////////////////////////////////////////////

    double w_1_2 = 0.;
    std::vector< double > uis_1_2;
    uis_1_2.resize(d->bezier_surface_data_1_2->uDegree());
    uis_1_2[0] = 0.1;
    std::vector< double > vis_1_2;
    vis_1_2.resize(d->bezier_surface_data_1_2->vDegree());
    vis_1_2[0] = 0.1;
    vis_1_2[1] = 0.1;

    d->bezier_surface_data_1_2->evaluatePoint(0.1, 0.1, point_result_test.data());
    d->bezier_surface_data_1_2->evaluatePointBlossom(uis_1_2.data(), vis_1_2.data(), point_result.data(), &w_1_2);
    QVERIFY(point_result[0] > point_result_test[0] - d->tolerance && point_result[0] < point_result_test[0] + d->tolerance);
    QVERIFY(point_result[1] > point_result_test[1] - d->tolerance && point_result[1] < point_result_test[1] + d->tolerance);
    QVERIFY(point_result[2] > point_result_test[2] - d->tolerance && point_result[2] < point_result_test[2] + d->tolerance);

}

void dtkRationalBezierSurfaceDataOCCTTestCase::cleanup(void)
{

}

void dtkRationalBezierSurfaceDataOCCTTestCase::cleanupTestCase(void)
{
    delete d->bezier_surface_data_1_1;
    d->bezier_surface_data_1_1 = nullptr;
    delete d->bezier_surface_data_1_2;
    d->bezier_surface_data_1_2 = nullptr;
    delete d->bezier_surface_data_2_2;
    d->bezier_surface_data_2_2 = nullptr;

}

DTKTEST_MAIN_NOGUI(dtkRationalBezierSurfaceDataOCCTTest, dtkRationalBezierSurfaceDataOCCTTestCase)

//
// dtkRationalBezierSurfaceDataOCCTTest.cpp ends here
