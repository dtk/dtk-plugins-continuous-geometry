// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <dtkContinuousGeometry.h>

#include <dtkContinuousGeometryUtils>

#include <dtkRationalBezierSurfaceDataOCCTExport.h>

#include <dtkAbstractRationalBezierSurfaceData>

#include <Standard_Handle.hxx>

class Geom_BezierSurface;

class DTKRATIONALBEZIERSURFACEDATAOCCT_EXPORT dtkRationalBezierSurfaceDataOCCT final : public dtkAbstractRationalBezierSurfaceData
{
private:
    typedef dtkContinuousGeometryPrimitives::Point_2 Point_2;

    typedef dtkContinuousGeometryPrimitives::Array_3 Array_3;
    typedef dtkContinuousGeometryPrimitives::Point_3 Point_3;
    typedef dtkContinuousGeometryPrimitives::Vector_3 Vector_3;

public:
    dtkRationalBezierSurfaceDataOCCT(void);
    // dtkRationalBezierSurfaceDataOCCT(const dtkRationalBezierSurfaceDataOCCT& mesh_data);
    ~dtkRationalBezierSurfaceDataOCCT(void) final ;

public:
    void create(std::size_t dim, std::size_t order_u, std::size_t order_v, double *cps) const override;
    void create(std::string path) const override;
    void create(const Handle(Geom_BezierSurface)& bezier_surface);

public:
    std::size_t uDegree(void) const override;
    std::size_t vDegree(void) const override;
    void controlPoint(std::size_t i, std::size_t j, double *r_cp) const override;
    void weight(std::size_t i, std::size_t j, double *r_w) const override;

    bool isDegenerate(void) const override;
    bool degenerateLocations(std::list< dtkContinuousGeometryPrimitives::Point_3 >& r_degenerate_locations) const override;

public:
    void evaluatePoint(double p_u, double p_v, double *r_point) const override;
    void evaluateNormal(double p_u, double p_v, double *r_normal) const override;
    void evaluate1stDer(double p_u, double p_v, double *r_point, double *r_u_deriv, double *r_v_deriv) const override;
    void evaluate1stDer(double p_u, double p_v, double *r_u_deriv, double *r_v_deriv) const override;

 public:
    void evaluatePointBlossom(const double* p_uis, const double* p_vis, double* r_point, double* r_weight) const override;

 public:
    void split(dtkRationalBezierSurface *r_split_surface, double *p_splitting_parameters) const override;

 public:
    void aabb(double *r_aabb) const override;
    void extendedAabb(double *r_aabb, double factor) const override;

 public:
    Geom_BezierSurface& occtRationalBezierSurface(void);

 public:
    dtkRationalBezierSurfaceDataOCCT *clone(void) const override;

 public:
    void print(std::ostream& stream) const override;

 private:
    mutable Handle(Geom_BezierSurface) d;
};


// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

inline dtkAbstractRationalBezierSurfaceData *dtkRationalBezierSurfaceDataOCCTCreator(void)
{
    return new dtkRationalBezierSurfaceDataOCCT();
}

//
// dtkRationalBezierSurfaceDataOCCT.h ends here
