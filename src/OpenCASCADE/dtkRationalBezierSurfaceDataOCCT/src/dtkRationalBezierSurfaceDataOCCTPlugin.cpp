// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#include "dtkRationalBezierSurfaceDataOCCT.h"
#include "dtkRationalBezierSurfaceDataOCCTPlugin.h"

#include <dtkCore>
#include <dtkContinuousGeometry>

// ///////////////////////////////////////////////////////////////////
// dtkRationalBezierSurfaceDataOCCTPlugin
// ///////////////////////////////////////////////////////////////////

void dtkRationalBezierSurfaceDataOCCTPlugin::initialize(void)
{
    dtkContinuousGeometry::abstractRationalBezierSurfaceData::pluginFactory().record("dtkRationalBezierSurfaceDataOCCT", dtkRationalBezierSurfaceDataOCCTCreator);
}

void dtkRationalBezierSurfaceDataOCCTPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkRationalBezierSurfaceDataOCCT)

//
// dtkRationalBezierSurfaceDataOCCTPlugin.cpp ends here
