// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <QtCore>

#include <dtkContinuousGeometry>

#include <dtkAbstractRationalBezierSurfaceData>

class dtkRationalBezierSurfaceDataOCCTPlugin : public dtkAbstractRationalBezierSurfaceDataPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkAbstractRationalBezierSurfaceDataPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkRationalBezierSurfaceDataOCCTPlugin" FILE "dtkRationalBezierSurfaceDataOCCTPlugin.json")

public:
     dtkRationalBezierSurfaceDataOCCTPlugin(void) {}
    ~dtkRationalBezierSurfaceDataOCCTPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

//
// dtkRationalBezierSurfaceDataOCCTPlugin.h ends here
