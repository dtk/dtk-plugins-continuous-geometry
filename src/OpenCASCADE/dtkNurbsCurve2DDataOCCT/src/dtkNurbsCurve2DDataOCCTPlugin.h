// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <QtCore>

#include <dtkContinuousGeometry>

#include <dtkAbstractNurbsCurve2DData>

class dtkNurbsCurve2DDataOCCTPlugin : public dtkAbstractNurbsCurve2DDataPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkAbstractNurbsCurve2DDataPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkNurbsCurve2DDataOCCTPlugin" FILE "dtkNurbsCurve2DDataOCCTPlugin.json")

public:
     dtkNurbsCurve2DDataOCCTPlugin(void) {}
    ~dtkNurbsCurve2DDataOCCTPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

//
// dtkNurbsCurve2DDataOCCTPlugin.h ends here
