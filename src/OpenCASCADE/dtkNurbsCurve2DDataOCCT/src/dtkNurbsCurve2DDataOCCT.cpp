// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkNurbsCurve2DDataOCCT.h"

#include <dtkRationalBezierCurve2DDataOCCT.h>

#include <cassert>
#include <algorithm>

#include <Geom2d_BSplineCurve.hxx>
#include <Bnd_Box2d.hxx>
#include <BndLib_Add2dCurve.hxx>
#include <Geom2dAdaptor_Curve.hxx>
#include <Geom2dConvert_BSplineCurveToBezierCurve.hxx>

#include "multiplicities.h"

/*!
  \class dtkNurbsCurve2DDataOCCT
  \inmodule dtkPluginsContinuousGeometry
  \brief dtkNurbsCurve2DDataOCCT is an openNURBS implementation of the concept dtkAbstractNurbsCurve2DData.

  The following lines of code show how to instanciate and use the class in pratice:
  \code
  dtkAbstractNurbsCurve2DData *nurbs_curve_data = dtkContinuousGeometry::abstractNurbsCurve2DData::pluginFactory().create("dtkNurbsCurve2DDataOCCT");
  dtkNurbsCurve2D *nurbs_curve = new dtkNurbsCurve2D(nurbs_curve_data);
  nurbs_curve->create(...);
  \endcode
*/

/*! \fn dtkNurbsCurve2DDataOCCT::dtkNurbsCurve2DDataOCCT(void)
  Instanciates the structure.

  To add the content, see \l create(std::size_t nb_cp, std::size_t order, double *knots, double *cps) const.
*/
dtkNurbsCurve2DDataOCCT::dtkNurbsCurve2DDataOCCT(void) : d(nullptr) {}

/*! \fn dtkNurbsCurve2DDataOCCT::~dtkNurbsCurve2DDataOCCT(void)
  Destroys the instance.
*/
dtkNurbsCurve2DDataOCCT::~dtkNurbsCurve2DDataOCCT(void)
{
}

/*! \fn dtkNurbsCurve2DDataOCCT::create(const ON_NurbsCurve& nurbs_curve)
  Not available in the dtkAbstractNurbsCurve2DData.

  Creates the 2D NURBS curve by providing a NURBS curve from the openNURBS API.

  \a nurbs_curve : the NURBS curve to copy.
*/
void dtkNurbsCurve2DDataOCCT::create(const Handle(Geom2d_BSplineCurve)& nurbs_curve)
{
    d = Handle(Geom2d_BSplineCurve)::DownCast(nurbs_curve->Copy());
    update_m_knots();
}

/*! \fn dtkNurbsCurve2DDataOCCT::create(std::size_t nb_cp, std::size_t order, double *knots, double *cps) const
  Creates the 2D NURBS curve by providing the required content.

  \a nb_cp : number of control points

  \a order : order

  \a knots : array containing the knots

  \a cps : array containing the weighted control points, specified as : [x0, y0, w0, ...]
*/
void dtkNurbsCurve2DDataOCCT::create(std::size_t nb_cp, std::size_t order, double *knots, double *cps) const
{
    TColgp_Array1OfPnt2d poles(1, nb_cp);
    TColStd_Array1OfReal weights(1, nb_cp);

    for(std::size_t i = 0; i < nb_cp; ++i) {
        /// @TODO: use a loop more efficient with pointers instead of ChangeValue
      poles.ChangeValue(i+1) = gp_Pnt2d{cps[0], cps[1]};
      weights.ChangeValue(i+1) = cps[2];
      cps += 3;
    }

    auto knots_and_multiplicities = multiplicities(knots, order+nb_cp-2);
    const auto& knots_ = knots_and_multiplicities.first;
    const auto& mults_ = knots_and_multiplicities.second;
    assert(knots_.size() == mults_.size());
    TColStd_Array1OfReal occt_knots(1, knots_.size());
    std::copy(knots_.begin(), knots_.end(), &occt_knots.ChangeValue(1));
    TColStd_Array1OfInteger occt_mults(1, mults_.size());
    std::copy(mults_.begin(), mults_.end(), &occt_mults.ChangeValue(1));

    /*
      Difference with the OpenNURBS API: the sum of multiplicities
      must be order+nb_cp and not order+nb_cp-2.
      See https://developer.rhino3d.com/guides/opennurbs/superfluous-knots/
    */
    ++occt_mults.ChangeFirst();
    ++occt_mults.ChangeLast();

    Standard_Integer degree_ = order - 1;
    assert(std::accumulate(mults_.begin(), mults_.end(), 0) ==
           static_cast<int>(order + nb_cp - 2));

    d = new Geom2d_BSplineCurve{poles, weights, occt_knots, occt_mults, degree_, false};
    auto nb_knots = order + nb_cp - 2;
    m_knots.resize(nb_knots);
    std::copy(knots, knots + nb_knots, &m_knots[0]);
}

/*! \fn  dtkNurbsCurve2DDataOCCT::degree(void) const
  Returns the degree of the curve.
*/
std::size_t dtkNurbsCurve2DDataOCCT::degree(void) const
{
    return d->Degree();
}

/*! \fn  dtkNurbsCurve2DDataOCCT::nbCps(void) const
  Returns the number of weighted control points controlling the curve.
*/
std::size_t dtkNurbsCurve2DDataOCCT::nbCps(void) const
{
    return d->NbPoles();
}

/*! \fn  dtkNurbsCurve2DDataOCCT::controlPoint(std::size_t i, double *r_cp) const
  Writes in \a r_cp the \a i th weighted control point coordinates[xi, yi].

  \a i : index of the weighted control point

  \a r_cp : array of size 2 to store the weighted control point coordinates[xi, yi]
*/
void dtkNurbsCurve2DDataOCCT::controlPoint(std::size_t i, double* r_cp) const
{
    const auto& pole = d->Pole(i+1);
    r_cp[0] = pole.X();
    r_cp[1] = pole.Y();
}

/*! \fn  dtkNurbsCurve2DDataOCCT::weight(std::size_t i, double *r_w) const
  Writes in \a r_w the \a i th weighted control point weight wi.

  \a i : index of the weighted control point

  \a r_w : array of size 1 to store the weighted control point weight wi
*/
void dtkNurbsCurve2DDataOCCT::weight(std::size_t i, double* r_w) const
{
    *r_w = d->Weight(i + 1);
}

void dtkNurbsCurve2DDataOCCT::update_m_knots() const
{
    m_knots.resize(d->NbPoles() + d->Degree() - 1);
    auto* r_knots = m_knots.data();
    for(int i = 1, nb = d->NbKnots(); i <= nb; ++i) {
        const auto knot = d->Knot(i);
        auto mult = d->Multiplicity(i);
        if(i == 1 || i == nb) mult -= 1;
        for(int j = 0; j < mult; ++j) {
            *r_knots++ = knot;
        }
    }
    assert((m_knots.data() + (d->NbPoles() + d->Degree() - 1)) == r_knots);
}

/*! \fn dtkNurbsCurve2DDataOCCT::knots(double *r_knots) const
  Writes in \a r_knots the knots of the curve.

  \a r_knots : array of size (nb_cp + order - 2) to store the knots
*/
void dtkNurbsCurve2DDataOCCT::knots(double* r_knots) const
{
    std::copy(m_knots.begin(), m_knots.end(), r_knots);
}

/*! \fn dtkNurbsCurve2DDataOCCT::knots(void) const
  Returns a pointer to the array storing the knots of the curve. The array is of size : (nb_cp + order - 2).
*/
const double *dtkNurbsCurve2DDataOCCT::knots(void) const
{
    return m_knots.data();
}

/*! \fn dtkNurbsCurve2DDataOCCT::evaluatePoint(double p_u, double *r_point) const
  Writes in \a r_point the evaluation at parameter \a p_u.

  \a p_u : parameter

  \a r_point : array of size 2 to store the result of the evaluation
*/
void dtkNurbsCurve2DDataOCCT::evaluatePoint(double p_u, double* r_point) const
{
    auto p = d->Value(p_u);
    r_point[0] = p.X();
    r_point[1] = p.Y();
}

/*! \fn dtkNurbsCurve2DDataOCCT::evaluateNormal(double p_u, double *r_normal) const
  Writes in \a r_normal the normal evaluation at parameter \a p_u.

  \a p_u : parameter

  \a r_normal : array of size 2 to store the result of the normal evaluation
*/
void dtkNurbsCurve2DDataOCCT::evaluateNormal(double p_u, double* r_normal) const
{
    gp_Pnt2d p;
    gp_Vec2d der1;
    d->D1(p_u, p, der1);
    r_normal[0] = der1.X();
    r_normal[1] = der1.Y();
}

/*! \fn dtkNurbsCurve2DDataOCCT::evaluateNormal(double p_u, double *r_point, double *r_normal) const
  Writes in \a r_normal the normal evaluation at parameter \a p_u.

  \a p_u : parameter

  \a r_point : array of size 2 to store the result of the evaluation

  \a r_normal : array of size 2 to store the result of the normal evaluation
*/
void dtkNurbsCurve2DDataOCCT::evaluateNormal(double p_u, double* r_point, double* r_normal) const
{
    gp_Pnt2d p;
    gp_Vec2d der1;
    d->D1(p_u, p, der1);
    r_point[0] = p.X();
    r_point[1] = p.Y();
    r_normal[0] = der1.X();
    r_normal[1] = der1.Y();
}

/*! \fn dtkNurbsCurve2DDataOCCT::decomposeToRationalBezierCurve2Ds(std::vector< std::pair< dtkRationalBezierCurve2D *, double * >>& r_rational_bezier_curves) const
  Decomposes the NURBS curve into dtkRationalBezierCurve2D \a r_rational_bezier_curves.

  \a r_rational_bezier_curves : vector in which the dtkRationalBezierCurve2D are stored, along with their limits in the NURBS curve parameter space
*/
void dtkNurbsCurve2DDataOCCT::decomposeToRationalBezierCurve2Ds(std::vector< std::pair< dtkRationalBezierCurve2D *, double * >>& r_rational_bezier_curves) const
{
    Geom2dConvert_BSplineCurveToBezierCurve convert{d};
    auto nb_arcs = convert.NbArcs();
    TColStd_Array1OfReal knots(1, nb_arcs + 1);
    convert.Knots(knots);

    for (auto i = 1; i <= nb_arcs; ++i) {
        auto bezier_curve_handle = convert.Arc(i);
        double* knots_ptr = new double[2]; /// @TODO memory leak!
        knots_ptr[0] = knots[i];
        knots_ptr[1] = knots[i+1];
        auto bezier = new dtkRationalBezierCurve2DDataOCCT;
        bezier->create(bezier_curve_handle);
        r_rational_bezier_curves.emplace_back(new dtkRationalBezierCurve2D{bezier}, knots_ptr);
    }
}

/*! \fn dtkNurbsCurve2DDataOCCT::aabb(double* r_aabb) const
  Writes in \a r_aabb the [xmin, ymin, xmax, ymax] coordinates.

  \a r_aabb : array of size 4 to store the limits coordinates
*/
void dtkNurbsCurve2DDataOCCT::aabb(double* r_aabb) const
{
    Bnd_Box2d bb;
    BndLib_Add2dCurve::Add(Geom2dAdaptor_Curve(this->d), 0., bb);
    bb.Get(r_aabb[0], r_aabb[1], r_aabb[2], r_aabb[3]);
}

/*! \fn dtkNurbsCurve2DDataOCCT::extendedAabb(double *r_aabb, double factor) const
  Writes in \a r_aabb the [xmin, ymin, xmax, ymax] coordinates of an extened bounding box of factor \a factor. The extension factor extends the diagonal.

  \a r_aabb : array of size 4 to store the extended limits coordinates

  \a factor : factor used to lengthen the diagonal of the axis aligned bounding box
*/
void dtkNurbsCurve2DDataOCCT::extendedAabb(double* r_aabb, double factor) const
{
    aabb(r_aabb);
    factor -= 1.;
    factor /= 2.;
    auto dx = factor * (r_aabb[3] - r_aabb[0]);
    auto dy = factor * (r_aabb[4] - r_aabb[1]);
    r_aabb[0] -= dx;
    r_aabb[1] -= dy;
    r_aabb[2] += dx;
    r_aabb[3] += dy;
}

/*! \fn dtkNurbsCurve2DDataOCCT::clone(void) const
  Clone.
*/
dtkNurbsCurve2DDataOCCT* dtkNurbsCurve2DDataOCCT::clone(void) const
{
    return new dtkNurbsCurve2DDataOCCT(*this);
}

//
// dtkNurbsCurve2DDataOCCT.cpp ends here

// Local Variables:
// c-basic-offset: 4
// End:
