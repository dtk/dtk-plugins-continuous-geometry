// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#include "dtkNurbsCurve2DDataOCCT.h"
#include "dtkNurbsCurve2DDataOCCTPlugin.h"

#include <dtkCore>
#include <dtkContinuousGeometry>

// ///////////////////////////////////////////////////////////////////
// dtkNurbsCurve2DDataOCCTPlugin
// ///////////////////////////////////////////////////////////////////

void dtkNurbsCurve2DDataOCCTPlugin::initialize(void)
{
    dtkContinuousGeometry::abstractNurbsCurve2DData::pluginFactory().record("dtkNurbsCurve2DDataOCCT", dtkNurbsCurve2DDataOCCTCreator);
}

void dtkNurbsCurve2DDataOCCTPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkNurbsCurve2DDataOCCT)

//
// dtkNurbsCurve2DDataOCCTPlugin.cpp ends here
