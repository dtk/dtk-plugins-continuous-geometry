#ifndef DTK_PLUGINS_CONTINUOUS_GEOMETRY_OCCT_MULTIPLICITIES_H
#define DTK_PLUGINS_CONTINUOUS_GEOMETRY_OCCT_MULTIPLICITIES_H
#include <Standard_TypeDef.hxx>
#include <vector>
#include <utility>

std::pair<std::vector<Standard_Real>, std::vector<Standard_Integer>>
multiplicities(const double* value_ptr, int count)
{
  std::pair<std::vector<Standard_Real>, std::vector<Standard_Integer>> result;
  result.first.reserve(count);
  result.second.reserve(count);
  auto const end = value_ptr + count;
  // std::cerr << "multiplicities:\n  input:";
  // for(auto p = value_ptr; p < end; ++p) std::cerr << ' ' << *p;
  // std::cerr << '\n';
  while(true) {
    Standard_Integer mult = 1;
    auto other_value_ptr = value_ptr + 1;
    while(other_value_ptr != end && ((*other_value_ptr) == (*value_ptr))) {
      ++mult;
      ++other_value_ptr;
    }
    result.first.push_back(*value_ptr);
    result.second.push_back(mult);
    if(other_value_ptr == end) break;
    value_ptr = other_value_ptr;
  };
  // std::cerr << "  knots:";
  // for(auto p: result.first) std::cerr << ' ' << p;
  // std::cerr << "\n  mults:";
  // for(auto p: result.second) std::cerr << ' ' << p;
  // std::cerr << std::endl;
  return result;
}

#endif
