// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <QTest>

class dtkNurbsCurve2DDataOCCTTestCasePrivate;

class dtkNurbsCurve2DDataOCCTTestCase : public QObject
{
    Q_OBJECT

public:
    dtkNurbsCurve2DDataOCCTTestCase(void);
    ~dtkNurbsCurve2DDataOCCTTestCase(void);

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testControlPoint(void);
    void testWeight(void);
    void testDegree(void);
    void testKnots(void);
    void testCurvePoint(void);
    void testCurveNormal(void);
    void testPrintOutCurve(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);

private:
    dtkNurbsCurve2DDataOCCTTestCasePrivate* d;
};

//
// dtkNurbsCurve2DDataOCCTTest.h ends here
