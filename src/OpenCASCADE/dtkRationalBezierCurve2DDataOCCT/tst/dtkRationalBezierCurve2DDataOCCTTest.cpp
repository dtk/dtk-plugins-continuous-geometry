// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkRationalBezierCurve2DDataOCCTTest.h"

#include "dtkRationalBezierCurve2DDataOCCT.h"

#include "dtkRationalBezierCurve2D.h"

#include "dtkContinuousGeometrySettings.h"

#include <dtkTest>

// ///////////////////////////////////////////////////////////////////
// D-Pointer implementation
// ///////////////////////////////////////////////////////////////////

class dtkRationalBezierCurve2DDataOCCTTestCasePrivate{
public:
    dtkRationalBezierCurve2DDataOCCT* bezier_curve_data;

    double m_tolerance = 1e-15;
};
// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkRationalBezierCurve2DDataOCCTTestCase::dtkRationalBezierCurve2DDataOCCTTestCase(void):d(new dtkRationalBezierCurve2DDataOCCTTestCasePrivate())
{
    dtkLogger::instance().attachConsole();
}

dtkRationalBezierCurve2DDataOCCTTestCase::~dtkRationalBezierCurve2DDataOCCTTestCase(void)
{
}

void dtkRationalBezierCurve2DDataOCCTTestCase::initTestCase(void)
{
    d->bezier_curve_data = new dtkRationalBezierCurve2DDataOCCT();
    //degree 3
    std::size_t order = 4;

    double* cps = new double[(2 + 1) * order];
    cps[0] = 2.;  cps[1] = 0.;  cps[2] = 1.;
    cps[3] = 5.;  cps[4] = 3.;  cps[5] = 0.4;
    cps[6] = 0.;  cps[7] = 3.;  cps[8] = 0.7;
    cps[9] = 3.; cps[10] = 0.; cps[11] = 3.;

    d->bezier_curve_data->create(order, cps);

    delete[] cps;
}

void dtkRationalBezierCurve2DDataOCCTTestCase::init(void)
{

}
#include <iomanip>
void dtkRationalBezierCurve2DDataOCCTTestCase::testControlPoint(void)
{
    dtkContinuousGeometryPrimitives::Point_2 point(0., 0.);
    d->bezier_curve_data->controlPoint(1, point.data());
    QVERIFY(point[0] == 5);
    QVERIFY(point[1] > 3. - d->m_tolerance && point[1] < 3. + d->m_tolerance);
}

void dtkRationalBezierCurve2DDataOCCTTestCase::testWeight(void)
{
    double w = 0.;
    d->bezier_curve_data->weight(1, &w);
    QVERIFY(w == 0.4);
}

void dtkRationalBezierCurve2DDataOCCTTestCase::testDegree(void)
{
    QVERIFY(d->bezier_curve_data->degree() == 3);
}

void dtkRationalBezierCurve2DDataOCCTTestCase::testCurvePoint(void)
{
    dtkContinuousGeometryPrimitives::Point_2 point(0., 0.);
    d->bezier_curve_data->evaluatePoint(0.5, point.data());
    // QVERIFY(point[0] > 2.5 - d->m_tolerance && point[0] < 2.5 + d->m_tolerance);
    // QVERIFY(point[1] > 2.25 - d->m_tolerance && point[1] < 2.25 + d->m_tolerance);
}

//To ameliorate with dataCurve2DPoint and several pints to test
void dtkRationalBezierCurve2DDataOCCTTestCase::testCurveNormal(void)
{
    dtkContinuousGeometryPrimitives::Vector_2 normal(0., 0.);
    d->bezier_curve_data->evaluateNormal(0.5, normal.data());
}

void  dtkRationalBezierCurve2DDataOCCTTestCase::testAabb(void)
{
    double box[4];
    d->bezier_curve_data->aabb(&box[0]);

    QVERIFY(box[0] == 0.);
    QVERIFY(box[1] == 0.);
    QVERIFY(box[2] == 5.);
    QVERIFY(box[3] > 3. - d->m_tolerance && box[3] < 3. + d->m_tolerance);
}

void  dtkRationalBezierCurve2DDataOCCTTestCase::testExtendedAabb(void)
{
    double box[4];
    double factor = 1.1;
    d->bezier_curve_data->extendedAabb(&box[0], factor);
}

void dtkRationalBezierCurve2DDataOCCTTestCase::testSplit(void)
{
    dtkRationalBezierCurve2D *bezier_a = new dtkRationalBezierCurve2D();
    dtkRationalBezierCurve2D *bezier_b = new dtkRationalBezierCurve2D();

    d->bezier_curve_data->split(bezier_a, bezier_b, 0.5);

    QVERIFY(bezier_a->data() != nullptr);
    QVERIFY(bezier_b->data() != nullptr);
    QVERIFY(bezier_a->degree() == 3);
    QVERIFY(bezier_b->degree() == 3);
}

void dtkRationalBezierCurve2DDataOCCTTestCase::testMultiSplit(void)
{
    std::vector< double > splitting_parameters;
    splitting_parameters.push_back(0.15);
    splitting_parameters.push_back(0.3);
    splitting_parameters.push_back(0.75);
    std::list< dtkRationalBezierCurve2D * > split_beziers;
    d->bezier_curve_data->split(split_beziers, splitting_parameters);
    QVERIFY(split_beziers.size() == 4);
}

void dtkRationalBezierCurve2DDataOCCTTestCase::testPrintOutCurve(void)
{
    dtkContinuousGeometryPrimitives::Point_2 cpoint(0., 0.);
    std::ofstream bezier_curve_data_on_cp_file("bezier_curve_data_on_cp.xyz");
    for (std::size_t i = 0; i <= d->bezier_curve_data->degree(); ++i) {
        d->bezier_curve_data->controlPoint(i, cpoint.data());
        bezier_curve_data_on_cp_file << cpoint[0] << " " << cpoint[1] << " 0" << std::endl;
    }
    bezier_curve_data_on_cp_file.close();

    std::ofstream bezier_curve_data_on_file("bezier_curve_data_on.xyz");

    dtkContinuousGeometryPrimitives::Point_2 epoint(0., 0.);
    for (double i = 0.; i <= 1.; i += 0.01) {
        d->bezier_curve_data->evaluatePoint(i, epoint.data());
        bezier_curve_data_on_file << epoint[0] << " " << epoint[1] << " 0" << std::endl;
    }
    bezier_curve_data_on_file.close();
}

void dtkRationalBezierCurve2DDataOCCTTestCase::cleanup(void)
{

}

void dtkRationalBezierCurve2DDataOCCTTestCase::cleanupTestCase(void)
{
    delete d->bezier_curve_data;
    d->bezier_curve_data = nullptr;
}

DTKTEST_MAIN_NOGUI(dtkRationalBezierCurve2DDataOCCTTest, dtkRationalBezierCurve2DDataOCCTTestCase)

//
// dtkRationalBezierCurve2DDataOCCTTest.cpp ends here
