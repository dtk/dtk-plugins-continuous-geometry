// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <QTest>

class dtkRationalBezierCurve2DDataOCCTTestCasePrivate;

class dtkRationalBezierCurve2DDataOCCTTestCase : public QObject
{
    Q_OBJECT

public:
    dtkRationalBezierCurve2DDataOCCTTestCase(void);
    ~dtkRationalBezierCurve2DDataOCCTTestCase(void);

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testControlPoint(void);
    void testWeight(void);
    void testDegree(void);
    void testCurvePoint(void);
    void testCurveNormal(void);
    void testPrintOutCurve(void);
    void testAabb(void);
    void testExtendedAabb(void);
    void testSplit(void);
    void testMultiSplit(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);

private:
    dtkRationalBezierCurve2DDataOCCTTestCasePrivate* d;
};

//
// dtkRationalBezierCurve2DDataOCCTTest.h ends here
