// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <dtkContinuousGeometry.h>

#include <dtkContinuousGeometryUtils>

#include <dtkRationalBezierCurve2DDataOCCTExport.h>

#include <dtkAbstractRationalBezierCurve2DData>

#include <Standard_Handle.hxx>

class Geom2d_BezierCurve;

class DTKRATIONALBEZIERCURVE2DDATAOCCT_EXPORT dtkRationalBezierCurve2DDataOCCT final : public dtkAbstractRationalBezierCurve2DData
{
private:
    typedef dtkContinuousGeometryPrimitives::Point_2 Point_2;
    typedef dtkContinuousGeometryPrimitives::Vector_2 Vector_2;

public:
    dtkRationalBezierCurve2DDataOCCT(void);
    // dtkRationalBezierCurve2DDataOCCT(const dtkRationalBezierCurve2DDataOCCT& mesh_data);
    ~dtkRationalBezierCurve2DDataOCCT(void) final ;

public:
    void create(std::size_t order, double* cps) const override;
    void create(const Handle(Geom2d_BezierCurve)& bezier_curve);

public:
    std::size_t degree(void) const override;
    void controlPoint(std::size_t i, double* r_cp) const override;
    void weight(std::size_t i, double* r_w) const override;

public:
    void evaluatePoint(double p_u, double* r_point) const override;
    void evaluateNormal(double p_u, double* r_normal) const override;

 public:
    void split(dtkRationalBezierCurve2D *r_split_curve_a, dtkRationalBezierCurve2D *r_split_curve_b, double splitting_parameter) const override;
    void split(std::list< dtkRationalBezierCurve2D * >& r_split_curves, const std::vector< double >& p_splitting_parameters) const override;

 public:
    void aabb(double* r_aabb) const override;
    void extendedAabb(double* r_aabb, double factor) const override;

 public:
    Geom2d_BezierCurve& occtRationalBezierCurve2D(void);

public:
    dtkRationalBezierCurve2DDataOCCT* clone(void) const override;

private :
    mutable Handle(Geom2d_BezierCurve) d;
};


// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

inline dtkAbstractRationalBezierCurve2DData *dtkRationalBezierCurve2DDataOCCTCreator(void)
{
    return new dtkRationalBezierCurve2DDataOCCT();
}

//
// dtkRationalBezierCurve2DDataOCCT.h ends here
