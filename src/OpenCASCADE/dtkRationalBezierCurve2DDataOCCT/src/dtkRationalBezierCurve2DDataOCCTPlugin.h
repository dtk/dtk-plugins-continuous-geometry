// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <QtCore>

#include <dtkContinuousGeometry>

#include <dtkAbstractRationalBezierCurve2DData>

class dtkRationalBezierCurve2DDataOCCTPlugin : public dtkAbstractRationalBezierCurve2DDataPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkAbstractRationalBezierCurve2DDataPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkRationalBezierCurve2DDataOCCTPlugin" FILE "dtkRationalBezierCurve2DDataOCCTPlugin.json")

public:
     dtkRationalBezierCurve2DDataOCCTPlugin(void) {}
    ~dtkRationalBezierCurve2DDataOCCTPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

//
// dtkRationalBezierCurve2DDataOCCTPlugin.h ends here
