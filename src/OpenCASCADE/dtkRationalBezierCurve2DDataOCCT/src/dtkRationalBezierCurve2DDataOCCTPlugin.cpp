// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#include "dtkRationalBezierCurve2DDataOCCT.h"
#include "dtkRationalBezierCurve2DDataOCCTPlugin.h"

#include <dtkCore>
#include <dtkContinuousGeometry>

// ///////////////////////////////////////////////////////////////////
// dtkRationalBezierCurve2DDataOCCTPlugin
// ///////////////////////////////////////////////////////////////////

void dtkRationalBezierCurve2DDataOCCTPlugin::initialize(void)
{
    dtkContinuousGeometry::abstractRationalBezierCurve2DData::pluginFactory().record("dtkRationalBezierCurve2DDataOCCT", dtkRationalBezierCurve2DDataOCCTCreator);
}

void dtkRationalBezierCurve2DDataOCCTPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkRationalBezierCurve2DDataOCCT)

//
// dtkRationalBezierCurve2DDataOCCTPlugin.cpp ends here
