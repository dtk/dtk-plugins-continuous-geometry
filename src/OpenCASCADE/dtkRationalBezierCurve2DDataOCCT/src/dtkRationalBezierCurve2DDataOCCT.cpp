// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkRationalBezierCurve2DDataOCCT.h"

#include <dtkRationalBezierCurve2D>

#include <cassert>

#include <Geom2d_BezierCurve.hxx>
#include <Geom2d_BSplineCurve.hxx>
#include <Bnd_Box2d.hxx>
#include <BndLib_Add2dCurve.hxx>
#include <Geom2dAdaptor_Curve.hxx>
#include <Geom2dConvert.hxx>
#include <Geom2dConvert_CompCurveToBSplineCurve.hxx>
#include <Geom2dConvert_BSplineCurveToBezierCurve.hxx>

/*!
  \class dtkRationalBezierCurve2DDataOCCT
  \inmodule dtkPluginsContinuousGeometry
  \brief dtkRationalBezierCurve2DDataOCCT is an openNURBS implementation of the concept dtkAbstractRationalBezierCurve2DData.

  The following lines of code show how to instantiate and use the class in pratice:
  \code
  dtkAbstractRationalBezierCurve2DData *rational_bezier_curve_data = dtkContinuousGeometry::abstractRationalBezierCurve2DData::pluginFactory().create("dtkRationalBezierCurve2DDataOCCT");
  dtkRationalBezierCurve2D *rational_bezier_curve = new dtkRationalBezierCurve2D(rational_bezier_curve_data);
  rational_bezier_curve->create(...);
  \endcode
*/

/*! \fn dtkRationalBezierCurve2DDataOCCT::dtkRationalBezierCurve2DDataOCCT(void)
  Instantiates the structure.

  To add the content, see \l create(std::size_t order, double *cps) const.
*/
dtkRationalBezierCurve2DDataOCCT::dtkRationalBezierCurve2DDataOCCT(void) : d(nullptr) {}

/*! \fn dtkRationalBezierCurve2DDataOCCT::~dtkRationalBezierCurve2DDataOCCT(void)
  Destroys the instance.
*/
dtkRationalBezierCurve2DDataOCCT::~dtkRationalBezierCurve2DDataOCCT(void)
{
}

/*!
  Not available in the dtkAbstractRationalBezierCurve2DData.

  Creates the 2D rational Bezier curve by providing a rational Bezier curve from the OpenCASCADE API.

  \a bezier_curve : the rational Bezier curve to copy.
*/
void dtkRationalBezierCurve2DDataOCCT::create(const Handle(Geom2d_BezierCurve)& bezier_curve)
{
    d = Handle(Geom2d_BezierCurve)::DownCast(bezier_curve->Copy());
}

/*! \fn dtkRationalBezierCurve2DDataOCCT::create(std::size_t order, double *cps) const
  Creates the 2D rational Bezier curve by providing the required content.

 \a order : order

 \a cps : array containing the weighted control points, specified as : [x0, y0, w0, ...]
*/
void dtkRationalBezierCurve2DDataOCCT::create(std::size_t order, double* cps) const
{
    TColgp_Array1OfPnt2d poles(1, order);
    TColStd_Array1OfReal weights(1, order);
    auto pole_ptr = &poles.ChangeValue(1);
    auto weights_ptr = &weights.ChangeValue(1);

    /// @TODO: use a loop more efficient with pointers instead of ChangeValue
    for(std::size_t i = 0; i < order; ++i) {
        poles.ChangeValue(i+1) = gp_Pnt2d{cps[0], cps[1]};
        weights.ChangeValue(i+1) = cps[2];
        cps += 3;
    }
    d = new Geom2d_BezierCurve{poles, weights};
}

/*! \fn  dtkRationalBezierCurve2DDataOCCT::degree(void) const
  Returns the degree of the curve.
*/
std::size_t dtkRationalBezierCurve2DDataOCCT::degree(void) const
{
    return d->Degree();
}

/*! \fn  dtkRationalBezierCurve2DDataOCCT::controlPoint(std::size_t i, double *r_cp) const
  Writes in \a r_cp the \a i th weighted control point coordinates[xi, yi].

  \a i : index of the weighted control point

  \a r_cp : array of size 2 to store the weighted control point coordinates[xi, yi]

  The method can be called as follow:
  \code
  dtkContinuousGeometryPrimitives::Point_2 point2(0, 0);
  rational_bezier_curve_data->controlPoint(3, point2.data());
  \endcode
*/
void dtkRationalBezierCurve2DDataOCCT::controlPoint(std::size_t i, double* r_cp) const
{
    const auto& pole = d->Pole(i+1);
    r_cp[0] = pole.X();
    r_cp[1] = pole.Y();
}

/*! \fn  dtkRationalBezierCurve2DDataOCCT::weight(std::size_t i, double *r_w) const
  Writes in \a r_w the \a i th weighted control point weight wi.

  \a i : index of the weighted control point

  \a r_w : array of size 1 to store the weighted control point weight wi
*/
void dtkRationalBezierCurve2DDataOCCT::weight(std::size_t i, double* r_w) const
{
    *r_w = d->Weight(i + 1);
}

/*! \fn dtkRationalBezierCurve2DDataOCCT::evaluatePoint(double p_u, double *r_point) const
  Writes in \a r_point the evaluation at parameter \a p_u.

  \a p_u : parameter

  \a r_point : array of size 2 to store the result of the evaluation
*/
void dtkRationalBezierCurve2DDataOCCT::evaluatePoint(double p_u, double* r_point) const
{
    auto p = d->Value(p_u);
    r_point[0] = p.X();
    r_point[1] = p.Y();
}

/*! \fn dtkRationalBezierCurve2DDataOCCT::evaluateNormal(double p_u, double *r_normal) const
  Writes in \a r_normal the normal evaluation at parameter \a p_u.

  \a p_u : parameter

  \a r_normal : array of size 2 to store the result of the normal evaluation
*/
void dtkRationalBezierCurve2DDataOCCT::evaluateNormal(double p_u, double* r_normal) const
{
    gp_Pnt2d p;
    gp_Vec2d der1;
    d->D1(p_u, p, der1);
    r_normal[0] = der1.X();
    r_normal[1] = der1.Y();
}

/*! \fn dtkRationalBezierCurve2DDataOCCT::split(dtkRationalBezierCurve2D *r_split_curve_a, dtkRationalBezierCurve2D *r_split_curve_b, double p_splitting_parameter) const
  Splits at \a p_splitting_parameter the rational Bezier curve and stores in \a r_split_curve_a and \a r_split_curve_b the split parts (from 0 to \a p_splitting_parameter and from \a p_splitting_parameter to 1 respectively).

  \a r_split_curve_a : a pointer to a valid dtkRationalBezierCurve2D with data not initialized, in which the first part of the curve (from 0 to to \a p_splitting_parameter) will be stored

  \a r_split_curve_b : a pointer to a valid dtkRationalBezierCurve2D with data not initialized, in which the second part of the curve (from \a p_splitting_parameter to 1) will be stored

  \a p_splitting_parameter : the parameter at which the curve will be split
*/
void dtkRationalBezierCurve2DDataOCCT::split(dtkRationalBezierCurve2D *r_split_curve_a, dtkRationalBezierCurve2D *r_split_curve_b, double splitting_parameter) const
{
    Geom2dConvert_CompCurveToBSplineCurve convert{d};
    auto bsplinecurve = convert.BSplineCurve();
    assert(bsplinecurve->NbPoles() > 1);
    auto bsplinecurve_a = Geom2dConvert::SplitBSplineCurve(bsplinecurve, bsplinecurve->FirstParameter(), splitting_parameter, 0.);
    auto bsplinecurve_b = Geom2dConvert::SplitBSplineCurve(bsplinecurve, splitting_parameter, bsplinecurve->LastParameter(), 0.);
    assert(bsplinecurve_a->NbPoles() > 1);
    assert(bsplinecurve_b->NbPoles() > 1);
    Geom2dConvert_BSplineCurveToBezierCurve convert_a_to_bezier{bsplinecurve_a};
    Geom2dConvert_BSplineCurveToBezierCurve convert_b_to_bezier{bsplinecurve_b};
    assert(convert_a_to_bezier.NbArcs() == 1);
    assert(convert_b_to_bezier.NbArcs() == 1);
    auto curve_a_data = new dtkRationalBezierCurve2DDataOCCT;
    curve_a_data->create(convert_a_to_bezier.Arc(1));
    auto curve_b_data = new dtkRationalBezierCurve2DDataOCCT;
    curve_b_data->create(convert_b_to_bezier.Arc(1));
    r_split_curve_a->setData(curve_a_data);
    r_split_curve_b->setData(curve_b_data);
}

/*! \fn dtkRationalBezierCurve2DDataOCCT::split(std::list< dtkRationalBezierCurve2D * >& r_split_curves, const std::vector< double >& p_splitting_parameters) const
  Splits the rational Bezier curve at each value of \a p_splitting_parameters and stores in \a r_split_curves the split parts.

  \a r_split_curves : a list to which the split rational Bezier curves will be added

  \a p_splitting_parameters : the parameters at which the curve will be split
*/
void dtkRationalBezierCurve2DDataOCCT::split(std::list< dtkRationalBezierCurve2D * >& r_split_curves, const std::vector< double >& p_splitting_parameters) const
{
    // ///////////////////////////////////////////////////////////////////
    // Makes sure that the list r_split_curves is empty or warn that some pointed data may be lost
    // ///////////////////////////////////////////////////////////////////
    for (auto it = r_split_curves.begin(); it != r_split_curves.end(); ++it) {
        if ((*it) != nullptr) {
            dtkWarn() << "r_split_curves contains some non nullptr, the pointed data might be lost";
        }
    }
    r_split_curves.clear();
    // ///////////////////////////////////////////////////////////////////
    // Make sure that all splitting parameters are between 0. and 1.
    // ///////////////////////////////////////////////////////////////////
    //TODO
    std::vector< double > splitting_parameters = p_splitting_parameters;
    std::sort(splitting_parameters.begin(), splitting_parameters.end());
    // ///////////////////////////////////////////////////////////////////
    // Copies the current dtkRationalBezierCurve
    // ///////////////////////////////////////////////////////////////////
    dtkRationalBezierCurve2D *curve = new dtkRationalBezierCurve2D(this->clone());
    r_split_curves.push_back(curve);
    auto split_curve = r_split_curves.begin();
    double param = 0.;
    auto p = splitting_parameters.begin();
    while (!splitting_parameters.empty()) {
        param = *p;
        if (param <= 0. || param >= 1.) {
            splitting_parameters.erase(p);
        } else {
            dtkRationalBezierCurve2D *split_curve_a = new dtkRationalBezierCurve2D();
            dtkRationalBezierCurve2D *split_curve_b = new dtkRationalBezierCurve2D();
            (*split_curve)->split(split_curve_a, split_curve_b, *p);
            delete (*split_curve);
            split_curve = r_split_curves.erase(split_curve);
            r_split_curves.insert(split_curve, split_curve_b);
            --split_curve;
            r_split_curves.insert(split_curve, split_curve_a);
            splitting_parameters.erase(p);
            // ///////////////////////////////////////////////////////////////////
            // Recompute the remaining parameters for splitting the second of the two curves
            // ///////////////////////////////////////////////////////////////////
            for (auto rp = splitting_parameters.begin(); rp != splitting_parameters.end(); ++rp) {
                (*rp) = ((*rp) - param) / (1. - param);
            }
        }
    }
}

/*! \fn dtkRationalBezierCurve2DDataOCCT::aabb(double* r_aabb) const
  Writes in \a r_aabb the [xmin, ymin, xmax, ymax] coordinates.

  \a r_aabb : array of size 4 to store the limits coordinates
*/
void dtkRationalBezierCurve2DDataOCCT::aabb(double* r_aabb) const
{
    Bnd_Box2d bb;
    BndLib_Add2dCurve::Add(Geom2dAdaptor_Curve(this->d), 0., bb);
    bb.Get(r_aabb[0], r_aabb[1], r_aabb[2], r_aabb[3]);
}

/*! \fn dtkRationalBezierCurve2DDataOCCT::extendedAabb(double *r_aabb, double factor) const
  Writes in \a r_aabb the [xmin, ymin, xmax, ymax] coordinates of an extened bounding box of factor \a factor. The extension factor extends the diagonal.

  \a r_aabb : array of size 4 to store the extended limits coordinates

  \a factor : factor used to lengthen the diagonal of the axis aligned bounding box
*/
void dtkRationalBezierCurve2DDataOCCT::extendedAabb(double* r_aabb, double factor) const
{
    aabb(r_aabb);
    factor -= 1.;
    factor /= 2.;
    auto dx = factor * (r_aabb[2] - r_aabb[0]);
    auto dy = factor * (r_aabb[3] - r_aabb[1]);
    r_aabb[0] -= dx;
    r_aabb[1] -= dy;
    r_aabb[2] += dx;
    r_aabb[3] += dy;
}

/*! \fn dtkRationalBezierCurve2DDataOCCT::onRationalBezierCurve2D(void)
  Not available in the dtkAbstractRationalBezierCurve2DData.

  Returns the underlying OpenCASCADE rational Bezier curve.
 */
Geom2d_BezierCurve& dtkRationalBezierCurve2DDataOCCT::occtRationalBezierCurve2D(void)
{
    return *d;
}

/*! \fn dtkRationalBezierCurve2DDataOCCT::clone(void) const
   Clone
*/
dtkRationalBezierCurve2DDataOCCT* dtkRationalBezierCurve2DDataOCCT::clone(void) const
{
    return new dtkRationalBezierCurve2DDataOCCT(*this);
}

//
// dtkRationalBezierCurve2DDataOCCT.cpp ends here

// Local Variables:
// c-basic-offset: 4
// End:
