// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "OpenCASCADEBRepReader.h"
#include "OpenCASCADEBRepReaderPlugin.h"
#include "dtkContinuousGeometry.h"

#include <dtkCore>

// ///////////////////////////////////////////////////////////////////
// OpenCASCADEBRepReaderPlugin
// ///////////////////////////////////////////////////////////////////

void OpenCASCADEBRepReaderPlugin::initialize(void)
{
    dtkContinuousGeometry::bRepReader::pluginFactory().record("OpenCASCADEBRepReader", OpenCASCADEBRepReaderCreator);
}

void OpenCASCADEBRepReaderPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(OpenCASCADEBRepReader)

//
// OpenCASCADEBRepReaderPlugin.cpp ends here
