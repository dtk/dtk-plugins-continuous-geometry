// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkBRepReader.h>

#include <OpenCASCADEBRepReaderExport.h>

#include <dtkCore>

class OPENCASCADEBREPREADER_EXPORT OpenCASCADEBRepReaderPlugin : public dtkBRepReaderPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkBRepReaderPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.OpenCASCADEBRepReaderPlugin" FILE "OpenCASCADEBRepReaderPlugin.json")
public:
    void initialize(void);
    void uninitialize(void);
};

//
// OpenCASCADEBRepReaderPlugin.h ends here
