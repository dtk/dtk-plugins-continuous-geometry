// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkBRepReader>
#include <dtkTrim>

#include <OpenCASCADEBRepReaderExport.h>

#include <TopoDS.hxx>
#include <Geom_Curve.hxx>
#include <Geom2d_Curve.hxx>
#include <Geom2d_BSplineCurve.hxx>
#include <Geom_Surface.hxx>
#include <ShapeAnalysis_WireOrder.hxx>

class OpenCASCADEBRepReaderPrivate;

class dtkBRep;

class OPENCASCADEBREPREADER_EXPORT OpenCASCADEBRepReader : public dtkBRepReader
{
public:
     OpenCASCADEBRepReader(void);
    ~OpenCASCADEBRepReader(void);

public:
    void  setInputBRepFilePath(const QString&) final;
    void  setInputDumpFilePath(const QString&) final;

    void run(void) final;

    dtkBRep* outputDtkBRep(void) const final;

private:
    bool checkEdgeCurveMatch(const TopoDS_Edge& edge_ds, const TopoDS_Face& face_ds, const Handle(Geom_Curve)& curve_3d, const Handle(Geom2d_Curve)& curve_2d, const Handle(Geom_Surface)& surface, double& parameter_first, double& parameter_last, bool reversed = false);
    bool checkPCurvesMatch(const ShapeAnalysis_WireOrder& wire_order, std::vector<Handle(Geom2d_BSplineCurve)> pcurves, std::vector<dtkTrim*>& trims, double tol = 1.e-6);

protected:
    OpenCASCADEBRepReaderPrivate *d;
};

// ///////////////////////////////////////////////////////////////////

inline dtkBRepReader* OpenCASCADEBRepReaderCreator(void)
{
    return new OpenCASCADEBRepReader();
}

//
// OpenCASCADEBRepReader.h ends here