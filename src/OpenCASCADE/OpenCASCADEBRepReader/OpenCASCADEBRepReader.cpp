#define DEBUG_TRIM_LOOPS 0

constexpr double tolerance_in_parameters_space = 1e-7;
// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "OpenCASCADEBRepReader.h"

#include <dtkNurbsCurve2DDataOCCT.h>
#include <dtkNurbsCurveDataOCCT.h>
#include <dtkNurbsSurfaceDataOCCT.h>

#include <dtkContinuousGeometry.h>

#include <dtkContinuousGeometryUtils>

#include <dtkBRep>
#include <dtkBRepData>
#include <dtkBRepReader>
#include <dtkAbstractNurbsSurfaceData>
#include <dtkNurbsSurface>
#include <dtkAbstractTrimLoopData>
#include <dtkNurbsCurve>
#include <dtkNurbsCurve2D>
#include <dtkTrimLoop>
#include <dtkTopoTrim>

#include <STEPControl_Reader.hxx>
#include <TopoDS_Shape.hxx>
#include <TopoDS_Wire.hxx>
#include <TopoDS_Iterator.hxx>
#include <BRepTools.hxx>
#include <BRep_Tool.hxx>
#include <BRepBuilderAPI_NurbsConvert.hxx>
#include <BRep_TFace.hxx>
#include <BRep_TEdge.hxx>
#include <BRep_TVertex.hxx>
#include <BRep_GCurve.hxx>
#include <Geom_BSplineSurface.hxx>
#include <Geom_BSplineCurve.hxx>
#include <Geom2d_Line.hxx>
#include <GeomTools_SurfaceSet.hxx>
#include <GeomTools_CurveSet.hxx>
#include <GeomTools_Curve2dSet.hxx>
#include <Bnd_Box.hxx>
#include <BRepBndLib.hxx>
#include <Geom_RectangularTrimmedSurface.hxx>
#include <Geom_CylindricalSurface.hxx>

#include <Geom2dConvert_BSplineCurveToBezierCurve.hxx>
#include <GeomConvert_BSplineCurveToBezierCurve.hxx>
#include <Geom2dConvert_CompCurveToBSplineCurve.hxx>
#include <GeomConvert_CompCurveToBSplineCurve.hxx>

#include <ShapeAnalysis_Edge.hxx>
#include <GeomAPI_ProjectPointOnCurve.hxx>
#include <GeomConvert.hxx>

#include <gp_Pnt2d.hxx>

#include <sstream>

// /////////////////////////////////////////////////////////////////
// OpenCASCADEBRepReaderPrivate
// /////////////////////////////////////////////////////////////////

class OpenCASCADEBRepReaderPrivate
{
public:
    QString in_brep_file_path;
    QString in_dump_file_path;

    dtkBRep* dtk_brep = nullptr;
};

// ///////////////////////////////////////////////////////////////////
// OpenCASCADEBRepReader implementation
// ///////////////////////////////////////////////////////////////////

OpenCASCADEBRepReader::OpenCASCADEBRepReader(void) : dtkBRepReader(), d(new OpenCASCADEBRepReaderPrivate)
{
}

OpenCASCADEBRepReader::~OpenCASCADEBRepReader(void)
{
    delete d;
    d = nullptr;
}

void OpenCASCADEBRepReader::setInputBRepFilePath(const QString& file)
{
    d->in_brep_file_path = file;
}

void OpenCASCADEBRepReader::setInputDumpFilePath(const QString& file)
{
    d->in_dump_file_path = file;
}

void OpenCASCADEBRepReader::run(void)
{
    STEPControl_Reader reader;

    switch(auto error = reader.ReadFile(d->in_brep_file_path.toStdString().c_str())) {
    case IFSelect_RetDone:
        dtkInfo() << "File " << d->in_brep_file_path << " is loaded";
        break;
    default:
        dtkInfo() << "File " << d->in_brep_file_path << " is NOT loaded"
                  << "\n  Error code: " << static_cast<int>(error);
        return;
    }
    reader.PrintCheckLoad(false, IFSelect_GeneralInfo);
    Standard_Integer NbRoots = reader.NbRootsForTransfer();

    // gets the number of transferable roots
    dtkInfo() << "Number of roots in STEP file:" << NbRoots;

    Standard_Integer NbTrans = reader.TransferRoots();
    // translates all transferable roots, and returns the number of    //successful translations
    dtkInfo() << "STEP roots transferred: " << NbTrans;
    dtkInfo() << "Number of resulting shapes is: " << reader.NbShapes();

    TopoDS_Shape result = reader.OneShape();

    std::function<void(const TopoDS_Shape &)> visit_and_fix_geometry =
        [&](const TopoDS_Shape &shape) {
          auto type = shape.ShapeType();
          switch (type) {
          case TopAbs_FACE: {
            auto face = Handle(BRep_TFace)::DownCast(shape.TShape());
            auto surface = face->Surface();
            auto rect_trimmed_surface =
                Handle(Geom_RectangularTrimmedSurface)::DownCast(surface);
            if (rect_trimmed_surface) {
              Standard_Real U1, U2, V1, V2;
              rect_trimmed_surface->Bounds(U1, U2, V1, V2);
              if (STANDARD_TYPE(Geom_CylindricalSurface) ==
                  rect_trimmed_surface->BasisSurface()->DynamicType()) {
                if (U1 < 0 && U1 >= -Epsilon(Abs(U2 - U1))) {
                  std::cerr << "BLAH!!!";
                  U1 = 0.;
                  rect_trimmed_surface->SetTrim(U1, U2, Standard_True);
                }
                if ((U2 - U1) > 2 * M_PI) {
                  U2 = U1 + 2 * M_PI;
                  while ((U2 - U1) > 2 * M_PI) {
                    U2 = std::nextafter(U2, U1);
                  }
                  rect_trimmed_surface->SetTrim(U1, U2, Standard_True);
                } else if ((U2 - U1) < 0.) {
                  U1 = U2 - 2 * M_PI;
                  while ((U2 - U1) < 0.) {
                    U1 = std::nextafter(U1, U2);
                  }
                  rect_trimmed_surface->SetTrim(U1, U2, Standard_True);
                }
              }
            };
          } break;
          default:
            break;
          };
          for (TopoDS_Iterator iterator(shape); iterator.More();
               iterator.Next()) {
            visit_and_fix_geometry(iterator.Value());
          }
        };
    visit_and_fix_geometry(result);
    BRepBuilderAPI_NurbsConvert nurbsconvert{result};
    TopoDS_Shape converted = nurbsconvert.Shape();
    // BRepTools::Dump(converted, std::cerr);
    const auto& main_shape = converted;

    dtkBRepData* dtk_brep_data = new dtkBRepData();
    d->dtk_brep = new dtkBRep(dtk_brep_data);

    Bnd_Box occt_bbox;
    BRepBndLib::Add(main_shape, occt_bbox);
    occt_bbox.Get(dtk_brep_data->m_aabb[0], dtk_brep_data->m_aabb[1],
                  dtk_brep_data->m_aabb[2], dtk_brep_data->m_aabb[3],
                  dtk_brep_data->m_aabb[4], dtk_brep_data->m_aabb[5]);

    std::unordered_map<const BRep_TEdge*, dtkTopoTrim*> trims_map;

    GeomTools_SurfaceSet all_surfaces;
    GeomTools_CurveSet all_3dcurves;
    GeomTools_Curve2dSet all_2dcurves;
    TopTools_IndexedMapOfShape all_shapes;

    auto add_all_shapes = [&]() {
        std::function<void (const TopoDS_Shape&)> add_shapes_rec = [&](const TopoDS_Shape& shape) {
            all_shapes.Add(shape);
            switch(shape.ShapeType()) {
            case TopAbs_FACE: {
                auto tface = Handle(BRep_TFace)::DownCast(shape.TShape());
                assert(!tface->Surface().IsNull());
                all_surfaces.Add(tface->Surface());
                break;
            }
            case TopAbs_EDGE: {
                auto tedge = Handle(BRep_TEdge)::DownCast(shape.TShape());
                for (auto curve : tedge->Curves()) {
                  if (curve->IsCurve3D()) {
                      all_3dcurves.Add(curve->Curve3D());
                  } else if (curve->IsCurveOnClosedSurface()) {
                      all_2dcurves.Add(curve->PCurve());
                      all_2dcurves.Add(curve->PCurve2());
                  } else if (curve->IsCurveOnSurface()) {
                      all_2dcurves.Add(curve->PCurve());
                  } else if (curve->IsRegularity()) {
                    // all_surfaces.Add(curve->Surface());
                    // all_surfaces.Add(curve->Surface2());
                  }
                }
                break;
            }
            case TopAbs_VERTEX: {
                auto tvertex = Handle(BRep_TVertex)::DownCast(shape.TShape());
                for(auto point: tvertex->Points()) {
                    if (point->IsPointOnCurve()) {
                        all_3dcurves.Add(point->Curve());
                    }
                    else if (point->IsPointOnCurveOnSurface()) {
                        all_2dcurves.Add(point->PCurve());
                        // all_surfaces.Add(point->Surface());
                    }
                    else if (point->IsPointOnSurface()) {
                        // all_surfaces.Add(point->Surface());
                    }
                }
                break;
            }
            case TopAbs_COMPOUND:
            case TopAbs_COMPSOLID:
            case TopAbs_SOLID:
            case TopAbs_SHELL:
            case TopAbs_WIRE:
            case TopAbs_SHAPE:
                // no geometry to add
                break;
            } // end switch
            for (TopoDS_Iterator it(shape); it.More(); it.Next()) {
                add_shapes_rec(it.Value());
            }
        };
        add_shapes_rec(main_shape);
    };

    add_all_shapes();

    auto shape_type = [](const TopoDS_Shape &shape) {
      using namespace std::string_literals;
      auto type = shape.ShapeType();
      switch (type) {
      case TopAbs_COMPOUND:
        return "COMPOUND"s;
      case TopAbs_COMPSOLID:
        return "COMPSOLID"s;
      case TopAbs_SOLID:
        return "SOLID"s;
      case TopAbs_SHELL:
        return "SHELL"s;
      case TopAbs_FACE:
        return "FACE"s;
      case TopAbs_WIRE:
        return "WIRE"s;
      case TopAbs_EDGE:
        return "EDGE"s;
      case TopAbs_VERTEX:
        return "VERTEX"s;
      case TopAbs_SHAPE:
        return "SHAPE"s;
      }
      std::stringstream s;
      s << "UNKNOWN TYPE (" << static_cast<int>(type) << ")";
      return s.str();
    };
    auto display_geom_type = [&](int indentation, const TopoDS_Shape &shape) {
      auto type = shape.ShapeType();
      std::string result{};
      switch (type) {
      case TopAbs_FACE: {
        auto face = Handle(BRep_TFace)::DownCast(shape.TShape());
        auto surface = face->Surface();
        result = "(surf#" + std::to_string(all_surfaces.Index(surface));
        result = result + ") surface type: ";
        result = result + surface->DynamicType()->Name();
        std::stringstream s;
        s.precision(17);
        auto rect_trimmed_surface = Handle(Geom_RectangularTrimmedSurface)::DownCast(surface);
        if (rect_trimmed_surface) {
          s << "  ("
            << rect_trimmed_surface->BasisSurface()->DynamicType()->Name()
            << ")";
        }
        Standard_Real U1, U2, V1, V2;
        surface->Bounds(U1, U2, V1, V2);
        s << " [ " << U1 << ", " << U2 << " ] ×";
        s << " [ " << V1 << ", " << V2 << " ]";
        result = result + s.str();
        if (rect_trimmed_surface) {
          if(STANDARD_TYPE(Geom_CylindricalSurface) == rect_trimmed_surface->BasisSurface()->DynamicType()) {
            if(U1 < 0 && U1 >= -Epsilon(Abs(U2-U1))) {
              U1 = 0.;
              rect_trimmed_surface->SetTrim(U1, U2, Standard_True);
            }
            if((U2-U1) > 2*M_PI) {
              U2 = U1 + 2*M_PI;
              while((U2-U1) > 2*M_PI) {
                U2 = std::nextafter(U2, U1);
              }
              rect_trimmed_surface->SetTrim(U1, U2, Standard_True);
            }
            else if((U2-U1) < 0.) {
              U1 = U2 - 2*M_PI;
              while((U2-U1) < 0.) {
                U1 = std::nextafter(U1, U2);
              }
              rect_trimmed_surface->SetTrim(U1, U2, Standard_True);
            }
          }
        };
      } break;
      case TopAbs_EDGE: {
        auto display_pcurve_curve = [&](const Handle(Geom2d_Curve) & pcurve) {
          std::string result = "curve #" +
                               std::to_string(all_2dcurves.Index(pcurve)) + " " +
                               pcurve->DynamicType()->Name();
          auto geom2d_line = Handle(Geom2d_Line)::DownCast(pcurve);
          if (!geom2d_line.IsNull()) {
            std::stringstream output;
            output.precision(17);
            output << "=( p:(" << geom2d_line->Location().X() << ", "
                   << geom2d_line->Location().Y() << "), dir:("
                   << geom2d_line->Direction().X() << ", "
                   << geom2d_line->Direction().Y() << ") )";
            result = result + output.str();
          }
          return result;
        };
        auto display_surface = [&](const Handle(Geom_Surface)& surf) {
          std::string result = "surf#" + std::to_string(all_surfaces.Index(surf));
          return result;
        };
        auto edge = Handle(BRep_TEdge)::DownCast(shape.TShape());
        for(auto c: edge->Curves()) {
          result = result + '\n' + std::string(indentation+2, ' ');
          if (c->IsCurve3D()) {
            result = result + "- 3d curve #" + std::to_string(all_3dcurves.Index(c->Curve3D()));
          } else {
            result = result + "- curve";
          }
          result = result + " " + c->DynamicType()->Name();
          auto gcurve = Handle(BRep_GCurve)::DownCast(c);
          if(!gcurve.IsNull()) {
              std::stringstream s;
              s.precision(17);
              s << " range("  << gcurve->First() << ", " << gcurve->Last() << ") ";
              result = result + s.str();
          }
          if (c->IsCurve3D()) {
            auto curve3d = c->Curve3D();
            result = result + "(" + (curve3d.IsNull() ? "NULL" : curve3d->DynamicType()->Name()) + ")";
          } else if (c->IsCurveOnClosedSurface()) {
            result = result + "(" + display_surface(c->Surface())+ ": ";
            auto pcurve = c->PCurve();
            result = result + display_pcurve_curve(pcurve) + " + ";
            pcurve = c->PCurve2();
            result = result + display_pcurve_curve(pcurve) + ")";
          } else if (c->IsCurveOnSurface()) {
            auto pcurve = c->PCurve();
            result = result + "(" + display_surface(c->Surface()) + ": " + display_pcurve_curve(pcurve) + ")";
          } else if (c->IsRegularity()) {
            result = result + "(" + display_surface(c->Surface()) + "/" + display_surface(c->Surface2()) + ")";
          }
        }
      } break;
      case TopAbs_VERTEX: {
        auto vertex = Handle(BRep_TVertex)::DownCast(shape.TShape());
        auto pt_xyz = vertex->Pnt().XYZ();
        std::stringstream output;
        output.precision(17);
        output << "(" << pt_xyz.X() << ", " << pt_xyz.Y() << ", " << pt_xyz.Z()<< ")";
        result = output.str();
      } break;
      case TopAbs_COMPOUND:
      case TopAbs_COMPSOLID:
      case TopAbs_SOLID:
      case TopAbs_SHELL:
      case TopAbs_WIRE:
      case TopAbs_SHAPE:
        break;
      }
      return result;
    };
    std::function<void(const TopoDS_Shape &, int)> visit =
        [&](const TopoDS_Shape &shape, int indent = 0) {
            std::cerr << indent << std::string(indent*2-1, ' ') << shape_type(shape);
            if (shape.Orientation() != TopAbs_FORWARD)
                std::cerr << "[-]";
            std::cerr << " #" << all_shapes.FindIndex(shape)
            << " (" << (void*) &(*shape.TShape()) <<")";
            auto geom_type = display_geom_type(indent*2, shape);
            if(!geom_type.empty()) {
                std::cerr << "  " << geom_type;
            }
            std::cerr << '\n';
            for (TopoDS_Iterator iterator(shape); iterator.More();
                 iterator.Next()) {
                visit(iterator.Value(), indent + 1);
            }
        };
    std::cerr << "Start the description of the content of the file...\n";
    visit(main_shape, 1);

    const bool no_trim_loops = qEnvironmentVariableIsSet("NO_TRIM");
    bool continue_first_loop = true;
    for(TopoDS_Iterator iterator_main_shape{main_shape}; continue_first_loop && iterator_main_shape.More();
        iterator_main_shape.Next())
    {
      const bool main_shape_is_a_solid = (TopAbs_SOLID == main_shape.ShapeType());
      if(main_shape_is_a_solid) {
        continue_first_loop = false;
      }
      auto solid = main_shape_is_a_solid ? main_shape : iterator_main_shape.Value();
      assert(TopAbs_SOLID == solid.ShapeType());
      for(TopoDS_Iterator iterator_solid{solid}; iterator_solid.More(); iterator_solid.Next()) {
        auto shell = iterator_solid.Value();
        assert(TopAbs_SHELL == shell.ShapeType());
        for(TopoDS_Iterator iterator(shell); iterator.More(); iterator.Next()) {
          auto face = iterator.Value();
          assert(TopAbs_FACE == face.ShapeType());
          auto brep_tface = Handle(BRep_TFace)::DownCast(face.TShape());
          assert(!brep_tface.IsNull());
          auto surface = brep_tface->Surface();
          auto occt_nurbs_surface = Handle(Geom_BSplineSurface)::DownCast(surface);
          assert(!occt_nurbs_surface.IsNull());
#if DEBUG_TRIM_LOOPS
          std::cerr << "Face #" << all_shapes.FindIndex(face) << '\n';
          std::cerr << "  surface #" << all_surfaces.Index(surface) << " degrees (" << occt_nurbs_surface->UDegree()
                    << ", " << occt_nurbs_surface->VDegree() << ")\n";
          const auto& array_of_poles = occt_nurbs_surface->Poles();
          for(auto i = array_of_poles.LowerRow(); i <= array_of_poles.UpperRow(); ++i) {
            std::cerr << "  ";
            for(auto j = array_of_poles.LowerCol(); j <= array_of_poles.UpperCol(); ++j) {
              const auto pnt = array_of_poles.Value(i, j).XYZ();
              std::cerr << " (" << pnt.X() << ", " << pnt.Y() << ", " << pnt.Z() << ")";
            }
            std::cerr << '\n';
          }
          std::stringstream filename;
          filename << "dump-3d_trim_loop_of_surface-" << all_surfaces.Index(surface) << ".polylines.txt";
          std::stringstream filename2d;
          filename2d << "dump-2d_trim_loop_of_surface-" << all_surfaces.Index(surface) << ".polylines.txt";
          std::ofstream dump_trimloop;
          std::ofstream dump_trimloop2d;
          if(!no_trim_loops) {
            dump_trimloop.open(filename.str());
            dump_trimloop2d.open(filename2d.str());
          }
          dump_trimloop.precision(17);
          dump_trimloop2d.precision(17);
#endif
          auto dtk_nurbs_surface_data = new dtkNurbsSurfaceDataOCCT;
          auto dtk_nurbs_surface = new dtkNurbsSurface{dtk_nurbs_surface_data};
          dtk_nurbs_surface->setSurfaceIndex(all_surfaces.Index(surface));
          dtk_nurbs_surface->setReversedOrientation(face.Orientation() == TopAbs_REVERSED);
          dtk_nurbs_surface->setUPeriodic(occt_nurbs_surface->IsUPeriodic());
          dtk_nurbs_surface->setVPeriodic(occt_nurbs_surface->IsVPeriodic());
          dtk_brep_data->m_nurbs_surfaces.push_back(dtk_nurbs_surface);
          std::cerr << "New NURBS surface #" << all_surfaces.Index(surface) << '\n';
          if(occt_nurbs_surface->IsUPeriodic()) {
            std::cout << "u period: " << occt_nurbs_surface->UPeriod() << std::endl;
          }
          if(occt_nurbs_surface->IsVPeriodic()) {
            std::cout << "v period: " << occt_nurbs_surface->VPeriod() << std::endl;
          }
          dtk_nurbs_surface_data->create(occt_nurbs_surface);
          TopoDS_Wire outer_wire = BRepTools::OuterWire(TopoDS::Face(face));
          for(TopoDS_Iterator wire_iterator(face); wire_iterator.More(); wire_iterator.Next()) {
            auto wire = wire_iterator.Value();
            assert(TopAbs_WIRE == wire.ShapeType());
            auto occt_orientation = wire.Orientation();
            assert(occt_orientation == TopAbs_FORWARD || occt_orientation == TopAbs_REVERSED);
            dtkAbstractTrimLoopData* trim_loop_data =
                dtkContinuousGeometry::abstractTrimLoopData::pluginFactory().create("dtkTrimLoopDataNp");
            auto trim_loop = new dtkTrimLoop{trim_loop_data};
            trim_loop->setTrimLoopIndex(all_shapes.FindIndex(wire));

            /// @TODO: fix the orientation of loops on dtk-continuous-geometry
            trim_loop->setType(TopoDS::Wire(wire) == outer_wire ? dtkContinuousGeometryEnums::TrimLoopType::inner
                                                                : dtkContinuousGeometryEnums::TrimLoopType::outer);
#if DEBUG_TRIM_LOOPS
            std::cerr << "Wire #" << all_shapes.FindIndex(wire) << " (" << wire.NbChildren() << " edges)";
            if(occt_orientation == TopAbs_REVERSED)
              std::cerr << " REVERSED";
            std::cerr << '\n';
#endif
            std::vector<dtkTrim*> trims;
            std::vector<Handle(Geom2d_BSplineCurve)> nurbs_curves_2d_occt;
            trims.reserve(wire.NbChildren());
            ShapeAnalysis_WireOrder wire_order(false /* 2d mode */, tolerance_in_parameters_space);
            for(TopoDS_Iterator edge_iterator(wire); edge_iterator.More(); edge_iterator.Next()) {
              auto edge = edge_iterator.Value();
              assert(TopAbs_EDGE == edge.ShapeType());
              const auto& edge_ds = TopoDS::Edge(edge);
              const auto& face_ds = TopoDS::Face(face);
              const bool is_seam = BRep_Tool::IsClosed(edge_ds, face_ds);
              const bool is_edge_reversed = (TopAbs_REVERSED == edge.Orientation());
              auto brep_tedge = Handle(BRep_TEdge)::DownCast(edge.TShape());
              assert(!brep_tedge.IsNull());
              auto brep_tedge_ptr = brep_tedge.get();

              Handle(Geom_Curve) curve_3d;
              Handle(Geom2d_Curve) curve_2d;

              auto curves = brep_tedge->Curves();
#if DEBUG_TRIM_LOOPS
              std::cerr << "Add " << (is_seam ? "a seam " : "an ") << "edge " << (is_edge_reversed ? "(reversed) " : "")
                        << "(shape " << (void*)&*edge.TShape() << ") with " << curves.Size()
                        << " curve representations\n";
#endif
              for(auto curve_rep : curves) {
#if DEBUG_TRIM_LOOPS
                std::string type = [&]() {
                  if(curve_rep->IsCurve3D()) {
                    return "Curve 3D";
                  } else if(curve_rep->IsCurveOnSurface()) {
                    return "PCurve";
                  } else if(curve_rep->IsRegularity()) {
                    return "Regularity";
                  } else if(curve_rep->IsPolygon3D()) {
                    return "Polygon 3D";
                  } else if(curve_rep->IsPolygonOnTriangulation()) {
                    return "PolygonOnTriangulation";
                  }
                  return "Unknown!";
                }();
                std::cerr << "  - " << type << '\n';
#endif
                if(curve_rep->IsCurve3D()) {
                  curve_3d = curve_rep->Curve3D();
                } else if(curve_rep->IsCurveOnSurface()) {
                  if(curve_rep->Surface() != surface)
                    continue;
                  if(curve_rep->IsCurveOnClosedSurface()) {
                    if(is_edge_reversed) {
                      curve_2d = curve_rep->PCurve2();
                    } else {
                      curve_2d = curve_rep->PCurve();
                    }
                    if(occt_orientation == TopAbs_REVERSED) {
                      curve_2d = curve_2d->Reversed();
                    }
                  } else {
                    curve_2d = curve_rep->PCurve();
                  }
                  if(is_edge_reversed)
                    curve_2d = curve_2d->Reversed();
                  auto nurbs_curve_2d = Handle(Geom2d_BSplineCurve)::DownCast(curve_2d);
                  assert(!nurbs_curve_2d.IsNull());
#if DEBUG_TRIM_LOOPS
                  auto display_2d_curve_with_poles = [&](auto curve_2d, std::string indent = "") {
                    auto nb_poles = curve_2d->NbPoles();
                    std::cerr << indent << "2d Bezier curve #" << all_2dcurves.Index(curve_2d) << ", degree "
                              << curve_2d->Degree() << ", periodic=" << curve_2d->IsPeriodic() << "\n";
                    std::cerr << indent << nb_poles << " poles [";
                    for(auto i = 1; i <= nb_poles; ++i /* i += (nb_poles - 1) */) {
                      auto p = curve_2d->Pole(i);
                      std::cerr << " (" << p.X() << ", " << p.Y() << ",  w=" << curve_2d->Weight(i) << ")";
                    }
                    std::cerr << " ]\n";
                  };
                  auto display_2d_nurb = [&](auto nurbs_curve_2d, std::string indent = "") {
                    display_2d_curve_with_poles(nurbs_curve_2d, indent);
                    auto nb_knots = nurbs_curve_2d->NbKnots();
                    std::cerr << indent << nb_knots << " knots [";
                    for(auto i = 1; i <= nb_knots; ++i) {
                      std::cerr << " (" << nurbs_curve_2d->Knot(i) << ", " << nurbs_curve_2d->Multiplicity(i) << ")";
                    }
                    std::cerr << " ]\n";
                  };
                  display_2d_nurb(nurbs_curve_2d, "    ");
#endif // DEBUG_TRIM_LOOPS
                  if(nurbs_curve_2d->IsPeriodic()) {
                    std::cout << "periodic p curve" << std::endl;
                    // Our code cannot deal with periodic curve.
                    // We transform the curve, so that is become a piecewise-Bezier NURBS.
#if DEBUG_TRIM_LOOPS
                    std::cerr << " --> convert the periodic curve to "
                                 "piecewise-Bezier:\n";
#endif // DEBUG_TRIM_LOOPS
                    Geom2dConvert_BSplineCurveToBezierCurve convert{nurbs_curve_2d};
                    Geom2dConvert_CompCurveToBSplineCurve concat{};
                    for(auto nb_arcs = convert.NbArcs(), i = 1; i <= nb_arcs; ++i) {
#if DEBUG_TRIM_LOOPS
                      std::cerr << "Bezier #" << i << '\n';
                      display_2d_curve_with_poles(convert.Arc(i), "    ");
#endif // DEBUG_TRIM_LOOPS
                      bool ok = concat.Add(convert.Arc(i), Precision::Confusion(), true);
                      assert(ok);
                      (void)ok;
                    }
                    nurbs_curve_2d = concat.BSplineCurve();
                    curve_2d = nurbs_curve_2d;
#if DEBUG_TRIM_LOOPS
                    std::cerr << " --> result of conversion to piecewise-Bezier:\n";
                    display_2d_nurb(nurbs_curve_2d, "    ");
#endif // DEBUG_TRIM_LOOPS
                  }
                }
              } // end loop on curve representations
              assert(!curve_2d.IsNull());
              if(!curve_3d.IsNull() && curve_3d->IsPeriodic()) {
                std::cout << "periodic 3d curve" << std::endl;
                // Our code cannot deal with periodic curve.
                // We transform the curve, so that is become a piecewise-Bezier NURBS.
                auto nurbs_curve_3d = Handle(Geom_BSplineCurve)::DownCast(curve_3d);
                assert(!nurbs_curve_3d.IsNull());
                GeomConvert_BSplineCurveToBezierCurve convert{nurbs_curve_3d};
                GeomConvert_CompCurveToBSplineCurve concat{};
                for(auto nb_arcs = convert.NbArcs(), i = 1; i <= nb_arcs; ++i) {
                  bool ok = concat.Add(convert.Arc(i), Precision::Confusion(), true);
                  assert(ok);
                  (void)ok;
                }
                nurbs_curve_3d = concat.BSplineCurve();
                curve_3d = nurbs_curve_3d;
              }
              wire_order.Add(curve_2d->Value(curve_2d->FirstParameter()).XY(),
                             curve_2d->Value(curve_2d->LastParameter()).XY());
#if DEBUG_TRIM_LOOPS
              if(!no_trim_loops) {
                constexpr int nb_of_samples = 10;
                dump_trimloop << nb_of_samples + 1;
                dump_trimloop2d << nb_of_samples + 1;
                // std::cout << "image of p-curve: " << std::endl;
                for(int i = 0; i <= nb_of_samples; ++i) {
                  const auto t0 = curve_2d->FirstParameter();
                  const auto t1 = curve_2d->LastParameter();
                  const auto delta = (t1 - t0) / nb_of_samples;
                  const auto t = t0 + i * delta;
                  const auto p_2d = curve_2d->Value(t);
                  const auto p_3d = occt_nurbs_surface->Value(p_2d.X(), p_2d.Y());
                  // std::cerr << p_3d.X() << " " << p_3d.Y() << " " << p_3d.Z() << std::endl;
                  dump_trimloop << "  " << p_3d.X() << " " << p_3d.Y() << " " << p_3d.Z();
                  dump_trimloop2d << "  " << p_2d.X() << " " << p_2d.Y() << " 0";
                }
                dump_trimloop << std::endl;
                dump_trimloop2d << std::endl;
              }
#endif

              dtkNurbsCurve2DDataOCCT* dtk_nurbs_curve_2d_data_occt = new dtkNurbsCurve2DDataOCCT;
              auto nurbs_curve_2d = Handle(Geom2d_BSplineCurve)::DownCast(curve_2d);
              assert(!nurbs_curve_2d.IsNull());
              dtk_nurbs_curve_2d_data_occt->create(nurbs_curve_2d);
              dtkNurbsCurve2D* dtk_nurbs_curve = new dtkNurbsCurve2D{dtk_nurbs_curve_2d_data_occt};
              dtk_brep_data->m_nurbs_curves_2d.push_back(dtk_nurbs_curve);
              dtkTopoTrim* dtk_topo_trim = [&]() {
                auto it = trims_map.find(brep_tedge_ptr);
                if(it == trims_map.end()) { // then create the 3d curve
                  dtkTopoTrim* dtk_topo_trim = new dtkTopoTrim;
                  dtk_topo_trim->setTrimIndex(all_shapes.FindIndex(edge));
                  auto nurbs_curve_3d = Handle(Geom_BSplineCurve)::DownCast(curve_3d);
                  if(!nurbs_curve_3d.IsNull()) {
                    double parameter_first = curve_3d->FirstParameter();
                    double parameter_last = curve_3d->LastParameter();
                    bool is_curve_3d_matched = checkEdgeCurveMatch(edge_ds, face_ds, curve_3d, curve_2d, surface,
                                                                   parameter_first, parameter_last, is_edge_reversed);
                    if(!is_curve_3d_matched) {
                      ShapeAnalysis_Edge edge_analysis;
                      bool is_edge_closed = edge_analysis.IsClosed3d(edge_ds);
                      if(!is_edge_closed)
                      { // a non-closed edge may also correspond to a periodic curve_3d in openCascade
                        std::cout << "Warning: split 3d curve" << std::endl;
                        nurbs_curve_3d =
                            GeomConvert::SplitBSplineCurve(nurbs_curve_3d, parameter_first, parameter_last, 1.e-6);
                      } else {
                        std::cout << "Warning: reconstruct 3d periodic curve" << std::endl;
                        double parameter_split = parameter_first;
                        if (is_edge_reversed) parameter_split = parameter_last;
                        auto curve1 = GeomConvert::SplitBSplineCurve(nurbs_curve_3d, parameter_split,
                                                                     curve_3d->LastParameter(), 1.e-6);
                        auto curve2 = GeomConvert::SplitBSplineCurve(nurbs_curve_3d, curve_3d->FirstParameter(),
                                                                     parameter_split, 1.e-6);

                        GeomConvert_CompCurveToBSplineCurve concat{};
                        concat.Add(curve1, Precision::Confusion());
                        concat.Add(curve2, Precision::Confusion(), true);
                        nurbs_curve_3d = concat.BSplineCurve();
                      }
                    }
                    dtkNurbsCurveDataOCCT* dtk_nurbs_curve_data_occt = new dtkNurbsCurveDataOCCT;
                    dtk_nurbs_curve_data_occt->create(nurbs_curve_3d);
                    dtk_topo_trim->m_nurbs_curve_3d = new dtkNurbsCurve{dtk_nurbs_curve_data_occt};
                    // decompose into Bezier curves
                    dtk_topo_trim->decomposeToRationalBezierCurves();
                  } else {
                    dtk_topo_trim->m_nurbs_curve_3d = nullptr;
                  }
                  /*
                                          std::cout << "3d nurbs curve: " << std::endl;
                                          double delta = (nurbs_curve_3d->LastParameter() -
                     nurbs_curve_3d->FirstParameter()) / 10.; for (double ii = nurbs_curve_3d->FirstParameter(); ii <
                     nurbs_curve_3d->LastParameter() + delta / 10.; ii += delta) { auto p_3d =
                     nurbs_curve_3d->Value(ii); std::cerr << p_3d.X() << " " << p_3d.Y() << " " << p_3d.Z() <<
                     std::endl;
                                          }
                  */
                  dtk_brep_data->m_topo_trims.push_back(dtk_topo_trim);
                  trims_map.emplace(brep_tedge_ptr, dtk_topo_trim);
                  return dtk_topo_trim;
                } else {
                  return it->second;
                }
              }();
              auto dtk_trim = new dtkTrim(*dtk_nurbs_surface, *dtk_nurbs_curve, dtk_topo_trim, is_seam);
              trims.push_back(dtk_trim);
              nurbs_curves_2d_occt.push_back(nurbs_curve_2d);
              // std::cerr << std::endl;
            } // end loop on edges
            wire_order.Perform();
            assert(wire_order.IsDone());
#if DEBUG_TRIM_LOOPS
            double gap = wire_order.Gap(trims.size());
            for(std::size_t i = 1, n = trims.size(); i < n; ++i) {
              gap = (std::max)(gap, wire_order.Gap(i));
            }
            std::cerr << "maximal gap: " << gap << '\n';
#endif
            assert(wire_order.Status() == 3 || std::abs(wire_order.Status()) < 2);

            checkPCurvesMatch(wire_order, nurbs_curves_2d_occt, trims);

#if DEBUG_TRIM_LOOPS
            std::cerr << "New order of edges:";
#endif
            for(std::size_t i = 0, n = trims.size(); i < n; ++i) {
              auto new_i = wire_order.Ordered(i + 1);
#if DEBUG_TRIM_LOOPS
              std::cerr << " " << new_i;
#endif
              if(new_i < 0) {
                new_i = -new_i;
              }
              assert(new_i > 0);
              assert(static_cast<unsigned>(new_i) <= n);
              trim_loop->trims().push_back(trims[new_i - 1]);
            }
#if DEBUG_TRIM_LOOPS
            std::cerr << '\n';
#endif

            if(!no_trim_loops) {
              dtk_nurbs_surface->trimLoops().push_back(trim_loop);
            } else {
              delete trim_loop;
            }
          } // end loop on wires
        }   // end loop on faces
      }     // end loop on shells
    }       // end loop on solids
}

dtkBRep* OpenCASCADEBRepReader::outputDtkBRep(void) const
{
    return d->dtk_brep;
}

bool OpenCASCADEBRepReader::checkEdgeCurveMatch(const TopoDS_Edge& edge_ds,
                                                const TopoDS_Face& face_ds,
                                                const Handle(Geom_Curve)& curve_3d,
                                                const Handle(Geom2d_Curve)& /*curve_2d*/,
                                                const Handle(Geom_Surface)& /*surface*/,
                                                double& parameter_first,
                                                double& parameter_last,
                                                bool reversed)
{
    bool is_edge_curve_matched = true;

    // first and last vertices of the edge
    ShapeAnalysis_Edge edge_analysis;
    auto vertex_first_ds = edge_analysis.FirstVertex(edge_ds);
    auto vertex_last_ds = edge_analysis.LastVertex(edge_ds);
    auto brep_tvertex_first = Handle(BRep_TVertex)::DownCast(vertex_first_ds.TShape());
    auto brep_tvertex_last = Handle(BRep_TVertex)::DownCast(vertex_last_ds.TShape());

    auto vertex_first = brep_tvertex_first->Pnt();
    auto vertex_last = brep_tvertex_last->Pnt();

    std::cerr << "first vertex: " << vertex_first.X() << " " << vertex_first.Y() << " " << vertex_first.Z() << std::endl;
    std::cerr << "last vertex: " << vertex_last.X() << " " << vertex_last.Y() << " " << vertex_last.Z() << std::endl;

    double tol1 = 0., tol2 = 0.;
    edge_analysis.CheckVertexTolerance(edge_ds, face_ds, tol1, tol2);

    if (reversed) std::cout << "Reversed" << std::endl;

    // compare end vertices of the edge and the 3d curve
    auto p_3d_0 = curve_3d->Value(curve_3d->FirstParameter());
    auto p_3d_n = curve_3d->Value(curve_3d->LastParameter());

    double diff_0 = 0., diff_n = 0.;
    bool is_first_matched = true, is_last_matched = true;
    if (!reversed) {
        diff_0 = vertex_first.Distance(p_3d_0);
        diff_n = vertex_last.Distance(p_3d_n);
        if (diff_0 > tol1) is_first_matched = false;
        if (diff_n > tol2) is_last_matched = false;
    }
    else {
        diff_0 = vertex_first.Distance(p_3d_n);
        diff_n = vertex_last.Distance(p_3d_0);
        if (diff_0 > tol1) is_first_matched = false;
        if (diff_n > tol2) is_last_matched = false;
    }

    if (!is_first_matched || !is_last_matched) is_edge_curve_matched = false;

    // split and reconstruct 3d curve if it does not match the edge
    if (!is_edge_curve_matched) {
      bool is_edge_closed = edge_analysis.IsClosed3d(edge_ds);
      if (!is_edge_closed) { // the edge is not closed
        if (!is_first_matched) {
          std::cout << "Warning: edge and 3d curve do NOT match!" << std::endl;
          GeomAPI_ProjectPointOnCurve projector(vertex_first, curve_3d);
          if (projector.NbPoints() == 0) std::cout << "Error: No 3d point projection." << std::endl;
          for (int ii = 1; ii <= projector.NbPoints(); ++ii) {
            auto pt = projector.Point(ii);
            double diff = vertex_first.Distance(pt);
            if (diff < diff_0) {
              if (!reversed) parameter_first = projector.Parameter(ii);
              else parameter_last = projector.Parameter(ii);
              diff_0 = diff;
            }
          }
        }

        if (!is_last_matched) {
          std::cout << "Warning: edge and 3d curve do NOT match!" << std::endl;
          GeomAPI_ProjectPointOnCurve projector(vertex_last, curve_3d);
          if (projector.NbPoints() == 0) std::cout << "Error: No 3d point projection." << std::endl;
          for (int ii = 1; ii <= projector.NbPoints(); ++ii) {
            auto pt = projector.Point(ii);
            double diff = vertex_last.Distance(pt);
            if (diff < diff_n) {
              if (!reversed) parameter_last = projector.Parameter(ii);
              else parameter_first = projector.Parameter(ii);
              diff_n = diff;
            }
          }
        }
      }
      else { // the edge is closed, hence only the first vertex is considered for reconstruction
        std::cout << "Warning: edge and 3d periodic curve do NOT match!" << std::endl;
        GeomAPI_ProjectPointOnCurve projector_first(vertex_first, curve_3d);
        if (projector_first.NbPoints() == 0) std::cout << "Error: No 3d point projection." << std::endl;
        for (int ii = 1; ii <= projector_first.NbPoints(); ++ii) {
          auto pt = projector_first.Point(ii);
          double diff = vertex_first.Distance(pt);
          if (diff < diff_0) {
            if (!reversed) parameter_first = projector_first.Parameter(ii);
            else parameter_last = projector_first.Parameter(ii);
            diff_0 = diff;
          }
        }
      }

    } // if the edge and curve_3d do not match

    return is_edge_curve_matched;
}

bool OpenCASCADEBRepReader::checkPCurvesMatch(const ShapeAnalysis_WireOrder& wire_order,
                                              std::vector<Handle(Geom2d_BSplineCurve)> nurbs_curves_2d_occt,
                                              std::vector<dtkTrim*>& trims,
                                              double tol)
{
  bool is_pcurves_matched = true;

  // estimate the precision
  double length_min = 0.;
  for (std::size_t i = 0; i < trims.size(); ++i) {
    const dtkTrim* trim = trims[i];
    const dtkNurbsCurve2D& curve_2d = trim->curve2D();
    double length = 0.;
    for (std::size_t ii = 1; ii < curve_2d.nbCps(); ++ii) {
      double cp1[2], cp2[2];
      curve_2d.controlPoint(ii - 1, cp1);
      curve_2d.controlPoint(ii, cp2);
      length += std::sqrt((cp2[0] - cp1[0]) * (cp2[0] - cp1[0]) + (cp2[1] - cp1[1]) * (cp2[1] - cp1[1]));
    }
    if (i == 0) length_min = length;
    else length_min = (length < length_min) ? length : length_min;
  }

  if (length_min*1.e-4 < tol) tol = length_min*1.e-4;

  //------------------------------------------------------------------------------------------------
  for (std::size_t i = 0; i < trims.size(); ++i) {
    const auto new_i = std::abs(wire_order.Ordered(i + 1));
    assert(new_i > 0);
    assert(static_cast<unsigned>(new_i) <= trims.size());

    // check if the 2d nurbs curve mathces at the two ends
    if (i > 0) {
      const dtkTrim *trim_pre = trims[std::abs(wire_order.Ordered(i)) - 1];
      const dtkTrim *trim_cur = trims[new_i - 1];

      const dtkNurbsCurve2D &curve_2d_pre = trim_pre->curve2D();
      const dtkNurbsCurve2D &curve_2d_cur = trim_cur->curve2D();

      double cp_end_pre[2], cp_begin_cur[2];
      curve_2d_pre.controlPoint(curve_2d_pre.nbCps() - 1, cp_end_pre);
      curve_2d_cur.controlPoint(0, cp_begin_cur);

      bool is_matched_begin = true, is_matched_end = true;

      double diff1 = std::sqrt((cp_begin_cur[0] - cp_end_pre[0]) * (cp_begin_cur[0] - cp_end_pre[0]) +
                              (cp_begin_cur[1] - cp_end_pre[1]) * (cp_begin_cur[1] - cp_end_pre[1]));

      double dir1[2];
      dir1[0] = (cp_end_pre[0] - cp_begin_cur[0])/diff1;
      dir1[1] = (cp_end_pre[1] - cp_begin_cur[1])/diff1;

      if (diff1 > tol) {
        std::cout << "End not match!" << std::endl;
        std::cout << "pre end:   " << cp_end_pre[0] << " " << cp_end_pre[1] << std::endl;
        std::cout << "cur begin: " << cp_begin_cur[0] << " " << cp_begin_cur[1] << std::endl;

        is_matched_begin = false;

        // modify the 2d nurbs curve
        double length = 0.;
        std::vector<double> sub_lengths;
        sub_lengths.reserve(curve_2d_cur.nbCps() - 1);
        for (std::size_t ii = 1; ii < curve_2d_cur.nbCps(); ++ii) {
          double cp1[2], cp2[2];
          curve_2d_cur.controlPoint(ii - 1, cp1);
          curve_2d_cur.controlPoint(ii, cp2);
          length += std::sqrt((cp2[0] - cp1[0])*(cp2[0] - cp1[0]) + (cp2[1] - cp1[1])*(cp2[1] - cp1[1]));
          sub_lengths.push_back(length);
        }

        // first control point
        gp_Pnt2d cp_new(cp_end_pre[0], cp_end_pre[1]);
        nurbs_curves_2d_occt[new_i - 1]->SetPole(1, cp_new);

        // other control points except the last one
        if (i < trims.size() - 1) {
          for (std::size_t ii = 1; ii < curve_2d_cur.nbCps() - 1; ++ii) {
            double cp_i[2];
            curve_2d_cur.controlPoint(ii, cp_i);

            double coeff = 1. - sub_lengths[ii - 1] / length;
            gp_Pnt2d cp_new_i(cp_i[0] + coeff * diff1 * dir1[0], cp_i[1] + coeff * diff1 * dir1[1]);
            nurbs_curves_2d_occt[new_i - 1]->SetPole(ii + 1, cp_new_i);
          }
        }

      }

      if (i == trims.size() - 1) { // the last trim
        double cp_end_last[2], cp_begin_first[2];
        curve_2d_cur.controlPoint(curve_2d_cur.nbCps() - 1, cp_end_last);

        const dtkTrim *trim_first = trims[std::abs(wire_order.Ordered(1)) - 1];
        const dtkNurbsCurve2D &curve_2d_first = trim_first->curve2D();
        curve_2d_first.controlPoint(0, cp_begin_first);

        double diff2 = std::sqrt((cp_end_last[0] - cp_begin_first[0]) * (cp_end_last[0] - cp_begin_first[0]) +
                         (cp_end_last[1] - cp_begin_first[1]) * (cp_end_last[1] - cp_begin_first[1]));

        if (diff2 > tol) {
          std::cout << "End not match!" << std::endl;
          std::cout << "cur end:     " << cp_end_last[0] << " " << cp_end_last[1] << std::endl;
          std::cout << "first begin: " << cp_begin_first[0] << " " << cp_begin_first[1] << std::endl;

          is_matched_end = false;

          double dir2[2];
          dir2[0] = (cp_begin_first[0] - cp_end_last[0])/diff2;
          dir2[1] = (cp_begin_first[1] - cp_end_last[1])/diff2;

          // modify the 2d nurbs curve
          double length = 0.;
          std::vector<double> sub_lengths;
          sub_lengths.reserve(curve_2d_cur.nbCps() - 1);
          for (std::size_t ii = 1; ii < curve_2d_cur.nbCps(); ++ii) {
            double cp1[2], cp2[2];
            curve_2d_cur.controlPoint(ii - 1, cp1);
            curve_2d_cur.controlPoint(ii, cp2);
            length += std::sqrt((cp2[0] - cp1[0]) * (cp2[0] - cp1[0]) + (cp2[1] - cp1[1]) * (cp2[1] - cp1[1]));
            sub_lengths.push_back(length);
          }

          // last control point
          gp_Pnt2d cp_new(cp_begin_first[0], cp_begin_first[1]);
          nurbs_curves_2d_occt[new_i - 1]->SetPole(curve_2d_cur.nbCps(), cp_new);

          // other control points except the first one
          for (std::size_t ii = 1; ii < curve_2d_cur.nbCps() - 1; ++ii) {
            double cp_i[2];
            curve_2d_cur.controlPoint(ii, cp_i);

            double coeff = sub_lengths[ii - 1]/length;
            gp_Pnt2d cp_new_i(cp_i[0] + coeff*diff2*dir2[0], cp_i[1] + coeff*diff2*dir2[1]);
            if (!is_matched_begin) {
              cp_new_i.SetX(cp_i[0] + coeff*diff2*dir2[0] + (1. - coeff)*diff1*dir1[0]);
              cp_new_i.SetY(cp_i[1] + coeff*diff2*dir2[1] + (1. - coeff)*diff1*dir1[1]);
            }
            nurbs_curves_2d_occt[new_i - 1]->SetPole(ii + 1, cp_new_i);
          }

        }
      }

      if (!is_matched_begin || !is_matched_end) { // replace trim
        const dtkNurbsSurface &nurbs_surface_new = trims[new_i - 1]->nurbsSurface();
        const dtkTopoTrim *topo_trim_new = trims[new_i - 1]->topoTrim();
        dtkNurbsCurve2DDataOCCT *dtk_nurbs_curve_2d_data_occt = new dtkNurbsCurve2DDataOCCT;
        dtk_nurbs_curve_2d_data_occt->create(nurbs_curves_2d_occt[new_i - 1]);
        dtkNurbsCurve2D *dtk_nurbs_curve_new = new dtkNurbsCurve2D{dtk_nurbs_curve_2d_data_occt};
        auto dtk_trim_new = new dtkTrim(nurbs_surface_new, *dtk_nurbs_curve_new, topo_trim_new, trims[new_i - 1]->isSeam());
        trims[new_i - 1] = dtk_trim_new;
      }
/*
      if (!is_matched_begin || !is_matched_end) {
        std::cout << "control points of the non-matching trim: " << std::endl;
        for (std::size_t ii = 0; ii < curve_2d_cur.nbCps(); ++ii) {
          double cp[2];
          curve_2d_cur.controlPoint(ii, cp);
          std::cout << cp[0] << "," << cp[1] << "," << 0 << std::endl;
        }
        std::cout << "control points of modified trim: " << std::endl;
        for (int ii = 1; ii <= nurbs_curves_2d_occt[new_i - 1]->NbPoles(); ++ii) {
          gp_Pnt2d cp = nurbs_curves_2d_occt[new_i - 1]->Pole(ii);
          std::cout << cp.X() << "," << cp.Y() << "," << 0 << std::endl;
        }
      }
      */
    }
  }

  return is_pcurves_matched;
}
//
// OpenCASCADEBRepReader.cpp ends here

// Local Variables:
// tab-width: 4
// c-basic-offset: 4
// End: