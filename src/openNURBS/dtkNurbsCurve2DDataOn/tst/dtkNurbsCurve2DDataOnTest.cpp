// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkNurbsCurve2DDataOnTest.h"

#include <dtkContinuousGeometryUtils>

#include "dtkNurbsCurve2DDataOn.h"

#include <dtkTest>
#include <fstream>

// ///////////////////////////////////////////////////////////////////
// D-Pointer implementation
// ///////////////////////////////////////////////////////////////////

class dtkNurbsCurve2DDataOnTestCasePrivate{
public:
    dtkNurbsCurve2DDataOn* nurbs_curve_data;
};
// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkNurbsCurve2DDataOnTestCase::dtkNurbsCurve2DDataOnTestCase(void):d(new dtkNurbsCurve2DDataOnTestCasePrivate())
{

}

dtkNurbsCurve2DDataOnTestCase::~dtkNurbsCurve2DDataOnTestCase(void)
{
}

void dtkNurbsCurve2DDataOnTestCase::initTestCase(void)
{
    d->nurbs_curve_data = new dtkNurbsCurve2DDataOn();
    //degree 1
    std::size_t dim = 2;
    std::size_t nb_cp = 4;
    std::size_t order = 3;

    std::vector< double > knots(nb_cp + order - 2);
    /* 0. */ knots[0] = 0.; knots[1] = 0.; knots[2] = 1.3; knots[3] = 2.; knots[4] = 2.; /* 2. */

    std::vector< double > cps((dim + 1) * nb_cp);
    cps[0] = 0.;   cps[1] = 0.; cps[2] = 3.;
    cps[3] = 1.;   cps[4] = 0.; cps[5] = 0.5;
    cps[6] = 2.;   cps[7] = 0.; cps[8] = 1.;
    cps[9] = 3.;  cps[10] = 0.;  cps[11] = 1.5;

    d->nurbs_curve_data->create(nb_cp, order, knots.data(), cps.data());
}

void dtkNurbsCurve2DDataOnTestCase::init(void)
{

}

void dtkNurbsCurve2DDataOnTestCase::testControlPoint(void)
{
    dtkContinuousGeometryPrimitives::Point_2 cp(0., 0.);
    d->nurbs_curve_data->controlPoint(1, cp.data());
    QVERIFY(cp[0] == 1.);
    QVERIFY(cp[1] == 0.);
}

void dtkNurbsCurve2DDataOnTestCase::testWeight(void)
{
    double w = 0.;
    d->nurbs_curve_data->weight(1, &w);
    QVERIFY(w == 0.5);
}

void dtkNurbsCurve2DDataOnTestCase::testDegree(void)
{
    QVERIFY(d->nurbs_curve_data->degree() == 2);
}

void dtkNurbsCurve2DDataOnTestCase::testKnots(void)
{
    std::vector< double> knots(d->nurbs_curve_data->nbCps() + d->nurbs_curve_data->degree() - 1);
    QVERIFY(d->nurbs_curve_data->nbCps() + d->nurbs_curve_data->degree() - 1 == 5);
    d->nurbs_curve_data->knots(knots.data());
    QVERIFY(knots[0] == 0.);
    QVERIFY(knots[1] == 0.);
    QVERIFY(knots[2] == 1.3);
    QVERIFY(knots[3] == 2.);
    QVERIFY(knots[4] == 2.);

    QVERIFY(d->nurbs_curve_data->knots()[0] == 0.);
    QVERIFY(d->nurbs_curve_data->knots()[1] == 0.);
    QVERIFY(d->nurbs_curve_data->knots()[2] == 1.3);
    QVERIFY(d->nurbs_curve_data->knots()[3] == 2.);
    QVERIFY(d->nurbs_curve_data->knots()[4] == 2.);
}

void dtkNurbsCurve2DDataOnTestCase::testCurvePoint(void)
{
    dtkContinuousGeometryPrimitives::Point_2 point(0., 0.);
    d->nurbs_curve_data->evaluatePoint(1.25, point.data());
    // QVERIFY(point[0] > 0.405975 - 10e-5 && point[0] < 0.405975 + 10e-5);
    // QVERIFY(point[1] > 0.25 - 10e-5 && point[1] < 0.25 + 10e-5);
    // QVERIFY(point[2] > 0.765378 - 10e-5 && point[2] < 0.765378 + 10e-5);
}

//To ameliorate with dataCurvePoint and several pints to test
void dtkNurbsCurve2DDataOnTestCase::testCurveNormal(void)
{
    dtkContinuousGeometryPrimitives::Vector_2 normal(0., 0.);
    d->nurbs_curve_data->evaluateNormal(1.5, normal.data());
}

void  dtkNurbsCurve2DDataOnTestCase::testPrintOutCurve(void)
{
    dtkContinuousGeometryPrimitives::Point_2 cp(0., 0.);
    std::ofstream nurbs_curve_data_on_cp_file("nurbs_curve_data_on_cp.xyz");
    for (std::size_t i = 0; i <= d->nurbs_curve_data->degree(); ++i) {
        d->nurbs_curve_data->controlPoint(i, cp.data());
        nurbs_curve_data_on_cp_file << cp[0] << " " << cp[1] << " 0." << std::endl;
    }
    nurbs_curve_data_on_cp_file.close();

    std::ofstream nurbs_curve_data_on_file("nurbs_curve_data_on.xyz");
    dtkContinuousGeometryPrimitives::Point_2 p(0., 0.);
    for (double i = 0.; i <= 2.; i += 0.01) {d->nurbs_curve_data->evaluatePoint(i, p.data());
        nurbs_curve_data_on_file << p[0] << " " << p[1] << " 0." << std::endl;
    }
    nurbs_curve_data_on_file.close();
}

void dtkNurbsCurve2DDataOnTestCase::cleanup(void)
{

}

void dtkNurbsCurve2DDataOnTestCase::cleanupTestCase(void)
{
    delete d->nurbs_curve_data;
    d->nurbs_curve_data = nullptr;
}

DTKTEST_MAIN_NOGUI(dtkNurbsCurve2DDataOnTest, dtkNurbsCurve2DDataOnTestCase)

//
// dtkNurbsCurve2DDataOnTest.cpp ends here
