// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <QTest>

class dtkNurbsCurve2DDataOnTestCasePrivate;

class dtkNurbsCurve2DDataOnTestCase : public QObject
{
    Q_OBJECT

public:
    dtkNurbsCurve2DDataOnTestCase(void);
    ~dtkNurbsCurve2DDataOnTestCase(void);

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testControlPoint(void);
    void testWeight(void);
    void testDegree(void);
    void testKnots(void);
    void testCurvePoint(void);
    void testCurveNormal(void);
    void testPrintOutCurve(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);

private:
    dtkNurbsCurve2DDataOnTestCasePrivate* d;
};

//
// dtkNurbsCurve2DDataOnTest.h ends here
