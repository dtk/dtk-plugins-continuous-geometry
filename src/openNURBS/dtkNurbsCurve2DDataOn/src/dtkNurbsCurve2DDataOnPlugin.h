// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <QtCore>

#include <dtkContinuousGeometry>

#include <dtkAbstractNurbsCurve2DData>

class dtkNurbsCurve2DDataOnPlugin : public dtkAbstractNurbsCurve2DDataPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkAbstractNurbsCurve2DDataPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkNurbsCurve2DDataOnPlugin" FILE "dtkNurbsCurve2DDataOnPlugin.json")

public:
     dtkNurbsCurve2DDataOnPlugin(void) {}
    ~dtkNurbsCurve2DDataOnPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

//
// dtkNurbsCurve2DDataOnPlugin.h ends here
