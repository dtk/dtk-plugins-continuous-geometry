// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#include "dtkNurbsCurve2DDataOn.h"
#include "dtkNurbsCurve2DDataOnPlugin.h"

#include <dtkCore>
#include <dtkContinuousGeometry>

// ///////////////////////////////////////////////////////////////////
// dtkNurbsCurve2DDataOnPlugin
// ///////////////////////////////////////////////////////////////////

void dtkNurbsCurve2DDataOnPlugin::initialize(void)
{
    dtkContinuousGeometry::abstractNurbsCurve2DData::pluginFactory().record("dtkNurbsCurve2DDataOn", dtkNurbsCurve2DDataOnCreator);
}

void dtkNurbsCurve2DDataOnPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkNurbsCurve2DDataOn)

//
// dtkNurbsCurve2DDataOnPlugin.cpp ends here
