// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkNurbsCurve2DDataOn.h"

#include <dtkRationalBezierCurve2DDataOn.h>

#include "opennurbs_nurbscurve.h"

#include <cassert>

/*!
  \class dtkNurbsCurve2DDataOn
  \inmodule dtkPluginsContinuousGeometry
  \brief dtkNurbsCurve2DDataOn is an openNURBS implementation of the concept dtkAbstractNurbsCurve2DData.

  The following lines of code show how to instanciate and use the class in pratice:
  \code
  dtkAbstractNurbsCurve2DData *nurbs_curve_data = dtkContinuousGeometry::abstractNurbsCurve2DData::pluginFactory().create("dtkNurbsCurve2DDataOn");
  dtkNurbsCurve2D *nurbs_curve = new dtkNurbsCurve2D(nurbs_curve_data);
  nurbs_curve->create(...);
  \endcode
*/

/*! \fn dtkNurbsCurve2DDataOn::dtkNurbsCurve2DDataOn(void)
  Instanciates the structure.

  To add the content, see \l create(std::size_t nb_cp, std::size_t order, double *knots, double *cps) const.
*/
dtkNurbsCurve2DDataOn::dtkNurbsCurve2DDataOn(void) : d(nullptr) {}

/*! \fn dtkNurbsCurve2DDataOn::~dtkNurbsCurve2DDataOn(void)
  Destroys the instance.
*/
dtkNurbsCurve2DDataOn::~dtkNurbsCurve2DDataOn(void)
{
    delete d;
}

/*! \fn dtkNurbsCurve2DDataOn::create(const ON_NurbsCurve& nurbs_curve)
  Not available in the dtkAbstractNurbsCurve2DData.

  Creates the 2D NURBS curve by providing a NURBS curve from the openNURBS API.

  \a nurbs_curve : the NURBS curve to copy.
*/
void dtkNurbsCurve2DDataOn::create(const ON_NurbsCurve& nurbs_curve)
{
    d = ON_NurbsCurve::New(nurbs_curve);
    assert(d->IsValid());
}

/*! \fn dtkNurbsCurve2DDataOn::create(std::size_t nb_cp, std::size_t order, double *knots, double *cps) const
  Creates the 2D NURBS curve by providing the required content.

  \a nb_cp : number of control points

  \a order : order

  \a knots : array containing the knots

  \a cps : array containing the weighted control points, specified as : [x0, y0, w0, ...]
*/
void dtkNurbsCurve2DDataOn::create(std::size_t nb_cp, std::size_t order, double *knots, double *cps) const
{
    // /////////////////////////////////////////////////////////////////
    // Initializes the Open Nurbs NURBS curve
    // dimension = 2
    // rational = true
    // /////////////////////////////////////////////////////////////////
    d = ON_NurbsCurve::New(2, true, order, nb_cp);

    // ///////////////////////////////////////////////////////////////////
    // In its call to New, openNURBS reserves enough space at d->m_knot, d->m_cv to store the values of knots and control points
    // ///////////////////////////////////////////////////////////////////
    for(std::size_t i = 0; i < order + nb_cp - 2; ++i) {
        d->m_knot[i] = knots[i];
    }

    for(std::size_t i = 0; i < nb_cp; ++i) {
        d->m_cv[3 * i]     = cps[3 * i]     * cps[3 * i + 2];
        d->m_cv[3 * i + 1] = cps[3 * i + 1] * cps[3 * i + 2];
        d->m_cv[3 * i + 2] = cps[3 * i + 2];
    }

    if(!d->IsValid()) {
        dtkFatal() << "The dtkNurbsCurve2DDataOn is not valid";
    }
}

/*! \fn  dtkNurbsCurve2DDataOn::degree(void) const
  Returns the degree of the curve.
*/
std::size_t dtkNurbsCurve2DDataOn::degree(void) const
{
    return d->Degree();
}

/*! \fn  dtkNurbsCurve2DDataOn::nbCps(void) const
  Returns the number of weighted control points controlling the curve.
*/
std::size_t dtkNurbsCurve2DDataOn::nbCps(void) const
{
    return d->m_cv_count;
}

/*! \fn  dtkNurbsCurve2DDataOn::controlPoint(std::size_t i, double *r_cp) const
  Writes in \a r_cp the \a i th weighted control point coordinates[xi, yi].

  \a i : index of the weighted control point

  \a r_cp : array of size 2 to store the weighted control point coordinates[xi, yi]
*/
void dtkNurbsCurve2DDataOn::controlPoint(std::size_t i, double* r_cp) const
{
    ON_3dPoint point;
    d->GetCV(i, point);
    for (int i = 0; i < d->m_dim; ++i) {
        r_cp[i] = point[i];
    }
}

/*! \fn  dtkNurbsCurve2DDataOn::weight(std::size_t i, double *r_w) const
  Writes in \a r_w the \a i th weighted control point weight wi.

  \a i : index of the weighted control point

  \a r_w : array of size 1 to store the weighted control point weight wi
*/
void dtkNurbsCurve2DDataOn::weight(std::size_t i, double* r_w) const
{
    *r_w = d->Weight(i);
}

/*! \fn dtkNurbsCurve2DDataOn::knots(double *r_knots) const
  Writes in \a r_knots the knots of the curve.

  \a r_knots : array of size (nb_cp + order - 2) to store the knots
*/
void dtkNurbsCurve2DDataOn::knots(double* r_knots) const
{
    std::size_t nb_of_knots = d->m_cv_count + d->Degree() - 1;
    for (std::size_t i = 0; i < nb_of_knots; ++i) {
        r_knots[i] = d->m_knot[i];
    }
}

/*! \fn dtkNurbsCurve2DDataOn::knots(void) const
  Returns a pointer to the array storing the knots of the curve. The array is of size : (nb_cp + order - 2).
*/
const double *dtkNurbsCurve2DDataOn::knots(void) const
{
    return d->m_knot;
}

/*! \fn dtkNurbsCurve2DDataOn::evaluatePoint(double p_u, double *r_point) const
  Writes in \a r_point the evaluation at parameter \a p_u.

  \a p_u : parameter

  \a r_point : array of size 2 to store the result of the evaluation
*/
void dtkNurbsCurve2DDataOn::evaluatePoint(double p_u, double* r_point) const
{
    ON_3dPoint on_point;
    if (!d->EvPoint(p_u, on_point)) {
        dtkFatal() << "Unable to evaluate the curve at " << p_u << ".";
    }
    for (int i = 0; i < d->m_dim; ++i) {
        r_point[i] = on_point[i];
    }
}

/*! \fn dtkNurbsCurve2DDataOn::evaluateNormal(double p_u, double *r_normal) const
  Writes in \a r_normal the normal evaluation at parameter \a p_u.

  \a p_u : parameter

  \a r_normal : array of size 2 to store the result of the normal evaluation
*/
void dtkNurbsCurve2DDataOn::evaluateNormal(double p_u, double* r_normal) const
{
    ON_3dPoint on_point;
    ON_3dVector on_normal;
    d->Ev1Der(p_u, on_point, on_normal);
    for (int i = 0; i < d->m_dim; ++i) {
        r_normal[i] = on_normal[i];
    }
}

/*! \fn dtkNurbsCurve2DDataOn::evaluateNormal(double p_u, double *r_point, double *r_normal) const
  Writes in \a r_normal the normal evaluation at parameter \a p_u.

  \a p_u : parameter

  \a r_point : array of size 2 to store the result of the evaluation

  \a r_normal : array of size 2 to store the result of the normal evaluation
*/
void dtkNurbsCurve2DDataOn::evaluateNormal(double p_u, double* r_point, double* r_normal) const
{
    ON_3dPoint on_point;
    ON_3dVector on_normal;
    d->Ev1Der(p_u, on_point, on_normal);
    for (int i = 0; i < d->m_dim; ++i) {
        r_normal[i] = on_normal[i];
        r_point[i] = on_point[i];
    }
}

/*! \fn dtkNurbsCurve2DDataOn::decomposeToRationalBezierCurve2Ds(std::vector< std::pair< dtkRationalBezierCurve2D *, double * >>& r_rational_bezier_curves) const
  Decomposes the NURBS curve into dtkRationalBezierCurve2D \a r_rational_bezier_curves.

  \a r_rational_bezier_curves : vector in which the dtkRationalBezierCurve2D are stored, along with their limits in the NURBS curve parameter space
*/
void dtkNurbsCurve2DDataOn::decomposeToRationalBezierCurve2Ds(std::vector< std::pair< dtkRationalBezierCurve2D *, double * >>& r_rational_bezier_curves) const
{
    for (std::size_t i = 0; i <= std::size_t(d->m_cv_count - d->m_order); ++i) {
        dtkRationalBezierCurve2DDataOn* on_bezier = new dtkRationalBezierCurve2DDataOn();
        on_bezier->create();
        if (d->ConvertSpanToBezier(i, on_bezier->onRationalBezierCurve2D())) {
            double* knots = new double[2];
            knots[0] = d->m_knot[i + d->m_order - 2]; //u0
            knots[1] = d->m_knot[i + d->m_order - 1]; //u1
            r_rational_bezier_curves.push_back(std::make_pair(new dtkRationalBezierCurve2D(dynamic_cast< dtkAbstractRationalBezierCurve2DData* >(on_bezier)), knots));
        } else {
            delete on_bezier;
        }
    }
}

/*! \fn dtkNurbsCurve2DDataOn::aabb(double* r_aabb) const
  Writes in \a r_aabb the [xmin, ymin, xmax, ymax] coordinates.

  \a r_aabb : array of size 4 to store the limits coordinates
*/
void dtkNurbsCurve2DDataOn::aabb(double* r_aabb) const
{
    ON_BoundingBox bb;
    bb = d->BoundingBox();
    r_aabb[0] = bb[0][0];
    r_aabb[1] = bb[0][1];
    r_aabb[2] = bb[1][0];
    r_aabb[3] = bb[1][1];
}

/*! \fn dtkNurbsCurve2DDataOn::extendedAabb(double *r_aabb, double factor) const
  Writes in \a r_aabb the [xmin, ymin, xmax, ymax] coordinates of an extened bounding box of factor \a factor. The extension factor extends the diagonal.

  \a r_aabb : array of size 4 to store the extended limits coordinates

  \a factor : factor used to lengthen the diagonal of the axis aligned bounding box
*/
void dtkNurbsCurve2DDataOn::extendedAabb(double* r_aabb, double factor) const
{
    ON_BoundingBox bb;
    bb = d->BoundingBox();
    r_aabb[0] = (bb[0][0] * ( 1 + factor) + bb[1][0] * (1 - factor)) / 2;
    r_aabb[1] = (bb[0][1] * ( 1 + factor) + bb[1][1] * (1 - factor)) / 2;
    r_aabb[2] = (bb[1][0] * ( 1 + factor) + bb[0][0] * (1 - factor)) / 2;
    r_aabb[3] = (bb[1][1] * ( 1 + factor) + bb[0][1] * (1 - factor)) / 2;
}

/*! \fn dtkNurbsCurve2DDataOn::clone(void) const
  Clone.
*/
dtkNurbsCurve2DDataOn* dtkNurbsCurve2DDataOn::clone(void) const
{
    return new dtkNurbsCurve2DDataOn(*this);
}

//
// dtkNurbsCurve2DDataOn.cpp ends here
