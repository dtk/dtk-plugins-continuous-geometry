// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <dtkContinuousGeometry.h>

#include <dtkNurbsCurve2DDataOnExport.h>

#include <dtkAbstractNurbsCurve2DData>
#include <dtkAbstractRationalBezierCurve2DData>

#include <dtkRationalBezierCurve2D>

#include "opennurbs_without_warnings.h"

class ON_NurbsCurve;

class DTKNURBSCURVE2DDATAON_EXPORT dtkNurbsCurve2DDataOn final : public dtkAbstractNurbsCurve2DData
{
public:
    dtkNurbsCurve2DDataOn(void);
    ~dtkNurbsCurve2DDataOn(void) final ;

public:
    void create(std::size_t nb_cp, std::size_t order, double *knots, double *cps) const override;
    void create(const ON_NurbsCurve& nurbs_curve);

public:
    std::size_t degree(void) const override;
    std::size_t nbCps(void) const override;

    void controlPoint(std::size_t i, double *r_cp) const override;
    void weight(std::size_t i, double *r_w) const override;

    void knots(double *r_u_knots) const override;

    const double *knots(void) const override;

 public:
    void evaluatePoint(double p_u, double *r_point) const override;
    void evaluateNormal(double p_u, double *r_normal) const override;
    void evaluateNormal(double p_u, double *r_point, double *r_normal) const override;

 public:
    void decomposeToRationalBezierCurve2Ds(std::vector< std::pair< dtkRationalBezierCurve2D *, double * >>& r_rational_bezier_curves) const override;

 public:
    void aabb(double *r_aabb) const override;
    void extendedAabb(double *r_aabb, double factor) const override;

public:
    dtkNurbsCurve2DDataOn* clone(void) const override;

private :
    mutable ON_NurbsCurve* d;
};


// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

inline dtkAbstractNurbsCurve2DData *dtkNurbsCurve2DDataOnCreator(void)
{
    return new dtkNurbsCurve2DDataOn();
}

//
// dtkNurbsCurve2DDataOn.h ends here
