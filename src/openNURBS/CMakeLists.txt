## Version: $Id$
##
######################################################################
##
### Commentary:
##
######################################################################
##
### Change Log:
##
######################################################################
##
### Code:

if(NOT openNURBS_FOUND)
  message(WARNING "openNURBS was not found. Please set OpenNURBS_DIR. Instead openNURBS plugins will not be compiled.")
endif(NOT openNURBS_FOUND)

if(openNURBS_FOUND)

include_directories(${openNURBS_INCLUDE_DIRS})

include_directories(${SISL_INCLUDE_DIR})

include_directories(${CGAL_INCLUDE_DIRS})

## ###################################################################
## Dependencies - internal
## ###################################################################

include_directories(dtkNurbsCurveDataOn/src)
include_directories(dtkNurbsCurve2DDataOn/src)
include_directories(dtkNurbsSurfaceDataOn/src)
include_directories(dtkRationalBezierCurveDataOn/src)
include_directories(dtkRationalBezierCurve2DDataOn/src)
include_directories(dtkRationalBezierSurfaceDataOn/src)

## #################################################################
## Inputs
## #################################################################
add_subdirectory(dtkNurbsCurveDataOn)
add_subdirectory(dtkNurbsCurve2DDataOn)
add_subdirectory(dtkNurbsSurfaceDataOn)
add_subdirectory(dtkRationalBezierCurveDataOn)
add_subdirectory(dtkRationalBezierCurve2DDataOn)
add_subdirectory(dtkRationalBezierSurfaceDataOn)

add_subdirectory(openNURBSBRepReader)

######################################################################

endif(openNURBS_FOUND)

# ######################################################################
# ### CMakeLists.txt ends here
