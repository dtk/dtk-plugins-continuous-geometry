// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <QTest>

class dtkRationalBezierSurfaceDataOnTestCasePrivate;

class dtkRationalBezierSurfaceDataOnTestCase : public QObject
{
    Q_OBJECT

public:
    dtkRationalBezierSurfaceDataOnTestCase(void);
    ~dtkRationalBezierSurfaceDataOnTestCase(void);

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testControlPoint(void);
    void testWeight(void);
    void testUDegree(void);
    void testVDegree(void);
    void testIsDegenerate(void);
    void testdegenerateLocations(void);
    void testSurfacePoint(void);
    void testSurfaceNormal(void);
    void testPrintOutSurface(void);
    void evaluatePointBlossom(void);
    void testSplit(void);
    void testAabb(void);
    void testExtendedAabb(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);

private:
    dtkRationalBezierSurfaceDataOnTestCasePrivate* d;
};

//
// dtkRationalBezierSurfaceDataOnTest.h ends here
