// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#include "dtkRationalBezierSurfaceDataOn.h"
#include "dtkRationalBezierSurfaceDataOnPlugin.h"

#include <dtkCore>
#include <dtkContinuousGeometry>

// ///////////////////////////////////////////////////////////////////
// dtkRationalBezierSurfaceDataOnPlugin
// ///////////////////////////////////////////////////////////////////

void dtkRationalBezierSurfaceDataOnPlugin::initialize(void)
{
    dtkContinuousGeometry::abstractRationalBezierSurfaceData::pluginFactory().record("dtkRationalBezierSurfaceDataOn", dtkRationalBezierSurfaceDataOnCreator);
}

void dtkRationalBezierSurfaceDataOnPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkRationalBezierSurfaceDataOn)

//
// dtkRationalBezierSurfaceDataOnPlugin.cpp ends here
