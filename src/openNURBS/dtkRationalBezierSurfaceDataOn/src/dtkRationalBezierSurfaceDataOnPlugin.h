// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <QtCore>

#include <dtkContinuousGeometry>

#include <dtkAbstractRationalBezierSurfaceData>

class dtkRationalBezierSurfaceDataOnPlugin : public dtkAbstractRationalBezierSurfaceDataPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkAbstractRationalBezierSurfaceDataPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkRationalBezierSurfaceDataOnPlugin" FILE "dtkRationalBezierSurfaceDataOnPlugin.json")

public:
     dtkRationalBezierSurfaceDataOnPlugin(void) {}
    ~dtkRationalBezierSurfaceDataOnPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

//
// dtkRationalBezierSurfaceDataOnPlugin.h ends here
