// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkRationalBezierSurfaceDataOn.h"

#include "dtkRationalBezierSurface.h"

#include "opennurbs_without_warnings.h"

#include <cassert>

#include <boost/container/small_vector.hpp>

/*!
  \class dtkRationalBezierSurfaceDataOn
  \inmodule dtkPluginsContinuousGeometry
  \brief dtkRationalBezierSurfaceDataOn is an openNURBS implementation of the concept dtkAbstractRationalBezierSurfaceData.

  The following lines of code show how to instanciate and use the class in pratice:
  \code
  dtkAbstractRationalBezierSurfaceData *nurbs_surface_data = dtkContinuousGeometry::abstractRationalBezierSurfaceData::pluginFactory().create("dtkRationalBezierSurfaceDataOn");
  dtkRationalBezierSurface *rational_bezier_surface = new dtkRationalBezierSurface(rational_bezier_surface_data);
  rational_bezier_surface->create(...);
  \endcode
*/

/*! \fn dtkRationalBezierSurfaceDataOn::dtkRationalBezierSurfaceDataOn(void)
  Instanciates the structure.

  To add the content, see \l create(std::size_t dim, std::size_t order_u, std::size_t order_v, double* cps) const.
*/
dtkRationalBezierSurfaceDataOn::dtkRationalBezierSurfaceDataOn(void) : d(nullptr) {}

/*! \fn dtkRationalBezierSurfaceDataOn::~dtkRationalBezierSurfaceDataOn(void)
  Destroys the instance.
*/
dtkRationalBezierSurfaceDataOn::~dtkRationalBezierSurfaceDataOn(void)
{
    d->Destroy();
    delete d;
}

/*! \fn dtkRationalBezierSurfaceDataOn::create()
  Not available in the dtkAbstractRationalBezierSurfaceData.

  Creates an empty rational Bezier surface object.
*/
void dtkRationalBezierSurfaceDataOn::create()
{
    d = new ON_BezierSurface();
}

/*! \fn dtkRationalBezierSurfaceDataOn::create(const ON_BezierSurface& bezier_surface)
  Not available in the dtkAbstractRationalBezierCurve2DData.

  Creates the rational Bezier surface by providing a rational Bezier surface from the openNURBS API.

  \a bezier_surface : the rational Bezier surface to copy.
*/
void dtkRationalBezierSurfaceDataOn::create(const ON_BezierSurface& bezier_surface)
{
    d = new ON_BezierSurface(bezier_surface);
    assert(d->IsValid());
}


/*! \fn void dtkRationalBezierSurfaceDataOn::create(std::size_t dim, std::size_t nb_cp_u, std::size_t nb_cp_v, double *cps) const
  Creates a NURBS surface by providing the required content.

  \a dim : dimension of the space in which lies the surface

  \a nb_cp_u : number of control points along the "u" direction

  \a nb_cp_v : number of control points along the "v" direction

  \a cps : array containing the weighted control points, secified as : [x_00, y_00, z_00, w_00, x_01, y_01, ...]

  \a cps are copied.

  The control points are given as such :

  v = 1      cps[1]      | cps[3]

        ---------------- | --------------

  v= 0       cps[0]      | cps[2]

              u=0           u=1
*/
void dtkRationalBezierSurfaceDataOn::create(std::size_t dim, std::size_t order_u, std::size_t order_v, double* cps) const
{
    // /////////////////////////////////////////////////////////////////
    // Initializes the Open RationalBezier NURBS surface
    // /////////////////////////////////////////////////////////////////
    d = new ON_BezierSurface(dim, true, order_u, order_v);

    // ///////////////////////////////////////////////////////////////////
    // In its call to New, openNURBS reserves enough space at d->m_cv to store the values of control points
    // ///////////////////////////////////////////////////////////////////
    for(std::size_t i = 0; i < order_u; ++i) {
        for(std::size_t j = 0; j < order_v; ++j) {
            d->m_cv[4 * order_v * i + 4 * j] =     cps[4 * order_v * i + 4 * j] *     cps[4 * order_v * i + 4 * j + 3];
            d->m_cv[4 * order_v * i + 4 * j + 1] = cps[4 * order_v * i + 4 * j + 1] * cps[4 * order_v * i + 4 * j + 3];
            d->m_cv[4 * order_v * i + 4 * j + 2] = cps[4 * order_v * i + 4 * j + 2] * cps[4 * order_v * i + 4 * j + 3];
            d->m_cv[4 * order_v * i + 4 * j + 3] = cps[4 * order_v * i + 4 * j + 3];
        }
    }

    if(!d->IsValid()) {
        dtkFatal() << "The dtkRationalBezierSurfaceDataOn is not valid";
    }
}

/*! \fn void dtkRationalBezierSurfaceDataOn::create(std::string path) const
  Creates a rational Bezier surface by providing a path to a file storing the required information to create a rational Bezier surface.

  \a path : a valid path to the file containing the information regarding a given rational Bezier surface
*/
void dtkRationalBezierSurfaceDataOn::create(std::string path) const
{
    QFile file(QString::fromStdString(path));
    QIODevice *in = &file;

    QIODevice::OpenMode mode = QIODevice::ReadOnly;
    mode |= QIODevice::Text;

    // to avoid troubles with floats separators ('.' and not ',')
    QLocale::setDefault(QLocale::c());
#if defined (Q_OS_UNIX) && !defined(Q_OS_MAC)
    setlocale(LC_NUMERIC, "C");
#endif

    if (!in->open(mode)) {
        dtkFatal() << Q_FUNC_INFO << "The file could not be found, please verify the path";
        return;
    }

    QString line = in->readLine().trimmed();
    //Run tests on the file to check its validity TODO more
    if ( line != "#dtkRationalBezierSurfaceDataOn"){
        dtkFatal() << Q_FUNC_INFO << "file not in a dtkRationalBezierSurfaceDataOn format.";
        return;
    }
    std::size_t dim = 3;
    QRegExp re = QRegExp("\\s+");

    line = in->readLine().trimmed();
    QStringList vals = line.split(re);
    std::size_t order_u = vals[1].toInt() + 1;
    line = in->readLine().trimmed();
    vals = line.split(re);
    std::size_t order_v = vals[1].toInt() + 1;

    d = new ON_BezierSurface(dim, true, order_u, order_v);

    for(std::size_t i = 0; i < order_u; ++i) {
        for(std::size_t j = 0; j < order_v;) {
            line = in->readLine().trimmed();
            if (line.startsWith("#")) {
                continue;
            }
            std::istringstream lin(line.toStdString());
            double x, y, z, w;
            lin >> x >> y >> z >> w;       //now read the whitespace-separated floats
            d->m_cv[4 * order_v * i + 4 * j] =     x * w;
            d->m_cv[4 * order_v * i + 4 * j + 1] = y * w;
            d->m_cv[4 * order_v * i + 4 * j + 2] = z * w;
            d->m_cv[4 * order_v * i + 4 * j + 3] = w;
            ++j;
        }
    }

    if(!d->IsValid()) {
        dtkFatal() << "The dtkRationalBezierSurfaceDataOn is not valid";
    }
}

/*! \fn dtkRationalBezierSurfaceDataOn::uDegree(void) const
  Returns the degree of the surface in the "u" direction.
*/
std::size_t dtkRationalBezierSurfaceDataOn::uDegree(void) const
{
    return d->Degree(0);
}

/*! \fn dtkRationalBezierSurfaceDataOn::vDegree(void) const
  Returns the degree of the surface in the "v" direction.
*/
std::size_t dtkRationalBezierSurfaceDataOn::vDegree(void) const
{
    return d->Degree(1);
}

/*! \fn dtkRationalBezierSurfaceDataOn::controlPoint(std::size_t i, std::size_t j, double *r_cp) const
  Writes in \a r_cp the \a i th, \a j th weighted control point coordinates[xij, yij, zij].

  \a i : index of the weighted control point along the "u" direction

  \a j : index of the weighted control point along the "v" direction

  \a r_cp : array of size 3 to store the weighted control point coordinates[xij, yij, zij]
 */
void dtkRationalBezierSurfaceDataOn::controlPoint(std::size_t i, std::size_t j, double* r_cp) const
{
    ON_3dPoint point;
     d->GetCV(i, j, point);
     r_cp[0] = point[0];
     r_cp[1] = point[1];
     r_cp[2] = point[2];
}

/*! \fn dtkRationalBezierSurfaceDataOn::weight(std::size_t i, std::size_t j, double *r_w) const
  Writes in \a r_w the \a i th, \a j th weighted control point weight wij.

  \a i : index of the weighted control point along the "u" direction

  \a j : index of the weighted control point along the "v" direction

  \a r_w : array of size 1 to store the weighted control point weight wij
*/
void dtkRationalBezierSurfaceDataOn::weight(std::size_t i, std::size_t j, double* r_w) const
{
    *r_w = d->Weight(i, j);
}

/*! \fn dtkRationalBezierSurfaceDataOn::isDegenerate(void) const
  Returns true if the rational Bezier surface is degenerate : i.e. a line of control points coalesce, else returns false.
*/
bool dtkRationalBezierSurfaceDataOn::isDegenerate(void) const
{
    // ///////////////////////////////////////////////////////////////////
    // To checks that a patch is degenerate, one must make sure that all control points  on a line do not colllapse to one single point
    //The algorithm checks each possible line and make sure at least 2 different control points are on it
    // ///////////////////////////////////////////////////////////////////
    bool isLineDegenerate = false;
    // ///////////////////////////////////////////////////////////////////
    // Checks in the first direction (from v = 0 -> v=1)
    // ///////////////////////////////////////////////////////////////////
    for (std::size_t i = 0; i < std::size_t(d->Degree(0) + 1); ++i) {
        ON_3dPoint pointA;
        d->GetCV(i, 0, pointA);
        isLineDegenerate = true;
        for (std::size_t k = 1; k < std::size_t(d->Degree(1) + 1); ++k) {
            ON_3dPoint pointB;
            d->GetCV(i, k, pointB);
            if (pointA != pointB) {
                isLineDegenerate = false;
                break;
            }
        }
        if (isLineDegenerate  == true) {
            return true;
        }
    }
    // ///////////////////////////////////////////////////////////////////
    // Checks it the second direction (from u = 0 -> u = 1)
    // ///////////////////////////////////////////////////////////////////
    for (std::size_t i = 0; i < std::size_t(d->Degree(1) + 1); ++i) {
        ON_3dPoint pointA;
        d->GetCV(0, i, pointA);
        isLineDegenerate = true;
        for (std::size_t k = 1; k < std::size_t(d->Degree(0) + 1); ++k) {
            ON_3dPoint pointB;
            d->GetCV(k, i, pointB);
            if (pointA != pointB) {
                isLineDegenerate = false;
                break;
            }
        }
        if (isLineDegenerate  == true) {
            return true;
        }
    }
    return false;

    // ///////////////////////////////////////////////////////////////////
    // TODO Checks that the diagonal are not collapsed
    // ///////////////////////////////////////////////////////////////////

}

/*! \fn dtkRationalBezierSurfaceDataOn::degenerateLocations(std::list< dtkContinuousGeometryPrimitives::Point_3 >& r_degenerate_locations) const
  Returns true if the rational Bezier surface is degenerate : i.e. a line of control points coalesce and adds in \a r_degenerate_locations the locations at which there are degeneracies, else returns false.
*/
bool dtkRationalBezierSurfaceDataOn::degenerateLocations(std::list< dtkContinuousGeometryPrimitives::Point_3 >& r_degenerate_locations) const
{
    // ///////////////////////////////////////////////////////////////////
    // Checks in the first direction (from v = 0 -> v=1)
    // ///////////////////////////////////////////////////////////////////
    bool isLineDegenerate = true;
    for (std::size_t i = 0; i < std::size_t(d->Degree(0) + 1); ++i) {
        ON_3dPoint pointA;
        d->GetCV(i, 0, pointA);
        isLineDegenerate = true;
        for (std::size_t k = 1; k < std::size_t(d->Degree(1) + 1); ++k) {
            ON_3dPoint pointB;
            d->GetCV(i, k, pointB);
            if (pointA != pointB) {
                isLineDegenerate = false;
                break;
            }
        }
        if (isLineDegenerate  == true) {
            r_degenerate_locations.push_back(dtkContinuousGeometryPrimitives::Point_3(pointA[0], pointA[1], pointA[2]));
        }
    }
    // ///////////////////////////////////////////////////////////////////
    // Checks it the second direction (from u = 0 -> u = 1)
    // ///////////////////////////////////////////////////////////////////
    for (std::size_t i = 0; i < std::size_t(d->Degree(1) + 1); ++i) {
        ON_3dPoint pointA;
        d->GetCV(0, i, pointA);
        isLineDegenerate = true;
        for (std::size_t k = 1; k < std::size_t(d->Degree(0) + 1); ++k) {
            ON_3dPoint pointB;
            d->GetCV(k, i, pointB);
            if (pointA != pointB) {
                isLineDegenerate = false;
                break;
            }
        }
        if (isLineDegenerate  == true) {
            r_degenerate_locations.push_back(dtkContinuousGeometryPrimitives::Point_3(pointA[0], pointA[1], pointA[2]));
        }
    }

    if (r_degenerate_locations.size() > 0) {
        return true;
    } else {
        return false;
    }
}

/*! \fn dtkRationalBezierSurfaceDataOn::evaluatePoint(double p_u, double p_v, double *r_point) const
  Writes in \a r_point the evaluation at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_point : array of size 3 to store the result of the evaluation
*/
void dtkRationalBezierSurfaceDataOn::evaluatePoint(double p_u, double p_v, double *r_point) const
{
    ON_3dPoint on_point = d->PointAt(p_u, p_v);
    r_point[0] = on_point[0];
    r_point[1] = on_point[1];
    r_point[2] = on_point[2];
}

/*! \fn dtkRationalBezierSurfaceDataOn::evaluateNormal(double p_u, double p_v, double *r_normal) const
  Writes in \a r_normal the normal evaluation at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_normal : array of size 3 to store the result of the normal evaluation
*/
void dtkRationalBezierSurfaceDataOn::evaluateNormal(double p_u, double p_v, double *r_normal) const
{
    double *deriv = new double[9];
    d->Evaluate(p_u, p_v, 1, d->m_dim, &deriv[0]);
    dtkContinuousGeometryPrimitives::Vector_3 du(deriv[3], deriv[4], deriv[5]);
    dtkContinuousGeometryPrimitives::Vector_3 dv(deriv[6], deriv[7], deriv[8]);
    dtkContinuousGeometryPrimitives::Vector_3 normal = dtkContinuousGeometryTools::crossProduct(du, dv);
    r_normal[0] = normal[0];
    r_normal[1] = normal[1];
    r_normal[2] = normal[2];
    delete[] deriv;
}

/*! \fn dtkRationalBezierSurfaceDataOn::evaluate1stDer(double p_u, double p_v, double *r_point, double *r_u_deriv, double *r_v_deriv) const
  Writes in \a r_point the evaluation, in \a r_u_deriv the first derivative in "u" direction, in \a r_v_deriv the first derivative in the "v" direction at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_point : array of size 3 to store the result of the evaluation

  \a r_u_deriv : array of size 3 to store the result of the first derivative evaluation in the "u" direction

  \a r_v_deriv : array of size 3 to store the result of the first derivative evaluation in the "v" direction
*/
void dtkRationalBezierSurfaceDataOn::evaluate1stDer(double, double, double*, double*, double*) const
{
    dtkWarn() << "Does nothing";
}

/*! \fn dtkRationalBezierSurfaceDataOn::evaluate1stDer(double p_u, double p_v, double *r_u_deriv, double *r_v_deriv) const
  Writes in \a r_u_deriv the first derivative in "u" direction and in \a r_v_deriv the first derivative in the "v" direction at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_u_deriv : array of size 3 to store the result of the first derivative evaluation in the "u" direction

  \a r_v_deriv : array of size 3 to store the result of the first derivative evaluation in the "v" direction
*/
void dtkRationalBezierSurfaceDataOn::evaluate1stDer(double, double, double*, double*) const
{
    dtkWarn() << "Does nothing";
}

/*! \fn dtkRationalBezierSurfaceDataOn::evaluatePointBlossom(const double* p_uis, const double* p_vis, double *r_point, double *r_weight) const
  Stores in \a r_point and \a r_weight the result of the blossom evaluation at \a p_uis, \a p_vis values.

  \a p_uis : "u" values for blossom evaluation

  \a p_vis : "v" values for blossom evaluation

  \a r_point : array of size 3 to slore the point result of the blossom evaluation

  \a r_weight : array of size 1 to slore the weight result of the blossom evaluation
*/
void dtkRationalBezierSurfaceDataOn::evaluatePointBlossom(const double* p_uis, const double* p_vis, double* r_point, double* r_weight) const
{
    // ///////////////////////////////////////////////////////////////////
    // For each j line, computes the F(u_1->u_degree, vj)
    // ///////////////////////////////////////////////////////////////////
    ON_3dPoint point;
    // std::vector< ON_3dPoint > vPjs; //F(u_I_1, .., u_I_i, ..., u_I_uDegree; v_I_j)
    // std::vector< double > vWjs; //w(u_I_1, .., u_I_i, ..., u_I_uDegree; v_I_j)
    // vPjs.resize(d->Degree(1) + 1);
    // vWjs.resize(d->Degree(1) + 1);
    // std::vector< ON_3dPoint > tempPs;
    // std::vector< double >     tempWs;
    // tempPs.resize(d->Degree(0) + 1);
    // tempWs.resize(d->Degree(0) + 1);
    using vector_of_double = boost::container::small_vector<double, 32>;
    vector_of_double vWPjs(4*(d->Degree(1)+1));
    vector_of_double tempWPs(4*(d->Degree(0) + 1));
    for(std::size_t j = 0, degree_1 = std::size_t(d->Degree(1));
        j <= degree_1; ++j)
    {
        // ///////////////////////////////////////////////////////////////////
        // Fills temps with the original control points and weights
        // ///////////////////////////////////////////////////////////////////
        for(std::size_t i = 0, degree_0 = std::size_t(d->Degree(0));
            i <= degree_0; ++i)
        {
            const double* cv = d->CV(i, j);
            std::copy(cv, cv+4, &tempWPs[4*i]);
        }

        for(std::size_t k = 0, degree_0 = std::size_t(d->Degree(0));
            k < degree_0; ++k)
        {
            const double d2 = p_uis[k];
            const double d1 = 1. - d2;
            for (std::size_t i = 0, degree_0_minus_k_time_4 = 4*(degree_0 - k);
                 i < degree_0_minus_k_time_4; ++i)
            {
                tempWPs[i] = tempWPs[i] * d1 + tempWPs[i+4] * d2;
            }
        }
        std::copy(&tempWPs[0], &tempWPs[0]+4, &vWPjs[4*j]);
    }
    // ///////////////////////////////////////////////////////////////////
    // Fills temps with the previously computed control points and weights
    // ///////////////////////////////////////////////////////////////////
    tempWPs = std::move(vWPjs);
    for(std::size_t k = 0, degree_1 = std::size_t(d->Degree(1));
        k < degree_1; ++k)
    {
        const double d2 = p_vis[k];
        const double d1 = 1. - d2;
        for (std::size_t i = 0, degree_1_minus_k_time_4 = 4*(degree_1 - k);
             i < degree_1_minus_k_time_4; ++i)
        {
            tempWPs[i] = tempWPs[i] * d1 + tempWPs[i+4] * d2;
        }
    }
    r_point[0] = tempWPs[0] / tempWPs[3];
    r_point[1] = tempWPs[1] / tempWPs[3];
    r_point[2] = tempWPs[2] / tempWPs[3];
    *r_weight = tempWPs[3];
}

/*! \fn dtkRationalBezierSurfaceDataOn::split(dtkRationalBezierSurface *r_split_surface, double *p_splitting_parameters) const
  Creates \a r_split_surface as the part of rational Bezier surface lying inside the bounding box of parameters \a p_splitting_parameters.

  \a r_split_surface : a pointer to a valid dtkRationalBezierSurface with data not initialized, the part of the initial surface restricted to the bouding box of parameters given in \a p_splitting_parameters will be added as data

  \a p_splitting_parameters : array of size 4, storing the parameters defining the restriction in the parameter space of the rational Bezier surface, given as [u_0, v_0, u_1, v_1]
 */
void dtkRationalBezierSurfaceDataOn::split(dtkRationalBezierSurface *r_split_surface, double *p_splitting_parameters) const
{
    ON_BezierSurface on_surface_A;
    ON_BezierSurface on_surface_B;
    d->Split(0, p_splitting_parameters[0], on_surface_A, on_surface_B); //W : on_surface_A , E : on_surface_B
    ON_BezierSurface on_surface_C;
    on_surface_B.Split(1, p_splitting_parameters[1], on_surface_A, on_surface_C); //S : on_surface_A , N : on_surface_C
    on_surface_C.Split(0, p_splitting_parameters[2], on_surface_A, on_surface_B); //W : on_surface_A, E : on_surface_B
    on_surface_A.Split(1, p_splitting_parameters[3], on_surface_B, on_surface_C); //S : on_surface_B , N : on_surface_C

    dtkRationalBezierSurfaceDataOn *dtk_surface_data = new dtkRationalBezierSurfaceDataOn();
    dtk_surface_data->create(on_surface_B);
    r_split_surface->setData(static_cast<dtkAbstractRationalBezierSurfaceData * >(dtk_surface_data));
}

/*! \fn dtkRationalBezierSurfaceDataOn::aabb(double* r_aabb) const
  Writes in \a r_aabb the [xmin, ymin, zmin, xmax, ymax, zmax] coordinates.

  \a r_aabb : array of size 6 to store the limits coordinates
 */
void dtkRationalBezierSurfaceDataOn::aabb(double* r_aabb) const
{
    d->GetBBox(&r_aabb[0], &r_aabb[3], false);
}

/*! \fn dtkRationalBezierSurfaceDataOn::extendedAabb(double *r_aabb, double factor) const
  Writes in \a r_aabb the [xmin, ymin, zmin, xmax, ymax, zmax] coordinates of an extened bounding box of factor \a factor. The extension factor extends the diagonal.

  \a r_aabb : array of size 6 to store the extended limits coordinates

  \a factor : factor used to lengthen the diagonal of the axis aligned bounding box
*/
void dtkRationalBezierSurfaceDataOn::extendedAabb(double* r_aabb, double factor) const
{
    ON_BoundingBox bb;
    bb = d->BoundingBox();
    r_aabb[0] = (bb[0][0] * ( 1 + factor) + bb[1][0] * (1 - factor)) / 2;
    r_aabb[1] = (bb[0][1] * ( 1 + factor) + bb[1][1] * (1 - factor)) / 2;
    r_aabb[2] = (bb[0][2] * ( 1 + factor) + bb[1][2] * (1 - factor)) / 2;
    r_aabb[3] = (bb[1][0] * ( 1 + factor) + bb[0][0] * (1 - factor)) / 2;
    r_aabb[4] = (bb[1][1] * ( 1 + factor) + bb[0][1] * (1 - factor)) / 2;
    r_aabb[5] = (bb[1][2] * ( 1 + factor) + bb[0][2] * (1 - factor)) / 2;
}

/*! \fn dtkRationalBezierSurfaceDataOn::onRationalBezierSurface(void)
  Not available in the dtkAbstractRationalBezierSurfaceData.

  Returns the underlying openNURBS rational Bezier surface.
 */
ON_BezierSurface& dtkRationalBezierSurfaceDataOn::onRationalBezierSurface(void)
{
    return *d;
}

/*! \fn dtkRationalBezierSurfaceDataOn::clone(void) const
   Clone.
*/
dtkRationalBezierSurfaceDataOn* dtkRationalBezierSurfaceDataOn::clone(void) const
{
    return new dtkRationalBezierSurfaceDataOn(*this);
}

#include <iomanip>
void dtkRationalBezierSurfaceDataOn::print(std::ostream& stream) const
{
    stream << "#dtkRationalBezierSurfaceDataOn" << std::endl;
    stream << "u_degree " << d->Degree(0) << std::endl;
    stream << "v_degree " << d->Degree(1) << std::endl;
    ON_3dPoint point;
    for(auto i = 0; i < d->Degree(0) + 1; ++i) {
        for(auto j = 0; j < d->Degree(0) + 1; ++j) {
            d->GetCV(i, j, point);
            stream << point[0] << " " << point[1] << " " << point[2] << " " << d->Weight(i, j) << std::setprecision(std::numeric_limits< double >::digits10 + 1) << std::endl;
        }
    }
    stream << "#dtkRationalBezierSurfaceDataOn" << std::endl;
}

//
// dtkRationalBezierSurfaceDataOn.cpp ends here

// Local Variables:
// c-basic-offset: 4
// End:
