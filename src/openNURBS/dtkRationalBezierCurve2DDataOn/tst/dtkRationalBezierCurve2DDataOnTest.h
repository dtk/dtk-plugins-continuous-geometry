// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <QTest>

class dtkRationalBezierCurve2DDataOnTestCasePrivate;

class dtkRationalBezierCurve2DDataOnTestCase : public QObject
{
    Q_OBJECT

public:
    dtkRationalBezierCurve2DDataOnTestCase(void);
    ~dtkRationalBezierCurve2DDataOnTestCase(void);

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testControlPoint(void);
    void testWeight(void);
    void testDegree(void);
    void testCurvePoint(void);
    void testCurveNormal(void);
    void testPrintOutCurve(void);
    void testAabb(void);
    void testExtendedAabb(void);
    void testSplit(void);
    void testMultiSplit(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);

private:
    dtkRationalBezierCurve2DDataOnTestCasePrivate* d;
};

//
// dtkRationalBezierCurve2DDataOnTest.h ends here
