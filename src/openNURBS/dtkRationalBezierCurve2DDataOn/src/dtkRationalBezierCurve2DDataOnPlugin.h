// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <QtCore>

#include <dtkContinuousGeometry>

#include <dtkAbstractRationalBezierCurve2DData>

class dtkRationalBezierCurve2DDataOnPlugin : public dtkAbstractRationalBezierCurve2DDataPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkAbstractRationalBezierCurve2DDataPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkRationalBezierCurve2DDataOnPlugin" FILE "dtkRationalBezierCurve2DDataOnPlugin.json")

public:
     dtkRationalBezierCurve2DDataOnPlugin(void) {}
    ~dtkRationalBezierCurve2DDataOnPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

//
// dtkRationalBezierCurve2DDataOnPlugin.h ends here
