// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <dtkContinuousGeometry.h>

#include <dtkContinuousGeometryUtils>

#include <dtkRationalBezierCurve2DDataOnExport.h>

#include <dtkAbstractRationalBezierCurve2DData>

class ON_BezierCurve;

class DTKRATIONALBEZIERCURVE2DDATAON_EXPORT dtkRationalBezierCurve2DDataOn final : public dtkAbstractRationalBezierCurve2DData
{
private:
    typedef dtkContinuousGeometryPrimitives::Point_2 Point_2;
    typedef dtkContinuousGeometryPrimitives::Vector_2 Vector_2;

public:
    dtkRationalBezierCurve2DDataOn(void);
    // dtkRationalBezierCurve2DDataOn(const dtkRationalBezierCurve2DDataOn& mesh_data);
    ~dtkRationalBezierCurve2DDataOn(void) final ;

public:
    void create(std::size_t order, double* cps) const override;
    void create(const ON_BezierCurve& bezier_curve);
    void create();

public:
    std::size_t degree(void) const override;
    void controlPoint(std::size_t i, double* r_cp) const override;
    void weight(std::size_t i, double* r_w) const override;

public:
    void evaluatePoint(double p_u, double* r_point) const override;
    void evaluateNormal(double p_u, double* r_normal) const override;

 public:
    void split(dtkRationalBezierCurve2D *r_split_curve_a, dtkRationalBezierCurve2D *r_split_curve_b, double splitting_parameter) const override;
    void split(std::list< dtkRationalBezierCurve2D * >& r_split_curves, const std::vector< double >& p_splitting_parameters) const override;

 public:
    void aabb(double* r_aabb) const override;
    void extendedAabb(double* r_aabb, double factor) const override;

 public:
    ON_BezierCurve& onRationalBezierCurve2D(void);

public:
    dtkRationalBezierCurve2DDataOn* clone(void) const override;

private :
    mutable ON_BezierCurve* d;
};


// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

inline dtkAbstractRationalBezierCurve2DData *dtkRationalBezierCurve2DDataOnCreator(void)
{
    return new dtkRationalBezierCurve2DDataOn();
}

//
// dtkRationalBezierCurve2DDataOn.h ends here
