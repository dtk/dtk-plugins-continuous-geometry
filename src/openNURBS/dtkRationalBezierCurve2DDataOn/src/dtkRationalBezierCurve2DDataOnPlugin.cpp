// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#include "dtkRationalBezierCurve2DDataOn.h"
#include "dtkRationalBezierCurve2DDataOnPlugin.h"

#include <dtkCore>
#include <dtkContinuousGeometry>

// ///////////////////////////////////////////////////////////////////
// dtkRationalBezierCurve2DDataOnPlugin
// ///////////////////////////////////////////////////////////////////

void dtkRationalBezierCurve2DDataOnPlugin::initialize(void)
{
    dtkContinuousGeometry::abstractRationalBezierCurve2DData::pluginFactory().record("dtkRationalBezierCurve2DDataOn", dtkRationalBezierCurve2DDataOnCreator);
}

void dtkRationalBezierCurve2DDataOnPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkRationalBezierCurve2DDataOn)

//
// dtkRationalBezierCurve2DDataOnPlugin.cpp ends here
