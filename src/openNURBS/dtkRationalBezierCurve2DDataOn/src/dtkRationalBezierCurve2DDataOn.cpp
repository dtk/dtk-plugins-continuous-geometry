// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkRationalBezierCurve2DDataOn.h"

#include <dtkRationalBezierCurve2D>

#include <opennurbs_without_warnings.h>
#include <opennurbs_bezier.h>

#include <cassert>

/*!
  \class dtkRationalBezierCurve2DDataOn
  \inmodule dtkPluginsContinuousGeometry
  \brief dtkRationalBezierCurve2DDataOn is an openNURBS implementation of the concept dtkAbstractRationalBezierCurve2DData.

  The following lines of code show how to instanciate and use the class in pratice:
  \code
  dtkAbstractRationalBezierCurve2DData *rational_bezier_curve_data = dtkContinuousGeometry::abstractRationalBezierCurve2DData::pluginFactory().create("dtkRationalBezierCurve2DDataOn");
  dtkRationalBezierCurve2D *rational_bezier_curve = new dtkRationalBezierCurve2D(rational_bezier_curve_data);
  rational_bezier_curve->create(...);
  \endcode
*/

/*! \fn dtkRationalBezierCurve2DDataOn::dtkRationalBezierCurve2DDataOn(void)
  Instanciates the structure.

  To add the content, see \l create(std::size_t order, double *cps) const.
*/
dtkRationalBezierCurve2DDataOn::dtkRationalBezierCurve2DDataOn(void) : d(nullptr) {}

/*! \fn dtkRationalBezierCurve2DDataOn::~dtkRationalBezierCurve2DDataOn(void)
  Destroys the instance.
*/
dtkRationalBezierCurve2DDataOn::~dtkRationalBezierCurve2DDataOn(void)
{
    d->Destroy();
    delete d;
}

/*! \fn dtkRationalBezierCurve2DDataOn::create()
  Not available in the dtkAbstractRationalBezierCurve2DData.

  Creates an empty 2D rational Bezier curve object.
*/
void dtkRationalBezierCurve2DDataOn::create()
{
    d = new ON_BezierCurve();
}

/*! \fn dtkRationalBezierCurve2DDataOn::create(const ON_BezierCurve& bezier_curve)
  Not available in the dtkAbstractRationalBezierCurve2DData.

  Creates the 2D rational Bezier curve by providing a rational Bezier curve from the openNURBS API.

  \a bezier_curve : the rational Bezier curve to copy.
*/
void dtkRationalBezierCurve2DDataOn::create(const ON_BezierCurve& bezier_curve)
{
    d = new ON_BezierCurve(bezier_curve);
    assert(d->IsValid());
}

/*! \fn dtkRationalBezierCurve2DDataOn::create(std::size_t order, double *cps) const
  Creates the 2D rational Bezier curve by providing the required content.

 \a order : order

 \a cps : array containing the weighted control points, specified as : [x0, y0, w0, ...]
*/
void dtkRationalBezierCurve2DDataOn::create(std::size_t order, double* cps) const
{
    // /////////////////////////////////////////////////////////////////
    // Initializes the Open RationalBezier NURBS surface
    // /////////////////////////////////////////////////////////////////
    d = new ON_BezierCurve(2, true, order);

    for(std::size_t i = 0; i < order; ++i) {
        d->m_cv[3 * i]     = cps[3 * i]     * cps[3 * i + 2];
        d->m_cv[3 * i + 1] = cps[3 * i + 1] * cps[3 * i + 2];
        d->m_cv[3 * i + 2] = cps[3 * i + 2];
    }

    if(!d->IsValid()) {
        dtkFatal() << "The dtkRationalBezierCurve2DDataOn is not valid";
    }
}

/*! \fn  dtkRationalBezierCurve2DDataOn::degree(void) const
  Returns the degree of the curve.
*/
std::size_t dtkRationalBezierCurve2DDataOn::degree(void) const
{
    return d->Degree();
}

/*! \fn  dtkRationalBezierCurve2DDataOn::controlPoint(std::size_t i, double *r_cp) const
  Writes in \a r_cp the \a i th weighted control point coordinates[xi, yi].

  \a i : index of the weighted control point

  \a r_cp : array of size 2 to store the weighted control point coordinates[xi, yi]

  The method can be called as follow:
  \code
  dtkContinuousGeometryPrimitives::Point_2 point2(0, 0);
  rational_bezier_curve_data->controlPoint(3, point2.data());
  \endcode
*/
void dtkRationalBezierCurve2DDataOn::controlPoint(std::size_t i, double* r_cp) const
{
    ON_3dPoint point;
    d->GetCV(i, point);
    r_cp[0] = point[0];
    r_cp[1] = point[1];
}

/*! \fn  dtkRationalBezierCurve2DDataOn::weight(std::size_t i, double *r_w) const
  Writes in \a r_w the \a i th weighted control point weight wi.

  \a i : index of the weighted control point

  \a r_w : array of size 1 to store the weighted control point weight wi
*/
void dtkRationalBezierCurve2DDataOn::weight(std::size_t i, double* r_w) const
{
    *r_w = d->Weight(i);
}

/*! \fn dtkRationalBezierCurve2DDataOn::evaluatePoint(double p_u, double *r_point) const
  Writes in \a r_point the evaluation at parameter \a p_u.

  \a p_u : parameter

  \a r_point : array of size 2 to store the result of the evaluation
*/
void dtkRationalBezierCurve2DDataOn::evaluatePoint(double p_u, double* r_point) const
{
    ON_3dPoint on_point = d->PointAt(p_u);
    r_point[0] = on_point[0];
    r_point[1] = on_point[1];
}

/*! \fn dtkRationalBezierCurve2DDataOn::evaluateNormal(double p_u, double *r_normal) const
  Writes in \a r_normal the normal evaluation at parameter \a p_u.

  \a p_u : parameter

  \a r_normal : array of size 2 to store the result of the normal evaluation
*/
void dtkRationalBezierCurve2DDataOn::evaluateNormal(double p_u, double* r_normal) const
{
    ON_3dPoint on_point;
    ON_3dVector on_normal;
    d->Ev1Der(p_u, on_point, on_normal);
    r_normal[0] = on_normal[0];
    r_normal[1] = on_normal[1];
}

/*! \fn dtkRationalBezierCurve2DDataOn::split(dtkRationalBezierCurve2D *r_split_curve_a, dtkRationalBezierCurve2D *r_split_curve_b, double p_splitting_parameter) const
  Splits at \a p_splitting_parameter the rational Bezier curve and stores in \a r_split_curve_a and \a r_split_curve_b the split parts (from 0 to \a p_splitting_parameter and from \a p_splitting_parameter to 1 respectively).

  \a r_split_curve_a : a pointer to a valid dtkRationalBezierCurve2D with data not initialized, in which the first part of the curve (from 0 to to \a p_splitting_parameter) will be stored

  \a r_split_curve_b : a pointer to a valid dtkRationalBezierCurve2D with data not initialized, in which the second part of the curve (from \a p_splitting_parameter to 1) will be stored

  \a p_splitting_parameter : the parameter at which the curve will be split
*/
void dtkRationalBezierCurve2DDataOn::split(dtkRationalBezierCurve2D *r_split_curve_a, dtkRationalBezierCurve2D *r_split_curve_b, double splitting_parameter) const
{
    if (r_split_curve_a == nullptr) {
        dtkFatal() << "The dtkRationalBezierCurve2D *r_split_curve_a points to nullptr, no memory is allocated here, it must be allocated before calling " << Q_FUNC_INFO << " with new dtkRationalBezierCurve2D().";
    }
    if (r_split_curve_b == nullptr) {
        dtkFatal() << "The dtkRationalBezierCurve2D *r_split_curve_b points to nullptr, no memory is allocated here, it must be allocated before calling " << Q_FUNC_INFO << " with new dtkRationalBezierCurve2D().";
    }
    // ///////////////////////////////////////////////////////////////////
    // Make sure that splitting parameter is between 0. and 1.
    // ///////////////////////////////////////////////////////////////////
    //TODO
    if (splitting_parameter <= 0 || splitting_parameter >=1) {
        dtkWarn() << "The curve can't be split at " << splitting_parameter << " since splitting_parameter does not belong to ]0;1[";
    }
    ON_BezierCurve on_curve_A;
    ON_BezierCurve on_curve_B;
    if(!d->Split(splitting_parameter, on_curve_A, on_curve_B)) {
        dtkFatal() << Q_FUNC_INFO << " The Bezier curve could not be splitted";
    };
    if(!d->IsValid()) {
        dtkFatal() << Q_FUNC_INFO << " Splitting messed up the curve";
    }

    dtkRationalBezierCurve2DDataOn *dtk_curve_data_A = new dtkRationalBezierCurve2DDataOn();
    dtk_curve_data_A->create(on_curve_A);
    dtkRationalBezierCurve2DDataOn *dtk_curve_data_B = new dtkRationalBezierCurve2DDataOn();
    dtk_curve_data_B->create(on_curve_B);

    r_split_curve_a->setData(static_cast< dtkAbstractRationalBezierCurve2DData * >(dtk_curve_data_A));
    r_split_curve_b->setData(static_cast< dtkAbstractRationalBezierCurve2DData * >(dtk_curve_data_B));
}

/*! \fn dtkRationalBezierCurve2DDataOn::split(std::list< dtkRationalBezierCurve2D * >& r_split_curves, const std::vector< double >& p_splitting_parameters) const
  Splits the rational Bezier curve at each value of \a p_splitting_parameters and stores in \a r_split_curves the split parts.

  \a r_split_curves : a list to which the split rational Bezier curves will be added

  \a p_splitting_parameters : the parameters at which the curve will be split
*/
void dtkRationalBezierCurve2DDataOn::split(std::list< dtkRationalBezierCurve2D * >& r_split_curves, const std::vector< double >& p_splitting_parameters) const
{
    // ///////////////////////////////////////////////////////////////////
    // Makes sure that the list r_split_curves is empty or warn that some pointed data may be lost
    // ///////////////////////////////////////////////////////////////////
    for (auto it = r_split_curves.begin(); it != r_split_curves.end(); ++it) {
        if ((*it) != nullptr) {
            dtkWarn() << "r_split_curves contains some non nullptr, the pointed data might be lost";
        }
    }
    r_split_curves.clear();
    // ///////////////////////////////////////////////////////////////////
    // Make sure that all splitting parameters are between 0. and 1.
    // ///////////////////////////////////////////////////////////////////
    //TODO
    std::vector< double > splitting_parameters = p_splitting_parameters;
    std::sort(splitting_parameters.begin(), splitting_parameters.end());
    // ///////////////////////////////////////////////////////////////////
    // Copies the current dtkRationalBezierCurve
    // ///////////////////////////////////////////////////////////////////
    ON_BezierCurve on_curve = *d;
    dtkRationalBezierCurve2DDataOn *dtk_curve_data = new dtkRationalBezierCurve2DDataOn();
    dtk_curve_data->create(on_curve);
    dtkRationalBezierCurve2D *curve = new dtkRationalBezierCurve2D(static_cast< dtkAbstractRationalBezierCurve2DData * >(dtk_curve_data));
    r_split_curves.push_back(curve);
    auto split_curve = r_split_curves.begin();
    double param = 0.;
    auto p = splitting_parameters.begin();
    while (!splitting_parameters.empty()) {
        param = *p;
        if (param <= 0. || param >= 1.) {
            splitting_parameters.erase(p);
        } else {
            dtkRationalBezierCurve2D *split_curve_a = new dtkRationalBezierCurve2D();
            dtkRationalBezierCurve2D *split_curve_b = new dtkRationalBezierCurve2D();
            (*split_curve)->split(split_curve_a, split_curve_b, *p);
            delete (*split_curve);
            split_curve = r_split_curves.erase(split_curve);
            r_split_curves.insert(split_curve, split_curve_b);
            --split_curve;
            r_split_curves.insert(split_curve, split_curve_a);
            splitting_parameters.erase(p);
            // ///////////////////////////////////////////////////////////////////
            // Recompute the remaining parameters for splitting the second of the two curves
            // ///////////////////////////////////////////////////////////////////
            for (auto rp = splitting_parameters.begin(); rp != splitting_parameters.end(); ++rp) {
                (*rp) = ((*rp) - param) / (1. - param);
            }
        }
    }
}

/*! \fn dtkRationalBezierCurve2DDataOn::aabb(double* r_aabb) const
  Writes in \a r_aabb the [xmin, ymin, xmax, ymax] coordinates.

  \a r_aabb : array of size 4 to store the limits coordinates
*/
void dtkRationalBezierCurve2DDataOn::aabb(double* r_aabb) const
{
    ON_BoundingBox bb;
    d->GetBoundingBox(bb);
    r_aabb[0] = bb[0][0];
    r_aabb[1] = bb[0][1];
    r_aabb[2] = bb[1][0];
    r_aabb[3] = bb[1][1];
}

/*! \fn dtkRationalBezierCurve2DDataOn::extendedAabb(double *r_aabb, double factor) const
  Writes in \a r_aabb the [xmin, ymin, xmax, ymax] coordinates of an extened bounding box of factor \a factor. The extension factor extends the diagonal.

  \a r_aabb : array of size 4 to store the extended limits coordinates

  \a factor : factor used to lengthen the diagonal of the axis aligned bounding box
*/
void dtkRationalBezierCurve2DDataOn::extendedAabb(double* r_aabb, double factor) const
{
    ON_BoundingBox bb;
    bb = d->BoundingBox();
    r_aabb[0] = (bb[0][0] * ( 1 + factor) + bb[1][0] * (1 - factor)) / 2;
    r_aabb[1] = (bb[0][1] * ( 1 + factor) + bb[1][1] * (1 - factor)) / 2;
    r_aabb[2] = (bb[1][0] * ( 1 + factor) + bb[0][0] * (1 - factor)) / 2;
    r_aabb[3] = (bb[1][1] * ( 1 + factor) + bb[0][1] * (1 - factor)) / 2;
}

/*! \fn dtkRationalBezierCurve2DDataOn::onRationalBezierCurve2D(void)
  Not available in the dtkAbstractRationalBezierCurve2DData.

  Returns the underlying openNURBS rational Bezier curve.
 */
ON_BezierCurve& dtkRationalBezierCurve2DDataOn::onRationalBezierCurve2D(void)
{
    return *d;
}

/*! \fn dtkRationalBezierCurve2DDataOn::clone(void) const
   Clone
*/
dtkRationalBezierCurve2DDataOn* dtkRationalBezierCurve2DDataOn::clone(void) const
{
    dtkRationalBezierCurve2DDataOn* res = new dtkRationalBezierCurve2DDataOn(*this);
    res->create(*d);
    return res;
}

//
// dtkRationalBezierCurve2DDataOn.cpp ends here
