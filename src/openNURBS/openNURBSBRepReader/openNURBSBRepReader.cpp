// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "openNURBSBRepReader.h"

#include <dtkNurbsCurve2DDataOn.h>
#include <dtkNurbsCurveDataOn.h>
#include <dtkNurbsSurfaceDataOn.h>

#include <dtkContinuousGeometry.h>

#include <dtkContinuousGeometryUtils>

#include <dtkBRep>
#include <dtkBRepData>
#include <dtkBRepReader>
#include <dtkAbstractNurbsSurfaceData>
#include <dtkNurbsSurface>
#include <dtkAbstractTrimLoopData>
#include <dtkNurbsCurve>
#include <dtkNurbsCurve2D>
#include <dtkTrim>
#include <dtkTrimLoop>
#include <dtkTopoTrim>

#include <opennurbs_without_warnings.h>

//--------------------------------------------------------------
#include <dtkRationalBezierCurve>

#include <sisl.h>

#include <iomanip>

#define LINEAR_
//--------------------------------------------------------------

// /////////////////////////////////////////////////////////////////
// openNURBSBRepReaderPrivate
// /////////////////////////////////////////////////////////////////

class openNURBSBRepReaderPrivate
{
public:
    QString in_brep_file_path;
    QString in_dump_file_path;

    dtkBRep* dtk_brep;
};

// ///////////////////////////////////////////////////////////////////
// openNURBSBRepReader implementation
// ///////////////////////////////////////////////////////////////////

openNURBSBRepReader::openNURBSBRepReader(void) : dtkBRepReader(), d(new openNURBSBRepReaderPrivate)
{
    d->dtk_brep = nullptr;
}

openNURBSBRepReader::~openNURBSBRepReader(void)
{
    delete d;
    d = nullptr;
}

void openNURBSBRepReader::setInputBRepFilePath(const QString& file)
{
    d->in_brep_file_path = file;
}

void openNURBSBRepReader::setInputDumpFilePath(const QString& file)
{
    d->in_dump_file_path = file;
}

void openNURBSBRepReader::run(void)
{
	// Call once in your application to initialize opennurbs library
	ON::Begin();
	// default dump is to stdout
	FILE* dump_fp = 0;

	std::unique_ptr<ONX_Model> onx_model = std::make_unique<ONX_Model>();
	bool bVerboseTextDump = false;
	FILE* text_fp = ON::OpenFile(d->in_dump_file_path.toStdString().c_str(),"w");
	if ( text_fp ){
		if ( dump_fp ){
			ON::CloseFile(dump_fp);
		}
	}
	dump_fp = text_fp;
	ON_TextLog dump(dump_fp);

	// open file containing opennurbs archive
    ON_wString ws_arg = d->in_brep_file_path.toStdString().c_str();
    const wchar_t* wchar_arg = ws_arg;
	FILE* to_load = ON::OpenFile(wchar_arg, L"rb");
	if ( NULL==to_load ){
		dtkFatal()  <<"  Unable to open file.";
		ON::CloseFile(to_load);
		return;
	}

	// create archive object from file pointer
	ON_BinaryFile archive( ON::read3dm, to_load );

	// read the contents of the file into "model"
    //Loads the data in the ONX_Model
	bool rc = onx_model->Read( archive, &dump );

	// close the file
	ON::CloseFile( to_load );

	// print diagnostic
	if ( rc ){
		dump.Print("Successfully read.\n");
	}
	else{
		dump.Print("Errors during reading.\n");
	}

	// see if everything is in good shape
	if ( onx_model->IsValid(&dump) ){
		dump.Print("Model is valid.\n");
	}
	else{
		dump.Print("Model is not valid.\n");
	}
	// create a text dump of the model
	if ( bVerboseTextDump ){
		dump.PushIndent();
		onx_model->Dump(dump);
		dump.PopIndent();
	}

	dump.PopIndent();

	if ( dump_fp ){
		// close the text dump file
        //delete dump;
		ON::CloseFile( dump_fp );
	}

    dtkBRepData* dtk_brep_data = new dtkBRepData();
    d->dtk_brep = new dtkBRep(dtk_brep_data);

    int num_objects = onx_model->m_object_table.Count();
    num_objects = 1;
    for (int i = 0; i < num_objects; ++i) {
    // ///////////////////////////////////////////////////////////////////
    // Considers that the onx_model is made of a single BRep
    // It recovers only the first Brep object
    // ///////////////////////////////////////////////////////////////////
    //ON_Brep* on_brep = const_cast< ON_Brep* >(ON_Brep::Cast(onx_model->m_object_table[0].m_object));
    ON_Brep* on_brep = const_cast< ON_Brep* >(ON_Brep::Cast(onx_model->m_object_table[i].m_object));
    if (on_brep == nullptr) {
        dtkFatal() << "No Brep could be retrieved from this model";
    }

    on_brep->Standardize();
    on_brep->Compact();

    // ///////////////////////////////////////////////////////////////////
    // Recovers the Axis Aligned Bounding Box
    // ///////////////////////////////////////////////////////////////////
    double bbox[6];
    //on_brep->GetBBox(&(dtk_brep_data->m_aabb[0]), &(dtk_brep_data->m_aabb[3]), false);
    on_brep->GetBBox(&bbox[0], &bbox[3], false);

    if (i == 0) {
      for (int ii = 0; ii < 6; ++ii) dtk_brep_data->m_aabb[ii] = bbox[ii];
    }
    else {
      for (int ii = 0; ii < 3; ++ii) {
        if (dtk_brep_data->m_aabb[ii] > bbox[ii]) dtk_brep_data->m_aabb[ii] = bbox[ii];
        if (dtk_brep_data->m_aabb[ii + 3] < bbox[ii + 3]) dtk_brep_data->m_aabb[ii + 3] = bbox[ii + 3];
      }
    }

    // ///////////////////////////////////////////////////////////////////
    // Creates a map to keep track of which surface is linked to which trim and vice-versa
    // ///////////////////////////////////////////////////////////////////
    std::unordered_map< const ON_Curve*, dtkTopoTrim* > trims_map;
    std::unordered_map< const ON_Curve*, std::vector<dtkContinuousGeometryPrimitives::Point_3> > map_curve_to_polyline;
    // ///////////////////////////////////////////////////////////////////
    // Iterates on the ON_BrepFace to be able to recover the trims
    // ///////////////////////////////////////////////////////////////////
    std::cout << "number of faces: " << on_brep->m_F.Count() << std::endl;
    for (int i = 0; i < on_brep->m_F.Count(); ++i) {
      std::cout << "face " << i << std::endl;
        ON_BrepFace* face = &on_brep->m_F[i];//m_F[i] returns a reference to the face...
        ON_Surface* surface = const_cast< ON_Surface* >(face->SurfaceOf());
        ON_NurbsSurface* nurbs_surface = ON_NurbsSurface::Cast(surface);
        bool nurbs_surface_needs_delete = false;
        if (nurbs_surface == nullptr) {
            nurbs_surface = ON_NurbsSurface::New();
            nurbs_surface_needs_delete = true;
            bool converted = surface->GetNurbForm(*nurbs_surface);
            if(converted == false) {
                delete nurbs_surface;
                dtkFatal() << "One of the surfaces could not be converted to a Nurbs surface.";
            }
        } else {
            nurbs_surface = ON_NurbsSurface::Cast(surface);

        }

        if (!nurbs_surface->IsRational()) {
            nurbs_surface->MakeRational();
        }
        dtkNurbsSurfaceDataOn* dtk_nurbs_surface_data_on = new dtkNurbsSurfaceDataOn();
        dtkAbstractNurbsSurfaceData* dtk_nurbs_surface_data = dynamic_cast< dtkAbstractNurbsSurfaceData* >(dtk_nurbs_surface_data_on);
        dtkNurbsSurface* dtk_nurbs_surface = new dtkNurbsSurface(dtk_nurbs_surface_data);
        dtk_nurbs_surface->setSurfaceIndex(face->m_face_index);
        dtk_brep_data->m_nurbs_surfaces.push_back(dtk_nurbs_surface);

        dtk_nurbs_surface_data_on->create(*nurbs_surface);

        //-----------------------------------------------------------------------------------------------------------------
#ifdef LINEAR_
        //std::cout << "u domain: " << std::endl;
        //std::cout << nurbs_surface->Knot(0, 0) << ", " << nurbs_surface->Knot(0, nurbs_surface->KnotCount(0) - 1) << std::endl;
        //std::cout << "v domain: " << std::endl;
        //std::cout << nurbs_surface->Knot(1, 0) << ", " << nurbs_surface->Knot(1, nurbs_surface->KnotCount(1) - 1) << std::endl;

        // split nurbs surface
        std::vector<ON_NurbsSurface*> nurbs_surfaces_split;
        if (nurbs_surface->IsClosed(0)) {
          std::cout << "face is closed in u direction" << std::endl;
          ON_Surface* west_side = 0;
          ON_Surface* east_side = 0;
          surface->Split(0, surface->Domain(0).Mid(), west_side, east_side);

          if (nurbs_surface->IsClosed(1)) {
            std::cout << "face is closed in v direction" << std::endl;
            ON_Surface* south_west_side = 0;
            ON_Surface* north_west_side = 0;
            west_side->Split(1, west_side->Domain(1).Mid(), south_west_side, north_west_side);

            ON_Surface* south_east_side = 0;
            ON_Surface* north_east_side = 0;
            east_side->Split(1, east_side->Domain(1).Mid(), south_east_side, north_east_side);

            ON_NurbsSurface* south_west_nurbs_surface = nullptr;
            cast_nurbs_surface(south_west_side, south_west_nurbs_surface);
            nurbs_surfaces_split.push_back(south_west_nurbs_surface);

            ON_NurbsSurface* north_west_nurbs_surface = nullptr;
            cast_nurbs_surface(north_west_side, north_west_nurbs_surface);
            nurbs_surfaces_split.push_back(north_west_nurbs_surface);

            ON_NurbsSurface* south_east_nurbs_surface = nullptr;
            cast_nurbs_surface(south_east_side, south_east_nurbs_surface);
            nurbs_surfaces_split.push_back(south_east_nurbs_surface);

            ON_NurbsSurface* north_east_nurbs_surface = nullptr;
            cast_nurbs_surface(north_east_side, north_east_nurbs_surface);
            nurbs_surfaces_split.push_back(north_east_nurbs_surface);
          }
          else {
            ON_NurbsSurface* west_nurbs_surface = nullptr;
            cast_nurbs_surface(west_side, west_nurbs_surface);
            nurbs_surfaces_split.push_back(west_nurbs_surface);

            ON_NurbsSurface* east_nurbs_surface = nullptr;
            cast_nurbs_surface(east_side, east_nurbs_surface);
            nurbs_surfaces_split.push_back(east_nurbs_surface);
          }
        }
        else {
          if (nurbs_surface->IsClosed(1)) {
            std::cout << "face is closed in v direction" << std::endl;
            ON_Surface* south_side = 0;
            ON_Surface* north_side = 0;
            surface->Split(1, surface->Domain(1).Mid(), south_side, north_side);

            ON_NurbsSurface* south_nurbs_surface = nullptr;
            cast_nurbs_surface(south_side, south_nurbs_surface);
            nurbs_surfaces_split.push_back(south_nurbs_surface);

            ON_NurbsSurface* north_nurbs_surface = nullptr;
            cast_nurbs_surface(north_side, north_nurbs_surface);
            nurbs_surfaces_split.push_back(north_nurbs_surface);
          }
          else {
            nurbs_surfaces_split.push_back(nurbs_surface);
          }
        }

        //-----------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------
        // convert to SISL surface
        std::vector<SISLSurf*> surfs_sisl_split;
        for (auto surf_split : nurbs_surfaces_split) {
          SISLSurf* surf_split_sisl;
          convert_nurbs_surface_to_sisl(surf_split, surf_split_sisl);
          surfs_sisl_split.push_back(surf_split_sisl);
        }

        if (nurbs_surfaces_split.size() > 1) { // consider the complete nurbs surface as well, in the closest point computation
          SISLSurf* surf_sisl;
          convert_nurbs_surface_to_sisl(nurbs_surface, surf_sisl);
          surfs_sisl_split.push_back(surf_sisl);
        }

        //--------------------------------------------------------------------------------
#endif
        dtkInfo() << "number of loops in face : " << face->m_li.Count();
        // ///////////////////////////////////////////////////////////////////
        // Loops over the ON trim loop of the ON face
        // ///////////////////////////////////////////////////////////////////
        //outer loop is m_li[0]
        for(int k = 0; k < face->m_li.Count(); ++k) {
            // ///////////////////////////////////////////////////////////////////
            // Recovers the ON_BrepLoop from the ON_BrepFace
            // ///////////////////////////////////////////////////////////////////
            ON_BrepLoop* loop = face->Loop(k);
            ///////////////////////////////////////////////////////////////////
            // Creates a dtkTrimLoop to be passed to a dtkNurbsSurfaceDataOn
            ///////////////////////////////////////////////////////////////////
#ifdef LINEAR_
            dtkAbstractTrimLoopData* trim_loop_data = dtkContinuousGeometry::abstractTrimLoopData::pluginFactory().create("dtkTrimLoopDataCgal");
#else
            dtkAbstractTrimLoopData* trim_loop_data = dtkContinuousGeometry::abstractTrimLoopData::pluginFactory().create("dtkTrimLoopDataNp");
#endif
            if (trim_loop_data == nullptr) {
                dtkFatal() << "In : " << Q_FUNC_INFO << " The dtkAbstractTrimLoopData using cgal implementation could not be created. Check that the cgal plugin is compiled.";
            }
            dtkTrimLoop* trim_loop = new dtkTrimLoop(trim_loop_data);
            trim_loop->setTrimLoopIndex(loop->m_loop_index);

            if (loop->m_type == ON_BrepLoop::TYPE::inner) { // clockwise orientation
                trim_loop->setType(dtkContinuousGeometryEnums::TrimLoopType::outer);
            } else if (loop->m_type == ON_BrepLoop::TYPE::outer){ // counter-clockwise orientation
                trim_loop->setType(dtkContinuousGeometryEnums::TrimLoopType::inner);
            } else {
                dtkWarn() << "A trim loop of the BRep model is of unknown type";
                trim_loop->setType(dtkContinuousGeometryEnums::TrimLoopType::unknown);
            }

            std::cout << "number of trims: " << loop->m_ti.Count() << std::endl;

            // check if the trim loop is closed
            bool is_loop_valid = check_loop_valid(loop, nurbs_surface, on_brep);
#ifdef LINEAR_
            // computer parameters of trim ends
            std::vector<K::Point_2> parameters_loop;
            loop_end_parameters(loop, surfs_sisl_split, parameters_loop);

            //std::cout << "loop trim end parameters: " << std::endl;
            //for (auto param : parameters_loop) {
            //  std::cout << param[0] << "," << param[1] << "," << 0 << std::endl;
            //}
            //std::cout << std::endl;
#endif
            for (int l = 0; l < loop->m_ti.Count(); ++l) {
                std::cout << "trim " << l << std::endl;
                // ///////////////////////////////////////////////////////////////////
                // Recovers the ON_BrepTrim from the ON_BrepLoop
                // ///////////////////////////////////////////////////////////////////
                ON_BrepTrim *on_trim = loop->Trim(l);

                std::cout << "trim index: " << on_trim->m_trim_index << std::endl;
                std::cout << "trim type: " << on_trim->m_type << std::endl;

                // ///////////////////////////////////////////////////////////////////
                // Retrieves the trim curve in the parameter space
                // ///////////////////////////////////////////////////////////////////
                ON_Curve *on_curve = const_cast< ON_Curve * >(on_trim->TrimCurveOf());
                ON_NurbsCurve *nurbs_curve = ON_NurbsCurve::Cast(on_curve);
                // ///////////////////////////////////////////////////////////////////
                // If the trim is not a NURBS curve, convert it if possible
                // ///////////////////////////////////////////////////////////////////
                if (nurbs_curve == nullptr) {
                    bool converted = on_curve->GetNurbForm(*nurbs_curve);
                    if (converted == false) {
                        dtkFatal() << "In : " << Q_FUNC_INFO <<" : One of the trim could not be converted to a NURBS curve";
                    }
                }
                if (!nurbs_curve->IsRational()) {
                    nurbs_curve->MakeRational();
                }
                dtkNurbsCurve2DDataOn *dtk_nurbs_curve_2d_data_on = new dtkNurbsCurve2DDataOn();
                dtk_nurbs_curve_2d_data_on->create(*nurbs_curve);
                dtkNurbsCurve2D *dtk_nurbs_curve = new dtkNurbsCurve2D(static_cast< dtkAbstractNurbsCurve2DData * >(dtk_nurbs_curve_2d_data_on));
                dtk_brep_data->m_nurbs_curves_2d.push_back(dtk_nurbs_curve);
                // ///////////////////////////////////////////////////////////////////
                // Checks if the trim has already been processed
                // ///////////////////////////////////////////////////////////////////
                std::unordered_map< const ON_Curve *, dtkTopoTrim * >::iterator t = trims_map.find(on_trim->EdgeCurveOf());
                // ///////////////////////////////////////////////////////////////////
                // If the edge has not yet been processed
                // ///////////////////////////////////////////////////////////////////
                if (t == trims_map.end()) {
                  std::cout << "not visited" << std::endl;
                    ///////////////////////////////////////////////////////////////////
                    // Creates a dtkTopoTrim and associate a 3d curve converted from on_curve
                    ///////////////////////////////////////////////////////////////////
                    dtkTopoTrim *dtk_topo_trim = new dtkTopoTrim();
                    dtk_topo_trim->setTrimIndex(on_trim->m_trim_index);
                    ON_Curve *on_edge = const_cast< ON_Curve * >(on_trim->EdgeCurveOf());
                    // ///////////////////////////////////////////////////////////////////
                    // Checks that there is a 3d curve
                    // ///////////////////////////////////////////////////////////////////
                    if (on_edge != nullptr) {
                        ON_NurbsCurve *nurbs_edge = ON_NurbsCurve::Cast(on_edge);
                        // ///////////////////////////////////////////////////////////////////
                        // If the edge is not a NURBS curve, convert it if possible
                        // ///////////////////////////////////////////////////////////////////
                        bool need_delete = false;
                        if (nurbs_edge == nullptr) {
                            /* Needs to allocate some memory, it is not done in the method GetNurbForm */
                            nurbs_edge = new ON_NurbsCurve();
                            bool converted = on_edge->GetNurbForm(*nurbs_edge);
                            if (converted == false) {
                                delete nurbs_edge;
                                dtkFatal() << "In : " << Q_FUNC_INFO <<" : One of the trim could not be converted to a NURBS curve";
                                return;
                            }
                            need_delete = true;
                        }
                        if (!nurbs_edge->IsRational()) {
                            nurbs_edge->MakeRational();
                        }
#ifdef LINEAR_
                        //-------------------------------------------------------------------
                        double bbox_min[3], bbox_max[3];
                        nurbs_edge->GetBBox(bbox_min, bbox_max);

                        double approx_tol = std::sqrt( std::pow(bbox_max[0] - bbox_min[0], 2.) +
                                                       std::pow(bbox_max[1] - bbox_min[1], 2.) +
                                                       std::pow(bbox_max[2] - bbox_min[2], 2.) ) / 1.e3;

                        // 3D curve discretization
                        std::vector< dtkContinuousGeometryPrimitives::Point_3 > polyline;
                        trimming_curve_approx(nurbs_edge, polyline);

                        map_curve_to_polyline.insert(std::make_pair(on_trim->EdgeCurveOf(), polyline));

                        //std::cout << "number of polyline points: " << polyline.size() << std::endl;

                        // find closest points in parametric domain
                        std::vector< dtkContinuousGeometryPrimitives::Point_2 > params;
                        params.reserve(polyline.size());

                        std::vector<K::Point_2> params_end;
                        params_end.reserve(2); // parameters of the two end vertices of the trim
if (parameters_loop.size() == loop->m_ti.Count()) {
                        if (l < loop->m_ti.Count() - 1) {
                          params_end.push_back(parameters_loop[l]);
                          params_end.push_back(parameters_loop[l + 1]);
                        }
                        else {
                          params_end.push_back(parameters_loop[l]);
                          params_end.push_back(parameters_loop[0]);
                        }
}
                        if (is_loop_valid) parametric_polyline_beta(surfs_sisl_split, polyline, params, nurbs_surface, nurbs_curve, on_trim->m_bRev3d, approx_tol);
                        else parametric_polyline_beta2(surfs_sisl_split, polyline, params, nurbs_surface, params_end, on_trim->m_bRev3d, approx_tol);
                        //else parametric_polyline_beta(surfs_sisl_split, polyline, params, nurbs_surface, nurbs_curve, on_trim->m_bRev3d, approx_tol);

                        if (on_trim->m_bRev3d) std::reverse(params.begin(), params.end());

                        //std::cout << "parameters: " << std::endl;
                        //for (auto param : params) {
                        //  std::cout << param[0] << " " << param[1] << " " << 0 << std::endl;
                        //}
                        //std::cout << std::endl;

                        // create new dtk trim
                        std::vector<double> params_dtk(params.size()*3);
                        for (std::size_t ii = 0; ii < params.size(); ++ii) {
                          params_dtk[3*ii] = params[ii][0];
                          params_dtk[3*ii + 1] = params[ii][1];
                          params_dtk[3*ii + 2] = 1.;
                        }

                        std::vector<double> knots_dtk(params.size());
                        for (int ii = 0; ii < 1; ++ii) {
                          knots_dtk[ii] = 0.;
                          knots_dtk[ii + params.size() - 1] = 1.;
                        }
                        for (std::size_t ii = 1; ii < params.size() - 1; ++ii) {
                          knots_dtk[ii] = double(ii)/double(params.size() - 1);
                        }

                        dtkNurbsCurve2DDataOn* dtk_nurbs_curve_2d_data_on_new = new dtkNurbsCurve2DDataOn();
                        dtk_nurbs_curve_2d_data_on_new->create(params.size(), 2, knots_dtk.data(), params_dtk.data());
                        dtkNurbsCurve2D* dtk_nurbs_curve_new = new dtkNurbsCurve2D(static_cast<dtkAbstractNurbsCurve2DData*>(dtk_nurbs_curve_2d_data_on_new));

                        dtk_nurbs_curve = dtk_nurbs_curve_new;
                        dtk_brep_data->m_nurbs_curves_2d.push_back(dtk_nurbs_curve);

                        std::vector<dtkContinuousGeometryPrimitives::Point_2> params_tmp;
                        std::vector<dtkContinuousGeometryPrimitives::Point_3> points_tmp;
                        if (on_trim->IsSeam()) std::cout << "Is a seam!" << std::endl;

                        for (std::size_t i = 0; i < dtk_nurbs_curve->nbCps(); ++i) {
                          double param[2];
                          dtk_nurbs_curve->controlPoint(i, param);
                          params_tmp.push_back( dtkContinuousGeometryPrimitives::Point_2(param[0], param[1]) );

                          double point[3];
                          dtk_nurbs_surface->evaluatePoint(param[0], param[1], point);
                          points_tmp.push_back( dtkContinuousGeometryPrimitives::Point_3(point[0], point[1], point[2]) );
                        }

                        //-------------------------------------------------------------------
#endif
                        dtkNurbsCurveDataOn* dtk_nurbs_curve_data_on = new dtkNurbsCurveDataOn();
                        dtk_nurbs_curve_data_on->create(*nurbs_edge);
                        dtk_topo_trim->m_nurbs_curve_3d = new dtkNurbsCurve(static_cast< dtkAbstractNurbsCurveData * >(dtk_nurbs_curve_data_on));
                        // Bezier decomposition
                        dtk_topo_trim->decomposeToRationalBezierCurves();
                        if(need_delete)
                          delete nurbs_edge;
                    }

                    else std::cout << "the trim is not an edge" << std::endl;
#ifndef LINEAR_
                    //----------------------------------------------------------------------------------------------------
                    if (on_trim->IsSeam()) std::cout << "Is a seam!" << std::endl;
                    /*
                    int num_knots_nurbs_curve = dtk_nurbs_curve->nbCps() + dtk_nurbs_curve->degree() - 1;
                    double knots_nurbs_curve[num_knots_nurbs_curve];
                    dtk_nurbs_curve->knots(knots_nurbs_curve);
                    for (double i = 0.; i < 1.01; i += 0.1) {
                      double t = knots_nurbs_curve[0] + i*(knots_nurbs_curve[num_knots_nurbs_curve - 1] - knots_nurbs_curve[0]);
                      double param[2];
                      dtk_nurbs_curve->evaluatePoint(t, param);
                      std::cout << param[0] << " " << param[1] << " " << 0 << std::endl;
                    }
                    std::cout << std::endl;
                    */

                    std::vector<dtkContinuousGeometryPrimitives::Point_2> params_tmp;
                    std::vector<dtkContinuousGeometryPrimitives::Point_3> points_tmp;
                    std::cout << "number of control points: " << dtk_nurbs_curve->nbCps() << std::endl;
                    std::cout << "degree: " << dtk_nurbs_curve->degree() << std::endl;
                    for (int i = 0; i < dtk_nurbs_curve->nbCps(); ++i) {
                      double param[2];
                      dtk_nurbs_curve->controlPoint(i, param);
                      params_tmp.push_back( dtkContinuousGeometryPrimitives::Point_2(param[0], param[1]) );

                      double point[3];
                      dtk_nurbs_surface->evaluatePoint(param[0], param[1], point);
                      points_tmp.push_back( dtkContinuousGeometryPrimitives::Point_3(point[0], point[1], point[2]) );
                    }

                    for (int i = 0; i < params_tmp.size() - 1; ++i) {
                      std::cout << 2 << " ";
                      std::cout << params_tmp[i][0] << " " << params_tmp[i][1] << " " << 0 << " ";
                      std::cout << params_tmp[i + 1][0] << " " << params_tmp[i + 1][1] << " " << 0 << std::endl;

                      //std::cout << points_tmp[i][0] << " " << points_tmp[i][1] << " " << points_tmp[i][2] << " ";
                      //std::cout << points_tmp[i + 1][0] << " " << points_tmp[i + 1][1] << " " << points_tmp[i + 1][2] << std::endl;
                    }

                    std::cout << std::endl;

                    //----------------------------------------------------------------------------------------------------
#endif
                    // ///////////////////////////////////////////////////////////////////
                    //  Adds it to the list of dtkTopoTrims (for later on deletion)
                    // ///////////////////////////////////////////////////////////////////
                    dtk_brep_data->m_topo_trims.push_back(dtk_topo_trim);
                    // ///////////////////////////////////////////////////////////////////
                    // Insert it into the map to find it later on
                    // ///////////////////////////////////////////////////////////////////
                    trims_map.insert(std::make_pair(on_trim->EdgeCurveOf(), dtk_topo_trim));
                    // ///////////////////////////////////////////////////////////////////
                    // Creates a dtkTrim
                    // ///////////////////////////////////////////////////////////////////
                    dtkTrim* dtk_trim = nullptr;
                    if (on_trim->IsSeam()) {
                        dtk_trim = new dtkTrim(*dtk_nurbs_surface, *dtk_nurbs_curve, dtk_topo_trim, true);
                    } else {
                        dtk_trim = new dtkTrim(*dtk_nurbs_surface, *dtk_nurbs_curve, dtk_topo_trim, false);
                    }
                    // ///////////////////////////////////////////////////////////////////
                    // Adds the trim to the trim loop
                    // ///////////////////////////////////////////////////////////////////
                    trim_loop->trims().push_back(dtk_trim);
                } else {
                  std::cout << "visited" << std::endl;
#ifdef LINEAR_
                  ON_Curve *on_edge = const_cast< ON_Curve * >(on_trim->EdgeCurveOf());

                  if (on_edge != nullptr) {

                  double bbox_min[3], bbox_max[3];
                  (on_trim->EdgeCurveOf())->GetBBox(bbox_min, bbox_max);

                  double approx_tol = std::sqrt( std::pow(bbox_max[0] - bbox_min[0], 2.) +
                                                 std::pow(bbox_max[1] - bbox_min[1], 2.) +
                                                 std::pow(bbox_max[2] - bbox_min[2], 2.) ) / 1.e3;

                  // find closest points in parametric domain
                    std::vector< dtkContinuousGeometryPrimitives::Point_2 > params;
                    params.reserve(map_curve_to_polyline[on_trim->EdgeCurveOf()].size());

                    std::vector<K::Point_2> params_end;
                    params_end.reserve(2); // parameters of the two end vertices of the trim
if (parameters_loop.size() == loop->m_ti.Count()) {
                    if (l < loop->m_ti.Count() - 1) {
                      params_end.push_back(parameters_loop[l]);
                      params_end.push_back(parameters_loop[l + 1]);
                    }
                    else {
                      params_end.push_back(parameters_loop[l]);
                      params_end.push_back(parameters_loop[0]);
                    }
}
                    if (is_loop_valid) parametric_polyline_beta(surfs_sisl_split, map_curve_to_polyline[on_trim->EdgeCurveOf()], params, nurbs_surface, nurbs_curve, on_trim->m_bRev3d, approx_tol);
                    else parametric_polyline_beta2(surfs_sisl_split, map_curve_to_polyline[on_trim->EdgeCurveOf()], params, nurbs_surface, params_end, on_trim->m_bRev3d, approx_tol);
                    //else parametric_polyline_beta(surfs_sisl_split, map_curve_to_polyline[on_trim->EdgeCurveOf()], params, nurbs_surface, nurbs_curve, on_trim->m_bRev3d, approx_tol);
                    if (on_trim->m_bRev3d) std::reverse(params.begin(), params.end());

                    // create new dtk trim
                    std::vector<double> params_dtk(params.size()*3);
                    for (std::size_t ii = 0; ii < params.size(); ++ii) {
                      params_dtk[3*ii] = params[ii][0];
                      params_dtk[3*ii + 1] = params[ii][1];
                      params_dtk[3*ii + 2] = 1.;
                    }

                    std::vector<double> knots_dtk(params.size());
                    for (int ii = 0; ii < 1; ++ii) {
                      knots_dtk[ii] = 0.;
                      knots_dtk[ii + params.size() - 1] = 1.;
                    }
                    for (std::size_t ii = 1; ii < params.size() - 1; ++ii) {
                      knots_dtk[ii] = double(ii)/double(params.size() - 1);
                    }

                    dtkNurbsCurve2DDataOn* dtk_nurbs_curve_2d_data_on_new = new dtkNurbsCurve2DDataOn();
                    dtk_nurbs_curve_2d_data_on_new->create(params.size(), 2, knots_dtk.data(), params_dtk.data());
                    dtkNurbsCurve2D* dtk_nurbs_curve_new = new dtkNurbsCurve2D(static_cast<dtkAbstractNurbsCurve2DData*>(dtk_nurbs_curve_2d_data_on_new));

                    dtk_nurbs_curve = dtk_nurbs_curve_new;
                    dtk_brep_data->m_nurbs_curves_2d.push_back(dtk_nurbs_curve);

                    std::vector<dtkContinuousGeometryPrimitives::Point_2> params_tmp;
                    std::vector<dtkContinuousGeometryPrimitives::Point_3> points_tmp;
                    if (on_trim->IsSeam()) std::cout << "Is a seam!" << std::endl;

                    for (std::size_t i = 0; i < dtk_nurbs_curve->nbCps(); ++i) {
                      double param[2];
                      dtk_nurbs_curve->controlPoint(i, param);
                      params_tmp.push_back( dtkContinuousGeometryPrimitives::Point_2(param[0], param[1]) );

                      double point[3];
                      dtk_nurbs_surface->evaluatePoint(param[0], param[1], point);
                      points_tmp.push_back( dtkContinuousGeometryPrimitives::Point_3(point[0], point[1], point[2]) );
                    }

                  }
#endif
#ifndef LINEAR_
                  //----------------------------------------------------------------------------------------------------
                  if (on_trim->IsSeam()) std::cout << "Is a seam!" << std::endl;
                  /*
                  int num_knots_nurbs_curve = dtk_nurbs_curve->nbCps() + dtk_nurbs_curve->degree() - 1;
                  double knots_nurbs_curve[num_knots_nurbs_curve];
                  dtk_nurbs_curve->knots(knots_nurbs_curve);
                  for (double i = 0.; i < 1.01; i += 0.1) {
                    double t = knots_nurbs_curve[0] + i*(knots_nurbs_curve[num_knots_nurbs_curve - 1] - knots_nurbs_curve[0]);
                    double param[2];
                    dtk_nurbs_curve->evaluatePoint(t, param);
                    std::cout << param[0] << " " << param[1] << " " << 0 << std::endl;
                  }
                  std::cout << std::endl;
                  */

                  std::vector<dtkContinuousGeometryPrimitives::Point_2> params_tmp;
                  std::vector<dtkContinuousGeometryPrimitives::Point_3> points_tmp;
                  for (int i = 0; i < dtk_nurbs_curve->nbCps(); ++i) {
                    double param[2];
                    dtk_nurbs_curve->controlPoint(i, param);
                    params_tmp.push_back( dtkContinuousGeometryPrimitives::Point_2(param[0], param[1]) );

                    double point[3];
                    dtk_nurbs_surface->evaluatePoint(param[0], param[1], point);
                    points_tmp.push_back( dtkContinuousGeometryPrimitives::Point_3(point[0], point[1], point[2]) );
                  }

                  for (int i = 0; i < params_tmp.size() - 1; ++i) {
                    std::cout << 2 << " ";
                    std::cout << params_tmp[i][0] << " " << params_tmp[i][1] << " " << 0 << " ";
                    std::cout << params_tmp[i + 1][0] << " " << params_tmp[i + 1][1] << " " << 0 << std::endl;

                    //std::cout << points_tmp[i][0] << " " << points_tmp[i][1] << " " << points_tmp[i][2] << " ";
                    //std::cout << points_tmp[i + 1][0] << " " << points_tmp[i + 1][1] << " " << points_tmp[i + 1][2] << std::endl;
                  }

                  std::cout << std::endl;

                  //----------------------------------------------------------------------------------------------------
#endif
                    // ///////////////////////////////////////////////////////////////////
                    // Creates a dtkTrim
                    // ///////////////////////////////////////////////////////////////////
                    dtkTrim* dtk_trim = nullptr;
                    if (on_trim->IsSeam()) {
                        dtk_trim = new dtkTrim(*dtk_nurbs_surface, *dtk_nurbs_curve, t->second, true);
                    } else {
                        dtk_trim = new dtkTrim(*dtk_nurbs_surface, *dtk_nurbs_curve, t->second, false);
                    }
                    // ///////////////////////////////////////////////////////////////////
                    // Adds the trim to the trim loop
                    // ///////////////////////////////////////////////////////////////////
                    trim_loop->trims().push_back(dtk_trim);
                }
            }

            if (!trim_loop->isValid()) {
                dtkError() << "In : " << Q_FUNC_INFO << " One of the trim loops is not valid ";
            }

#ifdef LINEAR_
            // check if the trim loop is self-intersected
            CGAL::Polygon_2<K> polygon_cgal;
            const std::vector< dtkTrim * >& trims = trim_loop->trims();
            for (auto trim : trims) {
              const dtkNurbsCurve2D& curve_2d = trim->curve2D();
              for (std::size_t i = 0; i < curve_2d.nbCps() - 1; ++i) {
                double cp[2];
                curve_2d.controlPoint(i, cp);
                polygon_cgal.push_back( K::Point_2(cp[0], cp[1]) );
              }
            }

            if (!polygon_cgal.is_simple()) {
              std::cout << "ERROR: 2D polygon is NOT simple!" << std::endl;

              CGAL::Bbox_2 bbox_polygon = polygon_cgal.bbox();
              double min_edge_bbox = (std::min)( bbox_polygon.xmax() - bbox_polygon.xmin(),
                                                 bbox_polygon.ymax() - bbox_polygon.ymin() );

              polygon_cgal.clear();
              // the first trim
              for (std::size_t i = 0; i < (trims[0]->curve2D()).nbCps(); ++i) {
                double cp[2];
                (trims[0]->curve2D()).controlPoint(i, cp);

                if (i > 0) {
                  K::Point_2 cp_prev = polygon_cgal[polygon_cgal.size() - 1];
                  double dist = std::sqrt( std::pow(cp[0] - cp_prev[0], 2.) + std::pow(cp[1] - cp_prev[1], 2.) );
                  if (dist < min_edge_bbox/1000.) continue;
                }

                polygon_cgal.push_back( K::Point_2(cp[0], cp[1]) );
              }

              // other trims, check self-intersection of each segment
              for (std::size_t i = 1; i < trims.size(); ++i) {
                const dtkNurbsCurve2D& curve_2d = trims[i]->curve2D();
                if (curve_2d.nbCps() > 2) { // exclude the case of only one line segment
                  CGAL::Polygon_2<K> polygon_trim;
                  bool is_intersected = false;
                  for (std::size_t ii = 1; ii < curve_2d.nbCps() - 1; ++ii) {

                    if (ii == curve_2d.nbCps() - 2 && i == trims.size() - 1) continue;

                    double cp1[2], cp2[2];
                    if (ii == 1) {
                      curve_2d.controlPoint(ii, cp1);
                    }
                    else {
                      cp1[0] = polygon_trim[polygon_trim.size() - 1][0];
                      cp1[1] = polygon_trim[polygon_trim.size() - 1][1];
                    }

                    curve_2d.controlPoint(ii + 1, cp2);

                    K::Point_2 r(cp1[0], cp1[1]);
                    K::Point_2 s(cp2[0], cp2[1]);

                    for (std::size_t jj = 0; jj < polygon_cgal.size() - 1; ++jj) {
                      K::Point_2 p = polygon_cgal[jj];
                      K::Point_2 q = polygon_cgal[jj + 1];

                      if ( do_intersect(p, q, r, s) ) {
                        is_intersected = true;

                        K::Vector_2 pq(p, q);
                        double length = std::sqrt(pq.squared_length());
                        pq /= length;

                        K::Vector_2 ps(p, s);

                        double proj = ps[0]*pq[0] + ps[1]*pq[1];

                        double dx = 2.*proj*pq[0] - ps[0];
                        double dy = 2.*proj*pq[1] - ps[1];

                        K::Point_2 s2(p[0] + dx, p[1] + dy);

                        s = s2;
                      }
                    }

                    if (ii > 1) polygon_trim.erase( polygon_trim.end() - 1 );

                    polygon_trim.push_back(r);
                    polygon_trim.push_back(s);

                  }

                  if (is_intersected) {
                    // add points to make the polygon complete
                    double cp[2];
                    curve_2d.controlPoint(0, cp);
                    polygon_trim.insert( polygon_trim.begin(), K::Point_2(cp[0], cp[1]) );

                    if (i == trims.size() - 1) {
                      curve_2d.controlPoint(curve_2d.nbCps() - 1, cp);
                      polygon_trim.push_back( K::Point_2(cp[0], cp[1]) );
                    }

                    // create a new dtkTrim
                    std::vector<double> params_new(polygon_trim.size()*3);
                    for (std::size_t ii = 0; ii < polygon_trim.size(); ++ii) {
                      params_new[3*ii] = polygon_trim[ii][0];
                      params_new[3*ii + 1] = polygon_trim[ii][1];
                      params_new[3*ii + 2] = 1.;
                    }

                    std::vector<double> knots_new(polygon_trim.size());
                    curve_2d.knots(knots_new.data());

                    dtkNurbsCurve2DDataOn* dtk_nurbs_curve_2d_data_on_new = new dtkNurbsCurve2DDataOn();
                    dtk_nurbs_curve_2d_data_on_new->create(polygon_trim.size(), 2, knots_new.data(), params_new.data());
                    dtkNurbsCurve2D* dtk_nurbs_curve_new = new dtkNurbsCurve2D(static_cast<dtkAbstractNurbsCurve2DData*>(dtk_nurbs_curve_2d_data_on_new));

                    dtkTrim* dtk_trim = nullptr;
                    if (trims[i]->isSeam()) {
                      dtk_trim = new dtkTrim(trims[i]->nurbsSurface(), *dtk_nurbs_curve_new, trims[i]->topoTrim(), true);
                    }
                    else {
                      dtk_trim = new dtkTrim(trims[i]->nurbsSurface(), *dtk_nurbs_curve_new, trims[i]->topoTrim(), false);
                    }

                    trim_loop->trims()[i] = dtk_trim;

                  }

                  for (std::size_t ii = 0; ii < polygon_trim.size(); ++ii) {
                    polygon_cgal.push_back(polygon_trim[ii]);
                  }

                }
              }

            }
#endif

            dtk_nurbs_surface->trimLoops().push_back(trim_loop);
        }
#ifdef LINEAR_
        for(auto s : surfs_sisl_split)
        {
          freeSurf(s);
        }

        for(auto s : nurbs_surfaces_split)
        {
          //only delete nurbs_surface if it has been allocated here
          if(s !=  nurbs_surface || nurbs_surface_needs_delete)
          {
            delete s;
          }
        }
#endif
    }
}

    // check if 3d curves are boundaries
    std::unordered_map<const dtkTopoTrim*, std::set<dtkNurbsSurface*>> map_curve_to_surfaces;
    std::unordered_map<const dtkTopoTrim*, std::set<dtkTrim*>> map_curve_to_trims;
    for (auto surf : dtk_brep_data->m_nurbs_surfaces) {
      for (auto trim_loop : surf->trimLoops()) {
        for (auto trim : trim_loop->trims()) {
          auto iterator = map_curve_to_surfaces.insert(std::make_pair(trim->topoTrim(), std::set< dtkNurbsSurface * >()));
          iterator.first->second.insert(surf);

          auto iter = map_curve_to_trims.insert( std::make_pair(trim->topoTrim(), std::set<dtkTrim*>()) );
          iter.first->second.insert(trim);
        }
      }
    }

    std::vector<const dtkTopoTrim*> curves_detached;
    for (auto topo_trim : map_curve_to_surfaces) {
      const dtkTopoTrim* topo_nurbs = topo_trim.first;
      if (topo_trim.second.size() == 1) {
        curves_detached.push_back(topo_nurbs);
      }
    }

    if (curves_detached.size() > 0) {
      std::cout << "number of curves detached: " << curves_detached.size() << std::endl;
      std::unordered_map<const dtkTopoTrim*, const dtkTopoTrim*> map_curve_to_curve;
      for (std::size_t i = 0; i < curves_detached.size(); ++i) {
        const dtkTopoTrim* topo_nurbs = curves_detached[i];
        if (topo_nurbs->m_nurbs_curve_3d != nullptr) {
          dtkNurbsCurve* curve_3d = topo_nurbs->m_nurbs_curve_3d;
          double cp_0[3], cp_n[3];
          curve_3d->controlPoint(0, cp_0);
          curve_3d->controlPoint(curve_3d->nbCps() - 1, cp_n);

          for (std::size_t j = i + 1; j < curves_detached.size(); ++j) {
            const dtkTopoTrim* topo_nurbs_j = curves_detached[j];
            if (topo_nurbs_j->m_nurbs_curve_3d != nullptr) {
              dtkNurbsCurve* curve_3d_j = topo_nurbs_j->m_nurbs_curve_3d;
              double cp_0_j[3], cp_n_j[3];
              curve_3d_j->controlPoint(0, cp_0_j);
              curve_3d_j->controlPoint(curve_3d_j->nbCps() - 1, cp_n_j);

              double dist_00 = std::sqrt( std::pow(cp_0[0] - cp_0_j[0], 2.) + std::pow(cp_0[1] - cp_0_j[1], 2.) + std::pow(cp_0[2] - cp_0_j[2], 2.) );
              double dist_nn = std::sqrt( std::pow(cp_n[0] - cp_n_j[0], 2.) + std::pow(cp_n[1] - cp_n_j[1], 2.) + std::pow(cp_n[2] - cp_n_j[2], 2.) );

              double dist_0n = std::sqrt( std::pow(cp_0[0] - cp_n_j[0], 2.) + std::pow(cp_0[1] - cp_n_j[1], 2.) + std::pow(cp_0[2] - cp_n_j[2], 2.) );
              double dist_n0 = std::sqrt( std::pow(cp_n[0] - cp_0_j[0], 2.) + std::pow(cp_n[1] - cp_0_j[1], 2.) + std::pow(cp_n[2] - cp_0_j[2], 2.) );

              if (dist_00 < 1.e-6 && dist_nn < 1.e-6) {
                map_curve_to_curve.insert( std::make_pair(topo_nurbs, topo_nurbs_j) );
                break;
              }

              if (dist_0n < 1.e-6 && dist_n0 < 1.e-6) {
                map_curve_to_curve.insert( std::make_pair(topo_nurbs, topo_nurbs_j) );
                break;
              }

            }
          }

        }
      }

      std::cout << "number of detached group: " << map_curve_to_curve.size() << std::endl;

      for (auto map : map_curve_to_curve) {
        const dtkTopoTrim* topo_nurbs_1 = map.first;
        const dtkTopoTrim* topo_nurbs_2 = map.second;

        //dtkNurbsSurface* nurbs_surface_1 = *(map_curve_to_surfaces[topo_nurbs_1].begin());
        dtkNurbsSurface* nurbs_surface_2 = *(map_curve_to_surfaces[topo_nurbs_2].begin());

        auto it = std::find(dtk_brep_data->m_nurbs_surfaces.begin(), dtk_brep_data->m_nurbs_surfaces.end(), nurbs_surface_2);

        //dtkTrim* trim_1 = *(map_curve_to_trims[topo_nurbs_1].begin());
        dtkTrim* trim_2 = *(map_curve_to_trims[topo_nurbs_2].begin());

        dtkTrim* trim_2_new = nullptr;
        trim_2_new = new dtkTrim(trim_2->nurbsSurface(), trim_2->curve2D(), topo_nurbs_1, trim_2->isSeam());

        for (std::size_t i = 0; i < nurbs_surface_2->trimLoops().size(); ++i) {
          dtkTrimLoop* trim_loop = nurbs_surface_2->trimLoops()[i];
          for (std::size_t j = 0; j < trim_loop->trims().size(); ++j) {
            dtkTrim* trim = trim_loop->trims()[j];
            if (trim == trim_2) {
              trim_loop->trims()[j] = trim_2_new;
            }
          }
          nurbs_surface_2->trimLoops()[i] = trim_loop;
        }

        *it = nurbs_surface_2;

      }

    }

	ON::End();
}

bool openNURBSBRepReader::on_segment(const K::Point_2& p, const K::Point_2& q, const K::Point_2& r)
{
  if (r[0] <= (std::max)(p[0], q[0]) && r[0] >= (std::min)(p[0], q[0]) &&
      r[1] <= (std::max)(p[1], q[1]) && r[1] >= (std::min)(p[1], q[1]) ) {
    return true;
  }

  return false;
}

int openNURBSBRepReader::side_directed(const K::Point_2& p, const K::Point_2& q, const K::Point_2& r)
{
  double val = (q[1] - p[1])*(r[0] - p[0]) - (q[0] - p[0])*(r[1] - p[1]);

  if (val == 0.) return 0;

  return (val > 0.) ? 1 : 2;
}

bool openNURBSBRepReader::do_intersect(const K::Point_2& p, const K::Point_2& q, const K::Point_2& r, const K::Point_2& s)
{

  int side1 = side_directed(p, q, r);
  int side2 = side_directed(p, q, s);
  int side3 = side_directed(r, s, p);
  int side4 = side_directed(r, s, q);

  if (side1 != side2 && side3 != side4) return true;

  if (side1 == 0 && on_segment(p, q, r)) return true;
  if (side2 == 0 && on_segment(p, q, s)) return true;
  if (side3 == 0 && on_segment(r, s, p)) return true;
  if (side4 == 0 && on_segment(r, s, q)) return true;

  return false;
}

dtkBRep* openNURBSBRepReader::outputDtkBRep(void) const
{
    return d->dtk_brep;
}

void openNURBSBRepReader::convert_nurbs_surface_to_sisl(const ON_NurbsSurface* nurbs_surface, SISLSurf*& surf_sisl)
{
  int u_degree = nurbs_surface->Order(0) - 1;
  int v_degree = nurbs_surface->Order(1) - 1;
  int num_cp_u = nurbs_surface->CVCount(0);
  int num_cp_v = nurbs_surface->CVCount(1);

  const double* u_knots = nurbs_surface->Knot(0);
  const double* v_knots = nurbs_surface->Knot(1);

  std::vector<double> u_knots_sisl(num_cp_u + u_degree + 1);
  std::vector<double> v_knots_sisl(num_cp_v + v_degree + 1);
  u_knots_sisl[0] = u_knots[0], u_knots_sisl[num_cp_u + u_degree] = u_knots[num_cp_u + u_degree - 2];
  v_knots_sisl[0] = v_knots[0], v_knots_sisl[num_cp_v + v_degree] = v_knots[num_cp_v + v_degree - 2];
  for (int i = 0; i < num_cp_u + u_degree - 1; ++i) {
    u_knots_sisl[i + 1] = u_knots[i];
  }
  for (int i = 0; i < num_cp_v + v_degree - 1; ++i) {
    v_knots_sisl[i + 1] = v_knots[i];
  }

  std::vector<double> coeff_sisl(4*num_cp_u*num_cp_v);
  for (int i = 0; i < num_cp_u; ++i) {
    for (int j = 0; j < num_cp_v; ++j) {
      double* cp = nurbs_surface->CV(i, j); // homogeneous coordinates
      coeff_sisl[4*num_cp_u*j + 4*i] = cp[0];
      coeff_sisl[4*num_cp_u*j + 4*i + 1] = cp[1];
      coeff_sisl[4*num_cp_u*j + 4*i + 2] = cp[2];
      coeff_sisl[4*num_cp_u*j + 4*i + 3] = cp[3];
    }
  }

  surf_sisl = newSurf(num_cp_u, num_cp_v, u_degree + 1, v_degree + 1, u_knots_sisl.data(), v_knots_sisl.data(), coeff_sisl.data(), 2, 3, 1);
}

void openNURBSBRepReader::trimming_curve_approx(const ON_NurbsCurve* nurbs_edge, std::vector<dtkContinuousGeometryPrimitives::Point_3>& polyline, const double tolerance)
{
  dtkNurbsCurveDataOn* dtk_nurbs_curve_data_on = new dtkNurbsCurveDataOn();
  dtk_nurbs_curve_data_on->create(*nurbs_edge);
  dtkNurbsCurve* nurbs_curve = new dtkNurbsCurve(static_cast< dtkAbstractNurbsCurveData * >(dtk_nurbs_curve_data_on));

  std::vector<dtkRationalBezierCurve*> rational_bezier_curves;
  nurbs_curve->decomposeToRationalBezierCurves(rational_bezier_curves);

  int num_bezier_curves = rational_bezier_curves.size();
  int bezier_id = 0;
  for (auto bezier_curve : rational_bezier_curves) {
    std::list<std::unique_ptr<dtkRationalBezierCurve> > bezier_curves_refined;
    bezier_curves_refined.push_back(std::make_unique<dtkRationalBezierCurve>(*bezier_curve));
    auto it = bezier_curves_refined.begin();

    while (it != bezier_curves_refined.end()) {
      double error = dtkContinuousGeometryTools::convexHullApproximationError1(*(*it));
      if (error > tolerance) {
        auto curve_a = std::make_unique<dtkRationalBezierCurve>();
        auto curve_b = std::make_unique<dtkRationalBezierCurve>();
        (*it)->split(curve_a.get(), curve_b.get(), 0.5);
        it = bezier_curves_refined.erase(it);
        bezier_curves_refined.insert(it, std::move(curve_b));
        --it;
        bezier_curves_refined.insert(it, std::move(curve_a));
        --it;
      }
      else {
        ++it;
      }
    } // refine a bezier curve

    // decide if the bezier curve is reversed
    bezier_curve->set_not_reversed();

    int deg = bezier_curve->degree();
    dtkContinuousGeometryPrimitives::Point_3 cp0(0., 0., 0.);
    dtkContinuousGeometryPrimitives::Point_3 cpn(0., 0., 0.);
    bezier_curve->controlPoint(0, cp0.data());
    bezier_curve->controlPoint(deg, cpn.data());

    if (bezier_id > 0) {
      dtkRationalBezierCurve* bezier_curve_pre = rational_bezier_curves[bezier_id - 1];
      int deg_pre = bezier_curve_pre->degree();
      dtkContinuousGeometryPrimitives::Point_3 cp0_pre(0., 0., 0.);
      dtkContinuousGeometryPrimitives::Point_3 cpn_pre(0., 0., 0.);
      bezier_curve_pre->controlPoint(0, cp0_pre.data());
      bezier_curve_pre->controlPoint(deg_pre, cpn_pre.data());

      if ( dtkContinuousGeometryTools::squaredDistance(cp0, cpn_pre) < 1.e-6 ) {
        if (bezier_curve_pre->is_reversed() == true) bezier_curve->reverse();
      }
      else {
        if (bezier_curve_pre->is_reversed() == false) bezier_curve->reverse();
      }
    }

    // store polyline points
    for (auto& bezier_curve_refined : bezier_curves_refined) {
      double coord[3];
      if (bezier_curve->is_reversed() == false) {
        bezier_curve_refined->controlPoint(0, coord);
      }
      else {
        int deg_refined = bezier_curve_refined->degree();
        bezier_curve_refined->controlPoint(deg_refined, coord);
      }
      polyline.push_back(dtkContinuousGeometryPrimitives::Point_3(coord[0], coord[1], coord[2]));
    }

    // only store the last control point of the last bezier curve
    if (bezier_id == num_bezier_curves - 1) {
      std::unique_ptr<dtkRationalBezierCurve>& bezier_curve_last = *(std::prev(bezier_curves_refined.end()));
      const int deg_last = bezier_curve_last->degree();
      double coord_last[3];
      if (bezier_curve->is_reversed() == false) {
        bezier_curve_last->controlPoint(deg_last, coord_last);
      }
      else {
        bezier_curve_last->controlPoint(0, coord_last);
      }
      polyline.push_back(dtkContinuousGeometryPrimitives::Point_3(coord_last[0], coord_last[1], coord_last[2]));
    }

    ++bezier_id;

  } // loop over a bezier curve

  delete nurbs_curve;
}

void openNURBSBRepReader::parametric_polyline_beta(const std::vector<SISLSurf*>& surfs_sisl, std::vector<dtkContinuousGeometryPrimitives::Point_3>& polyline, std::vector<dtkContinuousGeometryPrimitives::Point_2>& params,
                                                   const ON_NurbsSurface* nurbs_surface, const ON_NurbsCurve* nurbs_curve_ref, bool is_reversed, double tol)
{
  //std::cout << "tolerance: " << tol << std::endl;

  ON_3dPoint cv_0, cv_n;
  nurbs_curve_ref->GetCV(0, cv_0);
  nurbs_curve_ref->GetCV(nurbs_curve_ref->CVCount() - 1, cv_n);

  //----------------------------------------------------------------------------------------------------
  // get the knot boundary of the surface
  double knot_min_u = 0., knot_max_u = 0.;
  double knot_min_v = 0., knot_max_v = 0.;

  for (std::size_t i = 0; i < surfs_sisl.size(); ++i) {
    double* knots_u = surfs_sisl[i]->et1;
    double* knots_v = surfs_sisl[i]->et2;
    if (i == 0) {
      knot_min_u = knots_u[0], knot_max_u = knots_u[surfs_sisl[i]->in1 + surfs_sisl[i]->ik1 - 1];
      knot_min_v = knots_v[0], knot_max_v = knots_v[surfs_sisl[i]->in2 + surfs_sisl[i]->ik2 - 1];
    }
    else {
      if (knot_min_u > knots_u[0]) knot_min_u = knots_u[0];
      if (knot_max_u < knots_u[surfs_sisl[i]->in1 + surfs_sisl[i]->ik1 - 1]) knot_max_u = knots_u[surfs_sisl[i]->in1 + surfs_sisl[i]->ik1 - 1];

      if (knot_min_v > knots_v[0]) knot_min_v = knots_v[0];
      if (knot_max_v < knots_v[surfs_sisl[i]->in2 + surfs_sisl[i]->ik2 - 1]) knot_max_v = knots_v[surfs_sisl[i]->in2 + surfs_sisl[i]->ik2 - 1];
    }
  }

  //std::cout << "knot domain: " << knot_min_u << ", " << knot_max_u << "; " << knot_min_v << ", " << knot_max_v << std::endl;

  bool is_on_boundary_first = false;
  bool is_on_boundary_last = false;
  //std::cout << "end cps: " << std::endl;
  //std::cout << cv_0[0] << " " << cv_0[1] << std::endl;
  //std::cout << cv_n[0] << " " << cv_n[1] << std::endl;
  if (cv_0[0] > knot_min_u - 1.e-6 && cv_0[0] < knot_min_u + 1.e-6) {
    if (!is_reversed) is_on_boundary_first = true;
    else is_on_boundary_last = true;
  }
  if (cv_0[0] > knot_max_u - 1.e-6 && cv_0[0] < knot_max_u + 1.e-6) {
    if (!is_reversed) is_on_boundary_first = true;
    else is_on_boundary_last = true;
  }

  if (cv_0[1] > knot_min_v - 1.e-6 && cv_0[1] < knot_min_v + 1.e-6) {
    if (!is_reversed) is_on_boundary_first = true;
    else is_on_boundary_last = true;
  }
  if (cv_0[1] > knot_max_v - 1.e-6 && cv_0[1] < knot_max_v + 1.e-6) {
    if (!is_reversed) is_on_boundary_first = true;
    else is_on_boundary_last = true;
  }

  if (cv_n[0] > knot_min_u - 1.e-6 && cv_n[0] < knot_min_u + 1.e-6) {
    if (!is_reversed) is_on_boundary_last = true;
    else is_on_boundary_first = true;
  }
  if (cv_n[0] > knot_max_u - 1.e-6 && cv_n[0] < knot_max_u + 1.e-6) {
    if (!is_reversed) is_on_boundary_last = true;
    else is_on_boundary_first = true;
  }

  if (cv_n[1] > knot_min_v - 1.e-6 && cv_n[1] < knot_min_v + 1.e-6) {
    if (!is_reversed) is_on_boundary_last = true;
    else is_on_boundary_first = true;
  }
  if (cv_n[1] > knot_max_v - 1.e-6 && cv_n[1] < knot_max_v + 1.e-6) {
    if (!is_reversed) is_on_boundary_last = true;
    else is_on_boundary_first = true;
  }

  bool is_boundary = true;
  for (int i = 0; i < nurbs_curve_ref->CVCount(); ++i) {
    ON_3dPoint cv;
    nurbs_curve_ref->GetCV(i, cv);

    if (cv[0] > knot_min_u + 1.e-6 && cv[0] < knot_max_u - 1.e-6 && cv[1] > knot_min_v + 1.e-6 && cv[1] < knot_max_v - 1.e-6) {
      is_boundary = false;
      break;
    }
  }

  std::cout << "end points are on boundary: " << is_on_boundary_first << ", " << is_on_boundary_last << std::endl;
  std::cout << "curve is on boundary: " << is_boundary << std::endl;

  //---------------------------------------------------------------------------------------------------

  std::vector<int> ids;

  for (std::size_t i = 0; i < polyline.size(); ++i) {

    dtkContinuousGeometryPrimitives::Point_3 point = polyline[i];

    //std::cout << "point: " << std::endl;
    //std::cout << std::fixed << std::setprecision(12);
    //std::cout << point[0] << " " << point[1] << " " << point[2] << std::endl;

    std::vector<dtkContinuousGeometryPrimitives::Point_2> pointpars;
    std::vector<dtkContinuousGeometryPrimitives::Point_2> pointpars_valid;

    std::vector<dtkContinuousGeometryPrimitives::Point_3> pointvals;
    std::vector<dtkContinuousGeometryPrimitives::Point_3> pointvals_valid;

    for (auto surf_sisl : surfs_sisl) {
      double eps_co = 1.e-8;
      double eps_ge = 1.e-8;
      int numclopt = 0;
      double* pointpar = nullptr;
      int numclocr = 0;
      SISLIntcurve** clocurves;
      int stat = 0;

      s1954(surf_sisl, point.data(), 3, eps_co, eps_ge, &numclopt, &pointpar, &numclocr, &clocurves, &stat);

      //std::cout << "numclopt = " << numclopt << std::endl;
      //std::cout << "numclocr = " << numclocr << std::endl;

      for (int j = 0; j < numclopt; ++j) {
        pointpars.push_back( dtkContinuousGeometryPrimitives::Point_2(pointpar[2*j], pointpar[2*j + 1]) );

        //std::cout << "closest point: " << std::endl;
        double param[2];
        param[0] = pointpar[2*j], param[1] = pointpar[2*j + 1];
        int knot1 = 0, knot2 = 0;
        double deriv[3], normal[3];
        deriv[0] = 0., deriv[1] = 0., deriv[2] = 0.;
        normal[0] = 0., normal[1] = 0., normal[2] = 0.;
        int statj = 0;
        s1421(surf_sisl, 0, param, &knot1, &knot2, deriv, normal, &statj);

        pointvals.push_back( dtkContinuousGeometryPrimitives::Point_3(deriv[0], deriv[1], deriv[2]) );

        //std::cout << param[0] << " " << param[1] << " " << 0 << std::endl;
        //std::cout << deriv[0] << " " << deriv[1] << " " << deriv[2] << std::endl;

        double distj = std::sqrt( std::pow(deriv[0] - point[0], 2.) +
                                  std::pow(deriv[1] - point[1], 2.) +
                                  std::pow(deriv[2] - point[2], 2.) );

        if (distj < tol) {
          pointpars_valid.push_back( dtkContinuousGeometryPrimitives::Point_2(pointpar[2*j], pointpar[2*j + 1]) );
          pointvals_valid.push_back( dtkContinuousGeometryPrimitives::Point_3(deriv[0], deriv[1], deriv[2]) );
        }
      }

      for (int j = 0; j < numclocr; ++j) {
        int ipoint = (*clocurves)->ipoint;
        //std::cout << "number of points defining curve: " << ipoint << std::endl;

        //std::cout << "type of curve: " << (*clocurves)->itype << std::endl;

        for (int k = 0; k < ipoint; ++k) {
          double param[2];
          param[0] = (*clocurves)->epar1[2*k], param[1] = (*clocurves)->epar1[2*k + 1];
          int knot1 = 0, knot2 = 0;
          double deriv[3], normal[3];
          deriv[0] = 0., deriv[1] = 0., deriv[2] = 0.;
          normal[0] = 0., normal[1] = 0., normal[2] = 0.;
          int statj = 0;
          s1421(surf_sisl, 0, param, &knot1, &knot2, deriv, normal, &statj);

          //std::cout << param[0] << " " << param[1] << " " << 0 << std::endl;
          //std::cout << deriv[0] << " " << deriv[1] << " " << deriv[2] << std::endl;

          double distj = std::sqrt( std::pow(deriv[0] - point[0], 2.) +
                                    std::pow(deriv[1] - point[1], 2.) +
                                    std::pow(deriv[2] - point[2], 2.) );

          pointpars.push_back( dtkContinuousGeometryPrimitives::Point_2(param[0], param[1]) );
          pointvals.push_back( dtkContinuousGeometryPrimitives::Point_3(deriv[0], deriv[1], deriv[2]) );

          if (distj < tol) {
            if (is_boundary) {
              if (std::abs(cv_0[0] - cv_n[0]) < 1.e-6) {
                if (std::abs(param[0] - cv_0[0]) < 1.e-6) {
                  pointpars_valid.push_back( dtkContinuousGeometryPrimitives::Point_2(param[0], param[1]) );
                  pointvals_valid.push_back( dtkContinuousGeometryPrimitives::Point_3(deriv[0], deriv[1], deriv[2]) );
                }
              }
              else if (std::abs(cv_0[1] - cv_n[1]) < 1.e-6) {
                if (std::abs(param[1] - cv_0[1]) < 1.e-6) {
                  pointpars_valid.push_back( dtkContinuousGeometryPrimitives::Point_2(param[0], param[1]) );
                  pointvals_valid.push_back( dtkContinuousGeometryPrimitives::Point_3(deriv[0], deriv[1], deriv[2]) );
                }
              }
            }
            else {
              pointpars_valid.push_back( dtkContinuousGeometryPrimitives::Point_2(param[0], param[1]) );
              pointvals_valid.push_back( dtkContinuousGeometryPrimitives::Point_3(deriv[0], deriv[1], deriv[2]) );
            }
          }

        }
      }
      if(pointpar != nullptr)
      {
        free(pointpar);
      }
      for(int i=0; i< numclocr; ++i)
      {
        freeIntcurve(clocurves[i]);
      }
      if(numclocr > 0)
      {
        free(clocurves);
      }
    }

    dtkContinuousGeometryPrimitives::Point_2 param_ref(0., 0.);

    if (i == 0) { // the first point
      if (!is_reversed) param_ref[0] = cv_0[0], param_ref[1] = cv_0[1];
      else param_ref[0] = cv_n[0], param_ref[1] = cv_n[1];
    }
    else if (i == polyline.size() - 1) { // the last point
      if (!is_reversed) param_ref[0] = cv_n[0], param_ref[1] = cv_n[1];
      else param_ref[0] = cv_0[0], param_ref[1] = cv_0[1];
    }
    else {
      param_ref[0] = params.back()[0], param_ref[1] = params.back()[1];
    }

    //std::cout << "number of closest points: " << pointpars.size() << std::endl;

    std::vector<dtkContinuousGeometryPrimitives::Point_2> pointpars_tmp = pointpars;
    std::vector<dtkContinuousGeometryPrimitives::Point_3> pointvals_tmp = pointvals;

    if (pointpars_valid.size() > 0) pointpars_tmp = pointpars_valid, pointvals_tmp = pointvals_valid;

    if (i == 0 && is_on_boundary_first) pointpars_tmp = pointpars, pointvals_tmp = pointvals;
    if (i == polyline.size() - 1 && is_on_boundary_last) pointpars_tmp = pointpars, pointvals_tmp = pointvals;

    if (pointpars_tmp.size() > 0) {
      std::size_t id = 0;
      double dist_min = std::sqrt( std::pow(pointpars_tmp[0][0] - param_ref[0], 2.) + std::pow(pointpars_tmp[0][1] - param_ref[1], 2.) );

      for (std::size_t j = 1; j < pointpars_tmp.size(); ++j) {
        double distj = std::sqrt( std::pow(pointpars_tmp[j][0] - param_ref[0], 2.) + std::pow(pointpars_tmp[j][1] - param_ref[1], 2.) );
        if (distj < dist_min) dist_min = distj, id = j;
      }

      // check if other parameters are actually better
      double distance_min = std::sqrt( dtkContinuousGeometryTools::squaredDistance(point, pointvals_tmp[id]) );
      for (std::size_t j = 0; j < pointpars_tmp.size(); ++j) {
        if (j != id) {
          //double distj = std::sqrt( std::pow(pointpars_tmp[j][0] - param_ref[0], 2.) + std::pow(pointpars_tmp[j][1] - param_ref[1], 2.) );
          //if (distj < 1.e-6) { // \todo a constant value
          if ( std::abs(pointpars_tmp[j][0] - param_ref[0]) < (knot_max_u - knot_min_u)/2. &&
               std::abs(pointpars_tmp[j][1] - param_ref[1]) < (knot_max_v - knot_min_v)/2. ) {
            double distance = std::sqrt( dtkContinuousGeometryTools::squaredDistance(point, pointvals_tmp[j]) );
            if (distance < distance_min) distance_min = distance, id = j;
          }
        }
      }

      // check if the reference point is better
      if (i == 0 || i == polyline.size() - 1) {
        ON_3dPoint point_ref_on;
        nurbs_surface->EvPoint(param_ref[0], param_ref[1], point_ref_on);

        dtkContinuousGeometryPrimitives::Point_3 point_ref(point_ref_on[0], point_ref_on[1], point_ref_on[2]);
        double distance = std::sqrt( dtkContinuousGeometryTools::squaredDistance(point, point_ref) );
        if (std::abs(distance - distance_min) < 1.e-6) pointpars_tmp[id] = param_ref;
      }

      params.push_back(pointpars_tmp[id]);

      ids.push_back(i);

      //std::cout << "reference params: " << param_ref[0] << " " << param_ref[1] << " " << 0 << std::endl;

      //std::cout << "take: " << pointpars_tmp[id][0] << " " << pointpars_tmp[id][1] << " " << 0 << std::endl;
      //std::cout << std::endl;
    }

  }

  // special treatment to missing points due to SISL
  if (ids.size() < polyline.size()) {
    std::cout << "Warning: SISL doesn't give enough closest points!" << std::endl;
    std::vector<dtkContinuousGeometryPrimitives::Point_2> params_tmp;
    for (std::size_t i = 0; i < ids.size() - 1; ++i) {
      params_tmp.push_back(params[i]);
      int delta = ids[i + 1] - ids[i];
      if (delta > 1) {
        for (int j = 1; j < ids[i + 1] - ids[i]; ++j) {
          dtkContinuousGeometryPrimitives::Point_2 param(0., 0.);
          param[0] = params[i][0] + (params[i + 1][0] - params[i][0])*(double(j)/double(delta));
          param[1] = params[i][1] + (params[i + 1][1] - params[i][1])*(double(j)/double(delta));
          params_tmp.push_back(param);
        }
      }
    }
    params_tmp.push_back(params.back());

    params.clear();
    params = params_tmp;
  }

}

// in the case where 2d trim curves given in the 3dm file are not reliable
void openNURBSBRepReader::parametric_polyline_beta2(const std::vector<SISLSurf*>& surfs_sisl, std::vector<dtkContinuousGeometryPrimitives::Point_3>& polyline, std::vector<dtkContinuousGeometryPrimitives::Point_2>& params,
                                                   const ON_NurbsSurface* nurbs_surface, const std::vector<K::Point_2>& parameters_end, bool is_reversed, double tol)
{
  //std::cout << "tolerance: " << tol << std::endl;

  ON_2dPoint cv_0, cv_n;
  if (!is_reversed) { // the trim is not reversed
    cv_0[0] = parameters_end[0][0], cv_0[1] = parameters_end[0][1];
    cv_n[0] = parameters_end[1][0], cv_n[1] = parameters_end[1][1];
  }
  else {
    cv_n[0] = parameters_end[0][0], cv_0[1] = parameters_end[0][1];
    cv_0[0] = parameters_end[1][0], cv_n[1] = parameters_end[1][1];
  }

  //----------------------------------------------------------------------------------------------------
  // get the knot boundary of the surface
  double knot_min_u = 0., knot_max_u = 0.;
  double knot_min_v = 0., knot_max_v = 0.;

  for (std::size_t i = 0; i < surfs_sisl.size(); ++i) {
    double* knots_u = surfs_sisl[i]->et1;
    double* knots_v = surfs_sisl[i]->et2;
    if (i == 0) {
      knot_min_u = knots_u[0], knot_max_u = knots_u[surfs_sisl[i]->in1 + surfs_sisl[i]->ik1 - 1];
      knot_min_v = knots_v[0], knot_max_v = knots_v[surfs_sisl[i]->in2 + surfs_sisl[i]->ik2 - 1];
    }
    else {
      if (knot_min_u > knots_u[0]) knot_min_u = knots_u[0];
      if (knot_max_u < knots_u[surfs_sisl[i]->in1 + surfs_sisl[i]->ik1 - 1]) knot_max_u = knots_u[surfs_sisl[i]->in1 + surfs_sisl[i]->ik1 - 1];

      if (knot_min_v > knots_v[0]) knot_min_v = knots_v[0];
      if (knot_max_v < knots_v[surfs_sisl[i]->in2 + surfs_sisl[i]->ik2 - 1]) knot_max_v = knots_v[surfs_sisl[i]->in2 + surfs_sisl[i]->ik2 - 1];
    }
  }

  //std::cout << "knot domain: " << knot_min_u << ", " << knot_max_u << "; " << knot_min_v << ", " << knot_max_v << std::endl;

  bool is_on_boundary_first = false;
  bool is_on_boundary_last = false;
  //std::cout << "end cps: " << std::endl;
  //std::cout << cv_0[0] << " " << cv_0[1] << std::endl;
  //std::cout << cv_n[0] << " " << cv_n[1] << std::endl;
  if (cv_0[0] > knot_min_u - 1.e-6 && cv_0[0] < knot_min_u + 1.e-6) {
    if (!is_reversed) is_on_boundary_first = true;
    else is_on_boundary_last = true;
  }
  if (cv_0[0] > knot_max_u - 1.e-6 && cv_0[0] < knot_max_u + 1.e-6) {
    if (!is_reversed) is_on_boundary_first = true;
    else is_on_boundary_last = true;
  }

  if (cv_0[1] > knot_min_v - 1.e-6 && cv_0[1] < knot_min_v + 1.e-6) {
    if (!is_reversed) is_on_boundary_first = true;
    else is_on_boundary_last = true;
  }
  if (cv_0[1] > knot_max_v - 1.e-6 && cv_0[1] < knot_max_v + 1.e-6) {
    if (!is_reversed) is_on_boundary_first = true;
    else is_on_boundary_last = true;
  }

  if (cv_n[0] > knot_min_u - 1.e-6 && cv_n[0] < knot_min_u + 1.e-6) {
    if (!is_reversed) is_on_boundary_last = true;
    else is_on_boundary_first = true;
  }
  if (cv_n[0] > knot_max_u - 1.e-6 && cv_n[0] < knot_max_u + 1.e-6) {
    if (!is_reversed) is_on_boundary_last = true;
    else is_on_boundary_first = true;
  }

  if (cv_n[1] > knot_min_v - 1.e-6 && cv_n[1] < knot_min_v + 1.e-6) {
    if (!is_reversed) is_on_boundary_last = true;
    else is_on_boundary_first = true;
  }
  if (cv_n[1] > knot_max_v - 1.e-6 && cv_n[1] < knot_max_v + 1.e-6) {
    if (!is_reversed) is_on_boundary_last = true;
    else is_on_boundary_first = true;
  }

  bool is_boundary = true;
  if (cv_0[0] > knot_min_u + 1.e-6 && cv_0[0] < knot_max_u - 1.e-6 && 
      cv_0[1] > knot_min_v + 1.e-6 && cv_0[1] < knot_max_v - 1.e-6) {
    is_boundary = false;
  }
  if (cv_n[0] > knot_min_u + 1.e-6 && cv_n[0] < knot_max_u - 1.e-6 && 
      cv_n[1] > knot_min_v + 1.e-6 && cv_n[1] < knot_max_v - 1.e-6) {
    is_boundary = false;
  }

  std::cout << "end points are on boundary: " << is_on_boundary_first << ", " << is_on_boundary_last << std::endl;
  std::cout << "curve is on boundary: " << is_boundary << std::endl;

  //---------------------------------------------------------------------------------------------------

  std::vector<int> ids;

  for (std::size_t i = 0; i < polyline.size(); ++i) {

    dtkContinuousGeometryPrimitives::Point_3 point = polyline[i];

    //std::cout << "point: " << std::endl;
    //std::cout << std::fixed << std::setprecision(12);
    //std::cout << point[0] << " " << point[1] << " " << point[2] << std::endl;

    std::vector<dtkContinuousGeometryPrimitives::Point_2> pointpars;
    std::vector<dtkContinuousGeometryPrimitives::Point_2> pointpars_valid;

    std::vector<dtkContinuousGeometryPrimitives::Point_3> pointvals;
    std::vector<dtkContinuousGeometryPrimitives::Point_3> pointvals_valid;

    for (auto surf_sisl : surfs_sisl) {
      double eps_co = 1.e-8;
      double eps_ge = 1.e-8;
      int numclopt = 0;
      double* pointpar;
      int numclocr = 0;
      SISLIntcurve** clocurves;
      int stat = 0;

      s1954(surf_sisl, point.data(), 3, eps_co, eps_ge, &numclopt, &pointpar, &numclocr, &clocurves, &stat);

      //std::cout << "numclopt = " << numclopt << std::endl;
      //std::cout << "numclocr = " << numclocr << std::endl;

      for (int j = 0; j < numclopt; ++j) {
        pointpars.push_back( dtkContinuousGeometryPrimitives::Point_2(pointpar[2*j], pointpar[2*j + 1]) );

        //std::cout << "closest point: " << std::endl;
        double param[2];
        param[0] = pointpar[2*j], param[1] = pointpar[2*j + 1];
        int knot1 = 0, knot2 = 0;
        double deriv[3], normal[3];
        deriv[0] = 0., deriv[1] = 0., deriv[2] = 0.;
        normal[0] = 0., normal[1] = 0., normal[2] = 0.;
        int statj = 0;
        s1421(surf_sisl, 0, param, &knot1, &knot2, deriv, normal, &statj);

        pointvals.push_back( dtkContinuousGeometryPrimitives::Point_3(deriv[0], deriv[1], deriv[2]) );

        //std::cout << param[0] << " " << param[1] << " " << 0 << std::endl;
        //std::cout << deriv[0] << " " << deriv[1] << " " << deriv[2] << std::endl;

        double distj = std::sqrt( std::pow(deriv[0] - point[0], 2.) +
                                  std::pow(deriv[1] - point[1], 2.) +
                                  std::pow(deriv[2] - point[2], 2.) );

        if (distj < tol) {
          pointpars_valid.push_back( dtkContinuousGeometryPrimitives::Point_2(pointpar[2*j], pointpar[2*j + 1]) );
          pointvals_valid.push_back( dtkContinuousGeometryPrimitives::Point_3(deriv[0], deriv[1], deriv[2]) );
        }

      }

      for (int j = 0; j < numclocr; ++j) {
        int ipoint = (*clocurves)->ipoint;
        //std::cout << "number of points defining curve: " << ipoint << std::endl;

        //std::cout << "type of curve: " << (*clocurves)->itype << std::endl;

        for (int k = 0; k < ipoint; ++k) {
          double param[2];
          param[0] = (*clocurves)->epar1[2*k], param[1] = (*clocurves)->epar1[2*k + 1];
          int knot1 = 0, knot2 = 0;
          double deriv[3], normal[3];
          deriv[0] = 0., deriv[1] = 0., deriv[2] = 0.;
          normal[0] = 0., normal[1] = 0., normal[2] = 0.;
          int statj = 0;
          s1421(surf_sisl, 0, param, &knot1, &knot2, deriv, normal, &statj);

          //std::cout << param[0] << " " << param[1] << " " << 0 << std::endl;
          //std::cout << deriv[0] << " " << deriv[1] << " " << deriv[2] << std::endl;

          double distj = std::sqrt( std::pow(deriv[0] - point[0], 2.) +
                                    std::pow(deriv[1] - point[1], 2.) +
                                    std::pow(deriv[2] - point[2], 2.) );

          pointpars.push_back( dtkContinuousGeometryPrimitives::Point_2(param[0], param[1]) );
          pointvals.push_back( dtkContinuousGeometryPrimitives::Point_3(deriv[0], deriv[1], deriv[2]) );

          if (distj < tol) {
            if (is_boundary) {
              if (std::abs(cv_0[0] - cv_n[0]) < 1.e-6) {
                if (std::abs(param[0] - cv_0[0]) < 1.e-6) {
                  pointpars_valid.push_back( dtkContinuousGeometryPrimitives::Point_2(param[0], param[1]) );
                  pointvals_valid.push_back( dtkContinuousGeometryPrimitives::Point_3(deriv[0], deriv[1], deriv[2]) );
                }
              }
              else if (std::abs(cv_0[1] - cv_n[1]) < 1.e-6) {
                if (std::abs(param[1] - cv_0[1]) < 1.e-6) {
                  pointpars_valid.push_back( dtkContinuousGeometryPrimitives::Point_2(param[0], param[1]) );
                  pointvals_valid.push_back( dtkContinuousGeometryPrimitives::Point_3(deriv[0], deriv[1], deriv[2]) );
                }
              }
            }
            else {
              pointpars_valid.push_back( dtkContinuousGeometryPrimitives::Point_2(param[0], param[1]) );
              pointvals_valid.push_back( dtkContinuousGeometryPrimitives::Point_3(deriv[0], deriv[1], deriv[2]) );
            }
          }

        }

      }

    }

    dtkContinuousGeometryPrimitives::Point_2 param_ref(0., 0.);

    if (i == 0) { // the first point
      if (!is_reversed) param_ref[0] = cv_0[0], param_ref[1] = cv_0[1];
      else param_ref[0] = cv_n[0], param_ref[1] = cv_n[1];
    }
    else if (i == polyline.size() - 1) { // the last point
      if (!is_reversed) param_ref[0] = cv_n[0], param_ref[1] = cv_n[1];
      else param_ref[0] = cv_0[0], param_ref[1] = cv_0[1];
    }
    else {
      param_ref[0] = params.back()[0], param_ref[1] = params.back()[1];
    }

    //std::cout << "number of closest points: " << pointpars.size() << std::endl;

    std::vector<dtkContinuousGeometryPrimitives::Point_2> pointpars_tmp = pointpars;
    std::vector<dtkContinuousGeometryPrimitives::Point_3> pointvals_tmp = pointvals;

    if (pointpars_valid.size() > 0) pointpars_tmp = pointpars_valid, pointvals_tmp = pointvals_valid;

    if (i == 0 && is_on_boundary_first) pointpars_tmp = pointpars, pointvals_tmp = pointvals;
    if (i == polyline.size() - 1 && is_on_boundary_last) pointpars_tmp = pointpars, pointvals_tmp = pointvals;

    if (pointpars_tmp.size() > 0) {
      std::size_t id = 0;
      double dist_min = std::sqrt( std::pow(pointpars_tmp[0][0] - param_ref[0], 2.) + std::pow(pointpars_tmp[0][1] - param_ref[1], 2.) );

      for (std::size_t j = 1; j < pointpars_tmp.size(); ++j) {
        double distj = std::sqrt( std::pow(pointpars_tmp[j][0] - param_ref[0], 2.) + std::pow(pointpars_tmp[j][1] - param_ref[1], 2.) );
        if (distj < dist_min) dist_min = distj, id = j;
      }

      // check if other parameters are actually better
      double distance_min = std::sqrt( dtkContinuousGeometryTools::squaredDistance(point, pointvals_tmp[id]) );
      for (std::size_t j = 0; j < pointpars_tmp.size(); ++j) {
        if (j != id) {
          //double distj = std::sqrt( std::pow(pointpars_tmp[j][0] - param_ref[0], 2.) + std::pow(pointpars_tmp[j][1] - param_ref[1], 2.) );
          //if (distj < 1.e-6) { // \todo a constant value
          if ( std::abs(pointpars_tmp[j][0] - param_ref[0]) < (knot_max_u - knot_min_u)/2. &&
               std::abs(pointpars_tmp[j][1] - param_ref[1]) < (knot_max_v - knot_min_v)/2. ) {
            double distance = std::sqrt( dtkContinuousGeometryTools::squaredDistance(point, pointvals_tmp[j]) );
            if (distance < distance_min) distance_min = distance, id = j;
          }
        }
      }

      // check if the reference point is better
      if (i == 0 || i == polyline.size() - 1) {
        ON_3dPoint point_ref_on;
        nurbs_surface->EvPoint(param_ref[0], param_ref[1], point_ref_on);

        dtkContinuousGeometryPrimitives::Point_3 point_ref(point_ref_on[0], point_ref_on[1], point_ref_on[2]);
        double distance = std::sqrt( dtkContinuousGeometryTools::squaredDistance(point, point_ref) );
        if (std::abs(distance - distance_min) < 1.e-6) pointpars_tmp[id] = param_ref;
      }

      params.push_back(pointpars_tmp[id]);

      ids.push_back(i);

      //std::cout << "reference params: " << param_ref[0] << " " << param_ref[1] << " " << 0 << std::endl;

      //std::cout << "take: " << pointpars_tmp[id][0] << " " << pointpars_tmp[id][1] << " " << 0 << std::endl;
      //std::cout << std::endl;
    }

  }

  // special treatment to missing points due to SISL
  if (ids.size() < polyline.size()) {
    std::cout << "Warning: SISL doesn't give enough closest points!" << std::endl;
    std::vector<dtkContinuousGeometryPrimitives::Point_2> params_tmp;
    for (std::size_t i = 0; i < ids.size() - 1; ++i) {
      params_tmp.push_back(params[i]);
      int delta = ids[i + 1] - ids[i];
      if (delta > 1) {
        for (int j = 1; j < ids[i + 1] - ids[i]; ++j) {
          dtkContinuousGeometryPrimitives::Point_2 param(0., 0.);
          param[0] = params[i][0] + (params[i + 1][0] - params[i][0])*(double(j)/double(delta));
          param[1] = params[i][1] + (params[i + 1][1] - params[i][1])*(double(j)/double(delta));
          params_tmp.push_back(param);
        }
      }
    }
    params_tmp.push_back(params.back());

    params.clear();
    params = params_tmp;
  }

}

void openNURBSBRepReader::cast_nurbs_surface(ON_Surface* surface, ON_NurbsSurface*& nurbs_surface)
{
  nurbs_surface = ON_NurbsSurface::Cast(surface);
  if (nurbs_surface == nullptr) {
    nurbs_surface = ON_NurbsSurface::New();
    bool converted = surface->GetNurbForm(*nurbs_surface);
    if (!converted) std::cerr << "Error: conversion to nurbs form." << std::endl;
  }
  if (!nurbs_surface->IsRational()) nurbs_surface->MakeRational();
}

bool openNURBSBRepReader::check_loop_valid(const ON_BrepLoop* loop, const ON_NurbsSurface* nurbs_surface, const ON_Brep* on_brep) const
{
  bool is_loop_valid = true;

  int u_degree = nurbs_surface->Order(0) - 1;
  int v_degree = nurbs_surface->Order(1) - 1;
  int num_cp_u = nurbs_surface->CVCount(0);
  int num_cp_v = nurbs_surface->CVCount(1);

  const double *u_knots = nurbs_surface->Knot(0);
  const double *v_knots = nurbs_surface->Knot(1);

  const double delta_u = u_knots[num_cp_u + u_degree - 2] - u_knots[0];
  const double delta_v = v_knots[num_cp_v + v_degree - 2] - v_knots[0];

  ON_2dPoint trim_start, trim_end;
  ON_BrepTrim *on_trim_0 = loop->Trim(0);
  on_brep->GetTrim2dStart(on_trim_0->m_trim_index, trim_start);
  on_brep->GetTrim2dEnd(on_trim_0->m_trim_index, trim_end);

  for (int l = 1; l < loop->m_ti.Count(); ++l) {
    ON_BrepTrim *on_trim = loop->Trim(l);
    ON_2dPoint trim_start_tmp, trim_end_tmp;
    on_brep->GetTrim2dStart(on_trim->m_trim_index, trim_start_tmp);
    on_brep->GetTrim2dEnd(on_trim->m_trim_index, trim_end_tmp);
    //if (trim_start_tmp.DistanceTo(trim_end) > 1.e-6)
    //  is_loop_valid = false;
    if (std::abs(trim_start_tmp[0] - trim_end[0]) > 0.25 * delta_u ||
        std::abs(trim_start_tmp[1] - trim_end[1]) > 0.25 * delta_v) {
      is_loop_valid = false;
    }
    trim_end = trim_end_tmp;
    if (l == loop->m_ti.Count() - 1) { // the last trim
      //if (trim_end_tmp.DistanceTo(trim_start) > 1.e-6)
      //  is_loop_valid = false;
      if (std::abs(trim_end_tmp[0] - trim_start[0] > 0.25 * delta_u) ||
          std::abs(trim_end_tmp[1] - trim_start[1] > 0.25 * delta_v)) {
        is_loop_valid = false;
      }
    }

    if (is_loop_valid == false)
      break;
  }

  if (!is_loop_valid) std::cout << "Error: the trim loop is NOT closed!" << std::endl;

  return is_loop_valid;
}

void openNURBSBRepReader::loop_end_parameters(const ON_BrepLoop* loop, const std::vector<SISLSurf*>& surfs_sisl, std::vector<K::Point_2>& parameters_loop) const
{
  std::vector<ON_3dPoint> vertices;
  vertices.reserve(loop->m_ti.Count());
  double dist_min = 0.;
  for (int l = 0; l < loop->m_ti.Count(); ++l) {
    ON_BrepTrim* trim = loop->Trim(l);

    //const ON_Curve* curve_2d = trim->TrimCurveOf();
    const ON_Curve* edge = trim->EdgeCurveOf();
    if (edge != nullptr) {
      ON_3dPoint vtx_start = edge->PointAtStart();
      ON_3dPoint vtx_end = edge->PointAtEnd();
      if (trim->m_bRev3d) { // if the trim is reversed
        vtx_start = edge->PointAtEnd();
        vtx_end = edge->PointAtStart();
      }

      if (l == 0) {
        vertices.push_back(vtx_start);
        vertices.push_back(vtx_end);
        dist_min = vtx_end.DistanceTo(vtx_start);
      }
      else if (l < loop->m_ti.Count() - 1) {
        vertices.push_back(vtx_end);
        double dist = vtx_end.DistanceTo(vtx_start);
        if (dist < dist_min) dist_min = dist;
      }
      else {
        if (vtx_end.DistanceTo(vertices.front()) > 1.e-6) {
          std::cout << "Error: vertices of a loop are not closed." << std::endl;
        }
        double dist = vtx_end.DistanceTo(vtx_start);
        if (dist < dist_min) dist_min = dist;
      }
    }
  }

  double tol = dist_min/1000.;

  //std::cout << "tolerance: " << tol << std::endl;

  //std::cout << "loop vertices: " << std::endl;
  //for (auto vtx : vertices) {
    //std::cout << vtx[0] << "," << vtx[1] << "," << vtx[2] << std::endl;
  //}
  //std::cout << std::endl;

  // compute the closest points on the surface
  std::vector< std::vector<K::Point_2> > params_vertices; // each vertex may have several parameters
  params_vertices.reserve(vertices.size());
  for (auto vtx : vertices) {
    double point[3];
    point[0] = vtx[0], point[1] = vtx[1], point[2] = vtx[2];

    std::vector<K::Point_2> params_tmp;
    double param_closest[2];
    param_closest[0] = 0., param_closest[1] = 0.;

    //std::cout << "vertex: " << point[0] << " " << point[1] << " " << point[2] << std::endl;
    //std::cout << "parameters of closest points: " << std::endl;

    int count = 0;
    double diff_min = 0.;

    for (auto surf_sisl : surfs_sisl) {
      double eps_co = 1.e-8;
      double eps_ge = 1.e-8;
      int numclopt = 0;
      double *pointpar = nullptr;
      int numclocr = 0;
      SISLIntcurve **clocurves;
      int stat = 0;

      s1954(surf_sisl, point, 3, eps_co, eps_ge, &numclopt, &pointpar, &numclocr, &clocurves, &stat);

      // closest points
      for (int i = 0; i < numclopt; ++i) {
        double param[2];
        param[0] = pointpar[2*i], param[1] = pointpar[2*i + 1];
        // evaluation
        int knot1 = 0, knot2 = 0;
        double p[3], normal[3];
        p[0] = 0., p[1] = 0., p[2] = 0.;
        normal[0] = 0., normal[1] = 0., normal[2] = 0.;
        int statj = 0;
        s1421(surf_sisl, 0, param, &knot1, &knot2, p, normal, &statj);

        double diff = std::sqrt((p[0] - point[0])*(p[0] - point[0]) + 
                                (p[1] - point[1])*(p[1] - point[1]) + 
                                (p[2] - point[2])*(p[2] - point[2]));
        
        //std::cout << param[0] << " " << param[1] << " " << 0 << std::endl;
        //std::cout << diff << std::endl;

        if (diff < tol) {
          params_tmp.push_back(K::Point_2(param[0], param[1]));
        }

        if (count == 0) param_closest[0] = param[0], param_closest[1] = param[1], diff_min = diff;
        else {
          if (diff < diff_min) param_closest[0] = param[0], param_closest[1] = param[1], diff_min = diff;
        } 

        count += 1;

      }

      // points on closest curves
      for (int i = 0; i < numclocr; ++i) {
        int ipoint = (*clocurves)->ipoint; // number of points defining the curve
        for (int j = 0; j < ipoint; ++j) {
          double param[2];
          param[0] = (*clocurves)->epar1[2*j], param[1] = (*clocurves)->epar1[2*j + 1];
          // evaluation
          int knot1 = 0, knot2 = 0;
          double p[3], normal[3];
          p[0] = 0., p[1] = 0., p[2] = 0.;
          normal[0] = 0., normal[1] = 0., normal[2] = 0.;
          int statj = 0;
          s1421(surf_sisl, 0, param, &knot1, &knot2, p, normal, &statj);

          double diff = std::sqrt((p[0] - point[0])*(p[0] - point[0]) + 
                                  (p[1] - point[1])*(p[1] - point[1]) + 
                                  (p[2] - point[2])*(p[2] - point[2]));

          //std::cout << param[0] << " " << param[1] << " " << 0 << std::endl;
          //std::cout << diff << std::endl;

          if (diff < tol) {
            params_tmp.push_back(K::Point_2(param[0], param[1]));
          }

          if (count == 0)
            param_closest[0] = param[0], param_closest[1] = param[1], diff_min = diff;
          else {
            if (diff < diff_min)
              param_closest[0] = param[0], param_closest[1] = param[1], diff_min = diff;
          }

          count += 1;
        }
      }
      if(pointpar != nullptr)
      {
        free(pointpar);
      }
      for(int i=0; i< numclocr; ++i)
      {
        freeIntcurve(clocurves[i]);
      }
      if(numclocr > 0)
      {
        free(clocurves);
      }
    }

    if (params_tmp.size() == 0) { // no good parameters obtained, use the one with least error
      params_tmp.push_back(K::Point_2(param_closest[0], param_closest[1]));
    }

    // remove duplicate parameters
    std::vector<K::Point_2> params;
    for (auto param_tmp : params_tmp) {
      if (params.size() == 0) params.push_back(param_tmp);
      else {
        bool is_duplicate = false;
        for (auto param : params) {
          double dist = std::sqrt(CGAL::squared_distance(param_tmp, param));
          if (dist < 1.e-6) {
            is_duplicate = true;
            break;
          };
        }
        if (!is_duplicate) params.push_back(param_tmp);
      }
    }

    params_vertices.push_back(params);

    //std::cout << std::endl;
  }

  //std::cout << "parameters of end vertices: " << std::endl;
  std::size_t num_comb = 1;
  for (auto params : params_vertices) {
    //for (auto param : params) {
      //std::cout << param[0] << " " << param[1] << " " << 0 << std::endl;
    //}
    num_comb *= params.size();
    if (params.size() == 0) std::cout << "NO parameter found!" << std::endl;
    //std::cout << std::endl;
  }

  // all possible combinations of parameters
  std::vector<std::vector<int>> ids_comb;
  std::vector<int> id_tmp;
  parameter_combinations(0, params_vertices, id_tmp, ids_comb);

  //std::cout << "parameter combinations: " << ids_comb.size() << std::endl;
  if (ids_comb.size() != num_comb) std::cout << "Wrong in combination!" << std::endl;

  // sort parameters of vertices
  std::vector<K::Point_2> params_sorted;
  double diff_min = 0.;
  int count = 0;
  for (auto ids : ids_comb) {
    CGAL::Polygon_2<K> polygon;
    std::vector<K::Point_2> parameters;
    for (std::size_t i = 0; i < vertices.size(); ++i) {
      int id = ids[i];
      K::Point_2 param = params_vertices[i][id];
      polygon.push_back(param);
      parameters.push_back(param);
      //std::cout << param[0] << "," << param[1] << "," << 0 << std::endl;
    }
    // check if the combination is valid
    if (polygon.is_simple()) {
      std::cout << "simple polygon" << std::endl;
      auto orientation = polygon.orientation();
      if (orientation < 0) std::cout << "clockwise" << std::endl;
      else if (orientation > 0) std::cout << "counter-clockwise" << std::endl;
      else std::cout << "colinear" << std::endl;

      // compute difference from original loop vertices
      double diff_sum = 0.;
      for (std::size_t ii = 0; ii < parameters.size(); ++ii) {
        double param[2];
        param[0] = parameters[ii][0], param[1] = parameters[ii][1];
        int knot1 = 0, knot2 = 0;
        double p[3], normal[3];
        p[0] = 0., p[1] = 0., p[2] = 0.;
        normal[0] = 0., normal[1] = 0., normal[2] = 0.;
        int statj = 0;
        s1421(surfs_sisl.back(), 0, param, &knot1, &knot2, p, normal, &statj);

        double v[3];
        v[0] = vertices[ii][0], v[1] = vertices[ii][1], v[2] = vertices[ii][2];

        diff_sum += std::sqrt((p[0] - v[0])*(p[0] - v[0]) + 
                              (p[1] - v[1])*(p[1] - v[1]) + 
                              (p[2] - v[2])*(p[2] - v[2]));
      }

      if (loop->m_type == ON_BrepLoop::TYPE::outer && orientation > 0) {
        if (count == 0) parameters_loop = parameters, diff_min = diff_sum;
        else {
          if (diff_sum < diff_min) parameters_loop = parameters, diff_min = diff_sum;
        }
        count += 1;
      }
      else if (loop->m_type == ON_BrepLoop::TYPE::inner && orientation < 0) {
        if (count == 0) parameters_loop = parameters, diff_min = diff_sum;
        else {
          if (diff_sum < diff_min) parameters_loop = parameters, diff_min = diff_sum;
        }
        count += 1;
      }
      else if (orientation == 0 && parameters.size() == 2) { // circle
        if (count == 0) parameters_loop = parameters, diff_min = diff_sum;
        else {
          if (diff_sum < diff_min) parameters_loop = parameters, diff_min = diff_sum;
        }
        count += 1;
      }
    }
    else std::cout << "NOT simple polygon" << std::endl;

    //std::cout << std::endl;
  }


}

void openNURBSBRepReader::parameter_combinations(std::size_t i, 
                                                 const std::vector< std::vector<K::Point_2> > params_vertices,
                                                 std::vector<int> id,
                                                 std::vector<std::vector<int>>& ids) const
{
  std::vector<K::Point_2> params = params_vertices[i];
  for (std::size_t ii = 0; ii < params.size(); ++ii) {
    std::vector<int> id_tmp = id;
    id_tmp.push_back(ii);
    if (i < params_vertices.size() - 1) {
      parameter_combinations(i + 1, params_vertices, id_tmp, ids);
    }
    else {
      ids.push_back(id_tmp);
    }
  }
}

//
// openNURBSBRepReader.cpp ends here
