// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "openNURBSBRepReader.h"
#include "openNURBSBRepReaderPlugin.h"
#include "dtkContinuousGeometry.h"

#include <dtkCore>

// ///////////////////////////////////////////////////////////////////
// openNURBSBRepReaderPlugin
// ///////////////////////////////////////////////////////////////////

void openNURBSBRepReaderPlugin::initialize(void)
{
    dtkContinuousGeometry::bRepReader::pluginFactory().record("openNURBSBRepReader", openNURBSBRepReaderCreator);
}

void openNURBSBRepReaderPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(openNURBSBRepReader)

//
// openNURBSBRepReaderPlugin.cpp ends here
