// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkBRepReader>
#include <dtkNurbsCurve>
#include <dtkContinuousGeometryUtils>

#include <openNURBSBRepReaderExport.h>

#include <opennurbs_without_warnings.h>
#include <sisl.h>

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Polygon_2.h>

class openNURBSBRepReaderPrivate;

class dtkBRep;

class OPENNURBSBREPREADER_EXPORT openNURBSBRepReader : public dtkBRepReader
{
private:
  typedef CGAL::Exact_predicates_inexact_constructions_kernel K;

public:
     openNURBSBRepReader(void);
    ~openNURBSBRepReader(void);

public:
    void  setInputBRepFilePath(const QString&) final;
    void  setInputDumpFilePath(const QString&) final;

    void run(void) final;

    dtkBRep* outputDtkBRep(void) const final;

private:
    void convert_nurbs_surface_to_sisl(const ON_NurbsSurface* nurbs_surface, SISLSurf*& surf_sisl);

    void trimming_curve_approx(const ON_NurbsCurve* nurbs_curve, std::vector<dtkContinuousGeometryPrimitives::Point_3>& polyline, const double tolerance = 0.01);

    void parametric_polyline_beta(const std::vector<SISLSurf*>& surfs_sisl, std::vector<dtkContinuousGeometryPrimitives::Point_3>& polyline, std::vector<dtkContinuousGeometryPrimitives::Point_2>& params,
                                  const ON_NurbsSurface* nurbs_surface, const ON_NurbsCurve* nurbs_curve_ref, bool is_reversed, double tol);

    void parametric_polyline_beta2(const std::vector<SISLSurf*>& surfs_sisl, std::vector<dtkContinuousGeometryPrimitives::Point_3>& polyline, std::vector<dtkContinuousGeometryPrimitives::Point_2>& params,
                                  const ON_NurbsSurface* nurbs_surface, const std::vector<K::Point_2>& parameters_end, bool is_reversed, double tol);

    void cast_nurbs_surface(ON_Surface* surface, ON_NurbsSurface*& nurbs_surface);

    bool check_loop_valid(const ON_BrepLoop* loop, const ON_NurbsSurface* nurbs_surface, const ON_Brep* on_brep) const;

    // compute parameters of end vertices of a loop
    void loop_end_parameters(const ON_BrepLoop* loop, const std::vector<SISLSurf*>& surfs_sisl, std::vector<K::Point_2>& parameters_loop) const;

private:
    // functions to check if two 2D segments intersect
    // check if point r is on segment pq, if p, q and r are colinear;
    bool on_segment(const K::Point_2& p, const K::Point_2& q, const K::Point_2& r);

    // determine the side of pq on which r lies
    int side_directed(const K::Point_2& p, const K::Point_2& q, const K::Point_2& r);

    // check if two 2D segments intersect
    bool do_intersect(const K::Point_2& p, const K::Point_2& q, const K::Point_2& r, const K::Point_2& s);

    void parameter_combinations(std::size_t i, 
                                const std::vector< std::vector<K::Point_2> > params_vertices,
                                std::vector<int> id,
                                std::vector<std::vector<int>>& ids) const;

protected:
    openNURBSBRepReaderPrivate *d;
};

// ///////////////////////////////////////////////////////////////////

inline dtkBRepReader* openNURBSBRepReaderCreator(void)
{
    return new openNURBSBRepReader();
}

//
// openNURBSBRepReader.h ends here
