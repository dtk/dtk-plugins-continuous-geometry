// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkBRepReader.h>

#include <openNURBSBRepReaderExport.h>

#include <dtkCore>

class OPENNURBSBREPREADER_EXPORT openNURBSBRepReaderPlugin : public dtkBRepReaderPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkBRepReaderPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.openNURBSBRepReaderPlugin" FILE "openNURBSBRepReaderPlugin.json")

public:
     openNURBSBRepReaderPlugin(void) {}
    ~openNURBSBRepReaderPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

//
// openNURBSBRepReaderPlugin.h ends here
