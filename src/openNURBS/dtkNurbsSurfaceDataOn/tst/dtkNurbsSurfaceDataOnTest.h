// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <QTest>

class dtkNurbsSurfaceDataOnTestCasePrivate;

class dtkNurbsSurfaceDataOnTestCase : public QObject
{
    Q_OBJECT

public:
    dtkNurbsSurfaceDataOnTestCase(void);
    ~dtkNurbsSurfaceDataOnTestCase(void);

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testUNbCps(void);
    void testVNbCps(void);
    void testUDegree(void);
    void testVDegree(void);
    void testControlPoint(void);
    void testWeight(void);
    void testUKnots(void);
    void testVKnots(void);
    void testSurfacePoint(void);
    void testSurfaceNormal(void);
    void testDecomposeToRationalBezierSurfaces(void);
    void testAabb(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);

private:
    dtkNurbsSurfaceDataOnTestCasePrivate* d;
};

//
// dtkNurbsSurfaceDataOnTest.h ends here
