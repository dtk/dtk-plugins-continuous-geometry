// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkNurbsSurfaceDataOnTest.h"

#include "dtkNurbsSurfaceDataOn.h"

#include <dtkContinuousGeometryUtils>
#include <dtkTest>

// ///////////////////////////////////////////////////////////////////
// D-Pointer implementation
// ///////////////////////////////////////////////////////////////////

class dtkNurbsSurfaceDataOnTestCasePrivate{
public:
    dtkNurbsSurfaceDataOn* nurbs_surface_data_0;
    dtkNurbsSurfaceDataOn* nurbs_surface_data_1;
    dtkNurbsSurfaceDataOn* nurbs_surface_data_2;
    dtkNurbsSurfaceDataOn* nurbs_surface_data_3;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkNurbsSurfaceDataOnTestCase::dtkNurbsSurfaceDataOnTestCase(void):d(new dtkNurbsSurfaceDataOnTestCasePrivate())
{
    dtkLogger::instance().attachConsole();
    dtkLogger::instance().setLevel(dtk::LogLevel::Trace);
}

dtkNurbsSurfaceDataOnTestCase::~dtkNurbsSurfaceDataOnTestCase(void)
{
}

void dtkNurbsSurfaceDataOnTestCase::initTestCase(void)
{
    //bi-degree 1,2
    std::size_t dim = 3;

    // ///////////////////////////////////////////////////////////////////

    d->nurbs_surface_data_0 = new dtkNurbsSurfaceDataOn();

    std::size_t nb_cp_u_0 = 2;
    std::size_t nb_cp_v_0 = 2;
    std::size_t order_u_0 = 2;
    std::size_t order_v_0 = 2;

    std::vector< double > knots_u_0(nb_cp_u_0 + order_u_0 - 2);
    /* 0. */ knots_u_0[0] = 0.; knots_u_0[1] = 1.; /* 1. */
    std::vector< double > knots_v_0(nb_cp_v_0 + order_v_0 - 2);
    /* 0. */ knots_v_0[0] = 0.; knots_v_0[1] = 1.; /* 1. */

    std::vector< double > cps_0((dim + 1) * nb_cp_u_0 * nb_cp_v_0);
    //x_00,         y_00,        z_00,            w_00
    cps_0[0] =  0.;   cps_0[1] = 0.; cps_0[2] =  0.; cps_0[3]  =  1.;
    //x_01,         y_01,        z_01,            w_01
    cps_0[4] =  0.;   cps_0[5] = 1.; cps_0[6] =  0.; cps_0[7]  =  1.;
    //x_10,         y_10,        z_10,            w_10
    cps_0[8] =  1.;   cps_0[9] = 0.; cps_0[10] = 0.; cps_0[11] =  1.;
    //x_11,         y_11,        z_11,            w_11
    cps_0[12] = 1.;  cps_0[13] = 1.; cps_0[14] = 0.; cps_0[15] =  1.;

    d->nurbs_surface_data_0->create(dim, nb_cp_u_0, nb_cp_v_0, order_u_0, order_v_0, knots_u_0.data(), knots_v_0.data(), cps_0.data());

    // ///////////////////////////////////////////////////////////////////

    d->nurbs_surface_data_1 = new dtkNurbsSurfaceDataOn();

    std::size_t nb_cp_u_1 = 3;
    std::size_t nb_cp_v_1 = 3;
    std::size_t order_u_1 = 2;
    std::size_t order_v_1 = 2;

    std::vector< double > knots_u_1(nb_cp_u_1 + order_u_1 - 2);
    /* 0. */ knots_u_1[0] = 0.; knots_u_1[1] = 0.5; knots_u_1[2] = 1.; /* 1. */
    std::vector< double > knots_v_1(nb_cp_v_1 + order_v_1 - 2);
    /* 0. */ knots_v_1[0] = 0.; knots_v_1[1] = 0.5; knots_v_1[2] = 1.; /* 1. */

    std::vector< double > cps_1((dim + 1) * nb_cp_u_1 * nb_cp_v_1);
    //x_00,         y_00,        z_00,            w_00
    cps_1[0] =  0.;   cps_1[1] = 0.; cps_1[2] =  0.; cps_1[3]  =  1.;
    //x_01,         y_01,        z_01,            w_01
    cps_1[4] =  0.;   cps_1[5] = 0.5; cps_1[6] =  0.; cps_1[7]  =  1.;
    //x_02,         y_02,        z_02,            w_02
    cps_1[8] =  0.;   cps_1[9] = 1.; cps_1[10] =  0.; cps_1[11]  =  1.;
    //x_10,         y_10,        z_10,            w_10
    cps_1[12] = 0.5; cps_1[13] = 0.; cps_1[14] = 0.; cps_1[15] =  1.;
    //x_11,         y_11,        z_11,            w_11
    cps_1[16] = 0.5; cps_1[17] = 0.5; cps_1[18] = 0.; cps_1[19] =  1.;
    //x_12,         y_12,        z_12,            w_12
    cps_1[20] = 0.5; cps_1[21] = 1.; cps_1[22] = 0.; cps_1[23] =  1.;
    //x_20,         y_20,        z_20,            w_20
    cps_1[24] =  1.;   cps_1[25] = 0.; cps_1[26] = 0.; cps_1[27] =  1.;
    //x_21,         y_21,        z_21,            w_21
    cps_1[28] = 1.;  cps_1[29] = 0.5; cps_1[30] = 0.; cps_1[31] =  1.;
    //x_22,         y_22,        z_22,            w_22
    cps_1[32] = 1.;  cps_1[33] = 1.; cps_1[34] = 0.; cps_1[35] =  1.;

    d->nurbs_surface_data_1->create(dim, nb_cp_u_1, nb_cp_v_1, order_u_1, order_v_1, knots_u_1.data(), knots_v_1.data(), cps_1.data());

    // ///////////////////////////////////////////////////////////////

    d->nurbs_surface_data_2 = new dtkNurbsSurfaceDataOn();

    std::size_t nb_cp_u_2 = 4;
    std::size_t nb_cp_v_2 = 5;
    std::size_t order_u_2 = 2;
    std::size_t order_v_2 = 3;

    std::vector< double > knots_u_2(nb_cp_u_2 + order_u_2 - 2);
    /* -2. */ knots_u_2[0] = -2.; knots_u_2[1] = 1.2; knots_u_2[2] = 2.7; knots_u_2[3] = 3.; /* 3. */
    std::vector< double > knots_v_2(nb_cp_v_2 + order_v_2 - 2);
    /* -1. */ knots_v_2[0] = -1.; knots_v_2[1] = -1.; knots_v_2[2] = 0.66; knots_v_2[3] = 1.33; knots_v_2[4] = 2.; knots_v_2[5] = 2.; /* 2. */

    std::vector< double > cps_2((dim + 1) * nb_cp_u_2 * nb_cp_v_2);
    //x_00,         y_00,        z_00,            w_00
    cps_2[0] =  0.;   cps_2[1] = 0.; cps_2[2] =   0.;  cps_2[3] =  1.;
    //x_01,         y_01,        z_01,            w_01
    cps_2[4] =  1.;   cps_2[5] = 0.; cps_2[6] =   1.;  cps_2[7] =  1.5;
    cps_2[8] =  2.;   cps_2[9] = 0.; cps_2[10] =  2.; cps_2[11] =  3.;
    cps_2[12] = 3.;  cps_2[13] = 0.;  cps_2[14] = 3.; cps_2[15] =  4.;
    //x_04,         y_04,        z_04,            w_04
    cps_2[16] = 4.;  cps_2[17] = 0.;  cps_2[18] = 3.; cps_2[19] =  3.;
    //x_10,         y_10,        z_10,            w_10
    cps_2[20] = 0.;  cps_2[21] = 1.; cps_2[22] =  0.;  cps_2[23] = 0.5;
    cps_2[24] = 1.;  cps_2[25] = 1.; cps_2[26] =  0.;  cps_2[27] = 2.;
    cps_2[28] = 2.;  cps_2[29] = 1.; cps_2[30] =  0.; cps_2[31] =  0.5;
    cps_2[32] = 3.;  cps_2[33] = 1.;  cps_2[34] = 0.; cps_2[35] =  1.;
    //x_14,         y_14,        z_14,            w_14
    cps_2[36] = 4.;  cps_2[37] = 2.; cps_2[38] =  0.;  cps_2[39] = 1.;
    //x_20,         y_20,        z_20,            w_20
    cps_2[40] = 0.;  cps_2[41] = 2.; cps_2[42] =  0.;  cps_2[43] = 1.;
    cps_2[44] = 1.;  cps_2[45] = 2.; cps_2[46] =  0.;  cps_2[47] = 1.;
    cps_2[48] = 2.;  cps_2[49] = 2.; cps_2[50] =  0.; cps_2[51] =  1.;
    cps_2[52] = 3.;  cps_2[53] = 2.;  cps_2[54] = 0.; cps_2[55] =  1.;
    cps_2[56] = 4.;  cps_2[57] = 2.;  cps_2[58] = 0.; cps_2[59] =  1.;
    cps_2[60] = 0.;  cps_2[61] = 3.; cps_2[62] =  0.;  cps_2[63] = 1.;
    cps_2[64] = 1.;  cps_2[65] = 3.; cps_2[66] =  0.;  cps_2[67] = 1.;
    cps_2[68] = 2.;  cps_2[69] = 3.; cps_2[70] =  0.; cps_2[71] =  1.;
    cps_2[72] = 3.;  cps_2[73] = 3.;  cps_2[74] = 0.; cps_2[75] =  1.;
    //x_34,         y_34,        z_34,            w_34
    cps_2[76] = 4.;  cps_2[77] = 3.;  cps_2[78] = 0.; cps_2[79] =  1.;

    d->nurbs_surface_data_2->create(dim, nb_cp_u_2, nb_cp_v_2, order_u_2, order_v_2, knots_u_2.data(), knots_v_2.data(), cps_2.data());

    d->nurbs_surface_data_3 = new dtkNurbsSurfaceDataOn();
    d->nurbs_surface_data_3->create(QFINDTESTDATA("data/nurbs_surface_data_on_1").toStdString());
}

void dtkNurbsSurfaceDataOnTestCase::init(void) {}

void dtkNurbsSurfaceDataOnTestCase::testUNbCps(void)
{
    QVERIFY(d->nurbs_surface_data_0->uNbCps() == 2);
    QVERIFY(d->nurbs_surface_data_1->uNbCps() == 3);
    QVERIFY(d->nurbs_surface_data_2->uNbCps() == 4);
}

void dtkNurbsSurfaceDataOnTestCase::testVNbCps(void)
{
    QVERIFY(d->nurbs_surface_data_0->vNbCps() == 2);
    QVERIFY(d->nurbs_surface_data_1->vNbCps() == 3);
    QVERIFY(d->nurbs_surface_data_2->vNbCps() == 5);
}

void dtkNurbsSurfaceDataOnTestCase::testUDegree(void)
{
    QVERIFY(d->nurbs_surface_data_0->uDegree() == 1);
    QVERIFY(d->nurbs_surface_data_1->uDegree() == 1);
    QVERIFY(d->nurbs_surface_data_2->uDegree() == 1);
}

void dtkNurbsSurfaceDataOnTestCase::testVDegree(void)
{
    QVERIFY(d->nurbs_surface_data_0->vDegree() == 1);
    QVERIFY(d->nurbs_surface_data_1->vDegree() == 1);
    QVERIFY(d->nurbs_surface_data_2->vDegree() == 2);
}

void dtkNurbsSurfaceDataOnTestCase::testControlPoint(void)
{
    dtkContinuousGeometryPrimitives::Point_3 point(0., 0., 0.);

    // ///////////////////////////////////////////////////////////////

    d->nurbs_surface_data_0->controlPoint(0, 0, point.data());
    QVERIFY(point[0] == 0.); QVERIFY(point[1] == 0.); QVERIFY(point[2] == 0.);
    d->nurbs_surface_data_0->controlPoint(0, 1, point.data());
    QVERIFY(point[0] == 0.); QVERIFY(point[1] == 1.); QVERIFY(point[2] == 0.);
    d->nurbs_surface_data_0->controlPoint(1, 0, point.data());
    QVERIFY(point[0] == 1.); QVERIFY(point[1] == 0.); QVERIFY(point[2] == 0.);
    d->nurbs_surface_data_0->controlPoint(1, 1, point.data());
    QVERIFY(point[0] == 1.); QVERIFY(point[1] == 1.); QVERIFY(point[2] == 0.);

    // ///////////////////////////////////////////////////////////////////

    d->nurbs_surface_data_1->controlPoint(0, 0, point.data());
    QVERIFY(point[0] == 0.); QVERIFY(point[1] == 0.); QVERIFY(point[2] == 0.);
    d->nurbs_surface_data_1->controlPoint(0, 1, point.data());
    QVERIFY(point[0] == 0.); QVERIFY(point[1] == 0.5); QVERIFY(point[2] == 0.);
    d->nurbs_surface_data_1->controlPoint(0, 2, point.data());
    QVERIFY(point[0] == 0.); QVERIFY(point[1] == 1.); QVERIFY(point[2] == 0.);

    d->nurbs_surface_data_1->controlPoint(1, 0, point.data());
    QVERIFY(point[0] == 0.5); QVERIFY(point[1] == 0.); QVERIFY(point[2] == 0.);
    d->nurbs_surface_data_1->controlPoint(1, 1, point.data());
    QVERIFY(point[0] == 0.5); QVERIFY(point[1] == 0.5); QVERIFY(point[2] == 0.);
    d->nurbs_surface_data_1->controlPoint(1, 2, point.data());
    QVERIFY(point[0] == 0.5); QVERIFY(point[1] == 1.); QVERIFY(point[2] == 0.);

    d->nurbs_surface_data_1->controlPoint(2, 0, point.data());
    QVERIFY(point[0] == 1.); QVERIFY(point[1] == 0.); QVERIFY(point[2] == 0.);
    d->nurbs_surface_data_1->controlPoint(2, 1, point.data());
    QVERIFY(point[0] == 1.); QVERIFY(point[1] == 0.5); QVERIFY(point[2] == 0.);
    d->nurbs_surface_data_1->controlPoint(2, 2, point.data());
    QVERIFY(point[0] == 1.); QVERIFY(point[1] == 1.); QVERIFY(point[2] == 0.);

    // ///////////////////////////////////////////////////////////////

    d->nurbs_surface_data_2->controlPoint(0, 0, point.data());
    QVERIFY(point[0] == 0.); QVERIFY(point[1] == 0.); QVERIFY(point[2] == 0.);
    d->nurbs_surface_data_2->controlPoint(0, 1, point.data());
    QVERIFY(point[0] == 1.); QVERIFY(point[1] == 0.); QVERIFY(point[2] == 1.);
    d->nurbs_surface_data_2->controlPoint(0, 2, point.data());
    QVERIFY(point[0] == 2.); QVERIFY(point[1] == 0.); QVERIFY(point[2] == 2.);
    d->nurbs_surface_data_2->controlPoint(0, 3, point.data());
    QVERIFY(point[0] == 3.); QVERIFY(point[1] == 0.); QVERIFY(point[2] == 3.);
    d->nurbs_surface_data_2->controlPoint(0, 4, point.data());
    QVERIFY(point[0] == 4.); QVERIFY(point[1] == 0.); QVERIFY(point[2] == 3.);

    d->nurbs_surface_data_2->controlPoint(1, 0, point.data());
    QVERIFY(point[0] == 0.); QVERIFY(point[1] == 1.); QVERIFY(point[2] == 0.);
    d->nurbs_surface_data_2->controlPoint(1, 1, point.data());
    QVERIFY(point[0] == 1.); QVERIFY(point[1] == 1.); QVERIFY(point[2] == 0.);
    d->nurbs_surface_data_2->controlPoint(1, 2, point.data());
    QVERIFY(point[0] == 2.); QVERIFY(point[1] == 1.); QVERIFY(point[2] == 0.);
    d->nurbs_surface_data_2->controlPoint(1, 3, point.data());
    QVERIFY(point[0] == 3.); QVERIFY(point[1] == 1.); QVERIFY(point[2] == 0.);
    d->nurbs_surface_data_2->controlPoint(1, 4, point.data());
    QVERIFY(point[0] == 4.); QVERIFY(point[1] == 2.); QVERIFY(point[2] == 0.);

    d->nurbs_surface_data_2->controlPoint(2, 0, point.data());
    QVERIFY(point[0] == 0.); QVERIFY(point[1] == 2.); QVERIFY(point[2] == 0.);
    d->nurbs_surface_data_2->controlPoint(2, 1, point.data());
    QVERIFY(point[0] == 1.); QVERIFY(point[1] == 2.); QVERIFY(point[2] == 0.);
    d->nurbs_surface_data_2->controlPoint(2, 2, point.data());
    QVERIFY(point[0] == 2.); QVERIFY(point[1] == 2.); QVERIFY(point[2] == 0.);
    d->nurbs_surface_data_2->controlPoint(2, 3, point.data());
    QVERIFY(point[0] == 3.); QVERIFY(point[1] == 2.); QVERIFY(point[2] == 0.);
    d->nurbs_surface_data_2->controlPoint(2, 4, point.data());
    QVERIFY(point[0] == 4.); QVERIFY(point[1] == 2.); QVERIFY(point[2] == 0.);

    d->nurbs_surface_data_2->controlPoint(3, 0, point.data());
    QVERIFY(point[0] == 0.); QVERIFY(point[1] == 3.); QVERIFY(point[2] == 0.);
    d->nurbs_surface_data_2->controlPoint(3, 1, point.data());
    QVERIFY(point[0] == 1.); QVERIFY(point[1] == 3.); QVERIFY(point[2] == 0.);
    d->nurbs_surface_data_2->controlPoint(3, 2, point.data());
    QVERIFY(point[0] == 2.); QVERIFY(point[1] == 3.); QVERIFY(point[2] == 0.);
    d->nurbs_surface_data_2->controlPoint(3, 3, point.data());
    QVERIFY(point[0] == 3.); QVERIFY(point[1] == 3.); QVERIFY(point[2] == 0.);
    d->nurbs_surface_data_2->controlPoint(3, 4, point.data());
    QVERIFY(point[0] == 4.); QVERIFY(point[1] == 3.); QVERIFY(point[2] == 0.);
}

void dtkNurbsSurfaceDataOnTestCase::testWeight(void)
{
    double w = 0.;

    // ///////////////////////////////////////////////////////////////

    d->nurbs_surface_data_0->weight(0, 0, &w);
    QVERIFY(w == 1.);
    d->nurbs_surface_data_0->weight(0, 1, &w);
    QVERIFY(w == 1.);
    d->nurbs_surface_data_0->weight(1, 0, &w);
    QVERIFY(w == 1.);
    d->nurbs_surface_data_0->weight(1, 1, &w);
    QVERIFY(w == 1.);

    // ///////////////////////////////////////////////////////////////

    d->nurbs_surface_data_1->weight(0, 0, &w);
    QVERIFY(w == 1.);
    d->nurbs_surface_data_1->weight(0, 1, &w);
    QVERIFY(w == 1.);
    d->nurbs_surface_data_1->weight(0, 2, &w);
    QVERIFY(w == 1.);
    d->nurbs_surface_data_1->weight(1, 0, &w);
    QVERIFY(w == 1.);
    d->nurbs_surface_data_1->weight(1, 1, &w);
    QVERIFY(w == 1.);
    d->nurbs_surface_data_1->weight(1, 2, &w);
    QVERIFY(w == 1.);
    d->nurbs_surface_data_1->weight(2, 0, &w);
    QVERIFY(w == 1.);
    d->nurbs_surface_data_1->weight(2, 1, &w);
    QVERIFY(w == 1.);
    d->nurbs_surface_data_1->weight(2, 2, &w);
    QVERIFY(w == 1.);

    // ///////////////////////////////////////////////////////////////////

    d->nurbs_surface_data_2->weight(0, 0, &w);
    QVERIFY(w == 1.);
    d->nurbs_surface_data_2->weight(0, 1, &w);
    QVERIFY(w == 1.5);
    d->nurbs_surface_data_2->weight(0, 2, &w);
    QVERIFY(w == 3.);
    d->nurbs_surface_data_2->weight(0, 3, &w);
    QVERIFY(w == 4.);
    d->nurbs_surface_data_2->weight(0, 4, &w);
    QVERIFY(w == 3.);

    d->nurbs_surface_data_2->weight(1, 0, &w);
    QVERIFY(w == 0.5);
    d->nurbs_surface_data_2->weight(1, 1, &w);
    QVERIFY(w == 2.);
    d->nurbs_surface_data_2->weight(1, 2, &w);
    QVERIFY(w == 0.5);
    d->nurbs_surface_data_2->weight(1, 3, &w);
    QVERIFY(w == 1.);
    d->nurbs_surface_data_2->weight(1, 4, &w);
    QVERIFY(w == 1.);

    d->nurbs_surface_data_2->weight(2, 0, &w);
    QVERIFY(w == 1.);
    d->nurbs_surface_data_2->weight(2, 1, &w);
    QVERIFY(w == 1.);
    d->nurbs_surface_data_2->weight(2, 2, &w);
    QVERIFY(w == 1.);
    d->nurbs_surface_data_2->weight(2, 3, &w);
    QVERIFY(w == 1.);
    d->nurbs_surface_data_2->weight(2, 4, &w);
    QVERIFY(w == 1.);

    d->nurbs_surface_data_2->weight(3, 0, &w);
    QVERIFY(w == 1.);
    d->nurbs_surface_data_2->weight(3, 1, &w);
    QVERIFY(w == 1.);
    d->nurbs_surface_data_2->weight(3, 2, &w);
    QVERIFY(w == 1.);
    d->nurbs_surface_data_2->weight(3, 3, &w);
    QVERIFY(w == 1.);
    d->nurbs_surface_data_2->weight(3, 4, &w);
    QVERIFY(w == 1.);
}

void dtkNurbsSurfaceDataOnTestCase::testUKnots(void)
{
    std::vector<double> u_knots_0(d->nurbs_surface_data_0->uNbCps() + d->nurbs_surface_data_0->uDegree() - 1);
    QVERIFY(d->nurbs_surface_data_0->uNbCps() + d->nurbs_surface_data_0->uDegree() - 1 == 2);
    d->nurbs_surface_data_0->uKnots(u_knots_0.data());
    QVERIFY(u_knots_0[0] == 0.);
    QVERIFY(u_knots_0[1] == 1.);
    QVERIFY(d->nurbs_surface_data_0->uKnots()[0] == 0.);
    QVERIFY(d->nurbs_surface_data_0->uKnots()[1] == 1.);

    // ///////////////////////////////////////////////////////////////////

    std::vector<double> u_knots_1(d->nurbs_surface_data_1->uNbCps() + d->nurbs_surface_data_1->uDegree() - 1);
    QVERIFY(d->nurbs_surface_data_1->uNbCps() + d->nurbs_surface_data_1->uDegree() - 1 == 3);
    d->nurbs_surface_data_1->uKnots(u_knots_1.data());
    QVERIFY(u_knots_1[0] == 0.);
    QVERIFY(u_knots_1[1] == 0.5);
    QVERIFY(u_knots_1[2] == 1.);
    QVERIFY(d->nurbs_surface_data_1->uKnots()[0] == 0.);
    QVERIFY(d->nurbs_surface_data_1->uKnots()[1] == 0.5);
    QVERIFY(d->nurbs_surface_data_1->uKnots()[2] == 1.);

    // ///////////////////////////////////////////////////////////////////

    std::vector<double> u_knots_2(d->nurbs_surface_data_2->uNbCps() + d->nurbs_surface_data_2->uDegree() - 1);
    QVERIFY(d->nurbs_surface_data_2->uNbCps() + d->nurbs_surface_data_2->uDegree() - 1 == 4);
    d->nurbs_surface_data_2->uKnots(u_knots_2.data());
    QVERIFY(u_knots_2[0] == -2.);
    QVERIFY(u_knots_2[1] == 1.2);
    QVERIFY(u_knots_2[2] == 2.7);
    QVERIFY(u_knots_2[3] == 3.);
    QVERIFY(d->nurbs_surface_data_2->uKnots()[0] == -2.);
    QVERIFY(d->nurbs_surface_data_2->uKnots()[1] == 1.2);
    QVERIFY(d->nurbs_surface_data_2->uKnots()[2] == 2.7);
    QVERIFY(d->nurbs_surface_data_2->uKnots()[3] == 3.);
}

void dtkNurbsSurfaceDataOnTestCase::testVKnots(void)
{
    std::vector<double> v_knots_0(d->nurbs_surface_data_0->vNbCps() + d->nurbs_surface_data_0->vDegree() - 1);
    QVERIFY(d->nurbs_surface_data_0->vNbCps() + d->nurbs_surface_data_0->vDegree() - 1 == 2);
    d->nurbs_surface_data_0->vKnots(v_knots_0.data());
    QVERIFY(v_knots_0[0] == 0.);
    QVERIFY(v_knots_0[1] == 1.);
    QVERIFY(d->nurbs_surface_data_0->vKnots()[0] == 0);
    QVERIFY(d->nurbs_surface_data_0->vKnots()[1] == 1.);

    // ///////////////////////////////////////////////////////////////////

    std::vector<double> v_knots_1(d->nurbs_surface_data_1->vNbCps() + d->nurbs_surface_data_1->vDegree() - 1);
    QVERIFY(d->nurbs_surface_data_1->vNbCps() + d->nurbs_surface_data_1->vDegree() - 1 == 3);
    d->nurbs_surface_data_1->vKnots(v_knots_1.data());
    QVERIFY(v_knots_1[0] == 0.);
    QVERIFY(v_knots_1[1] == 0.5);
    QVERIFY(v_knots_1[2] == 1.);
    QVERIFY(d->nurbs_surface_data_1->vKnots()[0] == 0);
    QVERIFY(d->nurbs_surface_data_1->vKnots()[1] == 0.5);
    QVERIFY(d->nurbs_surface_data_1->vKnots()[2] == 1.);

    // ///////////////////////////////////////////////////////////////

    std::vector<double> v_knots_2(d->nurbs_surface_data_2->vNbCps() + d->nurbs_surface_data_2->vDegree() - 1);
    QVERIFY(d->nurbs_surface_data_2->vNbCps() + d->nurbs_surface_data_2->vDegree() - 1 == 6);
    d->nurbs_surface_data_2->vKnots(v_knots_2.data());
    QVERIFY(v_knots_2[0] == -1.);
    QVERIFY(v_knots_2[1] == -1.);
    QVERIFY(v_knots_2[2] == 0.66);
    QVERIFY(v_knots_2[3] == 1.33);
    QVERIFY(v_knots_2[4] == 2.);
    QVERIFY(v_knots_2[5] == 2.);
    QVERIFY(d->nurbs_surface_data_2->vKnots()[0] == -1.);
    QVERIFY(d->nurbs_surface_data_2->vKnots()[1] == -1.);
    QVERIFY(d->nurbs_surface_data_2->vKnots()[2] == 0.66);
    QVERIFY(d->nurbs_surface_data_2->vKnots()[3] == 1.33);
    QVERIFY(d->nurbs_surface_data_2->vKnots()[4] == 2.);
    QVERIFY(d->nurbs_surface_data_2->vKnots()[5] == 2.);
}

void dtkNurbsSurfaceDataOnTestCase::testSurfacePoint(void)
{
    dtkContinuousGeometryTools::Point_3 point(0., 0., 0.);

    // ///////////////////////////////////////////////////////////////

    d->nurbs_surface_data_0->evaluatePoint(0.25, 0.65, point.data());
    QVERIFY(point[0] > 0.25 - 10e-5 && point[0] < 0.25 + 10e-5);
    QVERIFY(point[1] > 0.65 - 10e-5 && point[1] < 0.65 + 10e-5);
    QVERIFY(point[2] > 0. - 10e-5 && point[2] < 0. + 10e-5);

    // ///////////////////////////////////////////////////////////////

    d->nurbs_surface_data_1->evaluatePoint(0.25, 0.65, point.data());
    QVERIFY(point[0] > 0.25 - 10e-5 && point[0] < 0.25 + 10e-5);
    QVERIFY(point[1] > 0.65 - 10e-5 && point[1] < 0.65 + 10e-5);
    QVERIFY(point[2] > 0. - 10e-5 && point[2] < 0. + 10e-5);

    // ///////////////////////////////////////////////////////////////

    d->nurbs_surface_data_2->evaluatePoint(0.25, 0.65, point.data());
    QVERIFY(point[0] > 1.61461 - 10e-5 && point[0] < 1.61461 + 10e-5);
    QVERIFY(point[1] > 0.466635 - 10e-5 && point[1] < 0.466635 + 10e-5);
    QVERIFY(point[2] > 0.974034 - 10e-5 && point[2] < 0.974034 + 10e-5);
}

//To ameliorate with dataSurfacePoint and several pints to test
void dtkNurbsSurfaceDataOnTestCase::testSurfaceNormal(void)
{
    dtkContinuousGeometryTools::Vector_3 normal(0., 0., 0.);

    // ///////////////////////////////////////////////////////////////

    d->nurbs_surface_data_0->evaluateNormal(0.5, 0.5, normal.data());
    QVERIFY(normal[0] > 0. - 10e-5 && normal[0] < 0. + 10e-5);
    QVERIFY(normal[1] > 0. - 10e-5 && normal[1] < 0. + 10e-5);
    QVERIFY(normal[2] > 1. - 10e-5 && normal[2] < 1. + 10e-5);

    // ///////////////////////////////////////////////////////////////

    d->nurbs_surface_data_1->evaluateNormal(0.5, 0.5, normal.data());
    QVERIFY(normal[0] > 0. - 10e-5 && normal[0] < 0. + 10e-5);
    QVERIFY(normal[1] > 0. - 10e-5 && normal[1] < 0. + 10e-5);
    QVERIFY(normal[2] > 1. - 10e-5 && normal[2] < 1. + 10e-5);

    // ///////////////////////////////////////////////////////////////

    d->nurbs_surface_data_2->evaluateNormal(0.5, 0.5, normal.data());
    QVERIFY(normal[0] > 0.202839 - 10e-5 && normal[0] < 0.202839 + 10e-5);
    QVERIFY(normal[1] > -0.823 - 10e-5 && normal[1] < -0.823 + 10e-5);
    QVERIFY(normal[2] > -0.530592 - 10e-5 && normal[2] < -0.530592 + 10e-5);
}

void dtkNurbsSurfaceDataOnTestCase::testDecomposeToRationalBezierSurfaces(void)
{
    std::vector< std::pair<dtkRationalBezierSurface*, double*> > bezier_surfaces_0;
    d->nurbs_surface_data_0->decomposeToRationalBezierSurfaces(bezier_surfaces_0);
    QVERIFY(bezier_surfaces_0.size() == 1);
    QVERIFY(bezier_surfaces_0.front().second[0] == 0.);
    QVERIFY(bezier_surfaces_0.front().second[1] == 0.);
    QVERIFY(bezier_surfaces_0.front().second[2] == 1.);
    QVERIFY(bezier_surfaces_0.front().second[3] == 1.);

    for (auto it = bezier_surfaces_0.begin(); it != bezier_surfaces_0.end(); ++it) {
        delete it->first;
        delete[] it->second;
    }

    // ///////////////////////////////////////////////////////////////////

    std::vector< std::pair<dtkRationalBezierSurface*, double*> > bezier_surfaces_1;
    d->nurbs_surface_data_1->decomposeToRationalBezierSurfaces(bezier_surfaces_1);
    QVERIFY(bezier_surfaces_1.size() == 4);
    QVERIFY(bezier_surfaces_1.front().second[0] == 0.);
    QVERIFY(bezier_surfaces_1.front().second[1] == 0.);
    QVERIFY(bezier_surfaces_1.front().second[2] == 0.5);
    QVERIFY(bezier_surfaces_1.front().second[3] == 0.5);

    for (auto it = bezier_surfaces_1.begin(); it != bezier_surfaces_1.end(); ++it) {
        delete it->first;
        delete[] it->second;
    }

    // ///////////////////////////////////////////////////////////////

    std::vector< std::pair<dtkRationalBezierSurface*, double*> > bezier_surfaces_2;
    d->nurbs_surface_data_2->decomposeToRationalBezierSurfaces(bezier_surfaces_2);
    QVERIFY(bezier_surfaces_2.size() == 9);
    QVERIFY(bezier_surfaces_2.front().second[0] == -2.);
    QVERIFY(bezier_surfaces_2.front().second[1] == -1.);
    QVERIFY(bezier_surfaces_2.front().second[2] == 1.2);
    QVERIFY(bezier_surfaces_2.front().second[3] == 0.66);

    for (auto it = bezier_surfaces_2.begin(); it != bezier_surfaces_2.end(); ++it) {
        delete it->first;
        delete[] it->second;
    }
}

void dtkNurbsSurfaceDataOnTestCase::testAabb(void)
{
    dtkContinuousGeometryPrimitives::AABB_3 aabb_0(0., 0., 0., 0., 0., 0.);
    d->nurbs_surface_data_0->aabb(aabb_0.data());

    QVERIFY(aabb_0[0] == 0.);
    QVERIFY(aabb_0[1] == 0.);
    QVERIFY(aabb_0[2] == 0.);
    QVERIFY(aabb_0[3] == 1.);
    QVERIFY(aabb_0[4] == 1.);
    QVERIFY(aabb_0[5] == 0.);

    // ///////////////////////////////////////////////////////////////

    dtkContinuousGeometryPrimitives::AABB_3 aabb_1(0., 0., 0., 0., 0., 0.);
    d->nurbs_surface_data_1->aabb(aabb_1.data());

    QVERIFY(aabb_1[0] == 0.);
    QVERIFY(aabb_1[1] == 0.);
    QVERIFY(aabb_1[2] == 0.);
    QVERIFY(aabb_1[3] == 1.);
    QVERIFY(aabb_1[4] == 1.);
    QVERIFY(aabb_1[5] == 0.);

    // ///////////////////////////////////////////////////////////////

    dtkContinuousGeometryPrimitives::AABB_3 aabb_2(0., 0., 0., 0., 0., 0.);
    d->nurbs_surface_data_2->aabb(aabb_2.data());

    QVERIFY(aabb_2[0] == 0.);
    QVERIFY(aabb_2[1] == 0.);
    QVERIFY(aabb_2[2] == 0.);
    QVERIFY(aabb_2[3] == 4.);
    QVERIFY(aabb_2[4] == 3.);
    QVERIFY(aabb_2[5] == 3.);
}

void dtkNurbsSurfaceDataOnTestCase::cleanup(void)
{

}

void dtkNurbsSurfaceDataOnTestCase::cleanupTestCase(void)
{
    delete d->nurbs_surface_data_0;
    d->nurbs_surface_data_0 = nullptr;
    delete d->nurbs_surface_data_1;
    d->nurbs_surface_data_1 = nullptr;
    delete d->nurbs_surface_data_2;
    d->nurbs_surface_data_2 = nullptr;
    delete d->nurbs_surface_data_3;
    d->nurbs_surface_data_3 = nullptr;
}

DTKTEST_MAIN_NOGUI(dtkNurbsSurfaceDataOnTest, dtkNurbsSurfaceDataOnTestCase)

//
// dtkNurbsSurfaceDataOnTest.cpp ends here
