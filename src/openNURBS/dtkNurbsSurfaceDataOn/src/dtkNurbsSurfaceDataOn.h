// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <dtkContinuousGeometry.h>

#include <dtkNurbsSurfaceDataOnExport.h>

#include <dtkAbstractNurbsSurfaceData>
#include <dtkAbstractRationalBezierSurfaceData>

#include <dtkRationalBezierSurface>
#include "opennurbs_without_warnings.h"

class ON_NurbsSurface;

class DTKNURBSSURFACEDATAON_EXPORT dtkNurbsSurfaceDataOn final : public dtkAbstractNurbsSurfaceData
{
public:
    dtkNurbsSurfaceDataOn(void);
    ~dtkNurbsSurfaceDataOn(void) final ;

public:
    void create(std::size_t dim, std::size_t nb_cp_u, std::size_t nb_cp_v, std::size_t order_u, std::size_t order_v, const double *knots_u, const double *knots_v, const double *cps) const override;
    void create(std::string path) const override;
    void create(const ON_NurbsSurface& nurbs_surface);

public:
    std::size_t uDegree(void) const override;
    std::size_t vDegree(void) const override;

    std::size_t uNbCps(void) const override;
    std::size_t vNbCps(void) const override;

    std::size_t uNbKnots(void) const override;
    std::size_t vNbKnots(void) const override;

    std::size_t dim(void) const override;

    void controlPoint(std::size_t i, std::size_t j, double *r_cp) const override;
    void weight(std::size_t i, std::size_t j, double *r_w) const override;

    void uKnots(double *r_u_knots) const override;
    void vKnots(double *r_v_knots) const  override;

    const double *uKnots(void) const override;
    const double *vKnots(void) const override;

    double uPeriod(void) const override;
    double vPeriod(void) const override;

    bool isPointCulled(const dtkContinuousGeometryPrimitives::Point_2& point) const override;

public:
    void evaluatePoint(double p_u, double p_v, double *r_point) const override;
    void evaluateNormal(double p_u, double p_v, double *r_normal) const override;
    void evaluate1stDer(double p_u, double p_v, double *r_point, double *r_u_deriv, double* r_v_deriv) const override;
    void evaluate1stDer(double p_u, double p_v, double *r_u_deriv, double *r_v_deriv) const override;

 public:
    void decomposeToRationalBezierSurfaces(std::vector< std::pair< dtkRationalBezierSurface *, double * > >& r_rational_bezier_surfaces) const override;

 public:
    void aabb(double* r_aabb) const override;
    void extendedAabb(double* r_aabb, double factor) const override;

 public:
    void print(std::ostream& stream) const override;

public:
    dtkNurbsSurfaceDataOn* clone(void) const override;

private :
    mutable ON_NurbsSurface* d;
};


// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

inline dtkAbstractNurbsSurfaceData *dtkNurbsSurfaceDataOnCreator(void)
{
    return new dtkNurbsSurfaceDataOn();
}

//
// dtkNurbsSurfaceDataOn.h ends here
