// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#include "dtkNurbsSurfaceDataOn.h"
#include "dtkNurbsSurfaceDataOnPlugin.h"

#include <dtkCore>
#include <dtkContinuousGeometry>

// ///////////////////////////////////////////////////////////////////
// dtkNurbsSurfaceDataOnPlugin
// ///////////////////////////////////////////////////////////////////

void dtkNurbsSurfaceDataOnPlugin::initialize(void)
{
    dtkContinuousGeometry::abstractNurbsSurfaceData::pluginFactory().record("dtkNurbsSurfaceDataOn", dtkNurbsSurfaceDataOnCreator);
}

void dtkNurbsSurfaceDataOnPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkNurbsSurfaceDataOn)

//
// dtkNurbsSurfaceDataOnPlugin.cpp ends here
