// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <QtCore>

#include <dtkContinuousGeometry>

#include <dtkAbstractNurbsSurfaceData>

class dtkNurbsSurfaceDataOnPlugin : public dtkAbstractNurbsSurfaceDataPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkAbstractNurbsSurfaceDataPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkNurbsSurfaceDataOnPlugin" FILE "dtkNurbsSurfaceDataOnPlugin.json")

public:
     dtkNurbsSurfaceDataOnPlugin(void) {}
    ~dtkNurbsSurfaceDataOnPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

//
// dtkNurbsSurfaceDataOnPlugin.h ends here
