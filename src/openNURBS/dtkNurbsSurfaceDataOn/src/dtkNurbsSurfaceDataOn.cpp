// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkNurbsSurfaceDataOn.h"

#include <dtkTrimLoop>

#include <dtkRationalBezierSurfaceDataOn.h>

#include "opennurbs_nurbssurface.h"

#include <cassert>

/*!
  \class dtkNurbsSurfaceDataOn
  \inmodule dtkPluginsContinuousGeometry
  \brief dtkNurbsSurfaceDataOn is an openNURBS implementation of the concept dtkAbstractNurbsSurfaceData.

  The following lines of code show how to instanciate and use the class in pratice:
  \code
  dtkAbstractNurbsSurfaceData *nurbs_surface_data = dtkContinuousGeometry::abstractNurbsSurfaceData::pluginFactory().create("dtkNurbsSurfaceDataOn");
  dtkNurbsSurface *nurbs_surface = new dtkNurbsSurface(nurbs_surface_data);
  nurbs_surface->create(...);
  \endcode
*/

/*! \fn dtkNurbsSurfaceDataOn::dtkNurbsSurfaceDataOn(void)
  Instanciates the structure.

  To add the content, see \l create(std::size_t dim, std::size_t nb_cp_u, std::size_t nb_cp_v, std::size_t order_u, std::size_t order_v, const double *knots_u, const double *knots_v, const double *cps) const.
*/
dtkNurbsSurfaceDataOn::dtkNurbsSurfaceDataOn(void) : d(nullptr)
{

}

/*! \fn dtkNurbsSurfaceDataOn::~dtkNurbsSurfaceDataOn(void)
  Destroys the instance.
*/
dtkNurbsSurfaceDataOn::~dtkNurbsSurfaceDataOn(void)
{
    for (auto it = m_trim_loops.begin(); it != m_trim_loops.end(); ++it) {
        delete (*it);
    }
    m_trim_loops.clear();
    delete d;
}

/*! \fn dtkNurbsSurfaceDataOn::create(const ON_NurbsSurface& nurbs_surface)
  Not available in the dtkAbstractNurbsSurfaceData.

  Creates the NURBS surface by providing a NURBS surface from the openNURBS API.

  \a nurbs_surface : the NURBS surface to copy.
*/
void dtkNurbsSurfaceDataOn::create(const ON_NurbsSurface& nurbs_surface)
{
    d = ON_NurbsSurface::New(nurbs_surface);
    assert(d->IsValid());
}

/*! \fn void dtkNurbsSurfaceDataOn::create(std::size_t dim, std::size_t nb_cp_u, std::size_t nb_cp_v, std::size_t order_u, std::size_t order_v, const double *knots_u, const double *knots_v, const double *cps) const
  Creates a NURBS surface by providing the required content.

  \a dim : dimension of the space in which lies the surface

  \a nb_cp_u : number of control points along the "u" direction

  \a nb_cp_v : number of control points along the "v" direction

  \a order_u : order(degree + 1) in the "u" direction

  \a order_v : order(degree + 1) in the "v" direction

  \a knots_u : array containing the knots in the "u" direction

  \a knots_v : array containing the knots in the "v" direction

  \a cps : array containing the weighted control points, secified as : [x_00, y_00, z_00, w_00, x_01, y_01, ...]

  \a knots_u, \a knots_v, and \a cps are copied.
*/
void dtkNurbsSurfaceDataOn::create(std::size_t dim, std::size_t nb_cp_u, std::size_t nb_cp_v, std::size_t order_u, std::size_t order_v, const double *knots_u, const double *knots_v, const double *cps) const
{
    // /////////////////////////////////////////////////////////////////
    // Initializes the Open Nurbs NURBS surface
    // /////////////////////////////////////////////////////////////////
    d = ON_NurbsSurface::New(dim, true, order_u, order_v, nb_cp_u, nb_cp_v);

    // ///////////////////////////////////////////////////////////////////
    // In its call to New, openNURBS reserves enough space at d->m_knot[0], d->m_knot[1], d->m_cv to store the values of knots and control points
    // ///////////////////////////////////////////////////////////////////
    for(std::size_t i = 0; i < order_u + nb_cp_u - 2; ++i) {
        d->m_knot[0][i] = knots_u[i];
    }

    for(std::size_t i = 0; i < order_v + nb_cp_v - 2; ++i) {
        d->m_knot[1][i] = knots_v[i];
    }

    for(std::size_t i = 0; i < nb_cp_u; ++i) {
        for(std::size_t j = 0; j < nb_cp_v; ++j) {
            d->m_cv[4 * nb_cp_v * i + 4 * j] =     cps[4 * nb_cp_v * i + 4 * j]     * cps[4 * nb_cp_v * i + 4 * j + 3];
            d->m_cv[4 * nb_cp_v * i + 4 * j + 1] = cps[4 * nb_cp_v * i + 4 * j + 1] * cps[4 * nb_cp_v * i + 4 * j + 3];
            d->m_cv[4 * nb_cp_v * i + 4 * j + 2] = cps[4 * nb_cp_v * i + 4 * j + 2] * cps[4 * nb_cp_v * i + 4 * j + 3];
            d->m_cv[4 * nb_cp_v * i + 4 * j + 3] = cps[4 * nb_cp_v * i + 4 * j + 3];
        }
    }

    if(!d->IsValid()) {
        dtkFatal() << "The dtkNurbsSurfaceDataOn is not valid";
    }
}

/*! \fn void dtkNurbsSurfaceDataOn::create(std::string path) const
  Creates a NURBS surface by providing a path to a file storing the required information to create a NURBS surface.

  \a path : a valid path to the file containing the information regarding a given NURBS surface
*/
void dtkNurbsSurfaceDataOn::create(std::string path) const
{
    QFile file(QString::fromStdString(path));
    QIODevice *in = &file;

    QIODevice::OpenMode mode = QIODevice::ReadOnly;
    mode |= QIODevice::Text;

    // to avoid troubles with floats separators ('.' and not ',')
    QLocale::setDefault(QLocale::c());
#if defined (Q_OS_UNIX) && !defined(Q_OS_MAC)
    setlocale(LC_NUMERIC, "C");
#endif

    if (!in->open(mode)) {
        dtkFatal() << Q_FUNC_INFO << "The file could not be found, please verify the path";
        return;
    }

    QString line = in->readLine().trimmed();
    //Run tests on the file to check its validity TODO more
    if ( line != "#dtkNurbsSurfaceDataOn"){
        dtkFatal() << Q_FUNC_INFO << "file not in a dtkNurbsSurfaceDataOn format.";
        return;
    }
    std::size_t dim = 3;
    QRegExp re = QRegExp("\\s+");

    line = in->readLine().trimmed();
    QStringList vals = line.split(re);
    std::size_t order_u = vals[1].toInt() + 1;
    line = in->readLine().trimmed();
    vals = line.split(re);
    std::size_t order_v = vals[1].toInt() + 1;
    line = in->readLine().trimmed();
    vals = line.split(re);
    std::size_t nb_cp_u = vals[1].toInt();
    line = in->readLine().trimmed();
    vals = line.split(re);
    std::size_t nb_cp_v = vals[1].toInt();
    line = in->readLine().trimmed();
    vals = line.split(re);
    std::vector<double> knots_u(vals.size());
    std::size_t i = 0;
    for(auto string : vals) {
        knots_u[i] = string.toDouble();
        ++i;
    }
    line = in->readLine().trimmed();
    vals = line.split(re);
    std::vector<double> knots_v(vals.size());
    i = 0;
    for(auto string : vals) {
        knots_v[i] = string.toDouble();
        ++i;
    }

    d = ON_NurbsSurface::New(dim, true, order_u, order_v, nb_cp_u, nb_cp_v);

    // ///////////////////////////////////////////////////////////////////
    // In its call to New, openNURBS reserves enough space at d->m_knot[0], d->m_knot[1], d->m_cv to store the values of knots and control points
    // ///////////////////////////////////////////////////////////////////
    for(std::size_t i = 0; i < order_u + nb_cp_u - 2; ++i) {
        d->m_knot[0][i] = knots_u[i];
    }

    for(std::size_t i = 0; i < order_v + nb_cp_v - 2; ++i) {
        d->m_knot[1][i] = knots_v[i];
    }

    for(std::size_t i = 0; i < nb_cp_u; ++i) {
        for(std::size_t j = 0; j < nb_cp_v;) {
            line = in->readLine().trimmed();
            if (line.startsWith("#")) {
                continue;
            }
            std::istringstream lin(line.toStdString());
            double x, y, z, w;
            lin >> x >> y >> z >> w;       //now read the whitespace-separated floats
            d->m_cv[4 * nb_cp_v * i + 4 * j] =     x * w;
            d->m_cv[4 * nb_cp_v * i + 4 * j + 1] = y * w;
            d->m_cv[4 * nb_cp_v * i + 4 * j + 2] = z * w;
            d->m_cv[4 * nb_cp_v * i + 4 * j + 3] = w;
            ++j;
        }
    }

    if(!d->IsValid()) {
        dtkFatal() << "The dtkNurbsSurfaceDataOn is not valid";
    }
}

/*! \fn dtkNurbsSurfaceDataOn::uDegree(void) const
  Returns the degree of in the "u" direction.
*/
std::size_t dtkNurbsSurfaceDataOn::uDegree(void) const
{
    return d->Degree(0);
}

/*! \fn dtkNurbsSurfaceDataOn::vDegree(void) const
  Returns the degree of in the "v" direction.
*/
std::size_t dtkNurbsSurfaceDataOn::vDegree(void) const
{
    return d->Degree(1);
}

/*! \fn dtkNurbsSurfaceDataOn::uNbCps(void) const
  Returns the number of weighted control points in the "u" direction.
*/
std::size_t dtkNurbsSurfaceDataOn::uNbCps(void) const
{
    return d->m_cv_count[0];
}

/*! \fn dtkNurbsSurfaceDataOn::vNbCps(void) const
  Returns the number of weighted control points in the "v" direction.
*/
std::size_t dtkNurbsSurfaceDataOn::vNbCps(void) const
{
    return d->m_cv_count[1];
}

std::size_t dtkNurbsSurfaceDataOn::uNbKnots(void) const
{
    return (d->m_cv_count[0] + d->Degree(0) - 1);
}

std::size_t dtkNurbsSurfaceDataOn::vNbKnots(void) const
{
    return (d->m_cv_count[1] + d->Degree(1) - 1);
}

/*! \fn dtkNurbsSurfaceDataOn::dim(void) const
  Returns the dimension of the space in which the NURBS surface lies.
*/
std::size_t dtkNurbsSurfaceDataOn::dim(void) const
{
    return d->m_dim;
}

/*! \fn dtkNurbsSurfaceDataOn::controlPoint(std::size_t i, std::size_t j, double *r_cp) const
  Writes in \a r_cp the \a i th, \a j th weighted control point coordinates[xij, yij, zij].

  \a i : index of the weighted control point along the "u" direction

  \a j : index of the weighted control point along the "v" direction

  \a r_cp : array of size 3 to store the weighted control point coordinates[xij, yij, zij]
*/
void dtkNurbsSurfaceDataOn::controlPoint(std::size_t i, std::size_t j, double* r_cp) const
{
    ON_3dPoint point;
    d->GetCV(i, j, point);
    r_cp[0] = point[0];
    r_cp[1] = point[1];
    r_cp[2] = point[2];
}

/*! \fn dtkNurbsSurfaceDataOn::weight(std::size_t i, std::size_t j, double *r_w) const
  Writes in \a r_w the \a i th, \a j th weighted control point weight wij.

  \a i : index of the weighted control point along the "u" direction

  \a j : index of the weighted control point along the "v" direction

  \a r_w : array of size 1 to store the weighted control point weight wij
*/
void dtkNurbsSurfaceDataOn::weight(std::size_t i, std::size_t j, double* r_w) const
{
    *r_w = d->Weight(i, j);
}

/*! \fn dtkNurbsSurfaceDataOn::uKnots(double *r_u_knots) const
  Writes in \a r_u_knots the knots of the curve along the "u" direction.

  \a r_u_knots : array of size (nb_cp_u + order_u - 2) to store the knots
*/
void dtkNurbsSurfaceDataOn::uKnots(double* r_u_knots) const
{
    std::size_t nb_of_knots_u = d->m_cv_count[0] + d->Degree(0) - 1;
    for (std::size_t i = 0; i < nb_of_knots_u; ++i) {
        r_u_knots[i] = d->m_knot[0][i];
    }
}

/*! \fn dtkNurbsSurfaceDataOn::vKnots(double *r_v_knots) const
  Writes in \a r_v_knots the knots of the curve along the "v" direction.

  \a r_v_knots : array of size (nb_cp_v + order_v - 2) to store the knots
*/
void dtkNurbsSurfaceDataOn::vKnots(double* r_v_knots) const
{
    std::size_t nb_of_knots_v = d->m_cv_count[1] + d->Degree(1) - 1;
    for (std::size_t i = 0; i < nb_of_knots_v; ++i) {
        r_v_knots[i] = d->m_knot[1][i];
    }
}

/*! \fn dtkNurbsSurfaceDataOn::uKnots(void) const
  Returns a pointer to the array storing the knots of the surface along the "u" direction. The array is of size : (nb_cp_u + order_u - 2).
*/
const double *dtkNurbsSurfaceDataOn::uKnots(void) const
{
    return d->m_knot[0];
}

/*! \fn dtkNurbsSurfaceDataOn::vKnots(void) const
  Returns a pointer to the array storing the knots of the surface along the "v" direction. The array is of size : (nb_cp_v + order_v - 2).
*/
const double *dtkNurbsSurfaceDataOn::vKnots(void) const
{
    return d->m_knot[1];
}

double dtkNurbsSurfaceDataOn::uPeriod(void) const
{
    // to be implemented with openNurbs
    return 0.;
}

double dtkNurbsSurfaceDataOn::vPeriod(void) const
{
    // to be implemented with openNurbs
    return 0.;
}

/*! \fn dtkNurbsSurfaceDataOn::isPointCulled(const dtkContinuousGeometryPrimitives::Point_2& point) const
  Returns true if \a point is culled by the trim loops of the surface, else returns false.
*/
bool dtkNurbsSurfaceDataOn::isPointCulled(const dtkContinuousGeometryPrimitives::Point_2& point) const
{
    //This works because the outter trim loop is necessarily of type inner (CCW)
    if (m_trim_loops.size() == 0) {
      return false;
        //dtkFatal() << "Trying to test if a point is culled when no trim loops are recorded.";
    } else if (m_trim_loops.size() == 1) {
        if((*m_trim_loops.begin())->isPointCulled(point)) {
            return true;
        } else {
            return false;
        }
    } else {
        std::size_t culled = 0;
        // std::size_t not_culled = 0;
/*
        for (auto trim_loop = m_trim_loops.begin(); trim_loop != m_trim_loops.end(); ++trim_loop) {
            if((*trim_loop)->isPointCulled(point) && (*trim_loop)->type() == dtkContinuousGeometryEnums::outer) {
                ++culled;
            } else if (!(*trim_loop)->isPointCulled(point) && (*trim_loop)->type() == dtkContinuousGeometryEnums::inner) {
                ++not_culled;
            }
        }
        if (culled == 0 && not_culled == 0) {
            return true;
        }
        if (culled == not_culled) {
            return true;
        } else {
            return false;
        }
*/

        std::size_t not_culled_inner = 0;
        std::size_t not_culled_outer = 0;

        for (auto trim_loop = m_trim_loops.begin(); trim_loop != m_trim_loops.end(); ++trim_loop) {

          if ( (*trim_loop)->type() ==  dtkContinuousGeometryEnums::inner ) {
            if ( (*trim_loop)->isPointCulled(point) == true ) ++culled;
            else ++not_culled_inner;
          }
          else if ( (*trim_loop)->type() ==  dtkContinuousGeometryEnums::outer ) {
            if ( (*trim_loop)->isPointCulled(point) == true ) ++culled;
            else ++not_culled_outer;
          }

        }

        if (culled > 0) {
          return true;
        }
        else {
          return false;
        }

    }

    return false; //Should never happen
}

/*! \fn dtkNurbsSurfaceDataOn::evaluatePoint(double p_u, double p_v, double *r_point) const
  Writes in \a r_point the evaluation at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_point : array of size 3 to store the result of the evaluation
*/
void dtkNurbsSurfaceDataOn::evaluatePoint(double p_u, double p_v, double* r_point) const
{
    ON_3dPoint on_point;
    d->EvPoint(p_u, p_v, on_point);
    r_point[0] = on_point[0];
    r_point[1] = on_point[1];
    r_point[2] = on_point[2];
}

/*! \fn dtkNurbsSurfaceDataOn::evaluateNormal(double p_u, double p_v, double *r_normal) const
  Writes in \a r_normal the normal evaluation at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_normal : array of size 3 to store the result of the normal evaluation
*/
void dtkNurbsSurfaceDataOn::evaluateNormal(double p_u, double p_v, double* r_normal) const
{
    ON_3dVector on_normal;
    d->EvNormal(p_u, p_v, on_normal);
    r_normal[0] = on_normal[0];
    r_normal[1] = on_normal[1];
    r_normal[2] = on_normal[2];
}

/*! \fn dtkNurbsSurfaceDataOn::evaluate1stDer(double p_u, double p_v, double *r_point, double *r_u_deriv, double *r_v_deriv) const
  Writes in \a r_point the evaluation, in \a r_u_deriv the first derivative in "u" direction, in \a r_v_deriv the first derivative in the "v" direction at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_point : array of size 3 to store the result of the evaluation

  \a r_u_deriv : array of size 3 to store the result of the first derivative evaluation in the "u" direction

  \a r_v_deriv : array of size 3 to store the result of the first derivative evaluation in the "v" direction
*/
void dtkNurbsSurfaceDataOn::evaluate1stDer(double p_u, double p_v, double* r_point, double* r_u_deriv, double* r_v_deriv) const
{
    ON_3dVector on_du;
    ON_3dVector on_dv;
    ON_3dPoint on_p;
    d->Ev1Der(p_u, p_v, on_p, on_du,on_dv);
    for (int i = 0; i < d->m_dim; ++i) {
        r_point[i] = on_p[i];
        r_u_deriv[i] = on_du[i];
        r_v_deriv[i] = on_dv[i];
    }
}

/*! \fn dtkNurbsSurfaceDataOn::evaluate1stDer(double p_u, double p_v, double *r_u_deriv, double *r_v_deriv) const
  Writes in \a r_u_deriv the first derivative in "u" direction and in \a r_v_deriv the first derivative in the "v" direction at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_u_deriv : array of size 3 to store the result of the first derivative evaluation in the "u" direction

  \a r_v_deriv : array of size 3 to store the result of the first derivative evaluation in the "v" direction
*/
void dtkNurbsSurfaceDataOn::evaluate1stDer(double p_u, double p_v, double* r_u_deriv, double* r_v_deriv) const
{
    ON_3dVector on_du;
    ON_3dVector on_dv;
    ON_3dPoint on_p;
    d->Ev1Der(p_u, p_v, on_p, on_du,on_dv);
    for (int i = 0; i < d->m_dim; ++i) {
        r_u_deriv[i] = on_du[i];
        r_v_deriv[i] = on_dv[i];
    }
}

/*! \fn dtkNurbsSurfaceDataOn::decomposeToRationalBezierSurfaces(std::vector< std::pair< dtkRationalBezierSurface *, double * > >& r_rational_bezier_surfaces) const
  Decomposes the NURBS surface into dtkRationalBezierSurface \a r_rational_bezier_surfaces.

  \a r_rational_bezier_surfaces : vector in which the dtkRationalBezierSurface are stored, along with their limits in the NURBS surface parameter space

  Not sure what happens when the not extreme nodes are of multiplicity >1.
*/
void dtkNurbsSurfaceDataOn::decomposeToRationalBezierSurfaces(std::vector< std::pair< dtkRationalBezierSurface *, double * > >& r_rational_bezier_surfaces) const
{
    // ///////////////////////////////////////////////////////////////////
    // TODO Checking multiplicity would be better than creating some bezier surfaces for nothing
    // ///////////////////////////////////////////////////////////////////
/*
    for (std::size_t i = 0; i <= std::size_t(d->m_cv_count[0] - d->m_order[0]); ++i) {
        for (std::size_t j = 0; j <= std::size_t(d->m_cv_count[1] - d->m_order[1]); ++j) {
            dtkRationalBezierSurfaceDataOn* on_bezier = new dtkRationalBezierSurfaceDataOn();
            on_bezier->create();
            if (d->ConvertSpanToBezier(i, j, on_bezier->onRationalBezierSurface())) {
                double* knots = new double[4];
                knots[0] = d->m_knot[0][i + d->m_order[0] - 2]; //u0
                knots[1] = d->m_knot[1][j + d->m_order[1] - 2]; //v0
                knots[2] = d->m_knot[0][i + d->m_order[0] - 1]; //u1
                knots[3] = d->m_knot[1][j + d->m_order[1] - 1]; //v1
                r_rational_bezier_surfaces.push_back(std::make_pair(new dtkRationalBezierSurface(dynamic_cast< dtkAbstractRationalBezierSurfaceData* >(on_bezier)), knots));
            } else {
                delete on_bezier;
            }
        }
    }
*/

    int num_beziers = 0;

    for (std::size_t i = 0; i <= std::size_t(d->m_cv_count[0] - d->m_order[0]); ++i) {
      for (std::size_t j = 0; j <= std::size_t(d->m_cv_count[1] - d->m_order[1]); ++j) {

        double* knots = new double[4];
        knots[0] = d->m_knot[0][i + d->m_order[0] - 2]; // u0
        knots[1] = d->m_knot[1][j + d->m_order[1] - 2]; // v0
        knots[2] = d->m_knot[0][i + d->m_order[0] - 1]; // u1
        knots[3] = d->m_knot[1][j + d->m_order[1] - 1]; // v1

        if (knots[0] < knots[2] && knots[1] < knots[3]) {
          dtkRationalBezierSurfaceDataOn* on_bezier = new dtkRationalBezierSurfaceDataOn();
          on_bezier->create();

          if (d->ConvertSpanToBezier(i, j, on_bezier->onRationalBezierSurface())) {
            r_rational_bezier_surfaces.push_back(std::make_pair(new dtkRationalBezierSurface(dynamic_cast< dtkAbstractRationalBezierSurfaceData* >(on_bezier)), knots));
            num_beziers += 1;
          }
          else {
            std::cout << "ERROR: nurbs cannot be converted to bezier." << std::endl;
            delete on_bezier;
          }
        }
        else
        {
          delete[] knots;
        }
      }
    }

    if (num_beziers != d->SpanCount(0)*d->SpanCount(1)) std::cout << "ERROR: Conversion to Bezier surfaces not correct." << std::endl;

}

/*! \fn dtkNurbsSurfaceDataOn::aabb(double* r_aabb) const
  Writes in \a r_aabb the [xmin, ymin, zmin, xmax, ymax, zmax] coordinates.

  \a r_aabb : array of size 6 to store the limits coordinates
*/
void dtkNurbsSurfaceDataOn::aabb(double* r_aabb) const
{
    ON_BoundingBox bb;
    bb = d->BoundingBox();
    r_aabb[0] = bb[0][0];
    r_aabb[1] = bb[0][1];
    r_aabb[2] = bb[0][2];
    r_aabb[3] = bb[1][0];
    r_aabb[4] = bb[1][1];
    r_aabb[5] = bb[1][2];
}

/*! \fn dtkNurbsSurfaceDataOn::extendedAabb(double *r_aabb, double factor) const
  Writes in \a r_aabb the [xmin, ymin, zmin, xmax, ymax, zmax] coordinates of an extened bounding box of factor \a factor. The extension factor extends the diagonal.

  \a r_aabb : array of size 6 to store the extended limits coordinates

  \a factor : factor used to lengthen the diagonal of the axis aligned bounding box
*/
void dtkNurbsSurfaceDataOn::extendedAabb(double* r_aabb, double factor) const
{
    ON_BoundingBox bb;
    bb = d->BoundingBox();
    r_aabb[0] = (bb[0][0] * ( 1 + factor) + bb[1][0] * (1 - factor)) / 2;
    r_aabb[1] = (bb[0][1] * ( 1 + factor) + bb[1][1] * (1 - factor)) / 2;
    r_aabb[2] = (bb[0][2] * ( 1 + factor) + bb[1][2] * (1 - factor)) / 2;
    r_aabb[3] = (bb[1][0] * ( 1 + factor) + bb[0][0] * (1 - factor)) / 2;
    r_aabb[4] = (bb[1][1] * ( 1 + factor) + bb[0][1] * (1 - factor)) / 2;
    r_aabb[5] = (bb[1][2] * ( 1 + factor) + bb[0][2] * (1 - factor)) / 2;
}

/*! \fn dtkNurbsSurfaceDataOn::clone(void) const
  Clone
*/
dtkNurbsSurfaceDataOn* dtkNurbsSurfaceDataOn::clone(void) const
{
    return new dtkNurbsSurfaceDataOn(*this);
}

#include <iomanip>
void dtkNurbsSurfaceDataOn::print(std::ostream& stream) const
{
    stream << "#dtkNurbsSurfaceDataOn" << std::endl;
    stream << "u_degree " << d->Degree(0) << std::endl;
    stream << "v_degree " << d->Degree(1) << std::endl;
    stream << "u_nb_cps " << d->m_cv_count[0] << std::endl;
    stream << "v_nb_cps " << d->m_cv_count[1] << std::endl;
    std::size_t nb_of_knots_u = d->m_cv_count[0] + d->Degree(0) - 1;
    for (std::size_t i = 0; i < nb_of_knots_u; ++i) {
        stream << d->m_knot[0][i] << " ";
    }
    stream << std::endl;
    std::size_t nb_of_knots_v = d->m_cv_count[1] + d->Degree(1) - 1;
    for (std::size_t i = 0; i < nb_of_knots_v; ++i) {
        stream << d->m_knot[1][i] << " ";
    }
    stream << std::endl;
    ON_3dPoint point;
    for(auto i = 0; i < d->m_cv_count[0]; ++i) {
        for(auto j = 0; j < d->m_cv_count[1]; ++j) {
            d->GetCV(i, j, point);
            stream << point[0] << " " << point[1] << " " << point[2] << " " << d->Weight(i, j) << std::setprecision(std::numeric_limits< double >::digits10 + 1) << std::endl;
        }
    }
    stream << "#dtkNurbsSurfaceDataOn" << std::endl;
}

//
// dtkNurbsSurfaceDataOn.cpp ends here
