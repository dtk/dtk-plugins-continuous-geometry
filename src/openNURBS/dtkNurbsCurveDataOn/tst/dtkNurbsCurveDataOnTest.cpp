// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkNurbsCurveDataOnTest.h"

#include "dtkNurbsCurveDataOn.h"

#include <dtkRationalBezierCurveDataOn.h>
#include <dtkRationalBezierCurve>

#include <dtkContinuousGeometryUtils>

#include <dtkTest>
#include <fstream>

// ///////////////////////////////////////////////////////////////////
// D-Pointer implementation
// ///////////////////////////////////////////////////////////////////

class dtkNurbsCurveDataOnTestCasePrivate{
public:
    dtkNurbsCurveDataOn *nurbs_curve_data;

    dtkNurbsCurveDataOn *nurbs_curve_data_from_beziers;
    dtkRationalBezierCurve *bezier_curve_1;
    dtkRationalBezierCurve *bezier_curve_2;
};
// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkNurbsCurveDataOnTestCase::dtkNurbsCurveDataOnTestCase(void):d(new dtkNurbsCurveDataOnTestCasePrivate())
{
    dtkLogger::instance().attachConsole();
    dtkLogger::instance().setLevel(dtk::LogLevel::Trace);
}

dtkNurbsCurveDataOnTestCase::~dtkNurbsCurveDataOnTestCase(void)
{
}

void dtkNurbsCurveDataOnTestCase::initTestCase(void)
{
    d->nurbs_curve_data = new dtkNurbsCurveDataOn();
    //degree 1
    std::size_t dim = 3;
    std::size_t nb_cp = 4;
    std::size_t order = 3;

    std::vector< double > knots(nb_cp + order - 2);
    /* 0. */ knots[0] = 0.; knots[1] = 0.; knots[2] = 1.3; knots[3] = 2.; knots[4] = 2.; /* 2. */

    std::vector< double > cps((dim + 1) * nb_cp);
    cps[0] = 0.;   cps[1] = 0.; cps[2] = 0.;  cps[3] = 1.;
    cps[4] = 1.;   cps[5] = 0.; cps[6] = 1.;  cps[7] = 3.;
    cps[8] = 2.;   cps[9] = 0.; cps[10] = 2.; cps[11] = 2.;
    cps[12] = 3.;  cps[13] = 0.;  cps[14] = 3.; cps[15] = 1.;

    d->nurbs_curve_data->create(dim, nb_cp, order, knots.data(), cps.data());

    std::vector< dtkRationalBezierCurve* > bezier_curves;
    dtkRationalBezierCurveDataOn *bezier_curve_data_on_1 = new dtkRationalBezierCurveDataOn();
    std::size_t order_beziers = 4;
    std::vector< double > cps_1((3 + 1) * order_beziers);
    cps_1[0] = 2.;  cps_1[1] = 0.;  cps_1[2] = 0.; cps_1[3] = 1.;
    cps_1[4] = 5.;  cps_1[5] = 3.;  cps_1[6] = 0.; cps_1[7] = 2.;
    cps_1[8] = 7.;  cps_1[9] = 3.;  cps_1[10] = 0.; cps_1[11] = 3.;
    cps_1[12] = 9.; cps_1[13] = 0.; cps_1[14] = 0.; cps_1[15] = 0.5;
    d->bezier_curve_1 = new dtkRationalBezierCurve(bezier_curve_data_on_1);
    d->bezier_curve_1->create(order_beziers, cps_1.data());
    bezier_curves.push_back(d->bezier_curve_1);

    dtkRationalBezierCurveDataOn *bezier_curve_data_on_2 = new dtkRationalBezierCurveDataOn();
    std::vector< double > cps_2((3 + 1) * order_beziers);
    cps_2[0] = 9.;  cps_2[1] = 0.;  cps_2[2] = 0.; cps_2[3] = 1.;
    cps_2[4] = 11.;  cps_2[5] = 3.;  cps_2[6] = 0.; cps_2[7] = 2.;
    cps_2[8] = 12.;  cps_2[9] = 3.;  cps_2[10] = 0.; cps_2[11] = 3.;
    cps_2[12] = 16.; cps_2[13] = 0.; cps_2[14] = 0.; cps_2[15] = 0.5;
    d->bezier_curve_2 = new dtkRationalBezierCurve(bezier_curve_data_on_2);
    d->bezier_curve_2->create(order_beziers, cps_2.data());
    bezier_curves.push_back(d->bezier_curve_2);
    d->nurbs_curve_data_from_beziers = new dtkNurbsCurveDataOn();
    d->nurbs_curve_data_from_beziers->create(bezier_curves);
}

void dtkNurbsCurveDataOnTestCase::init(void)
{

}

void dtkNurbsCurveDataOnTestCase::testControlPoint(void)
{
    dtkContinuousGeometryPrimitives::Point_3 point(0., 0., 0.);
    d->nurbs_curve_data->controlPoint(1, point.data());
    QVERIFY(point[0] == 1.);
    QVERIFY(point[1] == 0.);
    QVERIFY(point[2] == 1.);
}

void dtkNurbsCurveDataOnTestCase::testWeight(void)
{
    double w = 0.;
    d->nurbs_curve_data->weight(1, &w);
    QVERIFY(w == 3.);
}

void dtkNurbsCurveDataOnTestCase::testDegree(void)
{
    QVERIFY(d->nurbs_curve_data->degree() == 2);
}

void dtkNurbsCurveDataOnTestCase::testKnots(void)
{
    std::vector< double > knots(d->nurbs_curve_data->nbCps() + d->nurbs_curve_data->degree() - 1);
    QVERIFY(d->nurbs_curve_data->nbCps() + d->nurbs_curve_data->degree() - 1 == 5);
    d->nurbs_curve_data->knots(knots.data());
    QVERIFY(knots[0] == 0.);
    QVERIFY(knots[1] == 0.);
    QVERIFY(knots[2] == 1.3);
    QVERIFY(knots[3] == 2.);
    QVERIFY(knots[4] == 2.);

    QVERIFY(d->nurbs_curve_data->knots()[0] == 0.);
    QVERIFY(d->nurbs_curve_data->knots()[1] == 0.);
    QVERIFY(d->nurbs_curve_data->knots()[2] == 1.3);
    QVERIFY(d->nurbs_curve_data->knots()[3] == 2.);
    QVERIFY(d->nurbs_curve_data->knots()[4] == 2.);
}

void dtkNurbsCurveDataOnTestCase::testCurvePoint(void)
{
    double point[3];
    d->nurbs_curve_data->evaluatePoint(1.25, &point[0]);
    // QVERIFY(point[0] > 0.405975 - 10e-5 && point[0] < 0.405975 + 10e-5);
    // QVERIFY(point[1] > 0.25 - 10e-5 && point[1] < 0.25 + 10e-5);
    // QVERIFY(point[2] > 0.765378 - 10e-5 && point[2] < 0.765378 + 10e-5);
}

//To ameliorate with dataCurvePoint and several pints to test
void dtkNurbsCurveDataOnTestCase::testCurveNormal(void)
{
    double normal[3];
    d->nurbs_curve_data->evaluateNormal(1.5, &normal[0]);
}

void  dtkNurbsCurveDataOnTestCase::testPrintOutCurve(void)
{
    double point_cp[3];
    std::ofstream nurbs_curve_data_on_cp_file("nurbs_curve_data_on_cp.xyz");
    for (std::size_t i = 0; i <= d->nurbs_curve_data->degree(); ++i) {
        d->nurbs_curve_data->controlPoint(i, &point_cp[0]);
        nurbs_curve_data_on_cp_file << point_cp[0] << " " << point_cp[1] << " " << point_cp[2] << std::endl;
    }
    nurbs_curve_data_on_cp_file.close();

    std::ofstream nurbs_curve_data_on_file("nurbs_curve_data_on.xyz");
    double point[3];
    for (double i = 0.; i <= 2.; i += 0.01) {
        d->nurbs_curve_data->evaluatePoint(i,&point[0]);
        nurbs_curve_data_on_file << point[0] << " " << point[1] << " " << point[2] << std::endl;
    }
    nurbs_curve_data_on_file.close();
}

void dtkNurbsCurveDataOnTestCase::cleanup(void)
{

}

void dtkNurbsCurveDataOnTestCase::cleanupTestCase(void)
{
    delete d->nurbs_curve_data;
    d->nurbs_curve_data = nullptr;
}

DTKTEST_MAIN_NOGUI(dtkNurbsCurveDataOnTest, dtkNurbsCurveDataOnTestCase)

//
// dtkNurbsCurveDataOnTest.cpp ends here
