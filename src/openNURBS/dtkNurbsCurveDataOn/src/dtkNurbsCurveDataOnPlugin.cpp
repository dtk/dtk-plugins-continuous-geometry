// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#include "dtkNurbsCurveDataOn.h"
#include "dtkNurbsCurveDataOnPlugin.h"

#include <dtkCore>
#include <dtkContinuousGeometry>

// ///////////////////////////////////////////////////////////////////
// dtkNurbsCurveDataOnPlugin
// ///////////////////////////////////////////////////////////////////

void dtkNurbsCurveDataOnPlugin::initialize(void)
{
    dtkContinuousGeometry::abstractNurbsCurveData::pluginFactory().record("dtkNurbsCurveDataOn", dtkNurbsCurveDataOnCreator);
}

void dtkNurbsCurveDataOnPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkNurbsCurveDataOn)

//
// dtkNurbsCurveDataOnPlugin.cpp ends here
