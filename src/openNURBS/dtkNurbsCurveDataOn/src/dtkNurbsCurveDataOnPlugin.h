// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <QtCore>

#include <dtkContinuousGeometry>

#include <dtkAbstractNurbsCurveData>

class dtkNurbsCurveDataOnPlugin : public dtkAbstractNurbsCurveDataPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkAbstractNurbsCurveDataPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkNurbsCurveDataOnPlugin" FILE "dtkNurbsCurveDataOnPlugin.json")

public:
     dtkNurbsCurveDataOnPlugin(void) {}
    ~dtkNurbsCurveDataOnPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

//
// dtkNurbsCurveDataOnPlugin.h ends here
