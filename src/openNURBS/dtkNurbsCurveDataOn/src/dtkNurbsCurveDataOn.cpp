// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkNurbsCurveDataOn.h"

#include <dtkAbstractRationalBezierCurveData>
#include <dtkRationalBezierCurve>

#include <dtkRationalBezierCurveDataOn.h>

#include <opennurbs_nurbscurve.h>

#include <cassert>

/*!
  \class dtkNurbsCurveDataOn
  \inmodule dtkPluginsContinuousGeometry
  \brief dtkNurbsCurveDataOn is an openNURBS implementation of the concept dtkAbstractNurbsCurveData.

  The following lines of code show how to instanciate and use the class in pratice:
  \code
  dtkAbstractNurbsCurveData *nurbs_curve_data = dtkContinuousGeometry::abstractNurbsCurveData::pluginFactory().create("dtkNurbsCurveDataOn");
  dtkNurbsCurve *nurbs_curve = new dtkNurbsCurve2D(nurbs_curve_data);
  nurbs_curve->create(...);
  \endcode
*/

/*! \fn dtkNurbsCurveDataOn::dtkNurbsCurveDataOn(void)
  Instanciates the structure.

  To add the content, see \l create(std::size_t dim, std::size_t nb_cp, std::size_t order, double *knots, double *cps) const.
*/
dtkNurbsCurveDataOn::dtkNurbsCurveDataOn(void) : d(nullptr)
{

}

/*! \fn dtkNurbsCurveDataOn::~dtkNurbsCurveDataOn(void)
  Destroys the instance.
*/
dtkNurbsCurveDataOn::~dtkNurbsCurveDataOn(void)
{
    d->Destroy();
    delete d;
    // ///////////////////////////////////////////////////////////////////
    // Destroys if necessary the bezier curves
    // ///////////////////////////////////////////////////////////////////
    for(auto it = m_rational_bezier_curves.begin(); it != m_rational_bezier_curves.end(); ++it) {
        delete it->first;
        delete it->second;
    }
}

/*! \fn dtkNurbsCurveDataOn::create(const ON_NurbsCurve& nurbs_curve)
  Not available in the dtkAbstractNurbsCurveData.

  Creates the NURBS curve by providing a NURBS curve from the openNURBS API.

  \a nurbs_curve : the NURBS curve to copy.
*/
void dtkNurbsCurveDataOn::create(const ON_NurbsCurve& nurbs_curve)
{
    d = ON_NurbsCurve::New(nurbs_curve);
    assert(d->IsValid());
}

/*! \fn dtkNurbsCurveDataOn::create(std::size_t dim, std::size_t nb_cp, std::size_t order, double *knots, double *cps)
  Creates a dtkNurbsCurveDataOn by providing the required content for a nD NURBS curve.

  \a dim : dimension of the space in which lies the curve

  \a nb_cp : number of control points

  \a order : order

  \a knots : array containing the knots

  \a cps : array containing the weighted control points, specified as : [x0, y0, z0, w0, ...]
*/
void dtkNurbsCurveDataOn::create(std::size_t dim, std::size_t nb_cp, std::size_t order, double *p_knots, double *p_cps)
{
    // ///////////////////////////////////////////////////////////////////
    // Because almost not a single function of the ON_NurbsCurve API handles dim > 3
    // ///////////////////////////////////////////////////////////////////
    Q_ASSERT_X(dim < 4, __func__, "OpenNURBS do not handle dimensions higher than 3");
    // /////////////////////////////////////////////////////////////////
    // Initializes the Open Nurbs NURBS curve
    // /////////////////////////////////////////////////////////////////
    d = ON_NurbsCurve::New(dim, true, order, nb_cp);

    // ///////////////////////////////////////////////////////////////////
    // In its call to New, openNURBS reserves enough space at d->m_knot, d->m_cv to store the values of knots and control points
    // ///////////////////////////////////////////////////////////////////
    for(std::size_t i = 0; i < order + nb_cp - 2; ++i) {
        d->m_knot[i] = p_knots[i];
    }

    for(std::size_t i = 0; i < nb_cp; ++i) {
        d->m_cv[4 * i] =     p_cps[4 * i] *     p_cps[4 * i + 3];
        d->m_cv[4 * i + 1] = p_cps[4 * i + 1] * p_cps[4 * i + 3];
        d->m_cv[4 * i + 2] = p_cps[4 * i + 2] * p_cps[4 * i + 3];
        d->m_cv[4 * i + 3] = p_cps[4 * i + 3];
    }

    if(!d->IsValid()) {
        dtkFatal() << "The dtkNurbsCurveDataOn is not valid";
    }
}

/*! \fn dtkNurbsCurveDataOn::create(const std::vector< dtkRationalBezierCurve* >& rational_bezier_curves)
  Creates the NURBS curve by concatenating connected rational Bezier curves.

  \a rational_bezier_curves : the rational Bezier curves to concatenante

  The \a rational_bezier_curves must be given in order.

  The curves are copied.
*/
void dtkNurbsCurveDataOn::create(const std::vector< dtkRationalBezierCurve* >& rational_bezier_curves)
{
    Q_ASSERT(rational_bezier_curves.size() > 0);
    std::vector<dtkRationalBezierCurve*> rational_bezier_curves_new;
    rational_bezier_curves_new.reserve(rational_bezier_curves.size());
    // ///////////////////////////////////////////////////////////////////
    // Makes sure that all the rational_bezier_curves are connected (C0)
    // ///////////////////////////////////////////////////////////////////
    dtkContinuousGeometryPrimitives::Point_3 p_0(0., 0., 0.);
    dtkContinuousGeometryPrimitives::Point_3 p_n(0., 0., 0.);
    std::size_t degree = (*rational_bezier_curves.begin())->degree();
    for (auto bezier_curve : rational_bezier_curves) {
        if (bezier_curve->degree() > degree) degree = bezier_curve->degree();
    }

    for(auto bezier_curve = rational_bezier_curves.begin(); bezier_curve != std::prev(rational_bezier_curves.end()); ++bezier_curve) {
        if ((*bezier_curve)->degree() != degree) {
            dtkWarn() << "The degrees of the curves vary: " << (*bezier_curve)->degree() << ", " << degree;
            dtkWarn() << "Degree elevation";
            std::vector<dtkContinuousGeometryPrimitives::Point_3> cps_new;
            cps_new.reserve((*bezier_curve)->degree() + 1);
            for (std::size_t i = 0; i < (*bezier_curve)->degree() + 1; ++i) {
                dtkContinuousGeometryPrimitives::Point_3 cp(0., 0., 0.);
                (*bezier_curve)->controlPoint(i, cp.data());
                cps_new.push_back(cp);
            }

            dtkContinuousGeometryPrimitives::Point_3 cp_0 = cps_new[0];
            dtkContinuousGeometryPrimitives::Point_3 cp_n = cps_new.back();

            std::vector<dtkContinuousGeometryPrimitives::Point_3> cps_tmp;
            for (std::size_t i = 1; i < degree - (*bezier_curve)->degree() + 1; ++i) {
                int degree_new = (*bezier_curve)->degree() + i;
                cps_tmp.reserve(degree_new + 1);
                // first control point
                cps_tmp.push_back(cp_0);
                // control points in between
                for (int j = 1; j < degree_new; ++j) {
                    dtkContinuousGeometryPrimitives::Point_3 cp_1 = cps_new[j - 1];
                    dtkContinuousGeometryPrimitives::Point_3 cp_2 = cps_new[j];

                    dtkContinuousGeometryPrimitives::Point_3 cp_j(0., 0., 0.);
                    cp_j = cp_1*(double(j)/double(degree_new)) + cp_2*(1. - double(j)/double(degree_new));
                    cps_tmp.push_back(cp_j);
                }
                // last control point
                cps_tmp.push_back(cp_n);

                cps_new.clear();
                cps_new = cps_tmp;
                cps_tmp.clear();
            }

            // \todo the weights of new control points
            double* cps = new double[4*(degree + 1)];
            for (std::size_t i = 0; i < degree + 1; ++i) {
                cps[4*i] = cps_new[i][0];
                cps[4*i + 1] = cps_new[i][1];
                cps[4*i + 2] = cps_new[i][2];
                cps[4*i + 3] = 1.; // the weight is 1 by default, which is actually not reasonable.
            }

            dtkAbstractRationalBezierCurveData *rational_bezier_curve_data = dtkContinuousGeometry::abstractRationalBezierCurveData::pluginFactory().create("dtkRationalBezierCurveDataOn");
            rational_bezier_curve_data->create(degree + 1, cps);
            dtkRationalBezierCurve* rational_bezier_curve_3d = new dtkRationalBezierCurve(rational_bezier_curve_data);

            rational_bezier_curves_new.push_back(rational_bezier_curve_3d);

            //dtkFatal() << "The dtkNurbsCurveDataOn is not valid";
        }
        else {
            rational_bezier_curves_new.push_back(*bezier_curve);
        }
        (*bezier_curve)->controlPoint((*bezier_curve)->degree(), p_n.data());
        (*std::next(bezier_curve))->controlPoint(0, p_0.data());
        if(dtkContinuousGeometryTools::squaredDistance(p_0, p_n) > 1.e-9) {
            dtkWarn() << "The end/begin control points of two following bezier curves do not match";
            dtkWarn() << qSetRealNumberPrecision(17) << p_0 << " and " << p_n;
            dtkWarn() << "The dtkNurbsCurveDataOn is not valid";
        }
    }
    // ///////////////////////////////////////////////////////////////////
    // Copies the rational bezier curves
    // ///////////////////////////////////////////////////////////////////
    m_rational_bezier_curves.reserve(rational_bezier_curves.size());
    std::size_t i = 0;
    for(auto bezier_curve = rational_bezier_curves.begin(); bezier_curve != rational_bezier_curves.end(); ++bezier_curve) {
        double* knots = new double[2];
        knots[0] = i;
        knots[1] = i + 1;
        m_rational_bezier_curves.push_back(std::make_pair(new dtkRationalBezierCurve(*(*bezier_curve)), knots));
        ++i;
    }

    // /////////////////////////////////////////////////////////////////
    // Initializes the Open Nurbs NURBS curve
    // /////////////////////////////////////////////////////////////////
    std::size_t nb_cp = m_rational_bezier_curves.size() * degree + 1;
    d = ON_NurbsCurve::New(3, true, degree + 1, nb_cp);

    // ///////////////////////////////////////////////////////////////////
    // In its call to New, openNURBS reserves enough space at d->m_knot, d->m_cv to store the values of knots and control points
    // Each degree control points, the curve is interpolated
    // ///////////////////////////////////////////////////////////////////
    for(std::size_t j = 0; j < degree; ++j) {
        d->m_knot[j] = 0;
    }
    for(std::size_t i = 0; i < m_rational_bezier_curves.size(); ++i) {
        for(std::size_t j = 0; j < degree; ++j) {
            d->m_knot[(i + 1) * degree + j] = i + 1;
        }
    }

    dtkContinuousGeometryPrimitives::Point_3 p(0., 0., 0.);
    double w = 0.;
    m_rational_bezier_curves[0].first->controlPoint(0, p.data());
    m_rational_bezier_curves[0].first->weight(0, &w);
    d->m_cv[0] = p[0] * w;
    d->m_cv[1] = p[1] * w;
    d->m_cv[2] = p[2] * w;
    d->m_cv[3] = w;

    for(std::size_t i = 0; i < m_rational_bezier_curves.size(); ++i) {
        for(std::size_t j = 1; j <= degree; ++j) {
            m_rational_bezier_curves[i].first->controlPoint(j, p.data());
            m_rational_bezier_curves[i].first->weight(j, &w);
            d->m_cv[4 * (i * degree + j)] = p[0] * w;
            d->m_cv[4 * (i * degree + j) + 1] = p[1] * w;
            d->m_cv[4 * (i * degree + j) + 2] = p[2] * w;
            d->m_cv[4 * (i * degree + j) + 3] = w;
        }
    }

    if(!d->IsValid()) {
        dtkFatal() << "The dtkNurbsCurveDataOn is not valid";
    }
}

/*! \fn dtkNurbsCurveDataOn::degree(void) const
  Returns the degree of the curve.
*/
std::size_t dtkNurbsCurveDataOn::degree(void) const
{
    return d->Degree();
}

/*! \fn dtkNurbsCurveDataOn::nbCps(void) const
  Returns the number of weighted control points controlling the curve.
*/
std::size_t dtkNurbsCurveDataOn::nbCps(void) const
{
    return d->m_cv_count;
}

/*! \fn dtkNurbsCurveDataOn::dim(void) const
  Returns the dimension of the space in which the curve lies.
*/
std::size_t dtkNurbsCurveDataOn::dim(void) const
{
    return d->m_dim;
}

/*! \fn dtkNurbsCurveDataOn::controlPoint(std::size_t i, double *r_cp) const
  Writes in \a r_cp the \a i th weighted control point coordinates[xi, yi, zi].

  \a i : index of the weighted control point

  \a r_cp : array of size 3 to store the weighted control point coordinates[xi, yi, zi]
*/
void dtkNurbsCurveDataOn::controlPoint(std::size_t i, double* r_cp) const
{
    ON_3dPoint point;
    d->GetCV(i, point);
    for (int i = 0; i < d->m_dim; ++i) {
        r_cp[i] = point[i];
    }
}

/*! \fn dtkNurbsCurveDataOn::weight(std::size_t i, double *r_w) const
  Writes in \a r_w the \a i th weighted control point weight wi.

  \a i : index of the weighted control point

  \a r_w : array of size 1 to store the weighted control point weight wi
*/
void dtkNurbsCurveDataOn::weight(std::size_t i, double* r_w) const
{
    *r_w = d->Weight(i);
}

/*! \fn dtkNurbsCurveDataOn::knots(double *r_knots) const
  Writes in \a r_knots the knots of the curve.

  \a r_knots : array of size (nb_cp + order - 2) to store the knots
*/
void dtkNurbsCurveDataOn::knots(double* r_knots) const
{
    std::size_t nb_of_knots = d->m_cv_count + d->Degree() - 1;
    for (std::size_t i = 0; i < nb_of_knots; ++i) {
        r_knots[i] = d->m_knot[i];
    }
}

/*! \fn dtkNurbsCurveDataOn::knots(void) const
  Returns a pointer to the array storing the knots of the curve. The array is of size : (nb_cp + order - 2).
*/
const double *dtkNurbsCurveDataOn::knots(void) const
{
    return d->m_knot;
}

/*! \fn dtkNurbsCurveDataOn::evaluatePoint(double p_u, double *r_point) const
  Writes in \a r_point the evaluation at parameter \a p_u.

  \a p_u : parameter

  \a r_point : array of size 3 to store the result of the evaluation
*/
void dtkNurbsCurveDataOn::evaluatePoint(double p_u, double* r_point) const
{
    ON_3dPoint on_point;
    d->EvPoint(p_u, on_point);
    for (int i = 0; i < d->m_dim; ++i) {
        r_point[i] = on_point[i];
    }
}

/*! \fn dtkNurbsCurveDataOn::evaluateNormal(double p_u, double *r_normal) const
  Writes in \a r_normal the normal evaluation at parameter \a p_u.

  \a p_u : parameter

  \a r_normal : array of size 3 to store the result of the normal evaluation
*/
void dtkNurbsCurveDataOn::evaluateNormal(double p_u, double* r_normal) const
{
    ON_3dPoint on_point;
    ON_3dVector on_normal;
    d->Ev1Der(p_u, on_point, on_normal);
    for (int i = 0; i < d->m_dim; ++i) {
        r_normal[i] = on_normal[i];
    }
}

/*! \fn dtkNurbsCurveDataOn::evaluateNormal(double p_u, double *r_point, double *r_normal) const
  Writes in \a r_normal the normal evaluation at parameter \a p_u.

  \a p_u : parameter

  \a r_point : array of size 3 to store the result of the evaluation

  \a r_normal : array of size 3 to store the result of the normal evaluation
*/
void dtkNurbsCurveDataOn::evaluateNormal(double p_u, double* r_point, double* r_normal) const
{
    ON_3dPoint on_point;
    ON_3dVector on_normal;
    d->Ev1Der(p_u, on_point, on_normal);
    for (int i = 0; i < d->m_dim; ++i) {
        r_normal[i] = on_normal[i];
        r_point[i] = on_point[i];
    }
}

void dtkNurbsCurveDataOn::evaluateCurvature(double p_u, double* r_curvature) const
{
    ON_3dVector on_curvature = d->CurvatureAt(p_u);
    for (int i = 0; i < d->m_dim; ++i) {
      r_curvature[i] = on_curvature[i];
    }
}

void dtkNurbsCurveDataOn::initialize_m_rational_bezier_curves() const {
    std::lock_guard guard{m_mutex};
    if(m_rational_bezier_curves_initialized) return;
    for (std::size_t i = 0; i <= std::size_t(d->m_cv_count - d->m_order); ++i) {
        dtkRationalBezierCurveDataOn* on_bezier = new dtkRationalBezierCurveDataOn();
        on_bezier->create();
        if (d->ConvertSpanToBezier(i, on_bezier->onRationalBezierCurve())) {
            double* knots = new double[2];
            knots[0] = d->m_knot[i + d->m_order - 2]; //u0
            knots[1] = d->m_knot[i + d->m_order - 1]; //u1
            m_rational_bezier_curves.emplace_back(new dtkRationalBezierCurve{on_bezier}, knots);
        } else {
            delete on_bezier;
        }
    }
    m_rational_bezier_curves_initialized = true;
}

/*! \fn dtkNurbsCurveDataOn::decomposeToRationalBezierCurves(std::vector< std::pair< dtkRationalBezierCurve*, double* >>& r_rational_bezier_curves) const
  Decomposes the NURBS curve into dtkRationalBezierCurve \a r_rational_bezier_curves.

  \a r_rational_bezier_curves : vector in which the dtkRationalBezierCurve are stored, along with their limits in the NURBS curve parameter space
*/
void dtkNurbsCurveDataOn::decomposeToRationalBezierCurves(std::vector< std::pair< dtkRationalBezierCurve*, double* >>& r_rational_bezier_curves) const
{
    if(!m_rational_bezier_curves_initialized) {
        initialize_m_rational_bezier_curves();
    }
    r_rational_bezier_curves = m_rational_bezier_curves;
}

/*!  \fn dtkNurbsCurveDataOn::decomposeToRationalBezierCurves(std::vector< dtkRationalBezierCurve * >& r_rational_bezier_curves) const
  Decomposes the NURBS curve into dtkRationalBezierCurve \a r_rational_bezier_curves.

  \a r_rational_bezier_curves : vector in which the dtkRationalBezierCurve are stored

  To recover the limits of the rational Bezier curves in the NURBS parameter space, check : \l decomposeToRationalBezierCurves(std::vector< std::pair< dtkRationalBezierCurve*, double* > >& r_rational_bezier_curves).
*/
void dtkNurbsCurveDataOn::decomposeToRationalBezierCurves(std::vector< dtkRationalBezierCurve * >& r_rational_bezier_curves) const
{
    if(!m_rational_bezier_curves_initialized) {
        initialize_m_rational_bezier_curves();
    }
    for(auto bezier_curve = m_rational_bezier_curves.begin(); bezier_curve != m_rational_bezier_curves.end(); ++bezier_curve) {
        r_rational_bezier_curves.push_back(bezier_curve->first);
    }
}

/*!  \fn dtkNurbsCurveDataOn::clone(void) const
  Clone.
*/
dtkNurbsCurveDataOn* dtkNurbsCurveDataOn::clone(void) const
{
    dtkFatal() << "not implemented";
    return nullptr;
}

//
// dtkNurbsCurveDataOn.cpp ends here

// Local Variables:
// c-basic-offset: 4
// End:
