// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkBRepViewer.h>

class openNURBSBRepViewerPrivate;

class dtkBRep;

class openNURBSBRepViewer : public dtkBRepViewer
{
public:
     openNURBSBRepViewer(void);
    ~openNURBSBRepViewer(void);

public:
    void  setInputBRep( dtkBRep*) final;

    void run(void) final;

protected:
    openNURBSBRepViewerPrivate *d;
};

// ///////////////////////////////////////////////////////////////////

inline dtkBRepViewer* openNURBSBRepViewerCreator(void)
{
    return new openNURBSBRepViewer();
}

//
// openNURBSBRepViewer.h ends here
