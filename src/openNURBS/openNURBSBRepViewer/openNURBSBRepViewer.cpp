// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "openNURBSBRepViewer.h"

#include "openNURBSBRepData.h"

#include <dtkContinuousGeometry.h>

#include <dtkBRep.h>
#include <dtkBRepData.h>
#include <dtkBRepViewer.h>

#include <opennurbs_without_warnings.h>
#include "opennurbs_gl.h"

#include <GL/glut.h>   // Open GL auxillary functions

// /////////////////////////////////////////////////////////////////
// openNURBSBRepViewerPrivate
// /////////////////////////////////////////////////////////////////

class openNURBSBRepViewerPrivate
{
public:
    dtkBRep* dtk_brep;
};

// ///////////////////////////////////////////////////////////////////
// openNURBSBRepViewer implementation
// ///////////////////////////////////////////////////////////////////

openNURBSBRepViewer::openNURBSBRepViewer(void) : dtkBRepViewer(), d(new openNURBSBRepViewerPrivate)
{
}

openNURBSBRepViewer::~openNURBSBRepViewer(void)
{
    delete d;
    d = nullptr;
}

void openNURBSBRepViewer::setInputBRep(dtkBRep* brep)
{
    d->dtk_brep = brep;
}

class CModel : public ONX_Model
{
public:
  void GetObjectMaterial( int object_index, ON_Material& material ) const;
  ON_3dmView m_view;
  ON_BoundingBox m_bbox;
};

void CModel::GetObjectMaterial( 
          int object_index,
          ON_Material& material 
          ) const
{
  material.Default();
  //const ON_Geometry* geo = 0;

  if ( object_index >= 0 && object_index <= m_object_table.Count() )
  {
    const ONX_Model_Object& mo = m_object_table[object_index];
    if ( 0 != mo.m_object )
    {
      switch( mo.m_object->ObjectType() )
      {
      case ON::surface_object:
      case ON::brep_object:
      case ON::mesh_object:
      case ON::instance_reference:
        GetRenderMaterial( mo.m_attributes, material );
        break;
      default:
        {
          // use emmissive object color for curve objects
          ON_Color c = WireframeColor( mo.m_attributes );
          ON_Color black(0,0,0);
          material.Default();
          material.SetAmbient(black);
          material.SetDiffuse(black);
          material.SetSpecular(black);
          material.SetEmission(c);
        }
        break;
      }
    }
  }
}


void GetDefaultView( const ON_BoundingBox& bbox, ON_3dmView& view )
{
  // simple parallel projection of bounding box;
  double window_height = 1.0;
  double window_width = 1.0;
  double dx, dy, dz;
  double frus_near, frus_far;
  ON_3dPoint camLoc;
  ON_3dVector camDir, camUp;
  view.m_target = 0.5*(bbox.m_min + bbox.m_max);
  dx = 1.1*(bbox.m_max[0] - bbox.m_min[0]);
  dy = 1.1*(bbox.m_max[1] - bbox.m_min[1]);
  dz = 1.1*(bbox.m_max[2] - bbox.m_min[2]);
  if ( dx <= 1.0e-6 && dy <= 1.0e-6 )
    dx = dy = 2.0;
  if ( window_height*dx < window_width*dy ) {
    dx = dy*window_width/window_height;
  }
  else {
    dy = dx*window_height/window_width;
  }
  if ( dz <= 0.1*(dx+dy) )
    dz = 0.1*(dx+dy);
  dx *= 0.5;
  dy *= 0.5;
  dz *= 0.5;


  frus_near = 1.0;
  frus_far = frus_near + 2.0*dz;
  camLoc = view.m_target + (dz + frus_near)*ON_zaxis;
  camDir = -ON_zaxis;
  camUp = ON_yaxis;

  view.m_vp.SetProjection( ON::parallel_view );
  view.m_vp.SetCameraLocation( camLoc );
  view.m_vp.SetCameraDirection( camDir );
  view.m_vp.SetCameraUp( camUp );
  view.m_vp.SetFrustum( -dx, dx, -dy, dy, frus_near, frus_far );
}

///////////////////////////////////////////////////////////////////////
//
// Globals for myDisplay() function passed to auxMainLoop()
//
//////////////////////////////////////////////////////////////////////

// GL display list "name"
static GLuint glb_display_list_number = 1;

// global pointer to active model
CModel* glb_model = 0;

///////////////////////////////////////////////////////////////////////
//
// Functions used in main()
//
//////////////////////////////////////////////////////////////////////

ON_BOOL32 myInitGL( const ON_Viewport&, GLUnurbsObj*& );

void myBuildDisplayList( 
      GLuint,                  // display_list_number,
      GLUnurbsObj*,            // pointer to GL nurbs render
      const CModel&            // geometry to render
      );

extern "C" {
void  myNurbsErrorCallback( GLenum ); // for gluNurbsCallback()

void  myDisplay( void );              // for auxMainLoop()


void  myKeyLeftArrowEvent( void );    // for auxKeyFunc();
void  myKeyRightArrowEvent( void );   // for auxKeyFunc();
void  myKeyUpArrowEvent( void );      // for auxKeyFunc();
void  myKeyDownArrowEvent( void );    // for auxKeyFunc();
void  myKeyViewExtents( void );       // for auxKeyFunc();

void  myGLUT_Reshape( int, int );  // for glutReshapeFunc()

void  myGLUT_MouseEvent( int button, int state, int x, int y );
void  myGLUT_KeyboardEvent( unsigned char ch, int x, int y );
void  myGLUT_SpecialKeyEvent( int ch, int x, int y );    // for auxKeyFunc();

// typedef void (CALLBACK* RHINO_GL_NURBS_ERROR)(...);


}

///////////////////////////////////////////////////////////////////////
//
// used to set projections
//
// void SetGLModelViewMatrix( const ON_Viewport& );
// void SetGLProjectionMatrix( ON_Viewport& );
///////////////////////////////////////////////////////////////////////

void openNURBSBRepViewer::run(void)
{
  // reads model into global glb_model;
  ON::Begin();

  ON_TextLog error_log;

  ON_BOOL32 bOK;
  int window_width  = 500;
  int window_height = 500;
  //double port_aspect = ((double)window_width)/((double)window_height);

  // read the file into model
//   if ( argc != 2 ) {
//     printf("Syntax: %s filename.3dm\n",argv[0] );
//     return 0;
//   }
//   const char* sFileName = argv[1];
//   printf("\nFile:  %s\n", sFileName );

  // read the file
  openNURBSBRepData* brep_data = dynamic_cast<openNURBSBRepData*>(d->dtk_brep->data());
  CModel* model = static_cast<CModel*>(brep_data->oNX_Model());
//   if ( !model.Read( sFileName, &error_log ) )
//   {
//     // read failed
//     error_log.Print("Unable to read file %s\n",sFileName);
//     return 1;
//   }

  glb_model = model;
	std::cerr << model->m_3dm_opennurbs_version
  // set bbox = world bounding box of all the objects
  model->m_bbox = model->BoundingBox();
    std::cerr << "before checking if the bounding box is valid" << std::endl;

  if ( !model->m_bbox.IsValid() )
  {
	 ON_SimpleArray<ON_3dPoint>box_corners;
	box_corners.Append(ON_3dPoint(-100,-100,-100));
	box_corners.Append(ON_3dPoint(100, -100, -100));
	box_corners.Append(ON_3dPoint(100, 100, -100));
	box_corners.Append(ON_3dPoint(-100, 100, -100));
	box_corners.Append(ON_3dPoint(-100,-100, 100));
	box_corners.Append(ON_3dPoint(100, -100, 100));
	box_corners.Append(ON_3dPoint(100, 100, 100));
	box_corners.Append(ON_3dPoint(-100, 100, 100));
		model->m_bbox.Set(box_corners);
     std::cerr << "the bounding box is not valid" << std::endl;
//     return;
  }

  // set model->m_view
  if ( model->m_settings.m_views.Count() > 0 )
  {
    // use first viewport projection in file
    double angle;
    model->m_view.m_vp = model->m_settings.m_views[0].m_vp;
    model->m_view.m_target = model->m_settings.m_views[0].m_target;
    model->m_view.m_vp.GetCameraAngle( &angle );
    model->m_view.m_vp.Extents( angle, model->m_bbox );
  }
  else 
  {
    GetDefaultView( model->m_bbox, model->m_view );
  }

  // If needed, enlarge frustum so its aspect matches the window's aspect.
  // Since the Rhino file does not store the far frustum distance in the
  // file, viewports read from a Rhil file need to have the frustum's far
  // value set by inspecting the bounding box of the geometry to be
  // displayed.

  
  ///////////////////////////////////////////////////////////////////
  //
  // GL stuff starts here
  //
  	std::cerr << "before gl stuff starts" << std::endl;
  for(;;) {  
    
    char sWindowTitleString[256];

	sWindowTitleString[255] = 0;
//     if ( argv[0] && argv[0][0] )
//     {
//       int i;
//       for ( i = 0; i < 254 && argv[0][i]; i++ )
//         sWindowTitleString[i] = argv[0][i];
//       sWindowTitleString[i] = 0;
//     }

    glutInit(0,0);
    glutInitWindowPosition( 0, 0);
    glutInitWindowSize( window_width, window_height );
    glutInitDisplayMode( GLUT_SINGLE | GLUT_RGB | GLUT_DEPTH );
    glutCreateWindow( sWindowTitleString );

    // register event handler functions
    glutIdleFunc( 0 );
    glutReshapeFunc( myGLUT_Reshape );
    glutMouseFunc( myGLUT_MouseEvent );
    glutKeyboardFunc( myGLUT_KeyboardEvent );
    glutSpecialFunc( myGLUT_SpecialKeyEvent );
    glutDisplayFunc( myDisplay );

    // setup model view matrix, GL defaults, and the GL NURBS renderer
    GLUnurbsObj* pTheGLNURBSRender = NULL; // OpenGL NURBS rendering context
    bOK = myInitGL( model->m_view.m_vp, pTheGLNURBSRender );

    if ( bOK ) {
	std::cerr << "builds display list" << std::endl;
      // build display list
      myBuildDisplayList( glb_display_list_number,
                          pTheGLNURBSRender,
                          *model );


      glutMainLoop(  );

    }

    gluDeleteNurbsRenderer( pTheGLNURBSRender );

    break;
  }

  //
  // GL stuff ends here
  //
  ///////////////////////////////////////////////////////////////////

  ON::End();

  return;
}

///////////////////////////////////////////////////////////////////////
void SetGLModelViewMatrix( const ON_Viewport& viewport )
{
  ON_GL( viewport ); // updates GL model view matrix
}

void SetGLProjectionMatrix( ON_Viewport& viewport )
{
  int pl, pr, pb, pt;
  viewport.GetScreenPort( &pl, &pr, &pb, &pt, NULL, NULL );
  ON_GL( viewport, pl, pr, pb, pt ); // updates GL projection matrix
}

ON_BOOL32 myInitGL( const ON_Viewport& viewport, GLUnurbsObj*& nobj )
{
  // set the model view transform
  SetGLModelViewMatrix( viewport );

  // this stuff works with MSVC 4.2's Open GL. Changes may be needed for other
  // GLs.
  //ON_Color background_color(0,128,128);
  ON_Color background_color(0,63,127);
  //background_color = glb_model->m_settings.m_RenderSettings.m_background_color;
  glClearColor( (float)background_color.FractionRed(), 
                (float)background_color.FractionGreen(), 
                (float)background_color.FractionBlue(), 
                1.0f
                );

  glLightModeli( GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE );
  glDisable( GL_CULL_FACE );
  
  // Rhino viewports have camera "Z" pointing at the camera in a right
  // handed coordinate system.
  glClearDepth( 0.0f );
  glEnable( GL_DEPTH_TEST );
  glDepthFunc( GL_GEQUAL );

  glEnable( GL_LIGHTING );
  glEnable( GL_DITHER );
  //glEnable( GL_AUTO_NORMAL );
  //glEnable( GL_NORMALIZE );

  // default material
  ON_GL( (ON_Material*)NULL );


  // GL rendering of NURBS objects requires a GLUnurbsObj.
  nobj = gluNewNurbsRenderer();
  if ( !nobj )
    return false;
  
  gluNurbsProperty( nobj, GLU_SAMPLING_TOLERANCE,   20.0f );
  gluNurbsProperty( nobj, GLU_PARAMETRIC_TOLERANCE, 0.5f );
  gluNurbsProperty( nobj, GLU_DISPLAY_MODE,         (GLfloat)GLU_FILL );
  //gluNurbsProperty( nobj, GLU_DISPLAY_MODE,         GLU_OUTLINE_POLYGON );
  //gluNurbsProperty( nobj, GLU_DISPLAY_MODE,         GLU_OUTLINE_PATCH );
  gluNurbsProperty( nobj, GLU_SAMPLING_METHOD,      (GLfloat)GLU_PATH_LENGTH );
  //gluNurbsProperty( nobj, GLU_SAMPLING_METHOD,      GLU_PARAMETRIC_ERROR );
  //gluNurbsProperty( nobj, GLU_SAMPLING_METHOD,      GLU_DOMAIN_DISTANCE );
  gluNurbsProperty( nobj, GLU_CULLING,              (GLfloat)GL_FALSE );

  // register GL NURBS error callback
  {
    // hack to get around C vs C++ type checking trauma
    //RHINO_GL_NURBS_ERROR fn;
   // fn = (RHINO_GL_NURBS_ERROR)myNurbsErrorCallback;
    gluNurbsCallback( nobj, GLU_ERROR, NULL);
  }

  return true;

}


///////////////////////////////////////////////////////////////////////
// void SetGLModelViewMatrix( const ON_Viewport& viewport )
// {
//   ON_GL( viewport ); // updates GL model view matrix
// }

// void SetGLProjectionMatrix( ON_Viewport& viewport )
// {
//   int pl, pr, pb, pt;
//   viewport.GetScreenPort( &pl, &pr, &pb, &pt, NULL, NULL );
//   ON_GL( viewport, pl, pr, pb, pt ); // updates GL projection matrix
// }
/*
ON_BOOL32 myInitGL( const ON_Viewport& viewport, GLUnurbsObj*& nobj )
{
  // set the model view transform
  SetGLModelViewMatrix( viewport );

  // this stuff works with MSVC 4.2's Open GL. Changes may be needed for other
  // GLs.
  //ON_Color background_color(0,128,128);
  ON_Color background_color(0,63,127);
  //background_color = glb_model->m_settings.m_RenderSettings.m_background_color;
  glClearColor( (float)background_color.FractionRed(), 
                (float)background_color.FractionGreen(), 
                (float)background_color.FractionBlue(), 
                1.0f
                );

  glLightModeli( GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE );
  glDisable( GL_CULL_FACE );
  
  // Rhino viewports have camera "Z" pointing at the camera in a right
  // handed coordinate system.
  glClearDepth( 0.0f );
  glEnable( GL_DEPTH_TEST );
  glDepthFunc( GL_GEQUAL );

  glEnable( GL_LIGHTING );
  glEnable( GL_DITHER );
  //glEnable( GL_AUTO_NORMAL );
  //glEnable( GL_NORMALIZE );

  // default material
  ON_GL( (ON_Material*)NULL );


  // GL rendering of NURBS objects requires a GLUnurbsObj.
  nobj = gluNewNurbsRenderer();
  if ( !nobj )
    return false;
  
  gluNurbsProperty( nobj, GLU_SAMPLING_TOLERANCE,   20.0f );
  gluNurbsProperty( nobj, GLU_PARAMETRIC_TOLERANCE, 0.5f );
  gluNurbsProperty( nobj, GLU_DISPLAY_MODE,         (GLfloat)GLU_FILL );
  //gluNurbsProperty( nobj, GLU_DISPLAY_MODE,         GLU_OUTLINE_POLYGON );
  //gluNurbsProperty( nobj, GLU_DISPLAY_MODE,         GLU_OUTLINE_PATCH );
  gluNurbsProperty( nobj, GLU_SAMPLING_METHOD,      (GLfloat)GLU_PATH_LENGTH );
  //gluNurbsProperty( nobj, GLU_SAMPLING_METHOD,      GLU_PARAMETRIC_ERROR );
  //gluNurbsProperty( nobj, GLU_SAMPLING_METHOD,      GLU_DOMAIN_DISTANCE );
  gluNurbsProperty( nobj, GLU_CULLING,              (GLfloat)GL_FALSE );

  // register GL NURBS error callback
  {
    // hack to get around C vs C++ type checking trauma
    RHINO_GL_NURBS_ERROR fn;
    fn = (RHINO_GL_NURBS_ERROR)myNurbsErrorCallback;
    gluNurbsCallback( nobj, GLU_ERROR, fn );
  }

  return true;
}*/

///////////////////////////////////////////////////////////////////////

void myGLUT_Reshape( int w, int h )
{
  static int w0 = 0;
  static int h0 = 0;
  if ( w != w0 || h != h0 ) {
    h0 = h;
    w0 = w;
    ON_GL( glb_model->m_view.m_vp, 0, w-1, h-1, 0 ); // set projection transform
  }
  glViewport( 0, 0, w, h );
}


///////////////////////////////////////////////////////////////////////
static void myRotateView( ON_Viewport& viewport,
                          const ON_3dVector& axis,
                          const ON_3dPoint& center,
                          double angle )
{
  ON_Xform rot;
  ON_3dPoint camLoc;
  ON_3dVector camY, camZ;

  rot.Rotation( angle, axis, center );

  if ( !viewport.GetCameraFrame( camLoc, NULL, camY, camZ ) )
    return;

  camLoc = rot*camLoc;
  camY   = rot*camY;
  camZ   = -(rot*camZ);

  viewport.SetCameraLocation( camLoc );
  viewport.SetCameraDirection( camZ );
  viewport.SetCameraUp( camY );

  ON_GL( viewport ); // update model view
}

static void myRotateLeftRight( ON_Viewport& viewport, double angle )
{
  // ON_3dVector axis = ON_zaxis; // rotate camera about world z axis (z up feel)
  ON_3dVector axis = ON_zaxis; // rotate camera about world y axis (u up feel)
  
  ON_3dPoint center;
  if ( glb_model )
    center = glb_model->m_view.m_target;
  else
    viewport.GetFrustumCenter( center );
  myRotateView( viewport, axis, center, angle );
}

static void myRotateUpDown( ON_Viewport& viewport, double angle )
{
  // rotates camera around the screen x axis
  ON_3dVector camX;
  ON_3dPoint center;
  if ( glb_model )
    center = glb_model->m_view.m_target;
  else
    viewport.GetFrustumCenter( center );
  viewport.GetCameraFrame( NULL, camX, NULL, NULL );
  myRotateView( viewport, camX, center, angle );
}

///////////////////////////////////////////////////////////////////////


void myKeyLeftArrowEvent( void )
{
  myRotateLeftRight( glb_model->m_view.m_vp, ON_PI/12.0 );
}

void myKeyRightArrowEvent( void )
{
  myRotateLeftRight( glb_model->m_view.m_vp, -ON_PI/12.0 );
}

void myKeyUpArrowEvent( void )
{
  myRotateUpDown( glb_model->m_view.m_vp, ON_PI/12.0 );
}

void myKeyDownArrowEvent( void )
{
  myRotateUpDown( glb_model->m_view.m_vp, -ON_PI/12.0 );
}

void myKeyViewExtents( void )
{
  double half_angle = 7.5*ON_PI/180.0;
  glb_model->m_view.m_vp.Extents( half_angle, glb_model->m_bbox );
  SetGLModelViewMatrix( glb_model->m_view.m_vp );
  SetGLProjectionMatrix( glb_model->m_view.m_vp );
}

void myGLUT_KeyboardEvent( unsigned char ch, int x, int y )
{
	int m = glutGetModifiers();
	if (m != GLUT_ACTIVE_ALT)
		return;
	if (ch == 'e' || ch == 'z') {
		myKeyViewExtents();
		glutPostRedisplay();
	}
}


void myGLUT_SpecialKeyEvent( int ch, int x, int y )
{
	if (ch == GLUT_KEY_LEFT)
		myKeyLeftArrowEvent();
	if (ch == GLUT_KEY_UP)
		myKeyUpArrowEvent();
	if (ch == GLUT_KEY_RIGHT)
		myKeyRightArrowEvent();
	if (ch == GLUT_KEY_DOWN)
		myKeyDownArrowEvent();
	glutPostRedisplay();
}

void myGLUT_MouseEvent( int button, int state, int x, int y )
{
	static int mx0, my0;
	static int mButton;

	if ( state == GLUT_DOWN ) {
		switch (button) {
		case GLUT_LEFT_BUTTON:
		case GLUT_MIDDLE_BUTTON:
		case GLUT_RIGHT_BUTTON:
			mButton = button;
			mx0 = x;
			my0 = y;
			break;
		}
	}

	if ( state == GLUT_UP && button == mButton ) {
		switch (mButton) {
		case GLUT_LEFT_BUTTON:
			// zoom
			glb_model->m_view.m_vp.ZoomToScreenRect( mx0, my0, x, y );
			break;
		case GLUT_MIDDLE_BUTTON:
			break;
		case GLUT_RIGHT_BUTTON:
			// dolly
			{
				ON_3dVector dolly_vector;
				double d;
				ON_3dPoint camLoc;
				ON_3dVector camZ;
				glb_model->m_view.m_vp.GetCameraFrame( camLoc, NULL, NULL, camZ );
				d = (camLoc-glb_model->m_view.m_target)*camZ;
				if ( glb_model->m_view.m_vp.GetDollyCameraVector(mx0,my0,x,y,d,dolly_vector) ) {
					glb_model->m_view.m_vp.DollyCamera( dolly_vector );
				}
			}
			break;
		}

		// update GL model view and projection matrices to match viewport changes
		SetGLModelViewMatrix( glb_model->m_view.m_vp );
		SetGLProjectionMatrix( glb_model->m_view.m_vp );
		glutPostRedisplay();
	}
}

///////////////////////////////////////////////////////////////////////

void myDisplayObject( const ON_Object& geometry, const ON_Material& material, GLUnurbsObj* nobj )
{
  // Called from myDisplay() to show geometry.
  // Uses ON_GL() functions found in rhinoio_gl.cpp.
  const ON_Point* point=0;
  const ON_PointCloud* cloud=0;
  const ON_Brep* brep=0;
  const ON_Mesh* mesh=0;
  const ON_Curve* curve=0;
  const ON_Surface* surface=0;

  // specify rendering material
  ON_GL( material );

  brep = ON_Brep::Cast(&geometry);
  if ( brep ) 
  {
    ON_GL(*brep, nobj);
    return;
  }

  mesh = ON_Mesh::Cast(&geometry);
  if ( mesh ) 
  {
    ON_GL(*mesh);
    return;
  }

  curve = ON_Curve::Cast(&geometry);
  if ( curve ) 
  {
    ON_GL( *curve, nobj );
    return;
  }

  surface = ON_Surface::Cast(&geometry);
  if ( surface ) 
  {
    gluBeginSurface( nobj );
    ON_GL( *surface, nobj );
    gluEndSurface( nobj );
    return;
  }

  point = ON_Point::Cast(&geometry);
  if ( point ) 
  {
    ON_GL(*point);
    return;
  }

  cloud = ON_PointCloud::Cast(&geometry);
  if ( cloud ) 
  {
    ON_GL(*cloud);
    return;
  }

}

///////////////////////////////////////////////////////////////////////

void myDisplayLighting( const ON_Viewport&, // viewport, // unreferenced
                                       const CModel& model
                                     )
{
  int light_count = model.m_light_table.Count();
  if ( light_count > 0 ) {
    int maxlighti = light_count;
    if ( maxlighti > GL_MAX_LIGHTS )
      maxlighti = GL_MAX_LIGHTS;
    int lighti;
    for ( lighti = 0; lighti < maxlighti; lighti++ ) {
      ON_GL( model.m_light_table[lighti].m_light, lighti+GL_LIGHT0 );
    }
  }
  else {
    // use default headlight
    // use basic bright white head light with a bit of ambient
    ON_Light head_light;
    head_light.Default();
    ON_GL( head_light, GL_LIGHT0 );
  }
}

///////////////////////////////////////////////////////////////////////

void myBuildDisplayList( GLuint display_list_number,
                         GLUnurbsObj* pTheGLNurbsRender,
                         const CModel& model
                         )
{
  ON_Material material;
  glNewList( display_list_number, GL_COMPILE );

  // display Rhino geometry using ON_GL() functions found in rhinoio_gl.cpp
  int i;
  const int object_count = model.m_object_table.Count();
  for ( i = 0; i < object_count; i++ ) 
  {
    const ONX_Model_Object& mo = model.m_object_table[i];
    if ( 0 != mo.m_object )
    {
      model.GetObjectMaterial( i, material );
      myDisplayObject( *mo.m_object, material, pTheGLNurbsRender );
    }
  }

  glEndList();
}

///////////////////////////////////////////////////////////////////////

void myDisplay( void )
{
  // Uses globals glb_* because the GL aux tools don't provide an
  // easy way to pass information into this callback.
  int bUseRhinoSpotlights = false; // I like to use a simple headlight
                                   // for a basic preview.

  glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

  // lights
  if ( bUseRhinoSpotlights && glb_model ) {
    // Rhino spotlights (currently rotate along with geometry)
    myDisplayLighting( glb_model->m_view.m_vp, *glb_model );
  }
  else {
    // simple bright white headlight
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();
    GLfloat pos[4]  = { (GLfloat)0.0, (GLfloat)0.0, (GLfloat)1.0, (GLfloat)0.0 };
    glLightfv( GL_LIGHT0, GL_POSITION,  pos );
    GLfloat black[4] = { (GLfloat)0.0, (GLfloat)0.0, (GLfloat)0.0, (GLfloat)1.0 };
    GLfloat white[4] = { (GLfloat)1.0, (GLfloat)1.0, (GLfloat)1.0, (GLfloat)1.0 };
    glLightfv( GL_LIGHT0, GL_AMBIENT,  black );
    glLightfv( GL_LIGHT0, GL_DIFFUSE,  white );
    glLightfv( GL_LIGHT0, GL_SPECULAR, white );
    glEnable( GL_LIGHT0 );
    glPopMatrix();
  }

  // display list built with myBuildDisplayList()
  glCallList( glb_display_list_number ); 

  glFlush();
}

///////////////////////////////////////////////////////////////////////

void myNurbsErrorCallback( GLenum errCode )
{
  const GLubyte* s = gluErrorString( errCode );
  printf("GL NURBS ERROR: (%d) %s\n",errCode, s );
}

///////////////////////////////////////////////////////////////////////
//
// openNURBSBRepViewer.cpp ends here
