// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkCore>

#include <dtkBRepViewer.h>

class openNURBSBRepViewerPlugin : public dtkBRepViewerPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkBRepViewerPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.openNURBSBRepViewerPlugin" FILE "openNURBSBRepViewerPlugin.json")

public:
     openNURBSBRepViewerPlugin(void) {}
    ~openNURBSBRepViewerPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

//
// openNURBSBRepViewerPlugin.h ends here
