// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "openNURBSBRepViewer.h"
#include "openNURBSBRepViewerPlugin.h"
#include "dtkContinuousGeometry.h"

#include <dtkCore>

// ///////////////////////////////////////////////////////////////////
// openNURBSBRepViewerPlugin
// ///////////////////////////////////////////////////////////////////

void openNURBSBRepViewerPlugin::initialize(void)
{
    dtkContinuousGeometry::bRepViewer::pluginFactory().record("openNURBSBRepViewer", openNURBSBRepViewerCreator);
}

void openNURBSBRepViewerPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(openNURBSBRepViewer)

//
// openNURBSBRepViewerPlugin.cpp ends here
