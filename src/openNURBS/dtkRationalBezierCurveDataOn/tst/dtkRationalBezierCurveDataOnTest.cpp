// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkRationalBezierCurveDataOnTest.h"

#include "dtkRationalBezierCurveDataOn.h"

#include <dtkRationalBezierCurve>
#include <dtkContinuousGeometryUtils>
#include <dtkContinuousGeometrySettings>

#include <dtkTest>
#include <fstream>

// ///////////////////////////////////////////////////////////////////
// D-Pointer implementation
// ///////////////////////////////////////////////////////////////////

class dtkRationalBezierCurveDataOnTestCasePrivate{
public:
    dtkRationalBezierCurveDataOn *bezier_curve_data;
    dtkRationalBezierCurveDataOn *bezier_curve_data_2;

    double m_tolerance = 1e-15;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkRationalBezierCurveDataOnTestCase::dtkRationalBezierCurveDataOnTestCase(void):d(new dtkRationalBezierCurveDataOnTestCasePrivate())
{
  dtkLogger::instance().attachConsole();
}

dtkRationalBezierCurveDataOnTestCase::~dtkRationalBezierCurveDataOnTestCase(void)
{
}

void dtkRationalBezierCurveDataOnTestCase::initTestCase(void)
{
    d->bezier_curve_data = new dtkRationalBezierCurveDataOn();
    //degree 3
    std::size_t order = 4;

    std::vector< double > cps((3 + 1) * order);
    cps[0] = 2.;  cps[1] = 0.;  cps[2] = 0.; cps[3] = 1.;
    cps[4] = 5.;  cps[5] = 3.;  cps[6] = 0.; cps[7] = 2.;
    cps[8] = 0.;  cps[9] = 3.;  cps[10] = 0.; cps[11] = 3.;
    cps[12] = 3.; cps[13] = 0.; cps[14] = 0.; cps[15] = 0.5;

    d->bezier_curve_data->create(order, cps.data());

    d->bezier_curve_data_2 = new dtkRationalBezierCurveDataOn();

    d->bezier_curve_data_2->create(QFINDTESTDATA("data/rational_bezier_curve_data").toStdString());
}

void dtkRationalBezierCurveDataOnTestCase::init(void)
{

}

void dtkRationalBezierCurveDataOnTestCase::testControlPoint(void)
{
    dtkContinuousGeometryPrimitives::Point_3 point(0., 0., 0.);
    d->bezier_curve_data->controlPoint(1, point.data());
    QVERIFY(point[0] == 5);
    QVERIFY(point[1] == 3);
    QVERIFY(point[2] == 0);

    d->bezier_curve_data_2->controlPoint(1, point.data());
    QVERIFY(point[0] == 3);
    QVERIFY(point[1] == 0);
    QVERIFY(point[2] == 0);
}

void dtkRationalBezierCurveDataOnTestCase::testWeight(void)
{
    double w = 0.;
    d->bezier_curve_data->weight(1, &w);
    QVERIFY(w == 2.);
    d->bezier_curve_data_2->weight(1, &w);
    QVERIFY(w == 1.);
}

void dtkRationalBezierCurveDataOnTestCase::testSetWeightedControlPoint(void)
{
    //Recover the weight at 1
    double w = 0.;
    d->bezier_curve_data->weight(1, &w);

    double *new_point = new double[4];
    new_point[0] = 3.;
    new_point[1] = 4.;
    new_point[2] = 5.;
    new_point[3] = w;
    d->bezier_curve_data->setWeightedControlPoint(1, &new_point[0]);
    delete[] new_point;
    dtkContinuousGeometryPrimitives::Point_3 test_point(0., 0., 0.);
    d->bezier_curve_data->controlPoint(1, test_point.data());
    QVERIFY(test_point[0] == 3.);
    QVERIFY(test_point[1] == 4.);
    QVERIFY(test_point[2] == 5.);

    //Sets back the point to its initial value
    double *init_point = new double[4];
    init_point[0] = 5.;
    init_point[1] = 3.;
    init_point[2] = 0.;
    init_point[3] = 1.;
    d->bezier_curve_data->setWeightedControlPoint(1, &init_point[0]);
    delete[] init_point;

    d->bezier_curve_data->controlPoint(1, test_point.data());
    QVERIFY(test_point[0] == 5.);
    QVERIFY(test_point[1] == 3.);
    QVERIFY(test_point[2] == 0.);

}

void dtkRationalBezierCurveDataOnTestCase::testDegree(void)
{
    QVERIFY(d->bezier_curve_data->degree() == 3);
}

void dtkRationalBezierCurveDataOnTestCase::testCurvePoint(void)
{
    dtkContinuousGeometryPrimitives::Point_3 point(0., 0., 0.);
    d->bezier_curve_data->evaluatePoint(0.5, point.data());
    // QVERIFY(point[0] > 2.5 - d->m_tolerance && point[0] < 2.5 + d->m_tolerance);
    // QVERIFY(point[1] > 2.25 - d->m_tolerance && point[1] < 2.25 + d->m_tolerance);
    // QVERIFY(point[2] > 0. - d->m_tolerance && point[2] < 0. + d->m_tolerance);
}

//To ameliorate with dataCurvePoint and several pints to test
void dtkRationalBezierCurveDataOnTestCase::testCurveNormal(void)
{
    dtkContinuousGeometryPrimitives::Vector_3 normal(0., 0., 0.);
    d->bezier_curve_data->evaluateNormal(0.5, normal.data());
}

void  dtkRationalBezierCurveDataOnTestCase::testAabb(void)
{
    double box[6];
    d->bezier_curve_data->aabb(&box[0]);

    QVERIFY(box[0] == 0.);
    QVERIFY(box[1] == 0.);
    QVERIFY(box[2] == 0.);
    QVERIFY(box[3] == 5.);
    QVERIFY(box[4] == 3);
    QVERIFY(box[5] == 0.);
}

void  dtkRationalBezierCurveDataOnTestCase::testExtendedAabb(void)
{
    double box[6];
    double factor = 1.1;
    d->bezier_curve_data->extendedAabb(&box[0], factor);
}

void dtkRationalBezierCurveDataOnTestCase::testSplit(void)
{
    dtkRationalBezierCurve *bezier_a = new dtkRationalBezierCurve();
    dtkRationalBezierCurve *bezier_b = new dtkRationalBezierCurve();

    d->bezier_curve_data->split(bezier_a, bezier_b, 0.5);

    QVERIFY(bezier_a->degree() == 3);
    QVERIFY(bezier_b->degree() == 3);
}

void dtkRationalBezierCurveDataOnTestCase::testPrintOutCurve(void)
{
    dtkContinuousGeometryPrimitives::Point_3 cpoint(0., 0., 0.);
    std::ofstream bezier_curve_data_on_cp_file("bezier_curve_data_on_cp.xyz");
    for (std::size_t i = 0; i <= d->bezier_curve_data->degree(); ++i) {
        d->bezier_curve_data->controlPoint(i, cpoint.data());
        bezier_curve_data_on_cp_file << cpoint[0] << " " << cpoint[1] << cpoint[2] << std::endl;
    }
    bezier_curve_data_on_cp_file.close();

    std::ofstream bezier_curve_data_on_file("bezier_curve_data_on.xyz");

    dtkContinuousGeometryPrimitives::Point_3 epoint(0., 0., 0.);
    for (double i = 0.; i <= 1.; i += 0.01) {
        d->bezier_curve_data->evaluatePoint(i, epoint.data());
        bezier_curve_data_on_file << epoint[0] << " " << epoint[1] << epoint[1] << std::endl;
    }
    bezier_curve_data_on_file.close();
}

void dtkRationalBezierCurveDataOnTestCase::cleanup(void)
{

}

void dtkRationalBezierCurveDataOnTestCase::cleanupTestCase(void)
{
    delete d->bezier_curve_data;
    d->bezier_curve_data = nullptr;
}

DTKTEST_MAIN_NOGUI(dtkRationalBezierCurveDataOnTest, dtkRationalBezierCurveDataOnTestCase)

//
// dtkRationalBezierCurveDataOnTest.cpp ends here
