// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkRationalBezierCurveDataOn.h"

#include <dtkRationalBezierCurve>

#include <opennurbs_without_warnings.h>
#include <opennurbs_bezier.h>

#include <cassert>

/*!
  \class dtkRationalBezierCurveDataOn
  \inmodule dtkPluginsContinuousGeometry
  \brief dtkRationalBezierCurveDataOn is an openNURBS implementation of the concept dtkAbstractRationalBezierCurveData.

  The following lines of code show how to instanciate and use the class in pratice:
  \code
  dtkAbstractRationalBezierCurveData *rational_bezier_curve_data = dtkContinuousGeometry::abstractRationalBezierCurveData::pluginFactory().create("dtkRationalBezierCurveDataOn");
  dtkRationalBezierCurve *rational_bezier_curve = new dtkRationalBezierCurve(rational_bezier_curve_data);
  rational_bezier_curve->create(...);
  \endcode
*/

/*! \fn dtkRationalBezierCurveDataOn::dtkRationalBezierCurveDataOn(void)
  Instanciates the structure.

  To add the content, see \l create(std::size_t order, double* cps) const.
*/
dtkRationalBezierCurveDataOn::dtkRationalBezierCurveDataOn(void) : d(nullptr) {}

/*! \fn dtkRationalBezierCurveDataOn::dtkRationalBezierCurveDataOn(const dtkRationalBezierCurveDataOn& curve_data)
  Copy constructor.

  Not available in the dtkAbstractRationalBezierCurveData.

  Creates the rational Bezier curve by copying the provided rational Bezier curve.

  \a curve_data : the rational Bezier curve to copy.
*/
dtkRationalBezierCurveDataOn::dtkRationalBezierCurveDataOn(const dtkRationalBezierCurveDataOn& curve_data)
{
    d = new ON_BezierCurve(*curve_data.d);
    assert(d->IsValid());
}

/*! \fn dtkRationalBezierCurveDataOn::~dtkRationalBezierCurveDataOn(void)
  Destroys the instance.
*/
dtkRationalBezierCurveDataOn::~dtkRationalBezierCurveDataOn(void)
{
    d->Destroy();
    delete d;
}

/*! \fn dtkRationalBezierCurveDataOn::create()
  Not available in the dtkAbstractRationalBezierCurveData.

  Creates an empty rational Bezier curve object.
*/
void dtkRationalBezierCurveDataOn::create()
{
    d = new ON_BezierCurve();
}

/*! \fn dtkRationalBezierCurveDataOn::create(const ON_BezierCurve& bezier_curve)
  Not available in the dtkAbstractRationalBezierCurveData.

  Creates the rational Bezier curve by providing a rational Bezier curve from the openNURBS API.

  \a bezier_curve : the rational Bezier curve to copy.
*/
void dtkRationalBezierCurveDataOn::create(const ON_BezierCurve& bezier_curve)
{
    d = new ON_BezierCurve(bezier_curve);
    assert(d->IsValid());
}

/*! \fn dtkRationalBezierCurveDataOn::create(std::size_t order, double *cps) const
  Creates the rational Bezier curve by providing the required content.

 \a order : order

 \a cps : array containing the weighted control points, specified as : [x0, y0, w0, ...]
*/
void dtkRationalBezierCurveDataOn::create(std::size_t order, double* cps) const
{
    // /////////////////////////////////////////////////////////////////
    // Initializes the Open RationalBezier NURBS surface
    // /////////////////////////////////////////////////////////////////
    d = new ON_BezierCurve(3, true, order);

    // ///////////////////////////////////////////////////////////////////
    // In its call to New, openNURBS reserves enough space at d->m_cv to store the values of control points
    // ///////////////////////////////////////////////////////////////////
    for(std::size_t i = 0; i < order; ++i) {
        d->m_cv[4 * i]     = cps[4 * i]     * cps[4 * i + 3];
        d->m_cv[4 * i + 1] = cps[4 * i + 1] * cps[4 * i + 3];
        d->m_cv[4 * i + 2] = cps[4 * i + 2] * cps[4 * i + 3];
        d->m_cv[4 * i + 3] = cps[4 * i + 3];
    }

    if(!d->IsValid()) {
        dtkFatal() << "The dtkRationalBezierCurveDataOn is not valid";
    }
}

/*! \fn void dtkRationalBezierCurveDataOn::create(std::string path) const
  Creates a rational Bezier curve by providing a path to a file storing the required information to create a rational Bezier curve.

  \a path : a valid path to the file containing the information regarding a given rational Bezier curve
*/
void dtkRationalBezierCurveDataOn::create(std::string path) const
{
    QFile file(QString::fromStdString(path));
    QIODevice *in = &file;

    QIODevice::OpenMode mode = QIODevice::ReadOnly;
    mode |= QIODevice::Text;

    // to avoid troubles with floats separators ('.' and not ',')
    QLocale::setDefault(QLocale::c());
#if defined (Q_OS_UNIX) && !defined(Q_OS_MAC)
    setlocale(LC_NUMERIC, "C");
#endif

    if (!in->open(mode)) {
        dtkFatal() << Q_FUNC_INFO << "The file could not be found, please verify the path";
        return;
    }

    QString line = in->readLine().trimmed();
    //Run tests on the file to check its validity TODO more
    if ( line != "#dtkRationalBezierCurveData"){
        dtkFatal() << Q_FUNC_INFO << "file not in a dtkRationalBezierCurveData format.";
        return;
    }
    // std::size_t dim = 3;
    QRegExp re = QRegExp("\\s+");
    line = in->readLine().trimmed();
    QStringList vals = line.split(re);
    std::size_t order = vals[1].toInt() + 1;

    d = new ON_BezierCurve(3, true, order);

    // ///////////////////////////////////////////////////////////////////
    // In its call to New, openNURBS reserves enough space at d->m_cv to store the values of control points
    // ///////////////////////////////////////////////////////////////////
    for(std::size_t i = 0; i < order; ++i) {
        line = in->readLine().trimmed();
        if (line.startsWith("#")) {
            continue;
        }
        std::istringstream lin(line.toStdString());
        double x, y, z, w;
        lin >> x >> y >> z >> w;       //now read the whitespace-separated floats
        d->m_cv[4 * i]     = x * w;
        d->m_cv[4 * i + 1] = y * w;
        d->m_cv[4 * i + 2] = z * w;
        d->m_cv[4 * i + 3] = w;
    }

    if(!d->IsValid()) {
        dtkFatal() << "The dtkRationalBezierCurveDataOn is not valid";
    }
}

/*! \fn  dtkRationalBezierCurveDataOn::degree(void) const
  Returns the degree of the curve.
*/
std::size_t dtkRationalBezierCurveDataOn::degree(void) const
{
    return d->Degree();
}

/*! \fn  dtkRationalBezierCurveDataOn::controlPoint(std::size_t i, double *r_cp) const
  Writes in \a r_cp the \a i th weighted control point coordinates[xi, yi].

  \a i : index of the weighted control point

  \a r_cp : array of size 3 to store the weighted control point coordinates[xi, yi, zi]

  The method can be called as follow:
  \code
  dtkContinuousGeometryPrimitives::Point_3 point3(0, 0, 0);
  rational_bezier_curve_data->controlPoint(3, point3.data());
  \endcode
*/
void dtkRationalBezierCurveDataOn::controlPoint(std::size_t i, double* r_cp) const
{
    ON_3dPoint point;
    d->GetCV(i, point);
    r_cp[0] = point[0];
    r_cp[1] = point[1];
    r_cp[2] = point[2];
}

/*! \fn  dtkRationalBezierCurveDataOn::weight(std::size_t i, double *r_w) const
  Writes in \a r_w the \a i th weighted control point weight wi.

  \a i : index of the weighted control point

  \a r_w : array of size 1 to store the weighted control point weight wi
*/
void dtkRationalBezierCurveDataOn::weight(std::size_t i, double* r_w) const
{
    *r_w = d->Weight(i);
}

/*! \fn  dtkRationalBezierCurveDataOn::setWeightedControlPoint(std::size_t i, double *p_cp)
  Modifies the \a i th weighted control point by \a p_cp : [xi, yi, zi, wi].

  \a i : index of the weighted control point

  \a p_cp : array of size 4 containing the weighted control point coordinates and weight  : [xi, yi, zi, wi]
*/
void dtkRationalBezierCurveDataOn::setWeightedControlPoint(std::size_t i, double *p_cp)
{
    d->m_cv[4 * i]     = p_cp[0] * p_cp[3];
    d->m_cv[4 * i + 1] = p_cp[1] * p_cp[3];
    d->m_cv[4 * i + 2] = p_cp[2] * p_cp[3];
    d->m_cv[4 * i + 3] = p_cp[3];
}

/*! \fn dtkRationalBezierCurveDataOn::evaluatePoint(double p_u, double *r_point) const
  Writes in \a r_point the evaluation at parameter \a p_u.

  \a p_u : parameter

  \a r_point : array of size 3 to store the result of the evaluation
*/
void dtkRationalBezierCurveDataOn::evaluatePoint(double p_u, double* r_point) const
{
    ON_3dPoint on_point = d->PointAt(p_u);
    r_point[0] = on_point[0];
    r_point[1] = on_point[1];
    r_point[2] = on_point[2];
}

/*! \fn dtkRationalBezierCurveDataOn::evaluateNormal(double p_u, double *r_normal) const
  Writes in \a r_normal the normal evaluation at parameter \a p_u.

  \a p_u : parameter

  \a r_normal : array of size 3 to store the result of the normal evaluation
*/
void dtkRationalBezierCurveDataOn::evaluateNormal(double p_u, double* r_normal) const
{
    ON_3dPoint on_point;
    ON_3dVector on_normal;
    d->Ev1Der(p_u, on_point, on_normal);
    r_normal[0] = on_normal[0];
    r_normal[1] = on_normal[1];
    r_normal[2] = on_normal[2];
}

void dtkRationalBezierCurveDataOn::evaluateDerivative(double p_u, double* r_derivative) const
{
    ON_3dVector on_derivative = d->DerivativeAt(p_u);
    r_derivative[0] = on_derivative[0];
    r_derivative[1] = on_derivative[1];
    r_derivative[2] = on_derivative[2];
}

void dtkRationalBezierCurveDataOn::evaluateCurvature(double p_u, double* r_curvature) const
{
    ON_3dVector on_curvature = d->CurvatureAt(p_u);
    r_curvature[0] = on_curvature[0];
    r_curvature[1] = on_curvature[1];
    r_curvature[2] = on_curvature[2];
}

/*! \fn dtkRationalBezierCurveDataOn::split(dtkRationalBezierCurve *r_split_curve_a, dtkRationalBezierCurve *r_split_curve_b, double splitting_parameter) const
  Splits at \a splitting_parameter the rational Bezier curve and stores in \a r_split_curve_a and \a r_split_curve_b the split parts (from 0 to \a splitting_parameter and from \a splitting_parameter to 1 respectively).

  \a r_split_curve_a : a pointer to a valid dtkRationalBezierCurve with data not initialized, in which the first part of the curve (from 0 to to \a splitting_parameter) will be stored

  \a r_split_curve_b : a pointer to a valid dtkRationalBezierCurve with data not initialized, in which the second part of the curve (from \a splitting_parameter to 1) will be stored

  \a splitting_parameter : the parameter at which the curve will be split
*/
void dtkRationalBezierCurveDataOn::split(dtkRationalBezierCurve *r_split_curve_a, dtkRationalBezierCurve *r_split_curve_b, double splitting_parameter) const
{
    if (r_split_curve_a == nullptr) {
        dtkFatal() << "The dtkRationalBezierCurve * r_split_curve_a points to nullptr";
    }
    if (r_split_curve_b == nullptr) {
        dtkFatal() << "The dtkRationalBezierCurve * r_split_curve_b points to nullptr";
    }
    ON_BezierCurve on_curve_A;
    ON_BezierCurve on_curve_B;
    d->Split(splitting_parameter, on_curve_A, on_curve_B);
    dtkRationalBezierCurveDataOn *dtk_curve_data_A = new dtkRationalBezierCurveDataOn();
    dtk_curve_data_A->create(on_curve_A);
    dtkRationalBezierCurveDataOn *dtk_curve_data_B = new dtkRationalBezierCurveDataOn();
    dtk_curve_data_B->create(on_curve_B);
    r_split_curve_a->setData(static_cast< dtkAbstractRationalBezierCurveData * >(dtk_curve_data_A));
    r_split_curve_b->setData(static_cast< dtkAbstractRationalBezierCurveData * >(dtk_curve_data_B));
}

/*! \fn dtkRationalBezierCurveDataOn::split(std::list< dtkRationalBezierCurve * >& r_split_curves, const std::vector< double >& p_splitting_parameters) const
  Splits the rational Bezier curve at each value of \a p_splitting_parameters and stores in \a r_split_curves the split parts.

  \a r_split_curves : a list to which the split rational Bezier curves will be added

  \a p_splitting_parameters : the parameters at which the curve will be split
*/
void dtkRationalBezierCurveDataOn::split(std::list< dtkRationalBezierCurve * >& r_split_curves, const std::vector< double >& p_splitting_parameters) const
{
    // ///////////////////////////////////////////////////////////////////
    // Makes sure that the list r_split_curves is empty or warn that some pointed data may be lost
    // ///////////////////////////////////////////////////////////////////
    for (auto it = r_split_curves.begin(); it != r_split_curves.end(); ++it) {
        if ((*it) != nullptr) {
            dtkWarn() << "r_split_curves contains some non nullptr, the pointed data might be lost";
        }
    }
    r_split_curves.clear();
    // ///////////////////////////////////////////////////////////////////
    // Make sure that all splitting parameters are between 0. and 1.
    // ///////////////////////////////////////////////////////////////////
    //TODO
    std::vector< double > splitting_parameters = p_splitting_parameters;
    std::sort(splitting_parameters.begin(), splitting_parameters.end());
    // ///////////////////////////////////////////////////////////////////
    // Copies the current dtkRationalBezierCurve
    // ///////////////////////////////////////////////////////////////////
    ON_BezierCurve on_curve = *d;
    dtkRationalBezierCurveDataOn *dtk_curve_data = new dtkRationalBezierCurveDataOn();
    dtk_curve_data->create(on_curve);
    dtkRationalBezierCurve *curve = new dtkRationalBezierCurve(static_cast< dtkAbstractRationalBezierCurveData * >(dtk_curve_data));
    r_split_curves.push_back(curve);
    auto split_curve = r_split_curves.begin();
    double param = 0.;
    auto p = splitting_parameters.begin();
    while (!splitting_parameters.empty()) {
        param = *p;
        if (param <= 0 || param >=1) {
            splitting_parameters.erase(p);
        } else {
            dtkRationalBezierCurve *split_curve_a = new dtkRationalBezierCurve();
            dtkRationalBezierCurve *split_curve_b = new dtkRationalBezierCurve();
            (*split_curve)->split(split_curve_a, split_curve_b, *p);
            delete (*split_curve);
            split_curve = r_split_curves.erase(split_curve);
            r_split_curves.insert(split_curve, split_curve_b);
            --split_curve;
            r_split_curves.insert(split_curve, split_curve_a);
            splitting_parameters.erase(p);
            // ///////////////////////////////////////////////////////////////////
            // Recompute the remaining parameters for splitting the second of the two curves
            // ///////////////////////////////////////////////////////////////////
            for (auto rp = splitting_parameters.begin(); rp != splitting_parameters.end(); ++rp) {
                (*rp) = ((*rp) - param) / (1. - param);
            }
        }
    }
}

/*! \fn dtkRationalBezierCurveDataOn::aabb(double* r_aabb) const
  Writes in \a r_aabb the [xmin, ymin, zmin, xmax, ymax, zmax] coordinates.

  \a r_aabb : array of size 6 to store the limits coordinates
*/
void dtkRationalBezierCurveDataOn::aabb(double* r_aabb) const
{
    ON_BoundingBox bb;
    d->GetBoundingBox(bb);
    r_aabb[0] = bb[0][0];
    r_aabb[1] = bb[0][1];
    r_aabb[2] = bb[0][2];
    r_aabb[3] = bb[1][0];
    r_aabb[4] = bb[1][1];
    r_aabb[5] = bb[1][2];
}

/*! \fn dtkRationalBezierCurveDataOn::extendedAabb(double *r_aabb, double factor) const
  Writes in \a r_aabb the [xmin, ymin, xmax, ymax] coordinates of an extened bounding box of factor \a factor. The extension factor extends the diagonal.

  \a r_aabb : array of size 6 to store the extended limits coordinates

  \a factor : factor used to lengthen the diagonal of the axis aligned bounding box
*/
void dtkRationalBezierCurveDataOn::extendedAabb(double* r_aabb, double factor) const
{
    ON_BoundingBox bb;
    bb = d->BoundingBox();
    r_aabb[0] = (bb[0][0] * ( 1 + factor) + bb[1][0] * (1 - factor)) / 2;
    r_aabb[1] = (bb[0][1] * ( 1 + factor) + bb[1][1] * (1 - factor)) / 2;
    r_aabb[2] = (bb[0][2] * ( 1 + factor) + bb[1][2] * (1 - factor)) / 2;
    r_aabb[3] = (bb[1][0] * ( 1 + factor) + bb[0][0] * (1 - factor)) / 2;
    r_aabb[4] = (bb[1][1] * ( 1 + factor) + bb[0][1] * (1 - factor)) / 2;
    r_aabb[5] = (bb[1][2] * ( 1 + factor) + bb[0][2] * (1 - factor)) / 2;
}

/*! \fn dtkRationalBezierCurveDataOn::onRationalBezierCurve(void)
  Not available in the dtkAbstractRationalBezierCurveData.

  Returns the underlying openNURBS rational Bezier curve.
 */
ON_BezierCurve& dtkRationalBezierCurveDataOn::onRationalBezierCurve(void)
{
    return *d;
}

/*! \fn dtkRationalBezierCurveDataOn::clone(void) const
   Clone
*/
dtkRationalBezierCurveDataOn* dtkRationalBezierCurveDataOn::clone(void) const
{
    return new dtkRationalBezierCurveDataOn(*this);
}

#include <iomanip>
void dtkRationalBezierCurveDataOn::print(std::ostream& stream) const
{
    stream << "#dtkRationalBezierCurveData" << std::endl;
    stream << "degree " << d->Degree() << std::endl;
    ON_3dPoint point;
    for(auto i = 0; i < d->Degree() + 1; ++i) {
            d->GetCV(i, point);
            stream << point[0] << " " << point[1] << " " << point[2] << " " << d->Weight(i) << std::setprecision(std::numeric_limits< double >::digits10 + 1) << std::endl;
        }
    stream << "#dtkRationalBezierCurveData" << std::endl;
}

//
// dtkRationalBezierCurveDataOn.cpp ends here
