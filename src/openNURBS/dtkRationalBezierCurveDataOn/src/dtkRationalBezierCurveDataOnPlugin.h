// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <QtCore>

#include <dtkContinuousGeometry>

#include <dtkAbstractRationalBezierCurveData>

class dtkRationalBezierCurveDataOnPlugin : public dtkAbstractRationalBezierCurveDataPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkAbstractRationalBezierCurveDataPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkRationalBezierCurveDataOnPlugin" FILE "dtkRationalBezierCurveDataOnPlugin.json")

public:
     dtkRationalBezierCurveDataOnPlugin(void) {}
    ~dtkRationalBezierCurveDataOnPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

//
// dtkRationalBezierCurveDataOnPlugin.h ends here
