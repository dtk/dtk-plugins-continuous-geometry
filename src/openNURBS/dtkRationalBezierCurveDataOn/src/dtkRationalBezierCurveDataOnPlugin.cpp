// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#include "dtkRationalBezierCurveDataOn.h"
#include "dtkRationalBezierCurveDataOnPlugin.h"

#include <dtkCore>
#include <dtkContinuousGeometry>

// ///////////////////////////////////////////////////////////////////
// dtkRationalBezierCurveDataOnPlugin
// ///////////////////////////////////////////////////////////////////

void dtkRationalBezierCurveDataOnPlugin::initialize(void)
{
    dtkContinuousGeometry::abstractRationalBezierCurveData::pluginFactory().record("dtkRationalBezierCurveDataOn", dtkRationalBezierCurveDataOnCreator);
}

void dtkRationalBezierCurveDataOnPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkRationalBezierCurveDataOn)

//
// dtkRationalBezierCurveDataOnPlugin.cpp ends here
