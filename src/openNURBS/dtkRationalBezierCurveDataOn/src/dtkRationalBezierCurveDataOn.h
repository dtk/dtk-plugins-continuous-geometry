// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <dtkContinuousGeometry.h>

#include <dtkContinuousGeometryUtils>

#include <dtkRationalBezierCurveDataOnExport.h>

#include <dtkAbstractRationalBezierCurveData>

class ON_BezierCurve;

class DTKRATIONALBEZIERCURVEDATAON_EXPORT dtkRationalBezierCurveDataOn final : public dtkAbstractRationalBezierCurveData
{
private:
    typedef dtkContinuousGeometryPrimitives::Point_3 Point_3;
    typedef dtkContinuousGeometryPrimitives::Vector_3 Vector_3;

public:
    dtkRationalBezierCurveDataOn(void);
    dtkRationalBezierCurveDataOn(const dtkRationalBezierCurveDataOn& curve_data);
    ~dtkRationalBezierCurveDataOn(void) final ;

public:
    void create(std::size_t order, double *cps) const override;
    void create(std::string path) const override;
    void create(const ON_BezierCurve& bezier_curve);
    void create();

public:
    std::size_t degree(void) const override;
    void controlPoint(std::size_t i, double *r_cp) const override;
    void weight(std::size_t i, double *r_w) const override;

public:
    void setWeightedControlPoint(std::size_t i, double *p_cp) override;

public:
    void evaluatePoint(double p_u, double *r_point) const override;
    void evaluateNormal(double p_u, double *r_normal) const override;
    void evaluateDerivative(double p_u, double* r_derivative) const override;
    void evaluateCurvature(double p_u, double* r_curvature) const override;

public:
    void split(dtkRationalBezierCurve *r_split_curve_a, dtkRationalBezierCurve *r_split_curve_b, double splitting_parameter) const override;
    void split(std::list< dtkRationalBezierCurve * >& r_split_curves, const std::vector< double >& p_splitting_parameters) const override;

public:
    void aabb(double* r_aabb) const override;
    void extendedAabb(double* r_aabb, double factor) const override;

public:
    ON_BezierCurve& onRationalBezierCurve(void);

public:
    dtkRationalBezierCurveDataOn *clone(void) const override;

public:
    void print(std::ostream& stream) const override;

private:
    mutable ON_BezierCurve *d;
};


// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

inline dtkAbstractRationalBezierCurveData *dtkRationalBezierCurveDataOnCreator(void)
{
    return new dtkRationalBezierCurveDataOn();
}

//
// dtkRationalBezierCurveDataOn.h ends here
