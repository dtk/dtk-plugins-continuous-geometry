// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "sislRationalBezierCurveSphereIntersectorTest.h"

#include "sislRationalBezierCurveSphereIntersector.h"

#include <dtkContinuousGeometry>
#include <dtkContinuousGeometrySettings>

#include <dtkAbstractRationalBezierCurveData>
#include <dtkRationalBezierCurve>

#include <dtkContinuousGeometryUtils>

#include <dtkTest>
// ///////////////////////////////////////////////////////////////////
// D-Pointer implementation
// ///////////////////////////////////////////////////////////////////

class sislRationalBezierCurveSphereIntersectorTestCasePrivate{
public:
    sislRationalBezierCurveSphereIntersector* intersector;
};
// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

sislRationalBezierCurveSphereIntersectorTestCase::sislRationalBezierCurveSphereIntersectorTestCase(void):d(new sislRationalBezierCurveSphereIntersectorTestCasePrivate())
{
    dtkLogger::instance().attachConsole();
    dtkLogger::instance().setLevel(dtk::LogLevel::Trace);

    dtkContinuousGeometrySettings settings;
    settings.beginGroup("continuous-geometry");
    dtkContinuousGeometry::initialize(settings.value("plugins").toString());

    settings.endGroup();
    d->intersector = nullptr;
}

sislRationalBezierCurveSphereIntersectorTestCase::~sislRationalBezierCurveSphereIntersectorTestCase(void)
{
    delete d;
}

void sislRationalBezierCurveSphereIntersectorTestCase::initTestCase(void)
{
    d->intersector = new sislRationalBezierCurveSphereIntersector();
}

void sislRationalBezierCurveSphereIntersectorTestCase::init(void)
{
}

void sislRationalBezierCurveSphereIntersectorTestCase::testRun(void)
{
    dtkAbstractRationalBezierCurveData *curve_data_1 = dtkContinuousGeometry::abstractRationalBezierCurveData::pluginFactory().create("dtkRationalBezierCurveDataOn");
    if(curve_data_1 == nullptr) {
        dtkWarn() << "This test requires that the opennurbs implementation of dtkRationalBezierCurveData is available";
        return;
    } else {
        std::size_t order = 2;

        std::vector< double > cps(4 * order);
        cps[0] = 0.;   cps[1] = 0.; cps[2] = 0.;  cps[3] = 1.;
        cps[4] = 2.;   cps[5] = 0.; cps[6] = 0.;  cps[7] = 1.;

        dtkRationalBezierCurve *rb_curve_1 = new dtkRationalBezierCurve(curve_data_1);

        rb_curve_1->create(order, cps.data());

        dtkContinuousGeometryPrimitives::Sphere_3 sphere_0(dtkContinuousGeometryPrimitives::Point_3(0., 0., 0.), 1.);

        d->intersector->setRationalBezierCurve(rb_curve_1);
        d->intersector->setSphere(sphere_0);
        d->intersector->run();

        std::vector< double > result = d->intersector->intersectionParameters();
        QVERIFY(result.size() == 1);
        QVERIFY(result.front() = 0.5);
    }
}

void sislRationalBezierCurveSphereIntersectorTestCase::cleanup(void)
{

}

void sislRationalBezierCurveSphereIntersectorTestCase::cleanupTestCase(void)
{
    if(d->intersector != nullptr) {
        delete d->intersector;
    }
}

DTKTEST_MAIN_NOGUI(sislRationalBezierCurveSphereIntersectorTest, sislRationalBezierCurveSphereIntersectorTestCase)

//
// sislRationalBezierCurveSphereIntersectorTest.cpp ends here
