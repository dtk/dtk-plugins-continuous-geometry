// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <QTest>

class sislRationalBezierCurveSphereIntersectorTestCasePrivate;

class sislRationalBezierCurveSphereIntersectorTestCase : public QObject
{
    Q_OBJECT

public:
    sislRationalBezierCurveSphereIntersectorTestCase(void);
    ~sislRationalBezierCurveSphereIntersectorTestCase(void);

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testRun(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);

private:
    sislRationalBezierCurveSphereIntersectorTestCasePrivate* d;
};

//
// sislRationalBezierCurveSphereIntersectorTest.h ends here
