// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore>

#include <dtkContinuousGeometry.h>

#include <dtkRationalBezierCurveSphereIntersector.h>

class sislRationalBezierCurveSphereIntersectorPlugin : public dtkRationalBezierCurveSphereIntersectorPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkRationalBezierCurveSphereIntersectorPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.sislRationalBezierCurveSphereIntersectorPlugin" FILE "sislRationalBezierCurveSphereIntersectorPlugin.json")

public:
     sislRationalBezierCurveSphereIntersectorPlugin(void) {}
    ~sislRationalBezierCurveSphereIntersectorPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

//
// sislRationalBezierCurveSphereIntersectorPlugin.h ends here
