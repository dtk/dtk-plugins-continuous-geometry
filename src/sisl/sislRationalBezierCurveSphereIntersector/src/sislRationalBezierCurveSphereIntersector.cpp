// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "sislRationalBezierCurveSphereIntersector.h"

#include <dtkContinuousGeometryUtils>
#include <dtkRationalBezierCurve>
#include <dtkRationalBezierCurveDataSisl.h>

#include <sisl.h>

// /////////////////////////////////////////////////////////////////
// sislRationalBezierCurveSphereIntersectorPrivate
// /////////////////////////////////////////////////////////////////

struct sislRationalBezierCurveSphereIntersectorPrivate
{
    const dtkRationalBezierCurve *rational_bezier_curve = nullptr;
    dtkContinuousGeometryPrimitives::Sphere_3 sphere = { {0., 0., 0.}, 0. };

    std::vector< double > intersection_parameters;
};

/*!
  \class sislRationalBezierCurveSphereIntersector
  \inmodule dtkPluginsContinuousGeometry
  \brief sislRationalBezierCurveSphereIntersector is a dtkNurbsProbing implementation of the concept dtkRationalBezierCurveSphereIntersector.

  The following lines of code show how to instanciate and use the class in pratice :
  \code
  dtkRationalBezierCurveSphereIntersector *intersector = dtkContinuousGeometry::rationalBezierCurveElevator::pluginFactory().create("sislRationalBezierCurveSphereIntersector");
  intersector->setRationalBezierCurve(...);
  intersector->setSphere(...);
  intersector->run();
  std::vector< double > intersection_parameters = intersector->intersectionParameters();
  \endcode
*/
sislRationalBezierCurveSphereIntersector::sislRationalBezierCurveSphereIntersector(void) : d(new sislRationalBezierCurveSphereIntersectorPrivate) {}

sislRationalBezierCurveSphereIntersector::~sislRationalBezierCurveSphereIntersector(void)
{
    delete d;
    d = nullptr;
}

/*! \fn sislRationalBezierCurveSphereIntersector::setRationalBezierCurve(const dtkRationalBezierCurve *curve)
  Provides the rational Bezier curve to the intersector (\a curve).

  \a curve : the rational Bezier curve to intersect
*/
void sislRationalBezierCurveSphereIntersector::setRationalBezierCurve(const dtkRationalBezierCurve *rational_bezier_curve)
{
    d->rational_bezier_curve = rational_bezier_curve;
}

/*! \fn sislRationalBezierCurveSphereIntersector::setSphere(dtkContinuousGeometryPrimitives::Sphere_3 *sphere)
  Provides the \a sphere to intersect the rational Bezier curve with to the intersector.

  \a sphere : the sphere to intersect the rational Bezier curve with
*/
void sislRationalBezierCurveSphereIntersector::setSphere(const dtkContinuousGeometryPrimitives::Sphere_3& sphere)
{
    d->sphere = sphere;
}

void sislRationalBezierCurveSphereIntersector::reset() {
    d->intersection_parameters.clear();
}

/*! \fn sislRationalBezierCurveSphereIntersector::run(void)
  Intersects the rational Bezier curve (provided with \l setRationalBezierCurve(dtkRationalBezierCurve *curve)) with the sphere(provided with \l setSphere(Sphere_3 *sphere)), the result can be retrieved with \l intersectionParameters(void).
*/
bool sislRationalBezierCurveSphereIntersector::run(void)
{
    // ///////////////////////////////////////////////////////////////////
    // Checks that all inputs are set
    // ///////////////////////////////////////////////////////////////////
    if (d->rational_bezier_curve == nullptr) {
        dtkFatal() << "Not all inputs of sislRationalBezierCurveSphereIntersector were set";
        return false;
    }

    double tolerance_3d = 1e-10;
    double geometry_resolution = 1e-10;
    double computational_resolution = 1e-15; //Not used (according to documentation)
    int numintpt = 0;
    double *intpar = 0;
    int numintcu = 0;
    SISLIntcurve **intcurve;
    int stat = 0;

    const dtkRationalBezierCurveDataSisl *sisl_rb_curve = dynamic_cast< const dtkRationalBezierCurveDataSisl *>(d->rational_bezier_curve->data());
    SISLCurve *sisl_curve = nullptr;


    // ///////////////////////////////////////////////////////////////////
    // Scale the rational Bezier curve
    // - Goal : bring the numerical values of the curve and the sphere radius close to 1
    // - Ex : - curve = 1e10, sphere = 1e-10 => do nothing
    //           10 - 10 / 2 = 0 => 10 / -10
    //        - curve = 1, sphere = 1e-10 => curve = 1e5, sphere = 1e-5
    //          (0 - 10) / 2 = -5
    //        - curve = 1e-5, sphere 1e-10 => curve = 1e2.5, sphere = 1e-2.5
    //          (-5 - 10) / 2 = -7.5
    // -compute the scaling factor :
    // find the middle; translate to 1
    //   - (log10(size of the curve) + log10(size of the sphere) / 2)
    // ///////////////////////////////////////////////////////////////////
    dtkContinuousGeometryPrimitives::AABB_3 aabb(0., 0., 0., 0., 0., 0.);
    d->rational_bezier_curve->aabb(aabb.data());
    double length_curve = std::sqrt((aabb[3] - aabb[0]) * (aabb[3] - aabb[0]) + (aabb[4] - aabb[1]) * (aabb[4] - aabb[1]) + (aabb[5] - aabb[2]) * (aabb[5] - aabb[2]));
    double log_length_curve = std::log10(length_curve);
    double log_radius = std::log10(d->sphere.radius());
    double log_coef = (log_length_curve + log_radius) / 2.;
    double coef = 1. / std::pow(10, log_coef);
    if(sisl_rb_curve != nullptr) {
        sisl_curve = const_cast< SISLCurve *>(sisl_rb_curve->sislRationalBezierCurve());
    } else {
        // ///////////////////////////////////////////////////////////////////
        // Convert the dtkRationalBezierSurface to a SISLCurve
        // ///////////////////////////////////////////////////////////////////
        std::size_t degree = d->rational_bezier_curve->degree();
        std::size_t nb_cp = degree + 1;

        double *et = (double*)malloc(sizeof(double)*2 * nb_cp);
        for(std::size_t i = 0; i < nb_cp; ++i) { et[i] = 0; }
        for(std::size_t i = nb_cp; i < 2 * nb_cp; ++i) { et[i] = 1; }

        double *ecoef = (double*)malloc(sizeof(double)*(3 + 1) * nb_cp);
        dtkContinuousGeometryPrimitives::Point_3 cp(0., 0., 0.);
        double w = 0.;
        for(std::size_t i = 0; i < nb_cp; ++i) {
            d->rational_bezier_curve->controlPoint(i, cp.data());
            d->rational_bezier_curve->weight(i, &w);
            ecoef[(3 + 1) * i]     = coef * (cp[0] - d->sphere.center()[0]) * w;
            ecoef[(3 + 1) * i + 1] = coef * (cp[1] - d->sphere.center()[1]) * w;
            ecoef[(3 + 1) * i + 2] = coef * (cp[2] - d->sphere.center()[2]) * w;
            ecoef[(3 + 1) * i + 3] = w;
        }

        /* Copy Flag = 2 : Use pointers to et1, et2 and ecoef; freeSurf will free arrays*/
        sisl_curve = newCurve(nb_cp, nb_cp, et, ecoef, 4, 3, 2);
    }

    d->sphere = dtkContinuousGeometryPrimitives::Sphere_3(dtkContinuousGeometryPrimitives::Point_3(0., 0., 0.), d->sphere.radius() * coef);

    s1371(sisl_curve, d->sphere.center().data(), d->sphere.radius(), 3, computational_resolution, geometry_resolution, &numintpt, &intpar, &numintcu, &intcurve, &stat);

    if(stat < 0) {
        dtkError() << "The intersection routine did not go well.";
        return false;
    }
    if(numintcu > 0) {
        dtkError() << "Not handling yet intersections other than points. " << Q_FUNC_INFO << ". Line : " << __LINE__;
    }

    for(auto i = 0; i < numintpt; ++i) {
        d->intersection_parameters.push_back(intpar[i]);
    }

    if(sisl_rb_curve == nullptr) {
        freeCurve(sisl_curve);
    }
    if(intpar != 0)
      free(intpar);
    return true;
}

/*! \fn sislRationalBezierCurveSphereIntersector::intersectionsParameters(void);
  Returns the intersection parameters in the dtkRationalBezierCurve space of parameters.
*/
std::vector< double > sislRationalBezierCurveSphereIntersector::intersectionParameters(void)
{
    return d->intersection_parameters;
}

//
// sislRationalBezierCurveSphereIntersector.cpp ends here
