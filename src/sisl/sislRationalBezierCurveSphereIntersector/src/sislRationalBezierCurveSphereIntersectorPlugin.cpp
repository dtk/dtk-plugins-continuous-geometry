// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "sislRationalBezierCurveSphereIntersector.h"
#include "sislRationalBezierCurveSphereIntersectorPlugin.h"

#include <dtkCore>
#include <dtkContinuousGeometry>

// ///////////////////////////////////////////////////////////////////
// sislRationalBezierCurveSphereIntersectorPlugin
// ///////////////////////////////////////////////////////////////////

void sislRationalBezierCurveSphereIntersectorPlugin::initialize(void)
{
    dtkContinuousGeometry::rationalBezierCurveSphereIntersector::pluginFactory().record("sislRationalBezierCurveSphereIntersector", sislRationalBezierCurveSphereIntersectorCreator);
}

void sislRationalBezierCurveSphereIntersectorPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(sislRationalBezierCurveSphereIntersector)

//
// sislRationalBezierCurveSphereIntersectorPlugin.cpp ends here
