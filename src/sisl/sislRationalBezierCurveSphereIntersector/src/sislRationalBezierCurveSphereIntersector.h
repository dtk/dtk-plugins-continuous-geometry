// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <dtkRationalBezierCurveSphereIntersector>

#include <sislRationalBezierCurveSphereIntersectorExport.h>

#include <dtkContinuousGeometry>

class dtkRationalBezierCurve;

struct sislRationalBezierCurveSphereIntersectorPrivate;

class SISLRATIONALBEZIERCURVESPHEREINTERSECTOR_EXPORT sislRationalBezierCurveSphereIntersector : public dtkRationalBezierCurveSphereIntersector
{
 public:
    sislRationalBezierCurveSphereIntersector(void);
    ~sislRationalBezierCurveSphereIntersector(void);

 public:
    void setRationalBezierCurve(const dtkRationalBezierCurve *) final;
    void setSphere(const dtkContinuousGeometryPrimitives::Sphere_3& sphere) final;

 public:
    bool run(void) final;
    void reset() final;

 public:
    std::vector< double > intersectionParameters(void) final;

 protected:
    sislRationalBezierCurveSphereIntersectorPrivate *d;
};


// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

inline dtkRationalBezierCurveSphereIntersector *sislRationalBezierCurveSphereIntersectorCreator(void)
{
    return new sislRationalBezierCurveSphereIntersector();
}

//
// sislRationalBezierCurveSphereIntersector.h ends here
