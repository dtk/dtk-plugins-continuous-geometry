// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "sislRationalBezierSurfaceSegmentIntersectorTest.h"

#include "sislRationalBezierSurfaceSegmentIntersector.h"

#include <dtkContinuousGeometry>
#include <dtkContinuousGeometrySettings>

#include <dtkAbstractRationalBezierSurfaceData>
#include <dtkRationalBezierSurface>

#include <dtkContinuousGeometryUtils>

#include <dtkTest>
// ///////////////////////////////////////////////////////////////////
// D-Pointer implementation
// ///////////////////////////////////////////////////////////////////

class sislRationalBezierSurfaceSegmentIntersectorTestCasePrivate{
public:
    sislRationalBezierSurfaceSegmentIntersector* intersector;
};
// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

sislRationalBezierSurfaceSegmentIntersectorTestCase::sislRationalBezierSurfaceSegmentIntersectorTestCase(void):d(new sislRationalBezierSurfaceSegmentIntersectorTestCasePrivate())
{
    dtkLogger::instance().attachConsole();
    dtkLogger::instance().setLevel(dtk::LogLevel::Trace);

    dtkContinuousGeometrySettings settings;
    settings.beginGroup("continuous-geometry");
    dtkContinuousGeometry::initialize(settings.value("plugins").toString());

    settings.endGroup();
    d->intersector = nullptr;
}

sislRationalBezierSurfaceSegmentIntersectorTestCase::~sislRationalBezierSurfaceSegmentIntersectorTestCase(void)
{
    delete d;
}

void sislRationalBezierSurfaceSegmentIntersectorTestCase::initTestCase(void)
{
    d->intersector = new sislRationalBezierSurfaceSegmentIntersector();
}

void sislRationalBezierSurfaceSegmentIntersectorTestCase::init(void)
{
}

void sislRationalBezierSurfaceSegmentIntersectorTestCase::testRun(void)
{
    dtkAbstractRationalBezierSurfaceData *surface_data_1_1_0 = dtkContinuousGeometry::abstractRationalBezierSurfaceData::pluginFactory().create("dtkRationalBezierSurfaceDataOn");
    if(surface_data_1_1_0 == nullptr) {
        dtkWarn() << "This test requires that the openNURBS implementation of dtkRationalBezierSurfaceData is available";
        return;
    } else {
        std::size_t dim = 3;
        std::size_t order_u = 2;
        std::size_t order_v = 2;

        std::vector< double > cps_0((dim + 1) * order_u * order_v);
        cps_0[0] = 0.;   cps_0[1] = 0.; cps_0[2] = 0.;  cps_0[3] = 1.;
        cps_0[4] = 2.;   cps_0[5] = 0.; cps_0[6] = 0.;  cps_0[7] = 1.;
        cps_0[8] = 0.;   cps_0[9] = 2.; cps_0[10] = 0.; cps_0[11] = 1.;
        cps_0[12] = 2.;  cps_0[13] = 2.;  cps_0[14] = 0.; cps_0[15] = 1.;

        dtkRationalBezierSurface *rb_surface_1_1_0 = new dtkRationalBezierSurface(surface_data_1_1_0);

        rb_surface_1_1_0->create(dim, order_u, order_v, cps_0.data());

        dtkContinuousGeometryPrimitives::Segment_3 segment_0(dtkContinuousGeometryPrimitives::Point_3(.5, .5, -10.), dtkContinuousGeometryPrimitives::Point_3(.5, .5, 10.));

        d->intersector->setRationalBezierSurface(rb_surface_1_1_0);
        d->intersector->setSegment(&segment_0);
        d->intersector->run();

        auto result = d->intersector->intersection();
        QVERIFY(result.size() == 1);
        QVERIFY(result.front().isPoint());

        QVERIFY(result.front().pointIntersection().image()[0] == 0.5);
        QVERIFY(result.front().pointIntersection().image()[1] == 0.5);
        QVERIFY(result.front().pointIntersection().image()[2] == 0.);

        QVERIFY(result.front().pointIntersection().preImages().front()[0] == 0.25);
        QVERIFY(result.front().pointIntersection().preImages().front()[1] == 0.25);


        dtkContinuousGeometryPrimitives::Segment_3 segment_1(dtkContinuousGeometryPrimitives::Point_3(.5, .5, -10.), dtkContinuousGeometryPrimitives::Point_3(.5, .5, -5.));
        d->intersector->reset();
        d->intersector->setSegment(&segment_1);
        d->intersector->run();

        result = d->intersector->intersection();
        QVERIFY(result.size() == 0);
    }
}

void sislRationalBezierSurfaceSegmentIntersectorTestCase::cleanup(void)
{

}

void sislRationalBezierSurfaceSegmentIntersectorTestCase::cleanupTestCase(void)
{
    if(d->intersector != nullptr) {
        delete d->intersector;
    }
}

DTKTEST_MAIN_NOGUI(sislRationalBezierSurfaceSegmentIntersectorTest, sislRationalBezierSurfaceSegmentIntersectorTestCase)

//
// sislRationalBezierSurfaceSegmentIntersectorTest.cpp ends here
