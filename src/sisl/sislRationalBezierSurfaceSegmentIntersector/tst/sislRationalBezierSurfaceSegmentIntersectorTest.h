// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <QTest>

class sislRationalBezierSurfaceSegmentIntersectorTestCasePrivate;

class sislRationalBezierSurfaceSegmentIntersectorTestCase : public QObject
{
    Q_OBJECT

public:
    sislRationalBezierSurfaceSegmentIntersectorTestCase(void);
    ~sislRationalBezierSurfaceSegmentIntersectorTestCase(void);

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testRun(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);

private:
    sislRationalBezierSurfaceSegmentIntersectorTestCasePrivate* d;
};

//
// sislRationalBezierSurfaceSegmentIntersectorTest.h ends here
