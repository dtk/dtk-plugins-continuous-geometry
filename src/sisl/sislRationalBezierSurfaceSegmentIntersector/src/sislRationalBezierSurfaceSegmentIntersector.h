// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkRationalBezierSurfaceSegmentIntersector>

#include <sislRationalBezierSurfaceSegmentIntersectorExport.h>

#include <dtkContinuousGeometryUtils.h>

struct sislRationalBezierSurfaceSegmentIntersectorPrivate;

class dtkRationalBezierSurface;

class SISLRATIONALBEZIERSURFACESEGMENTINTERSECTOR_EXPORT sislRationalBezierSurfaceSegmentIntersector : public dtkRationalBezierSurfaceSegmentIntersector
{
 public:
    sislRationalBezierSurfaceSegmentIntersector(void);
    ~sislRationalBezierSurfaceSegmentIntersector(void);

    void setRationalBezierSurface(dtkRationalBezierSurface *surface) final;
    void setSegment(dtkContinuousGeometryPrimitives::Segment_3 *segment) final;

    bool run(void) final;

    void reset() final;

    const std::vector< dtkContinuousGeometryPrimitives::IntersectionObject > & intersection(void) final;

protected:
    sislRationalBezierSurfaceSegmentIntersectorPrivate *d;
};

// ///////////////////////////////////////////////////////////////////

inline dtkRationalBezierSurfaceSegmentIntersector* sislRationalBezierSurfaceSegmentIntersectorCreator(void)
{
    return new sislRationalBezierSurfaceSegmentIntersector();
}

//
// sislRationalBezierSurfaceSegmentIntersector.h ends here
