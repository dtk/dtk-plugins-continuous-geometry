// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <QtCore>

#include <dtkContinuousGeometry.h>

#include <dtkRationalBezierSurfaceSegmentIntersector.h>

class sislRationalBezierSurfaceSegmentIntersectorPlugin : public dtkRationalBezierSurfaceSegmentIntersectorPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkRationalBezierSurfaceSegmentIntersectorPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.sislRationalBezierSurfaceSegmentIntersectorPlugin" FILE "sislRationalBezierSurfaceSegmentIntersectorPlugin.json")

public:
     sislRationalBezierSurfaceSegmentIntersectorPlugin(void) {}
    ~sislRationalBezierSurfaceSegmentIntersectorPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

//
// sislRationalBezierSurfaceSegmentIntersectorPlugin.h ends here
