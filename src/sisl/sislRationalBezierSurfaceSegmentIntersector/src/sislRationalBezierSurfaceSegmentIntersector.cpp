// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#define newarray(a,b) ((b*)malloc((size_t)((a)*sizeof(b))))

#include "sislRationalBezierSurfaceSegmentIntersector.h"

#include <dtkContinuousGeometryUtils>
#include <dtkRationalBezierSurface>

#include <dtkRationalBezierSurfaceDataSisl.h>

#include <sisl.h>

struct sislRationalBezierSurfaceSegmentIntersectorPrivate
{
    void reset() {
      intersection_objects.clear();
    }

    dtkRationalBezierSurface *rb_surface = nullptr; // non-owning
    dtkContinuousGeometryPrimitives::Segment_3 *segment = nullptr; // non-owning
    std::vector< dtkContinuousGeometryPrimitives::IntersectionObject > intersection_objects;
};

/*!
  \class sislRationalBezierSurfaceSegmentIntersector
  \inmodule dtkPluginsContinuousGeometry
  \brief sislRationalBezierSurfaceSegmentIntersector is a SISL implementation of the concept dtkRationalBezierSurfaceSegmentIntersector describing the intersection between a segment and a rational Bezier surface.

  The following segments of code show how to instanciate and use the class in pratice :
  \code
  dtkRationalBezierSurfaceSegmentIntersector *intersector = dtkContinuousGeometry::nurbsSurfaceSegmentIntersector::pluginFactory().create("sislRationalBezierSurfaceSegmentIntersector");
  intersector->setRationalBezierSurface(...);
  intersector->setSegment(...);
  intersector->run();
  const std::vector< IntersectionObject > & intersection_object = intersector->intersection();
  \endcode
*/

sislRationalBezierSurfaceSegmentIntersector::sislRationalBezierSurfaceSegmentIntersector(void) : dtkRationalBezierSurfaceSegmentIntersector(), d(new sislRationalBezierSurfaceSegmentIntersectorPrivate) {}

sislRationalBezierSurfaceSegmentIntersector::~sislRationalBezierSurfaceSegmentIntersector(void)
{
    delete d;
}

void sislRationalBezierSurfaceSegmentIntersector::reset()
{
    d->reset();
}

/*! \fn sislRationalBezierSurfaceSegmentIntersector::setRationalBezierSurface(dtkRationalBezierSurface *surface)
  Provides the rational Bezier surface to the intersector (\a surface).

  \a surface : the rational Bezier surface to intersect
*/
void sislRationalBezierSurfaceSegmentIntersector::setRationalBezierSurface(dtkRationalBezierSurface *rb_surface)
{
    d->rb_surface = rb_surface;
}

/*! \fn sislRationalBezierSurfaceSegmentIntersector::setSegment(dtkContinuousGeometryPrimitives::Segment_3 *segment)
  Provides the \a segment to intersect the surface with to the intersector.

  \a segment : the segment to intersect the rational Bezier surface with
*/
void sislRationalBezierSurfaceSegmentIntersector::setSegment(dtkContinuousGeometryPrimitives::Segment_3* segment)
{
    d->segment = segment;
}

/*! \fn sislRationalBezierSurfaceSegmentIntersector::run(void)
  Intersects the rational Bezier surface (provided with \l setRationalBezierSurface(dtkRationalBezierSurface *surface)) with the segment(provided with \l setSegment(Segment_3 *segment)), the result can be retrieved with \l intersection(void).
*/
bool sislRationalBezierSurfaceSegmentIntersector::run(void)
{
    if(d->rb_surface == nullptr) {
        dtkFatal() << "The surface was not set before calling the run() method.";
    }
    if(d->segment == nullptr) {
        dtkFatal() << "The segment was not set before calling the run() method.";
    }

    double tolerance_3d = 1e-6;
    double geometry_resolution = 1e-6;
    double computational_resolution = 1e-15; //Not used (according to documentation)
    int numintpt = 0;
    double *pointpar = 0;
    int numintcr = 0;
    SISLIntcurve **intcurves;
    int stat = 0;

    dtkRationalBezierSurfaceDataSisl *sisl_rb_surface = dynamic_cast<dtkRationalBezierSurfaceDataSisl *>(d->rb_surface->data());
    SISLSurf *sisl_surf = nullptr;

    if(sisl_rb_surface != nullptr) {
        sisl_surf = const_cast< SISLSurf *>(sisl_rb_surface->sislRationalBezierSurface());
    } else {
        // ///////////////////////////////////////////////////////////////////
        // Convert the dtkRationalBezierSurface to a SISL_surf
        // ///////////////////////////////////////////////////////////////////
        std::size_t u_degree = d->rb_surface->uDegree();
        std::size_t v_degree = d->rb_surface->vDegree();
        std::size_t nb_cp_u = u_degree + 1;
        std::size_t nb_cp_v = v_degree + 1;

        double *et1 = newarray(2 * nb_cp_u, double);
        for(std::size_t i = 0; i < nb_cp_u; ++i) { et1[i] = 0; }
        for(std::size_t i = nb_cp_u; i < 2 * nb_cp_u; ++i) { et1[i] = 1; }
        double *et2 = newarray(2 * nb_cp_v, double);
        for(std::size_t i = 0; i < nb_cp_v; ++i) { et2[i] = 0; }
        for(std::size_t i = nb_cp_v; i < 2 * nb_cp_v; ++i) { et2[i] = 1; }

        double *ecoef = newarray((3 + 1) * nb_cp_u * nb_cp_v, double);
        dtkContinuousGeometryPrimitives::Point_3 cp(0., 0., 0.);
        double w = 0.;
        for(std::size_t i = 0; i < nb_cp_u; ++i) {
            for(std::size_t j = 0; j < nb_cp_v; ++j) {
                d->rb_surface->controlPoint(i, j, cp.data());
                d->rb_surface->weight(i, j, &w);
                ecoef[(3 + 1) * nb_cp_u * j + (3 + 1) * i]     = cp[0] * w;
                ecoef[(3 + 1) * nb_cp_u * j + (3 + 1) * i + 1] = cp[1] * w;
                ecoef[(3 + 1) * nb_cp_u * j + (3 + 1) * i + 2] = cp[2] * w;
                ecoef[(3 + 1) * nb_cp_u * j + (3 + 1) * i + 3] = w;
            }
        }

        /* Copy Flag = 2 : Use pointers to et1, et2 and ecoef; freeSurf will free arsegments*/
        sisl_surf = newSurf(nb_cp_u, nb_cp_v, nb_cp_u, nb_cp_v, et1, et2, ecoef, 4, 3, 2);
    }

    dtkContinuousGeometryPrimitives::Vector_3 dir = (*d->segment)[1] - (*d->segment)[0];
    s1856(sisl_surf, (*d->segment)[0].data(), dir.data(), 3, computational_resolution, geometry_resolution, &numintpt, &pointpar, &numintcr, &intcurves, &stat);

    if(stat < 0) {
        dtkError() << "The intersection routine did not go well.";
        return false;
    }
    if(numintcr > 0) {
        dtkError() << "Not handling yet intersections other than points. " << Q_FUNC_INFO << ". Line : " << __LINE__;
    }

    dtkContinuousGeometryPrimitives::Point_3 eval(0., 0., 0.);
    for (auto i = 0; i < numintpt; ++i) {
        d->rb_surface->evaluatePoint(pointpar[2*i], pointpar[2*i + 1], eval.data());
        // ///////////////////////////////////////////////////////////////////
        // Computes the angle between P-P0 and dir
        // ///////////////////////////////////////////////////////////////////
        dtkContinuousGeometryPrimitives::Vector_3 sol_a = eval - (*d->segment)[0];
        dtkContinuousGeometryPrimitives::Vector_3 sol_b = eval - (*d->segment)[1];
         if(dtkContinuousGeometryTools::norm(sol_a) > tolerance_3d && dtkContinuousGeometryTools::norm(sol_b) > tolerance_3d) {
             double angle_a = std::acos(std::min(std::max(dtkContinuousGeometryTools::dotProduct(sol_a, dir) / (dtkContinuousGeometryTools::norm(sol_a) * dtkContinuousGeometryTools::norm(dir)), -1.), 1.));
             double angle_b = std::acos(std::min(std::max(dtkContinuousGeometryTools::dotProduct(sol_b, dir) / (dtkContinuousGeometryTools::norm(sol_b) * dtkContinuousGeometryTools::norm(dir)), -1.), 1.));
            if(angle_a < (M_PI / 2.) && angle_b > (M_PI / 2.)) {
                dtkContinuousGeometryPrimitives::PointImage p_img(eval, true);
                dtkContinuousGeometryPrimitives::PointIntersection p_int(p_img);
                dtkContinuousGeometryPrimitives::PointPreImage p_pre_img(dtkContinuousGeometryPrimitives::Point_2(pointpar[2*i], pointpar[2*i + 1]), true);
                p_int.pushBackPreImage(p_pre_img);
                d->intersection_objects.emplace_back(p_int);
            }
        } else {
            dtkContinuousGeometryPrimitives::PointImage p_img(eval, false);
            dtkContinuousGeometryPrimitives::PointIntersection p_int(p_img);
            dtkContinuousGeometryPrimitives::PointPreImage p_pre_img(dtkContinuousGeometryPrimitives::Point_2(pointpar[2*i], pointpar[2*i + 1]), true);
            p_int.pushBackPreImage(p_pre_img);
            d->intersection_objects.emplace_back(p_int);
        }
    }

    if(sisl_rb_surface == nullptr) {
        freeSurf(sisl_surf);
        if (pointpar != nullptr)
        {
          free(pointpar);
          pointpar = nullptr;//allocated in SISL, needs freeing
        }
    }
    return true;
}

/*! \fn sislRationalBezierSurfaceSegmentIntersector::intersection(void)
  Returns the intersection objects.

  The caller is responsible for deleting all the IntersectionObject of the returned vector, as well as the vector.
*/
const std::vector< dtkContinuousGeometryPrimitives::IntersectionObject>&
sislRationalBezierSurfaceSegmentIntersector::intersection(void)
{
    return d->intersection_objects;
}

//
// sislRationalBezierSurfaceSegmentIntersector.cpp ends here
