// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "sislRationalBezierSurfaceSegmentIntersector.h"
#include "sislRationalBezierSurfaceSegmentIntersectorPlugin.h"

#include <dtkCore>
#include <dtkContinuousGeometry>

// ///////////////////////////////////////////////////////////////////
// sislRationalBezierSurfaceSegmentIntersectorPlugin
// ///////////////////////////////////////////////////////////////////

void sislRationalBezierSurfaceSegmentIntersectorPlugin::initialize(void)
{
    dtkContinuousGeometry::rationalBezierSurfaceSegmentIntersector::pluginFactory().record("sislRationalBezierSurfaceSegmentIntersector", sislRationalBezierSurfaceSegmentIntersectorCreator);
}

void sislRationalBezierSurfaceSegmentIntersectorPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(sislRationalBezierSurfaceSegmentIntersector)

//
// sislRationalBezierSurfaceSegmentIntersectorPlugin.cpp ends here
