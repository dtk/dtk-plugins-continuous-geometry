// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <QtCore>

#include <dtkContinuousGeometry>

#include <dtkAbstractRationalBezierCurveData>

class dtkRationalBezierCurveDataSislPlugin : public dtkAbstractRationalBezierCurveDataPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkAbstractRationalBezierCurveDataPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkRationalBezierCurveDataSislPlugin" FILE "dtkRationalBezierCurveDataSislPlugin.json")

public:
     dtkRationalBezierCurveDataSislPlugin(void) {}
    ~dtkRationalBezierCurveDataSislPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

//
// dtkRationalBezierCurveDataSislPlugin.h ends here
