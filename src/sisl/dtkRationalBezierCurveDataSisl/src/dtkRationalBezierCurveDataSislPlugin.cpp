// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#include "dtkRationalBezierCurveDataSisl.h"
#include "dtkRationalBezierCurveDataSislPlugin.h"

#include <dtkCore>
#include <dtkContinuousGeometry>

// ///////////////////////////////////////////////////////////////////
// dtkRationalBezierCurveDataSislPlugin
// ///////////////////////////////////////////////////////////////////

void dtkRationalBezierCurveDataSislPlugin::initialize(void)
{
    dtkContinuousGeometry::abstractRationalBezierCurveData::pluginFactory().record("dtkRationalBezierCurveDataSisl", dtkRationalBezierCurveDataSislCreator);
}

void dtkRationalBezierCurveDataSislPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkRationalBezierCurveDataSisl)

//
// dtkRationalBezierCurveDataSislPlugin.cpp ends here
