// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkRationalBezierCurveDataSisl.h"

#include <dtkRationalBezierCurve>

#include <sisl.h>

#include <cassert>

/*!
  \class dtkRationalBezierCurveDataSisl
  \inmodule dtkPluginsContinuousGeometry
  \brief dtkRationalBezierCurveDataSisl is an openNURBS implementation of the concept dtkAbstractRationalBezierCurveData.

  The following lines of code show how to instanciate and use the class in pratice:
  \code
  dtkAbstractRationalBezierCurveData *rational_bezier_curve_data = dtkContinuousGeometry::abstractRationalBezierCurveData::pluginFactory().create("dtkRationalBezierCurveDataSisl");
  dtkRationalBezierCurve *rational_bezier_curve = new dtkRationalBezierCurve(rational_bezier_curve_data);
  rational_bezier_curve->create(...);
  \endcode
*/

/*! \fn dtkRationalBezierCurveDataSisl::dtkRationalBezierCurveDataSisl(void)
  Instanciates the structure.

  To add the content, see \l create(std::size_t order, double* cps) const.
*/
dtkRationalBezierCurveDataSisl::dtkRationalBezierCurveDataSisl(void) : d(nullptr) {}

/*! \fn dtkRationalBezierCurveDataSisl::dtkRationalBezierCurveDataSisl(const dtkRationalBezierCurveDataSisl& curve_data)
  Copy constructor.

  Not available in the dtkAbstractRationalBezierCurveData.

  Creates the rational Bezier curve by copying the provided rational Bezier curve.

  \a curve_data : the rational Bezier curve to copy.
*/
dtkRationalBezierCurveDataSisl::dtkRationalBezierCurveDataSisl(const dtkRationalBezierCurveDataSisl& curve_data)
{
    d = copyCurve(curve_data.d);
    assert(d != nullptr);
}

/*! \fn dtkRationalBezierCurveDataSisl::~dtkRationalBezierCurveDataSisl(void)
  Destroys the instance.
*/
dtkRationalBezierCurveDataSisl::~dtkRationalBezierCurveDataSisl(void)
{
    delete[] m_ecoef;
    freeCurve(d);
}

/*! \fn dtkRationalBezierCurveDataSisl::create()
  Not available in the dtkAbstractRationalBezierCurveData.

  Creates an empty rational Bezier curve object.
*/
void dtkRationalBezierCurveDataSisl::create()
{
    dtkDebug() << "Not implemented.";
}

/*! \fn dtkRationalBezierCurveDataSisl::create(const ON_BezierCurve& bezier_curve)
  Not available in the dtkAbstractRationalBezierCurveData.

  Creates the rational Bezier curve by providing a rational Bezier curve from the openNURBS API.

  \a bezier_curve : the rational Bezier curve to copy.
*/
void dtkRationalBezierCurveDataSisl::create(const SISLCurve& bezier_curve)
{
    d = copyCurve(const_cast< SISLCurve * >(&bezier_curve));
    assert(d != nullptr);
}

/*! \fn dtkRationalBezierCurveDataSisl::create(std::size_t order, double *cps) const
  Creates the rational Bezier curve by providing the required content.

 \a order : order

 \a cps : array containing the weighted control points, specified as : [x0, y0, w0, ...]
*/
void dtkRationalBezierCurveDataSisl::create(std::size_t order, double *cps) const
{
    // /////////////////////////////////////////////////////////////////
    // Initializes the Open RationalBezier NURBS surface
    // /////////////////////////////////////////////////////////////////
    dtkRationalBezierCurveDataSisl *self = const_cast< dtkRationalBezierCurveDataSisl * >(this);
    self->m_et = new double[2 * order];
    for(std::size_t i = 0; i < order; ++i) {
        self->m_et[i] = 0;
    }
    for(std::size_t i = order; i < 2 * order; ++i) {
        self->m_et[i] = 1;
    }

    self->m_ecoef = new double[4 * order];
    for(std::size_t i = 0; i < order; ++i) {
        self->m_ecoef[4 * i]     = cps[4 * i]     * cps[4 * i + 3];
        self->m_ecoef[4 * i + 1] = cps[4 * i + 1] * cps[4 * i + 3];
        self->m_ecoef[4 * i + 2] = cps[4 * i + 2] * cps[4 * i + 3];
        self->m_ecoef[4 * i + 3] = cps[4 * i + 3];
    }

    /* Copy Flag = 0 : Set internal pointer to input array and remember to free arrays*/
    self->d = newCurve(order, order, self->m_et, self->m_ecoef, 4, 3, 0);

    if(d == nullptr) {
        dtkFatal() << "The dtkRationalBezierCurveDataSisl is not valid";
    }
}

/*! \fn void dtkRationalBezierCurveDataSisl::create(std::string path) const
  Creates a rational Bezier curve by providing a path to a file storing the required information to create a rational Bezier curve.

  \a path : a valid path to the file containing the information regarding a given rational Bezier curve
*/
void dtkRationalBezierCurveDataSisl::create(std::string path) const
{
    QFile file(QString::fromStdString(path));
    QIODevice *in = &file;

    QIODevice::OpenMode mode = QIODevice::ReadOnly;
    mode |= QIODevice::Text;

    // to avoid troubles with floats separators ('.' and not ',')
    QLocale::setDefault(QLocale::c());
#if defined (Q_OS_UNIX) && !defined(Q_OS_MAC)
    setlocale(LC_NUMERIC, "C");
#endif

    if (!in->open(mode)) {
        dtkFatal() << Q_FUNC_INFO << "The file could not be found, please verify the path";
        return;
    }

    QString line = in->readLine().trimmed();
    //Run tests on the file to check its validity TODO more
    if ( line != "#dtkRationalBezierCurveData"){
        dtkFatal() << Q_FUNC_INFO << "file not in a dtkRationalBezierCurveData format.";
        return;
    }
    // std::size_t dim = 3;
    QRegExp re = QRegExp("\\s+");
    line = in->readLine().trimmed();
    QStringList vals = line.split(re);
    std::size_t order = vals[1].toInt() + 1;

    dtkRationalBezierCurveDataSisl *self = const_cast< dtkRationalBezierCurveDataSisl * >(this);

    self->m_et = new double[2 * order];
    for(std::size_t i = 0; i < order; ++i) {
        self->m_et[i] = 0;
    }
    for(std::size_t i = order; i < 2 * order; ++i) {
        self->m_et[i] = 1;
    }

    self->m_ecoef = new double[4 * order];
    for(std::size_t i = 0; i < order; ++i) {
        line = in->readLine().trimmed();
        if (line.startsWith("#")) {
            continue;
        }
        std::istringstream lin(line.toStdString());
        double x, y, z, w;
        lin >> x >> y >> z >> w;       //now read the whitespace-separated floats
        self->m_ecoef[4 * i]     = x * w;
        self->m_ecoef[4 * i + 1] = y * w;
        self->m_ecoef[4 * i + 2] = z * w;
        self->m_ecoef[4 * i + 3] = w;
    }

    /* Copy Flag = 0 : Set internal pointer to input array and remember to free arrays*/
    self->d = newCurve(order, order, self->m_et, self->m_ecoef, 4, 3, 0);

    if(d == nullptr) {
        dtkFatal() << "The dtkRationalBezierCurveDataSisl is not valid";
    }
}

/*! \fn  dtkRationalBezierCurveDataSisl::degree(void) const
  Returns the degree of the curve.
*/
std::size_t dtkRationalBezierCurveDataSisl::degree(void) const
{
    return d->ik - 1;
}

/*! \fn  dtkRationalBezierCurveDataSisl::controlPoint(std::size_t i, double *r_cp) const
  Writes in \a r_cp the \a i th weighted control point coordinates[xi, yi].

  \a i : index of the weighted control point

  \a r_cp : array of size 3 to store the weighted control point coordinates[xi, yi, zi]

  The method can be called as follow:
  \code
  dtkContinuousGeometryPrimitives::Point_3 point3(0, 0, 0);
  rational_bezier_curve_data->controlPoint(3, point3.data());
  \endcode
*/
void dtkRationalBezierCurveDataSisl::controlPoint(std::size_t i, double* r_cp) const
{
    for (std::size_t k = 0; k < 3; ++k) {
        r_cp[k] = d->ecoef[i * 3 + k];
    }
}

/*! \fn  dtkRationalBezierCurveDataSisl::weight(std::size_t i, double *r_w) const
  Writes in \a r_w the \a i th weighted control point weight wi.

  \a i : index of the weighted control point

  \a r_w : array of size 1 to store the weighted control point weight wi
*/
void dtkRationalBezierCurveDataSisl::weight(std::size_t i, double* r_w) const
{
    *r_w = d->rcoef[i * 4 + 3];
}

/*! \fn  dtkRationalBezierCurveDataSisl::setWeightedControlPoint(std::size_t i, double *p_cp)
  Modifies the \a i th weighted control point by \a p_cp : [xi, yi, zi, wi].

  \a i : index of the weighted control point

  \a p_cp : array of size 4 containing the weighted control point coordinates and weight  : [xi, yi, zi, wi]
*/
void dtkRationalBezierCurveDataSisl::setWeightedControlPoint(std::size_t, double *)
{
    dtkError() << "Not implemented";
}

/*! \fn dtkRationalBezierCurveDataSisl::evaluatePoint(double p_u, double *r_point) const
  Writes in \a r_point the evaluation at parameter \a p_u.

  \a p_u : parameter

  \a r_point : array of size 3 to store the result of the evaluation
*/
void dtkRationalBezierCurveDataSisl::evaluatePoint(double, double*) const
{
    dtkError() << "Not implemented";
}

/*! \fn dtkRationalBezierCurveDataSisl::evaluateNormal(double p_u, double *r_normal) const
  Writes in \a r_normal the normal evaluation at parameter \a p_u.

  \a p_u : parameter

  \a r_normal : array of size 3 to store the result of the normal evaluation
*/
void dtkRationalBezierCurveDataSisl::evaluateNormal(double , double*) const
{
    dtkError() << "Not implemented";
}

void dtkRationalBezierCurveDataSisl::evaluateDerivative(double, double*) const
{
    dtkError() << "Not implemented";
}

void dtkRationalBezierCurveDataSisl::evaluateCurvature(double, double*) const
{
    dtkError() << "Not implemented";
}

/*! \fn dtkRationalBezierCurveDataSisl::split(dtkRationalBezierCurve *r_split_curve_a, dtkRationalBezierCurve *r_split_curve_b, double splitting_parameter) const
  Splits at \a splitting_parameter the rational Bezier curve and stores in \a r_split_curve_a and \a r_split_curve_b the split parts (from 0 to \a splitting_parameter and from \a splitting_parameter to 1 respectively).

  \a r_split_curve_a : a pointer to a valid dtkRationalBezierCurve with data not initialized, in which the first part of the curve (from 0 to to \a splitting_parameter) will be stored

  \a r_split_curve_b : a pointer to a valid dtkRationalBezierCurve with data not initialized, in which the second part of the curve (from \a splitting_parameter to 1) will be stored

  \a splitting_parameter : the parameter at which the curve will be split
*/
void dtkRationalBezierCurveDataSisl::split(dtkRationalBezierCurve *, dtkRationalBezierCurve *, double) const
{
    dtkError() << "Not implemented";
}

/*! \fn dtkRationalBezierCurveDataSisl::split(std::list< dtkRationalBezierCurve * >& r_split_curves, const std::vector< double >& p_splitting_parameters) const
  Splits the rational Bezier curve at each value of \a p_splitting_parameters and stores in \a r_split_curves the split parts.

  \a r_split_curves : a list to which the split rational Bezier curves will be added

  \a p_splitting_parameters : the parameters at which the curve will be split
*/
void dtkRationalBezierCurveDataSisl::split(std::list< dtkRationalBezierCurve * >&, const std::vector< double >&) const
{
    dtkError() << "Not implemented";
}

/*! \fn dtkRationalBezierCurveDataSisl::aabb(double* r_aabb) const
  Writes in \a r_aabb the [xmin, ymin, zmin, xmax, ymax, zmax] coordinates.

  \a r_aabb : array of size 6 to store the limits coordinates
*/
void dtkRationalBezierCurveDataSisl::aabb(double*) const
{
    dtkError() << "Not implemented";
}

/*! \fn dtkRationalBezierCurveDataSisl::extendedAabb(double *r_aabb, double factor) const
  Writes in \a r_aabb the [xmin, ymin, xmax, ymax] coordinates of an extened bounding box of factor \a factor. The extension factor extends the diagonal.

  \a r_aabb : array of size 6 to store the extended limits coordinates

  \a factor : factor used to lengthen the diagonal of the axis aligned bounding box
*/
void dtkRationalBezierCurveDataSisl::extendedAabb(double*, double) const
{
    dtkError() << "Not implemented";
}

/*! \fn dtkRationalBezierCurveDataSisl::onRationalBezierCurve(void)
  Not available in the dtkAbstractRationalBezierCurveData.

  Returns the underlying openNURBS rational Bezier curve.
 */
const SISLCurve *dtkRationalBezierCurveDataSisl::sislRationalBezierCurve(void) const
{
    return d;
}

/*! \fn dtkRationalBezierCurveDataSisl::clone(void) const
   Clone
*/
dtkRationalBezierCurveDataSisl* dtkRationalBezierCurveDataSisl::clone(void) const
{
    return new dtkRationalBezierCurveDataSisl(*this);
}

#include <iomanip>
void dtkRationalBezierCurveDataSisl::print(std::ostream& stream) const
{
    stream << "#dtkRationalBezierCurveData" << std::endl;
    stream << "degree " << d->ik << std::endl;
    for(auto i = 0; i < d->ik; ++i) {
        std::size_t index = i * 3;
        stream << d->ecoef[index] << " " << d->ecoef[index + 1] << " " << d->ecoef[index + 2] << " " << d->rcoef[i * 4 + 3] << std::setprecision(std::numeric_limits< double >::digits10 + 1) << std::endl;
    }
    stream << "#dtkRationalBezierCurveData" << std::endl;
}

//
// dtkRationalBezierCurveDataSisl.cpp ends here
