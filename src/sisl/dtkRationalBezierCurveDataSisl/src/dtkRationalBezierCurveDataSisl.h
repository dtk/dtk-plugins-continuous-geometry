// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkRationalBezierCurveDataSislExport.h>

#include <dtkContinuousGeometry>
#include <dtkAbstractRationalBezierCurveData>

struct SISLCurve;

class DTKRATIONALBEZIERCURVEDATASISL_EXPORT dtkRationalBezierCurveDataSisl final : public dtkAbstractRationalBezierCurveData
{
public:
    dtkRationalBezierCurveDataSisl(void);
    dtkRationalBezierCurveDataSisl(const dtkRationalBezierCurveDataSisl& curve_data);

    ~dtkRationalBezierCurveDataSisl(void) final ;

public:
    void create(std::size_t order, double *cps) const override;
    void create(std::string path) const override;
    void create(const SISLCurve& bezier_curve);
    void create();

public:
    std::size_t degree(void) const override;
    void controlPoint(std::size_t i, double *r_cp) const override;
    void weight(std::size_t i, double *r_w) const override;

public:
    void setWeightedControlPoint(std::size_t i, double *p_cp) override;

public:
    void evaluatePoint(double p_u, double *r_point) const override;
    void evaluateNormal(double p_u, double *r_normal) const override;
    void evaluateDerivative(double p_u, double* r_derivative) const override;
    void evaluateCurvature(double p_u, double* r_curvature) const override;

 public:
    void split(dtkRationalBezierCurve *r_split_curve_a, dtkRationalBezierCurve *r_split_curve_b, double splitting_parameter) const override;
    void split(std::list< dtkRationalBezierCurve * >& r_split_curves, const std::vector< double >& p_splitting_parameters) const override;

 public:
    void aabb(double *r_aabb) const override;
    void extendedAabb(double *r_aabb, double factor) const override;

 public:
    dtkRationalBezierCurveDataSisl *clone(void) const override;

 public:
    void print(std::ostream& stream) const override;

 public:
    const SISLCurve *sislRationalBezierCurve(void) const;

 private :
    SISLCurve *d;
    double *m_ecoef;
    double *m_et;
};


// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

inline dtkAbstractRationalBezierCurveData *dtkRationalBezierCurveDataSislCreator(void)
{
    return new dtkRationalBezierCurveDataSisl();
}

//
// dtkRationalBezierCurveDataSisl.h ends here
