// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <QTest>

class dtkRationalBezierCurveDataSislTestCasePrivate;

class dtkRationalBezierCurveDataSislTestCase : public QObject
{
    Q_OBJECT

public:
    dtkRationalBezierCurveDataSislTestCase(void);
    ~dtkRationalBezierCurveDataSislTestCase(void);

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testControlPoint(void);
    void testWeight(void);
    void testSetWeightedControlPoint(void);
    void testDegree(void);
    void testCurvePoint(void);
    void testCurveNormal(void);
    void testPrintOutCurve(void);
    void testAabb(void);
    void testExtendedAabb(void);
    void testSplit(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);

private:
    dtkRationalBezierCurveDataSislTestCasePrivate* d;
};

//
// dtkRationalBezierCurveDataSislTest.h ends here
