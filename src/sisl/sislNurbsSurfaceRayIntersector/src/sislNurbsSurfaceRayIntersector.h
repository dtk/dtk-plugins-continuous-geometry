// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkNurbsSurfaceRayIntersector>

#include <sislNurbsSurfaceRayIntersectorExport.h>

#include <dtkContinuousGeometryUtils.h>

struct sislNurbsSurfaceRayIntersectorPrivate;

class dtkNurbsSurface;

class SISLNURBSSURFACERAYINTERSECTOR_EXPORT sislNurbsSurfaceRayIntersector : public dtkNurbsSurfaceRayIntersector
{
 public:
    sislNurbsSurfaceRayIntersector(void);
    ~sislNurbsSurfaceRayIntersector(void);

    void setNurbsSurface(dtkNurbsSurface *surface) final;
    void setRay(dtkContinuousGeometryPrimitives::Ray_3 *ray) final;

    void run(void) final;

    const std::vector< dtkContinuousGeometryPrimitives::IntersectionObject > & intersection(void) final;

protected:
    sislNurbsSurfaceRayIntersectorPrivate *d;
};

// ///////////////////////////////////////////////////////////////////

inline dtkNurbsSurfaceRayIntersector* sislNurbsSurfaceRayIntersectorCreator(void)
{
    return new sislNurbsSurfaceRayIntersector();
}

//
// sislNurbsSurfaceRayIntersector.h ends here
