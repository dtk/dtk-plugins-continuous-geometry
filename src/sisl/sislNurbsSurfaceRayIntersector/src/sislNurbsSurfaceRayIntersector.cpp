// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "sislNurbsSurfaceRayIntersector.h"

#include <dtkContinuousGeometryUtils>
#include <dtkNurbsSurface>

#include <dtkNurbsSurfaceDataSisl.h>

#include <sisl.h>
#include <cstring>

struct sislNurbsSurfaceRayIntersectorPrivate
{
    dtkNurbsSurface *nurbs_surface = nullptr;
    dtkContinuousGeometryPrimitives::Ray_3 *ray = nullptr;
    std::vector< dtkContinuousGeometryPrimitives::IntersectionObject> intersection_objects;
};

/*!
  \class sislNurbsSurfaceRayIntersector
  \inmodule dtkPluginsContinuousGeometry
  \brief sislNurbsSurfaceRayIntersector is a SISL implementation of the concept dtkNurbsSurfaceRayIntersector describing the intersection between a ray and a NURBS surface.

  The following lines of code show how to instanciate and use the class in pratice :
  \code
  dtkNurbsSurfaceRayIntersector *intersector = dtkContinuousGeometry::nurbsSurfaceRayIntersector::pluginFactory().create("sislNurbsSurfaceRayIntersector");
  intersector->setNurbsSurface(...);
  intersector->setRay(...);
  intersector->run();
  const auto& intersection_object = intersector->intersection();
  \endcode
*/

sislNurbsSurfaceRayIntersector::sislNurbsSurfaceRayIntersector(void) : dtkNurbsSurfaceRayIntersector(), d(new sislNurbsSurfaceRayIntersectorPrivate) {}

sislNurbsSurfaceRayIntersector::~sislNurbsSurfaceRayIntersector(void){ delete d; }

/*! \fn sislNurbsSurfaceRayIntersector::setNurbsSurface(dtkNurbsSurface *surface)
  Provides the NURBS surface to the intersector (\a surface).

  \a surface : the NURBS surface to intersect
*/
void sislNurbsSurfaceRayIntersector::setNurbsSurface(dtkNurbsSurface *nurbs_surface)
{
    d->nurbs_surface = nurbs_surface;
}

/*! \fn sislNurbsSurfaceRayIntersector::setRay(dtkContinuousGeometryPrimitives::Ray_3 *ray)
  Provides the \a ray to intersect the surface with to the intersector.

  \a ray : the ray to intersect the NURBS surface with
*/
void sislNurbsSurfaceRayIntersector::setRay(dtkContinuousGeometryPrimitives::Ray_3* ray)
{
    d->ray = ray;
}

/*! \fn sislNurbsSurfaceRayIntersector::run(void)
  Intersects the NURBS surface (provided with \l setNurbsSurface(dtkNurbsSurface *surface)) with the ray(provided with \l setRay(Ray_3 *ray)), the result can be retrieved with \l intersection(void).
*/
void sislNurbsSurfaceRayIntersector::run(void)
{
    if(d->nurbs_surface == nullptr) {
        dtkFatal() << "The surface was not set before calling the run() method.";
    }
    if(d->ray == nullptr) {
        dtkFatal() << "The ray was not set before calling the run() method.";
    }

    double geometry_resolution = 1e-15;
    double computational_resolution = 1e-15; //Not used (according to docuentation)
    int numintpt = 0;
    double *pointpar;
    int numintcr = 0;
    SISLIntcurve **intcurves;
    int stat = 0;

    dtkNurbsSurfaceDataSisl *sisl_nurbs_surface = dynamic_cast<dtkNurbsSurfaceDataSisl *>(d->nurbs_surface->data());
    SISLSurf *sisl_surf = nullptr;

    if(sisl_nurbs_surface != nullptr) {
        sisl_surf = const_cast< SISLSurf *>(sisl_nurbs_surface->sislNurbsSurface());
    } else {
        // ///////////////////////////////////////////////////////////////////
        // Convert the dtkNurbsSurface to a SISL_surf
        // ///////////////////////////////////////////////////////////////////
        std::size_t nb_cp_u = d->nurbs_surface->uNbCps();
        std::size_t u_degree = d->nurbs_surface->uDegree();
        std::size_t nb_cp_v = d->nurbs_surface->vNbCps();
        std::size_t v_degree = d->nurbs_surface->vDegree();

        double *et1 = new double[nb_cp_u + u_degree + 1];
        double *et2 = new double[nb_cp_v + v_degree + 1];
        et1[0] = d->nurbs_surface->uKnots()[0];
        et2[0] = d->nurbs_surface->uKnots()[0];
        std::memcpy(&et1[1], d->nurbs_surface->uKnots(), sizeof(double) * (nb_cp_u + u_degree - 1));
        std::memcpy(&et2[1], d->nurbs_surface->vKnots(), sizeof(double) * (nb_cp_v + v_degree - 1));
        et1[nb_cp_u + u_degree] = d->nurbs_surface->uKnots()[nb_cp_u + u_degree - 2];
        et2[nb_cp_v + u_degree] = d->nurbs_surface->vKnots()[nb_cp_v + v_degree - 2];

        double *ecoef = new double[(3 + 1) * nb_cp_u * nb_cp_v];
        dtkContinuousGeometryPrimitives::Point_3 cp(0., 0., 0.);
        double w = 0.;
        for(std::size_t i = 0; i < nb_cp_u; ++i) {
            for(std::size_t j = 0; j < nb_cp_v; ++j) {
                d->nurbs_surface->controlPoint(i, j, cp.data());
                d->nurbs_surface->weight(i, j, &w);
                ecoef[(3 + 1) * nb_cp_u * j + (3 + 1) * i]     = cp[0] * w;
                ecoef[(3 + 1) * nb_cp_u * j + (3 + 1) * i + 1] = cp[1] * w;
                ecoef[(3 + 1) * nb_cp_u * j + (3 + 1) * i + 2] = cp[2] * w;
                ecoef[(3 + 1) * nb_cp_u * j + (3 + 1) * i + 3] = w;
            }
        }

        /* Copy Flag = 2 : Use pointers to et1, et2 and ecoef; freeSurf will free arrays*/
        sisl_surf = newSurf(nb_cp_u, nb_cp_v, u_degree + 1, v_degree + 1, et1, et2, ecoef, 2, 3, 2);
    }

    s1856(sisl_surf, d->ray->source().data(), d->ray->direction().data(), 3, computational_resolution, geometry_resolution, &numintpt, &pointpar, &numintcr, &intcurves, &stat);

    if(stat < 0) { dtkFatal() << "The intersection routine did not go well."; }
    if(numintcr > 0) { dtkFatal() << "Not handling yet intersections other than points. " << Q_FUNC_INFO << ". Line : " << __LINE__; }

    dtkContinuousGeometryPrimitives::Point_3 eval(0., 0., 0.);
    for(auto i = 0; i < numintpt; ++i) {
        d->nurbs_surface->evaluatePoint(pointpar[2*i], pointpar[2*i + 1], eval.data());
        dtkContinuousGeometryPrimitives::PointImage p_img(eval, false);
        dtkContinuousGeometryPrimitives::PointIntersection p_int(p_img);
        dtkContinuousGeometryPrimitives::PointPreImage p_pre_img(dtkContinuousGeometryPrimitives::Point_2(pointpar[i], pointpar[i + 1]), false);
        p_int.pushBackPreImage(p_pre_img);
        d->intersection_objects.emplace_back(p_int);
    }

    if(sisl_nurbs_surface == nullptr) {
        freeSurf(sisl_surf);
    }
}

/*! \fn sislNurbsSurfaceRayIntersector::intersection(void)
  Returns the intersection objects.

  The caller is responsible for deleting all the IntersectionObject of the returned vector, as well as the vector.
*/
const std::vector< dtkContinuousGeometryPrimitives::IntersectionObject>&
sislNurbsSurfaceRayIntersector::intersection(void)
{
    return d->intersection_objects;
}

//
// sislNurbsSurfaceRayIntersector.cpp ends here
