// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#include "sislNurbsSurfaceRayIntersector.h"
#include "sislNurbsSurfaceRayIntersectorPlugin.h"

#include <dtkCore>
#include <dtkContinuousGeometry>

// ///////////////////////////////////////////////////////////////////
// sislNurbsSurfaceRayIntersectorPlugin
// ///////////////////////////////////////////////////////////////////

void sislNurbsSurfaceRayIntersectorPlugin::initialize(void)
{
    dtkContinuousGeometry::nurbsSurfaceRayIntersector::pluginFactory().record("sislNurbsSurfaceRayIntersector", sislNurbsSurfaceRayIntersectorCreator);
}

void sislNurbsSurfaceRayIntersectorPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(sislNurbsSurfaceRayIntersector)

//
// sislNurbsSurfaceRayIntersectorPlugin.cpp ends here
