// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <QtCore>

#include <dtkContinuousGeometry.h>

#include <dtkNurbsSurfaceRayIntersector.h>

class sislNurbsSurfaceRayIntersectorPlugin : public dtkNurbsSurfaceRayIntersectorPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkNurbsSurfaceRayIntersectorPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.sislNurbsSurfaceRayIntersectorPlugin" FILE "sislNurbsSurfaceRayIntersectorPlugin.json")

public:
     sislNurbsSurfaceRayIntersectorPlugin(void) {}
    ~sislNurbsSurfaceRayIntersectorPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

//
// sislNurbsSurfaceRayIntersectorPlugin.h ends here
