// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "sislNurbsSurfaceRayIntersectorTest.h"

#include "sislNurbsSurfaceRayIntersector.h"

#include <dtkContinuousGeometry>
#include <dtkContinuousGeometrySettings>

#include <dtkAbstractNurbsSurfaceData>
#include <dtkNurbsSurface>

#include <dtkContinuousGeometryUtils>

#include <dtkTest>
// ///////////////////////////////////////////////////////////////////
// D-Pointer implementation
// ///////////////////////////////////////////////////////////////////

class sislNurbsSurfaceRayIntersectorTestCasePrivate{
public:
    sislNurbsSurfaceRayIntersector* intersector;
};
// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

sislNurbsSurfaceRayIntersectorTestCase::sislNurbsSurfaceRayIntersectorTestCase(void):d(new sislNurbsSurfaceRayIntersectorTestCasePrivate())
{
    dtkLogger::instance().attachConsole();
    dtkLogger::instance().setLevel(dtk::LogLevel::Trace);

    dtkContinuousGeometrySettings settings;
    settings.beginGroup("continuous-geometry");
    dtkContinuousGeometry::initialize(settings.value("plugins").toString());

    settings.endGroup();
    d->intersector = nullptr;
}

sislNurbsSurfaceRayIntersectorTestCase::~sislNurbsSurfaceRayIntersectorTestCase(void)
{
    delete d;
}

void sislNurbsSurfaceRayIntersectorTestCase::initTestCase(void)
{
    d->intersector = new sislNurbsSurfaceRayIntersector();
}

void sislNurbsSurfaceRayIntersectorTestCase::init(void)
{
}

void sislNurbsSurfaceRayIntersectorTestCase::testRun(void)
{
    dtkAbstractNurbsSurfaceData *surface_data_1_2 = dtkContinuousGeometry::abstractNurbsSurfaceData::pluginFactory().create("dtkNurbsSurfaceDataOn");
    if(surface_data_1_2 == nullptr) {
        dtkWarn() << "This test requires that the openNURBS implementation of dtkNurbsSurfaceData is available";
        return;
    } else {
        std::size_t dim = 3;

        // std::size_t nb_cp_u = 4;
        // std::size_t nb_cp_v = 4;
        // std::size_t order_u = 3;
        // std::size_t order_v = 3;

        // double *knots_u = new double[nb_cp_u + order_u - 2];
        // /* 0. */ knots_u[0] = 0.; knots_u[1] = 0.; knots_u[2] = 1.; knots_u[3] = 2.; knots_u[4] = 2.; /* 2. */
        // double *knots_v = new double[nb_cp_v + order_v - 2];
        // /* 0. */ knots_v[0] = 0.; knots_v[1] = 0.; knots_v[2] = 1.; knots_v[3] = 2.; knots_v[4] = 2.; /* 2. */

        // double *cps = new double[(dim + 1) * nb_cp_u * nb_cp_v];
        // cps[0] = 0.;   cps[1] = 0.; cps[2] = 0.;  cps[3] = 1.;
        // cps[4] = 1.;   cps[5] = 0.; cps[6] = 1.;  cps[7] = 1.;
        // cps[8] = 2.;   cps[9] = 0.; cps[10] = 1.; cps[11] = 1.;
        // cps[12] = 3.;  cps[13] = 0.;  cps[14] = 0.; cps[15] = 1.;
        // cps[16] = 0.;  cps[17] = 1.; cps[18] = 1.;  cps[19] = 1.;
        // cps[20] = 1.;  cps[21] = 1.; cps[22] = 1.;  cps[23] = 1.;
        // cps[24] = 2.;  cps[25] = 1.; cps[26] = 1.; cps[27] = 1.;
        // cps[28] = 3.;  cps[29] = 1.;  cps[30] = 1.; cps[31] = 1.;
        // cps[32] = 0.;  cps[33] = 2.; cps[34] = 1.;  cps[35] = 1.;
        // cps[36] = 1.;  cps[37] = 2.; cps[38] = 1.;  cps[39] = 1.;
        // cps[40] = 2.;  cps[41] = 2.; cps[42] = 1.; cps[43] = 1.;
        // cps[44] = 3.;  cps[45] = 2.;  cps[46] = 1.; cps[47] = 1.;
        // cps[48] = 0.;  cps[49] = 3.; cps[50] = 0.;  cps[51] = 1.;
        // cps[52] = 1.;  cps[53] = 3.; cps[54] = 1.;  cps[55] = 1.;
        // cps[56] = 2.;  cps[57] = 3.; cps[58] = 1.; cps[59] = 1.;
        // cps[60] = 3.;  cps[61] = 3.;  cps[62] = 0.; cps[63] = 1.;

        std::size_t nb_cp_u = 2;
        std::size_t nb_cp_v = 2;
        std::size_t order_u = 2;
        std::size_t order_v = 2;
        //Not working ! To check why.
        // double *knots_u = new double[nb_cp_u + order_u - 2];
        // /* 0. */ knots_u[0] = 0.; knots_u[1] = .5; knots_u[2] = 1.; /* 1. */
        // double *knots_v = new double[nb_cp_v + order_v - 2];
        // /* 0. */ knots_v[0] = 0.; knots_v[1] = .5; knots_v[2] = 1.; /* 1. */

        double *knots_u = new double[nb_cp_u + order_u - 2];
        /* 0. */ knots_u[0] = 0.; knots_u[1] = 1.; /* 1. */
        double *knots_v = new double[nb_cp_v + order_v - 2];
        /* 0. */ knots_v[0] = 0.; knots_v[1] = 1.; /* 1. */

        double *cps = new double[(dim + 1) * nb_cp_u * nb_cp_v];
        cps[0] = 0.;   cps[1] = 0.; cps[2] = 0.;  cps[3] = 1.;
        cps[4] = 2.;   cps[5] = 0.; cps[6] = 0.;  cps[7] = 1.;
        cps[8] = 0.;   cps[9] = 2.; cps[10] = 0.; cps[11] = 1.;
        cps[12] = 2.;  cps[13] = 2.;  cps[14] = 0.; cps[15] = 1.;

        dtkNurbsSurface *nurbs_surface_1_2 = new dtkNurbsSurface(surface_data_1_2);
        std::cerr << "Before create" << std::endl;
        nurbs_surface_1_2->create(dim, nb_cp_u, nb_cp_v, order_u, order_v, knots_u, knots_v, cps);
        std::cerr << "After  create" << std::endl;

        dtkContinuousGeometryPrimitives::Ray_3 ray(dtkContinuousGeometryPrimitives::Point_3(.5, .5, -10.), dtkContinuousGeometryPrimitives::Vector_3(0., 0., 1.));

        d->intersector->setNurbsSurface(nurbs_surface_1_2);
        d->intersector->setRay(&ray);
        d->intersector->run();

        const auto& result = d->intersector->intersection();
        for(const auto& intersection : result) {
            if(intersection.isPoint()) {
                dtkInfo() << intersection.pointIntersection().image();
            } else {
            }
        }
        delete[] knots_u;
        delete[] knots_v;
        delete[] cps;
    }
}

void sislNurbsSurfaceRayIntersectorTestCase::cleanup(void)
{

}

void sislNurbsSurfaceRayIntersectorTestCase::cleanupTestCase(void)
{
    if(d->intersector != nullptr) {
        delete d->intersector;
    }
}

DTKTEST_MAIN_NOGUI(sislNurbsSurfaceRayIntersectorTest, sislNurbsSurfaceRayIntersectorTestCase)

//
// sislNurbsSurfaceRayIntersectorTest.cpp ends here
