// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <QTest>

class sislNurbsSurfaceRayIntersectorTestCasePrivate;

class sislNurbsSurfaceRayIntersectorTestCase : public QObject
{
    Q_OBJECT

public:
    sislNurbsSurfaceRayIntersectorTestCase(void);
    ~sislNurbsSurfaceRayIntersectorTestCase(void);

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testRun(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);

private:
    sislNurbsSurfaceRayIntersectorTestCasePrivate* d;
};

//
// sislNurbsSurfaceRayIntersectorTest.h ends here
