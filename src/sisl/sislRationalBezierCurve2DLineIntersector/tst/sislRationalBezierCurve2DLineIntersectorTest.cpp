// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "sislRationalBezierCurve2DLineIntersectorTest.h"

#include "sislRationalBezierCurve2DLineIntersector.h"

#include <dtkContinuousGeometry>
#include <dtkContinuousGeometrySettings>

#include <dtkAbstractRationalBezierCurve2DData>
#include <dtkRationalBezierCurve2D>

#include <dtkContinuousGeometryUtils>

#include <dtkTest>
// ///////////////////////////////////////////////////////////////////
// D-Pointer implementation
// ///////////////////////////////////////////////////////////////////

class sislRationalBezierCurve2DLineIntersectorTestCasePrivate{
public:
    sislRationalBezierCurve2DLineIntersector* intersector;
};
// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

sislRationalBezierCurve2DLineIntersectorTestCase::sislRationalBezierCurve2DLineIntersectorTestCase(void):d(new sislRationalBezierCurve2DLineIntersectorTestCasePrivate())
{
    dtkLogger::instance().attachConsole();

    dtkContinuousGeometrySettings settings;
    settings.beginGroup("continuous-geometry");
    dtkContinuousGeometry::initialize(settings.value("plugins").toString());

    settings.endGroup();
    d->intersector = nullptr;
}

sislRationalBezierCurve2DLineIntersectorTestCase::~sislRationalBezierCurve2DLineIntersectorTestCase(void)
{
    delete d;
}

void sislRationalBezierCurve2DLineIntersectorTestCase::initTestCase(void)
{
    d->intersector = new sislRationalBezierCurve2DLineIntersector();
}

void sislRationalBezierCurve2DLineIntersectorTestCase::init(void)
{
}

void sislRationalBezierCurve2DLineIntersectorTestCase::testRun(void)
{
    dtkAbstractRationalBezierCurve2DData *curve_data = dtkContinuousGeometry::abstractRationalBezierCurve2DData::pluginFactory().create("dtkRationalBezierCurve2DDataOn");
    if(curve_data == nullptr) {
        dtkWarn() << "This test requires that the openNURBS implementation of dtkRationalBezierCurveData is available";
        return;
    } else {
        dtkRationalBezierCurve2D *curve = new dtkRationalBezierCurve2D(curve_data);
        std::size_t order = 3;
        double* cps = new double[(2 + 1) * order];
        cps[0] = 0.;  cps[1] = 0.;  cps[2] = 1.;
        cps[3] = 1.;  cps[4] = 10.;  cps[5] = 1.;
        cps[6] = 2.;  cps[7] = 0.;  cps[8] = 1.;
        curve_data->create(order, cps);

        dtkContinuousGeometryPrimitives::Line_2 line(dtkContinuousGeometryPrimitives::Point_2(1., 0.), dtkContinuousGeometryPrimitives::Vector_2(0., 1.));

        d->intersector->setRationalBezierCurve2D(curve);
        d->intersector->setLine(line);
        d->intersector->run();

        std::vector< double > intersections_parameters = d->intersector->intersectionsParameters();

        QVERIFY(intersections_parameters.size() == 1);
        QVERIFY(intersections_parameters[0] == 0.5);
        delete curve;
    }
}

void sislRationalBezierCurve2DLineIntersectorTestCase::cleanup(void)
{

}

void sislRationalBezierCurve2DLineIntersectorTestCase::cleanupTestCase(void)
{
    if(d->intersector != nullptr) {
        delete d->intersector;
    }
}

DTKTEST_MAIN_NOGUI(sislRationalBezierCurve2DLineIntersectorTest, sislRationalBezierCurve2DLineIntersectorTestCase)

//
// sislRationalBezierCurve2DLineIntersectorTest.cpp ends here
