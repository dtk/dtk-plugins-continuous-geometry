// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <QTest>

class sislRationalBezierCurve2DLineIntersectorTestCasePrivate;

class sislRationalBezierCurve2DLineIntersectorTestCase : public QObject
{
    Q_OBJECT

public:
    sislRationalBezierCurve2DLineIntersectorTestCase(void);
    ~sislRationalBezierCurve2DLineIntersectorTestCase(void);

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testRun(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);

private:
    sislRationalBezierCurve2DLineIntersectorTestCasePrivate* d;
};

//
// sislRationalBezierCurve2DLineIntersectorTest.h ends here
