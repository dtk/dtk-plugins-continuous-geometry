// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "sislRationalBezierCurve2DLineIntersector.h"

#include <dtkRationalBezierCurve2D>
#include <dtkContinuousGeometryUtils>

#include <sisl.h>

struct sislRationalBezierCurve2DLineIntersectorPrivate
{
    dtkRationalBezierCurve2D *bezier_curve = nullptr;
    dtkContinuousGeometryPrimitives::Line_2 line{ {0., 0.}, {0., 0.} };

    std::vector< double > intersection_parameters;
};

sislRationalBezierCurve2DLineIntersector::sislRationalBezierCurve2DLineIntersector(void) : dtkRationalBezierCurve2DLineIntersector(), d(new sislRationalBezierCurve2DLineIntersectorPrivate) {}

sislRationalBezierCurve2DLineIntersector::~sislRationalBezierCurve2DLineIntersector(void){ delete d;}

void sislRationalBezierCurve2DLineIntersector::setRationalBezierCurve2D(dtkRationalBezierCurve2D *bezier_curve)
{
    d->bezier_curve = bezier_curve;
}

void sislRationalBezierCurve2DLineIntersector::setLine(const dtkContinuousGeometryPrimitives::Line_2& line)
{
    d->line = line;
}

void sislRationalBezierCurve2DLineIntersector::run(void)
{
    d->intersection_parameters.clear();
    // ///////////////////////////////////////////////////////////////////
    // Convert the dtkRationalBezierCurve2D to a SISL_curve
    // ///////////////////////////////////////////////////////////////////
    double *knots = (double*)malloc(2 * (d->bezier_curve->degree() + 1) *sizeof(double));
    for (std::size_t i = 0; i < d->bezier_curve->degree() + 1; ++i) {
        knots[i] = 0.;
    }
    for (std::size_t i = d->bezier_curve->degree() + 1; i < 2 * (d->bezier_curve->degree() + 1); ++i) {
        knots[i] = 1.;
    }

    double *coef = (double*)malloc(3 * (d->bezier_curve->degree() + 1) * sizeof(double));
    dtkContinuousGeometryPrimitives::Point_2 p(0., 0.);
    double w = 0.;
    for (std::size_t i = 0; i < d->bezier_curve->degree() + 1; ++i) {
        d->bezier_curve->controlPoint(i, p.data());
        d->bezier_curve->weight(i, &w);
        coef[3 * i] = p[0];
        coef[3 * i + 1] = p[1];
        coef[3 * i + 2] = w;
    }
    SISLCurve *sisl_curve = newCurve(d->bezier_curve->degree() + 1, d->bezier_curve->degree() + 1, knots, coef, 4, 2, 2);

    double *point = new double[2];
    double *normal = new double[2];

    point[0] = d->line.point()[0];
    point[1] = d->line.point()[1];
    normal[0] = -d->line.direction()[1];
    normal[1] = d->line.direction()[0];

    int dim = 2;
    double epsco = 0.;
    double epsge = 1e-10;
    int numintpt = 0;
    double *intpar = nullptr;
    int numintcu = 0;
    SISLIntcurve **intcurve = nullptr;
    int stat;
    s1850(sisl_curve, point, normal, dim, epsco, epsge, &numintpt, &intpar, &numintcu, &intcurve, &stat);
    if(stat < 0) {
      dtkError() << "SISL error code" << stat;
    }
    for(std::size_t i = 0; i < std::size_t(numintpt); ++i) {
        d->intersection_parameters.push_back(intpar[i]);
    }
    freeIntcrvlist(intcurve, numintcu);
    std::free(intpar);
    delete[] point;
    delete[] normal;
    freeCurve(sisl_curve);
}

std::vector< double > sislRationalBezierCurve2DLineIntersector::intersectionsParameters(void)
{
    return d->intersection_parameters;
}

//
// sislRationalBezierCurve2DLineIntersector.cpp ends here
