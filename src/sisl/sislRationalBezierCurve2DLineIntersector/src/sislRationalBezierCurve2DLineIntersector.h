// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkRationalBezierCurve2DLineIntersector>

#include <sislRationalBezierCurve2DLineIntersectorExport.h>

struct sislRationalBezierCurve2DLineIntersectorPrivate;

class dtkRationalBezierCurve2D;
namespace dtkContinuousGeometryPrimitives {
    class Line_2;
}

class SISLRATIONALBEZIERCURVE2DLINEINTERSECTOR_EXPORT sislRationalBezierCurve2DLineIntersector : public dtkRationalBezierCurve2DLineIntersector
{
public:
     sislRationalBezierCurve2DLineIntersector(void);
     virtual ~sislRationalBezierCurve2DLineIntersector(void) final;

    void setRationalBezierCurve2D(dtkRationalBezierCurve2D *bezier_curve) final;
    void setLine(const dtkContinuousGeometryPrimitives::Line_2& line) final;

    void run(void) final;

    std::vector< double > intersectionsParameters(void) final;

protected:
    sislRationalBezierCurve2DLineIntersectorPrivate *d;
};

// ///////////////////////////////////////////////////////////////////

inline dtkRationalBezierCurve2DLineIntersector* sislRationalBezierCurve2DLineIntersectorCreator(void)
{
    return new sislRationalBezierCurve2DLineIntersector();
}

//
// sislRationalBezierCurve2DLineIntersector.h ends here
