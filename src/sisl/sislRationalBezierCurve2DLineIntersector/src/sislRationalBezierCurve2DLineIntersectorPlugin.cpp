// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#include "sislRationalBezierCurve2DLineIntersector.h"
#include "sislRationalBezierCurve2DLineIntersectorPlugin.h"

#include <dtkCore>
#include <dtkContinuousGeometry>

// ///////////////////////////////////////////////////////////////////
// sislRationalBezierCurve2DLineIntersectorPlugin
// ///////////////////////////////////////////////////////////////////

void sislRationalBezierCurve2DLineIntersectorPlugin::initialize(void)
{
    dtkContinuousGeometry::rationalBezierCurve2DLineIntersector::pluginFactory().record("sislRationalBezierCurve2DLineIntersector", sislRationalBezierCurve2DLineIntersectorCreator);
}

void sislRationalBezierCurve2DLineIntersectorPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(sislRationalBezierCurve2DLineIntersector)

//
// sislRationalBezierCurve2DLineIntersectorPlugin.cpp ends here
