// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <QtCore>

#include <dtkContinuousGeometry.h>

#include <dtkRationalBezierCurve2DLineIntersector.h>

class sislRationalBezierCurve2DLineIntersectorPlugin : public dtkRationalBezierCurve2DLineIntersectorPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkRationalBezierCurve2DLineIntersectorPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.sislRationalBezierCurve2DLineIntersectorPlugin" FILE "sislRationalBezierCurve2DLineIntersectorPlugin.json")

public:
     sislRationalBezierCurve2DLineIntersectorPlugin(void) {}
    ~sislRationalBezierCurve2DLineIntersectorPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

//
// sislRationalBezierCurve2DLineIntersectorPlugin.h ends here
