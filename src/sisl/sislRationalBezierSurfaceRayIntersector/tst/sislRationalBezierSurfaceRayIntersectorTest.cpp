// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "sislRationalBezierSurfaceRayIntersectorTest.h"

#include "sislRationalBezierSurfaceRayIntersector.h"

#include <dtkContinuousGeometry>
#include <dtkContinuousGeometrySettings>

#include <dtkAbstractRationalBezierSurfaceData>
#include <dtkRationalBezierSurface>

#include <dtkContinuousGeometryUtils>

#include <dtkTest>
// ///////////////////////////////////////////////////////////////////
// D-Pointer implementation
// ///////////////////////////////////////////////////////////////////

class sislRationalBezierSurfaceRayIntersectorTestCasePrivate{
public:
    sislRationalBezierSurfaceRayIntersector* intersector;
};
// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

sislRationalBezierSurfaceRayIntersectorTestCase::sislRationalBezierSurfaceRayIntersectorTestCase(void):d(new sislRationalBezierSurfaceRayIntersectorTestCasePrivate())
{
    dtkLogger::instance().attachConsole();
    dtkLogger::instance().setLevel(dtk::LogLevel::Trace);

    dtkContinuousGeometrySettings settings;
    settings.beginGroup("continuous-geometry");
    dtkContinuousGeometry::initialize(settings.value("plugins").toString());

    settings.endGroup();
    d->intersector = nullptr;
}

sislRationalBezierSurfaceRayIntersectorTestCase::~sislRationalBezierSurfaceRayIntersectorTestCase(void)
{
    delete d;
}

void sislRationalBezierSurfaceRayIntersectorTestCase::initTestCase(void)
{
    d->intersector = new sislRationalBezierSurfaceRayIntersector();
}

void sislRationalBezierSurfaceRayIntersectorTestCase::init(void)
{
}

void sislRationalBezierSurfaceRayIntersectorTestCase::testRun(void)
{
    dtkAbstractRationalBezierSurfaceData *surface_data_1_1 = dtkContinuousGeometry::abstractRationalBezierSurfaceData::pluginFactory().create("dtkRationalBezierSurfaceDataOn");
    if(surface_data_1_1 == nullptr) {
        dtkWarn() << "This test requires that the openNURBS implementation of dtkRationalBezierSurfaceData is available";
        return;
    } else {
        std::size_t dim = 3;
        std::size_t order_u = 2;
        std::size_t order_v = 2;

        std::vector< double > cps((dim + 1) * order_u * order_v);
        cps[0] = 0.;   cps[1] = 0.; cps[2] = 0.;  cps[3] = 1.;
        cps[4] = 2.;   cps[5] = 0.; cps[6] = 0.;  cps[7] = 1.;
        cps[8] = 0.;   cps[9] = 2.; cps[10] = 0.; cps[11] = 1.;
        cps[12] = 2.;  cps[13] = 2.;  cps[14] = 0.; cps[15] = 1.;

        dtkRationalBezierSurface *rb_surface_1_1 = new dtkRationalBezierSurface(surface_data_1_1);

        rb_surface_1_1->create(dim, order_u, order_v, cps.data());

        dtkContinuousGeometryPrimitives::Ray_3 ray_0(dtkContinuousGeometryPrimitives::Point_3(.5, .5, -10.), dtkContinuousGeometryPrimitives::Vector_3(0., 0., 1.));

        d->intersector->setRationalBezierSurface(rb_surface_1_1);
        d->intersector->setRay(&ray_0);
        d->intersector->run();

        auto result = d->intersector->intersection();
        QVERIFY(result.size() == 1);
        QVERIFY(result.front().isPoint());

        QVERIFY(result.front().pointIntersection().image()[0] == 0.5);
        QVERIFY(result.front().pointIntersection().image()[1] == 0.5);
        QVERIFY(result.front().pointIntersection().image()[2] == 0.);

        QVERIFY(result.front().pointIntersection().preImages().front()[0] == 0.25);
        QVERIFY(result.front().pointIntersection().preImages().front()[1] == 0.25);

        dtkContinuousGeometryPrimitives::Ray_3 ray_1(dtkContinuousGeometryPrimitives::Point_3(.5, .5, 10.), dtkContinuousGeometryPrimitives::Vector_3(0., 0., 1.));
        d->intersector->reset();
        d->intersector->setRay(&ray_1);
        d->intersector->run();

        result = d->intersector->intersection();
        QVERIFY(result.size() == 0);
    }
}

void sislRationalBezierSurfaceRayIntersectorTestCase::cleanup(void)
{

}

void sislRationalBezierSurfaceRayIntersectorTestCase::cleanupTestCase(void)
{
    if(d->intersector != nullptr) {
        delete d->intersector;
    }
}

DTKTEST_MAIN_NOGUI(sislRationalBezierSurfaceRayIntersectorTest, sislRationalBezierSurfaceRayIntersectorTestCase)

//
// sislRationalBezierSurfaceRayIntersectorTest.cpp ends here
