// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <QTest>

class sislRationalBezierSurfaceRayIntersectorTestCasePrivate;

class sislRationalBezierSurfaceRayIntersectorTestCase : public QObject
{
    Q_OBJECT

public:
    sislRationalBezierSurfaceRayIntersectorTestCase(void);
    ~sislRationalBezierSurfaceRayIntersectorTestCase(void);

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testRun(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);

private:
    sislRationalBezierSurfaceRayIntersectorTestCasePrivate* d;
};

//
// sislRationalBezierSurfaceRayIntersectorTest.h ends here
