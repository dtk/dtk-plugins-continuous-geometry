// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#include "sislRationalBezierSurfaceRayIntersector.h"
#include "sislRationalBezierSurfaceRayIntersectorPlugin.h"

#include <dtkCore>
#include <dtkContinuousGeometry>

// ///////////////////////////////////////////////////////////////////
// sislRationalBezierSurfaceRayIntersectorPlugin
// ///////////////////////////////////////////////////////////////////

void sislRationalBezierSurfaceRayIntersectorPlugin::initialize(void)
{
    dtkContinuousGeometry::rationalBezierSurfaceRayIntersector::pluginFactory().record("sislRationalBezierSurfaceRayIntersector", sislRationalBezierSurfaceRayIntersectorCreator);
}

void sislRationalBezierSurfaceRayIntersectorPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(sislRationalBezierSurfaceRayIntersector)

//
// sislRationalBezierSurfaceRayIntersectorPlugin.cpp ends here
