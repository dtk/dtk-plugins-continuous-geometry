// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <QtCore>

#include <dtkContinuousGeometry.h>

#include <dtkRationalBezierSurfaceRayIntersector.h>

class sislRationalBezierSurfaceRayIntersectorPlugin : public dtkRationalBezierSurfaceRayIntersectorPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkRationalBezierSurfaceRayIntersectorPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.sislRationalBezierSurfaceRayIntersectorPlugin" FILE "sislRationalBezierSurfaceRayIntersectorPlugin.json")

public:
     sislRationalBezierSurfaceRayIntersectorPlugin(void) {}
    ~sislRationalBezierSurfaceRayIntersectorPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

//
// sislRationalBezierSurfaceRayIntersectorPlugin.h ends here
