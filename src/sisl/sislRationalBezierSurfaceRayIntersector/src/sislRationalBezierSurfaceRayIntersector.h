// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkRationalBezierSurfaceRayIntersector>

#include <sislRationalBezierSurfaceRayIntersectorExport.h>

#include <dtkContinuousGeometryUtils.h>

struct sislRationalBezierSurfaceRayIntersectorPrivate;

class dtkRationalBezierSurface;

class SISLRATIONALBEZIERSURFACERAYINTERSECTOR_EXPORT sislRationalBezierSurfaceRayIntersector : public dtkRationalBezierSurfaceRayIntersector
{
 public:
    sislRationalBezierSurfaceRayIntersector(void);
    ~sislRationalBezierSurfaceRayIntersector(void);

    void setRationalBezierSurface(dtkRationalBezierSurface *surface) final;
    void setRay(dtkContinuousGeometryPrimitives::Ray_3 *ray) final;

    bool run(void) final;
    void reset() final;
    const std::vector< dtkContinuousGeometryPrimitives::IntersectionObject> & intersection(void) final;

protected:
    sislRationalBezierSurfaceRayIntersectorPrivate *d;
};

// ///////////////////////////////////////////////////////////////////

inline dtkRationalBezierSurfaceRayIntersector* sislRationalBezierSurfaceRayIntersectorCreator(void)
{
    return new sislRationalBezierSurfaceRayIntersector();
}

//
// sislRationalBezierSurfaceRayIntersector.h ends here
