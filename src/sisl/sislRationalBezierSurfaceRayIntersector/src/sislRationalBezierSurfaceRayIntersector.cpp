// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "sislRationalBezierSurfaceRayIntersector.h"

#include <dtkContinuousGeometryUtils>
#include <dtkRationalBezierSurface>
#include <dtkRationalBezierSurfaceDataSisl.h>

#include <sisl.h>

struct sislRationalBezierSurfaceRayIntersectorPrivate
{
    dtkRationalBezierSurface *rb_surface = nullptr;
    dtkContinuousGeometryPrimitives::Ray_3 *ray = nullptr;
    std::vector< dtkContinuousGeometryPrimitives::IntersectionObject > intersection_objects;

    void reset() { intersection_objects.clear(); }
};

/*!
  \class sislRationalBezierSurfaceRayIntersector
  \inmodule dtkPluginsContinuousGeometry
  \brief sislRationalBezierSurfaceRayIntersector is a SISL implementation of the concept dtkRationalBezierSurfaceRayIntersector describing the intersection between a ray and a rational Bezier surface.

  The following lines of code show how to instanciate and use the class in pratice :
  \code
  dtkRationalBezierSurfaceRayIntersector *intersector = dtkContinuousGeometry::nurbsSurfaceRayIntersector::pluginFactory().create("sislRationalBezierSurfaceRayIntersector");
  intersector->setRationalBezierSurface(...);
  intersector->setRay(...);
  intersector->run();
  const std::vector< IntersectionObject > & intersection_object = intersector->intersection();
  \endcode
*/

sislRationalBezierSurfaceRayIntersector::sislRationalBezierSurfaceRayIntersector(void) : dtkRationalBezierSurfaceRayIntersector(), d(new sislRationalBezierSurfaceRayIntersectorPrivate) {}

sislRationalBezierSurfaceRayIntersector::~sislRationalBezierSurfaceRayIntersector(void){}

/*! \fn sislRationalBezierSurfaceRayIntersector::setRationalBezierSurface(dtkRationalBezierSurface *surface)
  Provides the rational Bezier surface to the intersector (\a surface).

  \a surface : the rational Bezier surface to intersect
*/
void sislRationalBezierSurfaceRayIntersector::setRationalBezierSurface(dtkRationalBezierSurface *rb_surface)
{
    d->rb_surface = rb_surface;
}

/*! \fn sislRationalBezierSurfaceRayIntersector::setRay(dtkContinuousGeometryPrimitives::Ray_3 *ray)
  Provides the \a ray to intersect the surface with to the intersector.

  \a ray : the ray to intersect the rational Bezier surface with
*/
void sislRationalBezierSurfaceRayIntersector::setRay(dtkContinuousGeometryPrimitives::Ray_3* ray)
{
    d->ray = ray;
}

void sislRationalBezierSurfaceRayIntersector::reset() {
    d->intersection_objects.clear();
}
/*! \fn sislRationalBezierSurfaceRayIntersector::run(void)
  Intersects the rational Bezier surface (provided with \l setRationalBezierSurface(dtkRationalBezierSurface *surface)) with the ray(provided with \l setRay(Ray_3 *ray)), the result can be retrieved with \l intersection(void).
*/
bool sislRationalBezierSurfaceRayIntersector::run(void)
{
    if(d->rb_surface == nullptr) {
        dtkFatal() << "The surface was not set before calling the run() method.";
    }
    if(d->ray == nullptr) {
        dtkFatal() << "The ray was not set before calling the run() method.";
    }

    double tolerance_3d = 1e-10;
    double geometry_resolution = 1e-10;
    double computational_resolution = 1e-15; //Not used (according to documentation)
    int numintpt = 0;
    double *pointpar;
    int numintcr = 0;
    SISLIntcurve **intcurves;
    int stat = 0;

    dtkRationalBezierSurfaceDataSisl *sisl_rb_surface = dynamic_cast<dtkRationalBezierSurfaceDataSisl *>(d->rb_surface->data());
    SISLSurf *sisl_surf = nullptr;

    if(sisl_rb_surface != nullptr) {
        sisl_surf = const_cast< SISLSurf *>(sisl_rb_surface->sislRationalBezierSurface());
    } else {
        // ///////////////////////////////////////////////////////////////////
        // Convert the dtkRationalBezierSurface to a SISLSurf
        // ///////////////////////////////////////////////////////////////////
        std::size_t u_degree = d->rb_surface->uDegree();
        std::size_t v_degree = d->rb_surface->vDegree();
        std::size_t nb_cp_u = u_degree + 1;
        std::size_t nb_cp_v = v_degree + 1;

        double *et1 = new double[2 * nb_cp_u];
        for(std::size_t i = 0; i < nb_cp_u; ++i) { et1[i] = 0; }
        for(std::size_t i = nb_cp_u; i < 2 * nb_cp_u; ++i) { et1[i] = 1; }
        double *et2 = new double[2 * nb_cp_v];
        for(std::size_t i = 0; i < nb_cp_v; ++i) { et2[i] = 0; }
        for(std::size_t i = nb_cp_v; i < 2 * nb_cp_v; ++i) { et2[i] = 1; }

        double *ecoef = new double[(3 + 1) * nb_cp_u * nb_cp_v];
        dtkContinuousGeometryPrimitives::Point_3 cp(0., 0., 0.);
        double w = 0.;
        for(std::size_t i = 0; i < nb_cp_u; ++i) {
            for(std::size_t j = 0; j < nb_cp_v; ++j) {
                d->rb_surface->controlPoint(i, j, cp.data());
                d->rb_surface->weight(i, j, &w);
                ecoef[(3 + 1) * nb_cp_u * j + (3 + 1) * i]     = cp[0] * w;
                ecoef[(3 + 1) * nb_cp_u * j + (3 + 1) * i + 1] = cp[1] * w;
                ecoef[(3 + 1) * nb_cp_u * j + (3 + 1) * i + 2] = cp[2] * w;
                ecoef[(3 + 1) * nb_cp_u * j + (3 + 1) * i + 3] = w;
            }
        }

        /* Copy Flag = 2 : Use pointers to et1, et2 and ecoef; freeSurf will free arrays*/
        sisl_surf = newSurf(nb_cp_u, nb_cp_v, nb_cp_u, nb_cp_v, et1, et2, ecoef, 4, 3, 2);
    }

    s1856(sisl_surf, d->ray->source().data(), d->ray->direction().data(), 3, computational_resolution, geometry_resolution, &numintpt, &pointpar, &numintcr, &intcurves, &stat);

    if(stat < 0) {
        dtkError() << "The intersection routine did not go well.";
        return false;
    }
    if(numintcr > 0) {
        dtkError() << "Not handling yet intersections other than points. " << Q_FUNC_INFO << ". Line : " << __LINE__;
    }

    dtkContinuousGeometryPrimitives::Point_3 eval(0., 0., 0.);
    for(auto i = 0; i < numintpt; ++i) {
        d->rb_surface->evaluatePoint(pointpar[2*i], pointpar[2*i + 1], eval.data());
        // ///////////////////////////////////////////////////////////////////
        // Computes the angle between P-P0 and dir
        // ///////////////////////////////////////////////////////////////////
        dtkContinuousGeometryPrimitives::Vector_3 sol = eval - d->ray->source();
        if(dtkContinuousGeometryTools::norm(sol) > tolerance_3d) {
            double angle = std::acos(std::min(std::max(dtkContinuousGeometryTools::dotProduct(sol, d->ray->direction()) / (dtkContinuousGeometryTools::norm(sol) * dtkContinuousGeometryTools::norm(d->ray->direction())), -1.), 1.));
            if(angle < M_PI / 2){
                dtkContinuousGeometryPrimitives::PointImage p_img(eval, true);
                dtkContinuousGeometryPrimitives::PointIntersection p_int(p_img);
                dtkContinuousGeometryPrimitives::PointPreImage p_pre_img(dtkContinuousGeometryPrimitives::Point_2(pointpar[2*i], pointpar[2*i + 1]), true);
                p_int.pushBackPreImage(p_pre_img);
                d->intersection_objects.emplace_back(p_int);
            }
        } else {
            dtkContinuousGeometryPrimitives::PointImage p_img(eval, false);
            dtkContinuousGeometryPrimitives::PointIntersection p_int(p_img);
            dtkContinuousGeometryPrimitives::PointPreImage p_pre_img(dtkContinuousGeometryPrimitives::Point_2(pointpar[2*i], pointpar[2*i + 1]), true);
            p_int.pushBackPreImage(p_pre_img);
            d->intersection_objects.emplace_back(p_int);
        }
    }

    if(sisl_rb_surface == nullptr) {
        freeSurf(sisl_surf);
    }
    return true;
}

/*! \fn sislRationalBezierSurfaceRayIntersector::intersection(void)
  Returns the intersection objects.

  The caller is responsible for deleting all the IntersectionObject of the returned vector, as well as the vector.
*/
const std::vector< dtkContinuousGeometryPrimitives::IntersectionObject>&
sislRationalBezierSurfaceRayIntersector::intersection(void)
{
    return d->intersection_objects;
}

//
// sislRationalBezierSurfaceRayIntersector.cpp ends here
