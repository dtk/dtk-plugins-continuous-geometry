/*
 * dtkRationalBezierCurve2DSegmentIntersectorPlugin.cpp
 *
 *  Created on: Dec 19, 2019
 *      Author: xxiao
 */

#include "sislRationalBezierCurve2DSegmentIntersector.h"
#include "sislRationalBezierCurve2DSegmentIntersectorPlugin.h"

#include <dtkCore>

#include <dtkContinuousGeometry>

void sislRationalBezierCurve2DSegmentIntersectorPlugin::initialize(void)
{
    dtkContinuousGeometry::rationalBezierCurve2DSegmentIntersector::pluginFactory().record("sislRationalBezierCurve2DSegmentIntersector", sislRationalBezierCurve2DSegmentIntersectorCreator);
}

void sislRationalBezierCurve2DSegmentIntersectorPlugin::uninitialize(void)
{

}

DTK_DEFINE_PLUGIN(sislRationalBezierCurve2DSegmentIntersector)
