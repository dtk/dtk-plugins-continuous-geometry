/*
 * sislRationalBezierCurve2DSegmentIntersector.cpp
 *
 *  Created on: Dec 19, 2019
 *      Author: xxiao
 */

#include "sislRationalBezierCurve2DSegmentIntersector.h"

#include <dtkRationalBezierCurve2D>
#include <dtkContinuousGeometryUtils>

#include <sisl.h>

#include <vector>

struct sislRationalBezierCurve2DSegmentIntersectorPrivate
{
  void reset() { intersection_objects.clear(); }

  dtkRationalBezierCurve2D* bezier_curve = nullptr;
  dtkContinuousGeometryPrimitives::Segment_2 segment = { {0., 0.}, {0., 0.} };
  std::vector<dtkContinuousGeometryPrimitives::IntersectionObject_2> intersection_objects = {};
};

sislRationalBezierCurve2DSegmentIntersector::sislRationalBezierCurve2DSegmentIntersector(void) : dtkRationalBezierCurve2DSegmentIntersector(), d(new sislRationalBezierCurve2DSegmentIntersectorPrivate) {}

sislRationalBezierCurve2DSegmentIntersector::~sislRationalBezierCurve2DSegmentIntersector(void){ delete d; }

void sislRationalBezierCurve2DSegmentIntersector::reset() { d->reset(); }

void sislRationalBezierCurve2DSegmentIntersector::setRationalBezierCurve2D(dtkRationalBezierCurve2D* bezier_curve)
{
  d->bezier_curve = bezier_curve;
}

void sislRationalBezierCurve2DSegmentIntersector::setSegment(const dtkContinuousGeometryPrimitives::Segment_2& segment)
{
  d->segment = segment;
}

bool sislRationalBezierCurve2DSegmentIntersector::run(void)
{
  // point on the line and the normal to the line
  double* point = new double[2];
  double* normal = new double[2];

  point[0] = (d->segment)[0][0], point[1] = (d->segment)[0][1];

  dtkContinuousGeometryPrimitives::Point_2 delta = (d->segment)[1] - (d->segment)[0];

  dtkContinuousGeometryPrimitives::Vector_3 dir(delta[0], delta[1], 0.);

  normal[0] = -dir[1], normal[1] = dir[0];

  // define a sisl curve
  double* knots = (double*)malloc(sizeof(double)*2*((d->bezier_curve)->degree() + 1));
  for (std::size_t i = 0; i < (d->bezier_curve)->degree() + 1; ++i) {
    knots[i] = 0.;
  }
  for (std::size_t i = (d->bezier_curve)->degree() + 1; i < 2*((d->bezier_curve)->degree() + 1); ++i) {
    knots[i] = 1.;
  }

  //std::cout << "control points: " << std::endl;
  double *coeff = (double*)malloc(sizeof(double)*3*((d->bezier_curve)->degree() + 1));
  dtkContinuousGeometryPrimitives::Point_2 p(0., 0.);
  double w = 0.;
  double sf = 0.; // scale factor, scale normal*(coeff - point) by its maximum value for the robustness of sisl computation
  double sf_max = 0., sf_min = 0.;
  for (std::size_t i = 0; i < (d->bezier_curve)->degree() + 1; ++i) {
    (d->bezier_curve)->controlPoint(i, p.data());
    (d->bezier_curve)->weight(i, &w);
    coeff[3*i] = p[0]*w;
    coeff[3*i + 1] = p[1]*w;
    coeff[3*i + 2] = w;

    // control vertices after inserting Bezier curve to the line equation
    double cp = normal[0]*(p[0] - point[0]) + normal[1]*(p[1] - point[1]);

    if (i == 0) sf_min = cp, sf_max = cp;
    else {
      if (cp > sf_max) sf_max = cp;
      if (cp < sf_min) sf_min = cp;
    }

  }

  sf = sf_max - sf_min;

  // scale normal only
  normal[0] /= sf, normal[1] /= sf;

  //std::cout << "curve degree: " << (d->bezier_curve)->degree() << std::endl;
  SISLCurve* sisl_curve = newCurve((d->bezier_curve)->degree() + 1, (d->bezier_curve)->degree() + 1, knots, coeff, 4, 2, 2);

  // intersection computation with sisl
  int dim = 2;
  double epsco = 0.; // computational resolution, actually not used according to sisl manual
  double epsge = 1.e-10; // geometry resolution
  int numintpt = 0;
  double* intpar = nullptr;
  int numintcu = 0;
  SISLIntcurve **intcurve =nullptr;
  int stat = 0;

  double tolerance = 1.e-10;

  s1850(sisl_curve, point, normal, dim, epsco, epsge, &numintpt, &intpar, &numintcu, &intcurve, &stat);

  if (stat < 0) {
    dtkError() << "SISL failed in 2D curve / segment intersection";
    return false;
  }
/*
  std::cout << "2D curve" << std::endl;
  for (double i = 0.; i <= 1.05; i += 0.1) {
    double curve_point[2];
    (d->bezier_curve)->evaluatePoint(i, curve_point);
    std::cout << curve_point[0] << " " << curve_point[1] << " " << 0 << std::endl;
  }

  // evaluate sisl curve
  std::cout << "sisl curve" << std::endl;
  int degree = (d->bezier_curve)->degree();
  stat = 0;
  for (double i = 0.; i <= 1.05; i += 0.1) {
    double curve_point[2];
    s1227(sisl_curve, 0, i, &degree, curve_point, &stat);
    std::cout << curve_point[0] << " " << curve_point[1] << " " << 0 << std::endl;
  }

  std::cout << "segment" << std::endl;
  std::cout << (d->segment)[0][0] << " " << (d->segment)[0][1] << std::endl;
  std::cout << (d->segment)[1][0] << " " << (d->segment)[1][1] << std::endl;

  std::cout << "number of intersection points: " << numintpt << std::endl;
*/
  // filter intersection points outside the segment
  dtkContinuousGeometryPrimitives::Point_2 eval(0., 0.);
  for (int i = 0; i < numintpt; ++i) {
    (d->bezier_curve)->evaluatePoint(intpar[i], eval.data());

    //std::cout << eval[0] << " " << eval[1] << std::endl;

    dtkContinuousGeometryPrimitives::Vector_3 sol_a( (eval - (d->segment)[0])[0], (eval - (d->segment)[0])[1], 0. );
    dtkContinuousGeometryPrimitives::Vector_3 sol_b( (eval - (d->segment)[1])[0], (eval - (d->segment)[1])[1], 0. );

    if(dtkContinuousGeometryTools::norm(sol_a) > tolerance && dtkContinuousGeometryTools::norm(sol_b) > tolerance) {
      double angle_a = std::acos(std::min(std::max(dtkContinuousGeometryTools::dotProduct(sol_a, dir) / (dtkContinuousGeometryTools::norm(sol_a) * dtkContinuousGeometryTools::norm(dir)), -1.), 1.));
      double angle_b = std::acos(std::min(std::max(dtkContinuousGeometryTools::dotProduct(sol_b, dir) / (dtkContinuousGeometryTools::norm(sol_b) * dtkContinuousGeometryTools::norm(dir)), -1.), 1.));
      if(angle_a < (M_PI / 2.) && angle_b > (M_PI / 2.)) {
        dtkContinuousGeometryPrimitives::PointImage_2 p_img(eval, true);
        dtkContinuousGeometryPrimitives::PointIntersection_2 p_int(p_img);
        dtkContinuousGeometryPrimitives::PointPreImage_1 p_pre_img(intpar[i], true);
        p_int.pushBackPreImage(p_pre_img);
        d->intersection_objects.emplace_back(p_int);
      }
    } else {
      dtkContinuousGeometryPrimitives::PointImage_2 p_img(eval, false);
      dtkContinuousGeometryPrimitives::PointIntersection_2 p_int(p_img);
      dtkContinuousGeometryPrimitives::PointPreImage_1 p_pre_img(intpar[i], true);
      p_int.pushBackPreImage(p_pre_img);
      d->intersection_objects.emplace_back(p_int);
    }

  }

  if(intpar)
  {
    free(intpar);
  }

  for(int i=0; i< numintcu; ++i)
  {
    freeIntcurve(intcurve[i]);
  }
  if(intcurve)
  {
    free(intcurve);
  }
  delete[] point;
  delete[] normal;
  freeCurve(sisl_curve);

  return true;
}

const std::vector<dtkContinuousGeometryPrimitives::IntersectionObject_2> &
sislRationalBezierCurve2DSegmentIntersector::intersection(void)
{
  return d->intersection_objects;
}
