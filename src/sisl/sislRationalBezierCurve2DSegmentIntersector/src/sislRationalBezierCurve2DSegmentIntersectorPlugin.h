/*
 * sislRationalBezierCurve2DSegmentIntersectorPlugin.h
 *
 *  Created on: Dec 19, 2019
 *      Author: xxiao
 */

#ifndef SRC_SISL_SISLRATIONALBEZIERCURVE2DSEGMENTINTERSECTOR_SRC_SISLRATIONALBEZIERCURVE2DSEGMENTINTERSECTORPLUGIN_H_
#define SRC_SISL_SISLRATIONALBEZIERCURVE2DSEGMENTINTERSECTOR_SRC_SISLRATIONALBEZIERCURVE2DSEGMENTINTERSECTORPLUGIN_H_

#include <QtCore>

#include <dtkContinuousGeometry.h>

#include <dtkRationalBezierCurve2DSegmentIntersector.h>

class sislRationalBezierCurve2DSegmentIntersectorPlugin : public dtkRationalBezierCurve2DSegmentIntersectorPlugin
{
  Q_OBJECT
  Q_INTERFACES(dtkRationalBezierCurve2DSegmentIntersectorPlugin)
  Q_PLUGIN_METADATA(IID "fr.inria.sislRationalBezierCurve2DSegmentIntersectorPlugin" FILE "sislRationalBezierCurve2DSegmentIntersectorPlugin.json")

public:
  sislRationalBezierCurve2DSegmentIntersectorPlugin(void) {}
  ~sislRationalBezierCurve2DSegmentIntersectorPlugin(void) {}

public:
  void initialize(void);
  void uninitialize(void);
};

#endif /* SRC_SISL_SISLRATIONALBEZIERCURVE2DSEGMENTINTERSECTOR_SRC_SISLRATIONALBEZIERCURVE2DSEGMENTINTERSECTORPLUGIN_H_ */
