/*
 * sislRationalBezierCurve2DSegmentIntersector.h
 *
 *  Created on: Dec 19, 2019
 *      Author: xxiao
 */

#ifndef SRC_SISL_SISLRATIONALBEZIERCURVE2DSEGMENTINTERSECTOR_SRC_SISLRATIONALBEZIERCURVE2DSEGMENTINTERSECTOR_H_
#define SRC_SISL_SISLRATIONALBEZIERCURVE2DSEGMENTINTERSECTOR_SRC_SISLRATIONALBEZIERCURVE2DSEGMENTINTERSECTOR_H_

#include <dtkRationalBezierCurve2DSegmentIntersector>

#include <sislRationalBezierCurve2DSegmentIntersectorExport.h>

#include <dtkContinuousGeometryUtils.h>

struct sislRationalBezierCurve2DSegmentIntersectorPrivate;

class dtkRationalBezierCurve2D;

class SISLRATIONALBEZIERCURVE2DSEGMENTINTERSECTOR_EXPORT sislRationalBezierCurve2DSegmentIntersector : public dtkRationalBezierCurve2DSegmentIntersector
{
public:
  sislRationalBezierCurve2DSegmentIntersector(void);
  virtual ~sislRationalBezierCurve2DSegmentIntersector(void);

  void setRationalBezierCurve2D(dtkRationalBezierCurve2D *bezier_curve) final;
  void setSegment(const dtkContinuousGeometryPrimitives::Segment_2& segment) final;

  bool run(void) final;

  void reset() final;

  const std::vector< dtkContinuousGeometryPrimitives::IntersectionObject_2 > & intersection(void) final;

protected:
  sislRationalBezierCurve2DSegmentIntersectorPrivate* d;
};

inline dtkRationalBezierCurve2DSegmentIntersector* sislRationalBezierCurve2DSegmentIntersectorCreator(void)
{
    return new sislRationalBezierCurve2DSegmentIntersector();
}

#endif /* SRC_SISL_SISLRATIONALBEZIERCURVE2DSEGMENTINTERSECTOR_SRC_SISLRATIONALBEZIERCURVE2DSEGMENTINTERSECTOR_H_ */
