/*
 * sislRationalBezierCurvePlaneIntersector.cpp
 *
 *  Created on: Apr 1, 2020
 *      Author: xxiao
 */

#include "sislRationalBezierCurvePlaneIntersector.h"

#include <dtkContinuousGeometryUtils>
#include <dtkRationalBezierCurve>
#include <dtkRationalBezierCurveDataSisl.h>

#include <sisl.h>

struct sislRationalBezierCurvePlaneIntersectorPrivate
{
  void reset() {
    intersection_parameters.clear();
  }

  const dtkRationalBezierCurve* rational_bezier_curve = nullptr;
  dtkContinuousGeometryPrimitives::Plane_3 plane { {0., 0., 0.}, {0., 0., 0.} };
  std::vector<double> intersection_parameters;
};

sislRationalBezierCurvePlaneIntersector::sislRationalBezierCurvePlaneIntersector(void) : d(new sislRationalBezierCurvePlaneIntersectorPrivate) {}

sislRationalBezierCurvePlaneIntersector::~sislRationalBezierCurvePlaneIntersector(void)
{
  delete d;
  d = nullptr;
}

void sislRationalBezierCurvePlaneIntersector::setRationalBezierCurve(const dtkRationalBezierCurve* rational_bezier_curve)
{
  d->rational_bezier_curve = rational_bezier_curve;
}

void sislRationalBezierCurvePlaneIntersector::setPlane(const dtkContinuousGeometryPrimitives::Plane_3& plane)
{
  d->plane = plane;
}

void sislRationalBezierCurvePlaneIntersector::reset()
{
  d->reset();
}

bool sislRationalBezierCurvePlaneIntersector::run(void)
{
  if (d->rational_bezier_curve == nullptr) {
    dtkFatal() << "bezier curve in sislRationalBezierCurvePlaneIntersector is not set";
    return false;
  }

  const dtkRationalBezierCurveDataSisl *sisl_rb_curve = dynamic_cast< const dtkRationalBezierCurveDataSisl *>(d->rational_bezier_curve->data());
  SISLCurve *sisl_curve = nullptr;

  if (sisl_rb_curve != nullptr) {
    sisl_curve = const_cast< SISLCurve *>(sisl_rb_curve->sislRationalBezierCurve());
  }
  else {
    std::size_t degree = d->rational_bezier_curve->degree();
    double* knots = (double*)malloc(sizeof(double)*2*(degree + 1));
    for (std::size_t i = 0; i < degree + 1; ++i) knots[i] = 0.;
    for (std::size_t i = degree + 1; i < 2*(degree + 1); ++i) knots[i] = 1.;

    double* coeff = (double*)malloc(sizeof(double)*4*(degree + 1));
    dtkContinuousGeometryPrimitives::Point_3 cp(0., 0., 0.);
    double w = 0.;
    for (std::size_t i = 0; i < degree + 1; ++i) {
      d->rational_bezier_curve->controlPoint(i, cp.data());
      d->rational_bezier_curve->weight(i, &w);
      coeff[4*i] = cp[0]*w;
      coeff[4*i + 1] = cp[1]*w;
      coeff[4*i + 2] = cp[2]*w;
      coeff[4*i + 3] = w;
    }

    sisl_curve = newCurve(degree + 1, degree + 1, knots, coeff, 4, 3, 2);
  }

  int dim = 3;
  double tolerance_3d = 1e-10;
  double geometry_resolution = 1e-10;
  double computational_resolution = 1e-15; //Not used (according to documentation)
  int numintpt = 0;
  double *intpar = 0;
  int numintcu = 0;
  SISLIntcurve **intcurve;
  int stat = 0;

  double* point = new double[3];
  double* normal = new double[3];
  point[0] = (d->plane).point()[0], point[1] = (d->plane).point()[1], point[2] = (d->plane).point()[2];
  normal[0] = (d->plane).normal()[0], normal[1] = (d->plane).normal()[1], normal[2] = (d->plane).normal()[2];

  s1850(sisl_curve, point, normal, dim, computational_resolution, geometry_resolution, &numintpt, &intpar, &numintcu, &intcurve, &stat);

  if (stat < 0) {
    dtkError() << "SISL failed in curve / plane intersection";
    return false;
  }

  for (auto i = 0; i < numintpt; ++i) {
    d->intersection_parameters.push_back(intpar[i]);
  }

  delete[] point;
  delete[] normal;
  if(intpar != 0)
    free(intpar);
  if (sisl_rb_curve == nullptr) freeCurve(sisl_curve);

  return true;
}

std::vector<double> sislRationalBezierCurvePlaneIntersector::intersectionParameters(void)
{
  return d->intersection_parameters;
}
