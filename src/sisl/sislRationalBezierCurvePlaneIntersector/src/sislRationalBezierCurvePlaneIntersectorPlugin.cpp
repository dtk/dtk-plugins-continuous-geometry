/*
 * sislRationalBezierCurvePlaneIntersectorPlugin.cpp
 *
 *  Created on: Apr 1, 2020
 *      Author: xxiao
 */

#include "sislRationalBezierCurvePlaneIntersector.h"
#include "sislRationalBezierCurvePlaneIntersectorPlugin.h"

#include <dtkCore>
#include <dtkContinuousGeometry>

// ///////////////////////////////////////////////////////////////////
// sislRationalBezierCurvePlaneIntersectorPlugin
// ///////////////////////////////////////////////////////////////////

void sislRationalBezierCurvePlaneIntersectorPlugin::initialize(void)
{
    dtkContinuousGeometry::rationalBezierCurvePlaneIntersector::pluginFactory().record("sislRationalBezierCurvePlaneIntersector", sislRationalBezierCurvePlaneIntersectorCreator);
}

void sislRationalBezierCurvePlaneIntersectorPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(sislRationalBezierCurvePlaneIntersector)


