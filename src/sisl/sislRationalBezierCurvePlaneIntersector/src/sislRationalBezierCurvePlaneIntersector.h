/*
 * sislRatioinalBezierCurvePlaneIntersector.h
 *
 *  Created on: Apr 1, 2020
 *      Author: xxiao
 */

#ifndef SRC_SISL_SISLRATIONALBEZIERCURVEPLANEINTERSECTOR_SRC_SISLRATIONALBEZIERCURVEPLANEINTERSECTOR_H_
#define SRC_SISL_SISLRATIONALBEZIERCURVEPLANEINTERSECTOR_SRC_SISLRATIONALBEZIERCURVEPLANEINTERSECTOR_H_

#include <dtkRationalBezierCurvePlaneIntersector>
#include <sislRationalBezierCurvePlaneIntersectorExport.h>
#include <dtkContinuousGeometry>

class dtkRationalBezierCurve;

struct sislRationalBezierCurvePlaneIntersectorPrivate;

class SISLRATIONALBEZIERCURVEPLANEINTERSECTOR_EXPORT sislRationalBezierCurvePlaneIntersector : public dtkRationalBezierCurvePlaneIntersector
{
 public:
    sislRationalBezierCurvePlaneIntersector(void);
    ~sislRationalBezierCurvePlaneIntersector(void);

 public:
    void setRationalBezierCurve(const dtkRationalBezierCurve *) final;
    void setPlane(const dtkContinuousGeometryPrimitives::Plane_3& plane) final;

 public:
    bool run(void) final;
    void reset() final;

 public:
    std::vector< double > intersectionParameters(void) final;

 protected:
    sislRationalBezierCurvePlaneIntersectorPrivate *d;
};

inline dtkRationalBezierCurvePlaneIntersector *sislRationalBezierCurvePlaneIntersectorCreator(void)
{
    return new sislRationalBezierCurvePlaneIntersector();
}


#endif /* SRC_SISL_SISLRATIONALBEZIERCURVEPLANEINTERSECTOR_SRC_SISLRATIONALBEZIERCURVEPLANEINTERSECTOR_H_ */
