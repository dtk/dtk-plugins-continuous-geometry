/*
 * sislRationalBezierCurvePlaneIntersectorPlugin.h
 *
 *  Created on: Apr 1, 2020
 *      Author: xxiao
 */

#ifndef SRC_SISL_SISLRATIONALBEZIERCURVEPLANEINTERSECTOR_SRC_SISLRATIONALBEZIERCURVEPLANEINTERSECTORPLUGIN_H_
#define SRC_SISL_SISLRATIONALBEZIERCURVEPLANEINTERSECTOR_SRC_SISLRATIONALBEZIERCURVEPLANEINTERSECTORPLUGIN_H_

#include <QtCore>

#include <dtkContinuousGeometry.h>

#include <dtkRationalBezierCurvePlaneIntersector.h>

class sislRationalBezierCurvePlaneIntersectorPlugin : public dtkRationalBezierCurvePlaneIntersectorPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkRationalBezierCurvePlaneIntersectorPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.sislRationalBezierCurvePlaneIntersectorPlugin" FILE "sislRationalBezierCurvePlaneIntersectorPlugin.json")

public:
     sislRationalBezierCurvePlaneIntersectorPlugin(void) {}
    ~sislRationalBezierCurvePlaneIntersectorPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};


#endif /* SRC_SISL_SISLRATIONALBEZIERCURVEPLANEINTERSECTOR_SRC_SISLRATIONALBEZIERCURVEPLANEINTERSECTORPLUGIN_H_ */
