// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <QTest>

class sislRationalBezierSurfaceLineIntersectorTestCasePrivate;

class sislRationalBezierSurfaceLineIntersectorTestCase : public QObject
{
    Q_OBJECT

public:
    sislRationalBezierSurfaceLineIntersectorTestCase(void);
    ~sislRationalBezierSurfaceLineIntersectorTestCase(void);

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testRun(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);

private:
    sislRationalBezierSurfaceLineIntersectorTestCasePrivate* d;
};

//
// sislRationalBezierSurfaceLineIntersectorTest.h ends here
