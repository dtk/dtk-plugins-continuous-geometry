// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#include "sislRationalBezierSurfaceLineIntersector.h"
#include "sislRationalBezierSurfaceLineIntersectorPlugin.h"

#include <dtkCore>
#include <dtkContinuousGeometry>

// ///////////////////////////////////////////////////////////////////
// sislRationalBezierSurfaceLineIntersectorPlugin
// ///////////////////////////////////////////////////////////////////

void sislRationalBezierSurfaceLineIntersectorPlugin::initialize(void)
{
    dtkContinuousGeometry::rationalBezierSurfaceLineIntersector::pluginFactory().record("sislRationalBezierSurfaceLineIntersector", sislRationalBezierSurfaceLineIntersectorCreator);
}

void sislRationalBezierSurfaceLineIntersectorPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(sislRationalBezierSurfaceLineIntersector)

//
// sislRationalBezierSurfaceLineIntersectorPlugin.cpp ends here
