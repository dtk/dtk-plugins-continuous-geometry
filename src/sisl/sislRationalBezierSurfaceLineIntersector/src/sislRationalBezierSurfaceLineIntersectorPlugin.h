// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <QtCore>

#include <dtkContinuousGeometry.h>

#include <dtkRationalBezierSurfaceLineIntersector.h>

class sislRationalBezierSurfaceLineIntersectorPlugin : public dtkRationalBezierSurfaceLineIntersectorPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkRationalBezierSurfaceLineIntersectorPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.sislRationalBezierSurfaceLineIntersectorPlugin" FILE "sislRationalBezierSurfaceLineIntersectorPlugin.json")

public:
     sislRationalBezierSurfaceLineIntersectorPlugin(void) {}
    ~sislRationalBezierSurfaceLineIntersectorPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

//
// sislRationalBezierSurfaceLineIntersectorPlugin.h ends here
