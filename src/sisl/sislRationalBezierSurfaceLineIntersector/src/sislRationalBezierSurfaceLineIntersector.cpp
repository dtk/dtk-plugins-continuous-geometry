// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "sislRationalBezierSurfaceLineIntersector.h"

#include <dtkContinuousGeometryUtils>
#include <dtkRationalBezierSurface>

#include <dtkRationalBezierSurfaceDataSisl.h>

#include <sisl.h>

struct sislRationalBezierSurfaceLineIntersectorPrivate
{
    void reset() { intersection_objects.clear(); }

    dtkRationalBezierSurface *rb_surface = nullptr;
    dtkContinuousGeometryPrimitives::Line_3 *line = nullptr;
    std::vector< dtkContinuousGeometryPrimitives::IntersectionObject > intersection_objects = {};
};

/*!
  \class sislRationalBezierSurfaceLineIntersector
  \inmodule dtkPluginsContinuousGeometry
  \brief sislRationalBezierSurfaceLineIntersector is a SISL implementation of the concept dtkRationalBezierSurfaceLineIntersector describing the intersection between a line and a rational Bezier surface.

  The following lines of code show how to instanciate and use the class in pratice :
  \code
  dtkRationalBezierSurfaceLineIntersector *intersector = dtkContinuousGeometry::nurbsSurfaceLineIntersector::pluginFactory().create("sislRationalBezierSurfaceLineIntersector");
  intersector->setRationalBezierSurface(...);
  intersector->setLine(...);
  intersector->run();
  const std::vector< IntersectionObject > & intersection_object = intersector->intersection();
  \endcode
*/

sislRationalBezierSurfaceLineIntersector::sislRationalBezierSurfaceLineIntersector(void) : dtkRationalBezierSurfaceLineIntersector(), d(new sislRationalBezierSurfaceLineIntersectorPrivate) {}

sislRationalBezierSurfaceLineIntersector::~sislRationalBezierSurfaceLineIntersector(void){}

void sislRationalBezierSurfaceLineIntersector::reset() { d->reset(); }

/*! \fn sislRationalBezierSurfaceLineIntersector::setRationalBezierSurface(dtkRationalBezierSurface *surface)
  Provides the rational Bezier surface to the intersector (\a surface).

  \a surface : the rational Bezier surface to intersect
*/
void sislRationalBezierSurfaceLineIntersector::setRationalBezierSurface(dtkRationalBezierSurface *rb_surface)
{
    d->rb_surface = rb_surface;
}

/*! \fn sislRationalBezierSurfaceLineIntersector::setLine(dtkContinuousGeometryPrimitives::Line_3 *line)
  Provides the \a line to intersect the surface with to the intersector.

  \a line : the line to intersect the rational Bezier surface with
*/
void sislRationalBezierSurfaceLineIntersector::setLine(dtkContinuousGeometryPrimitives::Line_3* line)
{
    d->line = line;
}

/*! \fn sislRationalBezierSurfaceLineIntersector::run(void)
  Intersects the rational Bezier surface (provided with \l setRationalBezierSurface(dtkRationalBezierSurface *surface)) with the line(provided with \l setLine(Line_3 *line)), the result can be retrieved with \l intersection(void).
*/
bool sislRationalBezierSurfaceLineIntersector::run(void)
{
    if(d->rb_surface == nullptr) {
        dtkFatal() << "The surface was not set before calling the run() method.";
    }
    if(d->line == nullptr) {
        dtkFatal() << "The line was not set before calling the run() method.";
    }
    double tolerance_3d = 1e-10;
    double geometry_resolution = 1e-10;
    double computational_resolution = 1e-15; //Not used (according to documentation)
    int numintpt = 0;
    double *pointpar;
    int numintcr = 0;
    SISLIntcurve **intcurves;
    int stat = 0;

    dtkRationalBezierSurfaceDataSisl *sisl_rb_surface = dynamic_cast<dtkRationalBezierSurfaceDataSisl *>(d->rb_surface->data());
    SISLSurf *sisl_surf = nullptr;

    if(sisl_rb_surface != nullptr) {
        sisl_surf = const_cast< SISLSurf *>(sisl_rb_surface->sislRationalBezierSurface());
    } else {
        // ///////////////////////////////////////////////////////////////////
        // Convert the dtkRationalBezierSurface to a SISL_surf
        // ///////////////////////////////////////////////////////////////////
        std::size_t u_degree = d->rb_surface->uDegree();
        std::size_t v_degree = d->rb_surface->vDegree();
        std::size_t nb_cp_u = u_degree + 1;
        std::size_t nb_cp_v = v_degree + 1;

        double *et1 = new double[2 * nb_cp_u];
        for(std::size_t i = 0; i < nb_cp_u; ++i) { et1[i] = 0; }
        for(std::size_t i = nb_cp_u; i < 2 * nb_cp_u; ++i) { et1[i] = 1; }
        double *et2 = new double[2 * nb_cp_v];
        for(std::size_t i = 0; i < nb_cp_v; ++i) { et2[i] = 0; }
        for(std::size_t i = nb_cp_v; i < 2 * nb_cp_v; ++i) { et2[i] = 1; }

        double *ecoef = new double[(3 + 1) * nb_cp_u * nb_cp_v];
        dtkContinuousGeometryPrimitives::Point_3 cp(0., 0., 0.);
        double w = 0.;
        for(std::size_t i = 0; i < nb_cp_u; ++i) {
            for(std::size_t j = 0; j < nb_cp_v; ++j) {
                d->rb_surface->controlPoint(i, j, cp.data());
                d->rb_surface->weight(i, j, &w);
                ecoef[(3 + 1) * nb_cp_u * j + (3 + 1) * i]     = cp[0] * w;
                ecoef[(3 + 1) * nb_cp_u * j + (3 + 1) * i + 1] = cp[1] * w;
                ecoef[(3 + 1) * nb_cp_u * j + (3 + 1) * i + 2] = cp[2] * w;
                ecoef[(3 + 1) * nb_cp_u * j + (3 + 1) * i + 3] = w;
            }
        }

        /* Copy Flag = 2 : Use pointers to et1, et2 and ecoef; freeSurf will free arlines*/
        sisl_surf = newSurf(nb_cp_u, nb_cp_v, nb_cp_u, nb_cp_v, et1, et2, ecoef, 4, 3, 2);
    }

    s1856(sisl_surf, d->line->point().data(), d->line->direction().data(), 3, computational_resolution, geometry_resolution, &numintpt, &pointpar, &numintcr, &intcurves, &stat);

    if(stat < 0) {
        dtkError() << "The intersection routine did not go well.";
        return false;
    }
    if(numintcr > 0) {
        dtkError() << "Not handling yet intersections other than points. " << Q_FUNC_INFO << ". Line : " << __LINE__;
    }

    dtkContinuousGeometryPrimitives::Point_3 eval(0., 0., 0.);
    for(auto i = 0; i < numintpt; ++i) {
        d->rb_surface->evaluatePoint(pointpar[2*i], pointpar[2*i + 1], eval.data());

        // dtkContinuousGeometryPrimitives::Vector_3 sol = eval - d->line->point();
        dtkContinuousGeometryPrimitives::PointImage p_img(eval, true);
        dtkContinuousGeometryPrimitives::PointIntersection p_int(p_img);
        dtkContinuousGeometryPrimitives::PointPreImage p_pre_img(dtkContinuousGeometryPrimitives::Point_2(pointpar[2*i], pointpar[2*i + 1]), true);
        p_int.pushBackPreImage(p_pre_img);
        d->intersection_objects.emplace_back(p_int);
    }

    if(sisl_rb_surface == nullptr) {
        freeSurf(sisl_surf);
    }
    return true;
}

/*! \fn sislRationalBezierSurfaceLineIntersector::intersection(void)
  Returns the intersection objects.

  The caller is responsible for deleting all the IntersectionObject of the returned vector, as well as the vector.
*/
const std::vector< dtkContinuousGeometryPrimitives::IntersectionObject> &
sislRationalBezierSurfaceLineIntersector::intersection(void)
{
    return d->intersection_objects;
}

//
// sislRationalBezierSurfaceLineIntersector.cpp ends here
