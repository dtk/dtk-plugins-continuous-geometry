// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <sislRationalBezierSurfaceLineIntersectorExport.h>

#include <dtkRationalBezierSurfaceLineIntersector>

#include <dtkContinuousGeometryUtils.h>

struct sislRationalBezierSurfaceLineIntersectorPrivate;

class SISLRATIONALBEZIERSURFACELINEINTERSECTOR_EXPORT sislRationalBezierSurfaceLineIntersector : public dtkRationalBezierSurfaceLineIntersector
{
 public:
    sislRationalBezierSurfaceLineIntersector(void);
    ~sislRationalBezierSurfaceLineIntersector(void);

    void setRationalBezierSurface(dtkRationalBezierSurface *surface) final;
    void setLine(dtkContinuousGeometryPrimitives::Line_3 *line) final;

    bool run(void) final;
    void reset() final;

    const std::vector< dtkContinuousGeometryPrimitives::IntersectionObject>& intersection(void) final;

protected:
    sislRationalBezierSurfaceLineIntersectorPrivate *d;
};

// ///////////////////////////////////////////////////////////////////

inline dtkRationalBezierSurfaceLineIntersector* sislRationalBezierSurfaceLineIntersectorCreator(void)
{
    return new sislRationalBezierSurfaceLineIntersector();
}

//
// sislRationalBezierSurfaceLineIntersector.h ends here
