// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkRationalBezierSurfacePointIntersector>

#include <sislRationalBezierSurfacePointIntersectorExport.h>

#include <dtkContinuousGeometryUtils.h>

struct sislRationalBezierSurfacePointIntersectorPrivate;

class dtkRationalBezierSurface;

class SISLRATIONALBEZIERSURFACEPOINTINTERSECTOR_EXPORT sislRationalBezierSurfacePointIntersector : public dtkRationalBezierSurfacePointIntersector
{
 public:
    sislRationalBezierSurfacePointIntersector(void);
    ~sislRationalBezierSurfacePointIntersector(void);

    void setRationalBezierSurface(dtkRationalBezierSurface *surface) final;
    void setPoint(dtkContinuousGeometryPrimitives::Point_3 *point) final;

    bool run(void) final;
    void reset() final;

    const std::vector< dtkContinuousGeometryPrimitives::IntersectionObject>& intersection(void) final;

protected:
    sislRationalBezierSurfacePointIntersectorPrivate *d;
};

// ///////////////////////////////////////////////////////////////////

inline dtkRationalBezierSurfacePointIntersector* sislRationalBezierSurfacePointIntersectorCreator(void)
{
    return new sislRationalBezierSurfacePointIntersector();
}

//
// sislRationalBezierSurfacePointIntersector.h ends here
