// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <QtCore>

#include <dtkContinuousGeometry.h>

#include <dtkRationalBezierSurfacePointIntersector.h>

class sislRationalBezierSurfacePointIntersectorPlugin : public dtkRationalBezierSurfacePointIntersectorPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkRationalBezierSurfacePointIntersectorPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.sislRationalBezierSurfacePointIntersectorPlugin" FILE "sislRationalBezierSurfacePointIntersectorPlugin.json")

public:
     sislRationalBezierSurfacePointIntersectorPlugin(void) {}
    ~sislRationalBezierSurfacePointIntersectorPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

//
// sislRationalBezierSurfacePointIntersectorPlugin.h ends here
