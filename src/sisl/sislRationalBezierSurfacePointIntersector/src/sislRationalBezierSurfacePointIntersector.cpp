// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "sislRationalBezierSurfacePointIntersector.h"

#include <dtkContinuousGeometryUtils>
#include <dtkRationalBezierSurface>

#include <dtkRationalBezierSurfaceDataSisl.h>

#include <sisl.h>

struct sislRationalBezierSurfacePointIntersectorPrivate
{
    void reset() {
        intersection_objects.clear();
    }

    dtkRationalBezierSurface *rb_surface = nullptr;
    dtkContinuousGeometryPrimitives::Point_3 *point = nullptr;
    std::vector< dtkContinuousGeometryPrimitives::IntersectionObject > intersection_objects;
};

/*!
  \class sislRationalBezierSurfacePointIntersector
  \inmodule dtkPluginsContinuousGeometry
  \brief sislRationalBezierSurfacePointIntersector is a SISL implementation of the concept dtkRationalBezierSurfacePointIntersector describing the intersection between a point and a rational Bezier surface.

  The following lines of code show how to instanciate and use the class in pratice :
  \code
  dtkRationalBezierSurfacePointIntersector *intersector = dtkContinuousGeometry::nurbsSurfacePointIntersector::pluginFactory().create("sislRationalBezierSurfacePointIntersector");
  intersector->setRationalBezierSurface(...);
  intersector->setPoint(...);
  intersector->run();
  const std::vector< IntersectionObject > & intersection_object = intersector->intersection();
  \endcode
*/

sislRationalBezierSurfacePointIntersector::sislRationalBezierSurfacePointIntersector(void) : dtkRationalBezierSurfacePointIntersector(), d(new sislRationalBezierSurfacePointIntersectorPrivate) {}

sislRationalBezierSurfacePointIntersector::~sislRationalBezierSurfacePointIntersector(void){
    delete d;
}

void sislRationalBezierSurfacePointIntersector::reset() {
    d->reset();
}

/*! \fn sislRationalBezierSurfacePointIntersector::setRationalBezierSurface(dtkRationalBezierSurface *surface)
  Provides the rational Bezier surface to the intersector (\a surface).

  \a surface : the rational Bezier surface to intersect
*/
void sislRationalBezierSurfacePointIntersector::setRationalBezierSurface(dtkRationalBezierSurface *rb_surface)
{
    d->rb_surface = rb_surface;
}

/*! \fn sislRationalBezierSurfacePointIntersector::setPoint(dtkContinuousGeometryPrimitives::Point_3 *point)
  Provides the \a point to intersect the surface with to the intersector.

  \a point : the point to intersect the rational Bezier surface with
*/
void sislRationalBezierSurfacePointIntersector::setPoint(dtkContinuousGeometryPrimitives::Point_3* point)
{
    d->point = point;
}

/*! \fn sislRationalBezierSurfacePointIntersector::run(void)
  Intersects the rational Bezier surface (provided with \l setRationalBezierSurface(dtkRationalBezierSurface *surface)) with the point(provided with \l setPoint(Point_3 *point)), the result can be retrieved with \l intersection(void).
*/
bool sislRationalBezierSurfacePointIntersector::run(void)
{
    if(d->rb_surface == nullptr) {
        dtkFatal() << "The surface was not set before calling the run() method.";
    }
    if(d->point == nullptr) {
        dtkFatal() << "The point was not set before calling the run() method.";
    }

    double tolerance_3d = 1e-10;
    double geometry_resolution = 1e-10;
    int numintpt = 0;
    double *pointpar;
    int numintcr = 0;
    SISLIntcurve **intcurves;
    int stat = 0;

    dtkRationalBezierSurfaceDataSisl *sisl_rb_surface = dynamic_cast<dtkRationalBezierSurfaceDataSisl *>(d->rb_surface->data());
    SISLSurf *sisl_surf = nullptr;

    if(sisl_rb_surface != nullptr) {
        sisl_surf = const_cast< SISLSurf *>(sisl_rb_surface->sislRationalBezierSurface());
    } else {
        // ///////////////////////////////////////////////////////////////////
        // Convert the dtkRationalBezierSurface to a SISL_surf
        // ///////////////////////////////////////////////////////////////////
        std::size_t u_degree = d->rb_surface->uDegree();
        std::size_t v_degree = d->rb_surface->vDegree();
        std::size_t nb_cp_u = u_degree + 1;
        std::size_t nb_cp_v = v_degree + 1;

        double *et1 = (double*)malloc(sizeof(double) * 2 * nb_cp_u);
        for(std::size_t i = 0; i < nb_cp_u; ++i) { et1[i] = 0; }
        for(std::size_t i = nb_cp_u; i < 2 * nb_cp_u; ++i) { et1[i] = 1; }
        double *et2 = (double*)malloc(sizeof(double) * 2 * nb_cp_v);
        for(std::size_t i = 0; i < nb_cp_v; ++i) { et2[i] = 0; }
        for(std::size_t i = nb_cp_v; i < 2 * nb_cp_v; ++i) { et2[i] = 1; }

        double *ecoef = (double*)malloc(sizeof(double) * (3 + 1) * nb_cp_u * nb_cp_v);
        dtkContinuousGeometryPrimitives::Point_3 cp(0., 0., 0.);
        double w = 0.;
        for(std::size_t i = 0; i < nb_cp_u; ++i) {
            for(std::size_t j = 0; j < nb_cp_v; ++j) {
                d->rb_surface->controlPoint(i, j, cp.data());
                d->rb_surface->weight(i, j, &w);
                ecoef[(3 + 1) * nb_cp_u * j + (3 + 1) * i]     = cp[0] * w;
                ecoef[(3 + 1) * nb_cp_u * j + (3 + 1) * i + 1] = cp[1] * w;
                ecoef[(3 + 1) * nb_cp_u * j + (3 + 1) * i + 2] = cp[2] * w;
                ecoef[(3 + 1) * nb_cp_u * j + (3 + 1) * i + 3] = w;
            }
        }

        /* Copy Flag = 2 : Use pointers to et1, et2 and ecoef; freeSurf will free arpoints*/
        sisl_surf = newSurf(nb_cp_u, nb_cp_v, nb_cp_u, nb_cp_v, et1, et2, ecoef, 4, 3, 2);
    }

    s1870(sisl_surf, d->point->data(), 3, geometry_resolution, &numintpt, &pointpar, &numintcr, &intcurves, &stat);

    if(stat < 0) {
        dtkError() << "The intersection routine did not go well.";
        return false;
    }
    if(numintcr > 0) {
        dtkError() << "Not handling yet intersections other than points. " << Q_FUNC_INFO << ". Line : " << __LINE__;
    }

    dtkContinuousGeometryPrimitives::Point_3 eval(0., 0., 0.);
    for(auto i = 0; i < numintpt; ++i) {
        d->rb_surface->evaluatePoint(pointpar[2*i], pointpar[2*i + 1], eval.data());
        // ///////////////////////////////////////////////////////////////////
        // Computes the angle between P-P0 and dir
        // ///////////////////////////////////////////////////////////////////
        dtkContinuousGeometryPrimitives::Vector_3 sol = eval - (*d->point);
         if(dtkContinuousGeometryTools::norm(sol) > tolerance_3d) {
             dtkWarn() << "The evaluation of a solution is a point far from the intersection point.";
         }
         dtkContinuousGeometryPrimitives::PointImage p_img(eval, false);
         dtkContinuousGeometryPrimitives::PointIntersection p_int(p_img);
         dtkContinuousGeometryPrimitives::PointPreImage p_pre_img(dtkContinuousGeometryPrimitives::Point_2(pointpar[2*i], pointpar[2*i + 1]), true);
         p_int.pushBackPreImage(p_pre_img);
         d->intersection_objects.emplace_back(p_int);
    }

    if(sisl_rb_surface == nullptr) {
        freeSurf(sisl_surf);
    }
    return true;
}

/*! \fn sislRationalBezierSurfacePointIntersector::intersection(void)
  Returns the intersection objects.

  The caller is responsible for deleting all the IntersectionObject of the returned vector, as well as the vector.
*/
const std::vector< dtkContinuousGeometryPrimitives::IntersectionObject>&
sislRationalBezierSurfacePointIntersector::intersection(void)
{
    return d->intersection_objects;
}

//
// sislRationalBezierSurfacePointIntersector.cpp ends here
