// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "sislRationalBezierSurfacePointIntersector.h"
#include "sislRationalBezierSurfacePointIntersectorPlugin.h"

#include <dtkCore>
#include <dtkContinuousGeometry>

// ///////////////////////////////////////////////////////////////////
// sislRationalBezierSurfacePointIntersectorPlugin
// ///////////////////////////////////////////////////////////////////

void sislRationalBezierSurfacePointIntersectorPlugin::initialize(void)
{
    dtkContinuousGeometry::rationalBezierSurfacePointIntersector::pluginFactory().record("sislRationalBezierSurfacePointIntersector", sislRationalBezierSurfacePointIntersectorCreator);
}

void sislRationalBezierSurfacePointIntersectorPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(sislRationalBezierSurfacePointIntersector)

//
// sislRationalBezierSurfacePointIntersectorPlugin.cpp ends here
