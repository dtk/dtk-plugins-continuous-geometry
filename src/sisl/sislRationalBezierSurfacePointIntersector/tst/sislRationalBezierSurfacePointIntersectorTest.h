// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <QTest>

class sislRationalBezierSurfacePointIntersectorTestCasePrivate;

class sislRationalBezierSurfacePointIntersectorTestCase : public QObject
{
    Q_OBJECT

public:
    sislRationalBezierSurfacePointIntersectorTestCase(void);
    ~sislRationalBezierSurfacePointIntersectorTestCase(void);

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testRun(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);

private:
    sislRationalBezierSurfacePointIntersectorTestCasePrivate* d;
};

//
// sislRationalBezierSurfacePointIntersectorTest.h ends here
