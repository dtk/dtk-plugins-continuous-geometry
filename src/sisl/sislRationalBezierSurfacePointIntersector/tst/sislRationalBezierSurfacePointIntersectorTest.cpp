// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "sislRationalBezierSurfacePointIntersectorTest.h"

#include "sislRationalBezierSurfacePointIntersector.h"

#include <dtkContinuousGeometry>
#include <dtkContinuousGeometrySettings>

#include <dtkAbstractRationalBezierSurfaceData>
#include <dtkRationalBezierSurface>

#include <dtkContinuousGeometryUtils>

#include <dtkTest>
// ///////////////////////////////////////////////////////////////////
// D-Pointer implementation
// ///////////////////////////////////////////////////////////////////

class sislRationalBezierSurfacePointIntersectorTestCasePrivate{
public:
    sislRationalBezierSurfacePointIntersector* intersector;
};
// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

sislRationalBezierSurfacePointIntersectorTestCase::sislRationalBezierSurfacePointIntersectorTestCase(void):d(new sislRationalBezierSurfacePointIntersectorTestCasePrivate())
{
    dtkLogger::instance().attachConsole();
    dtkLogger::instance().setLevel(dtk::LogLevel::Trace);

    dtkContinuousGeometrySettings settings;
    settings.beginGroup("continuous-geometry");
    dtkContinuousGeometry::initialize(settings.value("plugins").toString());

    settings.endGroup();
    d->intersector = nullptr;
}

sislRationalBezierSurfacePointIntersectorTestCase::~sislRationalBezierSurfacePointIntersectorTestCase(void)
{
    delete d;
}

void sislRationalBezierSurfacePointIntersectorTestCase::initTestCase(void)
{
    d->intersector = new sislRationalBezierSurfacePointIntersector();
}

void sislRationalBezierSurfacePointIntersectorTestCase::init(void)
{
}

void sislRationalBezierSurfacePointIntersectorTestCase::testRun(void)
{
    dtkAbstractRationalBezierSurfaceData *surface_data_1_1_0 = dtkContinuousGeometry::abstractRationalBezierSurfaceData::pluginFactory().create("dtkRationalBezierSurfaceDataOn");
    if(surface_data_1_1_0 == nullptr) {
        dtkWarn() << "This test requires that the openNURBS implementation of dtkRationalBezierSurfaceData is available";
        return;
    } else {
        std::size_t dim = 3;
        std::size_t order_u = 2;
        std::size_t order_v = 2;

        std::vector< double > cps_0((dim + 1) * order_u * order_v);
        cps_0[0] = 0.;   cps_0[1] = 0.; cps_0[2] = 0.;  cps_0[3] = 1.;
        cps_0[4] = 2.;   cps_0[5] = 0.; cps_0[6] = 0.;  cps_0[7] = 1.;
        cps_0[8] = 0.;   cps_0[9] = 2.; cps_0[10] = 0.; cps_0[11] = 1.;
        cps_0[12] = 2.;  cps_0[13] = 2.;  cps_0[14] = 0.; cps_0[15] = 1.;

        dtkRationalBezierSurface *rb_surface_1_1_0 = new dtkRationalBezierSurface(surface_data_1_1_0);

        rb_surface_1_1_0->create(dim, order_u, order_v, cps_0.data());

        dtkContinuousGeometryPrimitives::Point_3 point_0(.5, .5, 0.);

        d->intersector->setRationalBezierSurface(rb_surface_1_1_0);
        d->intersector->setPoint(&point_0);
        d->intersector->run();

        auto result = d->intersector->intersection();
        QVERIFY(result.size() == 1);
        QVERIFY(result.front().isPoint());

        QVERIFY(result.front().pointIntersection().image()[0] == 0.5);
        QVERIFY(result.front().pointIntersection().image()[1] == 0.5);
        QVERIFY(result.front().pointIntersection().image()[2] == 0.);

        QVERIFY(result.front().pointIntersection().preImages().front()[0] == 0.25);
        QVERIFY(result.front().pointIntersection().preImages().front()[1] == 0.25);

        dtkContinuousGeometryPrimitives::Point_3 point_1(.5, .5, -1.);
        d->intersector->reset();
        d->intersector->setPoint(&point_1);
        d->intersector->run();
        result = d->intersector->intersection();
        QVERIFY(result.size() == 0);
    }
}

void sislRationalBezierSurfacePointIntersectorTestCase::cleanup(void)
{

}

void sislRationalBezierSurfacePointIntersectorTestCase::cleanupTestCase(void)
{
    if(d->intersector != nullptr) {
        delete d->intersector;
    }
}

DTKTEST_MAIN_NOGUI(sislRationalBezierSurfacePointIntersectorTest, sislRationalBezierSurfacePointIntersectorTestCase)

//
// sislRationalBezierSurfacePointIntersectorTest.cpp ends here
