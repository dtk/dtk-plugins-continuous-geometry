## Version: $Id$
##
######################################################################
##
### Commentary:
##
######################################################################
##
### Change Log:
##
######################################################################
##
### Code:
project(dtkRationalBezierSurfaceDataSisl)

include_directories(src)

## #################################################################
## Inputs
## #################################################################

add_subdirectory(src)
add_subdirectory(tst)

######################################################################
### CMakeLists.txt ends here
