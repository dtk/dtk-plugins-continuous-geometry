// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkRationalBezierSurfaceDataSislTest.h"

#include "dtkRationalBezierSurfaceDataSisl.h"

#include <dtkContinuousGeometryUtils>

#include <dtkTest>

#include <dtkRationalBezierSurface>

// ///////////////////////////////////////////////////////////////////
// D-Pointer implementation
// ///////////////////////////////////////////////////////////////////

class dtkRationalBezierSurfaceDataSislTestCasePrivate{
public:
    dtkRationalBezierSurfaceDataSislTestCasePrivate() : bezier_surface_data_1_2(nullptr), bezier_surface_data_1_1(nullptr), bezier_surface_data_2_2(nullptr), tolerance(1e-5) {};

public:
    dtkRationalBezierSurfaceDataSisl *bezier_surface_data_1_2;
    dtkRationalBezierSurfaceDataSisl *bezier_surface_data_1_1;

    dtkRationalBezierSurfaceDataSisl *bezier_surface_data_2_2;

    double tolerance;
};
// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkRationalBezierSurfaceDataSislTestCase::dtkRationalBezierSurfaceDataSislTestCase(void):d(new dtkRationalBezierSurfaceDataSislTestCasePrivate()) {}

dtkRationalBezierSurfaceDataSislTestCase::~dtkRationalBezierSurfaceDataSislTestCase(void) {}

void dtkRationalBezierSurfaceDataSislTestCase::initTestCase(void)
{
    dtkLogger::instance().attachConsole();
    dtkLogger::instance().setLevel(dtk::LogLevel::Trace);

    std::size_t dim = 3;

    d->bezier_surface_data_1_1 = new dtkRationalBezierSurfaceDataSisl();
    //bi-degree 1,1
    std::size_t order_u_1_1 = 2;
    std::size_t order_v_1_1 = 2;
    std::vector< double > cps_1_1((dim + 1) * order_u_1_1 * order_v_1_1);
    cps_1_1[0] = 0.;  cps_1_1[1] = 0.;  cps_1_1[2] = 0.;  cps_1_1[3] = 1.;
    cps_1_1[4] = 0.;  cps_1_1[5] = 1.;  cps_1_1[6] = 0.;  cps_1_1[7] = 0.6;
    cps_1_1[8] = 1.;  cps_1_1[9] = 0.;  cps_1_1[10] = 0.; cps_1_1[11] = 2.;
    cps_1_1[12] = 1.; cps_1_1[13] = 1.; cps_1_1[14] = 0.; cps_1_1[15] = 1.;
    d->bezier_surface_data_1_1->create(dim, order_u_1_1, order_v_1_1, cps_1_1.data());

    d->bezier_surface_data_1_2 = new dtkRationalBezierSurfaceDataSisl();
    //bi-degree 1,2
    std::size_t order_u_1_2 = 2;
    std::size_t order_v_1_2 = 3;
    std::vector< double > cps_1_2((dim + 1) * order_u_1_2 * order_v_1_2);
    //x_00,           y_00,             z_00,             w_00
    cps_1_2[0] = 1.;  cps_1_2[1] = 0.;  cps_1_2[2] = 0.;  cps_1_2[3] = 1.;
    //x_01,           y_01,             z_01,             w_01
    cps_1_2[4] = 1.;  cps_1_2[5] = 0.;  cps_1_2[6] = 1.;  cps_1_2[7] = 0.6;
    //x_02,           y_02,             z_02,             w_02
    cps_1_2[8] = 0.;  cps_1_2[9] = 0.;  cps_1_2[10] = 2.; cps_1_2[11] = 2.;
    //x_10,           y_10,             z_10,             w_10
    cps_1_2[12] = 1.; cps_1_2[13] = 1.; cps_1_2[14] = 0.; cps_1_2[15] = 1.;
    cps_1_2[16] = 1.; cps_1_2[17] = 1.; cps_1_2[18] = 1.; cps_1_2[19] = 1.;
    cps_1_2[20] = 0.; cps_1_2[21] = 2.; cps_1_2[22] = 0.; cps_1_2[23] = 2.;
    d->bezier_surface_data_1_2->create(dim, order_u_1_2, order_v_1_2, cps_1_2.data());

    d->bezier_surface_data_2_2 = new dtkRationalBezierSurfaceDataSisl();
    d->bezier_surface_data_2_2->create(QFINDTESTDATA("data/rational_bezier_surface_data_sisl_2_2").toStdString());
}

void dtkRationalBezierSurfaceDataSislTestCase::init(void)
{

}

void dtkRationalBezierSurfaceDataSislTestCase::testControlPoint(void)
{
    dtkContinuousGeometryPrimitives::Point_3 point(0., 0., 0.);
    d->bezier_surface_data_1_2->controlPoint(0, 1, point.data());
    QVERIFY(point[0] == 1.);
    QVERIFY(point[1] == 0.);
    QVERIFY(point[2] == 1.);

    d->bezier_surface_data_1_2->controlPoint(1, 1, point.data());
    QVERIFY(point[0] == 1.);
    QVERIFY(point[1] == 1.);
    QVERIFY(point[2] == 1.);

    d->bezier_surface_data_2_2->controlPoint(1, 1, point.data());
    QVERIFY(point[0] == 5.);
    QVERIFY(point[1] == 15.);
    QVERIFY(point[2] == 5.);
}

void dtkRationalBezierSurfaceDataSislTestCase::testWeight(void)
{
    double w = 0.;
    d->bezier_surface_data_1_2->weight(0, 1, &w);
    QVERIFY(w == 0.6);

    d->bezier_surface_data_2_2->weight(0, 1, &w);
    QVERIFY(w == 1.);
}

void dtkRationalBezierSurfaceDataSislTestCase::testUDegree(void)
{
    QVERIFY(d->bezier_surface_data_1_2->uDegree() == 1);
    QVERIFY(d->bezier_surface_data_2_2->uDegree() == 2);
}

void dtkRationalBezierSurfaceDataSislTestCase::testVDegree(void)
{
    QVERIFY(d->bezier_surface_data_1_2->vDegree() == 2);
    QVERIFY(d->bezier_surface_data_2_2->vDegree() == 2);
}

void dtkRationalBezierSurfaceDataSislTestCase::testSurfacePoint(void)
{
    dtkContinuousGeometryPrimitives::Point_3 point(0., 0., 0.);
    d->bezier_surface_data_1_2->evaluatePoint(0.25, 0.65, point.data());
    QVERIFY(point[0] > 0.342924 - 10e-5 && point[0] < 0.342924 + 10e-5);
    QVERIFY(point[1] > 0.440805 - 10e-5 && point[1] < 0.440805 + 10e-5);
    QVERIFY(point[2] > 1.23328 - 10e-5 && point[2] < 1.23328 + 10e-5);
}

//To ameliorate with dataSurfacePoint and several points to test
void dtkRationalBezierSurfaceDataSislTestCase::testSurfaceNormal(void)
{
    double *normal = new double[3];
    d->bezier_surface_data_1_2->evaluateNormal(0.5, 0.5, normal);
    delete[] normal;
}

void  dtkRationalBezierSurfaceDataSislTestCase::testAabb(void)
{
    double *box = new double[6];
    d->bezier_surface_data_1_2->aabb(box);
    QVERIFY(box[0] == 0.);
    QVERIFY(box[1] == 0.);
    QVERIFY(box[2] == 0.);
    QVERIFY(box[3] == 1.);
    QVERIFY(box[4] == 2.);
    QVERIFY(box[5] == 2.);
    delete[] box;
}

void dtkRationalBezierSurfaceDataSislTestCase::testPrintOutSurface(void)
{
    double point_cp[3];
    std::ofstream bezier_surface_data_on_cp_file("bezier_surface_data_on_cp.xyz");
    for (std::size_t i = 0; i <= d->bezier_surface_data_1_2->uDegree(); ++i) {
        for (std::size_t j = 0; j <= d->bezier_surface_data_1_2->vDegree(); ++j) {
            d->bezier_surface_data_1_2->controlPoint(i, j, &point_cp[0]);
            bezier_surface_data_on_cp_file << point_cp[0] << " " << point_cp[1] << " " << point_cp[2] << std::endl;
        }
    }
    bezier_surface_data_on_cp_file.close();

    std::ofstream bezier_surface_data_on_file("bezier_surface_data_on.xyz");
    double point[3];
    for (double i = 0.; i <= 1.; i += 0.01) {
        for (double j = 0.; j <= 1.; j += 0.01) {
            d->bezier_surface_data_1_2->evaluatePoint(i, j, &point[0]);
            bezier_surface_data_on_file << point[0] << " " << point[1] << " " << point[2] << std::endl;
        }
    }
    bezier_surface_data_on_file.close();
}

void dtkRationalBezierSurfaceDataSislTestCase::cleanup(void)
{

}

void dtkRationalBezierSurfaceDataSislTestCase::cleanupTestCase(void)
{
    delete d->bezier_surface_data_1_1;
    d->bezier_surface_data_1_1 = nullptr;
    delete d->bezier_surface_data_1_2;
    d->bezier_surface_data_1_2 = nullptr;
}

DTKTEST_MAIN_NOGUI(dtkRationalBezierSurfaceDataSislTest, dtkRationalBezierSurfaceDataSislTestCase)

//
// dtkRationalBezierSurfaceDataSislTest.cpp ends here
