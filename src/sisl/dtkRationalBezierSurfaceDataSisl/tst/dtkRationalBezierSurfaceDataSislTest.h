// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <QTest>

class dtkRationalBezierSurfaceDataSislTestCasePrivate;

class dtkRationalBezierSurfaceDataSislTestCase : public QObject
{
    Q_OBJECT

public:
    dtkRationalBezierSurfaceDataSislTestCase(void);
    ~dtkRationalBezierSurfaceDataSislTestCase(void);

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testControlPoint(void);
    void testWeight(void);
    void testUDegree(void);
    void testVDegree(void);
    void testSurfacePoint(void);
    void testSurfaceNormal(void);
    void testPrintOutSurface(void);
    void testAabb(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);

private:
    dtkRationalBezierSurfaceDataSislTestCasePrivate* d;
};

//
// dtkRationalBezierSurfaceDataSislTest.h ends here
