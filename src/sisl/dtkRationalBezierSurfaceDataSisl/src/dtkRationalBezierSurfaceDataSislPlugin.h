// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <QtCore>

#include <dtkContinuousGeometry>

#include <dtkAbstractRationalBezierSurfaceData>

class dtkRationalBezierSurfaceDataSislPlugin : public dtkAbstractRationalBezierSurfaceDataPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkAbstractRationalBezierSurfaceDataPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkRationalBezierSurfaceDataSislPlugin" FILE "dtkRationalBezierSurfaceDataSislPlugin.json")

public:
     dtkRationalBezierSurfaceDataSislPlugin(void) {}
    ~dtkRationalBezierSurfaceDataSislPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

//
// dtkRationalBezierSurfaceDataSislPlugin.h ends here
