// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkRationalBezierSurfaceDataSisl.h"

#include "dtkRationalBezierSurface.h"

#include <sisl.h>

#include <cassert>

/*!
  \class dtkRationalBezierSurfaceDataSisl
  \inmodule dtkPluginsContinuousGeometry
  \brief dtkRationalBezierSurfaceDataSisl is an openNURBS implementation of the concept dtkAbstractRationalBezierSurfaceData.

  The following lines of code show how to instanciate and use the class in pratice:
  \code
  dtkAbstractRationalBezierSurfaceData *nurbs_surface_data = dtkContinuousGeometry::abstractRationalBezierSurfaceData::pluginFactory().create("dtkRationalBezierSurfaceDataSisl");
  dtkRationalBezierSurface *rational_bezier_surface = new dtkRationalBezierSurface(rational_bezier_surface_data);
  rational_bezier_surface->create(...);
  \endcode
*/

/*! \fn dtkRationalBezierSurfaceDataSisl::dtkRationalBezierSurfaceDataSisl(void)
  Instanciates the structure.

  To add the content, see \l create(std::size_t dim, std::size_t order_u, std::size_t order_v, double* cps) const.
*/
dtkRationalBezierSurfaceDataSisl::dtkRationalBezierSurfaceDataSisl(void) : d(nullptr) {}

/*! \fn dtkRationalBezierSurfaceDataSisl::~dtkRationalBezierSurfaceDataSisl(void)
  Destroys the instance.
*/
dtkRationalBezierSurfaceDataSisl::~dtkRationalBezierSurfaceDataSisl(void)
{
    delete[] m_ecoef;
    freeSurf(d);
}

/*! \fn void dtkRationalBezierSurfaceDataSisl::create(std::size_t dim, std::size_t nb_cp_u, std::size_t nb_cp_v, double *cps) const
  Creates a NURBS surface by providing the required content.

  \a dim : dimension of the space in which lies the surface

  \a nb_cp_u : number of control points along the "u" direction

  \a nb_cp_v : number of control points along the "v" direction

  \a cps : array containing the weighted control points, secified as : [x_00, y_00, z_00, w_00, x_01, y_01, ...]

  \a cps are copied.

  The control points are given as such :

  v = 1      cps[1]      | cps[3]

        ---------------- | --------------

  v= 0       cps[0]      | cps[2]

              u=0           u=1
*/
void dtkRationalBezierSurfaceDataSisl::create(std::size_t dim, std::size_t order_u, std::size_t order_v, double* cps) const
{
    // /////////////////////////////////////////////////////////////////
    // Initializes the SISL NURBS surface
    // /////////////////////////////////////////////////////////////////
    dtkRationalBezierSurfaceDataSisl *self = const_cast< dtkRationalBezierSurfaceDataSisl * >(this);

    self->m_et1 = new double[2 * order_u];
    for(std::size_t i = 0; i < order_u; ++i) {
        self->m_et1[i] = 0;
    }
    for(std::size_t i = order_u; i < 2 * order_u; ++i) {
        self->m_et1[i] = 1;
    }
    self->m_et2 = new double[2 * order_v];
    for(std::size_t i = 0; i < order_v; ++i) {
        self->m_et2[i] = 0;
    }
    for(std::size_t i = order_v; i < 2 * order_v; ++i) {
        self->m_et2[i] = 1;
    }

    self->m_ecoef = new double[(dim + 1) * order_u * order_v];
    for(std::size_t i = 0; i < order_u; ++i) {
        for(std::size_t j = 0; j < order_v; ++j) {
            std::size_t new_coord = (dim + 1) * order_u * j + (dim + 1) * i;
            std::size_t old_coord = (dim + 1) * order_v * i + (dim + 1) * j;
            self->m_ecoef[new_coord] =     cps[old_coord] *     cps[old_coord + 3];
            self->m_ecoef[new_coord + 1] = cps[old_coord + 1] * cps[old_coord + 3];
            self->m_ecoef[new_coord + 2] = cps[old_coord + 2] * cps[old_coord + 3];
            self->m_ecoef[new_coord + 3] = cps[old_coord + 3];
        }
    }

    /* Copy Flag = 0 : Set internal pointer to input array and remember to free arrays*/
    self->d = newSurf(order_u, order_v, order_u, order_v, self->m_et1, self->m_et2, self->m_ecoef, 4, dim, 0);
    if(d == nullptr) {
        dtkFatal() << "The surface could not be created by SISL. " << Q_FUNC_INFO << ". Line : " << __LINE__;
    }
}

/*! \fn void dtkRationalBezierSurfaceDataSisl::create(std::string path) const
  Creates a NURBS surface by providing a path to a file storing the required information to create a rational Bezier surface.

  \a path : a valid path to the file containing the information regarding a given rational Bezier surface
*/
void dtkRationalBezierSurfaceDataSisl::create(std::string path) const
{
    QFile file(QString::fromStdString(path));
    QIODevice *in = &file;

    QIODevice::OpenMode mode = QIODevice::ReadOnly;
    mode |= QIODevice::Text;

    // to avoid troubles with floats separators ('.' and not ',')
    QLocale::setDefault(QLocale::c());
#if defined (Q_OS_UNIX) && !defined(Q_OS_MAC)
    setlocale(LC_NUMERIC, "C");
#endif

    if (!in->open(mode)) {
        dtkFatal() << Q_FUNC_INFO << "The file could not be found, please verify the path";
        return;
    }

    QString line = in->readLine().trimmed();
    //Run tests on the file to check its validity TODO more
    if ( line != "#dtkRationalBezierSurfaceDataSisl"){
        dtkFatal() << Q_FUNC_INFO << "file not in a dtkRationalBezierSurfaceDataSisl format.";
        return;
    }
    std::size_t dim = 3;
    QRegExp re = QRegExp("\\s+");

    line = in->readLine().trimmed();
    QStringList vals = line.split(re);
    std::size_t order_u = vals[1].toInt() + 1;
    line = in->readLine().trimmed();
    vals = line.split(re);
    std::size_t order_v = vals[1].toInt() + 1;

    dtkRationalBezierSurfaceDataSisl *self = const_cast< dtkRationalBezierSurfaceDataSisl * >(this);

    self->m_et1 = new double[2 * order_u];
    for(std::size_t i = 0; i < order_u; ++i) {
        self->m_et1[i] = 0;
    }
    for(std::size_t i = order_u; i < 2 * order_u; ++i) {
        self->m_et1[i] = 1;
    }
    self->m_et2 = new double[2 * order_v];
    for(std::size_t i = 0; i < order_v; ++i) {
        self->m_et2[i] = 0;
    }
    for(std::size_t i = order_v; i < 2 * order_v; ++i) {
        self->m_et2[i] = 1;
    }

    self->m_ecoef = new double[(dim + 1) * order_u * order_v];

    for(std::size_t i = 0; i < order_u; ++i) {
        for(std::size_t j = 0; j < order_v;) {
            line = in->readLine().trimmed();
            if (line.startsWith("#")) {
                continue;
            }
            std::istringstream lin(line.toStdString());
            double x, y, z, w;
            lin >> x >> y >> z >> w;       //now read the whitespace-separated floats
            std::size_t new_coord = (dim + 1) * order_u * j + (dim + 1) * i;
            self->m_ecoef[new_coord] =     x * w;
            self->m_ecoef[new_coord + 1] = y * w;
            self->m_ecoef[new_coord + 2] = z * w;
            self->m_ecoef[new_coord + 3] = w;
            ++j;
        }
    }

    /* Copy Flag = 0 : Set internal pointer to input array and remember to free arrays*/
    self->d = newSurf(order_u, order_v, order_u, order_v, self->m_et1, self->m_et2, self->m_ecoef, 4, dim, 0);
    if(d == nullptr) {
        dtkFatal() << "The surface could not be created by SISL. " << Q_FUNC_INFO << ". Line : " << __LINE__;
    }
}

/*! \fn dtkRationalBezierSurfaceDataSisl::uDegree(void) const
  Returns the degree of the surface in the "u" direction.
*/
std::size_t dtkRationalBezierSurfaceDataSisl::uDegree(void) const
{
    return d->ik1 - 1;
}

/*! \fn dtkRationalBezierSurfaceDataSisl::vDegree(void) const
  Returns the degree of the surface in the "v" direction.
*/
std::size_t dtkRationalBezierSurfaceDataSisl::vDegree(void) const
{
    return d->ik2 - 1;
}

/*! \fn dtkRationalBezierSurfaceDataSisl::controlPoint(std::size_t i, std::size_t j, double *r_cp) const
  Writes in \a r_cp the \a i th, \a j th weighted control point coordinates[xij, yij, zij].

  \a i : index of the weighted control point along the "u" direction

  \a j : index of the weighted control point along the "v" direction

  \a r_cp : array of size 3 to store the weighted control point coordinates[xij, yij, zij]
 */
void dtkRationalBezierSurfaceDataSisl::controlPoint(std::size_t i, std::size_t j, double* r_cp) const
{
    for (std::size_t k = 0; k < std::size_t(d->idim); ++k) {
        r_cp[k] = d->ecoef[(j * d->in1 + i) * (d->idim) + k];
    }
}

/*! \fn dtkRationalBezierSurfaceDataSisl::weight(std::size_t i, std::size_t j, double *r_w) const
  Writes in \a r_w the \a i th, \a j th weighted control point weight wij.

  \a i : index of the weighted control point along the "u" direction

  \a j : index of the weighted control point along the "v" direction

  \a r_w : array of size 1 to store the weighted control point weight wij
*/
void dtkRationalBezierSurfaceDataSisl::weight(std::size_t i, std::size_t j, double* r_w) const
{
    *r_w = d->rcoef[(j * d->in1 + i) * (d->idim + 1) + d->idim];
}

/*! \fn dtkRationalBezierSurfaceDataSisl::isDegenerate(void) const
  Returns true if the rational Bezier surface is degenerate : i.e. a line of control points coalesce, else returns false.
*/
bool dtkRationalBezierSurfaceDataSisl::isDegenerate(void) const
{
    // // ///////////////////////////////////////////////////////////////////
    // // To checks that a patch is degenerate, one must make sure that all control points  on a line do not colllapse to one single point
    // //The algorithm checks each possible line and make sure at least 2 different control points are on it
    // // ///////////////////////////////////////////////////////////////////
    // bool isLineDegenerate = false;
    // // ///////////////////////////////////////////////////////////////////
    // // Checks in the first direction (from v = 0 -> v=1)
    // // ///////////////////////////////////////////////////////////////////
    // for (std::size_t i = 0; i < std::size_t(d->Degree(0) + 1); ++i) {
    //     ON_3dPoint pointA;
    //     d->GetCV(i, 0, pointA);
    //     isLineDegenerate = true;
    //     for (std::size_t k = 1; k < std::size_t(d->Degree(1) + 1); ++k) {
    //         ON_3dPoint pointB;
    //         d->GetCV(i, k, pointB);
    //         if (pointA != pointB) {
    //             isLineDegenerate = false;
    //             break;
    //         }
    //     }
    //     if (isLineDegenerate  == true) {
    //         return true;
    //     }
    // }
    // // ///////////////////////////////////////////////////////////////////
    // // Checks it the second direction (from u = 0 -> u = 1)
    // // ///////////////////////////////////////////////////////////////////
    // for (std::size_t i = 0; i < std::size_t(d->Degree(1) + 1); ++i) {
    //     ON_3dPoint pointA;
    //     d->GetCV(0, i, pointA);
    //     isLineDegenerate = true;
    //     for (std::size_t k = 1; k < std::size_t(d->Degree(0) + 1); ++k) {
    //         ON_3dPoint pointB;
    //         d->GetCV(k, i, pointB);
    //         if (pointA != pointB) {
    //             isLineDegenerate = false;
    //             break;
    //         }
    //     }
    //     if (isLineDegenerate  == true) {
    //         return true;
    //     }
    // }
    // return false;

    // // ///////////////////////////////////////////////////////////////////
    // // TODO Checks that the diagonal are not collapsed
    // // ///////////////////////////////////////////////////////////////////
    dtkFatal() << "Does nothing";
    return false;
}

/*! \fn dtkRationalBezierSurfaceDataSisl::degenerateLocations(std::list< dtkContinuousGeometryPrimitives::Point_3 >& r_degenerate_locations) const
  Returns true if the rational Bezier surface is degenerate : i.e. a line of control points coalesce and adds in \a r_degenerate_locations the locations at which there are degeneracies, else returns false.
*/
bool dtkRationalBezierSurfaceDataSisl::degenerateLocations(std::list< dtkContinuousGeometryPrimitives::Point_3 >&) const
{
    // // ///////////////////////////////////////////////////////////////////
    // // Checks in the first direction (from v = 0 -> v=1)
    // // ///////////////////////////////////////////////////////////////////
    // bool isLineDegenerate = true;
    // for (std::size_t i = 0; i < std::size_t(d->Degree(0) + 1); ++i) {
    //     ON_3dPoint pointA;
    //     d->GetCV(i, 0, pointA);
    //     isLineDegenerate = true;
    //     for (std::size_t k = 1; k < std::size_t(d->Degree(1) + 1); ++k) {
    //         ON_3dPoint pointB;
    //         d->GetCV(i, k, pointB);
    //         if (pointA != pointB) {
    //             isLineDegenerate = false;
    //             break;
    //         }
    //     }
    //     if (isLineDegenerate  == true) {
    //         r_degenerate_locations.push_back(dtkContinuousGeometryPrimitives::Point_3(pointA[0], pointA[1], pointA[2]));
    //     }
    // }
    // // ///////////////////////////////////////////////////////////////////
    // // Checks it the second direction (from u = 0 -> u = 1)
    // // ///////////////////////////////////////////////////////////////////
    // for (std::size_t i = 0; i < std::size_t(d->Degree(1) + 1); ++i) {
    //     ON_3dPoint pointA;
    //     d->GetCV(0, i, pointA);
    //     isLineDegenerate = true;
    //     for (std::size_t k = 1; k < std::size_t(d->Degree(0) + 1); ++k) {
    //         ON_3dPoint pointB;
    //         d->GetCV(k, i, pointB);
    //         if (pointA != pointB) {
    //             isLineDegenerate = false;
    //             break;
    //         }
    //     }
    //     if (isLineDegenerate  == true) {
    //         r_degenerate_locations.push_back(dtkContinuousGeometryPrimitives::Point_3(pointA[0], pointA[1], pointA[2]));
    //     }
    // }

    // if (r_degenerate_locations.size() > 0) {
    //     return true;
    // } else {
    //     return false;
    // }
    dtkFatal() << "Does nothing";
    return false;
}

/*! \fn dtkRationalBezierSurfaceDataSisl::evaluatePoint(double p_u, double p_v, double *r_point) const
  Writes in \a r_point the evaluation at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_point : array of size 3 to store the result of the evaluation
*/
void dtkRationalBezierSurfaceDataSisl::evaluatePoint(double p_u, double p_v, double *r_point) const
{
    double par_values[2];
    double normal[3];
    par_values[0] = p_u;
    par_values[1] = p_v;
    int left_knot_1;
    int left_knot_2;
    int der = 0;
    int stat = 0;
    s1421(d, der, par_values, &left_knot_1, &left_knot_2, r_point, normal, &stat);
}

/*! \fn dtkRationalBezierSurfaceDataSisl::evaluateNormal(double p_u, double p_v, double *r_normal) const
  Writes in \a r_normal the normal evaluation at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_normal : array of size 3 to store the result of the normal evaluation
*/
void dtkRationalBezierSurfaceDataSisl::evaluateNormal(double p_u, double p_v, double *r_normal) const
{
    double par_values[2];
    double derive[9];
    par_values[0] = p_u;
    par_values[1] = p_v;
    int left_knot_1;
    int left_knot_2;
    int der = 1;
    int stat;
    s1421(d, der, par_values, &left_knot_1, &left_knot_2, derive, r_normal, &stat);
}

/*! \fn dtkRationalBezierSurfaceDataSisl::evaluate1stDer(double p_u, double p_v, double *r_point, double *r_u_deriv, double *r_v_deriv) const
  Writes in \a r_point the evaluation, in \a r_u_deriv the first derivative in "u" direction, in \a r_v_deriv the first derivative in the "v" direction at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_point : array of size 3 to store the result of the evaluation

  \a r_u_deriv : array of size 3 to store the result of the first derivative evaluation in the "u" direction

  \a r_v_deriv : array of size 3 to store the result of the first derivative evaluation in the "v" direction
*/
void dtkRationalBezierSurfaceDataSisl::evaluate1stDer(double p_u, double p_v, double* r_point, double* r_u_deriv, double* r_v_deriv) const
{
    double par_values[2];
    double derive[9];
    double normal[3];
    par_values[0] = p_u;
    par_values[1] = p_v;
    int left_knot_1;
    int left_knot_2;
    int der = 1;
    int stat;
    s1421(d, der, par_values, &left_knot_1, &left_knot_2, derive, normal, &stat);
    r_point[0] = derive[0];
    r_point[1] = derive[1];
    r_point[2] = derive[2];

    r_u_deriv[0] = derive[3];
    r_u_deriv[1] = derive[4];
    r_u_deriv[2] = derive[5];

    r_v_deriv[0] = derive[6];
    r_v_deriv[1] = derive[7];
    r_v_deriv[2] = derive[8];
}

/*! \fn dtkRationalBezierSurfaceDataSisl::evaluate1stDer(double p_u, double p_v, double *r_u_deriv, double *r_v_deriv) const
  Writes in \a r_u_deriv the first derivative in "u" direction and in \a r_v_deriv the first derivative in the "v" direction at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_u_deriv : array of size 3 to store the result of the first derivative evaluation in the "u" direction

  \a r_v_deriv : array of size 3 to store the result of the first derivative evaluation in the "v" direction
*/
void dtkRationalBezierSurfaceDataSisl::evaluate1stDer(double p_u, double p_v, double* r_u_deriv, double* r_v_deriv) const
{
    double par_values[2];
    double derive[9];
    double normal[3];
    par_values[0] = p_u;
    par_values[1] = p_v;
    int left_knot_1;
    int left_knot_2;
    int der = 1;
    int stat;
    s1421(d, der, par_values, &left_knot_1, &left_knot_2, derive, normal, &stat);
    r_u_deriv[0] = derive[0];
    r_u_deriv[1] = derive[1];
    r_u_deriv[2] = derive[2];

    r_u_deriv[0] = derive[3];
    r_u_deriv[1] = derive[4];
    r_u_deriv[2] = derive[5];

    r_v_deriv[0] = derive[6];
    r_v_deriv[1] = derive[7];
    r_v_deriv[2] = derive[8];
}

/*! \fn dtkRationalBezierSurfaceDataSisl::evaluatePointBlossom(const double* p_uis, const double* p_vis, double *r_point, double *r_weight) const
  Stores in \a r_point and \a r_weight the result of the blossom evaluation at \a p_uis, \a p_vis values.

  \a p_uis : "u" values for blossom evaluation

  \a p_vis : "v" values for blossom evaluation

  \a r_point : array of size 3 to slore the point result of the blossom evaluation

  \a r_weight : array of size 1 to slore the weight result of the blossom evaluation
*/
void dtkRationalBezierSurfaceDataSisl::evaluatePointBlossom(const double*, const double*, double*, double*) const
{
    dtkFatal() << "Not doing anything atm";
}

/*! \fn dtkRationalBezierSurfaceDataSisl::split(dtkRationalBezierSurface *r_split_surface, double *p_splitting_parameters) const
  Creates \a r_split_surface as the part of rational Bezier surface lying inside the bounding box of parameters \a p_splitting_parameters.

  \a r_split_surface : a pointer to a valid dtkRationalBezierSurface with data not initialized, the part of the initial surface restricted to the bouding box of parameters given in \a p_splitting_parameters will be added as data

  \a p_splitting_parameters : array of size 4, storing the parameters defining the restriction in the parameter space of the rational Bezier surface, given as [u_0, v_0, u_1, v_1]
 */
void dtkRationalBezierSurfaceDataSisl::split(dtkRationalBezierSurface *, double *) const
{
    dtkFatal() << "Not doing anything atm";
}

/*! \fn dtkRationalBezierSurfaceDataSisl::aabb(double* r_aabb) const
  Writes in \a r_aabb the [xmin, ymin, zmin, xmax, ymax, zmax] coordinates.

  \a r_aabb : array of size 6 to store the limits coordinates
 */
void dtkRationalBezierSurfaceDataSisl::aabb(double* r_aabb) const
{
    int jstat = 0;
    double *emax = new double[3];
    double *emin = new double[3];
    s1989(d, &emax, &emin, &jstat);
    if(jstat < 0 ) {
        dtkFatal() << "The bouding box of the NURBS surface could not be recovered. " << Q_FUNC_INFO << " .Line : " << __LINE__;
    }
    r_aabb[0] = emin[0];
    r_aabb[1] = emin[1];
    r_aabb[2] = emin[2];
    r_aabb[3] = emax[0];
    r_aabb[4] = emax[1];
    r_aabb[5] = emax[2];
    if(jstat < 0) {
        dtkFatal() << "The bounding box could not be computed. " << Q_FUNC_INFO << ". LINE : " << __LINE__;
    }
}

/*! \fn dtkRationalBezierSurfaceDataSisl::extendedAabb(double *r_aabb, double factor) const
  Writes in \a r_aabb the [xmin, ymin, zmin, xmax, ymax, zmax] coordinates of an extened bounding box of factor \a factor. The extension factor extends the diagonal.

  \a r_aabb : array of size 6 to store the extended limits coordinates

  \a factor : factor used to lengthen the diagonal of the axis aligned bounding box
*/
void dtkRationalBezierSurfaceDataSisl::extendedAabb(double*, double) const
{
    dtkFatal() << "Does nothing, not implemented";
}

/*! \fn dtkRationalBezierSurfaceDataSisl::sislRationalBezierSurface(void)
  Not available in the dtkAbstractRationalBezierSurfaceData.

  Returns the underlying sisl rational Bezier surface..
 */
const SISLSurf *dtkRationalBezierSurfaceDataSisl::sislRationalBezierSurface(void)
{
    return d;
}

/*! \fn dtkRationalBezierSurfaceDataSisl::clone(void) const
   Clone.
*/
dtkRationalBezierSurfaceDataSisl* dtkRationalBezierSurfaceDataSisl::clone(void) const
{
    return new dtkRationalBezierSurfaceDataSisl(*this);
}

#include <iomanip>
void dtkRationalBezierSurfaceDataSisl::print(std::ostream& stream) const
{
    stream << "#dtkRationalBezierSurfaceDataSisl" << std::endl;
    stream << "u_degree " << d->ik1 - 1 << std::endl;
    stream << "v_degree " << d->ik2 - 1 << std::endl;

    for(auto i = 0; i < d->ik1; ++i) {
        for(auto j = 0; j < d->ik2; ++j) {
            std::size_t index = (j * d->in1 + i) * 3;
            stream << d->ecoef[index] << " " << d->ecoef[index + 1] << " " << d->ecoef[index + 2] << " " << d->rcoef[(j * d->in1 + i) * (d->idim + 1) + d->idim] << std::setprecision(std::numeric_limits< double >::digits10 + 1) << std::endl;
        }
    }
    stream << "#dtkRationalBezierSurfaceDataSisl" << std::endl;
}

//
// dtkRationalBezierSurfaceDataSisl.cpp ends here
