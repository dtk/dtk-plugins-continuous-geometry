// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#include "dtkRationalBezierSurfaceDataSisl.h"
#include "dtkRationalBezierSurfaceDataSislPlugin.h"

#include <dtkCore>
#include <dtkContinuousGeometry>

// ///////////////////////////////////////////////////////////////////
// dtkRationalBezierSurfaceDataSislPlugin
// ///////////////////////////////////////////////////////////////////

void dtkRationalBezierSurfaceDataSislPlugin::initialize(void)
{
    dtkContinuousGeometry::abstractRationalBezierSurfaceData::pluginFactory().record("dtkRationalBezierSurfaceDataSisl", dtkRationalBezierSurfaceDataSislCreator);
}

void dtkRationalBezierSurfaceDataSislPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkRationalBezierSurfaceDataSisl)

//
// dtkRationalBezierSurfaceDataSislPlugin.cpp ends here
