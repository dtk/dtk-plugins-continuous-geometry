// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <QTest>

class dtkNurbsSurfaceDataSislTestCasePrivate;

class dtkNurbsSurfaceDataSislTestCase : public QObject
{
    Q_OBJECT

public:
    dtkNurbsSurfaceDataSislTestCase(void);
    ~dtkNurbsSurfaceDataSislTestCase(void);

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testControlPoint(void);
    void testWeight(void);
    void testUDegree(void);
    void testVDegree(void);
    void testUKnots(void);
    void testVKnots(void);
    void testSurfacePoint(void);
    void testSurfaceNormal(void);
    void testPrintOutSurface(void);
    void testAabb(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);

private:
    dtkNurbsSurfaceDataSislTestCasePrivate* d;
};

//
// dtkNurbsSurfaceDataSislTest.h ends here
