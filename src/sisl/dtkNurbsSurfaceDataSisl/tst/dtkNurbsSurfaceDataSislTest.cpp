// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkNurbsSurfaceDataSislTest.h"

#include "dtkNurbsSurfaceDataSisl.h"

#include <dtkContinuousGeometryUtils>

#include <dtkTest>

// ///////////////////////////////////////////////////////////////////
// D-Pointer implementation
// ///////////////////////////////////////////////////////////////////

class dtkNurbsSurfaceDataSislTestCasePrivate{
public:
    dtkNurbsSurfaceDataSislTestCasePrivate();
    ~dtkNurbsSurfaceDataSislTestCasePrivate();
public:
    dtkNurbsSurfaceDataSisl *nurbs_surface_data_1_2;
    dtkNurbsSurfaceDataSisl *nurbs_surface_data_2_2;
};

dtkNurbsSurfaceDataSislTestCasePrivate::dtkNurbsSurfaceDataSislTestCasePrivate() : nurbs_surface_data_1_2(nullptr), nurbs_surface_data_2_2(nullptr) {}
dtkNurbsSurfaceDataSislTestCasePrivate::~dtkNurbsSurfaceDataSislTestCasePrivate() {
    if(nurbs_surface_data_1_2 != nullptr) {
        delete nurbs_surface_data_1_2;
        nurbs_surface_data_1_2 = nullptr;
    }
    if(nurbs_surface_data_2_2 != nullptr) {
        delete nurbs_surface_data_2_2;
        nurbs_surface_data_2_2 = nullptr;
    }
}

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkNurbsSurfaceDataSislTestCase::dtkNurbsSurfaceDataSislTestCase(void):d(new dtkNurbsSurfaceDataSislTestCasePrivate())
{
    dtkLogger::instance().attachConsole();
    dtkLogger::instance().setLevel(dtk::LogLevel::Trace);
}

dtkNurbsSurfaceDataSislTestCase::~dtkNurbsSurfaceDataSislTestCase(void)
{
    delete d;
}

void dtkNurbsSurfaceDataSislTestCase::initTestCase(void)
{
    std::size_t dim = 3;

    // ///////////////////////////////////////////////////////////////////

    std::size_t nb_cp_u_1_2 = 4;
    std::size_t nb_cp_v_1_2 = 5;
    std::size_t order_u_1_2 = 2;
    std::size_t order_v_1_2 = 3;

    double *knots_u_1_2 = new double[nb_cp_u_1_2 + order_u_1_2 - 2];
    /* 0. */ knots_u_1_2[0] = 0.; knots_u_1_2[1] = 1.2; knots_u_1_2[2] = 2.7; knots_u_1_2[3] = 3.; /* 3. */
    double *knots_v_1_2 = new double[nb_cp_v_1_2 + order_v_1_2 - 2];
    /* 0. */ knots_v_1_2[0] = 0.;knots_v_1_2[1] = 0.; knots_v_1_2[2] = 0.66; knots_v_1_2[3] = 1.33; knots_v_1_2[4] = 2.; knots_v_1_2[5] = 2.; /* 2. */

    double *cps_1_2 = new double[(dim + 1) * nb_cp_u_1_2 * nb_cp_v_1_2];
    //x_00,             y_00,            z_00,               w_00
    cps_1_2[0] =  0.;   cps_1_2[1] = 0.; cps_1_2[2] =   0.;  cps_1_2[3] =  1.;
    //x_01,             y_01,            z_01,               w_01
    cps_1_2[4] =  1.;   cps_1_2[5] = 0.; cps_1_2[6] =   1.;  cps_1_2[7] =  1.5;
    cps_1_2[8] =  2.;   cps_1_2[9] = 0.; cps_1_2[10] =  2.; cps_1_2[11] =  3.;
    cps_1_2[12] = 3.;  cps_1_2[13] = 0.;  cps_1_2[14] = 3.; cps_1_2[15] =  4.;
    //x_04,             y_04,            z_04,               w_04
    cps_1_2[16] = 4.;  cps_1_2[17] = 0.;  cps_1_2[18] = 3.; cps_1_2[19] =  3.;
    //x_10,             y_10,            z_10,               w_10
    cps_1_2[20] = 0.;  cps_1_2[21] = 1.; cps_1_2[22] =  0.;  cps_1_2[23] = 0.5;
    cps_1_2[24] = 1.;  cps_1_2[25] = 1.; cps_1_2[26] =  0.;  cps_1_2[27] = 2.;
    cps_1_2[28] = 2.;  cps_1_2[29] = 1.; cps_1_2[30] =  0.; cps_1_2[31] =  0.5;
    cps_1_2[32] = 3.;  cps_1_2[33] = 1.;  cps_1_2[34] = 0.; cps_1_2[35] =  1.;
    //x_14,             y_14,            z_14,               w_14
    cps_1_2[36] = 4.;  cps_1_2[37] = 2.; cps_1_2[38] =  0.;  cps_1_2[39] = 1.;
    //x_20,             y_20,            z_20,               w_20
    cps_1_2[40] = 0.;  cps_1_2[41] = 2.; cps_1_2[42] =  0.;  cps_1_2[43] = 1.;
    cps_1_2[44] = 1.;  cps_1_2[45] = 2.; cps_1_2[46] =  0.;  cps_1_2[47] = 1.;
    cps_1_2[48] = 2.;  cps_1_2[49] = 2.; cps_1_2[50] =  0.; cps_1_2[51] =  1.;
    cps_1_2[52] = 3.;  cps_1_2[53] = 2.;  cps_1_2[54] = 0.; cps_1_2[55] =  1.;
    cps_1_2[56] = 4.;  cps_1_2[57] = 2.;  cps_1_2[58] = 0.; cps_1_2[59] =  1.;
    cps_1_2[60] = 0.;  cps_1_2[61] = 3.; cps_1_2[62] =  0.;  cps_1_2[63] = 1.;
    cps_1_2[64] = 1.;  cps_1_2[65] = 3.; cps_1_2[66] =  0.;  cps_1_2[67] = 1.;
    cps_1_2[68] = 2.;  cps_1_2[69] = 3.; cps_1_2[70] =  0.; cps_1_2[71] =  1.;
    cps_1_2[72] = 3.;  cps_1_2[73] = 3.;  cps_1_2[74] = 0.; cps_1_2[75] =  1.;
    //x_34,             y_34,            z_34,               w_34
    cps_1_2[76] = 4.;  cps_1_2[77] = 3.;  cps_1_2[78] = 0.; cps_1_2[79] =  1.;

    d->nurbs_surface_data_1_2 = new dtkNurbsSurfaceDataSisl();
    d->nurbs_surface_data_1_2->create(dim, nb_cp_u_1_2, nb_cp_v_1_2, order_u_1_2, order_v_1_2, knots_u_1_2, knots_v_1_2, cps_1_2);

    delete[] knots_u_1_2;
    delete[] knots_v_1_2;
    delete[] cps_1_2;

    // ///////////////////////////////////////////////////////////////

    std::size_t nb_cp_u_2_2 = 4;
    std::size_t nb_cp_v_2_2 = 4;
    std::size_t order_u_2_2 = 3;
    std::size_t order_v_2_2 = 3;

    double *knots_u_2_2 = new double[nb_cp_u_2_2 + order_u_2_2 - 2];
    /* 0. */ knots_u_2_2[0] = 0.; knots_u_2_2[1] = 0.; knots_u_2_2[2] = 1.; knots_u_2_2[3] = 2.; knots_u_2_2[4] = 2.; /* 2. */
    double *knots_v_2_2 = new double[nb_cp_v_2_2 + order_v_2_2 - 2];
    /* 0. */ knots_v_2_2[0] = 0.; knots_v_2_2[1] = 0.; knots_v_2_2[2] = 1.; knots_v_2_2[3] = 2.; knots_v_2_2[4] = 2.; /* 2. */

    double *cps_2_2 = new double[(dim + 1) * nb_cp_u_2_2 * nb_cp_v_2_2];
    //x_00,             y_00,            z_00,               w_00
    cps_2_2[0] = 0.;   cps_2_2[1] = 0.; cps_2_2[2] = 0.;  cps_2_2[3] = 1.;
    //x_01,             y_01,            z_01,               w_01
    cps_2_2[4] = 1.;   cps_2_2[5] = 0.; cps_2_2[6] = 1.;  cps_2_2[7] = 1.;
    cps_2_2[8] = 2.;   cps_2_2[9] = 0.; cps_2_2[10] = 1.; cps_2_2[11] = 1.;
    //x_03,             y_03,            z_03,               w_03
    cps_2_2[12] = 3.;  cps_2_2[13] = 0.;  cps_2_2[14] = 0.; cps_2_2[15] = 1.;
    //x_10,             y_10,            z_10,               w_10
    cps_2_2[16] = 0.;  cps_2_2[17] = 1.; cps_2_2[18] = 1.;  cps_2_2[19] = 1.;
    cps_2_2[20] = 1.;  cps_2_2[21] = 1.; cps_2_2[22] = 1.;  cps_2_2[23] = 1.;
    cps_2_2[24] = 2.;  cps_2_2[25] = 1.; cps_2_2[26] = 1.; cps_2_2[27] = 1.;
    cps_2_2[28] = 3.;  cps_2_2[29] = 1.;  cps_2_2[30] = 1.; cps_2_2[31] = 1.;
    cps_2_2[32] = 0.;  cps_2_2[33] = 2.; cps_2_2[34] = 1.;  cps_2_2[35] = 1.;
    cps_2_2[36] = 1.;  cps_2_2[37] = 2.; cps_2_2[38] = 1.;  cps_2_2[39] = 1.;
    cps_2_2[40] = 2.;  cps_2_2[41] = 2.; cps_2_2[42] = 1.; cps_2_2[43] = 1.;
    cps_2_2[44] = 3.;  cps_2_2[45] = 2.;  cps_2_2[46] = 1.; cps_2_2[47] = 1.;
    cps_2_2[48] = 0.;  cps_2_2[49] = 3.; cps_2_2[50] = 0.;  cps_2_2[51] = 1.;
    cps_2_2[52] = 1.;  cps_2_2[53] = 3.; cps_2_2[54] = 1.;  cps_2_2[55] = 1.;
    cps_2_2[56] = 2.;  cps_2_2[57] = 3.; cps_2_2[58] = 1.; cps_2_2[59] = 1.;
    cps_2_2[60] = 3.;  cps_2_2[61] = 3.;  cps_2_2[62] = 0.; cps_2_2[63] = 1.;

    d->nurbs_surface_data_2_2 = new dtkNurbsSurfaceDataSisl();
    d->nurbs_surface_data_2_2->create(dim, nb_cp_u_2_2, nb_cp_v_2_2, order_u_2_2, order_v_2_2, knots_u_2_2, knots_v_2_2, cps_2_2);

    delete[] knots_u_2_2;
    delete[] knots_v_2_2;
    delete[] cps_2_2;
}

void dtkNurbsSurfaceDataSislTestCase::init(void)
{
}

void dtkNurbsSurfaceDataSislTestCase::testControlPoint(void)
{
    dtkContinuousGeometryPrimitives::Point_3 point(0., 0., 0.);
    d->nurbs_surface_data_1_2->controlPoint(1, 0, point.data());
    QVERIFY(point[0] == 0.);
    QVERIFY(point[1] == 1.);
    QVERIFY(point[2] == 0.);
    d->nurbs_surface_data_1_2->controlPoint(0, 3, point.data());
    QVERIFY(point[0] == 3.);
    QVERIFY(point[1] == 0.);
    QVERIFY(point[2] == 3.);
    d->nurbs_surface_data_2_2->controlPoint(1, 0, point.data());
    QVERIFY(point[0] == 0.);
    QVERIFY(point[1] == 1.);
    QVERIFY(point[2] == 1.);
    d->nurbs_surface_data_2_2->controlPoint(0, 3, point.data());
    QVERIFY(point[0] == 3.);
    QVERIFY(point[1] == 0.);
    QVERIFY(point[2] == 0.);
}

void dtkNurbsSurfaceDataSislTestCase::testWeight(void)
{
    double w = 0.;
    d->nurbs_surface_data_1_2->weight(1, 0, &w);
    QVERIFY(w == 0.5);
    d->nurbs_surface_data_1_2->weight(0, 3, &w);
    QVERIFY(w == 4.);
    d->nurbs_surface_data_2_2->weight(1, 0, &w);
    QVERIFY(w == 1.);
    d->nurbs_surface_data_2_2->weight(0, 3, &w);
    QVERIFY(w == 1.);
}

void dtkNurbsSurfaceDataSislTestCase::testUDegree(void)
{
    QVERIFY(d->nurbs_surface_data_1_2->uDegree() == 1);
    QVERIFY(d->nurbs_surface_data_2_2->uDegree() == 2);
}

void dtkNurbsSurfaceDataSislTestCase::testVDegree(void)
{
    QVERIFY(d->nurbs_surface_data_1_2->vDegree() == 2);
    QVERIFY(d->nurbs_surface_data_2_2->vDegree() == 2);
}

void dtkNurbsSurfaceDataSislTestCase::testUKnots(void)
{
    std::vector<double> u_knots_1_2(d->nurbs_surface_data_1_2->uNbCps() + d->nurbs_surface_data_1_2->uDegree() - 1);
    QVERIFY(d->nurbs_surface_data_1_2->uNbCps() + d->nurbs_surface_data_1_2->uDegree() - 1 == 4);
    d->nurbs_surface_data_1_2->uKnots(u_knots_1_2.data());
    QVERIFY(u_knots_1_2[0] == 0.);
    QVERIFY(u_knots_1_2[1] == 1.2);
    QVERIFY(u_knots_1_2[2] == 2.7);
    QVERIFY(u_knots_1_2[3] == 3.);

    QVERIFY(d->nurbs_surface_data_1_2->uKnots()[0] == 0.);
    QVERIFY(d->nurbs_surface_data_1_2->uKnots()[1] == 1.2);
    QVERIFY(d->nurbs_surface_data_1_2->uKnots()[2] == 2.7);
    QVERIFY(d->nurbs_surface_data_1_2->uKnots()[3] == 3.);

    std::vector<double> u_knots_2_2(d->nurbs_surface_data_2_2->uNbCps() + d->nurbs_surface_data_2_2->uDegree() - 1);
    QVERIFY(d->nurbs_surface_data_2_2->uNbCps() + d->nurbs_surface_data_2_2->uDegree() - 1 == 5);
    d->nurbs_surface_data_2_2->uKnots(u_knots_2_2.data());
    QVERIFY(u_knots_2_2[0] == 0.);
    QVERIFY(u_knots_2_2[1] == 0.);
    QVERIFY(u_knots_2_2[2] == 1.);
    QVERIFY(u_knots_2_2[3] == 2.);
    QVERIFY(u_knots_2_2[4] == 2.);

    QVERIFY(d->nurbs_surface_data_2_2->uKnots()[0] == 0.);
    QVERIFY(d->nurbs_surface_data_2_2->uKnots()[1] == 0.);
    QVERIFY(d->nurbs_surface_data_2_2->uKnots()[2] == 1.);
    QVERIFY(d->nurbs_surface_data_2_2->uKnots()[3] == 2);
    QVERIFY(d->nurbs_surface_data_2_2->uKnots()[4] == 2);
}

void dtkNurbsSurfaceDataSislTestCase::testVKnots(void)
{
    std::vector<double> v_knots(d->nurbs_surface_data_1_2->vNbCps() + d->nurbs_surface_data_1_2->vDegree() - 1);
    QVERIFY(d->nurbs_surface_data_1_2->vNbCps() + d->nurbs_surface_data_1_2->vDegree() - 1 == 6);
    d->nurbs_surface_data_1_2->vKnots(v_knots.data());
    QVERIFY(v_knots[0] == 0.);
    QVERIFY(v_knots[1] == 0.);
    QVERIFY(v_knots[2] == 0.66);
    QVERIFY(v_knots[3] == 1.33);
    QVERIFY(v_knots[4] == 2.);
    QVERIFY(v_knots[5] == 2.);

    QVERIFY(d->nurbs_surface_data_1_2->vKnots()[0] == 0.);
    QVERIFY(d->nurbs_surface_data_1_2->vKnots()[1] == 0.);
    QVERIFY(d->nurbs_surface_data_1_2->vKnots()[2] == 0.66);
    QVERIFY(d->nurbs_surface_data_1_2->vKnots()[3] == 1.33);
    QVERIFY(d->nurbs_surface_data_1_2->vKnots()[4] == 2.);
    QVERIFY(d->nurbs_surface_data_1_2->vKnots()[5] == 2.);

    std::vector<double> v_knots_2_2(d->nurbs_surface_data_2_2->vNbCps() + d->nurbs_surface_data_2_2->vDegree() - 1);
    QVERIFY(d->nurbs_surface_data_2_2->vNbCps() + d->nurbs_surface_data_2_2->vDegree() - 1 == 5);
    d->nurbs_surface_data_2_2->vKnots(v_knots_2_2.data());
    QVERIFY(v_knots_2_2[0] == 0.);
    QVERIFY(v_knots_2_2[1] == 0.);
    QVERIFY(v_knots_2_2[2] == 1.);
    QVERIFY(v_knots_2_2[3] == 2.);
    QVERIFY(v_knots_2_2[4] == 2.);

    QVERIFY(d->nurbs_surface_data_2_2->vKnots()[0] == 0.);
    QVERIFY(d->nurbs_surface_data_2_2->vKnots()[1] == 0.);
    QVERIFY(d->nurbs_surface_data_2_2->vKnots()[2] == 1.);
    QVERIFY(d->nurbs_surface_data_2_2->vKnots()[3] == 2.);
    QVERIFY(d->nurbs_surface_data_2_2->vKnots()[4] == 2.);
}

void dtkNurbsSurfaceDataSislTestCase::testSurfacePoint(void)
{
    dtkContinuousGeometryTools::Point_3 point(0., 0., 0.);
    d->nurbs_surface_data_1_2->evaluatePoint(0.25, 0.65, point.data());

    QVERIFY(point[0] > 1.58912 - 10e-5 && point[0] < 1.58912 + 10e-5);
    QVERIFY(point[1] > 0.131438 - 10e-5 && point[1] < 0.131438 + 10e-5);
    QVERIFY(point[2] > 1.43294 - 10e-4 && point[2] < 1.43294 + 10e-4);
}

//To ameliorate with dataSurfacePoint and several pints to test
void dtkNurbsSurfaceDataSislTestCase::testSurfaceNormal(void)
{
    dtkContinuousGeometryPrimitives::Vector_3 normal(0., 0., 0.);
    d->nurbs_surface_data_1_2->evaluateNormal(0.5, 0.5, normal.data());
}

void dtkNurbsSurfaceDataSislTestCase::testPrintOutSurface(void) {
    std::ofstream nurbs_surface_data_sisl_cp_1_2_file("nurbs_surface_data_sisl_cp_1_2.xyz");
    if(!nurbs_surface_data_sisl_cp_1_2_file.is_open()) {
        dtkFatal() << "There has been a problem while creating the file.";
    }
    std::size_t u_degree_1_2 = d->nurbs_surface_data_1_2->uDegree();
    std::size_t v_degree_1_2 = d->nurbs_surface_data_1_2->vDegree();
    dtkContinuousGeometryPrimitives::Point_3 point_cp(0., 0., 0.);
    for (std::size_t i = 0; i <= u_degree_1_2; ++i) {
        for (std::size_t j = 0; j <= v_degree_1_2; ++j) {
            d->nurbs_surface_data_1_2->controlPoint(i, j, point_cp.data());
            nurbs_surface_data_sisl_cp_1_2_file << point_cp << std::endl;
        }
    }
    nurbs_surface_data_sisl_cp_1_2_file.close();

    std::ofstream nurbs_surface_data_sisl_1_2_file("nurbs_surface_data_sisl_1_2.xyz");
    if(!nurbs_surface_data_sisl_1_2_file.is_open()) {
        dtkFatal() << "There has been a problem while creating the file.";
    }
    dtkContinuousGeometryPrimitives::Point_3 point(0., 0., 0.);
    for (double i = 0.; i <= 3.; i += 0.01) {
        for (double j = 0.; j <= 2.; j += 0.01) {
            d->nurbs_surface_data_1_2->evaluatePoint(i, j, &point[0]);
            nurbs_surface_data_sisl_1_2_file << point << std::endl;
        }
    }
    nurbs_surface_data_sisl_1_2_file.close();

    std::ofstream nurbs_surface_data_sisl_cp_2_2_file("nurbs_surface_data_sisl_cp_2_2.xyz");
    if(!nurbs_surface_data_sisl_cp_2_2_file.is_open()) {
        dtkFatal() << "There has been a problem while creating the file.";
    }
    std::size_t u_degree = d->nurbs_surface_data_2_2->uDegree();
    std::size_t v_degree = d->nurbs_surface_data_2_2->vDegree();
    dtkContinuousGeometryPrimitives::Point_3 point_cp_2_2(0., 0., 0.);
    for (std::size_t i = 0; i <= u_degree; ++i) {
        for (std::size_t j = 0; j <= v_degree; ++j) {
            d->nurbs_surface_data_2_2->controlPoint(i, j, point_cp.data());
            nurbs_surface_data_sisl_cp_2_2_file << point_cp << std::endl;
        }
    }
    nurbs_surface_data_sisl_cp_2_2_file.close();

    std::ofstream nurbs_surface_data_sisl_2_2_file("nurbs_surface_data_sisl_2_2.xyz");
    if(!nurbs_surface_data_sisl_2_2_file.is_open()) {
        dtkFatal() << "There has been a problem while creating the file.";
    }
    dtkContinuousGeometryPrimitives::Point_3 point_2_2(0., 0., 0.);
    for (double i = 0.; i <= 2.; i += 0.01) {
        for (double j = 0.; j <= 2.; j += 0.01) {
            d->nurbs_surface_data_2_2->evaluatePoint(i, j, &point[0]);
            nurbs_surface_data_sisl_2_2_file << point << std::endl;
        }
    }
    nurbs_surface_data_sisl_2_2_file.close();
}

void dtkNurbsSurfaceDataSislTestCase::testAabb(void)
{
    dtkContinuousGeometryPrimitives::AABB_3 aabb(0., 0., 0., 0., 0., 0.);
    d->nurbs_surface_data_1_2->aabb(aabb.data());

    QVERIFY(aabb[0] == 0.);
    QVERIFY(aabb[1] == 0.);
    QVERIFY(aabb[2] == 0.);
    QVERIFY(aabb[3] == 4.);
    QVERIFY(aabb[4] == 3.);
    QVERIFY(aabb[5] == 3.);
}

void dtkNurbsSurfaceDataSislTestCase::cleanup(void)
{
}

void dtkNurbsSurfaceDataSislTestCase::cleanupTestCase(void)
{
}

DTKTEST_MAIN_NOGUI(dtkNurbsSurfaceDataSislTest, dtkNurbsSurfaceDataSislTestCase)

//
// dtkNurbsSurfaceDataSislTest.cpp ends here
