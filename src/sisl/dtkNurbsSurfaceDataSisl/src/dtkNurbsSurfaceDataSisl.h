// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <dtkNurbsSurfaceDataSislExport.h>

#include <dtkContinuousGeometry>
#include <dtkAbstractNurbsSurfaceData>
#include <dtkRationalBezierSurface>

#include <map>

struct SISLSurf;

class DTKNURBSSURFACEDATASISL_EXPORT dtkNurbsSurfaceDataSisl final : public dtkAbstractNurbsSurfaceData
{

public:
    dtkNurbsSurfaceDataSisl(void);
    ~dtkNurbsSurfaceDataSisl(void) final ;

public:
    void create(std::size_t dim, std::size_t nb_cp_u, std::size_t nb_cp_v, std::size_t order_u, std::size_t order_v, const double *knots_u, const double *knots_v, const double *cps) const override;
    void create(std::string path) const override;

public:
    std::size_t uDegree(void) const override;
    std::size_t vDegree(void) const override;

    std::size_t uNbCps(void) const override;
    std::size_t vNbCps(void) const override;

    std::size_t uNbKnots(void) const override;
    std::size_t vNbKnots(void) const override;

    std::size_t dim(void) const override;

    void controlPoint(std::size_t i, std::size_t j, double* r_cp) const override;
    void weight(std::size_t i, std::size_t j, double* r_w) const override;

    void uKnots(double* r_u_knots) const override;
    void vKnots(double* r_v_knots) const  override;

    const double *uKnots(void) const override;
    const double *vKnots(void) const override;

    double uPeriod(void) const override;
    double vPeriod(void) const override;

    bool isPointCulled(const dtkContinuousGeometryPrimitives::Point_2& point) const override;

public:
    void evaluatePoint(double p_u, double p_v, double* r_point) const override;
    void evaluateNormal(double p_u, double p_v, double* r_normal) const override;
    void evaluate1stDer(double p_u, double p_v, double* r_point, double* r_u_deriv, double* r_v_deriv) const override;
    void evaluate1stDer(double p_u, double p_v, double* r_u_deriv, double* r_v_deriv) const override;

 public:
    void decomposeToRationalBezierSurfaces(std::vector< std::pair< dtkRationalBezierSurface* , double*> >& p_rational_bezier_surfaces ) const override;

 public:
    void aabb(double* r_aabb) const override;
    void extendedAabb(double* r_aabb, double factor) const override;

 public:
    const SISLSurf *sislNurbsSurface(void);

 public:
    void print(std::ostream& stream) const override;

 public:
    dtkNurbsSurfaceDataSisl* clone(void) const override;

 private :
    SISLSurf *d;
    double *m_ecoef;
    double *m_et1;
    double *m_et2;
};


// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

inline dtkAbstractNurbsSurfaceData *dtkNurbsSurfaceDataSislCreator(void)
{
    return new dtkNurbsSurfaceDataSisl();
}

//
// dtkNurbsSurfaceDataSisl.h ends here
