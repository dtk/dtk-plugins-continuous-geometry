// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#include "dtkNurbsSurfaceDataSisl.h"
#include "dtkNurbsSurfaceDataSislPlugin.h"

#include <dtkCore>
#include <dtkContinuousGeometry>

// ///////////////////////////////////////////////////////////////////
// dtkNurbsSurfaceDataSislPlugin
// ///////////////////////////////////////////////////////////////////

void dtkNurbsSurfaceDataSislPlugin::initialize(void)
{
    dtkContinuousGeometry::abstractNurbsSurfaceData::pluginFactory().record("dtkNurbsSurfaceDataSisl", dtkNurbsSurfaceDataSislCreator);
}

void dtkNurbsSurfaceDataSislPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkNurbsSurfaceDataSisl)

//
// dtkNurbsSurfaceDataSislPlugin.cpp ends here
