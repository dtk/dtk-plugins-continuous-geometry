// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <QtCore>

#include <dtkContinuousGeometry.h>

#include <dtkAbstractNurbsSurfaceData.h>

class dtkNurbsSurfaceDataSislPlugin : public dtkAbstractNurbsSurfaceDataPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkAbstractNurbsSurfaceDataPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkNurbsSurfaceDataSislPlugin" FILE "dtkNurbsSurfaceDataSislPlugin.json")

public:
     dtkNurbsSurfaceDataSislPlugin(void) {}
    ~dtkNurbsSurfaceDataSislPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

//
// dtkNurbsSurfaceDataSislPlugin.h ends here
