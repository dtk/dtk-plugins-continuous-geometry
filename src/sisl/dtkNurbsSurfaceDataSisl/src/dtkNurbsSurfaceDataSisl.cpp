// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkNurbsSurfaceDataSisl.h"

#include <dtkContinuousGeometryUtils>
#include <dtkTrimLoop>

#include <sisl.h>
#include <cstring>

/*!
  \class dtkNurbsSurfaceDataSisl
  \inmodule dtkPluginsContinuousGeometry
  \brief dtkNurbsSurfaceDataSisl is a SISL implementation of the concept dtkAbstractNurbsSurfaceData.

  The following lines of code show how to instanciate and use the class in pratice:
  \code
  dtkAbstractNurbsSurfaceData *nurbs_surface_data = dtkContinuousGeometry::abstractNurbsSurfaceData::pluginFactory().create("dtkNurbsSurfaceDataSisl");
  dtkNurbsSurface *nurbs_surface = new dtkNurbsSurface(nurbs_surface_data);
  nurbs_surface->create(...);
  \endcode
*/

/*! \fn dtkNurbsSurfaceDataSisl::dtkNurbsSurfaceDataSisl(void)
  Instanciates the structure.

  To add the content, see \l create(std::size_t dim, std::size_t nb_cp_u, std::size_t nb_cp_v, std::size_t order_u, std::size_t order_v, const double *knots_u, const double *knots_v, const double *cps) const.
*/
dtkNurbsSurfaceDataSisl::dtkNurbsSurfaceDataSisl(void) : d(new SISLSurf), m_ecoef(nullptr), m_et1(nullptr), m_et2(nullptr) {}

/*! \fn dtkNurbsSurfaceDataSisl::~dtkNurbsSurfaceDataSisl(void)
  Destroys the instance.
*/
dtkNurbsSurfaceDataSisl::~dtkNurbsSurfaceDataSisl(void)
{
    delete[] m_et1;
    delete[] m_et2;
    delete[] m_ecoef;
    freeSurf(d);
}

/*! \fn void dtkNurbsSurfaceDataSisl::create(std::size_t dim, std::size_t nb_cp_u, std::size_t nb_cp_v, std::size_t order_u, std::size_t order_v, const double *knots_u, const double *knots_v, const double *cps) const
  Creates a NURBS surface by providing the required content.

  \a dim : dimension of the space in which lies the surface

  \a nb_cp_u : number of control points along the "u" direction

  \a nb_cp_v : number of control points along the "v" direction

  \a order_u : order(degree + 1) in the "u" direction

  \a order_v : order(degree + 1) in the "v" direction

  \a knots_u : array containing the knots in the "u" direction

  \a knots_v : array containing the knots in the "v" direction

  \a cps : array containing the weighted control points, secified as : [x_00, y_00, z_00, w_00, x_01, y_01, ...]

  \a knots_u, \a knots_v, and \a cps are copied.
*/
void dtkNurbsSurfaceDataSisl::create(std::size_t dim, std::size_t nb_cp_u, std::size_t nb_cp_v, std::size_t order_u, std::size_t order_v, const double *knots_u, const double *knots_v, const double *cps) const
{
    // /////////////////////////////////////////////////////////////////
    // Initializes the SISL NURBS surface
    // /////////////////////////////////////////////////////////////////
    dtkNurbsSurfaceDataSisl *self = const_cast<dtkNurbsSurfaceDataSisl *>(this);

    self->m_et1 = new double[nb_cp_u + order_u];
    self->m_et2 = new double[nb_cp_v + order_v];
    self->m_et1[0] = knots_u[0];
    self->m_et2[0] = knots_v[0];
    std::memcpy(&self->m_et1[1], knots_u, sizeof(double) * (nb_cp_u + order_u - 2));
    std::memcpy(&self->m_et2[1], knots_v, sizeof(double) * (nb_cp_v + order_v - 2));
    self->m_et1[nb_cp_u + order_u - 1] = knots_u[nb_cp_u + order_u - 3];
    self->m_et2[nb_cp_v + order_v - 1] = knots_v[nb_cp_v + order_v - 3];

    self->m_ecoef = new double[(dim + 1) * nb_cp_u * nb_cp_v];

    for(std::size_t i = 0; i < nb_cp_u; ++i) {
        for(std::size_t j = 0; j < nb_cp_v; ++j) {
            std::size_t new_coord = (dim + 1) * nb_cp_u * j + (dim + 1) * i;
            std::size_t old_coord = (dim + 1) * nb_cp_v * i + (dim + 1) * j;
            self->m_ecoef[new_coord] =     cps[old_coord] *     cps[old_coord + 3];
            self->m_ecoef[new_coord + 1] = cps[old_coord + 1] * cps[old_coord + 3];
            self->m_ecoef[new_coord + 2] = cps[old_coord + 2] * cps[old_coord + 3];
            self->m_ecoef[new_coord + 3] = cps[old_coord + 3];
        }
    }

    /* Copy Flag = 0 : Set internal pointer to input array and remember to free arrays*/
    self->d = newSurf(nb_cp_u, nb_cp_v, order_u, order_v, self->m_et1, self->m_et2, self->m_ecoef, 2, dim, 0);
    if(d == nullptr) {
        dtkFatal() << "The surface could not be created by SISL. " << Q_FUNC_INFO << ". Line : " << __LINE__;
    }
}

/*! \fn void dtkNurbsSurfaceDataSisl::create(std::string path) const
  Creates a NURBS surface by providing a path to a file storing the required information to create a NURBS surface.

  \a path : a valid path to the file containing the information regarding a given NURBS surface
*/
void dtkNurbsSurfaceDataSisl::create(std::string) const
{
    dtkFatal() << "Does nothing, not implemented";
}

/*! \fn dtkNurbsSurfaceDataSisl::uDegree(void) const
  Returns the degree of in the "u" direction.
*/
std::size_t dtkNurbsSurfaceDataSisl::uDegree(void) const
{
    return d->ik1 - 1;
}

/*! \fn dtkNurbsSurfaceDataSisl::vDegree(void) const
  Returns the degree of in the "v" direction.
*/
std::size_t dtkNurbsSurfaceDataSisl::vDegree(void) const
{
    return d->ik2 - 1;
}

/*! \fn dtkNurbsSurfaceDataSisl::uNbCps(void) const
  Returns the number of weighted control points in the "u" direction.
*/
std::size_t dtkNurbsSurfaceDataSisl::uNbCps(void) const
{
    return d->in1;
}

/*! \fn dtkNurbsSurfaceDataSisl::vNbCps(void) const
  Returns the number of weighted control points in the "v" direction.
*/
std::size_t dtkNurbsSurfaceDataSisl::vNbCps(void) const
{
    return d->in2;
}

std::size_t dtkNurbsSurfaceDataSisl::uNbKnots(void) const
{
    return (d->in1 + d->ik1 - 2);
}

std::size_t dtkNurbsSurfaceDataSisl::vNbKnots(void) const
{
    return (d->in2 + d->ik2 - 2);
}

/*! \fn dtkNurbsSurfaceDataSisl::dim(void) const
  Returns the dimension of the space in which the NURBS surface lies.
*/
std::size_t dtkNurbsSurfaceDataSisl::dim(void) const
{
    return d->idim;
}

/*! \fn dtkNurbsSurfaceDataSisl::controlPoint(std::size_t i, std::size_t j, double *r_cp) const
  Writes in \a r_cp the \a i th, \a j th weighted control point coordinates[xij, yij, zij].

  \a i : index of the weighted control point along the "u" direction

  \a j : index of the weighted control point along the "v" direction

  \a r_cp : array of size 3 to store the weighted control point coordinates[xij, yij, zij]
*/
void dtkNurbsSurfaceDataSisl::controlPoint(std::size_t i, std::size_t j, double* r_cp) const
{
    for (std::size_t k = 0; k < std::size_t(d->idim); ++k) {
        r_cp[k] = d->ecoef[(j * d->in1 + i) * (d->idim) + k];
    }
}

/*! \fn dtkNurbsSurfaceDataSisl::weight(std::size_t i, std::size_t j, double *r_w) const
  Writes in \a r_w the \a i th, \a j th weighted control point weight wij.

  \a i : index of the weighted control point along the "u" direction

  \a j : index of the weighted control point along the "v" direction

  \a r_w : array of size 1 to store the weighted control point weight wij
*/
void dtkNurbsSurfaceDataSisl::weight(std::size_t i, std::size_t j, double* r_w) const
{
    *r_w = d->rcoef[(j * d->in1 + i) * (d->idim + 1) + d->idim];
}

/*! \fn dtkNurbsSurfaceDataSisl::uKnots(double *r_u_knots) const
  Writes in \a r_u_knots the knots of the curve along the "u" direction.

  \a r_u_knots : array of size (nb_cp_u + order_u - 2) to store the knots
*/
void dtkNurbsSurfaceDataSisl::uKnots(double* r_u_knots) const
{
    std::size_t nb_of_knots_u = d->in1 + d->ik1 - 2;
    for (std::size_t i = 0; i < nb_of_knots_u; ++i) {
        r_u_knots[i] = d->et1[i + 1];
    }
}

/*! \fn dtkNurbsSurfaceDataSisl::vKnots(double *r_v_knots) const
  Writes in \a r_v_knots the knots of the curve along the "v" direction.

  \a r_v_knots : array of size (nb_cp_v + order_v - 2) to store the knots
*/
void dtkNurbsSurfaceDataSisl::vKnots(double* r_v_knots) const
{
    std::size_t nb_of_knots_v = d->in2 + d->ik2 - 2;
    for (std::size_t i = 0; i < nb_of_knots_v; ++i) {
        r_v_knots[i] = d->et2[i + 1];
    }
}

/*! \fn dtkNurbsSurfaceDataSisl::uKnots(void) const
  Returns a pointer to the array storing the knots of the surface along the "u" direction. The array is of size : (nb_cp_u + order_u - 2).
*/
const double *dtkNurbsSurfaceDataSisl::uKnots(void) const
{
    return &d->et1[1];
}

/*! \fn dtkNurbsSurfaceDataSisl::vKnots(void) const
  Returns a pointer to the array storing the knots of the surface along the "v" direction. The array is of size : (nb_cp_v + order_v - 2).
*/
const double *dtkNurbsSurfaceDataSisl::vKnots(void) const
{
    return &d->et2[1];
}

double dtkNurbsSurfaceDataSisl::uPeriod(void) const
{
    // do nothing
    return 0.;
}

double dtkNurbsSurfaceDataSisl::vPeriod(void) const
{
    // do nothing
    return 0.;
}

/*! \fn dtkNurbsSurfaceDataSisl::isPointCulled(const dtkContinuousGeometryPrimitives::Point_2& point) const
  Returns true if \a point is culled by the trim loops of the surface, else returns false.
*/
bool dtkNurbsSurfaceDataSisl::isPointCulled(const dtkContinuousGeometryPrimitives::Point_2& point) const
{
    //This works because the outter trim loop is necessarily of type inner (CCW)
    if (m_trim_loops.size() == 0) {
        dtkFatal() << "Trying to test if a point is culled when no trim loops are recorded.";
    } else if (m_trim_loops.size() == 1) {
        if((*m_trim_loops.begin())->isPointCulled(point)) {
            return true;
        } else {
            return false;
        }
    } else {
        std::size_t culled = 0;
        std::size_t not_culled = 0;
        for (auto trim_loop = m_trim_loops.begin(); trim_loop != m_trim_loops.end(); ++trim_loop) {
            if((*trim_loop)->isPointCulled(point) && (*trim_loop)->type() == dtkContinuousGeometryEnums::outer) {
                ++culled;
            } else if (!(*trim_loop)->isPointCulled(point) && (*trim_loop)->type() == dtkContinuousGeometryEnums::inner) {
                ++not_culled;
            }
        }
        if (culled == 0 && not_culled == 0) {
            return true;
        }
        if (culled == not_culled) {
            return true;
        } else {
            return false;
        }
    }
    return false; //Should never happen
}

/*! \fn dtkNurbsSurfaceDataSisl::evaluatePoint(double p_u, double p_v, double *r_point) const
  Writes in \a r_point the evaluation at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_point : array of size 3 to store the result of the evaluation
*/
void dtkNurbsSurfaceDataSisl::evaluatePoint(double p_u, double p_v, double* r_point) const
{
    double par_values[2];
    double normal[3];
    par_values[0] = p_u;
    par_values[1] = p_v;
    int left_knot_1;
    int left_knot_2;
    int der = 0;
    int stat = 0;
    s1421(d, der, par_values, &left_knot_1, &left_knot_2, r_point, normal, &stat);
}

/*! \fn dtkNurbsSurfaceDataSisl::evaluateNormal(double p_u, double p_v, double *r_normal) const
  Writes in \a r_normal the normal evaluation at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_normal : array of size 3 to store the result of the normal evaluation
*/
void dtkNurbsSurfaceDataSisl::evaluateNormal(double p_u, double p_v, double* r_normal) const
{
    double par_values[2];
    double derive[9];
    par_values[0] = p_u;
    par_values[1] = p_v;
    int left_knot_1;
    int left_knot_2;
    int der = 1;
    int stat;
    s1421(d, der, par_values, &left_knot_1, &left_knot_2, derive, r_normal, &stat);
}

/*! \fn dtkNurbsSurfaceDataSisl::evaluate1stDer(double p_u, double p_v, double *r_point, double *r_u_deriv, double *r_v_deriv) const
  Writes in \a r_point the evaluation, in \a r_u_deriv the first derivative in "u" direction, in \a r_v_deriv the first derivative in the "v" direction at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_point : array of size 3 to store the result of the evaluation

  \a r_u_deriv : array of size 3 to store the result of the first derivative evaluation in the "u" direction

  \a r_v_deriv : array of size 3 to store the result of the first derivative evaluation in the "v" direction
*/
void dtkNurbsSurfaceDataSisl::evaluate1stDer(double p_u, double p_v, double* r_point, double* r_u_deriv, double* r_v_deriv) const
{
    double par_values[2];
    double derive[9];
    double normal[3];
    par_values[0] = p_u;
    par_values[1] = p_v;
    int left_knot_1;
    int left_knot_2;
    int der = 1;
    int stat;
    s1421(d, der, par_values, &left_knot_1, &left_knot_2, derive, normal, &stat);
    r_point[0] = derive[0];
    r_point[1] = derive[1];
    r_point[2] = derive[2];

    r_u_deriv[0] = derive[3];
    r_u_deriv[1] = derive[4];
    r_u_deriv[2] = derive[5];

    r_v_deriv[0] = derive[6];
    r_v_deriv[1] = derive[7];
    r_v_deriv[2] = derive[8];
}

/*! \fn dtkNurbsSurfaceDataSisl::evaluate1stDer(double p_u, double p_v, double *r_u_deriv, double *r_v_deriv) const
  Writes in \a r_u_deriv the first derivative in "u" direction and in \a r_v_deriv the first derivative in the "v" direction at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_u_deriv : array of size 3 to store the result of the first derivative evaluation in the "u" direction

  \a r_v_deriv : array of size 3 to store the result of the first derivative evaluation in the "v" direction
*/
void dtkNurbsSurfaceDataSisl::evaluate1stDer(double p_u, double p_v, double* r_u_deriv, double* r_v_deriv) const
{
    double par_values[2];
    double derive[9];
    double normal[3];
    par_values[0] = p_u;
    par_values[1] = p_v;
    int left_knot_1;
    int left_knot_2;
    int der = 1;
    int stat;
    s1421(d, der, par_values, &left_knot_1, &left_knot_2, derive, normal, &stat);
    r_u_deriv[0] = derive[0];
    r_u_deriv[1] = derive[1];
    r_u_deriv[2] = derive[2];

    r_u_deriv[0] = derive[3];
    r_u_deriv[1] = derive[4];
    r_u_deriv[2] = derive[5];

    r_v_deriv[0] = derive[6];
    r_v_deriv[1] = derive[7];
    r_v_deriv[2] = derive[8];
}

/*! \fn dtkNurbsSurfaceDataSisl::decomposeToRationalBezierSurfaces(std::vector< std::pair< dtkRationalBezierSurface *, double * > >& r_rational_bezier_surfaces) const
  Decomposes the NURBS surface into dtkRationalBezierSurface \a r_rational_bezier_surfaces.

  \a r_rational_bezier_surfaces : vector in which the dtkRationalBezierSurface are stored, along with their limits in the NURBS surface parameter space

  Not sure what happens when the not extreme nodes are of multiplicity >1.
*/
void dtkNurbsSurfaceDataSisl::decomposeToRationalBezierSurfaces(std::vector< std::pair< dtkRationalBezierSurface*, double* > >&) const
{
    dtkFatal() << "Does nothing, not implemented";
}

/*! \fn dtkNurbsSurfaceDataSisl::aabb(double* r_aabb) const
  Writes in \a r_aabb the [xmin, ymin, zmin, xmax, ymax, zmax] coordinates.

  \a r_aabb : array of size 6 to store the limits coordinates
*/
void dtkNurbsSurfaceDataSisl::aabb(double *r_aabb) const
{
    int jstat = 0;
    double *emax = new double[3];
    double *emin = new double[3];
    s1989(d, &emax, &emin, &jstat);
    if(jstat < 0 ) {
        dtkFatal() << "The bouding box of the NURBS surface could not be recovered. " << Q_FUNC_INFO << " .Line : " << __LINE__;
    }
    r_aabb[0] = emin[0];
    r_aabb[1] = emin[1];
    r_aabb[2] = emin[2];
    r_aabb[3] = emax[0];
    r_aabb[4] = emax[1];
    r_aabb[5] = emax[2];
    if(jstat < 0) {
        dtkFatal() << "The bounding box could not be computed. " << Q_FUNC_INFO << ". LINE : " << __LINE__;
    }

}

/*! \fn dtkNurbsSurfaceDataSisl::extendedAabb(double *r_aabb, double factor) const
  Writes in \a r_aabb the [xmin, ymin, zmin, xmax, ymax, zmax] coordinates of an extened bounding box of factor \a factor. The extension factor extends the diagonal.

  \a r_aabb : array of size 6 to store the extended limits coordinates

  \a factor : factor used to lengthen the diagonal of the axis aligned bounding box
*/
void dtkNurbsSurfaceDataSisl::extendedAabb(double*, double) const
{
    dtkFatal() << "Does nothing, not implemented";
}

/*! \fn dtkNurbsSurfaceDataSisl::sislNurbsSurface(void)
  Not available in the dtkAbstractNurbsSurfaceData.

  Returns the underlying sisl NURBS surface..
 */
const SISLSurf *dtkNurbsSurfaceDataSisl::sislNurbsSurface(void)
{
    return d;
}

/*! \fn dtkNurbsSurfaceDataSisl::clone(void) const
  Clone
*/
dtkNurbsSurfaceDataSisl* dtkNurbsSurfaceDataSisl::clone(void) const
{
    return new dtkNurbsSurfaceDataSisl(*this);
}

void dtkNurbsSurfaceDataSisl::print(std::ostream&) const
{
    dtkFatal() << "Does nothing, not implemented";
}
//
// dtkNurbsSurfaceDataSisl.cpp ends here
