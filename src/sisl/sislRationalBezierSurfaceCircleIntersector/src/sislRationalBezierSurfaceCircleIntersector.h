#ifndef SRC_SISL_SISLRATIONALBEZIERSURFACECIRCLEINTERSECTOR_SRC_SISLRATIONALBEZIERSURFACECIRCLEINTERSECTOR_H_
#define SRC_SISL_SISLRATIONALBEZIERSURFACECIRCLEINTERSECTOR_SRC_SISLRATIONALBEZIERSURFACECIRCLEINTERSECTOR_H_

#include <dtkRationalBezierSurfaceCircleIntersector>

#include <sislRationalBezierSurfaceCircleIntersectorExport.h>

#include <dtkContinuousGeometryUtils.h>

struct sislRationalBezierSurfaceCircleIntersectorPrivate;

class dtkRationalBezierSurface;

class SISLRATIONALBEZIERSURFACECIRCLEINTERSECTOR_EXPORT sislRationalBezierSurfaceCircleIntersector : public dtkRationalBezierSurfaceCircleIntersector
{
public:
    sislRationalBezierSurfaceCircleIntersector(void);
    ~sislRationalBezierSurfaceCircleIntersector(void);

    void setRationalBezierSurface(dtkRationalBezierSurface* surface) final;
    void setCircle(dtkContinuousGeometryPrimitives::Circle_3* circle) final;

    bool run(void) final;
    void reset() final;

    const std::vector< dtkContinuousGeometryPrimitives::IntersectionObject > & intersection(void) final;

protected:
    sislRationalBezierSurfaceCircleIntersectorPrivate *d;
};

inline dtkRationalBezierSurfaceCircleIntersector* sislRationalBezierSurfaceCircleIntersectorCreator(void)
{
    return new sislRationalBezierSurfaceCircleIntersector();
}

#endif /* SRC_SISL_SISLRATIONALBEZIERSURFACECIRCLEINTERSECTOR_SRC_SISLRATIONALBEZIERSURFACECIRCLEINTERSECTOR_H_ */
