
#ifndef SRC_SISL_SISLRATIONALBEZIERSURFACECIRCLEINTERSECTOR_SRC_SISLRATIONALBEZIERSURFACECIRCLEINTERSECTORPLUGIN_H_
#define SRC_SISL_SISLRATIONALBEZIERSURFACECIRCLEINTERSECTOR_SRC_SISLRATIONALBEZIERSURFACECIRCLEINTERSECTORPLUGIN_H_

#include <QtCore>

#include <dtkContinuousGeometry.h>

#include <dtkRationalBezierSurfaceCircleIntersector.h>

class sislRationalBezierSurfaceCircleIntersectorPlugin : public dtkRationalBezierSurfaceCircleIntersectorPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkRationalBezierSurfaceCircleIntersectorPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.sislRationalBezierSurfaceCircleIntersectorPlugin" FILE "sislRationalBezierSurfaceCircleIntersectorPlugin.json")

public:
     sislRationalBezierSurfaceCircleIntersectorPlugin(void) {}
    ~sislRationalBezierSurfaceCircleIntersectorPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

#endif /* SRC_SISL_SISLRATIONALBEZIERSURFACECIRCLEINTERSECTOR_SRC_SISLRATIONALBEZIERSURFACECIRCLEINTERSECTORPLUGIN_H_ */
