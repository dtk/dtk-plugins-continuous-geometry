/*
 * sislRationalBezierSurfaceCircleIntersectorPlugin.cpp
 *
 *  Created on: May 6, 2020
 *      Author: xxiao
 */

#include "sislRationalBezierSurfaceCircleIntersector.h"
#include "sislRationalBezierSurfaceCircleIntersectorPlugin.h"

#include <dtkCore>
#include <dtkContinuousGeometry>

void sislRationalBezierSurfaceCircleIntersectorPlugin::initialize(void)
{
    dtkContinuousGeometry::rationalBezierSurfaceCircleIntersector::pluginFactory().record("sislRationalBezierSurfaceCircleIntersector", sislRationalBezierSurfaceCircleIntersectorCreator);
}

void sislRationalBezierSurfaceCircleIntersectorPlugin::uninitialize(void)
{

}

DTK_DEFINE_PLUGIN(sislRationalBezierSurfaceCircleIntersector)
