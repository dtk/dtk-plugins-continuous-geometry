// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "blazeRationalBezierCurve2DElevator.h"
#include "blazeRationalBezierCurve2DElevatorPlugin.h"

#include "dtkContinuousGeometry.h"

#include <dtkCore>

// ///////////////////////////////////////////////////////////////////
// blazeRationalBezierCurve2DElevatorPlugin
// ///////////////////////////////////////////////////////////////////

void blazeRationalBezierCurve2DElevatorPlugin::initialize(void)
{
    dtkContinuousGeometry::rationalBezierCurve2DElevator::pluginFactory().record("blazeRationalBezierCurve2DElevator", blazeRationalBezierCurve2DElevatorCreator);
}

void blazeRationalBezierCurve2DElevatorPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(blazeRationalBezierCurve2DElevator)

//
// blazeRationalBezierCurve2DElevatorPlugin.cpp ends here
