// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <dtkRationalBezierCurve2DElevator>

#include <blazeRationalBezierCurve2DElevatorExport.h>

#include <dtkContinuousGeometry>

class dtkRationalBezierCurve2D;
class dtkRationalBezierSurface;
class dtkRationalBezierCurve;
class blazeRationalBezierCurve2DElevatorPrivate;

class BLAZERATIONALBEZIERCURVE2DELEVATOR_EXPORT blazeRationalBezierCurve2DElevator : public dtkRationalBezierCurve2DElevator
{
 public:
    blazeRationalBezierCurve2DElevator(void);
    ~blazeRationalBezierCurve2DElevator(void);

 public:
    void setInputRationalBezierCurve2D(dtkRationalBezierCurve2D *) final;
    void setInputRationalBezierSurface(dtkRationalBezierSurface *) final;

 public:
    void run() final;

    dtkRationalBezierCurve *rationalBezierCurve(void) final;

 protected:
    blazeRationalBezierCurve2DElevatorPrivate *d;
};


// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

inline dtkRationalBezierCurve2DElevator *blazeRationalBezierCurve2DElevatorCreator(void)
{
    return new blazeRationalBezierCurve2DElevator();
}

//
// blazeRationalBezierCurve2DElevator.h ends here
