// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "blazeRationalBezierCurve2DElevator.h"

#include <dtkContinuousGeometry.h>

#include <dtkContinuousGeometryUtils>

#include <dtkRationalBezierCurve2D>
#include <dtkRationalBezierCurve>
#include <dtkRationalBezierSurface>

#include "Blaze_without_warnings.h"

extern "C" {
    /* DGESDD prototype */
    void dgesdd_( char* jobz, int* m, int* n, double* a,
                  int* lda, double* s, double* u, int* ldu, double* vt, int* ldvt,
                  double* work, int* lwork, int* iwork, int* info );
}

// /////////////////////////////////////////////////////////////////
// blazeRationalBezierCurve2DElevatorPrivate
// /////////////////////////////////////////////////////////////////

class blazeRationalBezierCurve2DElevatorPrivate
{
private:
    template< typename K > using dtkMatrixMap = dtkContinuousGeometryTools::dtkMatrixMap< K >;

public:
    void run(void);
    int sdd(blaze::DynamicMatrix< double, blaze::columnMajor >& A, blaze::DynamicVector< double >& vector_ev, blaze::DynamicMatrix< double, blaze::columnMajor>& matrix_u, blaze::DynamicMatrix< double, blaze::columnMajor >& matrix_vt);
    std::size_t rank(const blaze::DynamicVector< double >& p_vector_sv, double p_norm_tolerance, double p_normalized_tolerance);

public:
    dtkRationalBezierCurve2D *rational_bezier_curve_2d;
    dtkRationalBezierSurface *rational_bezier_surface;
    dtkRationalBezierCurve *rational_bezier_curve_3d;
};

void blazeRationalBezierCurve2DElevatorPrivate::run(void)
{
    // ///////////////////////////////////////////////////////////////////
    // Checks that all inputs are set
    // ///////////////////////////////////////////////////////////////////
    if (rational_bezier_curve_2d == nullptr || rational_bezier_surface == nullptr) {
        dtkFatal() << "Not all inputs of blazeRationalBezierCurve2DElevator were set";
    }
    // ///////////////////////////////////////////////////////////////////
    // Recover the maximal possible degree of the 3d rational bezier curve (a 2d rational bezier curve embedded in a rational_bezier surface)
    // ///////////////////////////////////////////////////////////////////
    std::size_t max_degree = rational_bezier_curve_2d->degree() * (rational_bezier_surface->uDegree() + rational_bezier_surface->vDegree());

    // ///////////////////////////////////////////////////////////////////
    //
    // ///////////////////////////////////////////////////////////////////
    blaze::DynamicMatrix< double, blaze::columnMajor > P(max_degree + 1, 4);

    blaze::DynamicMatrix< double, blaze::columnMajor > C(max_degree + 1, 4);

    blaze::DynamicMatrix< double, blaze::columnMajor > B_coefs(max_degree + 1, max_degree + 1);

    dtkContinuousGeometryPrimitives::Point_2 point_2d(0., 0.);
    dtkContinuousGeometryPrimitives::Point_3 point_3d(0., 0., 0.);
    dtkMatrixMap< double > binomial_coefficients;
    dtkContinuousGeometryTools::computeBinomialCoefficients(binomial_coefficients, max_degree + 1, max_degree + 1);

    double w = 0.;
    for (std::size_t i = 0; i < max_degree + 1; ++i) {
        double p = double(i) / double(max_degree);
        rational_bezier_curve_2d->evaluatePoint(p, point_2d.data());
        rational_bezier_surface->evaluatePoint(point_2d[0], point_2d[1], point_3d.data());
        for (std::size_t j = 0; j < max_degree + 1; ++j) {
            B_coefs(i, j) = binomial_coefficients[{max_degree, j}] * std::pow(p, j) * std::pow(1 - p, max_degree - j);
        }
        C(i, 0) = point_3d[0];
        C(i, 1) = point_3d[1];
        C(i, 2) = point_3d[2];
    }
    std::cerr << B_coefs << std::endl;
    // ///////////////////////////////////////////////////////////////////
    // Solves B_coef * P = C
    // 1. Computes the SVD of B_coef
    // ///////////////////////////////////////////////////////////////////

    blaze::DynamicVector< double > vector_ev;
    blaze::DynamicMatrix< double, blaze::columnMajor > matrix_u;
    blaze::DynamicMatrix< double, blaze::columnMajor > matrix_vt;

    sdd(B_coefs, vector_ev, matrix_u, matrix_vt);

    // ///////////////////////////////////////////////////////////////////
    // Checks that the matrix was full rank otherwise we would divide by 0
    // ///////////////////////////////////////////////////////////////////
    if (rank(vector_ev, 1e-7, 1e-7) != vector_ev.size()) {
        dtkFatal() << "The matrix of coefficients was singular, nothing can be done";
    }

    // ///////////////////////////////////////////////////////////////////
    // Creates the matrix Sigma from A = U * Sigma * Vt
    // And inverse it at the same time
    // ///////////////////////////////////////////////////////////////////
    blaze::DynamicMatrix< double, blaze::columnMajor > sigma_inv(vector_ev.size(), vector_ev.size());
    for (std::size_t i = 0; i < vector_ev.size(); ++i) {
        for (std::size_t j = 0; j < i; ++j) {
            sigma_inv(i, j) = 0.;
        }
        sigma_inv(i, i) = 1. / vector_ev[i];
        for (std::size_t j = i + 1; j < vector_ev.size(); ++j) {
            sigma_inv(i, j) = 0.;
        }
    }

    P = matrix_vt.transpose() * sigma_inv * matrix_u.transpose() * C;

    dtkAbstractRationalBezierCurveData *rational_bezier_curve_data = dtkContinuousGeometry::abstractRationalBezierCurveData::pluginFactory().create("dtkRationalBezierCurveDataOn");
    if(rational_bezier_curve_data == nullptr) {
        dtkFatal() << "To use " << Q_FUNC_INFO << " , dtkAbstractRationalBezierCurveData must be compiled under the openNURBS plugins";
    }

    double *cps = new double[P.rows() * 4];
    for(std::size_t i = 0; i < P.rows(); ++i) {
        for(std::size_t j = 0; j < P.columns(); ++j) {
            cps[i * P.columns() + j] = P(i, j);
        }
    }
    rational_bezier_curve_3d = new dtkRationalBezierCurve(rational_bezier_curve_data);
    rational_bezier_curve_3d->create(P.rows(), cps);
}

//Make a pretty documentation to explain that the matrices must have the right size, and that it overwrites A contents
int blazeRationalBezierCurve2DElevatorPrivate::sdd(blaze::DynamicMatrix< double, blaze::columnMajor >& A, blaze::DynamicVector< double >& vector_ev, blaze::DynamicMatrix< double, blaze::columnMajor>& matrix_u, blaze::DynamicMatrix< double, blaze::columnMajor >& matrix_vt)
{
    int m = A.rows();
    int n = A.columns();
    int singN = (m<n)? m:n;

    vector_ev.resize(singN, false);
    matrix_u.resize(m, m, false);
    matrix_vt.resize(n, n, false);

    int lda = A.spacing();
    int ldu = matrix_u.spacing();
    int ldvt = matrix_vt.spacing();

    int info;
    int lwork;
    double wkopt;

    /* mode */
    char mode = 'A';
    /* iwork dimension should be at least 8*min(m,n) */
    int iwork[8*lda];

    /* Query and allocate the optimal workspace */
    lwork = -1;

    dgesdd_( &mode, &m, &n, A.data(), &lda, vector_ev.data(), matrix_u.data(), &ldu, matrix_vt.data(), &ldvt, &wkopt, &lwork, iwork, &info );
    assert(info == 0);

    lwork = (int)wkopt;
    double work[lwork];

    /* Compute SVD (divide and conquer)*/
    dgesdd_( &mode, &m, &n, A.data(), &lda, vector_ev.data(), matrix_u.data(), &ldu, matrix_vt.data(), &ldvt, &work[0], &lwork, iwork, &info );

    return info;
}

/*!
  Returns the rank of an application by reading the eigen values \a p_vector_ev.

  Returns the rank of an application by reading the eigen values of either GEV or SVD decompositions of its matricial representation, with \a p_norm_tolerance controlling that the first eigen value is not too small (check that the rank is not zero), and p_normalized_tolerance controlling that the other eigen values do not differ too much from the first one.
*/
std::size_t blazeRationalBezierCurve2DElevatorPrivate::rank(const blaze::DynamicVector< double >& p_vector_sv, double p_norm_tolerance, double p_normalized_tolerance)
{
    // /////////////////////////////////////////////////////////////////
    // Checks that the first singluar/eigen value is not "zero"
    // /////////////////////////////////////////////////////////////////
    if (std::fabs(p_vector_sv[0]) < p_norm_tolerance) {
        return 0;
    }
    // /////////////////////////////////////////////////////////////////
    // Normalizes the singular/eigen values according to the first value
    // /////////////////////////////////////////////////////////////////
    blaze::DynamicVector< double > normalized_vector_sv(p_vector_sv.size());
    for (std::size_t i = 0; i < p_vector_sv.size(); ++i) {
        normalized_vector_sv[i] = p_vector_sv[i] / p_vector_sv[0];
    }
    // /////////////////////////////////////////////////////////////////
    // Checks when a singuar value becomes negligeable compared to the previous eigen value
    //, if not negligeable increases rank, otherwise returns rank
    // /////////////////////////////////////////////////////////////////
    std::size_t rank = 1;
    while (rank < normalized_vector_sv.size()) {
        if (std::fabs(normalized_vector_sv[rank] / normalized_vector_sv[rank - 1]) > p_normalized_tolerance) {
            ++rank;
        } else {
            break;
        }
    }
    return rank;
}

// ///////////////////////////////////////////////////////////////////
// blazeRationalBezierCurve2DElevator implementation
// ///////////////////////////////////////////////////////////////////

blazeRationalBezierCurve2DElevator::blazeRationalBezierCurve2DElevator(void) : d(new blazeRationalBezierCurve2DElevatorPrivate)
{
    d->rational_bezier_curve_2d = nullptr;
    d->rational_bezier_surface = nullptr;
    d->rational_bezier_curve_3d = nullptr;
}

blazeRationalBezierCurve2DElevator::~blazeRationalBezierCurve2DElevator(void)
{
    delete d;
    d = nullptr;
}

void blazeRationalBezierCurve2DElevator::setInputRationalBezierCurve2D(dtkRationalBezierCurve2D *rational_bezier_curve_2d)
{
    d->rational_bezier_curve_2d = rational_bezier_curve_2d;
}

void blazeRationalBezierCurve2DElevator::setInputRationalBezierSurface(dtkRationalBezierSurface *rational_bezier_surface)
{
    d->rational_bezier_surface = rational_bezier_surface;
}

void blazeRationalBezierCurve2DElevator::run(void)
{
    d->run();
}

dtkRationalBezierCurve *blazeRationalBezierCurve2DElevator::rationalBezierCurve(void)
{
    if (d->rational_bezier_curve_3d == nullptr) {
        dtkFatal() << "The run method has probably not been called";
    }
    return d->rational_bezier_curve_3d;
}


//
// blazeRationalBezierCurve2DElevator.cpp ends here
