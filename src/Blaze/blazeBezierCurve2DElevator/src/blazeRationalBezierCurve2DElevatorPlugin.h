// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkRationalBezierCurve2DElevator.h>

#include <blazeRationalBezierCurve2DElevatorExport.h>

#include <dtkCore>

class BLAZERATIONALBEZIERCURVE2DELEVATOR_EXPORT blazeRationalBezierCurve2DElevatorPlugin : public dtkRationalBezierCurve2DElevatorPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkRationalBezierCurve2DElevatorPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.blazeRationalBezierCurve2DElevatorPlugin" FILE "blazeRationalBezierCurve2DElevatorPlugin.json")

public:
     blazeRationalBezierCurve2DElevatorPlugin(void) {}
    ~blazeRationalBezierCurve2DElevatorPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

//
// blazeRationalBezierCurve2DElevatorPlugin.h ends here
