// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "blazeRationalBezierCurve2DElevatorTest.h"

#include <blazeRationalBezierCurve2DElevator.h>

#include <dtkAbstractRationalBezierCurve2DData>
#include <dtkRationalBezierCurve2D>
#include <dtkAbstractRationalBezierSurfaceData>
#include <dtkRationalBezierSurface>

#include <dtkTest>

// ///////////////////////////////////////////////////////////////////
// D-Pointer implementation
// ///////////////////////////////////////////////////////////////////

class blazeRationalBezierCurve2DElevatorTestCasePrivate{
public:
    dtkRationalBezierSurface *rational_bezier_surface;
    dtkAbstractRationalBezierSurfaceData *rational_bezier_surface_data;

    dtkRationalBezierCurve2D *rational_bezier_curve;
    dtkAbstractRationalBezierCurve2DData *rational_bezier_curve_data;

    double tolerance = 1e-15;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

blazeRationalBezierCurve2DElevatorTestCase::blazeRationalBezierCurve2DElevatorTestCase(void):d(new blazeRationalBezierCurve2DElevatorTestCasePrivate())
{
    dtkLogger::instance().attachConsole();

    dtkContinuousGeometrySettings settings;
    settings.beginGroup("continuous-geometry");
    dtkContinuousGeometry::initialize(settings.value("plugins").toString());
    settings.endGroup();
}

blazeRationalBezierCurve2DElevatorTestCase::~blazeRationalBezierCurve2DElevatorTestCase(void)
{
}

void blazeRationalBezierCurve2DElevatorTestCase::initTestCase(void)
{
    std::size_t dim = 2;
    std::size_t nb_cp = 3;
    std::size_t order = 3;

    /* Forces openNURBS plugins to be compiled and working
       TODO : change to default implementation
    */
    d->rational_bezier_curve_data = dtkContinuousGeometry::abstractRationalBezierCurve2DData::pluginFactory().create("dtkRationalBezierCurve2DDataOn");
    if (d->rational_bezier_curve_data == nullptr) {
        dtkFatal() << "The dtkAbstractRationalBezierCurve2DData could not be loaded by the factory under the openNURBS implementation";
    }
    d->rational_bezier_surface_data = dtkContinuousGeometry::abstractRationalBezierSurfaceData::pluginFactory().create("dtkRationalBezierSurfaceDataOn");
    if (d->rational_bezier_surface_data == nullptr) {
       dtkFatal() << "The dtkAbstractRationalBezierSurfaceData could not be loaded by the factory under the openNURBS implementation";
    }
    d->rational_bezier_curve = new dtkRationalBezierCurve2D(d->rational_bezier_curve_data);
    d->rational_bezier_surface = new dtkRationalBezierSurface(d->rational_bezier_surface_data);

    double* cps = new double[(dim + 1) * nb_cp];
    cps[0] = 0.;   cps[1] = 0.; cps[2] = 1.;
    cps[3] = 0.5;   cps[4] = 0.5; cps[5] = 1.;
    cps[6] = 1.;   cps[7] = 0.; cps[8] = 1.;

    d->rational_bezier_curve->create(order, cps);

    //bi-degree 1, 1
    std::size_t nb_cp_u = 2;
    std::size_t nb_cp_v = 2;

    double *cps_s = new double[(3 + 1) * nb_cp_u * nb_cp_v];
    cps_s[0] = 0.;   cps_s[1] = 0.; cps_s[2] = 0.;  cps_s[3] = 1.;
    cps_s[4] = 0.;   cps_s[5] = 1.; cps_s[6] = 0.;  cps_s[7] = 1.;
    cps_s[8] = 1.;   cps_s[9] = 0.; cps_s[10] = 0.; cps_s[11] = 1.;
    cps_s[12] = 1.;  cps_s[13] = 1.;  cps_s[14] = 0.; cps_s[15] = 1.;

    d->rational_bezier_surface->create(3, nb_cp_u, nb_cp_v, cps_s);
}

void blazeRationalBezierCurve2DElevatorTestCase::init(void)
{

}


void blazeRationalBezierCurve2DElevatorTestCase::testRun(void)
{
    blazeRationalBezierCurve2DElevator elevator = blazeRationalBezierCurve2DElevator();
    elevator.setInputRationalBezierCurve2D(d->rational_bezier_curve);
    elevator.setInputRationalBezierSurface(d->rational_bezier_surface);

    elevator.run();
}


void blazeRationalBezierCurve2DElevatorTestCase::cleanup(void)
{

}

void blazeRationalBezierCurve2DElevatorTestCase::cleanupTestCase(void)
{
    delete d->rational_bezier_curve;
    delete d->rational_bezier_surface;
}

DTKTEST_MAIN_NOGUI(blazeRationalBezierCurve2DElevatorTest, blazeRationalBezierCurve2DElevatorTestCase)

//
// blazeRationalBezierCurve2DElevatorTest.cpp ends here
