// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <QTest>

class blazeRationalBezierCurve2DElevatorTestCasePrivate;

class blazeRationalBezierCurve2DElevatorTestCase : public QObject
{
    Q_OBJECT

public:
    blazeRationalBezierCurve2DElevatorTestCase(void);
    ~blazeRationalBezierCurve2DElevatorTestCase(void);

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testRun(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);

private:
    blazeRationalBezierCurve2DElevatorTestCasePrivate* d;
};

//
// blazeRationalBezierCurve2DElevatorTest.h ends here
