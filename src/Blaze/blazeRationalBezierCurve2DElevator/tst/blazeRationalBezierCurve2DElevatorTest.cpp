// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "blazeRationalBezierCurve2DElevatorTest.h"

#include <dtkContinuousGeometryUtils.h>
#include <blazeRationalBezierCurve2DElevator.h>

#include <dtkAbstractRationalBezierCurve2DData>
#include <dtkRationalBezierCurve2D>
#include <dtkAbstractRationalBezierSurfaceData>
#include <dtkRationalBezierSurface>
#include <dtkRationalBezierCurve>

#include <dtkTest>

// ///////////////////////////////////////////////////////////////////
// D-Pointer implementation
// ///////////////////////////////////////////////////////////////////

class blazeRationalBezierCurve2DElevatorTestCasePrivate{
public:
    dtkRationalBezierSurface *rational_bezier_surface;
    dtkAbstractRationalBezierSurfaceData *rational_bezier_surface_data;

    dtkAbstractRationalBezierCurve2DData *rational_bezier_curve_data_1;
    dtkAbstractRationalBezierCurve2DData *rational_bezier_curve_data_2;
    dtkAbstractRationalBezierCurve2DData *rational_bezier_curve_data_3;
    dtkRationalBezierCurve2D *rational_bezier_curve_1;
    dtkRationalBezierCurve2D *rational_bezier_curve_2;
    dtkRationalBezierCurve2D *rational_bezier_curve_3;

    double tolerance = 1e-15;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

blazeRationalBezierCurve2DElevatorTestCase::blazeRationalBezierCurve2DElevatorTestCase(void):d(new blazeRationalBezierCurve2DElevatorTestCasePrivate())
{
    dtkLogger::instance().attachConsole();

    dtkContinuousGeometrySettings settings;
    settings.beginGroup("continuous-geometry");
    dtkContinuousGeometry::initialize(settings.value("plugins").toString());
    settings.endGroup();
}

blazeRationalBezierCurve2DElevatorTestCase::~blazeRationalBezierCurve2DElevatorTestCase(void)
{
}

void blazeRationalBezierCurve2DElevatorTestCase::initTestCase(void)
{
    std::size_t dim = 2;
    std::size_t nb_cp = 2;
    std::size_t order = 2;

    /* Forces openNURBS plugins to be compiled and working
       TODO : change to default implementation
    */
    d->rational_bezier_curve_data_1 = dtkContinuousGeometry::abstractRationalBezierCurve2DData::pluginFactory().create("dtkRationalBezierCurve2DDataOn");
    d->rational_bezier_curve_data_2 = dtkContinuousGeometry::abstractRationalBezierCurve2DData::pluginFactory().create("dtkRationalBezierCurve2DDataOn");
    d->rational_bezier_curve_data_3 = dtkContinuousGeometry::abstractRationalBezierCurve2DData::pluginFactory().create("dtkRationalBezierCurve2DDataOn");
    if (d->rational_bezier_curve_data_1 == nullptr || d->rational_bezier_curve_data_2 == nullptr || d->rational_bezier_curve_data_3 == nullptr) {
        dtkFatal() << "The dtkAbstractRationalBezierCurve2DData could not be loaded by the factory under the openNURBS implementation";
    }

    d->rational_bezier_surface_data = dtkContinuousGeometry::abstractRationalBezierSurfaceData::pluginFactory().create("dtkRationalBezierSurfaceDataOn");
    if (d->rational_bezier_surface_data == nullptr) {
       dtkFatal() << "The dtkAbstractRationalBezierSurfaceData could not be loaded by the factory under the openNURBS implementation";
    }

    d->rational_bezier_curve_1 = new dtkRationalBezierCurve2D(d->rational_bezier_curve_data_1);
    d->rational_bezier_curve_2 = new dtkRationalBezierCurve2D(d->rational_bezier_curve_data_2);
    d->rational_bezier_curve_3 = new dtkRationalBezierCurve2D(d->rational_bezier_curve_data_3);

    d->rational_bezier_surface = new dtkRationalBezierSurface(d->rational_bezier_surface_data);

    double* cps_1 = new double[(dim + 1) * nb_cp];
    cps_1[0] = 0.5;   cps_1[1] = 0.25; cps_1[2] = 1.;
    cps_1[3] = 0.5;   cps_1[4] = 0.75; cps_1[5] = 1.;
    d->rational_bezier_curve_1->create(order, cps_1);
    delete[] cps_1;

    double* cps_2 = new double[(dim + 1) * nb_cp];
    cps_2[0] = 0.25;   cps_2[1] = 0.25; cps_2[2] = 1.;
    cps_2[3] = 0.75;   cps_2[4] = 0.75; cps_2[5] = 1.;
    d->rational_bezier_curve_2->create(order, cps_2);
    delete[] cps_2;

    std::size_t nb_cp_3 = 5;
    std::size_t order_3 = 5;
    double* cps_3 = new double[(dim + 1) * nb_cp_3];
    cps_3[0] = 0.1;   cps_3[1] = 0.2; cps_3[2] = 1.;
    cps_3[3] = 0.2;   cps_3[4] = 0.25; cps_3[5] = 1.;
    cps_3[6] = 0.35;   cps_3[7] = 0.30; cps_3[8] = 1.;
    cps_3[9] = 0.60;   cps_3[10] = 0.75; cps_3[11] = 1.;
    cps_3[12] = 0.75;   cps_3[13] = 0.90; cps_3[14] = 1.;

    d->rational_bezier_curve_3->create(order_3, cps_3);
    delete[] cps_3;
    //bi-degree 1, 1
    std::size_t nb_cp_u = 2;
    std::size_t nb_cp_v = 2;

    double *cps_s = new double[(3 + 1) * nb_cp_u * nb_cp_v];
    cps_s[0] = 1.;   cps_s[1] = 1.; cps_s[2] = -1.;  cps_s[3] = 1.;
    cps_s[4] = 1.;   cps_s[5] = 2.; cps_s[6] = 1.;  cps_s[7] = 1.;
    cps_s[8] = 2.;   cps_s[9] = 1.; cps_s[10] = 0.; cps_s[11] = 1.;
    cps_s[12] = 2.;  cps_s[13] = 2.;  cps_s[14] = 1.; cps_s[15] = 1.;

    dtkContinuousGeometryPrimitives::Point_3 p(0., 0., 0.);
    d->rational_bezier_surface->create(3, nb_cp_u, nb_cp_v, cps_s);
    delete[] cps_s;

    std::ofstream surface_file("bezier_surface.xyz");
    for(double i = 0.; i <= 1.; i += 0.01) {
        for(double j = 0.; j <= 1.; j += 0.01) {
            d->rational_bezier_surface->evaluatePoint(i, j, p.data());
            surface_file << p[0] << " " << p[1] << " " << p[2] << std::endl;
        }
    }
}

void blazeRationalBezierCurve2DElevatorTestCase::init(void)
{

}


void blazeRationalBezierCurve2DElevatorTestCase::testRun(void)
{
    blazeRationalBezierCurve2DElevator elevator = blazeRationalBezierCurve2DElevator();
    elevator.setInputRationalBezierCurve2D(d->rational_bezier_curve_1);
    elevator.setInputRationalBezierSurface(d->rational_bezier_surface);

    elevator.run();

    dtkRationalBezierCurve *curve_1 = elevator.rationalBezierCurve();

    dtkContinuousGeometryPrimitives::Point_3 cp(0., 0., 0.);
    double w = 0.;
    for(std::size_t i = 0; i < std::size_t(curve_1->degree() + 1); ++i) {
        curve_1->controlPoint(i, cp.data());
        curve_1->weight(i, &w);
        qDebug() << cp[0] << " " << cp[1] << " " << cp[2];
        qDebug() << w;
    }
    // QVERIFY(curve_1->degree() == 1);
    // curve_1->controlPoint(0, cp.data());
    // curve_1->weight(0, &w);
    // QVERIFY(cp[0] = 1.25); QVERIFY(cp[1] = 1.5); QVERIFY(cp[2] = 1.75); QVERIFY(w = 1.);
    // curve_1->controlPoint(1, cp.data());
    // curve_1->weight(1, &w);
    // QVERIFY(cp[0] = 1.75); QVERIFY(cp[1] = 1.5); QVERIFY(cp[2] = 1.25); QVERIFY(w = 1.);

    qDebug();
    elevator.setInputRationalBezierCurve2D(d->rational_bezier_curve_2);
    elevator.run();
    dtkRationalBezierCurve *curve_2 = elevator.rationalBezierCurve();
    for(std::size_t i = 0; i < curve_2->degree() + 1; ++i) {
        curve_2->controlPoint(i, cp.data());
        curve_2->weight(i, &w);
        qDebug() << cp[0] << " " << cp[1] << " " << cp[2];
        qDebug() << w;
    }
    QVERIFY(curve_2->degree() == 2);

    elevator.setInputRationalBezierCurve2D(d->rational_bezier_curve_3);
    elevator.run();
    dtkRationalBezierCurve *curve_3 = elevator.rationalBezierCurve();

    qDebug() << "Curve 2";
    for(std::size_t i = 0; i < curve_2->degree() + 1; ++i) {
        curve_2->controlPoint(i, cp.data());
        curve_2->weight(i, &w);
        qDebug() << cp[0] << " " << cp[1] << " " << cp[2];
        qDebug() << w;
    }
    qDebug() << "Curve 3";
    for(std::size_t i = 0; i < curve_3->degree() + 1; ++i) {
        curve_3->controlPoint(i, cp.data());
        curve_3->weight(i, &w);
        qDebug() << cp[0] << " " << cp[1] << " " << cp[2];
        qDebug() << w;
    }

    dtkContinuousGeometryPrimitives::Point_3 p(0., 0., 0.);
    std::ofstream curve_1_file("bezier_curve_1.xyz");
    for(double i = 0.; i <= 1.; i += 0.01) {
        curve_1->evaluatePoint(i, p.data());
        curve_1_file << p[0] << " " << p[1] << " " << p[2] << std::endl;
    }

    std::ofstream curve_2_file("bezier_curve_2.xyz");
    for(double i = 0.; i <= 1.; i += 0.01) {
        curve_2->evaluatePoint(i, p.data());
        curve_2_file << p[0] << " " << p[1] << " " << p[2] << std::endl;
    }

    std::ofstream curve_3_file("bezier_curve_3.xyz");
    for(double i = 0.; i <= 1.; i += 0.01) {
        curve_3->evaluatePoint(i, p.data());
        curve_3_file << p[0] << " " << p[1] << " " << p[2] << std::endl;
    }
}


void blazeRationalBezierCurve2DElevatorTestCase::cleanup(void)
{

}

void blazeRationalBezierCurve2DElevatorTestCase::cleanupTestCase(void)
{
    delete d->rational_bezier_curve_1;
    delete d->rational_bezier_curve_2;
    delete d->rational_bezier_curve_3;
    delete d->rational_bezier_surface;
}

DTKTEST_MAIN_NOGUI(blazeRationalBezierCurve2DElevatorTest, blazeRationalBezierCurve2DElevatorTestCase)

//
// blazeRationalBezierCurve2DElevatorTest.cpp ends here
