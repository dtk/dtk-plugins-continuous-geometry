// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "blazeRationalBezierCurve2DElevator.h"

#include <dtkContinuousGeometry.h>

#include <dtkContinuousGeometryUtils>

#include <dtkRationalBezierCurve2D>
#include <dtkRationalBezierCurve>
#include <dtkRationalBezierSurface>

#include "Blaze_without_warnings.h"

extern "C" {
    /* DGESDD prototype */
    void dgesdd_( char* jobz, int* m, int* n, double* a,
                  int* lda, double* s, double* u, int* ldu, double* vt, int* ldvt,
                  double* work, int* lwork, int* iwork, int* info );
}

// /////////////////////////////////////////////////////////////////
// blazeRationalBezierCurve2DElevatorPrivate
// /////////////////////////////////////////////////////////////////

class blazeRationalBezierCurve2DElevatorPrivate
{
private:
    template< typename K > using dtkMatrixMap = dtkContinuousGeometryTools::dtkMatrixMap< K >;

public:
    void run(void);
    int sdd(blaze::DynamicMatrix< double, blaze::columnMajor >& A, blaze::DynamicVector< double >& vector_ev, blaze::DynamicMatrix< double, blaze::columnMajor>& matrix_u, blaze::DynamicMatrix< double, blaze::columnMajor >& matrix_vt);
    std::size_t rank(const blaze::DynamicVector< double >& p_vector_sv, double p_norm_tolerance, double p_normalized_tolerance);

public:
    dtkRationalBezierCurve2D *rational_bezier_curve_2d;
    dtkRationalBezierSurface *rational_bezier_surface;
    dtkRationalBezierCurve *rational_bezier_curve_3d;
};

void blazeRationalBezierCurve2DElevatorPrivate::run(void)
{
    // ///////////////////////////////////////////////////////////////////
    // Checks that all inputs are set
    // ///////////////////////////////////////////////////////////////////
    if (rational_bezier_curve_2d == nullptr || rational_bezier_surface == nullptr) {
        dtkFatal() << "Not all inputs of blazeRationalBezierCurve2DElevator were set";
    }
    // ///////////////////////////////////////////////////////////////////
    // Recover the maximal possible degree of the 3d rational bezier curve (a 2d rational bezier curve embedded in a rational_bezier surface)
    // ///////////////////////////////////////////////////////////////////
    std::size_t max_degree = rational_bezier_curve_2d->degree() * (rational_bezier_surface->uDegree() + rational_bezier_surface->vDegree());
    bool is_degree_valid = false;

    while (!is_degree_valid) {
        //If the curve leads to a point (degenerate 2D bezier curve or bezier surface),
        if(max_degree == 0) {
            return;
        }
        // ///////////////////////////////////////////////////////////////////
        // Computes the minimum number of points required to solve a system of 3 * (n + 1) + n = 4 * n + 3 unknown
        // i.e. 3 * ceil( 4 * n + 3 / 3) >= 4 * n + 3
        // ///////////////////////////////////////////////////////////////////
        std::size_t nb_un = 4 * max_degree + 3;
        std::size_t nb_pts = std::ceil(double(nb_un) / 3.);
        blaze::DynamicVector< double > C(3 * nb_pts);
        blaze::DynamicMatrix< double, blaze::columnMajor > B_coefs(3 * nb_pts, 4 * max_degree + 3, 0.);

        // ///////////////////////////////////////////////////////////////////
        // Computes the difference between the number of equations and the number of unknowns
        // ///////////////////////////////////////////////////////////////////
        dtkContinuousGeometryPrimitives::Point_2 point_2d(0., 0.);
        dtkContinuousGeometryPrimitives::Point_3 point_3d(0., 0., 0.);
        dtkMatrixMap< double > binomial_coefficients;
        dtkContinuousGeometryTools::computeBinomialCoefficients(binomial_coefficients, max_degree + 1, max_degree + 1);
        for (std::size_t i = 0; i < nb_pts; ++i) {
            double p = double(i) / double(nb_pts - 1);
            rational_bezier_curve_2d->evaluatePoint(p, point_2d.data());
            rational_bezier_surface->evaluatePoint(point_2d[0], point_2d[1], point_3d.data());
            //Sets the first Xis, Yis, Zis
            B_coefs(i, 0) = std::pow((1 - p), max_degree); //B(_0^n)(p)
            B_coefs(i + nb_pts, 2 * max_degree + 1) = std::pow((1 - p), max_degree); //B(_0^n)(p)
            B_coefs(i + 2 * nb_pts, 3 * max_degree + 2) = std::pow((1 -p), max_degree); //B(_0^n)(p)

            for (std::size_t j = 1; j <= max_degree; ++j) {
                double B_j = binomial_coefficients[{max_degree, j}] * std::pow(p, j) * std::pow(1 - p, max_degree - j);
                B_coefs(i, j) = B_j; //B(_0^n)(p) -> B(_(n-1)^n)(p) for xis
                B_coefs(i + nb_pts, j + 2 * max_degree + 1) = B_j; //B(_0^n)(p) -> B(_(n-1)^n)(p) for yis
                B_coefs(i + 2 * nb_pts, j + 3 * max_degree + 2) = B_j; //B(_0^n)(p) -> B(_(n-1)^n)(p) for zis

                // ///////////////////////////////////////////////////////////////////
                // Sets the ws
                // ///////////////////////////////////////////////////////////////////
                B_coefs(i, j + max_degree) = -  point_3d[0] * B_j; // - B(_1^n)(p) * X0 -> - B(_n^n)(p) * X0
                B_coefs(i + nb_pts, j + max_degree) = - point_3d[1] * B_j; // - B(_1^n)(p) * Y0 -> - B(_n^n)(p) * Y0
                B_coefs(i + 2 * nb_pts, j + max_degree) = - B_j * point_3d[2]; // - B(_1^n)(p) * Z0 -> - B(_n^n)(p) * Z0
            }

            double B_0 = std::pow(1 - p, max_degree);
            C[i] = B_0 * point_3d[0];
            C[i + nb_pts] = B_0 * point_3d[1];
            C[i + 2 * nb_pts] = B_0 * point_3d[2];
        }

        // ///////////////////////////////////////////////////////////////////
        // Solves B_coef * P = C
        // 1. Computes the SVD of B_coef
        // ///////////////////////////////////////////////////////////////////
        blaze::DynamicVector< double > vector_ev;
        blaze::DynamicMatrix< double, blaze::columnMajor > matrix_u;
        blaze::DynamicMatrix< double, blaze::columnMajor > matrix_vt;
        sdd(B_coefs, vector_ev, matrix_u, matrix_vt);

        // ///////////////////////////////////////////////////////////////////
        // Checks that the matrix was full rank otherwise we would divide by 0
        // ///////////////////////////////////////////////////////////////////
        std::size_t min_dim = std::min(B_coefs.columns(), B_coefs.rows());
        std::size_t r = rank(vector_ev, 1e-10, 1e-10);
        std::size_t corank = min_dim - r;
        if(corank > 0) {
            // ///////////////////////////////////////////////////////////////////
            // If there is more than one element in the kernel, we cannot find the solution,
            // ///////////////////////////////////////////////////////////////////
            max_degree -= corank;
        } else {
            blaze::DynamicVector< double > P(4 * max_degree + 3);
            blaze::DynamicVector< double > UtC(3 * nb_pts);
            blaze::DynamicVector< double > VtP(4 * max_degree + 3);
            UtC = matrix_u.transpose() * C;
            for (std::size_t i = 0; i < vector_ev.size(); ++i) {
                VtP[i] = UtC[i] / vector_ev[i];
            }
            P =  matrix_vt.transpose() * VtP;
            dtkAbstractRationalBezierCurveData *rational_bezier_curve_data = dtkContinuousGeometry::abstractRationalBezierCurveData::pluginFactory().create("dtkRationalBezierCurveDataOn");
            if(rational_bezier_curve_data == nullptr) {
                dtkFatal() << "To use " << Q_FUNC_INFO << " , dtkAbstractRationalBezierCurveData must be compiled under the openNURBS plugins";
            }

            std::size_t out_nb_pts = max_degree + 1;
            double *cps = new double[out_nb_pts * 4];
            cps[0] = P[0]; //  / wo = 1
            cps[1] = P[2 * max_degree + 1]; //  / wo = 1
            cps[2] = P[3 * max_degree + 2]; //  / wo = 1
            cps[3] = 1.;
            std::cerr << rational_bezier_curve_2d->degree() << " " << rational_bezier_surface->uDegree() << " " << rational_bezier_surface->vDegree() << std::endl;
            std::cerr << max_degree << std::endl;
            std::cerr << std::endl;
            std::cerr << "points "<< cps[3] << std::endl;
            for(std::size_t i = 1; i < out_nb_pts; ++i) {
                cps[i * 4] = P[i] / P[i + max_degree];
                cps[i * 4 + 1] = P[i + 2 * max_degree + 1] / P[i + max_degree];
                cps[i * 4 + 2] = P[i + 3 * max_degree + 2] / P[i + max_degree];
                cps[i * 4 + 3] = P[i + max_degree];
                std::cerr << "points " << cps[i * 4 + 3] << std::endl;
            }
            std::cerr << std::endl;
            rational_bezier_curve_3d = new dtkRationalBezierCurve(rational_bezier_curve_data);
            rational_bezier_curve_data->create(out_nb_pts, cps);
            is_degree_valid = true;
        }
    }
}

//Make a pretty documentation to explain that the matrices must have the right size, and that it overwrites A contents
int blazeRationalBezierCurve2DElevatorPrivate::sdd(blaze::DynamicMatrix< double, blaze::columnMajor >& A, blaze::DynamicVector< double >& vector_ev, blaze::DynamicMatrix< double, blaze::columnMajor>& matrix_u, blaze::DynamicMatrix< double, blaze::columnMajor >& matrix_vt)
{
    int m = A.rows();
    int n = A.columns();
    int singN = (m<n)? m:n;

    vector_ev.resize(singN, false);
    matrix_u.resize(m, m, false);
    matrix_vt.resize(n, n, false);

    int lda = A.spacing();
    int ldu = matrix_u.spacing();
    int ldvt = matrix_vt.spacing();

    int info;
    int lwork;
    double wkopt;

    /* mode */
    char mode = 'A';
    /* iwork dimension should be at least 8*min(m,n) */
    std::vector<int> iwork(8*lda);

    /* Query and allocate the optimal workspace */
    lwork = -1;

    dgesdd_( &mode, &m, &n, A.data(), &lda, vector_ev.data(), matrix_u.data(), &ldu, matrix_vt.data(), &ldvt, &wkopt, &lwork, iwork.data(), &info );
    assert(info == 0);

    lwork = (int)wkopt;
    std::vector<double> work(lwork);

    /* Compute SVD (divide and conquer)*/
    dgesdd_( &mode, &m, &n, A.data(), &lda, vector_ev.data(), matrix_u.data(), &ldu, matrix_vt.data(), &ldvt, &work[0], &lwork, iwork.data(), &info );

    return info;
}

/*!
  Returns the rank of an application by reading the eigen values \a p_vector_ev.

  Returns the rank of an application by reading the eigen values of either GEV or SVD decompositions of its matricial representation, with \a p_norm_tolerance controlling that the first eigen value is not too small (check that the rank is not zero), and p_normalized_tolerance controlling that the other eigen values do not differ too much from the first one.
*/
std::size_t blazeRationalBezierCurve2DElevatorPrivate::rank(const blaze::DynamicVector< double >& p_vector_sv, double p_norm_tolerance, double p_normalized_tolerance)
{
    // /////////////////////////////////////////////////////////////////
    // Checks that the first singluar/eigen value is not "zero"
    // /////////////////////////////////////////////////////////////////
    if (std::fabs(p_vector_sv[0]) < p_norm_tolerance) {
        return 0;
    }
    // /////////////////////////////////////////////////////////////////
    // Normalizes the singular/eigen values according to the first value
    // /////////////////////////////////////////////////////////////////
    blaze::DynamicVector< double > normalized_vector_sv(p_vector_sv.size());
    for (std::size_t i = 0; i < p_vector_sv.size(); ++i) {
        normalized_vector_sv[i] = p_vector_sv[i] / p_vector_sv[0];
    }
    // /////////////////////////////////////////////////////////////////
    // Checks when a singuar value becomes negligeable compared to the previous eigen value
    //, if not negligeable increases rank, otherwise returns rank
    // /////////////////////////////////////////////////////////////////
    std::size_t rank = 1;
    while (rank < normalized_vector_sv.size()) {
        if (std::fabs(normalized_vector_sv[rank] / normalized_vector_sv[rank - 1]) > p_normalized_tolerance) {
            ++rank;
        } else {
            break;
        }
    }
    return rank;
}

/*!
  \class blazeRationalBezierCurve2DElevator
  \inmodule dtkPluginsContinuousGeometry
  \brief blazeRationalBezierCurve2DElevator is a Blaze implementation of the concept dtkRationalBezierCurve2DElevator.

  The following lines of code show how to instanciate and use the class in pratice :
  \code
  dtkRationalBezierCurve2DElevator *embedder = dtkContinuousGeometry::rationalBezierCurve2DElevator::pluginFactory().create("blazeRationalBezierCurve2DElevator");
  embedder->setInputRationalBezierCurve2D(...);
  embedder->setInputRationalBezierSurface(...);
  embedder->run();
  dtkRationalBezierCurve *rational_bezier_curve = embedder->rationalBezierCurve();
  \endcode
*/
blazeRationalBezierCurve2DElevator::blazeRationalBezierCurve2DElevator(void) : d(new blazeRationalBezierCurve2DElevatorPrivate)
{
    d->rational_bezier_curve_2d = nullptr;
    d->rational_bezier_surface = nullptr;
    d->rational_bezier_curve_3d = nullptr;
}

blazeRationalBezierCurve2DElevator::~blazeRationalBezierCurve2DElevator(void)
{
    delete d;
    d = nullptr;
}

/*! \fn blazeRationalBezierCurve2DElevator::setInputRationalBezierCurve2D(dtkRationalBezierCurve2D *curve)
  Provides the 2D rational Bezier curve to the embedder (\a curve).

  \a curve : the 2D rational Bezier curve to embed in 3D

  The curve must lie in the rational Bezier surface parameter space.
*/
void blazeRationalBezierCurve2DElevator::setInputRationalBezierCurve2D(dtkRationalBezierCurve2D *rational_bezier_curve_2d)
{
    d->rational_bezier_curve_2d = rational_bezier_curve_2d;
    d->rational_bezier_curve_3d = nullptr;
}

/*! \fn blazeRationalBezierCurve2DElevator::setInputRationalBezierSurface(dtkRationalBezierSurface *surface)
  Provides the rational Bezier surface to the embedder (\a surface).

  \a surface : the rational Bezier surface used to embed the curve
*/
void blazeRationalBezierCurve2DElevator::setInputRationalBezierSurface(dtkRationalBezierSurface *rational_bezier_surface)
{
    d->rational_bezier_surface = rational_bezier_surface;
    d->rational_bezier_curve_3d = nullptr;
}

/*! \fn blazeRationalBezierCurve2DElevator::run(void)
  Embeds into 3D the 2D rational Bezier curve provided with \l setInputRationalBezierCurve2D(dtkRationalBezierCurve2D *curve) with the rational Bezier surface provided with \l setInputRationalBezierSurface(dtkRationalBezierSurface *surface).
*/
void blazeRationalBezierCurve2DElevator::run(void)
{
    d->run();
}

/*! \fn void blazeRationalBezierCurve2DElevator::rationalBezierCurve(void)
  Returns the 3D rational Bezier curve computed in the \l run(void) method.
*/
dtkRationalBezierCurve *blazeRationalBezierCurve2DElevator::rationalBezierCurve(void)
{
    if (d->rational_bezier_curve_3d == nullptr) {
        dtkInfo() << "The run method has probably not been called, or there was an issue computing the embedded bezier curve";
    }
    return d->rational_bezier_curve_3d;
}


//
// blazeRationalBezierCurve2DElevator.cpp ends here
