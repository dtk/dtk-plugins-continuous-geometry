// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkRationalBezierCurveLossLessDegreeReductor>

#include <blazeRationalBezierCurveLossLessDegreeReductorExport.h>

class blazeRationalBezierCurveLossLessDegreeReductorPrivate;

class dtkRationalBezierCurve;

class BLAZERATIONALBEZIERCURVELOSSLESSDEGREEREDUCTOR_EXPORT blazeRationalBezierCurveLossLessDegreeReductor : public dtkRationalBezierCurveLossLessDegreeReductor
{
public:
     blazeRationalBezierCurveLossLessDegreeReductor(void);
    ~blazeRationalBezierCurveLossLessDegreeReductor(void);

public:
    void setRationalBezierCurve(dtkRationalBezierCurve *) final;

    void run(void) final;

    dtkRationalBezierCurve *rationalBezierCurve(void) final;

protected:
    blazeRationalBezierCurveLossLessDegreeReductorPrivate *d;
};

// ///////////////////////////////////////////////////////////////////

inline dtkRationalBezierCurveLossLessDegreeReductor* blazeRationalBezierCurveLossLessDegreeReductorCreator(void)
{
    return new blazeRationalBezierCurveLossLessDegreeReductor();
}

//
// blazeRationalBezierCurveLossLessDegreeReductor.h ends here
