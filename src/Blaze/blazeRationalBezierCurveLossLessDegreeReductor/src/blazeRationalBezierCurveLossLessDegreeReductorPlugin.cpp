// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "blazeRationalBezierCurveLossLessDegreeReductor.h"
#include "blazeRationalBezierCurveLossLessDegreeReductorPlugin.h"

#include "dtkContinuousGeometry.h"

#include <dtkCore>

// ///////////////////////////////////////////////////////////////////
// blazeRationalBezierCurveLossLessDegreeReductorPlugin
// ///////////////////////////////////////////////////////////////////

void blazeRationalBezierCurveLossLessDegreeReductorPlugin::initialize(void)
{
    dtkContinuousGeometry::rationalBezierCurveLossLessDegreeReductor::pluginFactory().record("blazeRationalBezierCurveLossLessDegreeReductor", blazeRationalBezierCurveLossLessDegreeReductorCreator);
}

void blazeRationalBezierCurveLossLessDegreeReductorPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(blazeRationalBezierCurveLossLessDegreeReductor)

//
// blazeRationalBezierCurveLossLessDegreeReductorPlugin.cpp ends here
