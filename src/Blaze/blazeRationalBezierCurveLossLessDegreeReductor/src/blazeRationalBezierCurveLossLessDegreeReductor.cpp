// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "blazeRationalBezierCurveLossLessDegreeReductor.h"

#include <dtkContinuousGeometry.h>

#include <dtkContinuousGeometryUtils>

#include <dtkAbstractRationalBezierCurveData>
#include <dtkRationalBezierCurve>

#include "Blaze_without_warnings.h"

// /////////////////////////////////////////////////////////////////
// blazeRationalBezierCurveLossLessDegreeReductorPrivate
// /////////////////////////////////////////////////////////////////

class blazeRationalBezierCurveLossLessDegreeReductorPrivate
{
public:
    void computeBersteinToPowerMatrix(blaze::DynamicMatrix< double, blaze::columnMajor>& r_mat, std::size_t p_d, blaze::DynamicMatrix< double, blaze::columnMajor>& p_binomials);
    void computeBinomialCoefficients(blaze::DynamicMatrix< double, blaze::columnMajor>& r_binomial_coefficients, std::size_t n, std::size_t k);
public:
    dtkRationalBezierCurve *in_bezier_curve;
    dtkRationalBezierCurve *out_bezier_curve;
};

/*!
  Computes the transfer matrix from the Berstein polynomial basis of degree d to the power polynomial basis of degree d
*/
void blazeRationalBezierCurveLossLessDegreeReductorPrivate::computeBersteinToPowerMatrix(blaze::DynamicMatrix< double, blaze::columnMajor>& r_mat, std::size_t p_d, blaze::DynamicMatrix< double, blaze::columnMajor>& p_binomial_coefficients) {
    r_mat.resize(p_d + 1, p_d + 1, false);
    for (std::size_t i = 0; i <= p_d; ++i) {
        for (std::size_t j = 0; j <= p_d; ++j) {
            r_mat(i, j) = p_binomial_coefficients(p_d - i, p_d - j) / p_binomial_coefficients(p_d, j);
        }
    }
}

void blazeRationalBezierCurveLossLessDegreeReductorPrivate::computeBinomialCoefficients(blaze::DynamicMatrix< double, blaze::columnMajor>& r_binomial_coefficients, std::size_t n, std::size_t k)
{
    r_binomial_coefficients.resize(n + 1, n + 1, false);
    // Setup the first line
    r_binomial_coefficients(0, 0) = 1.0 ;
    for (std::size_t j = k; j > 0; --j) {
        r_binomial_coefficients(0, j) = 0.;
    }

    //Setup the first row
    for (std::size_t j = 0; j <= n; ++j) {
        r_binomial_coefficients(j, 0) = 1.;
    }

    // Setup the other lines
    for (std::size_t i = 0; i < n; ++i) {
        r_binomial_coefficients(i + 1, 0) = 1.;
        for(std::size_t j = 1; j <= k  ; ++j) {
            if( i + 1 < j){
                r_binomial_coefficients(i + 1, j) = 0.;
            } else{
                r_binomial_coefficients(i + 1, j) = r_binomial_coefficients(i, j) + r_binomial_coefficients(i, j - 1);
            }
        }
    }
}

/*!
  \class blazeRationalBezierCurveLossLessDegreeReductor
  \inmodule dtkPluginsContinuousGeometry
  \brief blazeRationalBezierCurveLossLessDegreeReductor is a Blaze implementation of the concept \l {dtkRationalBezierCurveLossLessDegreeReductor}.

  \b {This plugin is not yet working.}

  The following lines of code show how to instanciate and use the class in pratice :
  \code
  dtkRationalBezierCurveLossLessDegreeReductor *reductor = dtkContinuousGeometry::rationalBezierCurve2DElevator::pluginFactory().create("blazeRationalBezierCurveLossLessDegreeReductor");
  reductor->setRationalBezierCurve(...);
  reductor->run();
  dtkRationalBezierCurve *rational_bezier_curve = reductor->rationalBezierCurve();
  \endcode
*/
blazeRationalBezierCurveLossLessDegreeReductor::blazeRationalBezierCurveLossLessDegreeReductor(void) : dtkRationalBezierCurveLossLessDegreeReductor(), d(new blazeRationalBezierCurveLossLessDegreeReductorPrivate)
{
    d->in_bezier_curve = nullptr;
    d->out_bezier_curve = nullptr;
}

blazeRationalBezierCurveLossLessDegreeReductor::~blazeRationalBezierCurveLossLessDegreeReductor(void)
{
    delete d;
    d = nullptr;
}

/*! \fn blazeRationalBezierCurveLossLessDegreeReductor::setRationalBezierCurve(dtkRationalBezierCurve *curve)
  Provides the rational Bezier curve to the reductor (\a curve).

  \a curve : the rational Bezier curve which degree is to try to reduce
*/
void blazeRationalBezierCurveLossLessDegreeReductor::setRationalBezierCurve(dtkRationalBezierCurve *bezier_curve)
{
    d->in_bezier_curve = bezier_curve;
}

/*! \fn blazeRationalBezierCurveLossLessDegreeReductor::run(void)
  Tries to reduce rational Bezier curve degree provided with \l setRationalBezierCurve(dtkRationalBezierCurve *curve) without changing the geometry.
*/
void blazeRationalBezierCurveLossLessDegreeReductor::run(void)
{
    std::size_t in_degree = d->in_bezier_curve->degree();
    // ///////////////////////////////////////////////////////////////////
    // Computes the binomial coefficients
    // ///////////////////////////////////////////////////////////////////
    blaze::DynamicMatrix< double, blaze::columnMajor> bin_coefs;
    d->computeBinomialCoefficients(bin_coefs, in_degree, in_degree);
    std::cerr << bin_coefs << std::endl;
    // ///////////////////////////////////////////////////////////////////
    // Computes change of basis from Berstein to Polynomials
    // ///////////////////////////////////////////////////////////////////
    blaze::DynamicMatrix< double, blaze::columnMajor> bernstein_to_polynomials;
    d->computeBersteinToPowerMatrix(bernstein_to_polynomials, in_degree, bin_coefs);
    std::cerr << bernstein_to_polynomials << std::endl;
    bernstein_to_polynomials.transpose();
    std::cerr << bernstein_to_polynomials << std::endl;
    // ///////////////////////////////////////////////////////////////////
    // Multiplies the CoB matrix by the points coordinates
    // ///////////////////////////////////////////////////////////////////
    blaze::DynamicMatrix< double, blaze::columnMajor> bernstein_coordinates(in_degree + 1, 3);
    dtkContinuousGeometryPrimitives::Point_3 p(0., 0., 0.);
    for (std::size_t i = 0; i <= in_degree; ++i) {
        d->in_bezier_curve->controlPoint(i, p.data());
        bernstein_coordinates(i, 0) = p[0];
        bernstein_coordinates(i, 1) = p[1];
        bernstein_coordinates(i, 2) = p[2];
    }
    std::cerr << bernstein_coordinates << std::endl;

    blaze::DynamicMatrix< double, blaze::columnMajor> polynomial_coordinates = bernstein_to_polynomials * bernstein_coordinates;

    std::cerr << polynomial_coordinates << std::endl;
    // ///////////////////////////////////////////////////////////////////
    // Checks for 0s in the highest degrees and computes the max degree
    // ///////////////////////////////////////////////////////////////////

    // ///////////////////////////////////////////////////////////////////
    // Computes the CoB matrix from polynomials to Bernstein
    // ///////////////////////////////////////////////////////////////////

    // ///////////////////////////////////////////////////////////////////
    // Multiplies the CoB matrix by the points coordinates in the polynomial basis
    // ///////////////////////////////////////////////////////////////////

    // ///////////////////////////////////////////////////////////////////
    // Creates the new curve
    // ///////////////////////////////////////////////////////////////////
    dtkAbstractRationalBezierCurveData *data = dtkContinuousGeometry::abstractRationalBezierCurveData::pluginFactory().create("dtkRationalBezierCurveDataOn");
    if (data == nullptr) {
        dtkFatal() << "The dtkAbstractRationalBezierCurveData could not be loaded by the factory under the openNURBS implementation";
    }
    d->out_bezier_curve = new dtkRationalBezierCurve(data);
}

/*! \fn blazeRationalBezierCurveLossLessDegreeReductor::rationalBezierCurve(void)
  Returns the degree-reduced rational Bezier curve computed in the \l run(void) method.
*/
dtkRationalBezierCurve* blazeRationalBezierCurveLossLessDegreeReductor::rationalBezierCurve(void)
{
    return d->out_bezier_curve;
}

//
// blazeRationalBezierCurveLossLessDegreeReductor.cpp ends here
