// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkRationalBezierCurveLossLessDegreeReductor.h>

#include <blazeRationalBezierCurveLossLessDegreeReductorExport.h>

#include <dtkCore>

class BLAZERATIONALBEZIERCURVELOSSLESSDEGREEREDUCTOR_EXPORT blazeRationalBezierCurveLossLessDegreeReductorPlugin : public dtkRationalBezierCurveLossLessDegreeReductorPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkRationalBezierCurveLossLessDegreeReductorPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.blazeRationalBezierCurveLossLessDegreeReductorPlugin" FILE "blazeRationalBezierCurveLossLessDegreeReductorPlugin.json")

public:
     blazeRationalBezierCurveLossLessDegreeReductorPlugin(void) {}
    ~blazeRationalBezierCurveLossLessDegreeReductorPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

//
// blazeRationalBezierCurveLossLessDegreeReductorPlugin.h ends here
