// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <QTest>

class blazeRationalBezierCurveLossLessDegreeReductorTestCasePrivate;

class blazeRationalBezierCurveLossLessDegreeReductorTestCase : public QObject
{
    Q_OBJECT

public:
    blazeRationalBezierCurveLossLessDegreeReductorTestCase(void);
    ~blazeRationalBezierCurveLossLessDegreeReductorTestCase(void);

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testRun(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);

private:
    blazeRationalBezierCurveLossLessDegreeReductorTestCasePrivate* d;
};

//
// blazeRationalBezierCurveLossLessDegreeReductorTest.h ends here
