// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "blazeRationalBezierCurveLossLessDegreeReductorTest.h"

#include <blazeRationalBezierCurveLossLessDegreeReductor.h>

#include <dtkContinuousGeometry.h>
#include <dtkContinuousGeometrySettings.h>

#include <dtkAbstractRationalBezierCurveData>
#include <dtkRationalBezierCurve>

#include <dtkTest>

// ///////////////////////////////////////////////////////////////////
// D-Pointer implementation
// ///////////////////////////////////////////////////////////////////

class blazeRationalBezierCurveLossLessDegreeReductorTestCasePrivate{
public:
    dtkRationalBezierCurve *rational_bezier_curve;
    dtkAbstractRationalBezierCurveData *rational_bezier_curve_data;

    double tolerance = 1e-15;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

blazeRationalBezierCurveLossLessDegreeReductorTestCase::blazeRationalBezierCurveLossLessDegreeReductorTestCase(void):d(new blazeRationalBezierCurveLossLessDegreeReductorTestCasePrivate())
{
    dtkLogger::instance().attachConsole();

    dtkContinuousGeometrySettings settings;
    settings.beginGroup("continuous-geometry");
    dtkContinuousGeometry::initialize(settings.value("plugins").toString());
    settings.endGroup();
}

blazeRationalBezierCurveLossLessDegreeReductorTestCase::~blazeRationalBezierCurveLossLessDegreeReductorTestCase(void)
{

}

void blazeRationalBezierCurveLossLessDegreeReductorTestCase::initTestCase(void)
{
    d->rational_bezier_curve_data = dtkContinuousGeometry::abstractRationalBezierCurveData::pluginFactory().create("dtkRationalBezierCurveDataOn");
    if (d->rational_bezier_curve_data == nullptr) {
        dtkFatal() << "The dtkAbstractRationalBezierCurveData could not be loaded by the factory under the openNURBS implementation";
    }
    d->rational_bezier_curve = new dtkRationalBezierCurve(d->rational_bezier_curve_data);

    //degree 3
    std::size_t order = 4;
    double* cps = new double[(3 + 1) * order];
    cps[0] = 0.;  cps[1] = 0.;  cps[2] = 0.; cps[3] = 1.;
    cps[4] = 1.;  cps[5] = 0.;  cps[6] = 0.; cps[7] = 1.;
    cps[8] = 2.;  cps[9] = 0.;  cps[10] = 0.; cps[11] = 1.;
    cps[12] = 3.; cps[13] = 0.; cps[14] = 0.; cps[15] = 1.;

    d->rational_bezier_curve->create(order, cps);
}

void blazeRationalBezierCurveLossLessDegreeReductorTestCase::init(void)
{

}


void blazeRationalBezierCurveLossLessDegreeReductorTestCase::testRun(void)
{
    blazeRationalBezierCurveLossLessDegreeReductor reductor = blazeRationalBezierCurveLossLessDegreeReductor();
    reductor.setRationalBezierCurve(d->rational_bezier_curve);

    reductor.run();
}


void blazeRationalBezierCurveLossLessDegreeReductorTestCase::cleanup(void)
{

}

void blazeRationalBezierCurveLossLessDegreeReductorTestCase::cleanupTestCase(void)
{
    delete d->rational_bezier_curve;
}

DTKTEST_MAIN_NOGUI(blazeRationalBezierCurveLossLessDegreeReductorTest, blazeRationalBezierCurveLossLessDegreeReductorTestCase)

//
// blazeRationalBezierCurveLossLessDegreeReductorTest.cpp ends here
