## Version: $Id$
##
######################################################################
##
### Commentary:
##
######################################################################
##
### Change Log:
##
######################################################################
##
### Code:

## ###################################################################
## Input - introspected
## ###################################################################

set(${PROJECT_NAME}Test_SOURCES
  ${PROJECT_NAME}Test.cpp
  )

set(${PROJECT_NAME}Test_HEADERS
  ${PROJECT_NAME}Test.h
  )

## ###################################################################
## Input - introspected
## ###################################################################
create_test_sourcelist(
  ${PROJECT_NAME}Test_SOURCES_TST
  ${PROJECT_NAME}Tests.cpp
  ${${PROJECT_NAME}Test_SOURCES})

## ###################################################################
## Build rules
## ###################################################################

add_executable(${PROJECT_NAME}Test
  ${${PROJECT_NAME}Test_SOURCES_TST}
  ${${PROJECT_NAME}Test_SOURCES}
  ${${PROJECT_NAME}Test_HEADERS}
  )

## ###################################################################
## Link rules
## ###################################################################
target_link_libraries(${PROJECT_NAME}Test Qt5::Core Qt5::Test)

target_link_libraries(${PROJECT_NAME}Test dtkCore)
target_link_libraries(${PROJECT_NAME}Test dtkContinuousGeometryCore)

target_link_libraries(${PROJECT_NAME}Test ${PROJECT_NAME})

## ###################################################################
## Test rules
## ###################################################################

add_test(NAME ${PROJECT_NAME}Test
  WORKING_DIRECTORY  ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}
  COMMAND ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/${PROJECT_NAME}Test
  ${PROJECT_NAME}Test
 )

######################################################################
### CMakeLists.txt ends here
