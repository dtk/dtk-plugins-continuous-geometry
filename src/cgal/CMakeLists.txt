## Version: $Id$
##
######################################################################
##
### Commentary:
##
######################################################################
##
### Change Log:
##
######################################################################
##
### Code:

if(NOT CGAL_FOUND)
  message(WARNING "CGAL was not found. Please set CGAL_DIR. Instead CGAL_DIR plugins will not be compiled.")
endif(NOT CGAL_FOUND)

if(CGAL_FOUND)
  include(${CGAL_USE_FILE})
  if(NOT TARGET CGAL::CGAL)
    include_directories(${CGAL_INCLUDE_DIRS})
    include_directories( ${cgalMeshBridge_INCLUDE_DIRS})
  endif()

  ## ###################################################################
  ## Dependencies - internal
  ## ###################################################################
  include_directories(dtkTrimLoopDataCgal/src)
  include_directories(dtkNurbsPolyhedralSurfaceCgal/src)
  include_directories(dtkBRepReaderCgal/src)

  ## #################################################################
  ## Inputs
  ## #################################################################
  add_subdirectory(dtkTrimLoopDataCgal)
  add_subdirectory(dtkNurbsPolyhedralSurfaceCgal)
  add_subdirectory(dtkBRepReaderCgal)
  ######################################################################

endif(CGAL_FOUND)

# ######################################################################
# ### CMakeLists.txt ends here
