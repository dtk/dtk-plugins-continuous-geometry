// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <QTest>

class dtkNurbsPolyhedralSurfaceCgalTestCasePrivate;

class dtkNurbsPolyhedralSurfaceCgalTestCase : public QObject
{
    Q_OBJECT

public:
    dtkNurbsPolyhedralSurfaceCgalTestCase(void);
    ~dtkNurbsPolyhedralSurfaceCgalTestCase(void);

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testInitialize(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);

private:
    dtkNurbsPolyhedralSurfaceCgalTestCasePrivate* d;
};

//
// dtkNurbsPolyhedralSurfaceCgalTest.h ends here
