// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkNurbsPolyhedralSurfaceCgalTest.h"

#include "dtkNurbsPolyhedralSurfaceCgal.h"

#include <dtkTrimLoopDataCgal.h>

#include <dtkContinuousGeometry.h>
#include <dtkAbstractNurbsSurfaceData>
#include <dtkNurbsSurface>
#include <dtkAbstractNurbsCurve2DData>
#include <dtkNurbsCurve2D>
#include <dtkTrim>
#include <dtkTrimLoop>
#include <dtkTopoTrim>

#include <dtkContinuousGeometrySettings>
#include <dtkContinuousGeometryUtils>
#include <dtkTest>

// ///////////////////////////////////////////////////////////////////
// D-Pointer implementation
// ///////////////////////////////////////////////////////////////////

class dtkNurbsPolyhedralSurfaceCgalTestCasePrivate
{
public:
    dtkNurbsSurface *nurbs_surface;
    dtkNurbsPolyhedralSurfaceCgal *nurbs_polyhedral_surface_cgal;
    dtkAbstractNurbsSurfaceData *nurbs_surface_data;

    dtkAbstractNurbsCurve2DData* nurbs_curve_data_inner_1;
    dtkAbstractNurbsCurve2DData* nurbs_curve_data_inner_2;
    dtkAbstractNurbsCurve2DData* nurbs_curve_data_inner_3;
    dtkNurbsCurve2D *nurbs_curve_inner_1;
    dtkNurbsCurve2D *nurbs_curve_inner_2;
    dtkNurbsCurve2D *nurbs_curve_inner_3;
    dtkTrim *trim_inner_1;
    dtkTrim *trim_inner_2;
    dtkTrim *trim_inner_3;
    dtkTopoTrim *topo_trim_inner_1;
    dtkTopoTrim *topo_trim_inner_2;
    dtkTopoTrim *topo_trim_inner_3;
    dtkTrimLoopDataCgal* trim_loop_data_inner;
    dtkTrimLoop *trim_loop_inner;

    dtkAbstractNurbsCurve2DData* nurbs_curve_data_outer_1;
    dtkAbstractNurbsCurve2DData* nurbs_curve_data_outer_2;
    dtkAbstractNurbsCurve2DData* nurbs_curve_data_outer_3;
    dtkAbstractNurbsCurve2DData* nurbs_curve_data_outer_4;
    dtkNurbsCurve2D *nurbs_curve_outer_1;
    dtkNurbsCurve2D *nurbs_curve_outer_2;
    dtkNurbsCurve2D *nurbs_curve_outer_3;
    dtkNurbsCurve2D *nurbs_curve_outer_4;
    dtkTopoTrim *topo_trim_outer_1;
    dtkTopoTrim *topo_trim_outer_2;
    dtkTopoTrim *topo_trim_outer_3;
    dtkTopoTrim *topo_trim_outer_4;
    dtkTrim *trim_outer_1;
    dtkTrim *trim_outer_2;
    dtkTrim *trim_outer_3;
    dtkTrim *trim_outer_4;
    dtkTrimLoopDataCgal* trim_loop_data_outer;
    dtkTrimLoop *trim_loop_outer;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkNurbsPolyhedralSurfaceCgalTestCase::dtkNurbsPolyhedralSurfaceCgalTestCase(void):d(new dtkNurbsPolyhedralSurfaceCgalTestCasePrivate())
{
    dtkLogger::instance().attachConsole();

    dtkContinuousGeometrySettings settings;
    settings.beginGroup("continuous-geometry");
    dtkContinuousGeometry::initialize(settings.value("plugins").toString());
    settings.endGroup();
}

dtkNurbsPolyhedralSurfaceCgalTestCase::~dtkNurbsPolyhedralSurfaceCgalTestCase(void) {}

void dtkNurbsPolyhedralSurfaceCgalTestCase::initTestCase(void)
{
    /* Forces openNURBS plugins to be compiled and working
       TODO : change to default implementation
    */
    d->nurbs_curve_data_inner_1 = dtkContinuousGeometry::abstractNurbsCurve2DData::pluginFactory().create("dtkNurbsCurve2DDataOn");
    d->nurbs_curve_data_inner_2 = dtkContinuousGeometry::abstractNurbsCurve2DData::pluginFactory().create("dtkNurbsCurve2DDataOn");
    d->nurbs_curve_data_inner_3 = dtkContinuousGeometry::abstractNurbsCurve2DData::pluginFactory().create("dtkNurbsCurve2DDataOn");

    d->nurbs_curve_data_outer_1 = dtkContinuousGeometry::abstractNurbsCurve2DData::pluginFactory().create("dtkNurbsCurve2DDataOn");
    d->nurbs_curve_data_outer_2 = dtkContinuousGeometry::abstractNurbsCurve2DData::pluginFactory().create("dtkNurbsCurve2DDataOn");
    d->nurbs_curve_data_outer_3 = dtkContinuousGeometry::abstractNurbsCurve2DData::pluginFactory().create("dtkNurbsCurve2DDataOn");
    d->nurbs_curve_data_outer_4 = dtkContinuousGeometry::abstractNurbsCurve2DData::pluginFactory().create("dtkNurbsCurve2DDataOn");

    d->nurbs_surface_data = dtkContinuousGeometry::abstractNurbsSurfaceData::pluginFactory().create("dtkNurbsSurfaceDataOn");

    if (d->nurbs_curve_data_inner_1 == nullptr || d->nurbs_curve_data_inner_2 == nullptr || d->nurbs_curve_data_inner_3 == nullptr || d->nurbs_curve_data_outer_1 == nullptr || d->nurbs_curve_data_outer_2 == nullptr || d->nurbs_curve_data_outer_3 == nullptr || d->nurbs_curve_data_outer_4 == nullptr) {
        dtkFatal() << "The dtkAbstractNurbsCurve2DData could not be loaded by the factory under the openNURBS implementation";
    }
    if (d->nurbs_surface_data == nullptr) {
       dtkFatal() << "The dtkAbstractNurbsSurfaceData could not be loaded by the factory under the openNURBS implementation";
    }

    d->nurbs_curve_inner_1 = new dtkNurbsCurve2D(d->nurbs_curve_data_inner_1);
    d->nurbs_curve_inner_2 = new dtkNurbsCurve2D(d->nurbs_curve_data_inner_2);
    d->nurbs_curve_inner_3 = new dtkNurbsCurve2D(d->nurbs_curve_data_inner_3);

    d->nurbs_curve_outer_1 = new dtkNurbsCurve2D(d->nurbs_curve_data_outer_1);
    d->nurbs_curve_outer_2 = new dtkNurbsCurve2D(d->nurbs_curve_data_outer_2);
    d->nurbs_curve_outer_3 = new dtkNurbsCurve2D(d->nurbs_curve_data_outer_3);
    d->nurbs_curve_outer_4 = new dtkNurbsCurve2D(d->nurbs_curve_data_outer_4);

    //bi-degree 1,2
    std::size_t nb_cp_u = 2;
    std::size_t nb_cp_v = 3;
    std::size_t order_u = 2;
    std::size_t order_v = 3;

    std::vector<double> knots_u(nb_cp_u + order_u - 2);
    /* -6. */ knots_u[0] = -6.; knots_u[1] = 6.; /* 6. */
    std::vector<double> knots_v(nb_cp_v + order_v - 2);
    /* -2. */ knots_v[0] = -2.; knots_v[1] = -2.; knots_v[2] = 2.; knots_v[3] = 2.; /* 2. */

    std::vector<double> cps((3 + 1) * nb_cp_u * nb_cp_v);
    cps[0] = 0.;   cps[1] = 0.; cps[2] = 3.;  cps[3] = 1.;
    cps[4] = 0.;   cps[5] = 1.; cps[6] = 2.;  cps[7] = 1.;
    cps[8] = 0.;   cps[9] = 2.; cps[10] = 3.; cps[11] = 1.;
    cps[12] = 1.;  cps[13] = 0.;  cps[14] = 1.; cps[15] = 1.;
    cps[16] = 1.;  cps[17] = 1.;  cps[18] = 0.; cps[19] = 1.;
    cps[20] = 1.;  cps[21] = 2.; cps[22] = -1.;  cps[23] = 1.;

    d->nurbs_surface = new dtkNurbsSurface(d->nurbs_surface_data);
    d->nurbs_surface->create(3, nb_cp_u, nb_cp_v, order_u, order_v, knots_u.data(), knots_v.data(), cps.data());

    std::size_t nb_cp_inner = 3;
    std::size_t order_inner = 3;

    std::vector<double> knots_inner_1(nb_cp_inner + order_inner - 2);
    /* 0. */ knots_inner_1[0] = 0.; knots_inner_1[1] = 0.; knots_inner_1[2] = 1.; knots_inner_1[3] = 1.; /* 1.*/
    std::vector<double> knots_inner_2(nb_cp_inner + order_inner - 2);
    /* 0. */ knots_inner_2[0] = 0.; knots_inner_2[1] = 0.; knots_inner_2[2] = 1.; knots_inner_2[3] = 1.; /* 1.*/
    std::vector<double> knots_inner_3(nb_cp_inner + order_inner - 2);
    /* 0. */ knots_inner_3[0] = 0.; knots_inner_3[1] = 0.; knots_inner_3[2] = 1.; knots_inner_3[3] = 1.; /* 1.*/

    std::vector<double> cps_inner_1((2 + 1) * nb_cp_inner);
    cps_inner_1[0] = 0.;   cps_inner_1[1] = 0.; cps_inner_1[2] = 1.;
    cps_inner_1[3] = 0.;   cps_inner_1[4] = 3.46; cps_inner_1[5] = 1.;
    cps_inner_1[6] = 2.;   cps_inner_1[7] = 3.46; cps_inner_1[8] = 1.;
    std::vector<double> cps_inner_2((2 + 1) * nb_cp_inner);
    cps_inner_2[0] = 2.;   cps_inner_2[1] = 3.46; cps_inner_2[2] = 1.;
    cps_inner_2[3] = 4.;   cps_inner_2[4] = 3.46; cps_inner_2[5] = 1.;
    cps_inner_2[6] = 4.;   cps_inner_2[7] = 0.; cps_inner_2[8] = 1.;
    std::vector<double> cps_inner_3((2 + 1) * nb_cp_inner);
    cps_inner_3[0] = 4.;   cps_inner_3[1] = 0.; cps_inner_3[2] = 1.;
    cps_inner_3[3] = 2.;   cps_inner_3[4] = -1.732; cps_inner_3[5] = 1.;
    cps_inner_3[6] = 0.;   cps_inner_3[7] = 0.; cps_inner_3[8] = 1.;

    d->nurbs_curve_inner_1->create(nb_cp_inner, order_inner, knots_inner_1.data(), cps_inner_1.data());
    d->nurbs_curve_inner_2->create(nb_cp_inner, order_inner, knots_inner_2.data(), cps_inner_2.data());
    d->nurbs_curve_inner_3->create(nb_cp_inner, order_inner, knots_inner_3.data(), cps_inner_3.data());

    d->topo_trim_inner_1 = new dtkTopoTrim();
    d->topo_trim_inner_2 = new dtkTopoTrim();
    d->topo_trim_inner_3 = new dtkTopoTrim();

    d->trim_inner_1 = new dtkTrim(*d->nurbs_surface, *d->nurbs_curve_inner_1, d->topo_trim_inner_1);
    d->trim_inner_2 = new dtkTrim(*d->nurbs_surface, *d->nurbs_curve_inner_2, d->topo_trim_inner_2);
    d->trim_inner_3 = new dtkTrim(*d->nurbs_surface, *d->nurbs_curve_inner_3, d->topo_trim_inner_3);

    d->trim_loop_data_inner = new dtkTrimLoopDataCgal();
    d->trim_loop_inner = new dtkTrimLoop(dynamic_cast< dtkAbstractTrimLoopData * >(d->trim_loop_data_inner));

    d->trim_loop_inner->trims().push_back(d->trim_inner_1);
    d->trim_loop_inner->trims().push_back(d->trim_inner_2);
    d->trim_loop_inner->trims().push_back(d->trim_inner_3);

    d->trim_loop_inner->setType(dtkContinuousGeometryEnums::TrimLoopType::outer);

    d->nurbs_surface->trimLoops().push_back(d->trim_loop_inner);

    // ///////////////////////////////////////////////////////////////////
    //Outer loop
    std::size_t nb_cp_outer = 2;
    std::size_t order_outer = 2;

    std::vector<double> knots_outer_1(nb_cp_outer + order_outer - 2);
    /* 0. */ knots_outer_1[0] = 0.; knots_outer_1[1] = 1.; /* 1.*/
    std::vector<double> knots_outer_2(nb_cp_outer + order_outer - 2);
    /* 0. */ knots_outer_2[0] = 0.; knots_outer_2[1] = 1.; /* 1.*/
    std::vector<double> knots_outer_3(nb_cp_outer + order_outer - 2);
    /* 0. */ knots_outer_3[0] = 0.; knots_outer_3[1] = 1.; /* 1.*/
    std::vector<double> knots_outer_4(nb_cp_outer + order_outer - 2);
    /* 0. */ knots_outer_4[0] = 0.; knots_outer_4[1] = 1.; /* 1.*/

    std::vector<double> cps_outer_1((2 + 1) * nb_cp_outer);
    cps_outer_1[0] = -6.;   cps_outer_1[1] = -2.; cps_outer_1[2] = 1.;
    cps_outer_1[3] = 6.;   cps_outer_1[4] = -2.; cps_outer_1[5] = 1.;
    std::vector<double> cps_outer_2((2 + 1) * nb_cp_outer);
    cps_outer_2[0] = -2.;   cps_outer_2[1] = 6.; cps_outer_2[2] = 1.;
    cps_outer_2[3] = 2.;   cps_outer_2[4] = 6.; cps_outer_2[5] = 1.;
    std::vector<double> cps_outer_3((2 + 1) * nb_cp_outer);
    cps_outer_3[0] = 2.;   cps_outer_3[1] = 6.; cps_outer_3[2] = 1.;
    cps_outer_3[3] = 2.;   cps_outer_3[4] = -6.; cps_outer_3[5] = 1.;
    std::vector<double> cps_outer_4((2 + 1) * nb_cp_outer);
    cps_outer_4[0] = 2.;   cps_outer_4[1] = -6.; cps_outer_4[2] = 1.;
    cps_outer_4[3] = -2.;   cps_outer_4[4] = -6; cps_outer_4[5] = 1.;

    d->nurbs_curve_outer_1->create(nb_cp_outer, order_outer, knots_outer_1.data(), cps_outer_1.data());
    d->nurbs_curve_outer_2->create(nb_cp_outer, order_outer, knots_outer_2.data(), cps_outer_2.data());
    d->nurbs_curve_outer_3->create(nb_cp_outer, order_outer, knots_outer_3.data(), cps_outer_3.data());
    d->nurbs_curve_outer_4->create(nb_cp_outer, order_outer, knots_outer_4.data(), cps_outer_4.data());

    d->topo_trim_outer_1 = new dtkTopoTrim();
    d->topo_trim_outer_2 = new dtkTopoTrim();
    d->topo_trim_outer_3 = new dtkTopoTrim();
    d->topo_trim_outer_4 = new dtkTopoTrim();

    d->trim_outer_1 = new dtkTrim(*d->nurbs_surface, *d->nurbs_curve_outer_1, d->topo_trim_outer_1);
    d->trim_outer_2 = new dtkTrim(*d->nurbs_surface, *d->nurbs_curve_outer_2, d->topo_trim_outer_2);
    d->trim_outer_3 = new dtkTrim(*d->nurbs_surface, *d->nurbs_curve_outer_3, d->topo_trim_outer_3);

    d->trim_loop_data_outer = new dtkTrimLoopDataCgal();
    d->trim_loop_outer = new dtkTrimLoop(dynamic_cast< dtkAbstractTrimLoopData * >(d->trim_loop_data_outer));

    d->trim_loop_outer->trims().push_back(d->trim_outer_1);
    d->trim_loop_outer->trims().push_back(d->trim_outer_2);
    d->trim_loop_outer->trims().push_back(d->trim_outer_3);
    d->trim_loop_outer->trims().push_back(d->trim_outer_4);

    d->trim_loop_outer->setType(dtkContinuousGeometryEnums::TrimLoopType::inner);

    d->nurbs_polyhedral_surface_cgal = new dtkNurbsPolyhedralSurfaceCgal();
}

void dtkNurbsPolyhedralSurfaceCgalTestCase::init(void) {}

void dtkNurbsPolyhedralSurfaceCgalTestCase::testInitialize(void)
{
    double approximation = 0.01;
    d->nurbs_polyhedral_surface_cgal->initialize(d->nurbs_surface, approximation, 1);
}

void dtkNurbsPolyhedralSurfaceCgalTestCase::cleanup(void) {}

void dtkNurbsPolyhedralSurfaceCgalTestCase::cleanupTestCase(void)
{
    delete d->nurbs_curve_inner_1;
    delete d->nurbs_curve_inner_2;
    delete d->nurbs_curve_inner_3;
    delete d->trim_inner_1;
    delete d->trim_inner_2;
    delete d->trim_inner_3;
    delete d->topo_trim_inner_1;
    delete d->topo_trim_inner_2;
    delete d->topo_trim_inner_3;

    delete d->nurbs_curve_outer_1;
    delete d->nurbs_curve_outer_2;
    delete d->nurbs_curve_outer_3;
    delete d->nurbs_curve_outer_4;
    delete d->trim_outer_1;
    delete d->trim_outer_2;
    delete d->trim_outer_3;
    delete d->trim_outer_4;
    delete d->topo_trim_outer_1;
    delete d->topo_trim_outer_2;
    delete d->topo_trim_outer_3;
    delete d->topo_trim_outer_4;

    delete d->nurbs_surface;
    delete d->nurbs_polyhedral_surface_cgal;
}

DTKTEST_MAIN_NOGUI(dtkNurbsPolyhedralSurfaceCgalTest, dtkNurbsPolyhedralSurfaceCgalTestCase)

//
// dtkNurbsPolyhedralSurfaceCgalTest.cpp ends here
