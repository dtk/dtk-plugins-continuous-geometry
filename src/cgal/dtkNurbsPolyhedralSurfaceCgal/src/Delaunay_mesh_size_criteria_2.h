/*
 * Delaunay_mesh_size_criteria_2.h
 *
 *  Created on: Jul 27, 2020
 *      Author: xxiao
 */

#ifndef SRC_CGAL_DTKNURBSPOLYHEDRALSURFACECGAL_SRC_DELAUNAY_MESH_SIZE_CRITERIA_2_H_
#define SRC_CGAL_DTKNURBSPOLYHEDRALSURFACECGAL_SRC_DELAUNAY_MESH_SIZE_CRITERIA_2_H_

#include <CGAL/license/Mesh_2.h>

#include <CGAL/disable_warnings.h>

#include <CGAL/Mesh_2/Face_badness.h>
#include <CGAL/Delaunay_mesh_criteria_2.h>
#include <utility>
#include <ostream>

namespace PLUGIN {

template <class CDT, class NURBS>
class Delaunay_mesh_size_criteria_2 :
    public virtual CGAL::Delaunay_mesh_criteria_2<CDT>
{
protected:
  typedef typename CDT::Geom_traits Geom_traits;
  double approxbound;
  NURBS* nurbs_surface;

public:
  typedef CGAL::Delaunay_mesh_criteria_2<CDT> Base;

  Delaunay_mesh_size_criteria_2(const double aspect_bound = 0.125,
                                const double approx_bound = 0.,
                                NURBS* nurbs = nullptr,
                                const Geom_traits& traits = Geom_traits())
    : Base(aspect_bound, traits), approxbound(approx_bound), nurbs_surface(nurbs) {}

  inline
  double approx_bound() const { return approxbound; }

  inline
  void set_approx_bound(const double ab) { approxbound = ab; }

  struct Quality : public std::pair<double, double>
  {
    typedef std::pair<double, double> Base;

    Quality() : Base() {};
    Quality(double _sine, double _distance) : Base(_sine, _distance) {}

    const double& sine() const {return first;}
    const double& distance() const {return second;}

    bool operator<(const Quality& q) const
    {
      if (distance() > 1) {
        if (q.distance() > 1) {
          return (distance() > q.distance());
        }
        else {
          return true; // *this is big but not q
        }
      }
      else {
        if (q.distance() > 1) {
          return false; // q is big but not *this
        }
        else {
          return (sine() < q.sine());
        }
      }
    }

    std::ostream& operator<<(std::ostream& out) const
    {
      return out << "(distance=" << distance() << ", sine=" << sine() << ")";
    }
  };

  class Is_bad : public Base::Is_bad
  {
  protected:
    const double squared_distance_bound;
    NURBS* nurbs_surface;
  public:
    typedef typename Base::Is_bad::Point_2 Point_2;

    Is_bad(const double aspect_bound,
           const double distance_bound,
           NURBS* nurbs,
           const Geom_traits& traits)
      : Base::Is_bad(aspect_bound, traits),
        squared_distance_bound(distance_bound*distance_bound),
        nurbs_surface(nurbs){}

    CGAL::Mesh_2::Face_badness operator()(const Quality q) const
    {
      if (q.distance() > 1) return CGAL::Mesh_2::IMPERATIVELY_BAD;
      if (q.sine() < this->B) return CGAL::Mesh_2::BAD;
      else return CGAL::Mesh_2::NOT_BAD;
    }

    CGAL::Mesh_2::Face_badness operator()(const typename CDT::Face_handle& fh,
                                    Quality& q) const
    {
      typedef typename CDT::Geom_traits Geom_traits;
      //typedef typename Geom_traits::Compute_area_2 Compute_area_2;
      typedef typename Geom_traits::Compute_squared_distance_2 Compute_squared_distance_2;

      Geom_traits traits;

      Compute_squared_distance_2 squared_distance = traits.compute_squared_distance_2_object();

      const Point_2& pa = fh->vertex(0)->point();
      const Point_2& pb = fh->vertex(1)->point();
      const Point_2& pc = fh->vertex(2)->point();

      double a = CGAL::to_double(squared_distance(pb, pc));
      double b = CGAL::to_double(squared_distance(pc, pa));
      double c = CGAL::to_double(squared_distance(pa, pb));

      double max_sq_length;
      double second_max_sq_length;

      if (a < b) {
        if (b < c) {
          max_sq_length = c;
          second_max_sq_length = b;
        }
        else {
          max_sq_length = c;
          second_max_sq_length = (a < c ? c : a);
        }
      }
      else {
        if (a < c) {
          max_sq_length = c;
          second_max_sq_length = a;
        }
        else {
          max_sq_length = a;
          second_max_sq_length = (b < c ? c : b);
        }
      }

      // compute the distance between the triangle and the surface
      double coorda[3], coordb[3], coordc[3];
      nurbs_surface->evaluatePoint(pa[0], pa[1], coorda);
      nurbs_surface->evaluatePoint(pb[0], pb[1], coordb);
      nurbs_surface->evaluatePoint(pc[0], pc[1], coordc);

      const Point_2& p_centroid = CGAL::centroid(pa, pb, pc);
      double coord_centroid[3];
      nurbs_surface->evaluatePoint(p_centroid[0], p_centroid[1], coord_centroid);

      using K = typename Point_2::R;
      using Point_3 = typename K::Point_3;

      const Point_3 pa_3(coorda[0], coorda[1], coorda[2]);
      const Point_3 pb_3(coordb[0], coordb[1], coordb[2]);
      const Point_3 pc_3(coordc[0], coordc[1], coordc[2]);
      const Point_3 p_3d_centroid = CGAL::centroid(pa_3, pb_3, pc_3);

      const double sq_dist = squared_distance(Point_3(coord_centroid[0],
                                                      coord_centroid[1],
                                                      coord_centroid[2]),
                                              p_3d_centroid);
      // std::cerr << "triangle: " << std::endl;
      // std::cerr << pa_3[0] << " " << pa_3[1] << " " << pa_3[2] << std::endl;
      // std::cerr << pb_3[0] << " " << pb_3[1] << " " << pb_3[2] << std::endl;
      // std::cerr << pc_3[0] << " " << pc_3[1] << " " << pc_3[2] << std::endl;

      // std::cerr << "centroid: " << std::endl;
      // std::cerr << coord_centroid[0] << " " << coord_centroid[1] << " " << coord_centroid[2] << std::endl;

      // std::cerr << "distance: " << std::sqrt(sq_dist) << std::endl;
      // std::cerr << std::endl;

      q.second = 0.;
      if (squared_distance_bound != 0.) {
        //q.second = max_sq_length/squared_distance_bound;
        q.second = sq_dist/squared_distance_bound;
        if (q.distance() > 1.) {
          q.first = 1.;
          // std::cerr << "bad because of sq distance ratio " << q.distance() << '\n';
          return CGAL::Mesh_2::IMPERATIVELY_BAD;
        }
      }
      return CGAL::Mesh_2::NOT_BAD;
/*
      Compute_area_2 area_2 = traits.compute_area_2_object();

      double area = 2.*CGAL::to_double(area_2(pa, pb, pc));

      q.first = (area*area)/(max_sq_length*second_max_sq_length);

      if (q.sine() < this->B) {
        // std::cerr << "bad because of bad shape " << q.sine() << '\n';
        return CGAL::Mesh_2::BAD;
      }
      else return CGAL::Mesh_2::NOT_BAD;
      */
    }

  };

  Is_bad is_bad_object() const {
    return Is_bad(this->bound(), approx_bound(), nurbs_surface, this->traits);
  }

};

}

#include <CGAL/enable_warnings.h>



#endif /* SRC_CGAL_DTKNURBSPOLYHEDRALSURFACECGAL_SRC_DELAUNAY_MESH_SIZE_CRITERIA_2_H_ */
