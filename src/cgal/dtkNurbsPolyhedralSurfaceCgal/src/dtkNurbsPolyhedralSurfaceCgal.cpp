constexpr double cdt_sq_tolerance = 1e-6; /// @TODO: constant, relative squared
                                          /// tolerance in parameters
                                          /// space for the CDT
// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

//#define DEBUG_ 1
//#define CGAL_DEBUG_TRIM_LOOPS 1
#include "dtkNurbsPolyhedralSurfaceCgal.h"

#include <CGAL/Delaunay_mesher_2.h>
#include <CGAL/IO/write_VTU.h>
//#include <CGAL/Delaunay_mesh_size_criteria_2.h>
#include "Delaunay_mesh_size_criteria_2.h"

//#include <CGAL/Polygon_2_algorithms.h>
//#include <CGAL/Polygon_2.h>

#include <dtkContinuousGeometryUtils>
#include <dtkNurbsSurface>
#include <dtkClippedNurbsSurface>
#include <dtkNurbsCurve2D>
#include <dtkTrim>
#include <dtkTopoTrim>
#include <dtkTrimLoop>
#include <dtkClippedTrimLoop>
#include <dtkMesh>
#include <dtkMeshDataDefault>

#include <dtkDiscreteGeometryCore>

#include <dtkClippedNurbsSurface>

/*!
  \class dtkNurbsPolyhedralSurfaceCgal
  \inmodule dtkPluginsContinuousGeometry
  \brief dtkNurbsPolyhedralSurfaceCgal is a CGAL implementation of the concept dtkNurbsPolyhedarlSurface.

  The following line of code shows how to instanciate the class in pratice :
  \code
  dtkNurbsPolyhedralSurface *polyhedral_surface = dtkContinuousGeometry::nurbsPolyhedralSurface::pluginFactory().create("dtkNurbsPolyhedralSurfaceCgal");
  \endcode
*/

/*! \fn dtkNurbsPolyhedralSurfaceCgal::dtkNurbsPolyhedralSurfaceCgal(void)
  Instanciates.

  See \l initialize(dtkNurbsSurface *nurbs_surface) for the initialization.
*/
dtkNurbsPolyhedralSurfaceCgal::dtkNurbsPolyhedralSurfaceCgal() {}

/*! \fn dtkNurbsPolyhedralSurfaceCgal::~dtkNurbsPolyhedralSurfaceCgal(void)
  Destructor.
*/
dtkNurbsPolyhedralSurfaceCgal::~dtkNurbsPolyhedralSurfaceCgal(void) {
  delete m_polyhedral_surface;
}

/*! \fn  dtkNurbsPolyhedralSurfaceCgal::initialize(dtkNurbsSurface *p_nurbs_surface, double approximation)
  Initializes the dtkNurbsPolyhedralSurfaceCgal with a NURBS surface (\a p_nurbs_surface).

  \a nurbs_surface : the NURBS surface to polyhedralize

  \a approximation : the tolerated approximation from the NURBS surface to its polyhedralization
*/
void dtkNurbsPolyhedralSurfaceCgal::initialize(dtkNurbsSurface *nurbs_surface, double approximation, int /*surface_index*/)
{
    const int surface_index = nurbs_surface->surfaceIndex();
    std::cout << "surface index: " << surface_index << std::endl;

    dtkDebug() << "Initialization...";
    dtkNurbsPolyhedralSurface::initialize(nurbs_surface, approximation);
    dtkDebug() << "Initialized";

    // ///////////////////////////////////////////////////////////////////
    // Sample the trim loops and compute seeds for areas to be meshed (i.e. points inside the polygons to be meshed)
    // ///////////////////////////////////////////////////////////////////
    m_cgal_trim_polygons.reserve(m_clipped_surface->m_clipped_trim_loops.size());
    double sq_max_length = 0.;
    for(auto clip_trim_loop : m_clipped_surface->m_clipped_trim_loops) {
        std::cout << "  trim loop index: " << clip_trim_loop->trimLoopIndex() << std::endl;

        std::vector< Point > cgal_trim_polygon;
        std::list< dtkContinuousGeometryPrimitives::Point_2 > polyline;
        bool is_polyline_ok = clip_trim_loop->toPolyline3DApprox(m_clipped_surface->m_rational_bezier_surfaces, polyline, approximation);
        if(!is_polyline_ok) { dtkWarn() << "The polyline is not nice."; }
        cgal_trim_polygon.reserve(polyline.size());
        const CGAL::Bbox_2 bbox = [&]() {
            CGAL::Bbox_2 bbox;
            for (auto& point : polyline) {
                typename K::Point_2 p2d{point[0], point[1]};
                bbox += p2d.bbox();
            }
            return bbox;
        }();
        const double sq_local_max_length = CGAL::square(
            (std::max)(bbox.xmax() - bbox.xmin(), bbox.ymax() - bbox.ymin()));
        sq_max_length = (std::max)(sq_max_length, sq_local_max_length);
        for (auto &point : polyline) {
            typename K::Point_2 p2d{point[0], point[1]};
            cgal_trim_polygon.emplace_back(p2d);
        }
#if CGAL_DEBUG_TRIM_LOOPS
        {
          std::ofstream dump_trim_loop("dump_trim_loop.polylines.txt");
          dump_trim_loop.precision(17);
          dump_trim_loop << cgal_trim_polygon.size() << '\n';
          for (const auto &point : cgal_trim_polygon) {
            dump_trim_loop << point << " 0\n";
          }
        }
#endif // CGAL_DEBUG_TRIM_LOOPS
        CGAL_warning_msg(squared_distance(cgal_trim_polygon.front(),
                                          cgal_trim_polygon.back()) <
                         cdt_sq_tolerance * sq_local_max_length,
                         "Warning: The 2D trimming curve polygon is not well sampled. This may come from dtkClippedTrim or nurbs model.\n It may cause problem in 2D Delaunay meshing and ball generation.");
        cgal_trim_polygon.back() = cgal_trim_polygon.front();

#if CGAL_DEBUG_TRIM_POLYGON
        std::cout << "trim loop polygon: " << std::endl;
        for (auto point : cgal_trim_polygon) {
            std::cout << point[0] << "," << point[1] << "," << 0 << std::endl;
            //double p[3];
            //nurbs_surface->evaluatePoint(point[0], point[1], p);
            //std::cout << p[0] << "," << p[1] << "," << p[2] << std::endl;
        }
        std::cout << std::endl;
#endif
        m_cgal_trim_polygons.emplace_back(clip_trim_loop->m_type, std::move(cgal_trim_polygon));
    }

    std::size_t num_knots_u = nurbs_surface->uNbKnots();
    std::size_t num_knots_v = nurbs_surface->vNbKnots();
    std::vector<double> knots_u(num_knots_u), knots_v(num_knots_v);
    nurbs_surface->uKnots(knots_u.data());
    nurbs_surface->vKnots(knots_v.data());

    // fixed edge triangulation
    CDT1 triangulation;

    double u_min = std::numeric_limits<double>::max(), u_max = -std::numeric_limits<double>::max();
    double v_min = std::numeric_limits<double>::max(), v_max = -std::numeric_limits<double>::max();
    for (auto cgal_trim_polygon: m_cgal_trim_polygons) {
      for (std::size_t i = 0; i < cgal_trim_polygon.second.size() - 1; ++i) {
        K::Point_2 p = cgal_trim_polygon.second[i];
        if (p[0] < u_min) u_min = p[0];
        if (p[0] > u_max) u_max = p[0];
        if (p[1] < v_min) v_min = p[1];
        if (p[1] > v_max) v_max = p[1];
      }
    }

    if (nurbs_surface->isUPeriodic()) {
      std::vector<double> knots_u_new = knots_u;
      int iter_min = 0;
      while (knots_u_new[0] > u_min) {
        iter_min += 1;
        std::vector<double> knots_u_tmp = knots_u;
        for (std::size_t i = 0; i < knots_u.size(); ++i) {
          knots_u_tmp[i] -= iter_min*nurbs_surface->uPeriod();
        }
        knots_u_new.insert(knots_u_new.begin(), knots_u_tmp.begin(), std::prev(knots_u_tmp.end()));
      }
      int iter_max = 0;
      while (knots_u_new.back() < u_max) {
        iter_max += 1;
        std::vector<double> knots_u_tmp = knots_u;
        for (std::size_t i = 0; i < knots_u.size(); ++i) {
          knots_u_tmp[i] += iter_max*nurbs_surface->uPeriod();
        }
        knots_u_new.insert(knots_u_new.end(), std::next(knots_u_tmp.begin()), knots_u_tmp.end());
      }

      knots_u = knots_u_new;
    }

    // consider an inner boundary w.r.t. a ratio of p-curve loop
    // if the complete parametric domain is too large compared with p-curve loop
    // for example 3dm models converted from Rhino...
    double knot_u_min = knots_u[0], knot_u_max = knots_u.back();
    double knot_v_min = knots_v[0], knot_v_max = knots_v.back();

    bool is_knot_u_large = false, is_knot_v_large = false;
    if (knot_u_max - knot_u_min > 5.*(u_max - u_min)) {
      knot_u_min = u_min - 0.1*(u_max - u_min);
      knot_u_max = u_max + 0.1*(u_max - u_min);
      is_knot_u_large = true;
    }
    if (knot_v_max - knot_v_min > 5.*(v_max - v_min)) {
      knot_v_min = v_min - 0.1*(v_max - v_min);
      knot_v_max = v_max + 0.1*(v_max - v_min);
      is_knot_v_large = true;
    }

    double edge_length_target = 250.*approximation; // approximation = 1.e-4*bbox_diagonal

    {
      // get knots without repetition
      std::vector<double> knots_u_tmp, knots_v_tmp;
      for (std::size_t i = 0; i < knots_u.size(); ++i) {
        if ((i == 0) || (i > 0 && knots_u[i] > knots_u[i - 1])) {
          knots_u_tmp.push_back(knots_u[i]);
        }
      }

      for (std::size_t i = 0; i < knots_v.size(); ++i) {
        if ((i == 0) || (i > 0 && knots_v[i] > knots_v[i - 1])) {
          knots_v_tmp.push_back(knots_v[i]);
        }
      }

      // make knots uniform
      if (is_knot_u_large) knots_u_tmp[0] = knot_u_min, knots_u_tmp.back() = knot_u_max;
      if (is_knot_v_large) knots_v_tmp[0] = knot_v_min, knots_v_tmp.back() = knot_v_max;

      double delta_u = (knots_u_tmp.back() - knots_u_tmp[0])/(knots_u_tmp.size() - 1);
      double delta_v = (knots_v_tmp.back() - knots_v_tmp[0])/(knots_v_tmp.size() - 1);

      for (std::size_t i = 1; i < knots_u_tmp.size() - 1; ++i) {
        knots_u_tmp[i] = knots_u_tmp[0] + i*delta_u;
      }
      for (std::size_t i = 1; i < knots_v_tmp.size() - 1; ++i) {
        knots_v_tmp[i] = knots_v_tmp[0] + i*delta_v;
      }

      // extend the knot domain a bit
      knots_u_tmp[0] -= 0.01*(knots_u_tmp.back() - knots_u_tmp[0]);
      knots_u_tmp.back() += 0.01*(knots_u_tmp.back() - knots_u_tmp[0]);

      knots_v_tmp[0] -= 0.01*(knots_v_tmp.back() - knots_v_tmp[0]);
      knots_v_tmp.back() += 0.01*(knots_v_tmp.back() - knots_v_tmp[0]);

      // insert more points based on metric tensor
      for (std::size_t i = 0; i < knots_u_tmp.size() - 1; ++i) {
        double u0 = knots_u_tmp[i], u1 = knots_u_tmp[i + 1];
        for (std::size_t j = 0; j < knots_v_tmp.size() - 1; ++j) {
          // compute metric tensor of four vertices of each knot patch
          //\todo Note: the knot patch can be degenerated (need to examine degeneracy cases)
          double v0 = knots_v_tmp[j], v1 = knots_v_tmp[j + 1];
          std::vector<K::Point_2> vertices;
          // corner vertices
          vertices.push_back(K::Point_2(u0, v0));
          vertices.push_back(K::Point_2(u1, v0));
          vertices.push_back(K::Point_2(u1, v1));
          vertices.push_back(K::Point_2(u0, v1));
          // edge vertices
          vertices.push_back(K::Point_2((u0 + u1)/2., v0));
          vertices.push_back(K::Point_2(u1, (v0 + v1)/2.));
          vertices.push_back(K::Point_2((u0 + u1)/2., v1));
          vertices.push_back(K::Point_2(u0, (v0 + v1)/2.));
          // face vertex
          vertices.push_back(K::Point_2((u0 + u1)/2., (v0 + v1)/2.));

          std::vector<double> metric_u(9), metric_v(9);
          for (std::size_t ii = 0; ii < vertices.size(); ++ii) {
            double vtx_u = vertices[ii][0], vtx_v = vertices[ii][1];
            if (vtx_u < knots_u[0]) vtx_u = knots_u[0];
            else if (vtx_u > knots_u.back()) vtx_u = knots_u.back();

            if (vtx_v < knots_v[0]) vtx_v = knots_v[0];
            else if (vtx_v > knots_v.back()) vtx_v = knots_v.back();
            
            std::vector<double> coord(3), deriv_u(3), deriv_v(3);
            nurbs_surface->evaluate1stDer(vtx_u, vtx_v, coord.data(), deriv_u.data(), deriv_v.data());

            // metric tensor
            double j11 = deriv_u[0]*deriv_u[0] + deriv_u[1]*deriv_u[1] + deriv_u[2]*deriv_u[2];
            double j22 = deriv_v[0]*deriv_v[0] + deriv_v[1]*deriv_v[1] + deriv_v[2]*deriv_v[2];

            metric_u[ii] = std::sqrt(j11);
            metric_v[ii] = std::sqrt(j22);
          } // iterate over regions of a sknot patch

          // average metrics at nine vertices
          double metric1 = 0., metric2 = 0.;
          for (int ii = 0; ii < 4; ++ii) {
            metric1 += metric_u[ii]/16.;
            metric2 += metric_v[ii]/16.;
          }
          for (int ii = 4; ii < 8; ++ii) {
            metric1 += metric_u[ii]/8.;
            metric2 += metric_v[ii]/8.;
          }
          metric1 += metric_u.back()/4.;
          metric2 += metric_v.back()/4.;

          // add more points in each knot patch
          const double delta_u = edge_length_target/metric1;
          const double delta_v = edge_length_target/metric2;

          const int nu = std::ceil((u1 - u0)/delta_u);
          const int nv = std::ceil((v1 - v0)/delta_v);

          for (int iu = 0; iu < nu + 1; ++iu) {
            double u = u0 + iu*(u1 - u0)/nu;
            if (iu == nu) u = u1;
            for (int iv = 0; iv < nv + 1; ++iv) {
              double v = v0 + iv*(v1 - v0)/nv;
              if (iv == nv) v = v1;
              if (iu > 0 && iu < nu && iv > 0 && iv < nv) triangulation.insert(K::Point_2(u, v));
            }
          }

          for (int iu = 0; iu < nu + 1; iu += nu) {
            double u = u0 + iu*(u1 - u0)/nu;
            if (iu == nu) u = u1;
            std::vector<K::Point_2> points;
            points.reserve(nv + 1);
            for (int iv = 0; iv < nv + 1; ++iv) {
              double v = v0 + iv*(v1 - v0)/nv;
              if (iv == nv) v = v1;
              points.push_back(K::Point_2(u, v));
            }
            for (std::size_t ii = 0; ii < points.size() - 1; ++ii) {
              triangulation.insert_constraint(points[ii], points[ii + 1]);
            }
          }
          for (int iv = 0; iv < nv + 1; iv += nv) {
            double v = v0 + iv*(v1 - v0)/nv;
            if (iv == nv) v = v1;
            std::vector<K::Point_2> points;
            points.reserve(nu + 1);
            for (int iu = 0; iu < nu + 1; ++iu) {
              double u = u0 + iu*(u1 - u0)/nu;
              if (iu == nu) u = u1;
              points.push_back(K::Point_2(u, v));
            }
            for (std::size_t ii = 0; ii < points.size() - 1; ++ii) {
              triangulation.insert_constraint(points[ii], points[ii + 1]);
            }
          }

        }
      }
    }

    // refine triangles considering surface approximation
    bool do_surface_approximation = true;
    std::cout << "start surface approximation" << std::endl;
    while (do_surface_approximation) {
      do_surface_approximation = false;
      std::set<K::Point_2> vertices_refine;
      for (auto fh : triangulation.finite_face_handles()) {
        auto p1 = fh->vertex(0)->point();
        auto p2 = fh->vertex(1)->point();
        auto p3 = fh->vertex(2)->point();

        auto pc = CGAL::centroid(p1, p2, p3);

        bool is_inside = true;

        for (auto cgal_trim_polygon : m_cgal_trim_polygons) {
          CGAL::Bounded_side bounded_side = CGAL::bounded_side_2(cgal_trim_polygon.second.begin(), cgal_trim_polygon.second.end(),
                                                                 pc, K());
          if (bounded_side == CGAL::ON_BOUNDED_SIDE || bounded_side == CGAL::ON_BOUNDARY) {
            if (cgal_trim_polygon.first == dtkContinuousGeometryEnums::TrimLoopType::outer) {
              is_inside = false;
              break;
            }
          }
          else {
            if (cgal_trim_polygon.first == dtkContinuousGeometryEnums::TrimLoopType::inner) {
              is_inside = false;
              break;
            }
          }
        }

        if (is_inside) { // only refine triangles within p-curve loops; otherwise the refinemet somehow may be infinite in some cases
          // points in 3d
          std::array<double, 3> coord1, coord2, coord3;
          nurbs_surface->evaluatePoint(p1[0], p1[1], coord1.data());
          nurbs_surface->evaluatePoint(p2[0], p2[1], coord2.data());
          nurbs_surface->evaluatePoint(p3[0], p3[1], coord3.data());

          std::array<double, 3> coord;
          nurbs_surface->evaluatePoint(pc[0], pc[1], coord.data());

          K::Point_3 coord1_cgal(coord1[0], coord1[1], coord1[2]);
          K::Point_3 coord2_cgal(coord2[0], coord2[1], coord2[2]);
          K::Point_3 coord3_cgal(coord3[0], coord3[1], coord3[2]);
          K::Point_3 coord_cgal(coord[0], coord[1], coord[2]);

          if (CGAL::collinear(coord1_cgal, coord2_cgal, coord3_cgal) == false) {
            K::Triangle_3 tr(coord1_cgal, coord2_cgal, coord3_cgal);
            K::Plane_3 plane = tr.supporting_plane();
            K::Point_3 proj = plane.projection(coord_cgal);
            double sq_dist = CGAL::squared_distance(coord_cgal, proj);

            if (sq_dist > 100. * approximation * approximation) { // i.e. dist > 1.e-3*bbox_diagonal
              // compute the longest edge
              int id1 = 0, id2 = 1;
              double length1 = CGAL::squared_distance(coord1_cgal, coord2_cgal);
              double length_max = length1;

              double length2 = CGAL::squared_distance(coord2_cgal, coord3_cgal);
              if (length2 > length_max)
                length_max = length2, id1 = 1, id2 = 2;

              double length3 = CGAL::squared_distance(coord1_cgal, coord3_cgal);
              if (length3 > length_max)
                length_max = length3, id1 = 2, id2 = 0;

              // insert point in the middle of the longest edge
              K::Point_2 point1 = fh->vertex(id1)->point();
              K::Point_2 point2 = fh->vertex(id2)->point();
              double u_mid = (point1[0] + point2[0]) * 0.5;
              double v_mid = (point1[1] + point2[1]) * 0.5;
              vertices_refine.insert(K::Point_2(u_mid, v_mid));

              do_surface_approximation = true;
            }
          }
        }
      }
      for (auto v : vertices_refine) triangulation.insert(v);
    }
    std::cout << "finish surface approximation" << std::endl;

    m_ct = std::move(triangulation);

    // add boundary constraints considering metric tensors
    for (auto cgal_trim_polygon : m_cgal_trim_polygons) {
      for (std::size_t i = 0; i < cgal_trim_polygon.second.size() - 1; ++i) {
        double metric = std::numeric_limits<double>::max();
        K::Point_2 v1 = cgal_trim_polygon.second[i];
        K::Point_2 v2 = cgal_trim_polygon.second[i + 1];
        double deriv_1 = v2[0] - v1[0], deriv_2 = v2[1] - v1[1];
        for (std::size_t j = 0; j < 2; ++j) {
          K::Point_2 vtx = cgal_trim_polygon.second[i + j];
          std::vector<double> coord(3), deriv_u(3), deriv_v(3);
          nurbs_surface->evaluate1stDer(vtx[0], vtx[1], coord.data(), deriv_u.data(), deriv_v.data());
          // metric tensor
          double j11 = deriv_u[0]*deriv_u[0] + deriv_u[1]*deriv_u[1] + deriv_u[2]*deriv_u[2];
          double j12 = deriv_u[0]*deriv_v[0] + deriv_u[1]*deriv_v[1] + deriv_u[2]*deriv_v[2];
          double j22 = deriv_v[0]*deriv_v[0] + deriv_v[1]*deriv_v[1] + deriv_v[2]*deriv_v[2];
          double m = std::sqrt(j11*deriv_1*deriv_1 + 2.*j12*deriv_1*deriv_2 + j22*deriv_2*deriv_2);
          if (m < metric) metric = m;
        }

        const double dt = edge_length_target/metric;
        K::Point_2 vt(v1[0] + dt*(v2[0] - v1[0]), v1[1] + dt*(v2[1] - v1[1]));
        const double delta2 = CGAL::squared_distance(v1, vt);

        std::vector<K::Point_2> vertices;
        if (CGAL::squared_distance(v1, v2) > delta2) {
          // split the segment v1-v2
          int num_segment = std::ceil(std::sqrt(CGAL::squared_distance(v1, v2))/std::sqrt(delta2));
          if (num_segment > 1) {
            for (int ii = 1; ii < num_segment; ++ii) {
              double f = double(ii)/double(num_segment);
              double u = v1[0] + f*(v2[0] - v1[0]);
              double v = v1[1] + f*(v2[1] - v1[1]);
              vertices.push_back(K::Point_2(u, v));
            }
          }
        }

        vertices.insert(vertices.begin(), v1);
        vertices.insert(vertices.end(), v2);

        for (std::size_t j = 0; j < vertices.size() - 1; ++j) {
          m_ct.insert_constraint(vertices[j], vertices[j + 1]);
        }

      }
    }

    for(auto fh : m_ct.finite_face_handles()) {
      // filter triangles inside p-curve polygons
      auto p1 = fh->vertex(0)->point();
      auto p2 = fh->vertex(1)->point();
      auto p3 = fh->vertex(2)->point();

      K::Point_2 pc = CGAL::centroid(p1, p2, p3);

      bool is_inside = true;

      for (auto cgal_trim_polygon : m_cgal_trim_polygons) {
        CGAL::Bounded_side bounded_side = CGAL::bounded_side_2(cgal_trim_polygon.second.begin(), cgal_trim_polygon.second.end(), pc, K());
        if (bounded_side == CGAL::ON_BOUNDED_SIDE || bounded_side == CGAL::ON_BOUNDARY) {
          if (cgal_trim_polygon.first == dtkContinuousGeometryEnums::TrimLoopType::outer) {
            is_inside = false;
            break;
          }
        }
        else {
          if (cgal_trim_polygon.first == dtkContinuousGeometryEnums::TrimLoopType::inner) {
            is_inside = false;
            break;
          }
        }
      }

      fh->set_in_domain(is_inside);
    }
#ifdef DEBUG_
    std::stringstream ss_vtu;
    ss_vtu << "out/triangles_" << surface_index << ".vtu";
    std::ofstream ss_file(ss_vtu.str());
    CGAL::IO::write_VTU(ss_file, m_ct, CGAL::IO::ASCII);

    // output 3D mesh
    std::stringstream ss_obj;
    ss_obj << "out/triangles3d_" << surface_index << ".obj";
    std::ofstream obj_file(ss_obj.str());

    typedef typename CDT1::Vertex_handle Vertex;
    std::map<Vertex, int> map_vertex_to_index;
    int num_vertices = 0;
    for (auto vtx : m_ct.finite_vertex_handles()) {
      map_vertex_to_index[vtx] = num_vertices;
      num_vertices += 1;

      auto p = vtx->point();
      double coord[3];
      nurbs_surface->evaluatePoint(p[0], p[1], coord);

      obj_file << "v " << coord[0] << " " << coord[1] << " " << coord[2] << std::endl;
    }

    for (auto fh : m_ct.finite_face_handles()) {
      if (fh->is_in_domain()) {
        auto v1 = fh->vertex(0);
        auto v2 = fh->vertex(1);
        auto v3 = fh->vertex(2);

        int id1 = map_vertex_to_index[v1];
        int id2 = map_vertex_to_index[v2];
        int id3 = map_vertex_to_index[v3];

        obj_file << "f " << id1 + 1 << " " << id2 + 1 << " " << id3 + 1 << std::endl;
      }
    }
#endif
    // ///////////////////////////////////////////////////////////////////
    // Convert it into a dtkMesh
    // ///////////////////////////////////////////////////////////////////
    dtkDebug() << "Converting to dtkMesh..";
    dtkMeshData *mesh_data = dtkDiscreteGeometryCore::meshData::pluginFactory().create("dtkMeshDataDefault");
    this->m_polyhedral_surface = new dtkMesh(mesh_data);
    dtkDebug() << "Converting completed";
    /// @TODO: that mesh is empty, is not it?
}

/*! \fn dtkNurbsPolyhedralSurfaceCgal::isPointCulled(const dtkContinuousGeometryPrimitives::Point_2& point) const
  Returns true if \a point is culled by the trim loops of the surface, else returns false.
*/
bool dtkNurbsPolyhedralSurfaceCgal::isPointCulled(const dtkContinuousGeometryPrimitives::Point_2&) const
{
    return true;
}

bool dtkNurbsPolyhedralSurfaceCgal::isPointCulledTrimLoopInternal(const dtkContinuousGeometryPrimitives::Point_2& p_point) {
    //This works because the outter trim loop is necessarily of type inner (CCW)
    if (m_cgal_trim_polygons.size() == 0) {
        dtkFatal() << "Trying to test if a point is culled when no trim loops are recorded.";
    } else if (m_cgal_trim_polygons.size() == 1) {
        if(isPointCulledTrimInternal(p_point, m_cgal_trim_polygons.front())) {
            return true;
        } else {
            return false;
        }
    } else {
        std::size_t culled = 0;
        std::size_t not_culled = 0;
        for (auto& cgal_trim_polygon : m_cgal_trim_polygons) {
            if(isPointCulledTrimInternal(p_point, cgal_trim_polygon) && cgal_trim_polygon.first == dtkContinuousGeometryEnums::outer) {
                ++culled;
            } else if (!isPointCulledTrimInternal(p_point, cgal_trim_polygon) && cgal_trim_polygon.first == dtkContinuousGeometryEnums::inner) {
                ++not_culled;
            }
        }
        if (culled == 0 && not_culled == 0) {
            return true;
        }
        if (culled == not_culled) {
            return true;
        } else {
            return false;
        }
    }
    return false; //Should never happen
}

bool dtkNurbsPolyhedralSurfaceCgal::isPointCulledTrimInternal(const dtkContinuousGeometryPrimitives::Point_2& p_point, std::pair< dtkContinuousGeometryEnums::TrimLoopType, std::vector< Point > >& cgal_trim_polygon) {
    Point p(p_point[0], p_point[1]);
    CGAL::Bounded_side bounded_side = CGAL::bounded_side_2(cgal_trim_polygon.second.begin(), cgal_trim_polygon.second.end(), p, K());
    if (bounded_side == CGAL::ON_BOUNDED_SIDE || bounded_side == CGAL::ON_BOUNDARY) {
        // ///////////////////////////////////////////////////////////////////
        // If the pre image is in the loop and supposed to be inside : not culled, else culled
        // ///////////////////////////////////////////////////////////////////
        if(cgal_trim_polygon.first == dtkContinuousGeometryEnums::TrimLoopType::inner) {
            return false; // not to be culled
        } else {
            return true;
        }
    } else {
        // ///////////////////////////////////////////////////////////////////
        // If the pre image not is in the loop and supposed to be outside : not culled, else culled
        // ///////////////////////////////////////////////////////////////////
        if(cgal_trim_polygon.first == dtkContinuousGeometryEnums::TrimLoopType::outer) {
            return false;
        } else {
            return true;
        }
    }
}


/*! \fn dtkNurbsPolyhedralSurfaceCgal::evaluatePoint(double p_u, double p_v, double *r_point) const
  Writes in \a r_point the evaluation at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_point : array of size 3 to store the result of the evaluation
*/
void dtkNurbsPolyhedralSurfaceCgal::evaluatePoint(double p_u, double p_v, double* r_point) const
{
    return m_nurbs_surface->evaluatePoint(p_u, p_v, r_point);
}

/*! \fn dtkNurbsPolyhedralSurfaceCgal::evaluateNormal(double p_u, double p_v, double *r_normal) const
  Writes in \a r_normal the normal evaluation at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_normal : array of size 3 to store the result of the normal evaluation
*/
void dtkNurbsPolyhedralSurfaceCgal::evaluateNormal(double p_u, double p_v, double* r_normal) const
{
    return m_nurbs_surface->evaluateNormal(p_u, p_v, r_normal);
}

/*! \fn dtkNurbsPolyhedralSurfaceCgal::evaluate1stDer(double p_u, double p_v, double *r_point, double *r_u_deriv, double *r_v_deriv) const
  Writes in \a r_point the evaluation, in \a r_u_deriv the first derivative in "u" direction, in \a r_v_deriv the first derivative in the "v" direction at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_point : array of size 3 to store the result of the evaluation

  \a r_u_deriv : array of size 3 to store the result of the first derivative evaluation in the "u" direction

  \a r_v_deriv : array of size 3 to store the result of the first derivative evaluation in the "v" direction
*/
void dtkNurbsPolyhedralSurfaceCgal::evaluate1stDer(double p_u, double p_v, double* r_point, double* r_u_deriv, double* r_v_deriv) const
{
    return m_nurbs_surface->evaluate1stDer(p_u, p_v, r_point, r_u_deriv, r_v_deriv);
}

/*! \fn dtkNurbsPolyhedralSurfaceCgal::evaluate1stDer(double p_u, double p_v, double *r_u_deriv, double *r_v_deriv) const
  Writes in \a r_u_deriv the first derivative in "u" direction and in \a r_v_deriv the first derivative in the "v" direction at parameters (\a p_u, \a p_v).

  \a p_u : parameter in the "u" direction

  \a p_v : parameter in the "v" direction

  \a r_u_deriv : array of size 3 to store the result of the first derivative evaluation in the "u" direction

  \a r_v_deriv : array of size 3 to store the result of the first derivative evaluation in the "v" direction
*/
void dtkNurbsPolyhedralSurfaceCgal::evaluate1stDer(double p_u, double p_v, double* r_u_deriv, double* r_v_deriv) const
{
    return m_nurbs_surface->evaluate1stDer(p_u, p_v, r_u_deriv, r_v_deriv);
}

/*! \fn dtkNurbsPolyhedralSurfaceCgal::pointsAndTriangles(std::vector< dtkContinuousGeometryPrimitives::Point_3 >& r_points, std::vector< std::size_t >& r_triangles) const
  Returns a triangulation approximating the NURBS surface as points and triangles.

  \a r_points : the geometrical points of the triangulation

  \a r_triangles : the triangles of the triangulation
*/
void dtkNurbsPolyhedralSurfaceCgal::pointsAndTriangles(std::vector< dtkContinuousGeometryPrimitives::Point_3 >& r_points, std::vector< std::size_t >& r_triangles) const
{
    r_points.clear();
    r_points.reserve(m_ct.number_of_vertices());
    r_triangles.clear();
    r_triangles.reserve(m_ct.number_of_faces());
    std::map < Point, std::size_t > map_indices;
    std::size_t index = 0;
    dtkContinuousGeometryPrimitives::Point_3 p(0., 0., 0.);
    for (auto vit = m_ct.finite_vertices_begin(); vit != m_ct.finite_vertices_end(); ++vit){
        m_nurbs_surface->evaluatePoint(vit->point()[0], vit->point()[1], p.data());
        r_points.push_back(p);
        map_indices[vit->point()] = index;
        ++index;
    }
    if(!m_nurbs_surface->hasReversedOrientation()) {
        for (CDT::Finite_faces_iterator fit = m_ct.finite_faces_begin(); fit != m_ct.finite_faces_end(); ++fit) {
            if (!fit->is_in_domain()) continue;
            for (std::size_t i = 0; i < 3; ++i) {
                r_triangles.push_back(map_indices.at(fit->vertex(i)->point()));
            }
        }
    } else {
        for (CDT::Finite_faces_iterator fit = m_ct.finite_faces_begin(); fit != m_ct.finite_faces_end(); ++fit) {
            if (!fit->is_in_domain()) continue;
            r_triangles.push_back(map_indices.at(fit->vertex(0)->point()));
            r_triangles.push_back(map_indices.at(fit->vertex(2)->point()));
            r_triangles.push_back(map_indices.at(fit->vertex(1)->point()));
        }
    }
}

void dtkNurbsPolyhedralSurfaceCgal::pointsTrianglesAndNormals(std::vector< dtkContinuousGeometryPrimitives::Point_3 >& r_points, std::vector< std::size_t >& r_triangles, std::vector< dtkContinuousGeometryPrimitives::Vector_3 >& r_normals) const
{
    r_points.clear();
    r_points.reserve(m_ct.number_of_vertices());
    r_normals.clear();
    r_normals.reserve(m_ct.number_of_vertices());
    r_triangles.clear();
    r_triangles.reserve(m_ct.number_of_faces());
    std::map < Point, std::size_t > map_indices;
    std::size_t index = 0;
    dtkContinuousGeometryPrimitives::Point_3 p(0., 0., 0.);
    dtkContinuousGeometryPrimitives::Vector_3 n(0., 0., 0.);
    for (auto vit = m_ct.finite_vertices_begin(); vit != m_ct.finite_vertices_end(); ++vit){
        m_nurbs_surface->evaluatePoint(vit->point()[0], vit->point()[1], p.data());
        m_nurbs_surface->evaluateNormal(vit->point()[0], vit->point()[1], n.data());
        r_points.push_back(p);
        /*
        if(m_nurbs_surface->hasReversedOrientation()) {
            n[0] = -n[0];
            n[1] = -n[1];
            n[2] = -n[2];
        }
        */
        r_normals.push_back(n);
        map_indices[vit->point()] = index;
        ++index;
    }
    if(!m_nurbs_surface->hasReversedOrientation()) {
        for (CDT::Finite_faces_iterator fit = m_ct.finite_faces_begin(); fit != m_ct.finite_faces_end(); ++fit) {
            if (!fit->is_in_domain()) continue;
            for (std::size_t i = 0; i < 3; ++i) {
                r_triangles.push_back(map_indices.at(fit->vertex(i)->point()));
            }
        }
    } else {
        for (CDT::Finite_faces_iterator fit = m_ct.finite_faces_begin(); fit != m_ct.finite_faces_end(); ++fit) {
            if (!fit->is_in_domain()) continue;
            r_triangles.push_back(map_indices.at(fit->vertex(0)->point()));
            r_triangles.push_back(map_indices.at(fit->vertex(2)->point()));
            r_triangles.push_back(map_indices.at(fit->vertex(1)->point()));
        }
    }
}

/*! \fn dtkNurbsPolyhedralSurfaceCgal::aabb(double* r_aabb) const
  Writes in \a r_aabb the [xmin, ymin, zmin, xmax, ymax, zmax] coordinates.

  \a r_aabb : array of size 6 to store the limits coordinates
*/
void dtkNurbsPolyhedralSurfaceCgal::aabb(double *r_aabb) const
{
    //TODO change for the mesh bounding box
    return m_nurbs_surface->aabb(r_aabb);
}

/*! \fn dtkNurbsPolyhedralSurfaceCgal::extendedAabb(double *r_aabb, double factor) const
  Writes in \a r_aabb the [xmin, ymin, zmin, xmax, ymax, zmax] coordinates of an extened bounding box of factor \a factor. The extension factor extends the diagonal.

  \a r_aabb : array of size 6 to store the extended limits coordinates

  \a factor : factor used to lengthen the diagonal of the axis aligned bounding box
*/
void dtkNurbsPolyhedralSurfaceCgal::extendedAabb(double *r_aabb, double factor) const
{
    return m_nurbs_surface->extendedAabb(r_aabb, factor);
}

/*! \fn dtkNurbsPolyhedralSurfaceCgal::clone(void) const
  Clone
*/
dtkNurbsPolyhedralSurfaceCgal* dtkNurbsPolyhedralSurfaceCgal::clone(void) const
{
    return new dtkNurbsPolyhedralSurfaceCgal(*this);
}

//
// dtkNurbsPolyhedralSurfaceCgal.cpp ends here

// Local Variables:
// c-basic-offset: 4
// End: