// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#include "dtkNurbsPolyhedralSurfaceCgal.h"
#include "dtkNurbsPolyhedralSurfaceCgalPlugin.h"

#include <dtkCore>
#include <dtkContinuousGeometry>

// ///////////////////////////////////////////////////////////////////
// dtkNurbsPolyhedralSurfaceCgalPlugin
// ///////////////////////////////////////////////////////////////////

void dtkNurbsPolyhedralSurfaceCgalPlugin::initialize(void)
{
    dtkContinuousGeometry::nurbsPolyhedralSurface::pluginFactory().record("dtkNurbsPolyhedralSurfaceCgal", dtkNurbsPolyhedralSurfaceCgalCreator);
}

void dtkNurbsPolyhedralSurfaceCgalPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkNurbsPolyhedralSurfaceCgal)

//
// dtkNurbsPolyhedralSurfaceCgalPlugin.cpp ends here
