// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <dtkContinuousGeometry.h>

#include <dtkContinuousGeometryUtils>

#include <dtkNurbsPolyhedralSurfaceCgalExport.h>

#include <dtkNurbsPolyhedralSurface>

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Constrained_Delaunay_triangulation_2.h>
#include <CGAL/Delaunay_mesher_2.h>
#include <CGAL/Delaunay_mesh_vertex_base_2.h>
#include <CGAL/Delaunay_mesh_face_base_2.h>
#include <CGAL/Polygon_2.h>
#include <CGAL/Constrained_triangulation_2.h>
#include <CGAL/Constrained_triangulation_plus_2.h>
#include <CGAL/Polyline_simplification_2/simplify.h>
#include <CGAL/Polyline_simplification_2/Squared_distance_cost.h>

#include <map>

class DTKNURBSPOLYHEDRALSURFACECGAL_EXPORT dtkNurbsPolyhedralSurfaceCgal final : public dtkNurbsPolyhedralSurface
{
 private:
    typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
    typedef CGAL::Polyline_simplification_2::Vertex_base_2<K> Vb1;
    typedef CGAL::Delaunay_mesh_vertex_base_2<K, Vb1> Vb;
    typedef CGAL::Constrained_triangulation_face_base_2<K> Fb1;
    typedef CGAL::Delaunay_mesh_face_base_2<K, Fb1> Fb;
    typedef CGAL::Triangulation_data_structure_2<Vb, Fb> Tds;
    typedef CGAL::Exact_predicates_tag Itag;
    typedef CGAL::Constrained_Delaunay_triangulation_2<K, Tds, Itag> CDT1;
    typedef CGAL::Constrained_triangulation_plus_2<CDT1> CDT;
    typedef CDT::Vertex_handle Vertex_handle;
    typedef CGAL::Constrained_triangulation_2<K, Tds, Itag> CT;

    typedef K::Point_2 Point;

    typedef typename std::list< std::pair< dtkContinuousGeometryEnums::TrimLoopType, std::vector< dtkContinuousGeometryPrimitives::Point_2 > > > Trim_polygon_type;

 public:
    dtkNurbsPolyhedralSurfaceCgal(void);
    virtual ~dtkNurbsPolyhedralSurfaceCgal(void) final;

    void initialize(dtkNurbsSurface *nurbs_surface, double approximation, int surface_mesh) override;

 public:
    bool isPointCulled(const dtkContinuousGeometryPrimitives::Point_2& point) const override;

 public:
    void evaluatePoint(double p_u, double p_v, double* r_point) const override;
    void evaluateNormal(double p_u, double p_v, double* r_normal) const override;
    void evaluate1stDer(double p_u, double p_v, double* r_point, double* r_u_deriv, double* r_v_deriv) const override;
    void evaluate1stDer(double p_u, double p_v, double* r_u_deriv, double* r_v_deriv) const override;

 public:
    void aabb(double *r_aabb) const override;
    void extendedAabb(double *r_aabb, double factor) const override;

 public:
    void pointsAndTriangles(std::vector< dtkContinuousGeometryPrimitives::Point_3 >& r_points, std::vector< std::size_t >& r_triangles) const override;
    void pointsTrianglesAndNormals(std::vector< dtkContinuousGeometryPrimitives::Point_3 >& r_points, std::vector< std::size_t >& r_triangles, std::vector< dtkContinuousGeometryPrimitives::Vector_3 >& r_normals) const override;

 public:
    bool isPointCulledTrimInternal(const dtkContinuousGeometryPrimitives::Point_2& p_point, std::pair< dtkContinuousGeometryEnums::TrimLoopType, std::vector< Point > >& cgal_trim_polygon);
    bool isPointCulledTrimLoopInternal(const dtkContinuousGeometryPrimitives::Point_2& p_point);

 public:
    Trim_polygon_type trim_polygon_2d() const override {
      Trim_polygon_type polygons;
      for (const auto& trim_polygon : m_cgal_trim_polygons) {
        std::vector<Point> points = trim_polygon.second;
        std::vector<dtkContinuousGeometryPrimitives::Point_2> points_2;
        points_2.reserve(points.size());
        for (auto point : points) {
          dtkContinuousGeometryPrimitives::Point_2 p(point[0], point[1]);
          points_2.push_back(p);
        }

        polygons.push_back( std::make_pair(trim_polygon.first, points_2) );

      }
      return polygons;
    }

 public:
    dtkNurbsPolyhedralSurfaceCgal* clone(void) const override;

 private:
    CT m_ct;
    std::vector< std::pair< dtkContinuousGeometryEnums::TrimLoopType, std::vector< Point > > > m_cgal_trim_polygons;
};


// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

inline dtkNurbsPolyhedralSurface *dtkNurbsPolyhedralSurfaceCgalCreator()
{
    return new dtkNurbsPolyhedralSurfaceCgal();
}

//
// dtkNurbsPolyhedralSurfaceCgal.h ends here