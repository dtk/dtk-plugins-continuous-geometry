// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkBRepReader.h>

#include <dtkBRepReaderCgalExport.h>

#include <dtkCore>

class DTKBREPREADERCGAL_EXPORT dtkBRepReaderCgalPlugin : public dtkBRepReaderPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkBRepReaderPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkBRepReaderCgalPlugin" FILE "dtkBRepReaderCgalPlugin.json")

public:
     dtkBRepReaderCgalPlugin(void) {}
    ~dtkBRepReaderCgalPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

//
// dtkBRepReaderCgalPlugin.h ends here
