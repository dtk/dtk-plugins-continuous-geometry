// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkBRepReader>

#include <dtkBRepReaderCgalExport.h>

class dtkBRepReaderCgalPrivate;

class dtkBRep;

class DTKBREPREADERCGAL_EXPORT dtkBRepReaderCgal : public dtkBRepReader
{
public:
     dtkBRepReaderCgal(void);
    ~dtkBRepReaderCgal(void);

public:
    void  setInputBRepFilePath(const QString&) final;
    void  setInputDumpFilePath(const QString&) final;

    void run(void) final;

    dtkBRep* outputDtkBRep(void) const final;

protected:
    dtkBRepReaderCgalPrivate *d;
};

// ///////////////////////////////////////////////////////////////////

inline dtkBRepReader* dtkBRepReaderCgalCreator(void)
{
    return new dtkBRepReaderCgal();
}

//
// dtkBRepReaderCgal.h ends here
