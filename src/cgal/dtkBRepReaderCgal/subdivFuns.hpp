/*
 * subdivFuns.hpp
 *
 *  Created on: 18 Nov 2019
 *      Author: xxiao
 */

#pragma once

#include <vector>
#include <map>

namespace SubdivMesh {

  // declaration of auxiliary functions
  void onering_vertices_around_vertex(std::vector< vertex_descriptor >& onering_vertices_e,
                                      std::vector< vertex_descriptor >& onering_vertices_f,
                                      const vertex_descriptor& vtx,
                                      const PolygonMesh& pmesh);

  void onering_vertices_around_face(std::vector< vertex_descriptor >& onering_vertices,
                                    const face_descriptor& f,
                                    const PolygonMesh& pmesh);

  void collect_subdivision_map(std::map< face_descriptor, std::vector< halfedge_descriptor > >& subdiv_map,
                               const PolygonMesh& pmesh,
                               const PolygonMesh& pmesh_subdiv);

  void index_reorder_map(std::vector< std::pair<int, int> >& indices, const int& id1, const int& id2);

  void vertices_reorder_map(boost::numeric::ublas::bounded_matrix< vertex_descriptor, 5, 5 >& vtx_map,
                            const face_descriptor& f, const PolygonMesh& pmesh1, const PolygonMesh& pmesh2,
                            std::map< face_descriptor, std::vector< halfedge_descriptor > > subdiv_map_1,
                            std::map< face_descriptor, std::vector< halfedge_descriptor > > subdiv_map_2);

  void vertices_limit_map(boost::numeric::ublas::bounded_matrix< Kernel::Point_3, 5, 5 >& vtx_limit_map,
                          boost::numeric::ublas::bounded_matrix< vertex_descriptor, 5, 5 > vtx_map,
                          const PolygonMesh& pmesh2);

  bool check_regular_patch(const face_descriptor& f, const PolygonMesh& pmesh);

  void collect_edge_vertices_LS(std::vector< std::pair<int, int> >& indices, const int& edge_id);

  void edge_least_square_fitting(std::vector< Kernel::Point_3 >& bezier_points, const std::vector< Kernel::Point_3 >& edge_limit_points);

  void collect_face_vertices_LS(std::vector< std::pair<int, int> >& indices);

  void face_least_square_fitting(std::map<int, Kernel::Point_3 >& bezier_points, const std::vector< Kernel::Point_3 >& face_limit_points);

  void bspline_to_bezier(std::map<int, Kernel::Point_3>& bezier_points, const Eigen::MatrixXd& cps_spline);

  void create_bezier_control_points(std::map< std::pair<int, int>, Kernel::Point_3 >& cps_bezier, std::map<int, Kernel::Point_3> bezier_patch);

  Kernel::Point_3 evaluate_bezier_surface(const double& u, const double& v,
                                          std::map< std::pair<int, int>, Kernel::Point_3 > cps_bezier);
}
  // Implementations
  void SubdivMesh::onering_vertices_around_vertex(std::vector< typename boost::graph_traits<PolygonMesh>::vertex_descriptor >& onering_vertices_e,
                                      std::vector< typename boost::graph_traits<PolygonMesh>::vertex_descriptor >& onering_vertices_f,
                                      const typename boost::graph_traits<PolygonMesh>::vertex_descriptor& vtx,
                                      const PolygonMesh& pmesh)
  {
    typedef boost::graph_traits<PolygonMesh>::vertex_descriptor vertex_descriptor;
    typedef boost::graph_traits<PolygonMesh>::halfedge_descriptor halfedge_descriptor;

    halfedge_descriptor he_vtx = pmesh.halfedge(vtx);

    // loop over one facet
    while (true) {
      halfedge_descriptor he_next = CGAL::next(he_vtx, pmesh);
      vertex_descriptor vtx_next = CGAL::target(he_next, pmesh);

      halfedge_descriptor he_next_peep = CGAL::next(he_next, pmesh);
      if ( CGAL::target(he_next_peep, pmesh) == vtx ) {
        onering_vertices_e.push_back(vtx_next);
        he_vtx = he_next_peep;
        break;
      }
      else {
        if ( CGAL::source(he_next, pmesh) == vtx ) onering_vertices_e.push_back(vtx_next);
        else onering_vertices_f.push_back(vtx_next);
        he_vtx = he_next;
      }
    }

    // loop over other incident facets
    bool is_continue = true;
    while (is_continue) {
      halfedge_descriptor he_opposite = CGAL::opposite(he_vtx, pmesh);
      while (true) {
        he_vtx = CGAL::next(he_opposite, pmesh);
        vertex_descriptor vtx_next = CGAL::target(he_vtx, pmesh);

        halfedge_descriptor he_next_peep = CGAL::next(he_vtx, pmesh);
        if ( CGAL::target(he_next_peep, pmesh) == vtx ) {
          std::vector<vertex_descriptor>::iterator it = std::find(onering_vertices_e.begin(), onering_vertices_e.end(), vtx_next);
          if (it != onering_vertices_e.end()) {
            is_continue = false;
            break;
          }
          else {
            onering_vertices_e.push_back(vtx_next);
            he_vtx = he_next_peep;
            break;
          }
        } // termination criteria
        else {
          if ( CGAL::source(he_vtx, pmesh) == vtx ) onering_vertices_e.push_back(vtx_next);
          else onering_vertices_f.push_back(vtx_next);
          he_opposite = he_vtx;
        }
      } // loop within one facet
    } // loop over facets

  }

  void SubdivMesh::collect_subdivision_map(std::map< typename boost::graph_traits<PolygonMesh>::face_descriptor, std::vector< typename boost::graph_traits<PolygonMesh>::halfedge_descriptor > >& subdiv_map,
                               const PolygonMesh& pmesh,
                               const PolygonMesh& pmesh_subdiv)
  {
    typedef typename boost::graph_traits<PolygonMesh>::halfedge_descriptor halfedge_descriptor;
    typedef typename boost::graph_traits<PolygonMesh>::face_descriptor face_descriptor;

    typedef typename boost::graph_traits<PolygonMesh>::face_iterator face_iterator;

    for (face_iterator fitr = pmesh.faces_begin(); fitr != pmesh.faces_end(); ++fitr) {
      face_descriptor f = *fitr;

      typename PolygonMesh::size_type degree_face = pmesh.degree(f);

      halfedge_descriptor he_min = pmesh.halfedge(f);
      halfedge_descriptor he_tmp = pmesh.halfedge(f);
      for (auto i = 0u; i < degree_face; ++i) {
        if (CGAL::target(he_tmp, pmesh) < CGAL::target(he_min, pmesh)) {
          he_min = he_tmp;
        }
        he_tmp = CGAL::next(he_tmp, pmesh);
      }

      halfedge_descriptor he = he_min;

      std::vector<halfedge_descriptor> halfedges_subdiv_face;

      for (auto i = 0u; i < degree_face; ++i) {
        halfedge_descriptor he_subdiv = he;

        // make sure halfedges pointing to targets in original mesh
        if ( CGAL::target(he_subdiv, pmesh_subdiv) != CGAL::target(he, pmesh) ) {
          he_subdiv = CGAL::next(he_subdiv, pmesh_subdiv);
          he_subdiv = CGAL::opposite(he_subdiv, pmesh_subdiv);
          he_subdiv = CGAL::next(he_subdiv, pmesh_subdiv);
        }

        halfedges_subdiv_face.push_back(he_subdiv);

        he = CGAL::next(he, pmesh);

      } // loop within original facet

      subdiv_map[f] = halfedges_subdiv_face;

    } // loop over original facets

  }

  void SubdivMesh::index_reorder_map(std::vector< std::pair<int, int> >& indices, const int& id1, const int& id2)
  {
    std::vector<int> index1, index2;

    if (id1 == 0) {
      index1.push_back(0), index1.push_back(1), index1.push_back(2);
      index2.push_back(0), index2.push_back(1), index2.push_back(2);
    }
    else if (id1 == 1) {
      index1.push_back(0), index1.push_back(1), index1.push_back(2);
      index2.push_back(4), index2.push_back(3), index2.push_back(2);
    }
    else if (id1 == 2) {
      index1.push_back(4), index1.push_back(3), index1.push_back(2);
      index2.push_back(4), index2.push_back(3), index2.push_back(2);
    }
    else if (id1 == 3) {
      index1.push_back(4), index1.push_back(3), index1.push_back(2);
      index2.push_back(0), index2.push_back(1), index2.push_back(2);
    }

    std::vector< std::pair<int, int> > sub_indices;
    if (id2 == 0) {
      sub_indices.push_back( std::make_pair(0, 0) ), sub_indices.push_back( std::make_pair(1, 0) );
      sub_indices.push_back( std::make_pair(1, 1) ), sub_indices.push_back( std::make_pair(0, 1) );
    }
    else if (id2 == 1) {
      sub_indices.push_back( std::make_pair(2, 0) ), sub_indices.push_back( std::make_pair(2, 1) );
      sub_indices.push_back( std::make_pair(1, 1) ), sub_indices.push_back( std::make_pair(1, 0) );
    }
    else if (id2 == 2) {
      sub_indices.push_back( std::make_pair(2, 2) ), sub_indices.push_back( std::make_pair(1, 2) );
      sub_indices.push_back( std::make_pair(1, 1) ), sub_indices.push_back( std::make_pair(2, 1) );
    }
    else if (id2 == 3) {
      sub_indices.push_back( std::make_pair(0, 2) ), sub_indices.push_back( std::make_pair(0, 1) );
      sub_indices.push_back( std::make_pair(1, 1) ), sub_indices.push_back( std::make_pair(1, 2) );
    }

    for (auto i = 0u; i < sub_indices.size(); ++i) {
      std::pair<int, int> sub_index = sub_indices[i];
      int sub_id1 = index1[sub_index.first], sub_id2 = index2[sub_index.second];

      if (id1 == 0 || id1 == 2) indices.push_back( std::make_pair(sub_id1, sub_id2) );
      else if (id1 == 1 || id1 == 3) indices.push_back( std::make_pair(sub_id2, sub_id1) );

    }

  }

  void SubdivMesh::vertices_reorder_map(boost::numeric::ublas::bounded_matrix< vertex_descriptor, 5, 5 >& vtx_map,
                            const face_descriptor& f, const PolygonMesh& pmesh1, const PolygonMesh& pmesh2,
                            std::map< face_descriptor, std::vector< halfedge_descriptor > > subdiv_map_1,
                            std::map< face_descriptor, std::vector< halfedge_descriptor > > subdiv_map_2)
  {
    // child facets of facet f
    std::vector<halfedge_descriptor> hes_subdiv_1 = subdiv_map_1[f];
    for (auto i = 0u; i < hes_subdiv_1.size(); ++i) {
      halfedge_descriptor he_subdiv_1 = hes_subdiv_1[i];
      face_descriptor f_subdiv_1 = pmesh1.face(he_subdiv_1);

      // child facets of facet f_subdiv_1
      std::vector<halfedge_descriptor> hes_subdiv_2 = subdiv_map_2[f_subdiv_1];
      for (auto j = 0u; j < hes_subdiv_2.size(); ++j) {
        halfedge_descriptor he_subdiv_2 = hes_subdiv_2[j];
        face_descriptor f_subdiv_2 = pmesh2.face(he_subdiv_2);

        std::vector< std::pair<int, int> > indices;
        index_reorder_map(indices, i, j);

        // vertices of facet f_subdiv_2 (reorder to start from the smallest index)
        typename PolygonMesh::size_type degree_f_subdiv_2 = pmesh2.degree(f_subdiv_2);

        halfedge_descriptor he_min = pmesh2.halfedge(f_subdiv_2);
        halfedge_descriptor he_tmp = he_min;

        for (auto k = 0u; k < degree_f_subdiv_2; ++k) {
          if ( CGAL::target(he_tmp, pmesh2) < CGAL::target(he_min, pmesh2) ) he_min = he_tmp;
          he_tmp = CGAL::next(he_tmp, pmesh2);
        }

        halfedge_descriptor he = he_min;

        for (auto k = 0u; k < degree_f_subdiv_2; ++k) {
          vertex_descriptor vtx = CGAL::target(he, pmesh2);
          std::pair<int, int> index = indices[k];
          vtx_map(index.first, index.second) = vtx;
          he = CGAL::next(he, pmesh2);
        }

      } // loop over child facets of a child facet
    } // loop over child facets of a facet in original mesh

  }

  void SubdivMesh::vertices_limit_map(boost::numeric::ublas::bounded_matrix< Kernel::Point_3, 5, 5 >& vtx_limit_map,
                          boost::numeric::ublas::bounded_matrix< vertex_descriptor, 5, 5 > vtx_map,
                          const PolygonMesh& pmesh2)
  {
    for (int i = 0; i < 5; ++i) {
      for (int j = 0; j < 5; ++j) {
        vertex_descriptor vtx = vtx_map(i, j);

        typename PolygonMesh::size_type degree_vtx = pmesh2.degree(vtx);

        // get one-ring neighbouring vertices of the vertex in pmesh2
        std::vector<vertex_descriptor> onering_vertices_e;
        std::vector<vertex_descriptor> onering_vertices_f;

        onering_vertices_around_vertex(onering_vertices_e, onering_vertices_f, vtx, pmesh2);

        std::vector<double> point(3, 0.);

        // centre vertex
        Kernel::Point_3 p_vtx = pmesh2.point(vtx);
        for (int ii = 0; ii < 3; ++ii) {
          point[ii] += p_vtx[ii]*degree_vtx/(degree_vtx + 5);
        }

        // edge vertices
        for (auto ii = 0u; ii < onering_vertices_e.size(); ++ii) {
          vertex_descriptor vtx_e = onering_vertices_e[ii];
          Kernel::Point_3 p_e = pmesh2.point(vtx_e);

          for (int jj = 0; jj < 3; ++jj) {
            point[jj] += p_e[jj]*4./(degree_vtx*(degree_vtx + 5));
          }
        }

        // vertices not incident to vtx
        for (auto ii = 0u; ii < onering_vertices_f.size(); ++ii) {
          vertex_descriptor vtx_f = onering_vertices_f[ii];
          Kernel::Point_3 p_f = pmesh2.point(vtx_f);

          for (int jj = 0; jj < 3; ++jj) {
            point[jj] += p_f[jj]/(degree_vtx*(degree_vtx + 5));
          }
        }

        Kernel::Point_3 point_limit(point[0], point[1], point[2]);

        vtx_limit_map(i, j) = point_limit;

      }
    } // loop over subdivided vertices of a facet
  }

  bool SubdivMesh::check_regular_patch(const face_descriptor& f, const PolygonMesh& pmesh)
  {
    bool is_regular = true;

    typename PolygonMesh::size_type degree_face = pmesh.degree(f);

    halfedge_descriptor he = pmesh.halfedge(f);
    for (auto i = 0u; i < degree_face; ++i) {
      vertex_descriptor vtx = CGAL::target(he, pmesh);

      if (pmesh.degree(vtx) != 4) is_regular = false;

      he = CGAL::next(he, pmesh);
    }

    return is_regular;
  }

  void SubdivMesh::collect_edge_vertices_LS(std::vector< std::pair<int, int> >& indices, const int& edge_id)
  {
    if (edge_id == 0) {
      indices.push_back( std::make_pair(0, 4) ), indices.push_back( std::make_pair(0, 3) );
      indices.push_back( std::make_pair(0, 2) ), indices.push_back( std::make_pair(0, 1) );
      indices.push_back( std::make_pair(0, 0) );
    }
    else if (edge_id == 1) {
      indices.push_back( std::make_pair(0, 0) ), indices.push_back( std::make_pair(1, 0) );
      indices.push_back( std::make_pair(2, 0) ), indices.push_back( std::make_pair(3, 0) );
      indices.push_back( std::make_pair(4, 0) );
    }
    else if (edge_id == 2) {
      indices.push_back( std::make_pair(4, 0) ), indices.push_back( std::make_pair(4, 1) );
      indices.push_back( std::make_pair(4, 2) ), indices.push_back( std::make_pair(4, 3) );
      indices.push_back( std::make_pair(4, 4) );
    }
    else if (edge_id == 3) {
      indices.push_back( std::make_pair(4, 4) ), indices.push_back( std::make_pair(3, 4) );
      indices.push_back( std::make_pair(2, 4) ), indices.push_back( std::make_pair(1, 4) );
      indices.push_back( std::make_pair(0, 4) );
    }
  }

  void SubdivMesh::edge_least_square_fitting(std::vector< Kernel::Point_3 >& bezier_points, const std::vector< Kernel::Point_3 >& edge_limit_points)
  {
    Kernel::Vector_3 u(1./4., 1./2., 3./4.);

    Eigen::MatrixXd matrixL(3, 2);
    for (int i = 0; i < 3; ++i) {
      matrixL(i, 0) = 3.*u[i]*std::pow(1. - u[i], 2.);
      matrixL(i, 1) = 3.*std::pow(u[i], 2.)*(1. - u[i]);
    }

    Eigen::MatrixXd matrixLT = matrixL.transpose();

    Eigen::MatrixXd matrixP(3, 2);
    for (int i = 0; i < 3; ++i) {
      matrixP(i, 0) = std::pow(1. - u[i], 3.);
      matrixP(i, 1) = std::pow(u[i], 3.);
    }

    Eigen::MatrixXd matrixB1(3, 3);
    for (int i = 0; i < 3; ++i) {
      Kernel::Point_3 p = edge_limit_points[i + 1];
      for (int j = 0; j < 3; ++j) {
        matrixB1(i, j) = p[j];
      }
    }

    Eigen::MatrixXd matrixB2(2, 3);
    for (int i = 0; i < 3; ++i) {
      Kernel::Point_3 p1 = edge_limit_points[0];
      Kernel::Point_3 p2 = edge_limit_points[4];

      matrixB2(0, i) = p1[i];
      matrixB2(1, i) = p2[i];
    }

    Eigen::MatrixXd matrixR = matrixB1 - matrixP*matrixB2;

    Eigen::MatrixXd matrix_tmp = matrixLT*matrixL;
    Eigen::MatrixXd matrix_tmp_inv = matrix_tmp.inverse();
    Eigen::MatrixXd matrixLT_tmp = matrix_tmp_inv*matrixLT;

    Eigen::MatrixXd result = matrixLT_tmp*matrixR;

    Kernel::Point_3 bezier_point1( result(0, 0), result(0, 1), result(0, 2) );
    Kernel::Point_3 bezier_point2( result(1, 0), result(1, 1), result(1, 2) );

    bezier_points.push_back(bezier_point1);
    bezier_points.push_back(bezier_point2);

  }

  void SubdivMesh::collect_face_vertices_LS(std::vector< std::pair<int, int> >& indices)
  {
    indices.push_back( std::make_pair(1, 1) ), indices.push_back( std::make_pair(1, 2) ), indices.push_back( std::make_pair(1, 3) );
    indices.push_back( std::make_pair(2, 1) ), indices.push_back( std::make_pair(2, 2) ), indices.push_back( std::make_pair(2, 3) );
    indices.push_back( std::make_pair(3, 1) ), indices.push_back( std::make_pair(3, 2) ), indices.push_back( std::make_pair(3, 3) );
  }

  void SubdivMesh::face_least_square_fitting(std::map<int, Kernel::Point_3>& bezier_points, const std::vector< Kernel::Point_3 >& face_limit_points)
  {
    std::vector< std::pair<double, double> > params(9);
    params[0] = std::make_pair(1./4., 1./4.), params[1] = std::make_pair(1./2., 1./4.), params[2] = std::make_pair(3./4., 1./4.);
    params[3] = std::make_pair(1./4., 1./2.), params[4] = std::make_pair(1./2., 1./2.), params[5] = std::make_pair(3./4., 1./2.);
    params[6] = std::make_pair(1./4., 3./4.), params[7] = std::make_pair(1./2., 3./4.), params[8] = std::make_pair(3./4., 3./4.);

    Eigen::MatrixXd bernstein_full(9, 16); // 16 basis functions evaluated at 9 parameters
    for (int i = 0; i < 9; ++i) {
      double u = params[i].first, v = params[i].second;

      Eigen::Vector4d u_bernstein, v_bernstein;
      u_bernstein(0) = std::pow(1. - u, 3.), u_bernstein(1) = 3.*u*std::pow(1. - u, 2.), u_bernstein(2) = 3.*u*u*(1. - u), u_bernstein(3) = std::pow(u, 3.);
      v_bernstein(0) = std::pow(1. - v, 3.), v_bernstein(1) = 3.*v*std::pow(1. - v, 2.), v_bernstein(2) = 3.*v*v*(1. - v), v_bernstein(3) = std::pow(v, 3.);

      int count = 0;
      for (int j = 0; j < 4; ++j) {
        for (int k = 0; k < 4; ++k) {
          bernstein_full(i, count) = u_bernstein(k)*v_bernstein(j);
          count += 1;
        }
      }
    }

    int labelL[4] = {5, 6, 9, 10};
    int labelR[12] = {0, 1, 2, 3, 4, 7, 8, 11, 12, 13, 14, 15};

    Eigen::MatrixXd bernsteinL(9, 4), bernsteinR(9, 12);
    for (int i = 0; i < 9; ++i) {
      for (int j = 0; j < 4; ++j) {
        int label = labelL[j];
        bernsteinL(i, j) = bernstein_full(i, label);
      }

      for (int j = 0; j < 12; ++j) {
        int label = labelR[j];
        bernsteinR(i, j) = bernstein_full(i, label);
      }
    }

    Eigen::MatrixXd bezierR(12, 3);
    for (int i = 0; i < 12; ++i) {
      int label = labelR[i];

      for (int j = 0; j < 3; ++j) {
        bezierR(i, j) = bezier_points[label][j];
      }
    }

    Eigen::MatrixXd targets(9, 3);
    for (int i = 0; i < 9; ++i) {
      Kernel::Point_3 point = face_limit_points[i];
      for (int j = 0; j < 3; ++j) {
        targets(i, j) = point[j];
      }
    }

    Eigen::MatrixXd matrixR = targets - bernsteinR*bezierR;

    Eigen::MatrixXd bernsteinLT = bernsteinL.transpose();
    Eigen::MatrixXd matrix_tmp = bernsteinLT*bernsteinL;
    Eigen::MatrixXd matrix_tmp_inv = matrix_tmp.inverse();
    Eigen::MatrixXd matrixLT_tmp = matrix_tmp_inv*bernsteinLT;

    Eigen::MatrixXd result = matrixLT_tmp*matrixR;

    for (int i = 0; i < 4; ++i) {
      int label = labelL[i];

      Kernel::Point_3 point( result(i, 0), result(i, 1), result(i, 2) );
      bezier_points[label] = point;
    }

  }

  void SubdivMesh::bspline_to_bezier(std::map<int, Kernel::Point_3>& bezier_points, const Eigen::MatrixXd& cps_spline)
  {
    const int degree = 3;
    const int num_segments = degree + 1;

    Eigen::MatrixXd coeffs_bezier;
    coeffs_bezier.resize(num_segments, degree + 1);

    // Bezier coefficients for a cubic b-spline
    coeffs_bezier(0, 0) = 1., coeffs_bezier(0, 1) = 0., coeffs_bezier(0, 2) = 0., coeffs_bezier(0, 3) = 0.;
    coeffs_bezier(1, 0) = 4., coeffs_bezier(1, 1) = 4., coeffs_bezier(1, 2) = 2., coeffs_bezier(1, 3) = 1.;
    coeffs_bezier(2, 0) = 1., coeffs_bezier(2, 1) = 2., coeffs_bezier(2, 2) = 4., coeffs_bezier(2, 3) = 4.;
    coeffs_bezier(3, 0) = 0., coeffs_bezier(3, 1) = 0., coeffs_bezier(3, 2) = 0., coeffs_bezier(3, 3) = 1.;

    coeffs_bezier /= 6.;

    // transformation matrix from B-spline to Bezier
    Eigen::MatrixXd coeffs_transform;
    coeffs_transform.resize( (degree + 1)*num_segments, num_segments*num_segments );

    for (int j = 0; j < num_segments; ++j) {
      Eigen::MatrixXd coeffsj = coeffs_bezier.block(j, 0, 1, degree + 1);

      for (int i = 0; i < num_segments; ++i) {
        Eigen::MatrixXd coeffsi = coeffs_bezier.block(i, 0, 1, degree + 1);

        int col_id = i + j*num_segments;

        for (int jj = 0; jj < degree + 1; ++jj) {
          for (int ii = 0; ii < degree + 1; ++ii) {
            double coeff = coeffsi(0, ii)*coeffsj(0, jj);
            int row_id = ii + jj*(degree + 1);
            coeffs_transform(row_id, col_id) = coeff;
          }
        }
      }
    }

    // Bezier control points
    Eigen::MatrixXd cps_bezier = coeffs_transform*cps_spline;

    for (int i = 0; i < num_segments*num_segments; ++i) {
      Kernel::Point_3 point( cps_bezier(i, 0), cps_bezier(i, 1), cps_bezier(i, 2) );
      bezier_points[i] = point;
    }
  }

  void SubdivMesh::onering_vertices_around_face(std::vector< vertex_descriptor >& onering_vertices,
                                    const face_descriptor& f,
                                    const PolygonMesh& pmesh)
  {
    std::vector< vertex_descriptor > vertices_face;
    std::vector< vertex_descriptor > neighbour_vertices_face;

    typename PolygonMesh::size_type degree_face = pmesh.degree(f);
    halfedge_descriptor he = pmesh.halfedge(f);
    for (auto i = 0u; i < degree_face; ++i) {
      vertex_descriptor vtx = CGAL::target(he, pmesh);
      vertices_face.push_back(vtx);
      neighbour_vertices_face.push_back(vtx);
      he = CGAL::next(he, pmesh);
    }

    bool is_continue = true;
    while (is_continue) {
      halfedge_descriptor he_opposite = CGAL::opposite(he, pmesh);
      while (true) {
        he = CGAL::next(he_opposite, pmesh);
        vertex_descriptor vtx = CGAL::target(he, pmesh);

        std::vector<vertex_descriptor>::iterator it = std::find(vertices_face.begin(), vertices_face.end(), vtx);

        if (it != vertices_face.end()) {
          break;
        }
        else {
          neighbour_vertices_face.push_back(vtx);
          he_opposite = he;

          if (neighbour_vertices_face.size() == 16) {
            is_continue = false;
            break;
          }
        }
      }
    }

    // define an index array
    int id_array[16] = {5, 9, 10, 6, 2, 1, 0, 4, 8, 12, 13, 14, 15, 11, 7, 3};

    for (auto i = 0u; i < neighbour_vertices_face.size(); ++i) {
      int id = id_array[i];
      onering_vertices[id] = neighbour_vertices_face[i];
    }

  }

  void SubdivMesh::create_bezier_control_points(std::map< std::pair<int, int>, Kernel::Point_3 >& cps_bezier, std::map<int, Kernel::Point_3> bezier_patch)
  {
    int d1 = 3, d2 = 3;

    int count = 0;

    // note the ordering of control points (related to u and v directions)
    for (int j = 0; j < d2 + 1; ++j) {
      for (int i = 0; i < d1 + 1; ++i) {
        std::pair<int, int> id = std::make_pair(i, j);
        Kernel::Point_3 point = bezier_patch[count];
        cps_bezier[id] = point;
        count += 1;
      }
    }

  }

  Kernel::Point_3 SubdivMesh::evaluate_bezier_surface(const double& u, const double& v,
                                          std::map< std::pair<int, int>, Kernel::Point_3 > cps_bezier)
  {
    std::vector<double> p(3, 0.);

    int d1 = 3, d2 = 3;

    for (int j = 0; j < d2 + 1; ++j) {
      double coeff1 = boost::math::binomial_coefficient<double>(d2, j);
      double bernstein1 = coeff1*std::pow(v, j)*std::pow(1. - v, d2 - j);

      for (int i = 0; i < d1 + 1; ++i) {
        double coeff2 = boost::math::binomial_coefficient<double>(d1, i);
        double bernstein2 = coeff2*std::pow(u, i)*std::pow(1. - u, d1 -i);

        std::pair<int, int> id = std::make_pair(i, j);
        Kernel::Point_3 bezier_point = cps_bezier[id];

        for (int k = 0; k < 3; ++k) {
          p[k] += bernstein1*bernstein2*bezier_point[k];
        }
      }

    }

    Kernel::Point_3 point( p[0], p[1], p[2] );

    return point;
  }
