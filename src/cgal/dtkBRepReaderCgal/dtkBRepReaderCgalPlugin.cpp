// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkBRepReaderCgal.h"
#include "dtkBRepReaderCgalPlugin.h"
#include "dtkContinuousGeometry.h"

#include <dtkCore>

// ///////////////////////////////////////////////////////////////////
// dtkBRepReaderCgalPlugin
// ///////////////////////////////////////////////////////////////////

void dtkBRepReaderCgalPlugin::initialize(void)
{
    dtkContinuousGeometry::bRepReader::pluginFactory().record("dtkBRepReaderCgal", dtkBRepReaderCgalCreator);
}

void dtkBRepReaderCgalPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkBRepReaderCgal)

//
// dtkBRepReaderCgalPlugin.cpp ends here
