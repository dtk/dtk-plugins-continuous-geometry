// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkBRepReaderCgal.h"

#include <CGAL/Simple_cartesian.h>

#include <CGAL/Surface_mesh.h>
#include <CGAL/boost/graph/graph_traits_Surface_mesh.h>

#include <CGAL/subdivision_method_3.h>
#include <CGAL/Timer.h>

#include <CGAL/boost/graph/copy_face_graph.h>
#include <CGAL/boost/graph/helpers.h>
#include <CGAL/boost/graph/iterator.h>
#include <CGAL/circulator.h>

#include <boost/lexical_cast.hpp>

#include <boost/array.hpp>
#include <boost/foreach.hpp>
#include <boost/mpl/if.hpp>
#include <boost/unordered_map.hpp>
#include <boost/unordered_set.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/math/special_functions/binomial.hpp>

#include <Eigen/Dense>

#include <dtkAbstractRationalBezierSurfaceData.h>
#include <dtkRationalBezierSurface.h>
#include <dtkContinuousGeometry.h>
#include <dtkBRep.h>
#include <dtkBRepData.h>
#include <dtkNurbsSurface.h>
#include <dtkAbstractNurbsSurfaceData.h>

#include <iterator>
#include <list>
#include <vector>

#include <iostream>
#include <fstream>

typedef CGAL::Simple_cartesian<double>         Kernel;
typedef CGAL::Surface_mesh<Kernel::Point_3>    PolygonMesh;

typedef typename boost::graph_traits<PolygonMesh> graph_traits;

typedef typename graph_traits::vertex_descriptor vertex_descriptor;
typedef typename graph_traits::halfedge_descriptor halfedge_descriptor;
typedef typename graph_traits::edge_descriptor edge_descriptor;
typedef typename graph_traits::face_descriptor face_descriptor;

using namespace std;
using namespace CGAL;

#include "subdivFuns.hpp"

// /////////////////////////////////////////////////////////////////
// dtkBRepReaderCgalPrivate
// /////////////////////////////////////////////////////////////////

class dtkBRepReaderCgalPrivate
{
public:
    QString in_stl_file_path;
    QString in_dump_file_path;

    dtkBRep* dtk_brep;
};

// ///////////////////////////////////////////////////////////////////
// dtkBRepReaderCgal implementation
// ///////////////////////////////////////////////////////////////////

dtkBRepReaderCgal::dtkBRepReaderCgal(void) : dtkBRepReader(), d(new dtkBRepReaderCgalPrivate)
{
    d->dtk_brep = nullptr;
}

dtkBRepReaderCgal::~dtkBRepReaderCgal(void)
{
    delete d;
    d = nullptr;
}

void dtkBRepReaderCgal::setInputBRepFilePath(const QString& file)
{
    d->in_stl_file_path = file;
}

void dtkBRepReaderCgal::setInputDumpFilePath(const QString& file)
{
    d->in_dump_file_path = file;
}

void dtkBRepReaderCgal::run(void)
{
  PolygonMesh pmesh;
  std::ifstream in(d->in_stl_file_path.toStdString());
  if(in.fail()) {
    std::cerr << "Could not open input file " << d->in_stl_file_path.toStdString() << std::endl;
    return;
  }
  in >> pmesh;

  // test of surface mesh usage
  typedef typename graph_traits::face_iterator face_iterator;

  // mesh information
  typename graph_traits::vertices_size_type num_vertices = CGAL::num_vertices(pmesh);
  typename graph_traits::halfedges_size_type num_edges = CGAL::num_halfedges(pmesh)/2;
  typename graph_traits::faces_size_type num_facets = CGAL::num_faces(pmesh);

  std::cout << "# The polygon mesh has ";
  std::cout << num_vertices << " vertices, ";
  std::cout << num_edges << " edges, ";
  std::cout << num_facets << " faces." << std::endl;

  // find subdivided vertices of a facet
  // define an auxiliary mesh to subdivide
  PolygonMesh pmesh1;
  //CGAL::copy_face_graph(pmesh, pmesh1);
  in.clear();
  in.seekg(0, ios::beg);
  in >> pmesh1;

  Subdivision_method_3::CatmullClark_subdivision(pmesh1,
      Subdivision_method_3::parameters::vertex_point_map(get(vertex_point, pmesh1)).number_of_iterations(1));

  //===========================================================================
  // using halfedge information before and after subdivision
  //===========================================================================
  // loop over facets of the original mesh
  std::map<face_descriptor, std::vector<halfedge_descriptor> > subdiv_map_1;

  SubdivMesh::collect_subdivision_map(subdiv_map_1, pmesh, pmesh1);
  // define another auxiliary mesh to subdivide
  PolygonMesh pmesh2;
  in.clear();
  in.seekg(0, ios::beg);
  in >> pmesh2;

  Subdivision_method_3::CatmullClark_subdivision(pmesh2,
        Subdivision_method_3::parameters::vertex_point_map(get(vertex_point, pmesh2)).number_of_iterations(2));

  // loop over facets of the subdivided mesh pmesh1
  std::map<face_descriptor, std::vector<halfedge_descriptor> > subdiv_map_2;

  SubdivMesh::collect_subdivision_map(subdiv_map_2, pmesh1, pmesh2);

  dtkBRepData* brep_data = new dtkBRepData();

  d->dtk_brep = new dtkBRep(brep_data);

  // loop over original mesh and locate subdivided vertices
  int count_face = 0;
  for (face_iterator fitr = pmesh.faces_begin(); fitr != pmesh.faces_end(); ++fitr) {
    face_descriptor f = *fitr;

    //std::cout << "original facet " << f << ": " << std::endl;

    // rearrange the subdivided vertices in a 5 x 5 map
    boost::numeric::ublas::bounded_matrix< vertex_descriptor, 5, 5 > vtx_map; // it contains 5 x 5 vertices after two subdivisions of a facet
    SubdivMesh::vertices_reorder_map(vtx_map, f, pmesh1, pmesh2, subdiv_map_1, subdiv_map_2);

    // compute limit positions of vertices
    boost::numeric::ublas::bounded_matrix< Kernel::Point_3, 5, 5 > vtx_limit_map;
    SubdivMesh::vertices_limit_map(vtx_limit_map, vtx_map, pmesh2);

    // create Bezier patch
    bool is_regular = SubdivMesh::check_regular_patch(f, pmesh);

    typename PolygonMesh::size_type degree_face = pmesh.degree(f);

    if (is_regular == true) {
      // regular patch
      std::vector<vertex_descriptor> onering_vertices_face(16);
      SubdivMesh::onering_vertices_around_face(onering_vertices_face, f, pmesh);

      Eigen::MatrixXd cps_spline;
      cps_spline.resize(16, 3);

      for (int i = 0; i < 16; ++i) {
        vertex_descriptor vtx = onering_vertices_face[i];
        Kernel::Point_3 point = pmesh.point(vtx);
        for (int j = 0; j < 3; ++j) {
          cps_spline(i, j) = point[j];
        }
      }

      std::map<int, Kernel::Point_3> bezier_patch_regular;

      SubdivMesh::bspline_to_bezier(bezier_patch_regular, cps_spline);

      // rearrange Bezier control points
      std::map< std::pair<int, int>, Kernel::Point_3 > cps_bezier_regular;
      SubdivMesh::create_bezier_control_points(cps_bezier_regular, bezier_patch_regular);

      // try to convert to NURBS surfaces!
      dtkAbstractNurbsSurfaceData* nurbs_surface_data = dtkContinuousGeometry::abstractNurbsSurfaceData::pluginFactory().create("dtkNurbsSurfaceDataOn");

      dtkNurbsSurface* nurbs_surface = new dtkNurbsSurface(nurbs_surface_data);

      std::size_t dim = 3;
      std::size_t nb_cp_u = 4;
      std::size_t nb_cp_v = 4;
      std::size_t order_u = 4;
      std::size_t order_v = 4;

      double* knots_u = new double[6];
      knots_u[0] = 0., knots_u[1] = 0., knots_u[2] = 0.;
      knots_u[3] = 1., knots_u[4] = 1., knots_u[5] = 1.;

      double* knots_v = new double[6];
      knots_v[0] = 0., knots_v[1] = 0., knots_v[2] = 0.;
      knots_v[3] = 1., knots_v[4] = 1., knots_v[5] = 1.;

      double* cps = new double[(dim + 1)*nb_cp_u*nb_cp_v];

      for (auto i = 0u; i < nb_cp_u; ++i) {
        for (auto j = 0u; j < nb_cp_v; ++j) {
          std::pair<int, int> index = std::make_pair(i, j);
          Kernel::Point_3 coord = cps_bezier_regular[index];
          for (int k = 0; k < 3; ++k) {
            cps[(dim + 1)*(j + i*nb_cp_v) + k] = coord[k];
          }
          cps[(dim + 1)*(j + i*nb_cp_v) + 3] = 1.;
        }
      }

      nurbs_surface->create(dim, nb_cp_u, nb_cp_v, order_u, order_v, knots_u, knots_v, cps);

      brep_data->m_nurbs_surfaces.push_back(nurbs_surface);

      for (auto i = 0u; i < bezier_patch_regular.size(); ++i) {
        Kernel::Point_3 coord = bezier_patch_regular[i];
        if (i == 0 && count_face == 0) {
          for (int j = 0; j < 3; ++j) {
            brep_data->m_aabb[j] = coord[j];
          }
          for (int j = 3; j < 6; ++j) {
            brep_data->m_aabb[j] = coord[j - 3];
          }
        }

        else {
          for (int j = 0; j < 3; ++j) {
            if (coord[j] < brep_data->m_aabb[j]) brep_data->m_aabb[j] = coord[j];
            if (coord[j] > brep_data->m_aabb[j + 3]) brep_data->m_aabb[j + 3] = coord[j];
          }
        }

      }

    }
    else {
      // irregular patch, need least-square fitting approximation
      std::map<int, Kernel::Point_3> bezier_patch_ev;

      // corners use limit positions directly
      bezier_patch_ev[0] = vtx_limit_map(0, 0);
      bezier_patch_ev[3] = vtx_limit_map(0, 4);
      bezier_patch_ev[12] = vtx_limit_map(4, 0);
      bezier_patch_ev[15] = vtx_limit_map(4, 4);

      // internal edges
      // start from the edge with the smallest target id
      halfedge_descriptor he_min = pmesh.halfedge(f);
      halfedge_descriptor he_tmp = pmesh.halfedge(f);
      for (auto i = 0u; i < degree_face; ++i) {
        if ( CGAL::target(he_tmp, pmesh) < CGAL::target(he_min, pmesh) ) {
          he_min = he_tmp;
        }
        he_tmp = CGAL::next(he_tmp, pmesh);
      }

      halfedge_descriptor he = he_min;
      // loop over the halfedges of the facet
      for (auto i = 0u; i < degree_face; ++i) {
        face_descriptor fn = pmesh.face( CGAL::opposite(he, pmesh) );

        // if the neighbouring facet is regular
        if (SubdivMesh::check_regular_patch(fn, pmesh) == true) {
          // find vertices that determine the limit position of edge vertices
          std::vector<vertex_descriptor> vertices_edge_limit;
          halfedge_descriptor he_loop = he;
          for (auto j = 0u; j < degree_face; ++j) {
            vertex_descriptor vtx = CGAL::target(he_loop, pmesh);
            vertices_edge_limit.push_back(vtx);
            he_loop = CGAL::next(he_loop, pmesh);
          }

          typename PolygonMesh::size_type degree_fn = pmesh.degree(fn);
          he_loop = CGAL::opposite(he_loop, pmesh);
          he_loop = CGAL::next(he_loop, pmesh);
          for (auto j = 0u; j < degree_fn; ++j) {
            vertex_descriptor vtx = CGAL::target(he_loop, pmesh);
            if ( std::find(vertices_edge_limit.begin(), vertices_edge_limit.end(), vtx) == vertices_edge_limit.end() ) {
              vertices_edge_limit.push_back(vtx);
            }
            he_loop = CGAL::next(he_loop, pmesh);
          }

          // the limit coefficients
          double edge_limit_coeffs1[6] = {4./9., 1./9., 1./18., 2./9., 1./18., 1./9.};
          double edge_limit_coeffs2[6] = {2./9., 1./18., 1./9., 4./9., 1./9., 1./18.};

          std::vector<double> ep1(3, 0.), ep2(3, 0.);
          for (auto j = 0u; j < vertices_edge_limit.size(); ++j) {
            vertex_descriptor vtx = vertices_edge_limit[j];
            Kernel::Point_3 p = pmesh.point(vtx);

            for (int k = 0; k < 3; ++k) {
              ep1[k] += p[k]*edge_limit_coeffs1[j];
              ep2[k] += p[k]*edge_limit_coeffs2[j];
            }
          }

          Kernel::Point_3 ep_limit1(ep1[0], ep1[1], ep1[2]);
          Kernel::Point_3 ep_limit2(ep2[0], ep2[1], ep2[2]);

          if (i == 0) bezier_patch_ev[1] = ep_limit1, bezier_patch_ev[2] = ep_limit2;
          else if (i == 1) bezier_patch_ev[8] = ep_limit1, bezier_patch_ev[4] = ep_limit2;
          else if (i == 2) bezier_patch_ev[14] = ep_limit1, bezier_patch_ev[13] = ep_limit2;
          else if (i == 3) bezier_patch_ev[7] = ep_limit1, bezier_patch_ev[11] = ep_limit2;

        } // for edges with regular neighbouring facet
        else {
          std::vector< std::pair<int, int> > edge_vertices_LS;
          SubdivMesh::collect_edge_vertices_LS(edge_vertices_LS, i);

          // limit points on the edge to be approximated by least-square fitting
          std::vector< Kernel::Point_3 > edge_limit_points_LS;
          for (auto j = 0u; j < edge_vertices_LS.size(); ++j) {
            std::pair<int, int> edge_vertex_LS = edge_vertices_LS[j];
            Kernel::Point_3 limit_point = vtx_limit_map(edge_vertex_LS.first, edge_vertex_LS.second);
            edge_limit_points_LS.push_back(limit_point);
          }

          // least-square fitting by bezier points
          std::vector< Kernel::Point_3 > bezier_points;
          SubdivMesh::edge_least_square_fitting(bezier_points, edge_limit_points_LS);

          if (i == 0) bezier_patch_ev[2] = bezier_points[0], bezier_patch_ev[1] = bezier_points[1];
          else if (i == 1) bezier_patch_ev[4] = bezier_points[0], bezier_patch_ev[8] = bezier_points[1];
          else if (i == 2) bezier_patch_ev[13] = bezier_points[0], bezier_patch_ev[14] = bezier_points[1];
          else if (i == 3) bezier_patch_ev[11] = bezier_points[0], bezier_patch_ev[7] = bezier_points[1];

        } // for edges with irregular neighbouring facet

        he = CGAL::next(he, pmesh);

      } // loop over halfedges of the facet

      // centre bezier points within facet using least-square fitting
      std::vector< std::pair<int, int> > face_vertices_LS;
      SubdivMesh::collect_face_vertices_LS(face_vertices_LS);

      std::vector< Kernel::Point_3 > face_limit_points_LS;
      for (auto j = 0u; j < face_vertices_LS.size(); ++j) {
        std::pair<int, int> face_vertex_LS = face_vertices_LS[j];
        Kernel::Point_3 limit_point = vtx_limit_map(face_vertex_LS.first, face_vertex_LS.second);
        face_limit_points_LS.push_back(limit_point);
      }

      // least-square fitting by bezier points
      SubdivMesh::face_least_square_fitting(bezier_patch_ev, face_limit_points_LS);

      // rearrange Bezier control points
      std::map< std::pair<int, int>, Kernel::Point_3 > cps_bezier_ev;
      SubdivMesh::create_bezier_control_points(cps_bezier_ev, bezier_patch_ev);

      // convert to NURBS surface
      dtkAbstractNurbsSurfaceData* nurbs_surface_data = dtkContinuousGeometry::abstractNurbsSurfaceData::pluginFactory().create("dtkNurbsSurfaceDataOn");

      dtkNurbsSurface* nurbs_surface = new dtkNurbsSurface(nurbs_surface_data);

      std::size_t dim = 3;
      std::size_t nb_cp_u = 4;
      std::size_t nb_cp_v = 4;
      std::size_t order_u = 4;
      std::size_t order_v = 4;

      double* knots_u = new double[6];
      knots_u[0] = 0., knots_u[1] = 0., knots_u[2] = 0.;
      knots_u[3] = 1., knots_u[4] = 1., knots_u[5] = 1.;

      double* knots_v = new double[6];
      knots_v[0] = 0., knots_v[1] = 0., knots_v[2] = 0.;
      knots_v[3] = 1., knots_v[4] = 1., knots_v[5] = 1.;

      double* cps = new double[(dim + 1)*nb_cp_u*nb_cp_v];

      for (auto i = 0u; i < nb_cp_u; ++i) {
        for (auto j = 0u; j < nb_cp_v; ++j) {
          std::pair<int, int> index = std::make_pair(i, j);
          Kernel::Point_3 coord = cps_bezier_ev[index];
          for (int k = 0; k < 3; ++k) {
            cps[(dim + 1)*(j + i*nb_cp_v) + k] = coord[k];
          }
          cps[(dim + 1)*(j + i*nb_cp_v) + 3] = 1.;
        }
      }

      nurbs_surface->create(dim, nb_cp_u, nb_cp_v, order_u, order_v, knots_u, knots_v, cps);

      brep_data->m_nurbs_surfaces.push_back(nurbs_surface);

      for (auto i = 0u; i < bezier_patch_ev.size(); ++i) {
        Kernel::Point_3 coord = bezier_patch_ev[i];
        if (i == 0 && count_face == 0) {
          for (int j = 0; j < 3; ++j) {
            brep_data->m_aabb[j] = coord[j];
          }
          for (int j = 3; j < 6; ++j) {
            brep_data->m_aabb[j] = coord[j - 3];
          }
        }

        else {
          for (int j = 0; j < 3; ++j) {
            if (coord[j] < brep_data->m_aabb[j]) brep_data->m_aabb[j] = coord[j];
            if (coord[j] > brep_data->m_aabb[j + 3]) brep_data->m_aabb[j + 3] = coord[j];
          }
        }

      }

    } // irregular patches

    count_face += 1;

  } // loop over facets in orginal mesh
}

dtkBRep* dtkBRepReaderCgal::outputDtkBRep(void) const
{
    return d->dtk_brep;
}

//
// dtkBRepReaderCgal.cpp ends here
