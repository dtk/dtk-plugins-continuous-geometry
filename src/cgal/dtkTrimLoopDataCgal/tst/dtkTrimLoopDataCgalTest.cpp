// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkTrimLoopDataCgalTest.h"

#include "dtkTrimLoopDataCgal.h"

#include <dtkContinuousGeometry>
#include <dtkContinuousGeometryUtils>

#include <dtkAbstractNurbsCurve2DData>
#include <dtkNurbsCurve2D>
#include <dtkAbstractNurbsSurfaceData>
#include <dtkNurbsSurface>

#include <dtkTopoTrim>
#include <dtkTrim>
#include <dtkTrimLoop>

#include <dtkTest>

// ///////////////////////////////////////////////////////////////////
// D-Pointer implementation
// ///////////////////////////////////////////////////////////////////

class dtkTrimLoopDataCgalTestCasePrivate{
public:
    dtkNurbsSurface* nurbs_surface;
    dtkAbstractNurbsSurfaceData* nurbs_surface_data;

    dtkNurbsCurve2D* nurbs_curve_1;
    dtkNurbsCurve2D* nurbs_curve_2;
    dtkNurbsCurve2D* nurbs_curve_3;

    dtkTopoTrim* topo_trim_1;
    dtkTopoTrim* topo_trim_2;
    dtkTopoTrim* topo_trim_3;

    dtkTrim* trim_1;
    dtkTrim* trim_2;
    dtkTrim* trim_3;

    dtkTrimLoopDataCgal* trim_loop_data;
    dtkTrimLoop* trim_loop;

    double tolerance = 1e-15;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkTrimLoopDataCgalTestCase::dtkTrimLoopDataCgalTestCase(void):d(new dtkTrimLoopDataCgalTestCasePrivate())
{
    dtkLogger::instance().attachConsole();

    dtkContinuousGeometrySettings settings;
    settings.beginGroup("continuous-geometry");
    dtkContinuousGeometry::initialize(settings.value("plugins").toString());
    settings.endGroup();
}

dtkTrimLoopDataCgalTestCase::~dtkTrimLoopDataCgalTestCase(void)
{
}

void dtkTrimLoopDataCgalTestCase::initTestCase(void)
{
    std::size_t dim = 2;
    std::size_t nb_cp = 3;
    std::size_t order = 3;

    /* Forces openNURBS plugins to be compiled and working
       TODO : change to default implementation
    */
    dtkAbstractNurbsCurve2DData* data_1 = dtkContinuousGeometry::abstractNurbsCurve2DData::pluginFactory().create("dtkNurbsCurve2DDataOn");
    dtkAbstractNurbsCurve2DData* data_2 = dtkContinuousGeometry::abstractNurbsCurve2DData::pluginFactory().create("dtkNurbsCurve2DDataOn");
    dtkAbstractNurbsCurve2DData* data_3 = dtkContinuousGeometry::abstractNurbsCurve2DData::pluginFactory().create("dtkNurbsCurve2DDataOn");
    d->nurbs_surface_data = dtkContinuousGeometry::abstractNurbsSurfaceData::pluginFactory().create("dtkNurbsSurfaceDataOn");

    if (data_1 == nullptr || data_2 == nullptr || data_3 == nullptr) {
        dtkFatal() << "The dtkAbstractNurbsCurve2DData could not be loaded by the factory under the openNURBS implementation";
    }
    if (d->nurbs_surface_data == nullptr) {
       dtkFatal() << "The dtkAbstractNurbsSurfaceData could not be loaded by the factory under the openNURBS implementation";
    }

    d->nurbs_curve_1 = new dtkNurbsCurve2D(data_1);
    d->nurbs_curve_2 = new dtkNurbsCurve2D(data_2);
    d->nurbs_curve_3 = new dtkNurbsCurve2D(data_3);

    d->nurbs_surface = new dtkNurbsSurface(d->nurbs_surface_data);

    double* knots_1 = new double[nb_cp + order - 2];
    /* 0. */ knots_1[0] = 0.; knots_1[1] = 0.; knots_1[2] = 1.; knots_1[3] = 1.; /* 1.*/

    double* knots_2 = new double[nb_cp + order - 2];
    /* 0. */ knots_2[0] = 0.; knots_2[1] = 0.; knots_2[2] = 1.; knots_2[3] = 1.; /* 1.*/

    double* knots_3 = new double[nb_cp + order - 2];
    /* 0. */ knots_3[0] = 0.; knots_3[1] = 0.; knots_3[2] = 1.; knots_3[3] = 1.; /* 1.*/

    double* cps_1 = new double[(dim + 1) * nb_cp];
    cps_1[0] = 0.;   cps_1[1] = 0.; cps_1[2] = 1.;
    cps_1[3] = 0.;   cps_1[4] = 3.46; cps_1[5] = 1.;
    cps_1[6] = 2.;   cps_1[7] = 3.46; cps_1[8] = 1.;

    double* cps_2 = new double[(dim + 1) * nb_cp];
    cps_2[0] = 2.;   cps_2[1] = 3.46; cps_2[2] = 1.;
    cps_2[3] = 4.;   cps_2[4] = 3.46; cps_2[5] = 1.;
    cps_2[6] = 4.;   cps_2[7] = 0.; cps_2[8] = 1.;

    double* cps_3 = new double[(dim + 1) * nb_cp];
    cps_3[0] = 4.;   cps_3[1] = 0.; cps_3[2] = 1.;
    cps_3[3] = 2.;   cps_3[4] = -1.732; cps_3[5] = 1.;
    cps_3[6] = 0.;   cps_3[7] = 0.; cps_3[8] = 1.;

    d->nurbs_curve_1->create(nb_cp, order, knots_1, cps_1);
    d->nurbs_curve_2->create(nb_cp, order, knots_2, cps_2);
    d->nurbs_curve_3->create(nb_cp, order, knots_3, cps_3);


    d->topo_trim_1 = new dtkTopoTrim();
    d->topo_trim_2 = new dtkTopoTrim();
    d->topo_trim_3 = new dtkTopoTrim();

    d->trim_1 = new dtkTrim(*d->nurbs_surface, *d->nurbs_curve_1, d->topo_trim_1);
    d->trim_2 = new dtkTrim(*d->nurbs_surface, *d->nurbs_curve_2, d->topo_trim_2);
    d->trim_3 = new dtkTrim(*d->nurbs_surface, *d->nurbs_curve_3, d->topo_trim_3);


    d->trim_loop_data = new dtkTrimLoopDataCgal();
    d->trim_loop = new dtkTrimLoop(dynamic_cast< dtkAbstractTrimLoopData * >(d->trim_loop_data));

    d->trim_loop->trims().push_back(d->trim_1);
    d->trim_loop->trims().push_back(d->trim_2);
    d->trim_loop->trims().push_back(d->trim_3);

    d->trim_loop->setType(dtkContinuousGeometryEnums::TrimLoopType::inner);
}

void dtkTrimLoopDataCgalTestCase::init(void)
{

}

void dtkTrimLoopDataCgalTestCase::testType(void)
{
    QVERIFY(d->trim_loop->type() == dtkContinuousGeometryEnums::TrimLoopType::inner);
}

void dtkTrimLoopDataCgalTestCase::testToPolyline(void)
{
    std::list< dtkContinuousGeometryPrimitives::Point_2 > polyline;
    bool ok = d->trim_loop->toPolyline(polyline, 0.1);
    QVERIFY(ok);
}

void dtkTrimLoopDataCgalTestCase::testIsPointCulled(void)
{
    QVERIFY(d->trim_loop->isPointCulled(dtkContinuousGeometryPrimitives::Point_2(2., 1.5)) == false);
    QVERIFY(d->trim_loop->isPointCulled(dtkContinuousGeometryPrimitives::Point_2(5., 0.)) == true);
}

void dtkTrimLoopDataCgalTestCase::cleanup(void)
{

}

void dtkTrimLoopDataCgalTestCase::cleanupTestCase(void)
{
    delete d->nurbs_curve_1;
    delete d->nurbs_curve_2;
    delete d->nurbs_curve_3;

    delete d->trim_1;
    delete d->trim_2;
    delete d->trim_3;

    delete d->topo_trim_1;
    delete d->topo_trim_2;
    delete d->topo_trim_3;

    delete d->trim_loop;
}

DTKTEST_MAIN_NOGUI(dtkTrimLoopDataCgalTest, dtkTrimLoopDataCgalTestCase)

//
// dtkTrimLoopDataCgalTest.cpp ends here
