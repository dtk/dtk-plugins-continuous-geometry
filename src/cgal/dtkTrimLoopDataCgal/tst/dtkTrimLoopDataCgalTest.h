// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <QTest>

class dtkTrimLoopDataCgalTestCasePrivate;

class dtkTrimLoopDataCgalTestCase : public QObject
{
    Q_OBJECT

public:
    dtkTrimLoopDataCgalTestCase(void);
    ~dtkTrimLoopDataCgalTestCase(void);

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testType(void);
    void testToPolyline(void);
    void testIsPointCulled(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);

private:
    dtkTrimLoopDataCgalTestCasePrivate* d;
};

//
// dtkTrimLoopDataCgalTest.h ends here
