// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#include "dtkTrimLoopDataCgal.h"
#include "dtkTrimLoopDataCgalPlugin.h"

#include <dtkCore>
#include <dtkContinuousGeometry>

// ///////////////////////////////////////////////////////////////////
// dtkTrimLoopDataCgalPlugin
// ///////////////////////////////////////////////////////////////////

void dtkTrimLoopDataCgalPlugin::initialize(void)
{
    dtkContinuousGeometry::abstractTrimLoopData::pluginFactory().record("dtkTrimLoopDataCgal", dtkTrimLoopDataCgalCreator);
}

void dtkTrimLoopDataCgalPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkTrimLoopDataCgal)

//
// dtkTrimLoopDataCgalPlugin.cpp ends here
