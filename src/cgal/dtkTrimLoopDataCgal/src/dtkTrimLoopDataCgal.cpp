// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkTrimLoopDataCgal.h"

#include <dtkNurbsCurve2D>
#include <dtkTrim>
#include <dtkNurbsCurve2DCircleIntersector>

#include <CGAL/Polygon_2_algorithms.h>
#include <CGAL/Polygon_2.h>

#include <array>

typedef CGAL::Polygon_2< K > Polygon_2;

/*!
  \class dtkTrimLoopDataCgal
  \inmodule dtkPluginsContinuousGeometry
  \brief dtkTrimLoopDataCgal is a CGAL implementation of the concept dtkAbstracTrimLoopData.

  The following lines of code show how to instanciate and use the class in pratice:
  \code
  dtkAbstracTrimLoopData *trim_loop_data = dtkContinuousGeometry::abstractTrimLoopData::pluginFactory().create("dtkTrimLoopDataCgal");
  dtkTrimLoop *trim_loop = new dtkTrimLoop(trim_loop);
  trim_loop->trims().push_back(...);
  trim_loop->setType(...);
  \endcode
*/

/*! \fn dtkTrimLoopDataCgal::dtkTrimLoopDataCgal(void)
   Instanciates an empty class.
*/
dtkTrimLoopDataCgal::dtkTrimLoopDataCgal(void) : m_initialized(false) {}

/*! \fn dtkTrimLoopDataCgal::~dtkTrimLoopDataCgal(void)
  Destroys the instance.
*/
dtkTrimLoopDataCgal::~dtkTrimLoopDataCgal(void)
{
  for(auto trim : m_trims)
    delete trim;
}

/*! \fn dtkTrimLoopDataCgal::type(void) const
  Returns the type of the trim loop, it can either be dtkContinuousGeometryEnums::TrimLoopType::outer, dtkContinuousGeometryEnums::TrimLoopType::inner or dtkContinuousGeometryEnums::TrimLoopType::unknown.
 */
dtkContinuousGeometryEnums::TrimLoopType dtkTrimLoopDataCgal::type(void) const
{
    return m_type;
}

/*! \fn dtkTrimLoopDataCgal::setType(dtkContinuousGeometryEnums::TrimLoopType p_type) const
  Sets the type of the trim loop.

  \a p_type : type of the trim loop, it can be dtkContinuousGeometryEnums::TrimLoopType::outer, dtkContinuousGeometryEnums::TrimLoopType::inner or dtkContinuousGeometryEnums::TrimLoopType::unknown
*/
void dtkTrimLoopDataCgal::setType(dtkContinuousGeometryEnums::TrimLoopType p_type) const
{
    m_type = p_type;
}

/*! \fn dtkTrimLoopDataCgal::isPointCulled(dtkContinuousGeometryPrimitives::Point_2 p_point) const
  Returns true if \a p_point is culled by the trim loop.

  \a p_point : point to check if culled or not
*/
bool dtkTrimLoopDataCgal::isPointCulled(dtkContinuousGeometryPrimitives::Point_2 p_point) const
{
    if (m_initialized == false) {
        std::lock_guard<std::mutex> guard{mutex};
        if (m_initialized == false) {
        m_initialized = true;

/*        double segment_length = 0.001;
        std::list< dtkContinuousGeometryPrimitives::Point_2 > r_polyline;
        if(!this->toPolyline(r_polyline, segment_length)) {
            dtkWarn() << "The trim loop polyline could not be initialized";
        }
        // ///////////////////////////////////////////////////////////////////
        // Create the cgal equivalent (not the last point because same as first)
        // ///////////////////////////////////////////////////////////////////
        m_trim_polygon.reserve(r_polyline.size());
        for (auto& point : r_polyline) {
            m_trim_polygon.push_back(Point(point[0], point[1]));
        }*/

        for (auto trim : m_trims) {
          const dtkNurbsCurve2D& curve_2d = trim->curve2D();
          for (auto i = 0ul; i < curve_2d.nbCps() - 1; ++i) {
            double cp[2];
            curve_2d.controlPoint(i, cp);
            m_trim_polygon.push_back( K::Point_2(cp[0], cp[1]) );
          }
        }

        m_trim_polygon.push_back( m_trim_polygon.front() );

        }
    }

    Point p(p_point[0], p_point[1]);
    CGAL::Bounded_side bounded_side = CGAL::bounded_side_2(m_trim_polygon.begin(), m_trim_polygon.end(), p, K());
    if (bounded_side == CGAL::ON_BOUNDED_SIDE || bounded_side == CGAL::ON_BOUNDARY) {
        // ///////////////////////////////////////////////////////////////////
        // If the pre image is in the loop and supposed to be inside : not culled, else culled
        // ///////////////////////////////////////////////////////////////////
        if(m_type == dtkContinuousGeometryEnums::TrimLoopType::inner) {
            return false; // not to be culled
        } else {
            return true;
        }
    } else {
        // ///////////////////////////////////////////////////////////////////
        // If the pre image not is in the loop and supposed to be outside : not culled, else culled
        // ///////////////////////////////////////////////////////////////////
        if(m_type == dtkContinuousGeometryEnums::TrimLoopType::outer) {
            return false;
        } else {
            return true;
        }
    }
}

/*! \fn dtkTrimLoopDataCgal::toPolyline(std::list< dtkContinuousGeometryPrimitives::Point_2 >& r_polyline, double p_discretisation_length) const
  Stores in \a r_polyline a polyline representation of the trim loop with regard to the length of discretisation (\a p_discretisation_length), returns true if successfull, else returns false.

  \a r_polyline : list in which the dtkContinuousGeometryPrimitives::Point_2 will be added as polyline representation of the curve
  \a p_discretisation_length : length in parameter space between two samples
 */
bool dtkTrimLoopDataCgal::toPolyline(std::list< dtkContinuousGeometryPrimitives::Point_2 >& r_polyline, double p_discretisation_length) const
{
    if (m_trims.empty()) {
        dtkWarn() << "The trim loop is empty, it cannot be converted to polyline";
        return false;
    }
    // ///////////////////////////////////////////////////////////////////
    // Stores the first trim polyline in r_polyline
    // ///////////////////////////////////////////////////////////////////
    (*m_trims.begin())->toPolyline(r_polyline, p_discretisation_length);
    if (r_polyline.empty()) {
        dtkWarn() << "One of the trims is empty, it cannot be converted to polyline";
        return false;
    }
    if (m_trims.size() > 1) {
        std::list< dtkContinuousGeometryPrimitives::Point_2 > temp;
        for (auto it = std::next(m_trims.begin()); it !=m_trims.end(); ++it) {
            (*it)->toPolyline(temp, p_discretisation_length);
            if (!temp.empty()) {
                // ///////////////////////////////////////////////////////////////////
                // Clustering
                // ///////////////////////////////////////////////////////////////////
                double distance = dtkContinuousGeometryTools::squaredDistance(*temp.begin(), *std::prev(r_polyline.end()));
                if(distance > 1e-2) {
                    dtkWarn() << "End point : " << (*std::prev(r_polyline.end()))[0] << " " << (*std::prev(r_polyline.end()))[1];
                    dtkWarn() << "Start point : " << (*temp.begin())[0] << " " << (*temp.begin())[1];
                    dtkWarn() << "Squared distance : " << distance;
                    dtkWarn() << "The start point and end point of two consecutives trims do not match.";
                    return false;
                }
                temp.erase(temp.begin());
                r_polyline.splice(r_polyline.end(), temp);
            } else {
                dtkWarn() << "The trim loop is empty, it cannot be converted to polyline";
                return false;
            }
        }
    }
    double distance = dtkContinuousGeometryTools::squaredDistance(*r_polyline.begin(), *std::prev(r_polyline.end()));
    if ( distance > 1e-2) {
        dtkWarn() << "End point : " << (*r_polyline.begin())[0] << " " << (*r_polyline.begin())[1];
        dtkWarn() << "Start point : " <<  (*std::prev(r_polyline.end()))[0] << " " <<  (*std::prev(r_polyline.end()))[1];
        dtkWarn() << "Squared distance : " << distance;
        dtkWarn() << "The start point and end point of the first and last trims do not match, it doesnt form a loop";
        return false;
    }
    return true;
}

/*! \fn dtkTrimLoopDataCgal::toPolylineApprox(std::list< dtkContinuousGeometryPrimitives::Point_2 >& r_polyline, double p_approximation) const
  Stores in \a r_polyline a polyline representation of the trim loop with regard to an approximation distance to the curve(\a p_approximation), returns true if successfull, else returns false.

  \a r_polyline : list in which the dtkContinuousGeometryPrimitives::Point_2 will be added as polyline representation of the curve
  \a p_approximation : maximal tolerated distance from the polygonalization to the trim loop
 */
bool dtkTrimLoopDataCgal::toPolylineApprox(std::list< dtkContinuousGeometryPrimitives::Point_2 >& r_polyline, double p_approximation) const
{
    if (m_trims.empty()) {
        dtkError() << "The trim loop is empty, it cannot be converted to polyline";
        return false;
    }
    // ///////////////////////////////////////////////////////////////////
    // Check that the last control point of the last trim matches the first control point of the first trim
    // ///////////////////////////////////////////////////////////////////
    dtkContinuousGeometryPrimitives::Point_2 first_point(0., 0.);
    m_trims.front()->curve2D().controlPoint(0, first_point.data());
    dtkContinuousGeometryPrimitives::Point_2 last_point(0., 0.);
    m_trims.back()->curve2D().controlPoint(m_trims.back()->curve2D().nbCps() - 1, last_point.data());
    double distance = dtkContinuousGeometryTools::squaredDistance(first_point, last_point);

    // ///////////////////////////////////////////////////////////////////
    // /!\ Arbitrary merging tolerance
    // ///////////////////////////////////////////////////////////////////
    if(distance > 1e-5) {
        // ///////////////////////////////////////////////////////////////////
        // Recover the parameters of the points on the nurbs curves
        // ///////////////////////////////////////////////////////////////////
        double first_point_p = 0.;
        double last_point_p = m_trims.back()->curve2D().knots()[m_trims.back()->curve2D().degree() + m_trims.back()->curve2D().nbCps() - 2];
        dtkContinuousGeometryPrimitives::Point_2 start_point(0., 0.);
        m_trims.front()->curve2D().evaluatePoint(m_trims.front()->curve2D().knots()[0], start_point.data());
        if(start_point != first_point) {
            first_point_p = m_trims.front()->curve2D().knots()[m_trims.front()->curve2D().degree() + m_trims.front()->curve2D().nbCps() - 2];
        }
        dtkContinuousGeometryPrimitives::Point_2 end_point(0., 0.);
        m_trims.back()->curve2D().evaluatePoint(m_trims.back()->curve2D().knots()[m_trims.back()->curve2D().degree() + m_trims.back()->curve2D().nbCps() - 2], end_point.data());
        if(end_point != last_point) {
            last_point_p = 0.;
        }

        // ///////////////////////////////////////////////////////////////////
        // Computes a circle at the barycenter of the two extremities of radius alpha times the distance
        // ///////////////////////////////////////////////////////////////////
        dtkContinuousGeometryPrimitives::Circle_2 circle(dtkContinuousGeometryPrimitives::Point_2((first_point[0] + last_point[0]) / 2., (first_point[1] + last_point[1]) / 2.), 2 * distance);

        // ///////////////////////////////////////////////////////////////////
        // Computes the intersections between the circle and the NURBS curves
        // ///////////////////////////////////////////////////////////////////

        dtkNurbsCurve2DCircleIntersector *start_intersector = dtkContinuousGeometry::nurbsCurve2DCircleIntersector::pluginFactory().create("npNurbsCurve2DCircleIntersector");
        start_intersector->setNurbsCurve2D(const_cast< dtkNurbsCurve2D * >(&m_trims.front()->curve2D()));
        start_intersector->setCircle(circle);
        start_intersector->run();
        std::vector< double > start_intersection_parameters = start_intersector->intersectionParameters();
        for(auto p : start_intersection_parameters) {
            std::cerr << p << std::endl;
        }

        dtkNurbsCurve2DCircleIntersector *end_intersector = dtkContinuousGeometry::nurbsCurve2DCircleIntersector::pluginFactory().create("npNurbsCurve2DCircleIntersector");
        end_intersector->setNurbsCurve2D(const_cast< dtkNurbsCurve2D * >(&m_trims.back()->curve2D()));
        end_intersector->setCircle(circle);
        end_intersector->run();
        std::vector< double > end_intersection_parameters = end_intersector->intersectionParameters();
        for(auto p : end_intersection_parameters) {
            std::cerr << p << std::endl;
        }
        std::cerr << Q_FUNC_INFO << std::endl;
        getchar();
        // m_trims.front()->toPolylineApprox(r_polyline, p_approximation, p_0, 1.);
    } else {
        m_trims.front()->toPolylineApprox(r_polyline, p_approximation);
    }
    // ///////////////////////////////////////////////////////////////////
    // Stores the first trim polyline in r_polyline
    // ///////////////////////////////////////////////////////////////////
    if (r_polyline.empty()) {
        dtkWarn() << "One of the trims is empty, it cannot be converted to polyline";
        return false;
    }
    if (m_trims.size() > 1) {
        std::list< dtkContinuousGeometryPrimitives::Point_2 > temp;
        for (auto it = std::next(m_trims.begin()); it !=m_trims.end(); ++it) {
            // ///////////////////////////////////////////////////////////////////
            // Check that the last control point of the previous trim matches the first control point of the current trim
            // ///////////////////////////////////////////////////////////////////
            dtkContinuousGeometryPrimitives::Point_2 first_point(0., 0.);
            (*it)->curve2D().controlPoint(0, first_point.data());
            dtkContinuousGeometryPrimitives::Point_2 last_point(0., 0.);
            (*std::prev(it))->curve2D().controlPoint((*std::prev(it))->curve2D().nbCps() - 1, last_point.data());

            double distance = dtkContinuousGeometryTools::squaredDistance(first_point, last_point);
            if(distance > 1e-5) {
                // dtkWarn() << "End point : " << (*std::prev(r_polyline.end()))[0] << " " << (*std::prev(r_polyline.end()))[1];
                // dtkWarn() << "Start point : " << (*temp.begin())[0] << " " << (*temp.begin())[1];
                // dtkWarn() << "Squared distance : " << distance;
                // dtkWarn() << "The start point and end point of two consecutives trims do not match.";

                // ///////////////////////////////////////////////////////////////////
                // Recover the parameters of the points on the nurbs curves
                // ///////////////////////////////////////////////////////////////////
                double first_point_p = 0.;
                double last_point_p = (*std::prev(it))->curve2D().knots()[(*std::prev(it))->curve2D().degree() + (*std::prev(it))->curve2D().nbCps() - 2];
                dtkContinuousGeometryPrimitives::Point_2 start_point(0., 0.);
                (*it)->curve2D().evaluatePoint((*it)->curve2D().knots()[0], start_point.data());
                if(start_point != first_point) {
                    first_point_p = (*it)->curve2D().knots()[(*it)->curve2D().degree() + (*it)->curve2D().nbCps() - 2];
                }
                dtkContinuousGeometryPrimitives::Point_2 end_point(0., 0.);
                (*std::prev(it))->curve2D().evaluatePoint((*std::prev(it))->curve2D().knots()[(*std::prev(it))->curve2D().degree() + (*std::prev(it))->curve2D().nbCps() - 2], end_point.data());
                if(end_point != last_point) {
                    last_point_p = 0.;
                }

                // ///////////////////////////////////////////////////////////////////
                // Computes a circle at the barycenter of the two extremities of radius alpha times the distance
                // ///////////////////////////////////////////////////////////////////
                dtkContinuousGeometryPrimitives::Circle_2 circle(dtkContinuousGeometryPrimitives::Point_2((first_point[0] + last_point[0]) / 2., (first_point[1] + last_point[1]) / 2.), 2 * distance);

                // ///////////////////////////////////////////////////////////////////
                // Computes the intersections between the circle and the NURBS curves
                // ///////////////////////////////////////////////////////////////////

                dtkNurbsCurve2DCircleIntersector *start_intersector = dtkContinuousGeometry::nurbsCurve2DCircleIntersector::pluginFactory().create("npNurbsCurve2DCircleIntersector");
                start_intersector->setNurbsCurve2D(const_cast< dtkNurbsCurve2D * >(&(*it)->curve2D()));
                start_intersector->setCircle(circle);
                start_intersector->run();
                std::vector< double > start_intersection_parameters = start_intersector->intersectionParameters();
                for(auto p : start_intersection_parameters) {
                    std::cerr << p << std::endl;
                }

                dtkNurbsCurve2DCircleIntersector *end_intersector = dtkContinuousGeometry::nurbsCurve2DCircleIntersector::pluginFactory().create("npNurbsCurve2DCircleIntersector");
                end_intersector->setNurbsCurve2D(const_cast< dtkNurbsCurve2D * >(&(*std::prev(it))->curve2D()));
                end_intersector->setCircle(circle);
                end_intersector->run();
                std::vector< double > end_intersection_parameters = end_intersector->intersectionParameters();
                for(auto p : end_intersection_parameters) {
                    std::cerr << p << std::endl;
                }
                std::cerr << Q_FUNC_INFO << std::endl;
                getchar();
                return false;
            } else {
                (*it)->toPolylineApprox(temp, p_approximation);
            }
            if (!temp.empty()) {
                // ///////////////////////////////////////////////////////////////////
                // Clustering
                // ///////////////////////////////////////////////////////////////////
                temp.erase(temp.begin());
                r_polyline.splice(r_polyline.end(), temp);
            } else {
                dtkWarn() << "The trim loop is empty, it cannot be converted to polyline";
                return false;
            }
        }
    }
    distance = dtkContinuousGeometryTools::squaredDistance(*r_polyline.begin(), *std::prev(r_polyline.end()));
    if ( distance > 1e-2) {
        dtkWarn() << "End point : " << (*r_polyline.begin())[0] << " " << (*r_polyline.begin())[1];
        dtkWarn() << "Start point : " <<  (*std::prev(r_polyline.end()))[0] << " " <<  (*std::prev(r_polyline.end()))[1];
        dtkWarn() << "Squared distance : " << distance;
        dtkWarn() << "The start point and end point of the first and last trims do not match, it doesnt form a loop";
        return false;
    }
    return true;
}

/*! \fn dtkTrimLoopDataCgal::isValid(void) const
  Returns true if the all the trims of the trim loop are connected, else returns false.
 */
bool dtkTrimLoopDataCgal::isValid(void) const
{
    // ///////////////////////////////////////////////////////////////////
    // Checks that all the trims of the trim loop are connected
    // ///////////////////////////////////////////////////////////////////
    dtkContinuousGeometryPrimitives::Point_2 p_start(0., 0.);
    dtkContinuousGeometryPrimitives::Point_2 p_end(0., 0.);
    std::size_t nb_of_knots = 0;
    if (m_trims.size() == 0) {
        dtkWarn() << "The trim loop doesn't contain any trim";
        return false;
    } else if (m_trims.size() == 1) {
        nb_of_knots = (*m_trims.begin())->curve2D().nbCps() + (*m_trims.begin())->curve2D().degree() -1;
        double* knots = new double[nb_of_knots];
        (*m_trims.begin())->curve2D().knots(&knots[0]);
        (*m_trims.begin())->curve2D().evaluatePoint(knots[0], p_start.data());
        (*m_trims.begin())->curve2D().evaluatePoint(knots[nb_of_knots - 1], p_end.data());
        delete[] knots;
        if (!(p_start == p_end)) {
            dtkWarn() << "There is a single trim in the loop, and the trim is not closed";
            return false;
        } else {
            return true;
        }
    } else {
        // ///////////////////////////////////////////////////////////////////
        // Recovers the first trim
        // ///////////////////////////////////////////////////////////////////
        nb_of_knots = (*m_trims.begin())->curve2D().nbCps() + (*m_trims.begin())->curve2D().degree() - 1;
        dtkContinuousGeometryPrimitives::Point_2 p_init(0., 0.);
        double* knots_1 = new double[nb_of_knots];
        (*m_trims.begin())->curve2D().knots(&knots_1[0]);
        (*m_trims.begin())->curve2D().evaluatePoint(knots_1[0], p_init.data());
        (*m_trims.begin())->curve2D().evaluatePoint(knots_1[nb_of_knots - 1], p_end.data());
        delete[] knots_1;
        for (auto trim = std::next(m_trims.begin()); trim != m_trims.end(); ++trim) {
            nb_of_knots = (*trim)->curve2D().nbCps() + (*trim)->curve2D().degree() - 1;
            double* knots = new double[nb_of_knots];
            (*trim)->curve2D().knots(&knots[0]);
            (*trim)->curve2D().evaluatePoint(knots[0], p_start.data());
            double distance = dtkContinuousGeometryTools::squaredDistance(p_start, p_end);
            if (distance > 1e-5) {
                delete[] knots;
                dtkWarn() << "End point : " << p_end[0] << " " << p_end[1];
                dtkWarn() << "Start point : " << p_start[0] << " " << p_start[1];
                dtkWarn() << "Squared distance : " << distance;
                dtkWarn() << "The trims do not form a loop.";
                return false;
            }
            (*trim)->curve2D().evaluatePoint(knots[nb_of_knots - 1], p_end.data());
            delete[] knots;
        }
        // ///////////////////////////////////////////////////////////////////
        // Checks the last point of the last curve with the first point of the first curve
        // ///////////////////////////////////////////////////////////////////
        double distance = dtkContinuousGeometryTools::squaredDistance(p_end, p_init);
        if (distance > 1e-5) {
            dtkWarn() << "End point : " << p_end[0] << " " << p_end[1];
            dtkWarn() << "Start point : " << p_init[0] << " " << p_init[1];
            dtkWarn() << "Squared distance : " << distance;
            dtkWarn() << "The trims do not form a loop.";
            return false;
        }
        return true;
    }
}

/*! \fn dtkTrimLoopDataCgal::aabb(double* r_aabb) const
  Writes in \a r_aabb the [xmin, ymin, zmin, xmax, ymax, zmax] coordinates.

  \a r_aabb : array of size 6 to store the limits coordinates
*/
void dtkTrimLoopDataCgal::aabb(double* r_aabb) const
{
    std::array<double,4> aabb;
    r_aabb[0] = std::numeric_limits<double>::max();
    r_aabb[1] = std::numeric_limits<double>::max();
    r_aabb[2] = std::numeric_limits<double>::min();
    r_aabb[3] = std::numeric_limits<double>::min();

    for (auto trim = m_trims.begin(); trim != m_trims.end(); ++trim) {
        (*trim)->curve2D().aabb(&aabb[0]);
        if(r_aabb[0] > aabb[0]) {
            r_aabb[0] = aabb[0];
        }
        if(r_aabb[1] > aabb[1]) {
            r_aabb[1] = aabb[1];
        }
        if(r_aabb[2] < aabb[2]) {
            r_aabb[2] = aabb[2];
        }
        if(r_aabb[3] < aabb[3]) {
            r_aabb[3] = aabb[3];
        }
    }
}

/*! \fn dtkTrimLoopDataCgal::clone(void) const
   Clone
*/
dtkTrimLoopDataCgal* dtkTrimLoopDataCgal::clone(void) const
{
    return new dtkTrimLoopDataCgal(*this);
}

//
// dtkTrimLoopDataCgal.cpp ends here
