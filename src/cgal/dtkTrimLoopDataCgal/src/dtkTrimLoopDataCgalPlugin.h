// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <QtCore>

#include <dtkContinuousGeometry>

#include <dtkAbstractTrimLoopData>

class dtkTrimLoopDataCgalPlugin : public dtkAbstractTrimLoopDataPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkAbstractTrimLoopDataPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkTrimLoopDataCgalPlugin" FILE "dtkTrimLoopDataCgalPlugin.json")

public:
     dtkTrimLoopDataCgalPlugin(void) {}
    ~dtkTrimLoopDataCgalPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

//
// dtkTrimLoopDataCgalPlugin.h ends here
