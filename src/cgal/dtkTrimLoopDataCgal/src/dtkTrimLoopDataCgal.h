// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <dtkContinuousGeometry.h>

#include <dtkContinuousGeometryUtils>

#include <dtkTrimLoopDataCgalExport.h>

#include <dtkAbstractTrimLoopData>

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>

#include <atomic>
#include <mutex>

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef K::Point_2 Point;

class DTKTRIMLOOPDATACGAL_EXPORT dtkTrimLoopDataCgal final : public dtkAbstractTrimLoopData
{
 public:
    dtkTrimLoopDataCgal(void);
    dtkTrimLoopDataCgal(const dtkTrimLoopDataCgal& other) : dtkAbstractTrimLoopData(other) {}
    ~dtkTrimLoopDataCgal(void) final;

 public:
    void setType(dtkContinuousGeometryEnums::TrimLoopType p_type) const override;

    dtkContinuousGeometryEnums::TrimLoopType type(void) const override;

    bool isPointCulled(dtkContinuousGeometryPrimitives::Point_2 p_point) const override;

    bool toPolyline(std::list< dtkContinuousGeometryPrimitives::Point_2 >& r_polyline, double p_discretisation_length) const override;
    bool toPolylineApprox(std::list< dtkContinuousGeometryPrimitives::Point_2 >& r_polyline, double p_approximation) const override;

    bool isValid(void) const override;

    void aabb(double* r_aabb) const override;

public:
    dtkTrimLoopDataCgal* clone(void) const override;

 private:
    mutable dtkContinuousGeometryEnums::TrimLoopType m_type;

    mutable std::vector< Point > m_trim_polygon;
    mutable std::atomic<bool> m_initialized = { false };
    mutable std::mutex mutex;
};


// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

inline dtkAbstractTrimLoopData *dtkTrimLoopDataCgalCreator(void)
{
    return new dtkTrimLoopDataCgal();
}

//
// dtkTrimLoopDataCgal.h ends here
